/* start module: html5.document */
$pyjs['loaded_modules']['html5.document'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['html5.document']['__was_initialized__']) return $pyjs['loaded_modules']['html5.document'];
	if(typeof $pyjs['loaded_modules']['html5'] == 'undefined' || !$pyjs['loaded_modules']['html5']['__was_initialized__']) $p['___import___']('html5', null);
	var $m = $pyjs['loaded_modules']['html5.document'];
	$m['__repr__'] = function() { return '<module: html5.document>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'html5.document';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	$pyjs['loaded_modules']['html5']['document'] = $pyjs['loaded_modules']['html5.document'];
	try {
		$m.__track_lines__[1] = 'html5.document.py, line 1:\n    ';
		$m.__track_lines__[2] = 'html5.document.py, line 2:\n    def createAttribute( tag ):';
		$m.__track_lines__[6] = 'html5.document.py, line 6:\n    return( eval( "window.top.document.createAttribute( \\"%s\\" )" % tag ) )';
		$m.__track_lines__[9] = 'html5.document.py, line 9:\n    def createElement( element ):';
		$m.__track_lines__[13] = 'html5.document.py, line 13:\n    return( eval( "window.top.document.createElement( \\"%s\\" )" % element ) )';
		$m.__track_lines__[16] = 'html5.document.py, line 16:\n    def getElementById( idTag ):';
		$m.__track_lines__[17] = 'html5.document.py, line 17:\n    return( eval( "window.top.document.getElementById( \\"%s\\" )" % idTag ) )';
		$m.__track_lines__[19] = 'html5.document.py, line 19:\n    def getElementsByTagName( tagName ):';
		$m.__track_lines__[20] = 'html5.document.py, line 20:\n    doc = eval("window.top.document");';
		$m.__track_lines__[21] = 'html5.document.py, line 21:\n    res = []';
		$m.__track_lines__[22] = 'html5.document.py, line 22:\n    htmlCol = doc.getElementsByTagName( tagName )';
		$m.__track_lines__[23] = 'html5.document.py, line 23:\n    for x in range( 0, htmlCol.length ):';
		$m.__track_lines__[24] = 'html5.document.py, line 24:\n    res.append( htmlCol.item(x) )';
		$m.__track_lines__[25] = 'html5.document.py, line 25:\n    return( res )';

		var $constant_int_0 = new $p['int'](0);
		$pyjs['track']['module']='html5.document';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=2;
		$m['createAttribute'] = function(tag) {
			if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);

			$pyjs['track']={'module':'html5.document','lineno':2};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='html5.document';
			$pyjs['track']['lineno']=2;
			$pyjs['track']['lineno']=6;
			$pyjs['track']['lineno']=6;
			var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
			return (typeof eval == "undefined"?$m['eval']:eval)((function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['sprintf']('window.top.document.createAttribute( "%s" )', tag);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})();
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['createAttribute']['__name__'] = 'createAttribute';

		$m['createAttribute']['__bind_type__'] = 0;
		$m['createAttribute']['__args__'] = [null,null,['tag']];
		$pyjs['track']['lineno']=9;
		$m['createElement'] = function(element) {
			if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);

			$pyjs['track']={'module':'html5.document','lineno':9};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='html5.document';
			$pyjs['track']['lineno']=9;
			$pyjs['track']['lineno']=13;
			$pyjs['track']['lineno']=13;
			var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
			return (typeof eval == "undefined"?$m['eval']:eval)((function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['sprintf']('window.top.document.createElement( "%s" )', element);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})();
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['createElement']['__name__'] = 'createElement';

		$m['createElement']['__bind_type__'] = 0;
		$m['createElement']['__args__'] = [null,null,['element']];
		$pyjs['track']['lineno']=16;
		$m['getElementById'] = function(idTag) {
			if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);

			$pyjs['track']={'module':'html5.document','lineno':16};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='html5.document';
			$pyjs['track']['lineno']=16;
			$pyjs['track']['lineno']=17;
			$pyjs['track']['lineno']=17;
			var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
			return (typeof eval == "undefined"?$m['eval']:eval)((function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['sprintf']('window.top.document.getElementById( "%s" )', idTag);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})();
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['getElementById']['__name__'] = 'getElementById';

		$m['getElementById']['__bind_type__'] = 0;
		$m['getElementById']['__args__'] = [null,null,['idTag']];
		$pyjs['track']['lineno']=19;
		$m['getElementsByTagName'] = function(tagName) {
			if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
			var $iter1_nextval,$iter1_type,doc,$pyjs__trackstack_size_1,$iter1_iter,$iter1_array,res,x,htmlCol,$iter1_idx;
			$pyjs['track']={'module':'html5.document','lineno':19};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='html5.document';
			$pyjs['track']['lineno']=19;
			$pyjs['track']['lineno']=20;
			doc = (function(){try{try{$pyjs['in_try_except'] += 1;
			return (typeof eval == "undefined"?$m['eval']:eval)('window.top.document');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})();
			$pyjs['track']['lineno']=21;
			res = (function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})();
			$pyjs['track']['lineno']=22;
			htmlCol = (function(){try{try{$pyjs['in_try_except'] += 1;
			return doc['getElementsByTagName'](tagName);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})();
			$pyjs['track']['lineno']=23;
			$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
			$iter1_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
			return (function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['range']($constant_int_0, $p['getattr'](htmlCol, 'length'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})();
			$iter1_nextval=$p['__iter_prepare']($iter1_iter,false);
			while (typeof($p['__wrapped_next']($iter1_nextval)['$nextval']) != 'undefined') {
				x = $iter1_nextval['$nextval'];
				$pyjs['track']['lineno']=24;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return res['append']((function(){try{try{$pyjs['in_try_except'] += 1;
				return htmlCol['item'](x);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})();
			}
			if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
				$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
				$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
			}
			$pyjs['track']['module']='html5.document';
			$pyjs['track']['lineno']=25;
			$pyjs['track']['lineno']=25;
			var $pyjs__ret = res;
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['getElementsByTagName']['__name__'] = 'getElementsByTagName';

		$m['getElementsByTagName']['__bind_type__'] = 0;
		$m['getElementsByTagName']['__args__'] = [null,null,['tagName']];
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end html5.document */


/* end module: html5.document */


