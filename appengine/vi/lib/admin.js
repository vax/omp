/* start module: admin */
$pyjs['loaded_modules']['admin'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['admin']['__was_initialized__']) return $pyjs['loaded_modules']['admin'];
	var $m = $pyjs['loaded_modules']['admin'];
	$m['__repr__'] = function() { return '<module: admin>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'admin';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	try {
		$m.__track_lines__[1] = 'admin.py, line 1:\n    import pyjd';
		$m.__track_lines__[2] = 'admin.py, line 2:\n    import html5';
		$m.__track_lines__[3] = 'admin.py, line 3:\n    from config import conf';
		$m.__track_lines__[4] = 'admin.py, line 4:\n    from widgets import TopBarWidget';
		$m.__track_lines__[5] = 'admin.py, line 5:\n    from widgets.userlogoutmsg import UserLogoutMsg';
		$m.__track_lines__[6] = 'admin.py, line 6:\n    import json';
		$m.__track_lines__[7] = 'admin.py, line 7:\n    from network import NetworkService, DeferredCall';
		$m.__track_lines__[8] = 'admin.py, line 8:\n    import handler';
		$m.__track_lines__[9] = 'admin.py, line 9:\n    import bones';
		$m.__track_lines__[10] = 'admin.py, line 10:\n    import actions';
		$m.__track_lines__[11] = 'admin.py, line 11:\n    import i18n';
		$m.__track_lines__[12] = 'admin.py, line 12:\n    from event import viInitializedEvent';
		$m.__track_lines__[13] = 'admin.py, line 13:\n    from priorityqueue import HandlerClassSelector, initialHashHandler, startupQueue';
		$m.__track_lines__[14] = 'admin.py, line 14:\n    from log import Log';
		$m.__track_lines__[15] = 'admin.py, line 15:\n    from pane import Pane, GroupPane';
		$m.__track_lines__[16] = 'admin.py, line 16:\n    from i18n import translate';
		$m.__track_lines__[18] = 'admin.py, line 18:\n    try:';
		$m.__track_lines__[19] = 'admin.py, line 19:\n    import vi_plugins';
		$m.__track_lines__[21] = 'admin.py, line 21:\n    pass';
		$m.__track_lines__[23] = 'admin.py, line 23:\n    class CoreWindow( html5.Div ):';
		$m.__track_lines__[25] = 'admin.py, line 25:\n    def __init__(self, *args, **kwargs ):';
		$m.__track_lines__[26] = 'admin.py, line 26:\n    super( CoreWindow, self ).__init__( *args, **kwargs )';
		$m.__track_lines__[27] = 'admin.py, line 27:\n    self["id"]="CoreWindow"';
		$m.__track_lines__[28] = 'admin.py, line 28:\n    self.topBar = TopBarWidget()';
		$m.__track_lines__[29] = 'admin.py, line 29:\n    self.appendChild( self.topBar )';
		$m.__track_lines__[30] = 'admin.py, line 30:\n    self.workSpace = html5.Div()';
		$m.__track_lines__[31] = 'admin.py, line 31:\n    self.workSpace["class"] = "vi_workspace"';
		$m.__track_lines__[32] = 'admin.py, line 32:\n    self.appendChild( self.workSpace )';
		$m.__track_lines__[33] = 'admin.py, line 33:\n    self.modulMgr = html5.Div()';
		$m.__track_lines__[34] = 'admin.py, line 34:\n    self.modulMgr["class"] = "vi_wm"';
		$m.__track_lines__[35] = 'admin.py, line 35:\n    self.appendChild( self.modulMgr )';
		$m.__track_lines__[36] = 'admin.py, line 36:\n    self.modulList = html5.Nav()';
		$m.__track_lines__[37] = 'admin.py, line 37:\n    self.modulList["class"] = "vi_manager"';
		$m.__track_lines__[38] = 'admin.py, line 38:\n    self.modulMgr.appendChild( self.modulList )';
		$m.__track_lines__[39] = 'admin.py, line 39:\n    self.modulListUl = html5.Ul()';
		$m.__track_lines__[40] = 'admin.py, line 40:\n    self.modulListUl["class"] = "modullist"';
		$m.__track_lines__[41] = 'admin.py, line 41:\n    self.modulList.appendChild( self.modulListUl )';
		$m.__track_lines__[42] = 'admin.py, line 42:\n    self.viewport = html5.Div()';
		$m.__track_lines__[43] = 'admin.py, line 43:\n    self.viewport["class"] = "vi_viewer"';
		$m.__track_lines__[44] = 'admin.py, line 44:\n    self.workSpace.appendChild( self.viewport)';
		$m.__track_lines__[45] = 'admin.py, line 45:\n    self.logWdg = Log()';
		$m.__track_lines__[46] = 'admin.py, line 46:\n    self.appendChild( self.logWdg )';
		$m.__track_lines__[51] = 'admin.py, line 51:\n    self.currentPane = None';
		$m.__track_lines__[52] = 'admin.py, line 52:\n    self.nextPane = None #Which pane gains focus once the deferred call fires';
		$m.__track_lines__[53] = 'admin.py, line 53:\n    self.panes = [] # List of known panes. The ordering represents the order in which the user visited them.';
		$m.__track_lines__[54] = 'admin.py, line 54:\n    self.config = None';
		$m.__track_lines__[55] = 'admin.py, line 55:\n    self.user = None';
		$m.__track_lines__[56] = 'admin.py, line 56:\n    self.userLoggedOutMsg=UserLogoutMsg()';
		$m.__track_lines__[57] = 'admin.py, line 57:\n    self["class"].append("is_loading")';
		$m.__track_lines__[58] = 'admin.py, line 58:\n    startupQueue.setFinalElem( self.startup )';
		$m.__track_lines__[59] = "admin.py, line 59:\n    self.sinkEvent('onUserTryedToLogin')";
		$m.__track_lines__[61] = 'admin.py, line 61:\n    le = eval("window.top.logError")';
		$m.__track_lines__[62] = 'admin.py, line 62:\n    w = eval("window")';
		$m.__track_lines__[63] = 'admin.py, line 63:\n    w.onerror = le';
		$m.__track_lines__[64] = 'admin.py, line 64:\n    w = eval("window.top")';
		$m.__track_lines__[65] = 'admin.py, line 65:\n    w.onerror = le';
		$m.__track_lines__[67] = 'admin.py, line 67:\n    def onUserTryedToLogin(self,event):';
		$m.__track_lines__[68] = 'admin.py, line 68:\n    self.userLoggedOutMsg.testUserAvaiable()';
		$m.__track_lines__[70] = 'admin.py, line 70:\n    def startup(self):';
		$m.__track_lines__[71] = 'admin.py, line 71:\n    NetworkService.request( None, "/admin/config", successHandler=self.onConfigAvailable,';
		$m.__track_lines__[73] = 'admin.py, line 73:\n    NetworkService.request( "user", "view/self", successHandler=self.onUserAvaiable,';
		$m.__track_lines__[76] = 'admin.py, line 76:\n    def log(self, type, msg ):';
		$m.__track_lines__[77] = 'admin.py, line 77:\n    self.logWdg.log( type, msg )';
		$m.__track_lines__[79] = 'admin.py, line 79:\n    def onConfigAvailable(self, req):';
		$m.__track_lines__[80] = 'admin.py, line 80:\n    self.config = NetworkService.decode(req)';
		$m.__track_lines__[81] = 'admin.py, line 81:\n    if self.user is not None:';
		$m.__track_lines__[82] = 'admin.py, line 82:\n    self.postInit()';
		$m.__track_lines__[84] = 'admin.py, line 84:\n    def onUserAvaiable(self, req):';
		$m.__track_lines__[85] = 'admin.py, line 85:\n    data = NetworkService.decode(req)';
		$m.__track_lines__[86] = 'admin.py, line 86:\n    conf["currentUser"] = data["values"]';
		$m.__track_lines__[87] = 'admin.py, line 87:\n    self.user = data';
		$m.__track_lines__[88] = 'admin.py, line 88:\n    if self.config is not None:';
		$m.__track_lines__[89] = 'admin.py, line 89:\n    self.postInit()';
		$m.__track_lines__[91] = 'admin.py, line 91:\n    def postInit(self):';
		$m.__track_lines__[92] = 'admin.py, line 92:\n    def getModulName(argIn):';
		$m.__track_lines__[93] = 'admin.py, line 93:\n    try:';
		$m.__track_lines__[94] = 'admin.py, line 94:\n    return( argIn[1]["name"].lower() )';
		$m.__track_lines__[96] = 'admin.py, line 96:\n    return( None )';
		$m.__track_lines__[98] = 'admin.py, line 98:\n    def getModulSortIndex(argIn):';
		$m.__track_lines__[99] = 'admin.py, line 99:\n    try:';
		$m.__track_lines__[100] = 'admin.py, line 100:\n    return( argIn[1]["sortIndex"] )';
		$m.__track_lines__[102] = 'admin.py, line 102:\n    return( None )';
		$m.__track_lines__[105] = 'admin.py, line 105:\n    groups = {}';
		$m.__track_lines__[106] = 'admin.py, line 106:\n    panes = []';
		$m.__track_lines__[107] = 'admin.py, line 107:\n    userAccess = self.user["values"].get( "access" ) or []';
		$m.__track_lines__[108] = 'admin.py, line 108:\n    predefinedFilterCounter = 1';
		$m.__track_lines__[110] = 'admin.py, line 110:\n    if ( "configuration" in self.config.keys()';
		$m.__track_lines__[115] = 'admin.py, line 115:\n    for group in self.config["configuration"]["modulGroups"]:';
		$m.__track_lines__[116] = 'admin.py, line 116:\n    p = GroupPane( group["name"], iconURL=group["icon"] )';
		$m.__track_lines__[118] = 'admin.py, line 118:\n    groups[ group["prefix"] ] = p';
		$m.__track_lines__[119] = 'admin.py, line 119:\n    if "sortIndex" in group.keys():';
		$m.__track_lines__[120] = 'admin.py, line 120:\n    sortIndex = group["sortIndex"]';
		$m.__track_lines__[122] = 'admin.py, line 122:\n    sortIndex = None';
		$m.__track_lines__[124] = 'admin.py, line 124:\n    panes.append( (group["name"], sortIndex, p) )';
		$m.__track_lines__[127] = 'admin.py, line 127:\n    sorted_modules = [(x,y) for x,y in self.config["modules"].items()]';
		$m.__track_lines__[128] = 'admin.py, line 128:\n    sorted_modules.sort(key=getModulName)';
		$m.__track_lines__[129] = 'admin.py, line 129:\n    sorted_modules.sort(key=getModulSortIndex, reverse=True)';
		$m.__track_lines__[131] = 'admin.py, line 131:\n    for module, info in sorted_modules:';
		$m.__track_lines__[132] = 'admin.py, line 132:\n    if not "root" in userAccess and not any([x.startswith(module) for x in userAccess]):';
		$m.__track_lines__[134] = 'admin.py, line 134:\n    continue';
		$m.__track_lines__[136] = 'admin.py, line 136:\n    conf["modules"][module] = info';
		$m.__track_lines__[138] = 'admin.py, line 138:\n    if "views" in conf["modules"][module].keys() and conf["modules"][module]["views"]:';
		$m.__track_lines__[139] = 'admin.py, line 139:\n    for v in conf["modules"][module]["views"]: #Work-a-round for PyJS not supporting id()';
		$m.__track_lines__[140] = 'admin.py, line 140:\n    v["__id"] = predefinedFilterCounter';
		$m.__track_lines__[141] = 'admin.py, line 141:\n    predefinedFilterCounter += 1';
		$m.__track_lines__[143] = 'admin.py, line 143:\n    handlerCls = HandlerClassSelector.select( module, info )';
		$m.__track_lines__[144] = 'admin.py, line 144:\n    assert handlerCls is not None, "No handler available for modul %s" % module';
		$m.__track_lines__[146] = 'admin.py, line 146:\n    isChild = False';
		$m.__track_lines__[147] = 'admin.py, line 147:\n    for k in groups.keys():';
		$m.__track_lines__[148] = 'admin.py, line 148:\n    if info["name"].startswith(k):';
		$m.__track_lines__[149] = 'admin.py, line 149:\n    handler = handlerCls( module, info, groupName=k )';
		$m.__track_lines__[150] = 'admin.py, line 150:\n    groups[k].addChildPane( handler )';
		$m.__track_lines__[151] = 'admin.py, line 151:\n    isChild = True';
		$m.__track_lines__[152] = 'admin.py, line 152:\n    break';
		$m.__track_lines__[154] = 'admin.py, line 154:\n    if not isChild:';
		$m.__track_lines__[155] = 'admin.py, line 155:\n    handler = handlerCls( module, info )';
		$m.__track_lines__[156] = 'admin.py, line 156:\n    if "sortIndex" in info.keys():';
		$m.__track_lines__[157] = 'admin.py, line 157:\n    sortIndex = info["sortIndex"]';
		$m.__track_lines__[159] = 'admin.py, line 159:\n    sortIndex = None';
		$m.__track_lines__[160] = 'admin.py, line 160:\n    panes.append( ( info["name"], sortIndex, handler ) )';
		$m.__track_lines__[163] = 'admin.py, line 163:\n    panes.sort( key=lambda x: x[0] )';
		$m.__track_lines__[164] = 'admin.py, line 164:\n    panes.sort( key=lambda x: x[1], reverse=True )';
		$m.__track_lines__[167] = 'admin.py, line 167:\n    for k, v, pane in panes:';
		$m.__track_lines__[169] = 'admin.py, line 169:\n    if ( isinstance( pane, GroupPane )';
		$m.__track_lines__[172] = 'admin.py, line 172:\n    continue';
		$m.__track_lines__[174] = 'admin.py, line 174:\n    self.addPane( pane )';
		$m.__track_lines__[177] = 'admin.py, line 177:\n    viInitializedEvent.fire()';
		$m.__track_lines__[178] = 'admin.py, line 178:\n    DeferredCall( self.checkInitialHash )';
		$m.__track_lines__[179] = 'admin.py, line 179:\n    self["class"].remove("is_loading")';
		$m.__track_lines__[181] = 'admin.py, line 181:\n    def checkInitialHash( self, *args, **kwargs ):';
		$m.__track_lines__[182] = 'admin.py, line 182:\n    urlHash = eval("window.top.location.hash")';
		$m.__track_lines__[183] = 'admin.py, line 183:\n    if not urlHash:';
		$m.__track_lines__[184] = 'admin.py, line 184:\n    return';
		$m.__track_lines__[185] = 'admin.py, line 185:\n    if "?" in urlHash:';
		$m.__track_lines__[186] = 'admin.py, line 186:\n    hashStr = urlHash[ : urlHash.find("?") ]';
		$m.__track_lines__[187] = 'admin.py, line 187:\n    paramsStr = urlHash[ urlHash.find("?")+1: ]';
		$m.__track_lines__[189] = 'admin.py, line 189:\n    hashStr = urlHash';
		$m.__track_lines__[190] = 'admin.py, line 190:\n    paramsStr = ""';
		$m.__track_lines__[191] = 'admin.py, line 191:\n    hashList = hashStr[1:].split("/")';
		$m.__track_lines__[192] = 'admin.py, line 192:\n    hashList = [x for x in hashList if x]';
		$m.__track_lines__[193] = 'admin.py, line 193:\n    params = {}';
		$m.__track_lines__[194] = 'admin.py, line 194:\n    if paramsStr:';
		$m.__track_lines__[195] = 'admin.py, line 195:\n    for pair in paramsStr.split("&"):';
		$m.__track_lines__[196] = 'admin.py, line 196:\n    if not "=" in pair:';
		$m.__track_lines__[197] = 'admin.py, line 197:\n    continue';
		$m.__track_lines__[198] = 'admin.py, line 198:\n    key = pair[ :pair.find("=") ]';
		$m.__track_lines__[199] = 'admin.py, line 199:\n    value = pair[ pair.find("=")+1: ]';
		$m.__track_lines__[200] = 'admin.py, line 200:\n    if not (key and value):';
		$m.__track_lines__[201] = 'admin.py, line 201:\n    continue';
		$m.__track_lines__[202] = 'admin.py, line 202:\n    if key in params.keys():';
		$m.__track_lines__[203] = 'admin.py, line 203:\n    if not isinstance( params[ key ], list ):';
		$m.__track_lines__[204] = 'admin.py, line 204:\n    params[ key ] = [ params[ key ] ]';
		$m.__track_lines__[205] = 'admin.py, line 205:\n    params[ key ].append( value )';
		$m.__track_lines__[207] = 'admin.py, line 207:\n    params[ key ] = value';
		$m.__track_lines__[208] = 'admin.py, line 208:\n    gen = initialHashHandler.select( hashList, params )';
		$m.__track_lines__[209] = 'admin.py, line 209:\n    if gen:';
		$m.__track_lines__[210] = 'admin.py, line 210:\n    gen( hashList, params )';
		$m.__track_lines__[212] = 'admin.py, line 212:\n    def onError(self, req, code):';
		$m.__track_lines__[213] = 'admin.py, line 213:\n    print("ONERROR")';
		$m.__track_lines__[215] = 'admin.py, line 215:\n    def _registerChildPanes(self, pane ):';
		$m.__track_lines__[216] = 'admin.py, line 216:\n    for childPane in pane.childPanes:';
		$m.__track_lines__[217] = 'admin.py, line 217:\n    self.panes.append(childPane)';
		$m.__track_lines__[218] = 'admin.py, line 218:\n    self.viewport.appendChild(childPane.widgetsDomElm)';
		$m.__track_lines__[219] = 'admin.py, line 219:\n    childPane.widgetsDomElm["style"]["display"] = "none"';
		$m.__track_lines__[220] = 'admin.py, line 220:\n    self._registerChildPanes(childPane)';
		$m.__track_lines__[222] = 'admin.py, line 222:\n    def addPane(self, pane, parentPane=None):';
		$m.__track_lines__[225] = 'admin.py, line 225:\n    if len(pane.childPanes)>0:';
		$m.__track_lines__[226] = 'admin.py, line 226:\n    self._registerChildPanes( pane )';
		$m.__track_lines__[227] = 'admin.py, line 227:\n    self.panes.append( pane )';
		$m.__track_lines__[228] = 'admin.py, line 228:\n    if parentPane:';
		$m.__track_lines__[229] = 'admin.py, line 229:\n    parentPane.addChildPane( pane )';
		$m.__track_lines__[230] = 'admin.py, line 230:\n    pane.parent = parentPane';
		$m.__track_lines__[232] = 'admin.py, line 232:\n    self.modulListUl.appendChild( pane )';
		$m.__track_lines__[233] = 'admin.py, line 233:\n    self.viewport.appendChild(pane.widgetsDomElm)';
		$m.__track_lines__[234] = 'admin.py, line 234:\n    pane.widgetsDomElm["style"]["display"] = "none"';
		$m.__track_lines__[237] = 'admin.py, line 237:\n    def stackPane(self, pane, focus=False):';
		$m.__track_lines__[238] = 'admin.py, line 238:\n    assert self.currentPane is not None, "Cannot stack a pane. There\'s no current one."';
		$m.__track_lines__[239] = 'admin.py, line 239:\n    self.addPane( pane, parentPane=self.currentPane )';
		$m.__track_lines__[240] = 'admin.py, line 240:\n    if focus and not self.nextPane:';
		$m.__track_lines__[244] = 'admin.py, line 244:\n    self.nextPane = pane';
		$m.__track_lines__[245] = 'admin.py, line 245:\n    DeferredCall( self.focusNextPane )';
		$m.__track_lines__[247] = 'admin.py, line 247:\n    def focusNextPane(self, *args, **kwargs):';
		$m.__track_lines__[251] = 'admin.py, line 251:\n    if not self.nextPane:';
		$m.__track_lines__[252] = 'admin.py, line 252:\n    return';
		$m.__track_lines__[253] = 'admin.py, line 253:\n    nextPane = self.nextPane';
		$m.__track_lines__[254] = 'admin.py, line 254:\n    self.nextPane = None';
		$m.__track_lines__[255] = 'admin.py, line 255:\n    self.focusPane( nextPane )';
		$m.__track_lines__[257] = 'admin.py, line 257:\n    def focusPane(self, pane):';
		$m.__track_lines__[258] = 'admin.py, line 258:\n    assert pane in self.panes, "Cannot focus unknown pane!"';
		$m.__track_lines__[262] = 'admin.py, line 262:\n    if pane == self.currentPane:';
		$m.__track_lines__[263] = 'admin.py, line 263:\n    if self.currentPane.collapseable and self.currentPane.childDomElem:';
		$m.__track_lines__[264] = 'admin.py, line 264:\n    if self.currentPane.childDomElem["style"]["display"] == "none":';
		$m.__track_lines__[265] = 'admin.py, line 265:\n    self.currentPane.childDomElem["style"]["display"] = "block"';
		$m.__track_lines__[267] = 'admin.py, line 267:\n    self.currentPane.childDomElem["style"]["display"] = "none"';
		$m.__track_lines__[268] = 'admin.py, line 268:\n    return';
		$m.__track_lines__[270] = 'admin.py, line 270:\n    self.panes.remove( pane ) # Move the pane to the end of the list';
		$m.__track_lines__[271] = 'admin.py, line 271:\n    self.panes.append( pane )';
		$m.__track_lines__[274] = 'admin.py, line 274:\n    if self.currentPane is not None:';
		$m.__track_lines__[275] = 'admin.py, line 275:\n    self.currentPane["class"].remove("is_active")';
		$m.__track_lines__[276] = 'admin.py, line 276:\n    self.currentPane.widgetsDomElm["style"]["display"] = "none"';
		$m.__track_lines__[279] = 'admin.py, line 279:\n    self.topBar.setCurrentModulDescr( pane.descr, pane.iconURL, pane.iconClasses )';
		$m.__track_lines__[280] = 'admin.py, line 280:\n    self.currentPane = pane';
		$m.__track_lines__[281] = 'admin.py, line 281:\n    self.currentPane.widgetsDomElm["style"]["display"] = "block"';
		$m.__track_lines__[283] = 'admin.py, line 283:\n    if self.currentPane.collapseable and self.currentPane.childDomElem:';
		$m.__track_lines__[284] = 'admin.py, line 284:\n    self.currentPane.childDomElem["style"]["display"] = "block"';
		$m.__track_lines__[286] = 'admin.py, line 286:\n    self.currentPane["class"].append("is_active")';
		$m.__track_lines__[288] = 'admin.py, line 288:\n    def removePane(self, pane):';
		$m.__track_lines__[289] = 'admin.py, line 289:\n    assert pane in self.panes, "Cannot remove unknown pane!"';
		$m.__track_lines__[290] = 'admin.py, line 290:\n    self.panes.remove( pane )';
		$m.__track_lines__[291] = 'admin.py, line 291:\n    if pane==self.currentPane:';
		$m.__track_lines__[292] = 'admin.py, line 292:\n    if self.panes:';
		$m.__track_lines__[293] = 'admin.py, line 293:\n    self.focusPane( self.panes[-1])';
		$m.__track_lines__[295] = 'admin.py, line 295:\n    self.currentPane = None';
		$m.__track_lines__[296] = 'admin.py, line 296:\n    if pane.parent == self:';
		$m.__track_lines__[297] = 'admin.py, line 297:\n    self.modulListUl.removeChild( pane )';
		$m.__track_lines__[299] = 'admin.py, line 299:\n    pane.parent.removeChildPane( pane )';
		$m.__track_lines__[300] = 'admin.py, line 300:\n    self.viewport.removeChild( pane.widgetsDomElm )';
		$m.__track_lines__[302] = 'admin.py, line 302:\n    def addWidget(self, widget, pane ):';
		$m.__track_lines__[303] = 'admin.py, line 303:\n    pane.addWidget( widget )';
		$m.__track_lines__[305] = 'admin.py, line 305:\n    def stackWidget(self, widget ):';
		$m.__track_lines__[306] = 'admin.py, line 306:\n    assert self.currentPane is not None, "Cannot stack a widget while no pane is active"';
		$m.__track_lines__[307] = 'admin.py, line 307:\n    self.addWidget( widget, self.currentPane )';
		$m.__track_lines__[309] = 'admin.py, line 309:\n    def removeWidget(self, widget ):';
		$m.__track_lines__[310] = 'admin.py, line 310:\n    for pane in self.panes:';
		$m.__track_lines__[311] = 'admin.py, line 311:\n    if pane.containsWidget( widget ):';
		$m.__track_lines__[312] = 'admin.py, line 312:\n    pane.removeWidget( widget )';
		$m.__track_lines__[313] = 'admin.py, line 313:\n    return';
		$m.__track_lines__[314] = 'admin.py, line 314:\n    raise AssertionError("Tried to remove unknown widget %s" % str( widget ))';
		$m.__track_lines__[316] = "admin.py, line 316:\n    if __name__ == '__main__':";
		$m.__track_lines__[317] = 'admin.py, line 317:\n    pyjd.setup("public/admin.html")';
		$m.__track_lines__[318] = 'admin.py, line 318:\n    conf["mainWindow"] = CoreWindow()';
		$m.__track_lines__[319] = 'admin.py, line 319:\n    html5.Body().appendChild( conf["mainWindow"] )';
		$m.__track_lines__[323] = 'admin.py, line 323:\n    startupQueue.run()';
		$m.__track_lines__[324] = 'admin.py, line 324:\n    pyjd.run()';
		var $pyjs_try_err;
		var $constant_int_0 = new $p['int'](0);
		var $constant_int_1 = new $p['int'](1);
		$pyjs['track']['module']='admin';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=1;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['pyjd'] = $p['___import___']('pyjd', null);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=2;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['html5'] = $p['___import___']('html5', null);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=3;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['conf'] = $p['___import___']('config.conf', null, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=4;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['TopBarWidget'] = $p['___import___']('widgets.TopBarWidget', null, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=5;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['UserLogoutMsg'] = $p['___import___']('widgets.userlogoutmsg.UserLogoutMsg', null, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=6;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['json'] = $p['___import___']('json', null);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=7;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['NetworkService'] = $p['___import___']('network.NetworkService', null, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['DeferredCall'] = $p['___import___']('network.DeferredCall', null, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=8;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['handler'] = $p['___import___']('handler', null);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=9;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['bones'] = $p['___import___']('bones', null);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=10;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['actions'] = $p['___import___']('actions', null);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=11;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['i18n'] = $p['___import___']('i18n', null);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=12;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['viInitializedEvent'] = $p['___import___']('event.viInitializedEvent', null, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=13;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['HandlerClassSelector'] = $p['___import___']('priorityqueue.HandlerClassSelector', null, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['initialHashHandler'] = $p['___import___']('priorityqueue.initialHashHandler', null, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['startupQueue'] = $p['___import___']('priorityqueue.startupQueue', null, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=14;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['Log'] = $p['___import___']('log.Log', null, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=15;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['Pane'] = $p['___import___']('pane.Pane', null, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['GroupPane'] = $p['___import___']('pane.GroupPane', null, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=16;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['translate'] = $p['___import___']('i18n.translate', null, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=18;
		var $pyjs__trackstack_size_1 = $pyjs['trackstack']['length'];
		try {
			try {
				$pyjs['in_try_except'] += 1;
				$pyjs['track']['lineno']=19;
				$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
				$m['vi_plugins'] = $p['___import___']('vi_plugins', null);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			} finally { $pyjs['in_try_except'] -= 1; }
		} catch($pyjs_try_err) {
			$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_1 - 1);
			$pyjs['__active_exception_stack__'] = null;
			$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
			var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
			$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
			if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
				$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
				$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
			}
			$pyjs['track']['module']='admin';
			if (($pyjs_try_err_name == $p['ImportError']['__name__'])||$p['_isinstance']($pyjs_try_err,$p['ImportError'])) {
				$pyjs['track']['lineno']=21;
			} else { $pyjs['__active_exception_stack__'] = $pyjs['__last_exception_stack__']; $pyjs['__last_exception_stack__'] = null; throw $pyjs_try_err; }
		}
		$pyjs['track']['lineno']=23;
		$m['CoreWindow'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'admin';
			$cls_definition['__md5__'] = 'e15094da3383fa9fb30fd5c3924d8e57';
			$pyjs['track']['lineno']=25;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var le,w;
				$pyjs['track']={'module':'admin', 'lineno':25};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=25;
				$pyjs['track']['lineno']=26;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['CoreWindow'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})();
				$pyjs['track']['lineno']=27;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('id', 'CoreWindow');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})();
				$pyjs['track']['lineno']=28;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('topBar', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['TopBarWidget']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})()) : $p['setattr'](self, 'topBar', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['TopBarWidget']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})()); 
				$pyjs['track']['lineno']=29;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'topBar'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})();
				$pyjs['track']['lineno']=30;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('workSpace', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})()) : $p['setattr'](self, 'workSpace', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})()); 
				$pyjs['track']['lineno']=31;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'workSpace')['__setitem__']('class', 'vi_workspace');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})();
				$pyjs['track']['lineno']=32;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'workSpace'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})();
				$pyjs['track']['lineno']=33;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modulMgr', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})()) : $p['setattr'](self, 'modulMgr', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})()); 
				$pyjs['track']['lineno']=34;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'modulMgr')['__setitem__']('class', 'vi_wm');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})();
				$pyjs['track']['lineno']=35;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'modulMgr'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})();
				$pyjs['track']['lineno']=36;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modulList', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Nav']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})()) : $p['setattr'](self, 'modulList', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Nav']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})()); 
				$pyjs['track']['lineno']=37;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'modulList')['__setitem__']('class', 'vi_manager');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})();
				$pyjs['track']['lineno']=38;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['modulMgr']['appendChild']($p['getattr'](self, 'modulList'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_14_err){if (!$p['isinstance']($pyjs_dbg_14_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_14_err);}throw $pyjs_dbg_14_err;
}})();
				$pyjs['track']['lineno']=39;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modulListUl', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Ul']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})()) : $p['setattr'](self, 'modulListUl', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Ul']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})()); 
				$pyjs['track']['lineno']=40;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'modulListUl')['__setitem__']('class', 'modullist');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_16_err){if (!$p['isinstance']($pyjs_dbg_16_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_16_err);}throw $pyjs_dbg_16_err;
}})();
				$pyjs['track']['lineno']=41;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['modulList']['appendChild']($p['getattr'](self, 'modulListUl'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_17_err){if (!$p['isinstance']($pyjs_dbg_17_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_17_err);}throw $pyjs_dbg_17_err;
}})();
				$pyjs['track']['lineno']=42;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('viewport', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})()) : $p['setattr'](self, 'viewport', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})()); 
				$pyjs['track']['lineno']=43;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'viewport')['__setitem__']('class', 'vi_viewer');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_19_err){if (!$p['isinstance']($pyjs_dbg_19_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_19_err);}throw $pyjs_dbg_19_err;
}})();
				$pyjs['track']['lineno']=44;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['workSpace']['appendChild']($p['getattr'](self, 'viewport'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})();
				$pyjs['track']['lineno']=45;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('logWdg', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['Log']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})()) : $p['setattr'](self, 'logWdg', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['Log']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})()); 
				$pyjs['track']['lineno']=46;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'logWdg'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})();
				$pyjs['track']['lineno']=51;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentPane', null) : $p['setattr'](self, 'currentPane', null); 
				$pyjs['track']['lineno']=52;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('nextPane', null) : $p['setattr'](self, 'nextPane', null); 
				$pyjs['track']['lineno']=53;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('panes', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})()) : $p['setattr'](self, 'panes', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})()); 
				$pyjs['track']['lineno']=54;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('config', null) : $p['setattr'](self, 'config', null); 
				$pyjs['track']['lineno']=55;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('user', null) : $p['setattr'](self, 'user', null); 
				$pyjs['track']['lineno']=56;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('userLoggedOutMsg', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['UserLogoutMsg']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})()) : $p['setattr'](self, 'userLoggedOutMsg', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['UserLogoutMsg']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})()); 
				$pyjs['track']['lineno']=57;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__getitem__']('class')['append']('is_loading');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})();
				$pyjs['track']['lineno']=58;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['startupQueue']['setFinalElem']($p['getattr'](self, 'startup'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_26_err){if (!$p['isinstance']($pyjs_dbg_26_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_26_err);}throw $pyjs_dbg_26_err;
}})();
				$pyjs['track']['lineno']=59;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['sinkEvent']('onUserTryedToLogin');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_27_err){if (!$p['isinstance']($pyjs_dbg_27_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_27_err);}throw $pyjs_dbg_27_err;
}})();
				$pyjs['track']['lineno']=61;
				le = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof eval == "undefined"?$m['eval']:eval)('window.top.logError');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_28_err){if (!$p['isinstance']($pyjs_dbg_28_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_28_err);}throw $pyjs_dbg_28_err;
}})();
				$pyjs['track']['lineno']=62;
				w = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof eval == "undefined"?$m['eval']:eval)('window');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_29_err){if (!$p['isinstance']($pyjs_dbg_29_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_29_err);}throw $pyjs_dbg_29_err;
}})();
				$pyjs['track']['lineno']=63;
				w['__is_instance__'] && typeof w['__setattr__'] == 'function' ? w['__setattr__']('onerror', le) : $p['setattr'](w, 'onerror', le); 
				$pyjs['track']['lineno']=64;
				w = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof eval == "undefined"?$m['eval']:eval)('window.top');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_30_err){if (!$p['isinstance']($pyjs_dbg_30_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_30_err);}throw $pyjs_dbg_30_err;
}})();
				$pyjs['track']['lineno']=65;
				w['__is_instance__'] && typeof w['__setattr__'] == 'function' ? w['__setattr__']('onerror', le) : $p['setattr'](w, 'onerror', le); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=67;
			$method = $pyjs__bind_method2('onUserTryedToLogin', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'admin', 'lineno':67};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=67;
				$pyjs['track']['lineno']=68;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['userLoggedOutMsg']['testUserAvaiable']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_31_err){if (!$p['isinstance']($pyjs_dbg_31_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_31_err);}throw $pyjs_dbg_31_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onUserTryedToLogin'] = $method;
			$pyjs['track']['lineno']=70;
			$method = $pyjs__bind_method2('startup', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'admin', 'lineno':70};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=70;
				$pyjs['track']['lineno']=71;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'successHandler':$p['getattr'](self, 'onConfigAvailable'), 'failureHandler':$p['getattr'](self, 'onError'), 'cacheable':true}, null, '/admin/config']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_32_err){if (!$p['isinstance']($pyjs_dbg_32_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_32_err);}throw $pyjs_dbg_32_err;
}})();
				$pyjs['track']['lineno']=73;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'successHandler':$p['getattr'](self, 'onUserAvaiable'), 'failureHandler':$p['getattr']($p['getattr'](self, 'userLoggedOutMsg'), 'onUserTestFail'), 'cacheable':true}, 'user', 'view/self']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_33_err){if (!$p['isinstance']($pyjs_dbg_33_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_33_err);}throw $pyjs_dbg_33_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['startup'] = $method;
			$pyjs['track']['lineno']=76;
			$method = $pyjs__bind_method2('log', function(type, msg) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					type = arguments[1];
					msg = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'admin', 'lineno':76};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=76;
				$pyjs['track']['lineno']=77;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['logWdg']['log'](type, msg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_34_err){if (!$p['isinstance']($pyjs_dbg_34_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_34_err);}throw $pyjs_dbg_34_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['type'],['msg']]);
			$cls_definition['log'] = $method;
			$pyjs['track']['lineno']=79;
			$method = $pyjs__bind_method2('onConfigAvailable', function(req) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					req = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'admin', 'lineno':79};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=79;
				$pyjs['track']['lineno']=80;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('config', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['decode'](req);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})()) : $p['setattr'](self, 'config', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['decode'](req);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})()); 
				$pyjs['track']['lineno']=81;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['op_is']($p['getattr'](self, 'user'), null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_36_err){if (!$p['isinstance']($pyjs_dbg_36_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_36_err);}throw $pyjs_dbg_36_err;
}})()) {
					$pyjs['track']['lineno']=82;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['postInit']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_37_err){if (!$p['isinstance']($pyjs_dbg_37_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_37_err);}throw $pyjs_dbg_37_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['req']]);
			$cls_definition['onConfigAvailable'] = $method;
			$pyjs['track']['lineno']=84;
			$method = $pyjs__bind_method2('onUserAvaiable', function(req) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					req = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var data;
				$pyjs['track']={'module':'admin', 'lineno':84};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=84;
				$pyjs['track']['lineno']=85;
				data = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['decode'](req);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_38_err){if (!$p['isinstance']($pyjs_dbg_38_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_38_err);}throw $pyjs_dbg_38_err;
}})();
				$pyjs['track']['lineno']=86;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__setitem__']('currentUser', data['__getitem__']('values'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_39_err){if (!$p['isinstance']($pyjs_dbg_39_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_39_err);}throw $pyjs_dbg_39_err;
}})();
				$pyjs['track']['lineno']=87;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('user', data) : $p['setattr'](self, 'user', data); 
				$pyjs['track']['lineno']=88;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['op_is']($p['getattr'](self, 'config'), null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_40_err){if (!$p['isinstance']($pyjs_dbg_40_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_40_err);}throw $pyjs_dbg_40_err;
}})()) {
					$pyjs['track']['lineno']=89;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['postInit']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_41_err){if (!$p['isinstance']($pyjs_dbg_41_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_41_err);}throw $pyjs_dbg_41_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['req']]);
			$cls_definition['onUserAvaiable'] = $method;
			$pyjs['track']['lineno']=91;
			$method = $pyjs__bind_method2('postInit', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter5_nextval,$and8,predefinedFilterCounter,$lambda2,$lambda1,$iter7_idx,$iter6_type,$iter5_array,$iter3_array,module,$iter1_iter,$iter8_iter,$iter5_iter,pane,$iter5_type,$iter1_nextval,$iter6_iter,$iter8_array,$iter7_iter,$iter6_nextval,sortIndex,$iter3_idx,group,$or3,handlerCls,getModulSortIndex,handler,$iter1_array,$iter5_idx,$iter8_idx,userAccess,$iter7_type,getModulName,$iter3_iter,panes,$and9,$or4,$or1,$iter6_idx,$or2,$and1,$and2,$and3,$and4,$and5,$and6,$and7,$iter6_array,$iter8_nextval,groups,$iter3_type,$iter8_type,isChild,info,child,$iter1_type,$iter7_nextval,k,$iter7_array,v,$and10,$add2,p,$add1,$pyjs__trackstack_size_2,$iter1_idx,$pyjs__trackstack_size_1,$iter3_nextval,sorted_modules;
				$pyjs['track']={'module':'admin', 'lineno':91};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=91;
				$pyjs['track']['lineno']=92;
				getModulName = function(argIn) {
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
					var $pyjs_try_err;
					$pyjs['track']={'module':'admin','lineno':92};$pyjs['trackstack']['push']($pyjs['track']);
					$pyjs['track']['module']='admin';
					$pyjs['track']['lineno']=92;
					$pyjs['track']['lineno']=93;
					var $pyjs__trackstack_size_1 = $pyjs['trackstack']['length'];
					try {
						try {
							$pyjs['in_try_except'] += 1;
							$pyjs['track']['lineno']=94;
							$pyjs['track']['lineno']=94;
							var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
							return argIn['__getitem__']($constant_int_1)['__getitem__']('name')['lower']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_42_err){if (!$p['isinstance']($pyjs_dbg_42_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_42_err);}throw $pyjs_dbg_42_err;
}})();
							$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
							return $pyjs__ret;
						} finally { $pyjs['in_try_except'] -= 1; }
					} catch($pyjs_try_err) {
						$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_1 - 1);
						$pyjs['__active_exception_stack__'] = null;
						$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
						var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
						$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='admin';
						if (true) {
							$pyjs['track']['lineno']=96;
							$pyjs['track']['lineno']=96;
							var $pyjs__ret = null;
							$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
							return $pyjs__ret;
						}
					}
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return null;
				};
				getModulName['__name__'] = 'getModulName';

				getModulName['__bind_type__'] = 0;
				getModulName['__args__'] = [null,null,['argIn']];
				$pyjs['track']['lineno']=98;
				getModulSortIndex = function(argIn) {
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
					var $pyjs_try_err;
					$pyjs['track']={'module':'admin','lineno':98};$pyjs['trackstack']['push']($pyjs['track']);
					$pyjs['track']['module']='admin';
					$pyjs['track']['lineno']=98;
					$pyjs['track']['lineno']=99;
					var $pyjs__trackstack_size_1 = $pyjs['trackstack']['length'];
					try {
						try {
							$pyjs['in_try_except'] += 1;
							$pyjs['track']['lineno']=100;
							$pyjs['track']['lineno']=100;
							var $pyjs__ret = argIn['__getitem__']($constant_int_1)['__getitem__']('sortIndex');
							$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
							return $pyjs__ret;
						} finally { $pyjs['in_try_except'] -= 1; }
					} catch($pyjs_try_err) {
						$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_1 - 1);
						$pyjs['__active_exception_stack__'] = null;
						$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
						var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
						$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='admin';
						if (true) {
							$pyjs['track']['lineno']=102;
							$pyjs['track']['lineno']=102;
							var $pyjs__ret = null;
							$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
							return $pyjs__ret;
						}
					}
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return null;
				};
				getModulSortIndex['__name__'] = 'getModulSortIndex';

				getModulSortIndex['__bind_type__'] = 0;
				getModulSortIndex['__args__'] = [null,null,['argIn']];
				$pyjs['track']['lineno']=105;
				groups = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_43_err){if (!$p['isinstance']($pyjs_dbg_43_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_43_err);}throw $pyjs_dbg_43_err;
}})();
				$pyjs['track']['lineno']=106;
				panes = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_44_err){if (!$p['isinstance']($pyjs_dbg_44_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_44_err);}throw $pyjs_dbg_44_err;
}})();
				$pyjs['track']['lineno']=107;
				userAccess = ($p['bool']($or1=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'user')['__getitem__']('values')['get']('access');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_45_err){if (!$p['isinstance']($pyjs_dbg_45_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_45_err);}throw $pyjs_dbg_45_err;
}})())?$or1:(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_46_err){if (!$p['isinstance']($pyjs_dbg_46_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_46_err);}throw $pyjs_dbg_46_err;
}})());
				$pyjs['track']['lineno']=108;
				predefinedFilterCounter = $constant_int_1;
				$pyjs['track']['lineno']=110;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and1=(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['config']['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})()['__contains__']('configuration'))?($p['bool']($and2=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance']($p['getattr'](self, 'config')['__getitem__']('configuration'), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_48_err){if (!$p['isinstance']($pyjs_dbg_48_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_48_err);}throw $pyjs_dbg_48_err;
}})())?($p['bool']($and3=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'config')['__getitem__']('configuration')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_49_err){if (!$p['isinstance']($pyjs_dbg_49_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_49_err);}throw $pyjs_dbg_49_err;
}})()['__contains__']('modulGroups'))?(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance']($p['getattr'](self, 'config')['__getitem__']('configuration')['__getitem__']('modulGroups'), $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_50_err){if (!$p['isinstance']($pyjs_dbg_50_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_50_err);}throw $pyjs_dbg_50_err;
}})():$and3):$and2):$and1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_51_err){if (!$p['isinstance']($pyjs_dbg_51_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_51_err);}throw $pyjs_dbg_51_err;
}})()) {
					$pyjs['track']['lineno']=115;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter1_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'config')['__getitem__']('configuration')['__getitem__']('modulGroups');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_52_err){if (!$p['isinstance']($pyjs_dbg_52_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_52_err);}throw $pyjs_dbg_52_err;
}})();
					$iter1_nextval=$p['__iter_prepare']($iter1_iter,false);
					while (typeof($p['__wrapped_next']($iter1_nextval)['$nextval']) != 'undefined') {
						group = $iter1_nextval['$nextval'];
						$pyjs['track']['lineno']=116;
						p = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call(null, $m['GroupPane'], null, null, [{'iconURL':group['__getitem__']('icon')}, group['__getitem__']('name')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_53_err){if (!$p['isinstance']($pyjs_dbg_53_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_53_err);}throw $pyjs_dbg_53_err;
}})();
						$pyjs['track']['lineno']=118;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return groups['__setitem__'](group['__getitem__']('prefix'), p);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_54_err){if (!$p['isinstance']($pyjs_dbg_54_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_54_err);}throw $pyjs_dbg_54_err;
}})();
						$pyjs['track']['lineno']=119;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return group['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_55_err){if (!$p['isinstance']($pyjs_dbg_55_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_55_err);}throw $pyjs_dbg_55_err;
}})()['__contains__']('sortIndex'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_56_err){if (!$p['isinstance']($pyjs_dbg_56_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_56_err);}throw $pyjs_dbg_56_err;
}})()) {
							$pyjs['track']['lineno']=120;
							sortIndex = group['__getitem__']('sortIndex');
						}
						else {
							$pyjs['track']['lineno']=122;
							sortIndex = null;
						}
						$pyjs['track']['lineno']=124;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return panes['append']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['tuple']([group['__getitem__']('name'), sortIndex, p]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_57_err){if (!$p['isinstance']($pyjs_dbg_57_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_57_err);}throw $pyjs_dbg_57_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_58_err){if (!$p['isinstance']($pyjs_dbg_58_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_58_err);}throw $pyjs_dbg_58_err;
}})();
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='admin';
				}
				$pyjs['track']['lineno']=127;
				sorted_modules = function(){
					var $iter2_nextval,$iter2_type,$iter2_iter,$collcomp1,$iter2_idx,$pyjs__trackstack_size_1,y,x,$iter2_array;
	$collcomp1 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter2_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'config')['__getitem__']('modules')['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_60_err){if (!$p['isinstance']($pyjs_dbg_60_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_60_err);}throw $pyjs_dbg_60_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_61_err){if (!$p['isinstance']($pyjs_dbg_61_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_61_err);}throw $pyjs_dbg_61_err;
}})();
				$iter2_nextval=$p['__iter_prepare']($iter2_iter,false);
				while (typeof($p['__wrapped_next']($iter2_nextval)['$nextval']) != 'undefined') {
					var $tupleassign1 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter2_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_59_err){if (!$p['isinstance']($pyjs_dbg_59_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_59_err);}throw $pyjs_dbg_59_err;
}})();
					x = $tupleassign1[0];
					y = $tupleassign1[1];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp1['append']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['tuple']([x, y]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_62_err){if (!$p['isinstance']($pyjs_dbg_62_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_62_err);}throw $pyjs_dbg_62_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_63_err){if (!$p['isinstance']($pyjs_dbg_63_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_63_err);}throw $pyjs_dbg_63_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='admin';

	return $collcomp1;}();
				$pyjs['track']['lineno']=128;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(sorted_modules, 'sort', null, null, [{'key':getModulName}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})();
				$pyjs['track']['lineno']=129;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(sorted_modules, 'sort', null, null, [{'key':getModulSortIndex, 'reverse':true}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_65_err){if (!$p['isinstance']($pyjs_dbg_65_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_65_err);}throw $pyjs_dbg_65_err;
}})();
				$pyjs['track']['lineno']=131;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter3_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return sorted_modules;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})();
				$iter3_nextval=$p['__iter_prepare']($iter3_iter,false);
				while (typeof($p['__wrapped_next']($iter3_nextval)['$nextval']) != 'undefined') {
					var $tupleassign2 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter3_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_66_err){if (!$p['isinstance']($pyjs_dbg_66_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_66_err);}throw $pyjs_dbg_66_err;
}})();
					module = $tupleassign2[0];
					info = $tupleassign2[1];
					$pyjs['track']['lineno']=132;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and5=!$p['bool'](userAccess['__contains__']('root')))?!$p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['any'](function(){
						var $iter4_nextval,$collcomp2,$iter4_idx,$pyjs__trackstack_size_2,$iter4_type,$iter4_array,x,$iter4_iter;
	$collcomp2 = $p['list']();
					$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
					$iter4_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return userAccess;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_68_err){if (!$p['isinstance']($pyjs_dbg_68_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_68_err);}throw $pyjs_dbg_68_err;
}})();
					$iter4_nextval=$p['__iter_prepare']($iter4_iter,false);
					while (typeof($p['__wrapped_next']($iter4_nextval)['$nextval']) != 'undefined') {
						x = $iter4_nextval['$nextval'];
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $collcomp2['append']((function(){try{try{$pyjs['in_try_except'] += 1;
						return x['startswith'](module);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_69_err){if (!$p['isinstance']($pyjs_dbg_69_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_69_err);}throw $pyjs_dbg_69_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_70_err){if (!$p['isinstance']($pyjs_dbg_70_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_70_err);}throw $pyjs_dbg_70_err;
}})();
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='admin';

	return $collcomp2;}());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_71_err){if (!$p['isinstance']($pyjs_dbg_71_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_71_err);}throw $pyjs_dbg_71_err;
}})()):$and5));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_72_err){if (!$p['isinstance']($pyjs_dbg_72_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_72_err);}throw $pyjs_dbg_72_err;
}})()) {
						$pyjs['track']['lineno']=134;
						continue;
					}
					$pyjs['track']['lineno']=136;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['conf']['__getitem__']('modules')['__setitem__'](module, info);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_73_err){if (!$p['isinstance']($pyjs_dbg_73_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_73_err);}throw $pyjs_dbg_73_err;
}})();
					$pyjs['track']['lineno']=138;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and7=(function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['conf']['__getitem__']('modules')['__getitem__'](module)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_74_err){if (!$p['isinstance']($pyjs_dbg_74_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_74_err);}throw $pyjs_dbg_74_err;
}})()['__contains__']('views'))?$m['conf']['__getitem__']('modules')['__getitem__'](module)['__getitem__']('views'):$and7));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})()) {
						$pyjs['track']['lineno']=139;
						$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
						$iter5_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['conf']['__getitem__']('modules')['__getitem__'](module)['__getitem__']('views');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})();
						$iter5_nextval=$p['__iter_prepare']($iter5_iter,false);
						while (typeof($p['__wrapped_next']($iter5_nextval)['$nextval']) != 'undefined') {
							v = $iter5_nextval['$nextval'];
							$pyjs['track']['lineno']=140;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return v['__setitem__']('__id', predefinedFilterCounter);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_77_err){if (!$p['isinstance']($pyjs_dbg_77_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_77_err);}throw $pyjs_dbg_77_err;
}})();
							$pyjs['track']['lineno']=141;
							predefinedFilterCounter = $p['__op_add']($add1=predefinedFilterCounter,$add2=$constant_int_1);
						}
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='admin';
					}
					$pyjs['track']['lineno']=143;
					handlerCls = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['HandlerClassSelector']['select'](module, info);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_78_err){if (!$p['isinstance']($pyjs_dbg_78_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_78_err);}throw $pyjs_dbg_78_err;
}})();
					$pyjs['track']['lineno']=144;
					if (!( !$p['op_is'](handlerCls, null) )) {
					   throw $p['AssertionError']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('No handler available for modul %s', module);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_79_err){if (!$p['isinstance']($pyjs_dbg_79_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_79_err);}throw $pyjs_dbg_79_err;
}})());
					 }
					$pyjs['track']['lineno']=146;
					isChild = false;
					$pyjs['track']['lineno']=147;
					$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
					$iter6_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return groups['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_80_err){if (!$p['isinstance']($pyjs_dbg_80_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_80_err);}throw $pyjs_dbg_80_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_81_err){if (!$p['isinstance']($pyjs_dbg_81_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_81_err);}throw $pyjs_dbg_81_err;
}})();
					$iter6_nextval=$p['__iter_prepare']($iter6_iter,false);
					while (typeof($p['__wrapped_next']($iter6_nextval)['$nextval']) != 'undefined') {
						k = $iter6_nextval['$nextval'];
						$pyjs['track']['lineno']=148;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return info['__getitem__']('name')['startswith'](k);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_82_err){if (!$p['isinstance']($pyjs_dbg_82_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_82_err);}throw $pyjs_dbg_82_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_83_err){if (!$p['isinstance']($pyjs_dbg_83_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_83_err);}throw $pyjs_dbg_83_err;
}})()) {
							$pyjs['track']['lineno']=149;
							handler = (function(){try{try{$pyjs['in_try_except'] += 1;
							return $pyjs_kwargs_call(null, handlerCls, null, null, [{'groupName':k}, module, info]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_84_err){if (!$p['isinstance']($pyjs_dbg_84_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_84_err);}throw $pyjs_dbg_84_err;
}})();
							$pyjs['track']['lineno']=150;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return groups['__getitem__'](k)['addChildPane'](handler);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})();
							$pyjs['track']['lineno']=151;
							isChild = true;
							$pyjs['track']['lineno']=152;
							break;
						}
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='admin';
					$pyjs['track']['lineno']=154;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool'](isChild));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})()) {
						$pyjs['track']['lineno']=155;
						handler = (function(){try{try{$pyjs['in_try_except'] += 1;
						return handlerCls(module, info);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})();
						$pyjs['track']['lineno']=156;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return info['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})()['__contains__']('sortIndex'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_89_err){if (!$p['isinstance']($pyjs_dbg_89_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_89_err);}throw $pyjs_dbg_89_err;
}})()) {
							$pyjs['track']['lineno']=157;
							sortIndex = info['__getitem__']('sortIndex');
						}
						else {
							$pyjs['track']['lineno']=159;
							sortIndex = null;
						}
						$pyjs['track']['lineno']=160;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return panes['append']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['tuple']([info['__getitem__']('name'), sortIndex, handler]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_91_err){if (!$p['isinstance']($pyjs_dbg_91_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_91_err);}throw $pyjs_dbg_91_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=163;
				var 				$lambda1 = function(x) {
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);

					$pyjs['track']={'module':'admin','lineno':163};$pyjs['trackstack']['push']($pyjs['track']);
					$pyjs['track']['module']='admin';
					$pyjs['track']['lineno']=163;
					$pyjs['track']['lineno']=163;
					$pyjs['track']['lineno']=163;
					var $pyjs__ret = x['__getitem__']($constant_int_0);
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				};
				$lambda1['__name__'] = '$lambda1';

				$lambda1['__bind_type__'] = 0;
				$lambda1['__args__'] = [null,null,['x']];
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(panes, 'sort', null, null, [{'key':$lambda1}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_92_err){if (!$p['isinstance']($pyjs_dbg_92_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_92_err);}throw $pyjs_dbg_92_err;
}})();
				$pyjs['track']['lineno']=164;
				var 				$lambda2 = function(x) {
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);

					$pyjs['track']={'module':'admin','lineno':164};$pyjs['trackstack']['push']($pyjs['track']);
					$pyjs['track']['module']='admin';
					$pyjs['track']['lineno']=164;
					$pyjs['track']['lineno']=164;
					$pyjs['track']['lineno']=164;
					var $pyjs__ret = x['__getitem__']($constant_int_1);
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				};
				$lambda2['__name__'] = '$lambda2';

				$lambda2['__bind_type__'] = 0;
				$lambda2['__args__'] = [null,null,['x']];
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(panes, 'sort', null, null, [{'key':$lambda2, 'reverse':true}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_93_err){if (!$p['isinstance']($pyjs_dbg_93_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_93_err);}throw $pyjs_dbg_93_err;
}})();
				$pyjs['track']['lineno']=167;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter7_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return panes;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_95_err){if (!$p['isinstance']($pyjs_dbg_95_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_95_err);}throw $pyjs_dbg_95_err;
}})();
				$iter7_nextval=$p['__iter_prepare']($iter7_iter,false);
				while (typeof($p['__wrapped_next']($iter7_nextval)['$nextval']) != 'undefined') {
					var $tupleassign3 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter7_nextval['$nextval'], 3, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_94_err){if (!$p['isinstance']($pyjs_dbg_94_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_94_err);}throw $pyjs_dbg_94_err;
}})();
					k = $tupleassign3[0];
					v = $tupleassign3[1];
					pane = $tupleassign3[2];
					$pyjs['track']['lineno']=169;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and9=(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](pane, $m['GroupPane']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_96_err){if (!$p['isinstance']($pyjs_dbg_96_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_96_err);}throw $pyjs_dbg_96_err;
}})())?($p['bool']($or3=!$p['bool']($p['getattr'](pane, 'childPanes')))?$or3:(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['all'](function(){
var $generator_state = [0], $generator_exc = [null], $yield_value = null, $exc = null, $is_executing=false;
						var $generator = function () {};
						$generator['next'] = function (noStop) {
						
							var $res;
							$yield_value = $exc = null;
							try {
								$res = $generator['$genfunc']();
								$is_executing=false;
								if (typeof $res == 'undefined') {
									if (noStop === true) {
										$generator_state[0] = -1;
										return;
									}
									throw $p['StopIteration']();
								}
							} catch (e) {
						
								$is_executing=false;
								$generator_state[0] = -1;
								if (noStop === true && $p['isinstance'](e, $p['StopIteration'])) {
									return;
								}
								throw e;
							}
							return $res;
						};
						$generator['__iter__'] = function () {return $generator;};
						$generator['send'] = function ($val) {
						
							$yield_value = $val;
							$exc = null;
							try {
								var $res = $generator['$genfunc']();
								if (typeof $res == 'undefined') throw $p['StopIteration']();
							} catch (e) {
						
								$generator_state[0] = -1;
								$is_executing=false;
								throw e;
							}
							$is_executing=false;
							return $res;
						};
						$generator['$$throw'] = function ($exc_type, $exc_value) {
						
							$yield_value = null;
							$exc=(typeof $exc_value == 'undefined' ? $exc_type() :
																	($p['isinstance']($exc_value, $exc_type)
																	 ? $exc_value : $exc_type($exc_value)));
							try {
								var $res = $generator['$genfunc']();
							} catch (e) {
						
								$generator_state[0] = -1;
								$is_executing=false;
								throw (e);
							}
							$is_executing=false;
							return $res;
						};
						$generator['close'] = function () {
						
							$yield_value = null;
							$exc=$p['GeneratorExit']();
							try {
								var $res = $generator['$genfunc']();
								$is_executing=false;
								if (typeof $res != 'undefined') throw $p['RuntimeError']('generator ignored GeneratorExit');
							} catch (e) {
						
								$generator_state[0] = -1;
								$is_executing=false;
								if ($p['isinstance'](e, $p['StopIteration']) || $p['isinstance'](e, $p['GeneratorExit'])) return null;
								throw (e);
							}
							return null;
						};
						$generator['$genfunc'] = function () {
							var $yielding = false;
							if ($is_executing) throw $p['ValueError']('generator already executing');
							$is_executing = true;
						
						if (typeof $generator_state[0] == 'undefined' || $generator_state[0] === 0) {
							for (var $i = 0 ; $i < ($generator_state['length']<2?2:$generator_state['length']); $i++) $generator_state[$i]=0;
							if (typeof $exc != 'undefined' && $exc !== null) {
								$yielding = null;
								$generator_state[0] = -1;
								throw $exc;
							}
							$iter8_iter = $p['getattr'](pane, 'childPanes');
							$iter8_nextval=$p['__iter_prepare']($iter8_iter,false);
							$generator_state[0]=1;
						}
						if ($generator_state[0] == 1) {
							$generator_state[1] = 0;
							$generator_state[0]=2;
						}
						if ($generator_state[0] == 2) {
							for (;($generator_state[1] > 0 || typeof($p['__wrapped_next']($iter8_nextval)['$nextval']) != 'undefined');$generator_state[1] = 0) {
								if (typeof $generator_state[1] == 'undefined' || $generator_state[1] === 0) {
									for (var $i = 1 ; $i < ($generator_state['length']<3?3:$generator_state['length']); $i++) $generator_state[$i]=0;
									child = $iter8_nextval['$nextval'];
									$yield_value = $p['op_eq'](child['__getitem__']('style')['get']('display'), 'none');
									$yielding = true;
									$generator_state[1] = 1;
									return $yield_value;
									$generator_state[1]=1;
								}
								if ($generator_state[1] == 1) {
									if (typeof $exc != 'undefined' && $exc !== null) {
										$yielding = null;
										$generator_state[1] = -1;
										throw $exc;
									}
									$generator_state[1]=2;
								}
								if ($generator_state[1] == 2) {
								}
							}
							$generator_state[0]=3;
						}
						if ($generator_state[0] == 3) {
							$generator_state[0]=4;
						}
						if ($generator_state[0] == 4) {
						}

							return;
						};
						return $generator;
					}()
);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_97_err){if (!$p['isinstance']($pyjs_dbg_97_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_97_err);}throw $pyjs_dbg_97_err;
}})()):$and9));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_98_err){if (!$p['isinstance']($pyjs_dbg_98_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_98_err);}throw $pyjs_dbg_98_err;
}})()) {
						$pyjs['track']['lineno']=172;
						continue;
					}
					$pyjs['track']['lineno']=174;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['addPane'](pane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_99_err){if (!$p['isinstance']($pyjs_dbg_99_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_99_err);}throw $pyjs_dbg_99_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=177;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['viInitializedEvent']['fire']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_100_err){if (!$p['isinstance']($pyjs_dbg_100_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_100_err);}throw $pyjs_dbg_100_err;
}})();
				$pyjs['track']['lineno']=178;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['DeferredCall']($p['getattr'](self, 'checkInitialHash'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_101_err){if (!$p['isinstance']($pyjs_dbg_101_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_101_err);}throw $pyjs_dbg_101_err;
}})();
				$pyjs['track']['lineno']=179;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__getitem__']('class')['remove']('is_loading');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_102_err){if (!$p['isinstance']($pyjs_dbg_102_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_102_err);}throw $pyjs_dbg_102_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['postInit'] = $method;
			$pyjs['track']['lineno']=181;
			$method = $pyjs__bind_method2('checkInitialHash', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var hashList,$iter10_nextval,$pyjs__trackstack_size_1,hashStr,value,$iter10_iter,params,paramsStr,$and12,urlHash,$and11,key,pair,gen,$iter10_array,$add3,$add6,$add4,$add5,$iter10_type,$iter10_idx;
				$pyjs['track']={'module':'admin', 'lineno':181};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=181;
				$pyjs['track']['lineno']=182;
				urlHash = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof eval == "undefined"?$m['eval']:eval)('window.top.location.hash');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_103_err){if (!$p['isinstance']($pyjs_dbg_103_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_103_err);}throw $pyjs_dbg_103_err;
}})();
				$pyjs['track']['lineno']=183;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool'](urlHash));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_104_err){if (!$p['isinstance']($pyjs_dbg_104_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_104_err);}throw $pyjs_dbg_104_err;
}})()) {
					$pyjs['track']['lineno']=184;
					$pyjs['track']['lineno']=184;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=185;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](urlHash['__contains__']('?'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_105_err){if (!$p['isinstance']($pyjs_dbg_105_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_105_err);}throw $pyjs_dbg_105_err;
}})()) {
					$pyjs['track']['lineno']=186;
					hashStr = $p['__getslice'](urlHash, 0, (function(){try{try{$pyjs['in_try_except'] += 1;
					return urlHash['find']('?');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_106_err){if (!$p['isinstance']($pyjs_dbg_106_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_106_err);}throw $pyjs_dbg_106_err;
}})());
					$pyjs['track']['lineno']=187;
					paramsStr = $p['__getslice'](urlHash, $p['__op_add']($add3=(function(){try{try{$pyjs['in_try_except'] += 1;
					return urlHash['find']('?');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_107_err){if (!$p['isinstance']($pyjs_dbg_107_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_107_err);}throw $pyjs_dbg_107_err;
}})(),$add4=$constant_int_1), null);
				}
				else {
					$pyjs['track']['lineno']=189;
					hashStr = urlHash;
					$pyjs['track']['lineno']=190;
					paramsStr = '';
				}
				$pyjs['track']['lineno']=191;
				hashList = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__getslice'](hashStr, $constant_int_1, null)['$$split']('/');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_108_err){if (!$p['isinstance']($pyjs_dbg_108_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_108_err);}throw $pyjs_dbg_108_err;
}})();
				$pyjs['track']['lineno']=192;
				hashList = function(){
					var $iter9_iter,$collcomp3,$iter9_nextval,$iter9_idx,$iter9_type,$pyjs__trackstack_size_1,x,$iter9_array;
	$collcomp3 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter9_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return hashList;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_109_err){if (!$p['isinstance']($pyjs_dbg_109_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_109_err);}throw $pyjs_dbg_109_err;
}})();
				$iter9_nextval=$p['__iter_prepare']($iter9_iter,false);
				while (typeof($p['__wrapped_next']($iter9_nextval)['$nextval']) != 'undefined') {
					x = $iter9_nextval['$nextval'];
					$pyjs['track']['lineno']=192;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](x);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_110_err){if (!$p['isinstance']($pyjs_dbg_110_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_110_err);}throw $pyjs_dbg_110_err;
}})()) {
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $collcomp3['append'](x);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_111_err){if (!$p['isinstance']($pyjs_dbg_111_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_111_err);}throw $pyjs_dbg_111_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='admin';

	return $collcomp3;}();
				$pyjs['track']['lineno']=193;
				params = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_112_err){if (!$p['isinstance']($pyjs_dbg_112_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_112_err);}throw $pyjs_dbg_112_err;
}})();
				$pyjs['track']['lineno']=194;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](paramsStr);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_113_err){if (!$p['isinstance']($pyjs_dbg_113_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_113_err);}throw $pyjs_dbg_113_err;
}})()) {
					$pyjs['track']['lineno']=195;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter10_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return paramsStr['$$split']('&');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_114_err){if (!$p['isinstance']($pyjs_dbg_114_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_114_err);}throw $pyjs_dbg_114_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_115_err){if (!$p['isinstance']($pyjs_dbg_115_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_115_err);}throw $pyjs_dbg_115_err;
}})();
					$iter10_nextval=$p['__iter_prepare']($iter10_iter,false);
					while (typeof($p['__wrapped_next']($iter10_nextval)['$nextval']) != 'undefined') {
						pair = $iter10_nextval['$nextval'];
						$pyjs['track']['lineno']=196;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['bool'](pair['__contains__']('=')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_116_err){if (!$p['isinstance']($pyjs_dbg_116_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_116_err);}throw $pyjs_dbg_116_err;
}})()) {
							$pyjs['track']['lineno']=197;
							continue;
						}
						$pyjs['track']['lineno']=198;
						key = $p['__getslice'](pair, 0, (function(){try{try{$pyjs['in_try_except'] += 1;
						return pair['find']('=');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_117_err){if (!$p['isinstance']($pyjs_dbg_117_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_117_err);}throw $pyjs_dbg_117_err;
}})());
						$pyjs['track']['lineno']=199;
						value = $p['__getslice'](pair, $p['__op_add']($add5=(function(){try{try{$pyjs['in_try_except'] += 1;
						return pair['find']('=');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_118_err){if (!$p['isinstance']($pyjs_dbg_118_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_118_err);}throw $pyjs_dbg_118_err;
}})(),$add6=$constant_int_1), null);
						$pyjs['track']['lineno']=200;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['bool'](($p['bool']($and11=key)?value:$and11)));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_119_err){if (!$p['isinstance']($pyjs_dbg_119_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_119_err);}throw $pyjs_dbg_119_err;
}})()) {
							$pyjs['track']['lineno']=201;
							continue;
						}
						$pyjs['track']['lineno']=202;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return params['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_120_err){if (!$p['isinstance']($pyjs_dbg_120_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_120_err);}throw $pyjs_dbg_120_err;
}})()['__contains__'](key));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_121_err){if (!$p['isinstance']($pyjs_dbg_121_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_121_err);}throw $pyjs_dbg_121_err;
}})()) {
							$pyjs['track']['lineno']=203;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool'](!$p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['isinstance'](params['__getitem__'](key), $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_122_err){if (!$p['isinstance']($pyjs_dbg_122_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_122_err);}throw $pyjs_dbg_122_err;
}})()));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_123_err){if (!$p['isinstance']($pyjs_dbg_123_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_123_err);}throw $pyjs_dbg_123_err;
}})()) {
								$pyjs['track']['lineno']=204;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return params['__setitem__'](key, (function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['list']([params['__getitem__'](key)]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_124_err){if (!$p['isinstance']($pyjs_dbg_124_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_124_err);}throw $pyjs_dbg_124_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_125_err){if (!$p['isinstance']($pyjs_dbg_125_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_125_err);}throw $pyjs_dbg_125_err;
}})();
							}
							$pyjs['track']['lineno']=205;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return params['__getitem__'](key)['append'](value);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_126_err){if (!$p['isinstance']($pyjs_dbg_126_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_126_err);}throw $pyjs_dbg_126_err;
}})();
						}
						else {
							$pyjs['track']['lineno']=207;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return params['__setitem__'](key, value);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_127_err){if (!$p['isinstance']($pyjs_dbg_127_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_127_err);}throw $pyjs_dbg_127_err;
}})();
						}
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='admin';
				}
				$pyjs['track']['lineno']=208;
				gen = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['initialHashHandler']['select'](hashList, params);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_128_err){if (!$p['isinstance']($pyjs_dbg_128_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_128_err);}throw $pyjs_dbg_128_err;
}})();
				$pyjs['track']['lineno']=209;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](gen);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_129_err){if (!$p['isinstance']($pyjs_dbg_129_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_129_err);}throw $pyjs_dbg_129_err;
}})()) {
					$pyjs['track']['lineno']=210;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return gen(hashList, params);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_130_err){if (!$p['isinstance']($pyjs_dbg_130_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_130_err);}throw $pyjs_dbg_130_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['checkInitialHash'] = $method;
			$pyjs['track']['lineno']=212;
			$method = $pyjs__bind_method2('onError', function(req, code) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					req = arguments[1];
					code = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'admin', 'lineno':212};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=212;
				$pyjs['track']['lineno']=213;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['printFunc'](['ONERROR'], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_131_err){if (!$p['isinstance']($pyjs_dbg_131_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_131_err);}throw $pyjs_dbg_131_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['req'],['code']]);
			$cls_definition['onError'] = $method;
			$pyjs['track']['lineno']=215;
			$method = $pyjs__bind_method2('_registerChildPanes', function(pane) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					pane = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter11_iter,$iter11_type,$iter11_array,$iter11_nextval,$iter11_idx,$pyjs__trackstack_size_1,childPane;
				$pyjs['track']={'module':'admin', 'lineno':215};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=215;
				$pyjs['track']['lineno']=216;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter11_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](pane, 'childPanes');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_132_err){if (!$p['isinstance']($pyjs_dbg_132_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_132_err);}throw $pyjs_dbg_132_err;
}})();
				$iter11_nextval=$p['__iter_prepare']($iter11_iter,false);
				while (typeof($p['__wrapped_next']($iter11_nextval)['$nextval']) != 'undefined') {
					childPane = $iter11_nextval['$nextval'];
					$pyjs['track']['lineno']=217;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['panes']['append'](childPane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_133_err){if (!$p['isinstance']($pyjs_dbg_133_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_133_err);}throw $pyjs_dbg_133_err;
}})();
					$pyjs['track']['lineno']=218;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['viewport']['appendChild']($p['getattr'](childPane, 'widgetsDomElm'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_134_err){if (!$p['isinstance']($pyjs_dbg_134_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_134_err);}throw $pyjs_dbg_134_err;
}})();
					$pyjs['track']['lineno']=219;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](childPane, 'widgetsDomElm')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_135_err){if (!$p['isinstance']($pyjs_dbg_135_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_135_err);}throw $pyjs_dbg_135_err;
}})();
					$pyjs['track']['lineno']=220;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['_registerChildPanes'](childPane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_136_err){if (!$p['isinstance']($pyjs_dbg_136_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_136_err);}throw $pyjs_dbg_136_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='admin';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['pane']]);
			$cls_definition['_registerChildPanes'] = $method;
			$pyjs['track']['lineno']=222;
			$method = $pyjs__bind_method2('addPane', function(pane, parentPane) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					pane = arguments[1];
					parentPane = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 2 || arguments['length'] > 3)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof parentPane == 'undefined') parentPane=arguments['callee']['__args__'][4][1];

				$pyjs['track']={'module':'admin', 'lineno':222};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=222;
				$pyjs['track']['lineno']=225;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len']($p['getattr'](pane, 'childPanes'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_137_err){if (!$p['isinstance']($pyjs_dbg_137_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_137_err);}throw $pyjs_dbg_137_err;
}})(), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_138_err){if (!$p['isinstance']($pyjs_dbg_138_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_138_err);}throw $pyjs_dbg_138_err;
}})()) {
					$pyjs['track']['lineno']=226;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['_registerChildPanes'](pane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_139_err){if (!$p['isinstance']($pyjs_dbg_139_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_139_err);}throw $pyjs_dbg_139_err;
}})();
				}
				$pyjs['track']['lineno']=227;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['panes']['append'](pane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_140_err){if (!$p['isinstance']($pyjs_dbg_140_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_140_err);}throw $pyjs_dbg_140_err;
}})();
				$pyjs['track']['lineno']=228;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](parentPane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_141_err){if (!$p['isinstance']($pyjs_dbg_141_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_141_err);}throw $pyjs_dbg_141_err;
}})()) {
					$pyjs['track']['lineno']=229;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return parentPane['addChildPane'](pane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_142_err){if (!$p['isinstance']($pyjs_dbg_142_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_142_err);}throw $pyjs_dbg_142_err;
}})();
					$pyjs['track']['lineno']=230;
					pane['__is_instance__'] && typeof pane['__setattr__'] == 'function' ? pane['__setattr__']('parent', parentPane) : $p['setattr'](pane, 'parent', parentPane); 
				}
				else {
					$pyjs['track']['lineno']=232;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['modulListUl']['appendChild'](pane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_143_err){if (!$p['isinstance']($pyjs_dbg_143_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_143_err);}throw $pyjs_dbg_143_err;
}})();
				}
				$pyjs['track']['lineno']=233;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['viewport']['appendChild']($p['getattr'](pane, 'widgetsDomElm'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_144_err){if (!$p['isinstance']($pyjs_dbg_144_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_144_err);}throw $pyjs_dbg_144_err;
}})();
				$pyjs['track']['lineno']=234;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](pane, 'widgetsDomElm')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_145_err){if (!$p['isinstance']($pyjs_dbg_145_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_145_err);}throw $pyjs_dbg_145_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['pane'],['parentPane', null]]);
			$cls_definition['addPane'] = $method;
			$pyjs['track']['lineno']=237;
			$method = $pyjs__bind_method2('stackPane', function(pane, focus) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					pane = arguments[1];
					focus = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 2 || arguments['length'] > 3)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof focus == 'undefined') focus=arguments['callee']['__args__'][4][1];
				var $and14,$and13;
				$pyjs['track']={'module':'admin', 'lineno':237};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=237;
				$pyjs['track']['lineno']=238;
				if (!( !$p['op_is']($p['getattr'](self, 'currentPane'), null) )) {
				   throw $p['AssertionError']("Cannot stack a pane. There's no current one.");
				 }
				$pyjs['track']['lineno']=239;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(self, 'addPane', null, null, [{'parentPane':$p['getattr'](self, 'currentPane')}, pane]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_146_err){if (!$p['isinstance']($pyjs_dbg_146_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_146_err);}throw $pyjs_dbg_146_err;
}})();
				$pyjs['track']['lineno']=240;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and13=focus)?!$p['bool']($p['getattr'](self, 'nextPane')):$and13));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_147_err){if (!$p['isinstance']($pyjs_dbg_147_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_147_err);}throw $pyjs_dbg_147_err;
}})()) {
					$pyjs['track']['lineno']=244;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('nextPane', pane) : $p['setattr'](self, 'nextPane', pane); 
					$pyjs['track']['lineno']=245;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['DeferredCall']($p['getattr'](self, 'focusNextPane'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_148_err){if (!$p['isinstance']($pyjs_dbg_148_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_148_err);}throw $pyjs_dbg_148_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['pane'],['focus', false]]);
			$cls_definition['stackPane'] = $method;
			$pyjs['track']['lineno']=247;
			$method = $pyjs__bind_method2('focusNextPane', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var nextPane;
				$pyjs['track']={'module':'admin', 'lineno':247};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=247;
				$pyjs['track']['lineno']=251;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, 'nextPane')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_149_err){if (!$p['isinstance']($pyjs_dbg_149_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_149_err);}throw $pyjs_dbg_149_err;
}})()) {
					$pyjs['track']['lineno']=252;
					$pyjs['track']['lineno']=252;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=253;
				nextPane = $p['getattr'](self, 'nextPane');
				$pyjs['track']['lineno']=254;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('nextPane', null) : $p['setattr'](self, 'nextPane', null); 
				$pyjs['track']['lineno']=255;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['focusPane'](nextPane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_150_err){if (!$p['isinstance']($pyjs_dbg_150_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_150_err);}throw $pyjs_dbg_150_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['focusNextPane'] = $method;
			$pyjs['track']['lineno']=257;
			$method = $pyjs__bind_method2('focusPane', function(pane) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					pane = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and16,$and17,$and15,$and18;
				$pyjs['track']={'module':'admin', 'lineno':257};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=257;
				$pyjs['track']['lineno']=258;
				if (!( $p['getattr'](self, 'panes')['__contains__'](pane) )) {
				   throw $p['AssertionError']('Cannot focus unknown pane!');
				 }
				$pyjs['track']['lineno']=262;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq'](pane, $p['getattr'](self, 'currentPane')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_151_err){if (!$p['isinstance']($pyjs_dbg_151_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_151_err);}throw $pyjs_dbg_151_err;
}})()) {
					$pyjs['track']['lineno']=263;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and15=$p['getattr']($p['getattr'](self, 'currentPane'), 'collapseable'))?$p['getattr']($p['getattr'](self, 'currentPane'), 'childDomElem'):$and15));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_152_err){if (!$p['isinstance']($pyjs_dbg_152_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_152_err);}throw $pyjs_dbg_152_err;
}})()) {
						$pyjs['track']['lineno']=264;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']($p['op_eq']($p['getattr']($p['getattr'](self, 'currentPane'), 'childDomElem')['__getitem__']('style')['__getitem__']('display'), 'none'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_153_err){if (!$p['isinstance']($pyjs_dbg_153_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_153_err);}throw $pyjs_dbg_153_err;
}})()) {
							$pyjs['track']['lineno']=265;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['getattr']($p['getattr'](self, 'currentPane'), 'childDomElem')['__getitem__']('style')['__setitem__']('display', 'block');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_154_err){if (!$p['isinstance']($pyjs_dbg_154_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_154_err);}throw $pyjs_dbg_154_err;
}})();
						}
						else {
							$pyjs['track']['lineno']=267;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['getattr']($p['getattr'](self, 'currentPane'), 'childDomElem')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_155_err){if (!$p['isinstance']($pyjs_dbg_155_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_155_err);}throw $pyjs_dbg_155_err;
}})();
						}
					}
					$pyjs['track']['lineno']=268;
					$pyjs['track']['lineno']=268;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=270;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['panes']['remove'](pane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_156_err){if (!$p['isinstance']($pyjs_dbg_156_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_156_err);}throw $pyjs_dbg_156_err;
}})();
				$pyjs['track']['lineno']=271;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['panes']['append'](pane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_157_err){if (!$p['isinstance']($pyjs_dbg_157_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_157_err);}throw $pyjs_dbg_157_err;
}})();
				$pyjs['track']['lineno']=274;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['op_is']($p['getattr'](self, 'currentPane'), null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_158_err){if (!$p['isinstance']($pyjs_dbg_158_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_158_err);}throw $pyjs_dbg_158_err;
}})()) {
					$pyjs['track']['lineno']=275;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'currentPane')['__getitem__']('class')['remove']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_159_err){if (!$p['isinstance']($pyjs_dbg_159_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_159_err);}throw $pyjs_dbg_159_err;
}})();
					$pyjs['track']['lineno']=276;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr']($p['getattr'](self, 'currentPane'), 'widgetsDomElm')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_160_err){if (!$p['isinstance']($pyjs_dbg_160_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_160_err);}throw $pyjs_dbg_160_err;
}})();
				}
				$pyjs['track']['lineno']=279;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['topBar']['setCurrentModulDescr']($p['getattr'](pane, 'descr'), $p['getattr'](pane, 'iconURL'), $p['getattr'](pane, 'iconClasses'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_161_err){if (!$p['isinstance']($pyjs_dbg_161_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_161_err);}throw $pyjs_dbg_161_err;
}})();
				$pyjs['track']['lineno']=280;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentPane', pane) : $p['setattr'](self, 'currentPane', pane); 
				$pyjs['track']['lineno']=281;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr']($p['getattr'](self, 'currentPane'), 'widgetsDomElm')['__getitem__']('style')['__setitem__']('display', 'block');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_162_err){if (!$p['isinstance']($pyjs_dbg_162_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_162_err);}throw $pyjs_dbg_162_err;
}})();
				$pyjs['track']['lineno']=283;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and17=$p['getattr']($p['getattr'](self, 'currentPane'), 'collapseable'))?$p['getattr']($p['getattr'](self, 'currentPane'), 'childDomElem'):$and17));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_163_err){if (!$p['isinstance']($pyjs_dbg_163_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_163_err);}throw $pyjs_dbg_163_err;
}})()) {
					$pyjs['track']['lineno']=284;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr']($p['getattr'](self, 'currentPane'), 'childDomElem')['__getitem__']('style')['__setitem__']('display', 'block');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_164_err){if (!$p['isinstance']($pyjs_dbg_164_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_164_err);}throw $pyjs_dbg_164_err;
}})();
				}
				$pyjs['track']['lineno']=286;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'currentPane')['__getitem__']('class')['append']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_165_err){if (!$p['isinstance']($pyjs_dbg_165_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_165_err);}throw $pyjs_dbg_165_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['pane']]);
			$cls_definition['focusPane'] = $method;
			$pyjs['track']['lineno']=288;
			$method = $pyjs__bind_method2('removePane', function(pane) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					pane = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'admin', 'lineno':288};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=288;
				$pyjs['track']['lineno']=289;
				if (!( $p['getattr'](self, 'panes')['__contains__'](pane) )) {
				   throw $p['AssertionError']('Cannot remove unknown pane!');
				 }
				$pyjs['track']['lineno']=290;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['panes']['remove'](pane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_166_err){if (!$p['isinstance']($pyjs_dbg_166_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_166_err);}throw $pyjs_dbg_166_err;
}})();
				$pyjs['track']['lineno']=291;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq'](pane, $p['getattr'](self, 'currentPane')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_167_err){if (!$p['isinstance']($pyjs_dbg_167_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_167_err);}throw $pyjs_dbg_167_err;
}})()) {
					$pyjs['track']['lineno']=292;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'panes'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_168_err){if (!$p['isinstance']($pyjs_dbg_168_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_168_err);}throw $pyjs_dbg_168_err;
}})()) {
						$pyjs['track']['lineno']=293;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['focusPane']($p['getattr'](self, 'panes')['__getitem__']((typeof ($usub1=$constant_int_1)=='number'?
							-$usub1:
							$p['op_usub']($usub1))));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_169_err){if (!$p['isinstance']($pyjs_dbg_169_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_169_err);}throw $pyjs_dbg_169_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=295;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentPane', null) : $p['setattr'](self, 'currentPane', null); 
					}
				}
				$pyjs['track']['lineno']=296;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](pane, 'parent'), self));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_170_err){if (!$p['isinstance']($pyjs_dbg_170_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_170_err);}throw $pyjs_dbg_170_err;
}})()) {
					$pyjs['track']['lineno']=297;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['modulListUl']['removeChild'](pane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_171_err){if (!$p['isinstance']($pyjs_dbg_171_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_171_err);}throw $pyjs_dbg_171_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=299;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return pane['parent']['removeChildPane'](pane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_172_err){if (!$p['isinstance']($pyjs_dbg_172_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_172_err);}throw $pyjs_dbg_172_err;
}})();
				}
				$pyjs['track']['lineno']=300;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['viewport']['removeChild']($p['getattr'](pane, 'widgetsDomElm'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_173_err){if (!$p['isinstance']($pyjs_dbg_173_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_173_err);}throw $pyjs_dbg_173_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['pane']]);
			$cls_definition['removePane'] = $method;
			$pyjs['track']['lineno']=302;
			$method = $pyjs__bind_method2('addWidget', function(widget, pane) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					widget = arguments[1];
					pane = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'admin', 'lineno':302};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=302;
				$pyjs['track']['lineno']=303;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return pane['addWidget'](widget);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_174_err){if (!$p['isinstance']($pyjs_dbg_174_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_174_err);}throw $pyjs_dbg_174_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['widget'],['pane']]);
			$cls_definition['addWidget'] = $method;
			$pyjs['track']['lineno']=305;
			$method = $pyjs__bind_method2('stackWidget', function(widget) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					widget = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'admin', 'lineno':305};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=305;
				$pyjs['track']['lineno']=306;
				if (!( !$p['op_is']($p['getattr'](self, 'currentPane'), null) )) {
				   throw $p['AssertionError']('Cannot stack a widget while no pane is active');
				 }
				$pyjs['track']['lineno']=307;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['addWidget'](widget, $p['getattr'](self, 'currentPane'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_175_err){if (!$p['isinstance']($pyjs_dbg_175_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_175_err);}throw $pyjs_dbg_175_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['widget']]);
			$cls_definition['stackWidget'] = $method;
			$pyjs['track']['lineno']=309;
			$method = $pyjs__bind_method2('removeWidget', function(widget) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					widget = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e15094da3383fa9fb30fd5c3924d8e57') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter12_type,$iter12_array,$iter12_nextval,$iter12_iter,pane,$pyjs__trackstack_size_1,$iter12_idx;
				$pyjs['track']={'module':'admin', 'lineno':309};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=309;
				$pyjs['track']['lineno']=310;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter12_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'panes');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_176_err){if (!$p['isinstance']($pyjs_dbg_176_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_176_err);}throw $pyjs_dbg_176_err;
}})();
				$iter12_nextval=$p['__iter_prepare']($iter12_iter,false);
				while (typeof($p['__wrapped_next']($iter12_nextval)['$nextval']) != 'undefined') {
					pane = $iter12_nextval['$nextval'];
					$pyjs['track']['lineno']=311;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return pane['containsWidget'](widget);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_177_err){if (!$p['isinstance']($pyjs_dbg_177_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_177_err);}throw $pyjs_dbg_177_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_178_err){if (!$p['isinstance']($pyjs_dbg_178_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_178_err);}throw $pyjs_dbg_178_err;
}})()) {
						$pyjs['track']['lineno']=312;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return pane['removeWidget'](widget);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_179_err){if (!$p['isinstance']($pyjs_dbg_179_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_179_err);}throw $pyjs_dbg_179_err;
}})();
						$pyjs['track']['lineno']=313;
						$pyjs['track']['lineno']=313;
						var $pyjs__ret = null;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='admin';
				$pyjs['track']['lineno']=314;
				$pyjs['__active_exception_stack__'] = null;
				throw ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['AssertionError']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('Tried to remove unknown widget %s', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['str'](widget);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_180_err){if (!$p['isinstance']($pyjs_dbg_180_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_180_err);}throw $pyjs_dbg_180_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_181_err){if (!$p['isinstance']($pyjs_dbg_181_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_181_err);}throw $pyjs_dbg_181_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_182_err){if (!$p['isinstance']($pyjs_dbg_182_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_182_err);}throw $pyjs_dbg_182_err;
}})());
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['widget']]);
			$cls_definition['removeWidget'] = $method;
			$pyjs['track']['lineno']=23;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('CoreWindow', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=316;
		if ((function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['bool']($p['op_eq']((typeof __name__ == "undefined"?$m['__name__']:__name__), '__main__'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_183_err){if (!$p['isinstance']($pyjs_dbg_183_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_183_err);}throw $pyjs_dbg_183_err;
}})()) {
			$pyjs['track']['lineno']=317;
			(function(){try{try{$pyjs['in_try_except'] += 1;
			return $m['pyjd']['setup']('public/admin.html');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_184_err){if (!$p['isinstance']($pyjs_dbg_184_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_184_err);}throw $pyjs_dbg_184_err;
}})();
			$pyjs['track']['lineno']=318;
			(function(){try{try{$pyjs['in_try_except'] += 1;
			return $m['conf']['__setitem__']('mainWindow', (function(){try{try{$pyjs['in_try_except'] += 1;
			return $m['CoreWindow']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_185_err){if (!$p['isinstance']($pyjs_dbg_185_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_185_err);}throw $pyjs_dbg_185_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_186_err){if (!$p['isinstance']($pyjs_dbg_186_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_186_err);}throw $pyjs_dbg_186_err;
}})();
			$pyjs['track']['lineno']=319;
			(function(){try{try{$pyjs['in_try_except'] += 1;
			return (function(){try{try{$pyjs['in_try_except'] += 1;
			return $m['html5']['Body']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_187_err){if (!$p['isinstance']($pyjs_dbg_187_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_187_err);}throw $pyjs_dbg_187_err;
}})()['appendChild']($m['conf']['__getitem__']('mainWindow'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_188_err){if (!$p['isinstance']($pyjs_dbg_188_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_188_err);}throw $pyjs_dbg_188_err;
}})();
			$pyjs['track']['lineno']=323;
			(function(){try{try{$pyjs['in_try_except'] += 1;
			return $m['startupQueue']['run']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_189_err){if (!$p['isinstance']($pyjs_dbg_189_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_189_err);}throw $pyjs_dbg_189_err;
}})();
			$pyjs['track']['lineno']=324;
			(function(){try{try{$pyjs['in_try_except'] += 1;
			return $m['pyjd']['run']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_190_err){if (!$p['isinstance']($pyjs_dbg_190_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_190_err);}throw $pyjs_dbg_190_err;
}})();
		}
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end admin */


/* end module: admin */


/*
PYJS_DEPS: ['pyjd', 'html5', 'config.conf', 'config', 'widgets.TopBarWidget', 'widgets', 'widgets.userlogoutmsg.UserLogoutMsg', 'widgets.userlogoutmsg', 'json', 'network.NetworkService', 'network', 'network.DeferredCall', 'handler', 'bones', 'actions', 'i18n', 'event.viInitializedEvent', 'event', 'priorityqueue.HandlerClassSelector', 'priorityqueue', 'priorityqueue.initialHashHandler', 'priorityqueue.startupQueue', 'log.Log', 'log', 'pane.Pane', 'pane', 'pane.GroupPane', 'i18n.translate', 'vi_plugins']
*/
