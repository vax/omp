/* start module: actions.list */
$pyjs['loaded_modules']['actions.list'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['actions.list']['__was_initialized__']) return $pyjs['loaded_modules']['actions.list'];
	if(typeof $pyjs['loaded_modules']['actions'] == 'undefined' || !$pyjs['loaded_modules']['actions']['__was_initialized__']) $p['___import___']('actions', null);
	var $m = $pyjs['loaded_modules']['actions.list'];
	$m['__repr__'] = function() { return '<module: actions.list>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'actions.list';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	$pyjs['loaded_modules']['actions']['list'] = $pyjs['loaded_modules']['actions.list'];
	try {
		$m.__track_lines__[1] = 'actions.list.py, line 1:\n    import html5, utils';
		$m.__track_lines__[2] = 'actions.list.py, line 2:\n    from network import NetworkService';
		$m.__track_lines__[3] = 'actions.list.py, line 3:\n    from priorityqueue import actionDelegateSelector';
		$m.__track_lines__[4] = 'actions.list.py, line 4:\n    from widgets.edit import EditWidget';
		$m.__track_lines__[5] = 'actions.list.py, line 5:\n    from config import conf';
		$m.__track_lines__[6] = 'actions.list.py, line 6:\n    from pane import Pane';
		$m.__track_lines__[7] = 'actions.list.py, line 7:\n    from widgets.repeatdate import RepeatDatePopup';
		$m.__track_lines__[8] = 'actions.list.py, line 8:\n    from widgets.csvexport import CsvExport';
		$m.__track_lines__[9] = 'actions.list.py, line 9:\n    from widgets.table import DataTable';
		$m.__track_lines__[10] = 'actions.list.py, line 10:\n    from widgets.preview import Preview';
		$m.__track_lines__[11] = 'actions.list.py, line 11:\n    from sidebarwidgets.internalpreview import InternalPreview';
		$m.__track_lines__[12] = 'actions.list.py, line 12:\n    from sidebarwidgets.filterselector import FilterSelector';
		$m.__track_lines__[13] = 'actions.list.py, line 13:\n    from i18n import translate';
		$m.__track_lines__[15] = 'actions.list.py, line 15:\n    class EditPane( Pane ):';
		$m.__track_lines__[16] = 'actions.list.py, line 16:\n    pass';
		$m.__track_lines__[18] = 'actions.list.py, line 18:\n    """';
		$m.__track_lines__[21] = 'actions.list.py, line 21:\n    class AddAction( html5.ext.Button ):';
		$m.__track_lines__[25] = 'actions.list.py, line 25:\n    def __init__(self, *args, **kwargs):';
		$m.__track_lines__[26] = 'actions.list.py, line 26:\n    super( AddAction, self ).__init__(translate("Add"), *args, **kwargs )';
		$m.__track_lines__[27] = 'actions.list.py, line 27:\n    self["class"] = "icon add list"';
		$m.__track_lines__[30] = 'actions.list.py, line 29:\n    @staticmethod ... def isSuitableFor( modul, handler, actionName ):';
		$m.__track_lines__[31] = 'actions.list.py, line 31:\n    if modul is None:';
		$m.__track_lines__[32] = 'actions.list.py, line 32:\n    return( False )';
		$m.__track_lines__[33] = 'actions.list.py, line 33:\n    correctAction = actionName=="add"';
		$m.__track_lines__[34] = 'actions.list.py, line 34:\n    correctHandler = handler == "list" or handler.startswith("list.")';
		$m.__track_lines__[35] = 'actions.list.py, line 35:\n    hasAccess = conf["currentUser"] and ("root" in conf["currentUser"]["access"] or modul+"-add" in conf["currentUser"]["access"])';
		$m.__track_lines__[36] = 'actions.list.py, line 36:\n    isDisabled = modul is not None and "disabledFunctions" in conf["modules"][modul].keys() and conf["modules"][modul]["disabledFunctions"] and "add" in conf["modules"][modul]["disabledFunctions"]';
		$m.__track_lines__[37] = 'actions.list.py, line 37:\n    return(  correctAction and correctHandler and hasAccess and not isDisabled )';
		$m.__track_lines__[39] = 'actions.list.py, line 39:\n    def onClick(self, sender=None):';
		$m.__track_lines__[40] = 'actions.list.py, line 40:\n    pane = EditPane(translate("Add"), closeable=True, iconClasses=["modul_%s" % self.parent().parent().modul, "apptype_list", "action_add" ])';
		$m.__track_lines__[41] = 'actions.list.py, line 41:\n    conf["mainWindow"].stackPane( pane )';
		$m.__track_lines__[42] = 'actions.list.py, line 42:\n    edwg = EditWidget( self.parent().parent().modul, EditWidget.appList )';
		$m.__track_lines__[43] = 'actions.list.py, line 43:\n    pane.addWidget( edwg )';
		$m.__track_lines__[44] = 'actions.list.py, line 44:\n    pane.focus()';
		$m.__track_lines__[46] = 'actions.list.py, line 46:\n    def resetLoadingState(self):';
		$m.__track_lines__[47] = 'actions.list.py, line 47:\n    pass';
		$m.__track_lines__[49] = 'actions.list.py, line 49:\n    actionDelegateSelector.insert( 1, AddAction.isSuitableFor, AddAction )';
		$m.__track_lines__[52] = 'actions.list.py, line 52:\n    class EditAction( html5.ext.Button ):';
		$m.__track_lines__[57] = 'actions.list.py, line 57:\n    def __init__(self, *args, **kwargs):';
		$m.__track_lines__[58] = 'actions.list.py, line 58:\n    super( EditAction, self ).__init__( translate("Edit"), *args, **kwargs )';
		$m.__track_lines__[59] = 'actions.list.py, line 59:\n    self["class"] = "icon edit"';
		$m.__track_lines__[60] = 'actions.list.py, line 60:\n    self["disabled"]= True';
		$m.__track_lines__[61] = 'actions.list.py, line 61:\n    self.isDisabled=True';
		$m.__track_lines__[63] = 'actions.list.py, line 63:\n    def onAttach(self):';
		$m.__track_lines__[64] = 'actions.list.py, line 64:\n    super(EditAction,self).onAttach()';
		$m.__track_lines__[65] = 'actions.list.py, line 65:\n    self.parent().parent().selectionChangedEvent.register( self )';
		$m.__track_lines__[66] = 'actions.list.py, line 66:\n    self.parent().parent().selectionActivatedEvent.register( self )';
		$m.__track_lines__[68] = 'actions.list.py, line 68:\n    def onDetach(self):';
		$m.__track_lines__[69] = 'actions.list.py, line 69:\n    self.parent().parent().selectionChangedEvent.unregister( self )';
		$m.__track_lines__[70] = 'actions.list.py, line 70:\n    self.parent().parent().selectionActivatedEvent.unregister( self )';
		$m.__track_lines__[71] = 'actions.list.py, line 71:\n    super(EditAction,self).onDetach()';
		$m.__track_lines__[73] = 'actions.list.py, line 73:\n    def onSelectionChanged(self, table, selection ):';
		$m.__track_lines__[74] = 'actions.list.py, line 74:\n    if len(selection)>0:';
		$m.__track_lines__[75] = 'actions.list.py, line 75:\n    if self.isDisabled:';
		$m.__track_lines__[76] = 'actions.list.py, line 76:\n    self.isDisabled = False';
		$m.__track_lines__[77] = 'actions.list.py, line 77:\n    self["disabled"]= False';
		$m.__track_lines__[79] = 'actions.list.py, line 79:\n    if not self.isDisabled:';
		$m.__track_lines__[80] = 'actions.list.py, line 80:\n    self["disabled"]= True';
		$m.__track_lines__[81] = 'actions.list.py, line 81:\n    self.isDisabled = True';
		$m.__track_lines__[83] = 'actions.list.py, line 83:\n    def onSelectionActivated(self, table, selection ):';
		$m.__track_lines__[84] = 'actions.list.py, line 84:\n    if not self.parent().parent().isSelector and len(selection)==1:';
		$m.__track_lines__[85] = 'actions.list.py, line 85:\n    self.openEditor( selection[0]["id"] )';
		$m.__track_lines__[88] = 'actions.list.py, line 87:\n    @staticmethod ... def isSuitableFor( modul, handler, actionName ):';
		$m.__track_lines__[89] = 'actions.list.py, line 89:\n    if modul is None:';
		$m.__track_lines__[90] = 'actions.list.py, line 90:\n    return( False )';
		$m.__track_lines__[91] = 'actions.list.py, line 91:\n    correctAction = actionName=="edit"';
		$m.__track_lines__[92] = 'actions.list.py, line 92:\n    correctHandler = handler == "list" or handler.startswith("list.")';
		$m.__track_lines__[93] = 'actions.list.py, line 93:\n    hasAccess = conf["currentUser"] and ("root" in conf["currentUser"]["access"] or modul+"-edit" in conf["currentUser"]["access"])';
		$m.__track_lines__[94] = 'actions.list.py, line 94:\n    isDisabled = modul is not None and "disabledFunctions" in conf["modules"][modul].keys() and conf["modules"][modul]["disabledFunctions"] and "edit" in conf["modules"][modul]["disabledFunctions"]';
		$m.__track_lines__[95] = 'actions.list.py, line 95:\n    return(  correctAction and correctHandler and hasAccess and not isDisabled )';
		$m.__track_lines__[97] = 'actions.list.py, line 97:\n    def onClick(self, sender=None):';
		$m.__track_lines__[98] = 'actions.list.py, line 98:\n    selection = self.parent().parent().getCurrentSelection()';
		$m.__track_lines__[99] = 'actions.list.py, line 99:\n    if not selection:';
		$m.__track_lines__[100] = 'actions.list.py, line 100:\n    return';
		$m.__track_lines__[101] = 'actions.list.py, line 101:\n    for s in selection:';
		$m.__track_lines__[102] = 'actions.list.py, line 102:\n    self.openEditor( s["id"] )';
		$m.__track_lines__[104] = 'actions.list.py, line 104:\n    def openEditor(self, id ):';
		$m.__track_lines__[105] = 'actions.list.py, line 105:\n    pane = Pane(translate("Edit"), closeable=True, iconClasses=["modul_%s" % self.parent().parent().modul, "apptype_list", "action_edit" ])';
		$m.__track_lines__[106] = 'actions.list.py, line 106:\n    conf["mainWindow"].stackPane( pane, focus=True )';
		$m.__track_lines__[107] = 'actions.list.py, line 107:\n    edwg = EditWidget( self.parent().parent().modul, EditWidget.appList, key=id )';
		$m.__track_lines__[108] = 'actions.list.py, line 108:\n    pane.addWidget( edwg )';
		$m.__track_lines__[110] = 'actions.list.py, line 110:\n    def resetLoadingState(self):';
		$m.__track_lines__[111] = 'actions.list.py, line 111:\n    pass';
		$m.__track_lines__[113] = 'actions.list.py, line 113:\n    actionDelegateSelector.insert( 1, EditAction.isSuitableFor, EditAction )';
		$m.__track_lines__[116] = 'actions.list.py, line 116:\n    class CloneAction( html5.ext.Button ):';
		$m.__track_lines__[121] = 'actions.list.py, line 121:\n    def __init__(self, *args, **kwargs):';
		$m.__track_lines__[122] = 'actions.list.py, line 122:\n    super( CloneAction, self ).__init__( translate("Clone"), *args, **kwargs )';
		$m.__track_lines__[123] = 'actions.list.py, line 123:\n    self["class"] = "icon clone"';
		$m.__track_lines__[124] = 'actions.list.py, line 124:\n    self["disabled"]= True';
		$m.__track_lines__[125] = 'actions.list.py, line 125:\n    self.isDisabled=True';
		$m.__track_lines__[127] = 'actions.list.py, line 127:\n    def onAttach(self):';
		$m.__track_lines__[128] = 'actions.list.py, line 128:\n    super(CloneAction,self).onAttach()';
		$m.__track_lines__[129] = 'actions.list.py, line 129:\n    self.parent().parent().selectionChangedEvent.register( self )';
		$m.__track_lines__[131] = 'actions.list.py, line 131:\n    def onDetach(self):';
		$m.__track_lines__[132] = 'actions.list.py, line 132:\n    self.parent().parent().selectionChangedEvent.unregister( self )';
		$m.__track_lines__[133] = 'actions.list.py, line 133:\n    super(CloneAction,self).onDetach()';
		$m.__track_lines__[135] = 'actions.list.py, line 135:\n    def onSelectionChanged(self, table, selection ):';
		$m.__track_lines__[136] = 'actions.list.py, line 136:\n    if len(selection)>0:';
		$m.__track_lines__[137] = 'actions.list.py, line 137:\n    if self.isDisabled:';
		$m.__track_lines__[138] = 'actions.list.py, line 138:\n    self.isDisabled = False';
		$m.__track_lines__[139] = 'actions.list.py, line 139:\n    self["disabled"]= False';
		$m.__track_lines__[141] = 'actions.list.py, line 141:\n    if not self.isDisabled:';
		$m.__track_lines__[142] = 'actions.list.py, line 142:\n    self["disabled"]= True';
		$m.__track_lines__[143] = 'actions.list.py, line 143:\n    self.isDisabled = True';
		$m.__track_lines__[146] = 'actions.list.py, line 145:\n    @staticmethod ... def isSuitableFor( modul, handler, actionName ):';
		$m.__track_lines__[147] = 'actions.list.py, line 147:\n    if modul is None:';
		$m.__track_lines__[148] = 'actions.list.py, line 148:\n    return( False )';
		$m.__track_lines__[149] = 'actions.list.py, line 149:\n    correctAction = actionName=="clone"';
		$m.__track_lines__[150] = 'actions.list.py, line 150:\n    correctHandler = handler == "list" or handler.startswith("list.")';
		$m.__track_lines__[151] = 'actions.list.py, line 151:\n    hasAccess = conf["currentUser"] and ("root" in conf["currentUser"]["access"] or modul+"-edit" in conf["currentUser"]["access"])';
		$m.__track_lines__[152] = 'actions.list.py, line 152:\n    isDisabled = modul is not None and "disabledFunctions" in conf["modules"][modul].keys() and conf["modules"][modul]["disabledFunctions"] and "clone" in conf["modules"][modul]["disabledFunctions"]';
		$m.__track_lines__[153] = 'actions.list.py, line 153:\n    return(  correctAction and correctHandler and hasAccess and not isDisabled )';
		$m.__track_lines__[155] = 'actions.list.py, line 155:\n    def onClick(self, sender=None):';
		$m.__track_lines__[156] = 'actions.list.py, line 156:\n    selection = self.parent().parent().getCurrentSelection()';
		$m.__track_lines__[157] = 'actions.list.py, line 157:\n    if not selection:';
		$m.__track_lines__[158] = 'actions.list.py, line 158:\n    return';
		$m.__track_lines__[159] = 'actions.list.py, line 159:\n    for s in selection:';
		$m.__track_lines__[160] = 'actions.list.py, line 160:\n    self.openEditor( s["id"] )';
		$m.__track_lines__[162] = 'actions.list.py, line 162:\n    def openEditor(self, id ):';
		$m.__track_lines__[163] = 'actions.list.py, line 163:\n    pane = Pane(translate("Clone"), closeable=True, iconClasses=["modul_%s" % self.parent().parent().modul, "apptype_list", "action_edit" ])';
		$m.__track_lines__[164] = 'actions.list.py, line 164:\n    conf["mainWindow"].stackPane( pane )';
		$m.__track_lines__[165] = 'actions.list.py, line 165:\n    edwg = EditWidget( self.parent().parent().modul, EditWidget.appList, key=id, clone=True )';
		$m.__track_lines__[166] = 'actions.list.py, line 166:\n    pane.addWidget( edwg )';
		$m.__track_lines__[167] = 'actions.list.py, line 167:\n    pane.focus()';
		$m.__track_lines__[169] = 'actions.list.py, line 169:\n    def resetLoadingState(self):';
		$m.__track_lines__[170] = 'actions.list.py, line 170:\n    pass';
		$m.__track_lines__[172] = 'actions.list.py, line 172:\n    actionDelegateSelector.insert( 1, CloneAction.isSuitableFor, CloneAction )';
		$m.__track_lines__[176] = 'actions.list.py, line 176:\n    class DeleteAction( html5.ext.Button ):';
		$m.__track_lines__[180] = 'actions.list.py, line 180:\n    def __init__(self, *args, **kwargs):';
		$m.__track_lines__[181] = 'actions.list.py, line 181:\n    super( DeleteAction, self ).__init__( translate("Delete"), *args, **kwargs )';
		$m.__track_lines__[182] = 'actions.list.py, line 182:\n    self["class"] = "icon delete"';
		$m.__track_lines__[183] = 'actions.list.py, line 183:\n    self["disabled"]= True';
		$m.__track_lines__[184] = 'actions.list.py, line 184:\n    self.isDisabled=True';
		$m.__track_lines__[186] = 'actions.list.py, line 186:\n    def onAttach(self):';
		$m.__track_lines__[187] = 'actions.list.py, line 187:\n    super(DeleteAction,self).onAttach()';
		$m.__track_lines__[188] = 'actions.list.py, line 188:\n    self.parent().parent().selectionChangedEvent.register( self )';
		$m.__track_lines__[190] = 'actions.list.py, line 190:\n    def onDetach(self):';
		$m.__track_lines__[191] = 'actions.list.py, line 191:\n    self.parent().parent().selectionChangedEvent.unregister( self )';
		$m.__track_lines__[192] = 'actions.list.py, line 192:\n    super(DeleteAction,self).onDetach()';
		$m.__track_lines__[194] = 'actions.list.py, line 194:\n    def onSelectionChanged(self, table, selection ):';
		$m.__track_lines__[195] = 'actions.list.py, line 195:\n    if len(selection)>0:';
		$m.__track_lines__[196] = 'actions.list.py, line 196:\n    if self.isDisabled:';
		$m.__track_lines__[197] = 'actions.list.py, line 197:\n    self.isDisabled = False';
		$m.__track_lines__[198] = 'actions.list.py, line 198:\n    self["disabled"]= False';
		$m.__track_lines__[200] = 'actions.list.py, line 200:\n    if not self.isDisabled:';
		$m.__track_lines__[201] = 'actions.list.py, line 201:\n    self["disabled"]= True';
		$m.__track_lines__[202] = 'actions.list.py, line 202:\n    self.isDisabled = True';
		$m.__track_lines__[205] = 'actions.list.py, line 204:\n    @staticmethod ... def isSuitableFor( modul, handler, actionName ):';
		$m.__track_lines__[206] = 'actions.list.py, line 206:\n    if modul is None:';
		$m.__track_lines__[207] = 'actions.list.py, line 207:\n    return( False )';
		$m.__track_lines__[208] = 'actions.list.py, line 208:\n    correctAction = actionName=="delete"';
		$m.__track_lines__[209] = 'actions.list.py, line 209:\n    correctHandler = handler == "list" or handler.startswith("list.")';
		$m.__track_lines__[210] = 'actions.list.py, line 210:\n    hasAccess = conf["currentUser"] and ("root" in conf["currentUser"]["access"] or modul+"-delete" in conf["currentUser"]["access"])';
		$m.__track_lines__[211] = 'actions.list.py, line 211:\n    isDisabled = modul is not None and "disabledFunctions" in conf["modules"][modul].keys() and conf["modules"][modul]["disabledFunctions"] and "delete" in conf["modules"][modul]["disabledFunctions"]';
		$m.__track_lines__[212] = 'actions.list.py, line 212:\n    return(  correctAction and correctHandler and hasAccess and not isDisabled )';
		$m.__track_lines__[215] = 'actions.list.py, line 215:\n    def onClick(self, sender=None):';
		$m.__track_lines__[216] = 'actions.list.py, line 216:\n    selection = self.parent().parent().getCurrentSelection()';
		$m.__track_lines__[217] = 'actions.list.py, line 217:\n    if not selection:';
		$m.__track_lines__[218] = 'actions.list.py, line 218:\n    return';
		$m.__track_lines__[219] = 'actions.list.py, line 219:\n    d = html5.ext.YesNoDialog(translate("Delete {amt} Entries?",amt=len(selection)) ,title=translate("Delete them?"), yesCallback=self.doDelete, yesLabel=translate("Delete"), noLabel=translate("Keep") )';
		$m.__track_lines__[220] = 'actions.list.py, line 220:\n    d.deleteList = [x["id"] for x in selection]';
		$m.__track_lines__[221] = 'actions.list.py, line 221:\n    d["class"].append( "delete" )';
		$m.__track_lines__[223] = 'actions.list.py, line 223:\n    def doDelete(self, dialog):';
		$m.__track_lines__[224] = 'actions.list.py, line 224:\n    deleteList = dialog.deleteList';
		$m.__track_lines__[225] = 'actions.list.py, line 225:\n    for x in deleteList:';
		$m.__track_lines__[226] = 'actions.list.py, line 226:\n    NetworkService.request( self.parent().parent().modul, "delete", {"id": x}, secure=True, modifies=True )';
		$m.__track_lines__[228] = 'actions.list.py, line 228:\n    def resetLoadingState(self):';
		$m.__track_lines__[229] = 'actions.list.py, line 229:\n    pass';
		$m.__track_lines__[231] = 'actions.list.py, line 231:\n    actionDelegateSelector.insert( 1, DeleteAction.isSuitableFor, DeleteAction )';
		$m.__track_lines__[233] = 'actions.list.py, line 233:\n    class ListPreviewAction( html5.Span ):';
		$m.__track_lines__[234] = 'actions.list.py, line 234:\n    def __init__(self, *args, **kwargs ):';
		$m.__track_lines__[235] = 'actions.list.py, line 235:\n    super( ListPreviewAction, self ).__init__( *args, **kwargs )';
		$m.__track_lines__[236] = 'actions.list.py, line 236:\n    self.urlCb = html5.Select()';
		$m.__track_lines__[237] = 'actions.list.py, line 237:\n    self.appendChild( self.urlCb )';
		$m.__track_lines__[238] = 'actions.list.py, line 238:\n    btn = html5.ext.Button( translate("Preview"), callback=self.onClick )';
		$m.__track_lines__[239] = 'actions.list.py, line 239:\n    btn["class"] = "icon preview"';
		$m.__track_lines__[240] = 'actions.list.py, line 240:\n    self.appendChild(btn)';
		$m.__track_lines__[241] = 'actions.list.py, line 241:\n    self.urls = None';
		$m.__track_lines__[243] = 'actions.list.py, line 243:\n    def onChange(self, event):';
		$m.__track_lines__[244] = 'actions.list.py, line 244:\n    event.stopPropagation()';
		$m.__track_lines__[245] = 'actions.list.py, line 245:\n    newUrl = self.urlCb["options"].item(self.urlCb["selectedIndex"]).value';
		$m.__track_lines__[246] = 'actions.list.py, line 246:\n    self.setUrl( newUrl )';
		$m.__track_lines__[248] = 'actions.list.py, line 248:\n    def rebuildCB(self, *args, **kwargs):';
		$m.__track_lines__[249] = 'actions.list.py, line 249:\n    self.urlCb.element.innerHTML = ""';
		$m.__track_lines__[251] = 'actions.list.py, line 251:\n    if not isinstance(self.urls, dict):';
		$m.__track_lines__[252] = 'actions.list.py, line 252:\n    self.urlCb["style"]["display"] = "none"';
		$m.__track_lines__[253] = 'actions.list.py, line 253:\n    return';
		$m.__track_lines__[255] = 'actions.list.py, line 255:\n    for name,url in self.urls.items():';
		$m.__track_lines__[256] = 'actions.list.py, line 256:\n    o = html5.Option()';
		$m.__track_lines__[257] = 'actions.list.py, line 257:\n    o["value"] = url';
		$m.__track_lines__[258] = 'actions.list.py, line 258:\n    o.appendChild(html5.TextNode(name))';
		$m.__track_lines__[259] = 'actions.list.py, line 259:\n    self.urlCb.appendChild(o)';
		$m.__track_lines__[261] = 'actions.list.py, line 261:\n    if len( self.urls.keys() ) == 1:';
		$m.__track_lines__[262] = 'actions.list.py, line 262:\n    self.urlCb["style"]["display"] = "none"';
		$m.__track_lines__[264] = 'actions.list.py, line 264:\n    self.urlCb["style"]["display"] = ""';
		$m.__track_lines__[266] = 'actions.list.py, line 266:\n    def onAttach(self):';
		$m.__track_lines__[267] = 'actions.list.py, line 267:\n    super(ListPreviewAction,self).onAttach()';
		$m.__track_lines__[268] = 'actions.list.py, line 268:\n    modul = self.parent().parent().modul';
		$m.__track_lines__[269] = 'actions.list.py, line 269:\n    if modul in conf["modules"].keys():';
		$m.__track_lines__[270] = 'actions.list.py, line 270:\n    modulConfig = conf["modules"][modul]';
		$m.__track_lines__[271] = 'actions.list.py, line 271:\n    if "previewurls" in modulConfig.keys() and modulConfig["previewurls"]:';
		$m.__track_lines__[272] = 'actions.list.py, line 272:\n    self.urls = modulConfig["previewurls"]';
		$m.__track_lines__[273] = 'actions.list.py, line 273:\n    self.rebuildCB()';
		$m.__track_lines__[276] = 'actions.list.py, line 276:\n    def onClick(self, sender=None):';
		$m.__track_lines__[277] = 'actions.list.py, line 277:\n    if self.urls is None:';
		$m.__track_lines__[278] = 'actions.list.py, line 278:\n    return';
		$m.__track_lines__[279] = 'actions.list.py, line 279:\n    selection = self.parent().parent().getCurrentSelection()';
		$m.__track_lines__[280] = 'actions.list.py, line 280:\n    if not selection:';
		$m.__track_lines__[281] = 'actions.list.py, line 281:\n    return';
		$m.__track_lines__[283] = 'actions.list.py, line 283:\n    for entry in selection:';
		$m.__track_lines__[284] = 'actions.list.py, line 284:\n    if isinstance(self.urls, str):';
		$m.__track_lines__[285] = 'actions.list.py, line 285:\n    newUrl = self.urls';
		$m.__track_lines__[287] = 'actions.list.py, line 287:\n    newUrl = self.urls.values()[0]';
		$m.__track_lines__[289] = 'actions.list.py, line 289:\n    newUrl = self.urlCb["options"].item(self.urlCb["selectedIndex"]).value';
		$m.__track_lines__[291] = 'actions.list.py, line 291:\n    print(newUrl)';
		$m.__track_lines__[293] = 'actions.list.py, line 293:\n    newUrl = newUrl \\';
		$m.__track_lines__[297] = 'actions.list.py, line 297:\n    for k, v in entry.items():';
		$m.__track_lines__[298] = 'actions.list.py, line 298:\n    newUrl = newUrl.replace("{{%s}}" % k, v)';
		$m.__track_lines__[300] = 'actions.list.py, line 300:\n    newUrl = newUrl.replace("\'", "\\\\\'")';
		$m.__track_lines__[302] = 'actions.list.py, line 302:\n    print(newUrl)';
		$m.__track_lines__[303] = 'actions.list.py, line 303:\n    eval("""window.open(\'"""+newUrl+"""\', \'ViPreview\');""")';
		$m.__track_lines__[308] = 'actions.list.py, line 307:\n    @staticmethod ... def isSuitableFor( modul, handler, actionName ):';
		$m.__track_lines__[309] = 'actions.list.py, line 309:\n    if modul is None:';
		$m.__track_lines__[310] = 'actions.list.py, line 310:\n    return( False )';
		$m.__track_lines__[311] = 'actions.list.py, line 311:\n    correctAction = actionName=="preview"';
		$m.__track_lines__[312] = 'actions.list.py, line 312:\n    correctHandler = handler == "list" or handler.startswith("list.")';
		$m.__track_lines__[313] = 'actions.list.py, line 313:\n    hasAccess = conf["currentUser"] and ("root" in conf["currentUser"]["access"] or modul+"-view" in conf["currentUser"]["access"])';
		$m.__track_lines__[314] = 'actions.list.py, line 314:\n    isDisabled = modul is not None and "disabledFunctions" in conf["modules"][modul].keys() and conf["modules"][modul]["disabledFunctions"] and "view" in conf["modules"][modul]["disabledFunctions"]';
		$m.__track_lines__[315] = 'actions.list.py, line 315:\n    isAvailable = False';
		$m.__track_lines__[316] = 'actions.list.py, line 316:\n    if modul in conf["modules"].keys():';
		$m.__track_lines__[317] = 'actions.list.py, line 317:\n    modulConfig = conf["modules"][modul]';
		$m.__track_lines__[318] = 'actions.list.py, line 318:\n    if "previewurls" in modulConfig.keys() and modulConfig["previewurls"]:';
		$m.__track_lines__[319] = 'actions.list.py, line 319:\n    isAvailable = True';
		$m.__track_lines__[320] = 'actions.list.py, line 320:\n    return(  correctAction and correctHandler and hasAccess and not isDisabled and isAvailable )';
		$m.__track_lines__[322] = 'actions.list.py, line 322:\n    actionDelegateSelector.insert( 2, ListPreviewAction.isSuitableFor, ListPreviewAction )';
		$m.__track_lines__[325] = 'actions.list.py, line 325:\n    class ListPreviewInlineAction( html5.ext.Button ):';
		$m.__track_lines__[326] = 'actions.list.py, line 326:\n    def __init__(self, *args, **kwargs ):';
		$m.__track_lines__[327] = 'actions.list.py, line 327:\n    super( ListPreviewInlineAction, self ).__init__( translate("Preview"), *args, **kwargs )';
		$m.__track_lines__[328] = 'actions.list.py, line 328:\n    self["class"] = "icon preview"';
		$m.__track_lines__[329] = 'actions.list.py, line 329:\n    self["disabled"] = True';
		$m.__track_lines__[330] = 'actions.list.py, line 330:\n    self.urls = None';
		$m.__track_lines__[332] = 'actions.list.py, line 332:\n    def onAttach(self):';
		$m.__track_lines__[333] = 'actions.list.py, line 333:\n    super( ListPreviewInlineAction,self ).onAttach()';
		$m.__track_lines__[334] = 'actions.list.py, line 334:\n    self.parent().parent().selectionChangedEvent.register( self )';
		$m.__track_lines__[336] = 'actions.list.py, line 336:\n    def onDetach(self):';
		$m.__track_lines__[337] = 'actions.list.py, line 337:\n    self.parent().parent().selectionChangedEvent.unregister( self )';
		$m.__track_lines__[338] = 'actions.list.py, line 338:\n    super( ListPreviewInlineAction, self ).onDetach()';
		$m.__track_lines__[340] = 'actions.list.py, line 340:\n    def onSelectionChanged(self, table, selection):';
		$m.__track_lines__[341] = 'actions.list.py, line 341:\n    if self.parent().parent().isSelector:';
		$m.__track_lines__[342] = 'actions.list.py, line 342:\n    return';
		$m.__track_lines__[345] = 'actions.list.py, line 345:\n    module = self.parent().parent().modul';
		$m.__track_lines__[346] = 'actions.list.py, line 346:\n    if ("disableInternalPreview" in conf["modules"][module].keys()';
		$m.__track_lines__[348] = 'actions.list.py, line 348:\n    return';
		$m.__track_lines__[351] = 'actions.list.py, line 351:\n    if (self.parent().parent().sideBar.getWidget()';
		$m.__track_lines__[353] = 'actions.list.py, line 353:\n    return';
		$m.__track_lines__[357] = 'actions.list.py, line 357:\n    if len(selection)==1:';
		$m.__track_lines__[358] = 'actions.list.py, line 358:\n    preview = InternalPreview( self.parent().parent().modul, self.parent().parent()._structure, selection[0])';
		$m.__track_lines__[359] = 'actions.list.py, line 359:\n    self.parent().parent().sideBar.setWidget( preview )';
		$m.__track_lines__[361] = 'actions.list.py, line 361:\n    if isinstance( self.parent().parent().sideBar.getWidget(), InternalPreview ):';
		$m.__track_lines__[362] = 'actions.list.py, line 362:\n    self.parent().parent().sideBar.setWidget( None )';
		$m.__track_lines__[365] = 'actions.list.py, line 364:\n    @staticmethod ... def isSuitableFor( modul, handler, actionName ):';
		$m.__track_lines__[366] = 'actions.list.py, line 366:\n    if modul is None:';
		$m.__track_lines__[367] = 'actions.list.py, line 367:\n    return( False )';
		$m.__track_lines__[368] = 'actions.list.py, line 368:\n    correctAction = actionName=="preview"';
		$m.__track_lines__[369] = 'actions.list.py, line 369:\n    correctHandler = handler == "list" or handler.startswith("list.")';
		$m.__track_lines__[370] = 'actions.list.py, line 370:\n    hasAccess = conf["currentUser"] and ("root" in conf["currentUser"]["access"] or modul+"-view" in conf["currentUser"]["access"])';
		$m.__track_lines__[371] = 'actions.list.py, line 371:\n    isDisabled = modul is not None and "disabledFunctions" in conf["modules"][modul].keys() and conf["modules"][modul]["disabledFunctions"] and "view" in conf["modules"][modul]["disabledFunctions"]';
		$m.__track_lines__[372] = 'actions.list.py, line 372:\n    return(  correctAction and correctHandler and hasAccess and not isDisabled )';
		$m.__track_lines__[374] = 'actions.list.py, line 374:\n    actionDelegateSelector.insert( 1, ListPreviewInlineAction.isSuitableFor, ListPreviewInlineAction )';
		$m.__track_lines__[377] = 'actions.list.py, line 377:\n    class CloseAction( html5.ext.Button ):';
		$m.__track_lines__[378] = 'actions.list.py, line 378:\n    def __init__(self, *args, **kwargs ):';
		$m.__track_lines__[379] = 'actions.list.py, line 379:\n    super( CloseAction, self ).__init__( translate("Close"), *args, **kwargs )';
		$m.__track_lines__[380] = 'actions.list.py, line 380:\n    self["class"] = "icon close"';
		$m.__track_lines__[382] = 'actions.list.py, line 382:\n    def onClick(self, sender=None):';
		$m.__track_lines__[383] = 'actions.list.py, line 383:\n    conf["mainWindow"].removeWidget( self.parent().parent() )';
		$m.__track_lines__[386] = 'actions.list.py, line 385:\n    @staticmethod ... def isSuitableFor( modul, handler, actionName ):';
		$m.__track_lines__[387] = 'actions.list.py, line 387:\n    return( actionName=="close" )';
		$m.__track_lines__[389] = 'actions.list.py, line 389:\n    actionDelegateSelector.insert( 1, CloseAction.isSuitableFor, CloseAction )';
		$m.__track_lines__[391] = 'actions.list.py, line 391:\n    class ActivateSelectionAction( html5.ext.Button ):';
		$m.__track_lines__[392] = 'actions.list.py, line 392:\n    def __init__(self, *args, **kwargs ):';
		$m.__track_lines__[393] = 'actions.list.py, line 393:\n    super( ActivateSelectionAction, self ).__init__( translate("Select"), *args, **kwargs )';
		$m.__track_lines__[394] = 'actions.list.py, line 394:\n    self["class"] = "icon activateselection"';
		$m.__track_lines__[396] = 'actions.list.py, line 396:\n    def onClick(self, sender=None):';
		$m.__track_lines__[397] = 'actions.list.py, line 397:\n    self.parent().parent().activateCurrentSelection()';
		$m.__track_lines__[400] = 'actions.list.py, line 399:\n    @staticmethod ... def isSuitableFor( modul, handler, actionName ):';
		$m.__track_lines__[401] = 'actions.list.py, line 401:\n    return( actionName=="select" )';
		$m.__track_lines__[403] = 'actions.list.py, line 403:\n    actionDelegateSelector.insert( 1, ActivateSelectionAction.isSuitableFor, ActivateSelectionAction )';
		$m.__track_lines__[406] = 'actions.list.py, line 406:\n    class SelectFieldsPopup( html5.ext.Popup ):';
		$m.__track_lines__[407] = 'actions.list.py, line 407:\n    def __init__(self, listWdg, *args, **kwargs):';
		$m.__track_lines__[408] = 'actions.list.py, line 408:\n    if not listWdg._structure:';
		$m.__track_lines__[409] = 'actions.list.py, line 409:\n    return';
		$m.__track_lines__[411] = 'actions.list.py, line 411:\n    super( SelectFieldsPopup, self ).__init__( title=translate("Select fields"), *args, **kwargs )';
		$m.__track_lines__[413] = 'actions.list.py, line 413:\n    self["class"].append("selectfields")';
		$m.__track_lines__[414] = 'actions.list.py, line 414:\n    self.listWdg = listWdg';
		$m.__track_lines__[415] = 'actions.list.py, line 415:\n    self.checkboxes = []';
		$m.__track_lines__[417] = 'actions.list.py, line 417:\n    ul = html5.Ul()';
		$m.__track_lines__[418] = 'actions.list.py, line 418:\n    self.appendChild( ul )';
		$m.__track_lines__[420] = 'actions.list.py, line 420:\n    for key, bone in self.listWdg._structure:';
		$m.__track_lines__[421] = 'actions.list.py, line 421:\n    li = html5.Li()';
		$m.__track_lines__[422] = 'actions.list.py, line 422:\n    ul.appendChild( li )';
		$m.__track_lines__[424] = 'actions.list.py, line 424:\n    chkBox = html5.Input()';
		$m.__track_lines__[425] = 'actions.list.py, line 425:\n    chkBox["type"] = "checkbox"';
		$m.__track_lines__[426] = 'actions.list.py, line 426:\n    chkBox["value"] = key';
		$m.__track_lines__[428] = 'actions.list.py, line 428:\n    li.appendChild(chkBox)';
		$m.__track_lines__[429] = 'actions.list.py, line 429:\n    self.checkboxes.append( chkBox )';
		$m.__track_lines__[431] = 'actions.list.py, line 431:\n    if key in self.listWdg.getFields():';
		$m.__track_lines__[432] = 'actions.list.py, line 432:\n    chkBox["checked"] = True';
		$m.__track_lines__[433] = 'actions.list.py, line 433:\n    lbl = html5.Label(bone["descr"],forElem=chkBox)';
		$m.__track_lines__[434] = 'actions.list.py, line 434:\n    li.appendChild(lbl)';
		$m.__track_lines__[437] = 'actions.list.py, line 437:\n    div = html5.Div()';
		$m.__track_lines__[438] = 'actions.list.py, line 438:\n    div[ "class" ].append( "selectiontools" )';
		$m.__track_lines__[440] = 'actions.list.py, line 440:\n    self.appendChild( div )';
		$m.__track_lines__[442] = 'actions.list.py, line 442:\n    self.selectAllBtn =  html5.ext.Button( translate( "Select all" ), callback=self.doSelectAll )';
		$m.__track_lines__[443] = 'actions.list.py, line 443:\n    self.selectAllBtn[ "class" ].append( "icon" )';
		$m.__track_lines__[444] = 'actions.list.py, line 444:\n    self.selectAllBtn[ "class" ].append( "selectall" )';
		$m.__track_lines__[445] = 'actions.list.py, line 445:\n    self.unselectAllBtn =  html5.ext.Button( translate( "Unselect all" ), callback=self.doUnselectAll )';
		$m.__track_lines__[446] = 'actions.list.py, line 446:\n    self.unselectAllBtn[ "class" ].append( "icon" )';
		$m.__track_lines__[447] = 'actions.list.py, line 447:\n    self.unselectAllBtn[ "class" ].append( "unselectall" )';
		$m.__track_lines__[448] = 'actions.list.py, line 448:\n    self.invertSelectionBtn =  html5.ext.Button( translate( "Invert selection" ), callback=self.doInvertSelection )';
		$m.__track_lines__[449] = 'actions.list.py, line 449:\n    self.invertSelectionBtn[ "class" ].append( "icon" )';
		$m.__track_lines__[450] = 'actions.list.py, line 450:\n    self.invertSelectionBtn[ "class" ].append( "selectinvert" )';
		$m.__track_lines__[452] = 'actions.list.py, line 452:\n    div.appendChild(self.selectAllBtn)';
		$m.__track_lines__[453] = 'actions.list.py, line 453:\n    div.appendChild(self.unselectAllBtn)';
		$m.__track_lines__[454] = 'actions.list.py, line 454:\n    div.appendChild(self.invertSelectionBtn)';
		$m.__track_lines__[457] = 'actions.list.py, line 457:\n    self.cancelBtn = html5.ext.Button( translate( "Cancel" ), callback=self.doCancel)';
		$m.__track_lines__[458] = 'actions.list.py, line 458:\n    self.cancelBtn["class"].append("btn_no")';
		$m.__track_lines__[460] = 'actions.list.py, line 460:\n    self.applyBtn = html5.ext.Button( translate( "Apply" ), callback=self.doApply)';
		$m.__track_lines__[461] = 'actions.list.py, line 461:\n    self.applyBtn["class"].append("btn_yes")';
		$m.__track_lines__[463] = 'actions.list.py, line 463:\n    self.appendChild(self.applyBtn)';
		$m.__track_lines__[464] = 'actions.list.py, line 464:\n    self.appendChild(self.cancelBtn)';
		$m.__track_lines__[466] = 'actions.list.py, line 466:\n    def doApply(self, *args, **kwargs):';
		$m.__track_lines__[467] = 'actions.list.py, line 467:\n    self.applyBtn["class"].append("is_loading")';
		$m.__track_lines__[468] = 'actions.list.py, line 468:\n    self.applyBtn["disabled"] = True';
		$m.__track_lines__[470] = 'actions.list.py, line 470:\n    res = []';
		$m.__track_lines__[471] = 'actions.list.py, line 471:\n    for c in self.checkboxes:';
		$m.__track_lines__[472] = 'actions.list.py, line 472:\n    if c["checked"]:';
		$m.__track_lines__[473] = 'actions.list.py, line 473:\n    res.append( c["value"] )';
		$m.__track_lines__[475] = 'actions.list.py, line 475:\n    self.listWdg.setFields( res )';
		$m.__track_lines__[476] = 'actions.list.py, line 476:\n    self.close()';
		$m.__track_lines__[478] = 'actions.list.py, line 478:\n    def doCancel(self, *args, **kwargs):';
		$m.__track_lines__[479] = 'actions.list.py, line 479:\n    self.close()';
		$m.__track_lines__[481] = 'actions.list.py, line 481:\n    def doSelectAll(self, *args, **kwargs):';
		$m.__track_lines__[482] = 'actions.list.py, line 482:\n    for cb in self.checkboxes:';
		$m.__track_lines__[483] = 'actions.list.py, line 483:\n    if cb[ "checked" ] == False:';
		$m.__track_lines__[484] = 'actions.list.py, line 484:\n    cb[ "checked" ] = True';
		$m.__track_lines__[486] = 'actions.list.py, line 486:\n    def doUnselectAll(self, *args, **kwargs):';
		$m.__track_lines__[487] = 'actions.list.py, line 487:\n    for cb in self.checkboxes:';
		$m.__track_lines__[488] = 'actions.list.py, line 488:\n    if cb[ "checked" ] == True:';
		$m.__track_lines__[489] = 'actions.list.py, line 489:\n    cb[ "checked" ] = False';
		$m.__track_lines__[491] = 'actions.list.py, line 491:\n    def doInvertSelection(self, *args, **kwargs):';
		$m.__track_lines__[492] = 'actions.list.py, line 492:\n    for cb in self.checkboxes:';
		$m.__track_lines__[493] = 'actions.list.py, line 493:\n    if cb[ "checked" ] == False:';
		$m.__track_lines__[494] = 'actions.list.py, line 494:\n    cb[ "checked" ] = True';
		$m.__track_lines__[496] = 'actions.list.py, line 496:\n    cb[ "checked" ] = False';
		$m.__track_lines__[498] = 'actions.list.py, line 498:\n    class SelectFieldsAction( html5.ext.Button ):';
		$m.__track_lines__[499] = 'actions.list.py, line 499:\n    def __init__(self, *args, **kwargs ):';
		$m.__track_lines__[500] = 'actions.list.py, line 500:\n    super( SelectFieldsAction, self ).__init__( translate("Select fields"), *args, **kwargs )';
		$m.__track_lines__[501] = 'actions.list.py, line 501:\n    self["class"] = "icon selectfields"';
		$m.__track_lines__[502] = 'actions.list.py, line 502:\n    self["disabled"] = self.isDisabled = True';
		$m.__track_lines__[504] = 'actions.list.py, line 504:\n    def onClick(self, sender=None):';
		$m.__track_lines__[505] = 'actions.list.py, line 505:\n    SelectFieldsPopup( self.parent().parent() )';
		$m.__track_lines__[507] = 'actions.list.py, line 507:\n    def onAttach(self):';
		$m.__track_lines__[508] = 'actions.list.py, line 508:\n    super(SelectFieldsAction,self).onAttach()';
		$m.__track_lines__[509] = 'actions.list.py, line 509:\n    self.parent().parent().tableChangedEvent.register( self )';
		$m.__track_lines__[511] = 'actions.list.py, line 511:\n    def onDetach(self):';
		$m.__track_lines__[512] = 'actions.list.py, line 512:\n    self.parent().parent().tableChangedEvent.unregister( self )';
		$m.__track_lines__[513] = 'actions.list.py, line 513:\n    super(SelectFieldsAction,self).onDetach()';
		$m.__track_lines__[515] = 'actions.list.py, line 515:\n    def onTableChanged(self, table, count):';
		$m.__track_lines__[516] = 'actions.list.py, line 516:\n    if count > 0:';
		$m.__track_lines__[517] = 'actions.list.py, line 517:\n    self["disabled"] = self.isDisabled = False';
		$m.__track_lines__[519] = 'actions.list.py, line 519:\n    self["disabled"] = self.isDisabled = True';
		$m.__track_lines__[522] = 'actions.list.py, line 521:\n    @staticmethod ... def isSuitableFor( modul, handler, actionName ):';
		$m.__track_lines__[523] = 'actions.list.py, line 523:\n    return( actionName=="selectfields" )';
		$m.__track_lines__[525] = 'actions.list.py, line 525:\n    actionDelegateSelector.insert( 1, SelectFieldsAction.isSuitableFor, SelectFieldsAction )';
		$m.__track_lines__[527] = 'actions.list.py, line 527:\n    class ReloadAction( html5.ext.Button ):';
		$m.__track_lines__[531] = 'actions.list.py, line 531:\n    def __init__(self, *args, **kwargs):';
		$m.__track_lines__[532] = 'actions.list.py, line 532:\n    super( ReloadAction, self ).__init__( translate("Reload"), *args, **kwargs )';
		$m.__track_lines__[533] = 'actions.list.py, line 533:\n    self["class"] = "icon reload"';
		$m.__track_lines__[536] = 'actions.list.py, line 535:\n    @staticmethod ... def isSuitableFor( modul, handler, actionName ):';
		$m.__track_lines__[537] = 'actions.list.py, line 537:\n    correctAction = actionName=="reload"';
		$m.__track_lines__[538] = 'actions.list.py, line 538:\n    correctHandler = handler == "list" or handler.startswith("list.")';
		$m.__track_lines__[539] = 'actions.list.py, line 539:\n    return(  correctAction and correctHandler )';
		$m.__track_lines__[541] = 'actions.list.py, line 541:\n    def onClick(self, sender=None):';
		$m.__track_lines__[542] = 'actions.list.py, line 542:\n    self["class"].append("is_loading")';
		$m.__track_lines__[543] = 'actions.list.py, line 543:\n    NetworkService.notifyChange( self.parent().parent().modul )';
		$m.__track_lines__[545] = 'actions.list.py, line 545:\n    def resetLoadingState(self):';
		$m.__track_lines__[546] = 'actions.list.py, line 546:\n    if "is_loading" in self["class"]:';
		$m.__track_lines__[547] = 'actions.list.py, line 547:\n    self["class"].remove("is_loading")';
		$m.__track_lines__[550] = 'actions.list.py, line 550:\n    actionDelegateSelector.insert( 1, ReloadAction.isSuitableFor, ReloadAction )';
		$m.__track_lines__[553] = 'actions.list.py, line 553:\n    class ListSelectFilterAction( html5.ext.Button ):';
		$m.__track_lines__[554] = 'actions.list.py, line 554:\n    def __init__(self, *args, **kwargs ):';
		$m.__track_lines__[555] = 'actions.list.py, line 555:\n    super( ListSelectFilterAction, self ).__init__( translate("Select Filter"), *args, **kwargs )';
		$m.__track_lines__[556] = 'actions.list.py, line 556:\n    self["class"] = "icon selectfilter"';
		$m.__track_lines__[557] = 'actions.list.py, line 557:\n    self.urls = None';
		$m.__track_lines__[558] = 'actions.list.py, line 558:\n    self.filterSelector = None';
		$m.__track_lines__[560] = 'actions.list.py, line 560:\n    def onAttach(self):';
		$m.__track_lines__[561] = 'actions.list.py, line 561:\n    super(ListSelectFilterAction,self).onAttach()';
		$m.__track_lines__[562] = 'actions.list.py, line 562:\n    modul = self.parent().parent().modul';
		$m.__track_lines__[563] = 'actions.list.py, line 563:\n    if self.parent().parent().filterID:';
		$m.__track_lines__[565] = 'actions.list.py, line 565:\n    self["disabled"] = True';
		$m.__track_lines__[566] = 'actions.list.py, line 566:\n    if modul in conf["modules"].keys():';
		$m.__track_lines__[567] = 'actions.list.py, line 567:\n    modulConfig = conf["modules"][modul]';
		$m.__track_lines__[568] = 'actions.list.py, line 568:\n    if "disabledFunctions" in modulConfig.keys() and modulConfig[ "disabledFunctions" ] and "fulltext-search" in modulConfig[ "disabledFunctions" ]:';
		$m.__track_lines__[570] = 'actions.list.py, line 570:\n    if not "views" in modulConfig.keys() or not modulConfig["views"]:';
		$m.__track_lines__[572] = 'actions.list.py, line 572:\n    self["disabled"] = True';
		$m.__track_lines__[574] = 'actions.list.py, line 574:\n    def onClick(self, sender=None):';
		$m.__track_lines__[575] = 'actions.list.py, line 575:\n    if isinstance(self.parent().parent().sideBar.getWidget(), FilterSelector):';
		$m.__track_lines__[576] = 'actions.list.py, line 576:\n    self.parent().parent().sideBar.setWidget(None)';
		$m.__track_lines__[577] = 'actions.list.py, line 577:\n    self.filterSelector = None';
		$m.__track_lines__[579] = 'actions.list.py, line 579:\n    self.filterSelector = FilterSelector(self.parent().parent().modul)';
		$m.__track_lines__[580] = 'actions.list.py, line 580:\n    self.parent().parent().sideBar.setWidget(self.filterSelector)';
		$m.__track_lines__[583] = 'actions.list.py, line 582:\n    @staticmethod ... def isSuitableFor( modul, handler, actionName ):';
		$m.__track_lines__[584] = 'actions.list.py, line 584:\n    if modul is None:';
		$m.__track_lines__[585] = 'actions.list.py, line 585:\n    return( False )';
		$m.__track_lines__[586] = 'actions.list.py, line 586:\n    correctAction = actionName=="selectfilter"';
		$m.__track_lines__[587] = 'actions.list.py, line 587:\n    correctHandler = handler == "list" or handler.startswith("list.")';
		$m.__track_lines__[588] = 'actions.list.py, line 588:\n    hasAccess = conf["currentUser"] and ("root" in conf["currentUser"]["access"] or modul+"-view" in conf["currentUser"]["access"])';
		$m.__track_lines__[589] = 'actions.list.py, line 589:\n    isDisabled = modul is not None and "disabledFunctions" in conf["modules"][modul].keys() and conf["modules"][modul]["disabledFunctions"] and "view" in conf["modules"][modul]["disabledFunctions"]';
		$m.__track_lines__[590] = 'actions.list.py, line 590:\n    return(  correctAction and correctHandler and hasAccess and not isDisabled )';
		$m.__track_lines__[592] = 'actions.list.py, line 592:\n    actionDelegateSelector.insert( 1, ListSelectFilterAction.isSuitableFor, ListSelectFilterAction )';
		$m.__track_lines__[594] = 'actions.list.py, line 594:\n    class RecurrentDateAction( html5.ext.Button ):';
		$m.__track_lines__[599] = 'actions.list.py, line 599:\n    def __init__(self, *args, **kwargs):';
		$m.__track_lines__[600] = 'actions.list.py, line 600:\n    super( RecurrentDateAction, self ).__init__( translate("Recurrent Events"), *args, **kwargs )';
		$m.__track_lines__[601] = 'actions.list.py, line 601:\n    self["class"] = "icon createrecurrent_small"';
		$m.__track_lines__[602] = 'actions.list.py, line 602:\n    self["disabled"]= True';
		$m.__track_lines__[603] = 'actions.list.py, line 603:\n    self.isDisabled=True';
		$m.__track_lines__[605] = 'actions.list.py, line 605:\n    def onAttach(self):';
		$m.__track_lines__[606] = 'actions.list.py, line 606:\n    super(RecurrentDateAction,self).onAttach()';
		$m.__track_lines__[607] = 'actions.list.py, line 607:\n    self.parent().parent().selectionChangedEvent.register( self )';
		$m.__track_lines__[609] = 'actions.list.py, line 609:\n    def onDetach(self):';
		$m.__track_lines__[610] = 'actions.list.py, line 610:\n    self.parent().parent().selectionChangedEvent.unregister( self )';
		$m.__track_lines__[611] = 'actions.list.py, line 611:\n    super(RecurrentDateAction,self).onDetach()';
		$m.__track_lines__[613] = 'actions.list.py, line 613:\n    def onSelectionChanged(self, table, selection ):';
		$m.__track_lines__[614] = 'actions.list.py, line 614:\n    if len(selection)>0:';
		$m.__track_lines__[615] = 'actions.list.py, line 615:\n    if self.isDisabled:';
		$m.__track_lines__[616] = 'actions.list.py, line 616:\n    self.isDisabled = False';
		$m.__track_lines__[617] = 'actions.list.py, line 617:\n    self["disabled"]= False';
		$m.__track_lines__[619] = 'actions.list.py, line 619:\n    if not self.isDisabled:';
		$m.__track_lines__[620] = 'actions.list.py, line 620:\n    self["disabled"]= True';
		$m.__track_lines__[621] = 'actions.list.py, line 621:\n    self.isDisabled = True';
		$m.__track_lines__[624] = 'actions.list.py, line 623:\n    @staticmethod ... def isSuitableFor( modul, handler, actionName ):';
		$m.__track_lines__[625] = 'actions.list.py, line 625:\n    if modul is None:';
		$m.__track_lines__[626] = 'actions.list.py, line 626:\n    return( False )';
		$m.__track_lines__[627] = 'actions.list.py, line 627:\n    correctAction = actionName=="repeatdate"';
		$m.__track_lines__[628] = 'actions.list.py, line 628:\n    correctHandler = handler == "list.calendar" or handler.startswith("list.calendar.")';
		$m.__track_lines__[629] = 'actions.list.py, line 629:\n    hasAccess = conf["currentUser"] and ("root" in conf["currentUser"]["access"] or modul+"-edit" in conf["currentUser"]["access"])';
		$m.__track_lines__[630] = 'actions.list.py, line 630:\n    isDisabled = modul is not None and "disabledFunctions" in conf["modules"][modul].keys() and conf["modules"][modul]["disabledFunctions"] and "edit" in conf["modules"][modul]["disabledFunctions"]';
		$m.__track_lines__[631] = 'actions.list.py, line 631:\n    return(  correctAction and correctHandler and hasAccess and not isDisabled )';
		$m.__track_lines__[633] = 'actions.list.py, line 633:\n    def onClick(self, sender=None):';
		$m.__track_lines__[634] = 'actions.list.py, line 634:\n    selection = self.parent().parent().getCurrentSelection()';
		$m.__track_lines__[635] = 'actions.list.py, line 635:\n    if not selection:';
		$m.__track_lines__[636] = 'actions.list.py, line 636:\n    return';
		$m.__track_lines__[637] = 'actions.list.py, line 637:\n    for s in selection:';
		$m.__track_lines__[638] = 'actions.list.py, line 638:\n    self.openEditor( s["id"] )';
		$m.__track_lines__[640] = 'actions.list.py, line 640:\n    def openEditor(self, id ):';
		$m.__track_lines__[641] = 'actions.list.py, line 641:\n    pane = Pane(translate("Recurrent Events"), closeable=True, iconClasses=["modul_%s" % self.parent().parent().modul, "apptype_list", "action_edit" ])';
		$m.__track_lines__[642] = 'actions.list.py, line 642:\n    conf["mainWindow"].stackPane( pane )';
		$m.__track_lines__[643] = 'actions.list.py, line 643:\n    edwg = RepeatDatePopup(self.parent().parent().modul, key=id)';
		$m.__track_lines__[644] = 'actions.list.py, line 644:\n    pane.addWidget( edwg )';
		$m.__track_lines__[645] = 'actions.list.py, line 645:\n    pane.focus()';
		$m.__track_lines__[647] = 'actions.list.py, line 647:\n    def resetLoadingState(self):';
		$m.__track_lines__[648] = 'actions.list.py, line 648:\n    pass';
		$m.__track_lines__[650] = 'actions.list.py, line 650:\n    actionDelegateSelector.insert( 1, RecurrentDateAction.isSuitableFor, RecurrentDateAction )';
		$m.__track_lines__[654] = 'actions.list.py, line 654:\n    class CreateRecurrentAction( html5.ext.Button ):';
		$m.__track_lines__[655] = 'actions.list.py, line 655:\n    def __init__(self, *args, **kwargs):';
		$m.__track_lines__[656] = 'actions.list.py, line 656:\n    super(CreateRecurrentAction, self ).__init__( translate("Save-Close"), *args, **kwargs )';
		$m.__track_lines__[657] = 'actions.list.py, line 657:\n    self["class"] = "icon save close"';
		$m.__track_lines__[660] = 'actions.list.py, line 659:\n    @staticmethod ... def isSuitableFor( modul, handler, actionName ):';
		$m.__track_lines__[661] = 'actions.list.py, line 661:\n    return( actionName=="create.recurrent" )';
		$m.__track_lines__[663] = 'actions.list.py, line 663:\n    def onClick(self, sender=None):';
		$m.__track_lines__[664] = 'actions.list.py, line 664:\n    self["class"].append("is_loading")';
		$m.__track_lines__[665] = 'actions.list.py, line 665:\n    self.parent().parent().doSave(closeOnSuccess=True)';
		$m.__track_lines__[667] = 'actions.list.py, line 667:\n    actionDelegateSelector.insert( 1, CreateRecurrentAction.isSuitableFor, CreateRecurrentAction)';
		$m.__track_lines__[670] = 'actions.list.py, line 670:\n    class CsvExportAction( html5.ext.Button ):';
		$m.__track_lines__[671] = 'actions.list.py, line 671:\n    def __init__(self, *args, **kwargs):';
		$m.__track_lines__[672] = 'actions.list.py, line 672:\n    super(CsvExportAction, self ).__init__( translate("Export Csv"), *args, **kwargs )';
		$m.__track_lines__[673] = 'actions.list.py, line 673:\n    self["class"] = "icon download"';
		$m.__track_lines__[676] = 'actions.list.py, line 675:\n    @staticmethod ... def isSuitableFor( modul, handler, actionName ):';
		$m.__track_lines__[677] = 'actions.list.py, line 677:\n    return actionName=="exportcsv" and (handler == "list" or handler.startswith("list."))';
		$m.__track_lines__[679] = 'actions.list.py, line 679:\n    def onClick(self, sender=None):';
		$m.__track_lines__[680] = 'actions.list.py, line 680:\n    pane = Pane(translate("Csv Exporter"), closeable=True, iconClasses=["modul_%s" % self.parent().parent().modul, "apptype_list", "exportcsv" ])';
		$m.__track_lines__[681] = 'actions.list.py, line 681:\n    conf["mainWindow"].stackPane( pane )';
		$m.__track_lines__[682] = 'actions.list.py, line 682:\n    edwg = CsvExport(self.parent().parent())';
		$m.__track_lines__[683] = 'actions.list.py, line 683:\n    pane.addWidget( edwg )';
		$m.__track_lines__[684] = 'actions.list.py, line 684:\n    pane.focus()';
		$m.__track_lines__[686] = 'actions.list.py, line 686:\n    actionDelegateSelector.insert( 1, CsvExportAction.isSuitableFor, CsvExportAction)';
		$m.__track_lines__[689] = 'actions.list.py, line 689:\n    class SelectAllAction(html5.ext.Button):';
		$m.__track_lines__[690] = 'actions.list.py, line 690:\n    def __init__(self, *args, **kwargs):';
		$m.__track_lines__[691] = 'actions.list.py, line 691:\n    super(SelectAllAction, self ).__init__(translate("Select all"), *args, **kwargs)';
		$m.__track_lines__[692] = 'actions.list.py, line 692:\n    self["class"] = "icon selectall"';
		$m.__track_lines__[693] = 'actions.list.py, line 693:\n    self["disabled"] = self.isDisabled = True';
		$m.__track_lines__[696] = 'actions.list.py, line 695:\n    @staticmethod ... def isSuitableFor(modul, handler, actionName):';
		$m.__track_lines__[697] = 'actions.list.py, line 697:\n    return actionName == "selectall" and (handler == "list" or handler.startswith("list."))';
		$m.__track_lines__[699] = 'actions.list.py, line 699:\n    def onClick(self, sender=None):';
		$m.__track_lines__[700] = 'actions.list.py, line 700:\n    cnt = self.parent().parent().table.table.selectAll()';
		$m.__track_lines__[701] = 'actions.list.py, line 701:\n    conf["mainWindow"].log("info", translate(u"{items} items had been selected", items=cnt))';
		$m.__track_lines__[703] = 'actions.list.py, line 703:\n    def onAttach(self):';
		$m.__track_lines__[704] = 'actions.list.py, line 704:\n    super(SelectAllAction,self).onAttach()';
		$m.__track_lines__[705] = 'actions.list.py, line 705:\n    self.parent().parent().tableChangedEvent.register( self )';
		$m.__track_lines__[707] = 'actions.list.py, line 707:\n    def onDetach(self):';
		$m.__track_lines__[708] = 'actions.list.py, line 708:\n    self.parent().parent().tableChangedEvent.unregister( self )';
		$m.__track_lines__[709] = 'actions.list.py, line 709:\n    super(SelectAllAction,self).onDetach()';
		$m.__track_lines__[711] = 'actions.list.py, line 711:\n    def onTableChanged(self, table, count):';
		$m.__track_lines__[712] = 'actions.list.py, line 712:\n    if count > 0:';
		$m.__track_lines__[713] = 'actions.list.py, line 713:\n    self["disabled"] = self.isDisabled = False';
		$m.__track_lines__[715] = 'actions.list.py, line 715:\n    self["disabled"] = self.isDisabled = True';
		$m.__track_lines__[717] = 'actions.list.py, line 717:\n    actionDelegateSelector.insert(1, SelectAllAction.isSuitableFor, SelectAllAction)';
		$m.__track_lines__[720] = 'actions.list.py, line 720:\n    class UnSelectAllAction(html5.ext.Button):';
		$m.__track_lines__[721] = 'actions.list.py, line 721:\n    def __init__(self, *args, **kwargs):';
		$m.__track_lines__[722] = 'actions.list.py, line 722:\n    super(UnSelectAllAction, self ).__init__(translate("Unselect all"), *args, **kwargs)';
		$m.__track_lines__[723] = 'actions.list.py, line 723:\n    self["class"] = "icon unselectall"';
		$m.__track_lines__[724] = 'actions.list.py, line 724:\n    self["disabled"] = self.isDisabled = True';
		$m.__track_lines__[727] = 'actions.list.py, line 726:\n    @staticmethod ... def isSuitableFor(modul, handler, actionName):';
		$m.__track_lines__[728] = 'actions.list.py, line 728:\n    return actionName == "unselectall" and (handler == "list" or handler.startswith("list."))';
		$m.__track_lines__[730] = 'actions.list.py, line 730:\n    def onClick(self, sender=None):';
		$m.__track_lines__[731] = 'actions.list.py, line 731:\n    cnt = self.parent().parent().table.table.unSelectAll()';
		$m.__track_lines__[732] = 'actions.list.py, line 732:\n    conf["mainWindow"].log("info", translate(u"{items} items had been unselected", items=cnt))';
		$m.__track_lines__[734] = 'actions.list.py, line 734:\n    def onAttach(self):';
		$m.__track_lines__[735] = 'actions.list.py, line 735:\n    super(UnSelectAllAction,self).onAttach()';
		$m.__track_lines__[736] = 'actions.list.py, line 736:\n    self.parent().parent().tableChangedEvent.register( self )';
		$m.__track_lines__[738] = 'actions.list.py, line 738:\n    def onDetach(self):';
		$m.__track_lines__[739] = 'actions.list.py, line 739:\n    self.parent().parent().tableChangedEvent.unregister( self )';
		$m.__track_lines__[740] = 'actions.list.py, line 740:\n    super(UnSelectAllAction,self).onDetach()';
		$m.__track_lines__[742] = 'actions.list.py, line 742:\n    def onTableChanged(self, table, count):';
		$m.__track_lines__[743] = 'actions.list.py, line 743:\n    if count > 0:';
		$m.__track_lines__[744] = 'actions.list.py, line 744:\n    self["disabled"] = self.isDisabled = False';
		$m.__track_lines__[746] = 'actions.list.py, line 746:\n    self["disabled"] = self.isDisabled = True';
		$m.__track_lines__[748] = 'actions.list.py, line 748:\n    actionDelegateSelector.insert(1, UnSelectAllAction.isSuitableFor, UnSelectAllAction)';
		$m.__track_lines__[750] = 'actions.list.py, line 750:\n    class SelectInvertAction(html5.ext.Button):';
		$m.__track_lines__[751] = 'actions.list.py, line 751:\n    def __init__(self, *args, **kwargs):';
		$m.__track_lines__[752] = 'actions.list.py, line 752:\n    super(SelectInvertAction, self ).__init__(translate("Invert selection"), *args, **kwargs)';
		$m.__track_lines__[753] = 'actions.list.py, line 753:\n    self["class"] = "icon selectinvert"';
		$m.__track_lines__[754] = 'actions.list.py, line 754:\n    self["disabled"] = self.isDisabled = True';
		$m.__track_lines__[757] = 'actions.list.py, line 756:\n    @staticmethod ... def isSuitableFor(modul, handler, actionName):';
		$m.__track_lines__[758] = 'actions.list.py, line 758:\n    return actionName == "selectinvert" and (handler == "list" or handler.startswith("list."))';
		$m.__track_lines__[760] = 'actions.list.py, line 760:\n    def onClick(self, sender=None):';
		$m.__track_lines__[761] = 'actions.list.py, line 761:\n    (added, removed) = self.parent().parent().table.table.invertSelection()';
		$m.__track_lines__[763] = 'actions.list.py, line 763:\n    if removed and added:';
		$m.__track_lines__[764] = 'actions.list.py, line 764:\n    conf["mainWindow"].log("info", translate(u"{added} items selected, {removed} items deselected",';
		$m.__track_lines__[767] = 'actions.list.py, line 767:\n    conf["mainWindow"].log("info", translate(u"{items} items had been selected", items=added))';
		$m.__track_lines__[769] = 'actions.list.py, line 769:\n    conf["mainWindow"].log("info", translate(u"{items} items had been unselected", items=removed))';
		$m.__track_lines__[771] = 'actions.list.py, line 771:\n    def onAttach(self):';
		$m.__track_lines__[772] = 'actions.list.py, line 772:\n    super(SelectInvertAction,self).onAttach()';
		$m.__track_lines__[773] = 'actions.list.py, line 773:\n    self.parent().parent().tableChangedEvent.register( self )';
		$m.__track_lines__[775] = 'actions.list.py, line 775:\n    def onDetach(self):';
		$m.__track_lines__[776] = 'actions.list.py, line 776:\n    self.parent().parent().tableChangedEvent.unregister( self )';
		$m.__track_lines__[777] = 'actions.list.py, line 777:\n    super(SelectInvertAction,self).onDetach()';
		$m.__track_lines__[779] = 'actions.list.py, line 779:\n    def onTableChanged(self, table, count):';
		$m.__track_lines__[780] = 'actions.list.py, line 780:\n    if count > 0:';
		$m.__track_lines__[781] = 'actions.list.py, line 781:\n    self["disabled"] = self.isDisabled = False';
		$m.__track_lines__[783] = 'actions.list.py, line 783:\n    self["disabled"] = self.isDisabled = True';
		$m.__track_lines__[785] = 'actions.list.py, line 785:\n    actionDelegateSelector.insert(1, SelectInvertAction.isSuitableFor, SelectInvertAction)';

		var $constant_int_0 = new $p['int'](0);
		var $constant_int_1 = new $p['int'](1);
		var $constant_int_2 = new $p['int'](2);
		$pyjs['track']['module']='actions.list';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=1;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['html5'] = $p['___import___']('html5', 'actions');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['utils'] = $p['___import___']('utils', 'actions');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=2;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['NetworkService'] = $p['___import___']('network.NetworkService', 'actions', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=3;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['actionDelegateSelector'] = $p['___import___']('priorityqueue.actionDelegateSelector', 'actions', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=4;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['EditWidget'] = $p['___import___']('widgets.edit.EditWidget', 'actions', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=5;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['conf'] = $p['___import___']('config.conf', 'actions', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=6;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['Pane'] = $p['___import___']('pane.Pane', 'actions', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=7;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['RepeatDatePopup'] = $p['___import___']('widgets.repeatdate.RepeatDatePopup', 'actions', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=8;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['CsvExport'] = $p['___import___']('widgets.csvexport.CsvExport', 'actions', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=9;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['DataTable'] = $p['___import___']('widgets.table.DataTable', 'actions', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=10;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['Preview'] = $p['___import___']('widgets.preview.Preview', 'actions', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=11;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['InternalPreview'] = $p['___import___']('sidebarwidgets.internalpreview.InternalPreview', 'actions', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=12;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['FilterSelector'] = $p['___import___']('sidebarwidgets.filterselector.FilterSelector', 'actions', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=13;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['translate'] = $p['___import___']('i18n.translate', 'actions', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=15;
		$m['EditPane'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = 'a2900a4260b62cfdf048ff666c5dbc47';
			$pyjs['track']['lineno']=16;
			$pyjs['track']['lineno']=15;
			var $bases = new Array($m['Pane']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('EditPane', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=18;
		$pyjs['track']['lineno']=21;
		$m['AddAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = 'ef05e4aaa745eb85143b283a236501e7';
			$pyjs['track']['lineno']=25;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'ef05e4aaa745eb85143b283a236501e7') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':25};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=25;
				$pyjs['track']['lineno']=26;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['AddAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Add');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})();
				$pyjs['track']['lineno']=27;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon add list');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=30;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var $and9,correctHandler,isDisabled,$and8,$or4,$or1,$or3,$or2,$and1,$and2,$and3,$and4,$and5,$and6,$and7,$and10,hasAccess,correctAction,$add2,$add1;
				$pyjs['track']={'module':'actions.list', 'lineno':30};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=30;
				$pyjs['track']['lineno']=31;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](modul, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})()) {
					$pyjs['track']['lineno']=32;
					$pyjs['track']['lineno']=32;
					var $pyjs__ret = false;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=33;
				correctAction = $p['op_eq'](actionName, 'add');
				$pyjs['track']['lineno']=34;
				correctHandler = ($p['bool']($or1=$p['op_eq'](handler, 'list'))?$or1:(function(){try{try{$pyjs['in_try_except'] += 1;
				return handler['startswith']('list.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})());
				$pyjs['track']['lineno']=35;
				hasAccess = ($p['bool']($and1=$m['conf']['__getitem__']('currentUser'))?($p['bool']($or3=$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']('root'))?$or3:$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']($p['__op_add']($add1=modul,$add2='-add'))):$and1);
				$pyjs['track']['lineno']=36;
				isDisabled = ($p['bool']($and3=!$p['op_is'](modul, null))?($p['bool']($and4=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['__getitem__'](modul)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})()['__contains__']('disabledFunctions'))?($p['bool']($and5=$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions'))?$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions')['__contains__']('add'):$and5):$and4):$and3);
				$pyjs['track']['lineno']=37;
				$pyjs['track']['lineno']=37;
				var $pyjs__ret = ($p['bool']($and7=correctAction)?($p['bool']($and8=correctHandler)?($p['bool']($and9=hasAccess)?!$p['bool'](isDisabled):$and9):$and8):$and7);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=39;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'ef05e4aaa745eb85143b283a236501e7') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];
				var edwg,pane;
				$pyjs['track']={'module':'actions.list', 'lineno':39};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=39;
				$pyjs['track']['lineno']=40;
				pane = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['EditPane'], null, null, [{'closeable':true, 'iconClasses':(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('modul_%s', $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})(), 'modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})(), 'apptype_list', 'action_add']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})()}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Add');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})();
				$pyjs['track']['lineno']=41;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['stackPane'](pane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_14_err){if (!$p['isinstance']($pyjs_dbg_14_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_14_err);}throw $pyjs_dbg_14_err;
}})();
				$pyjs['track']['lineno']=42;
				edwg = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EditWidget']($p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_16_err){if (!$p['isinstance']($pyjs_dbg_16_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_16_err);}throw $pyjs_dbg_16_err;
}})(), 'modul'), $p['getattr']($m['EditWidget'], 'appList'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_17_err){if (!$p['isinstance']($pyjs_dbg_17_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_17_err);}throw $pyjs_dbg_17_err;
}})();
				$pyjs['track']['lineno']=43;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return pane['addWidget'](edwg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})();
				$pyjs['track']['lineno']=44;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return pane['focus']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_19_err){if (!$p['isinstance']($pyjs_dbg_19_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_19_err);}throw $pyjs_dbg_19_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=46;
			$method = $pyjs__bind_method2('resetLoadingState', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'ef05e4aaa745eb85143b283a236501e7') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':46};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=46;
				$pyjs['track']['lineno']=47;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['resetLoadingState'] = $method;
			$pyjs['track']['lineno']=21;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('AddAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=49;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['AddAction'], 'isSuitableFor'), $m['AddAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})();
		$pyjs['track']['lineno']=52;
		$m['EditAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = '82c3349344a66d3d4014970fde988077';
			$pyjs['track']['lineno']=57;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '82c3349344a66d3d4014970fde988077') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':57};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=57;
				$pyjs['track']['lineno']=58;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['EditAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Edit');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})();
				$pyjs['track']['lineno']=59;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon edit');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})();
				$pyjs['track']['lineno']=60;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})();
				$pyjs['track']['lineno']=61;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', true) : $p['setattr'](self, 'isDisabled', true); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=63;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '82c3349344a66d3d4014970fde988077') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':63};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=63;
				$pyjs['track']['lineno']=64;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['EditAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_26_err){if (!$p['isinstance']($pyjs_dbg_26_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_26_err);}throw $pyjs_dbg_26_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_27_err){if (!$p['isinstance']($pyjs_dbg_27_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_27_err);}throw $pyjs_dbg_27_err;
}})();
				$pyjs['track']['lineno']=65;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_28_err){if (!$p['isinstance']($pyjs_dbg_28_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_28_err);}throw $pyjs_dbg_28_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_29_err){if (!$p['isinstance']($pyjs_dbg_29_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_29_err);}throw $pyjs_dbg_29_err;
}})()['selectionChangedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_30_err){if (!$p['isinstance']($pyjs_dbg_30_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_30_err);}throw $pyjs_dbg_30_err;
}})();
				$pyjs['track']['lineno']=66;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_31_err){if (!$p['isinstance']($pyjs_dbg_31_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_31_err);}throw $pyjs_dbg_31_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_32_err){if (!$p['isinstance']($pyjs_dbg_32_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_32_err);}throw $pyjs_dbg_32_err;
}})()['selectionActivatedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_33_err){if (!$p['isinstance']($pyjs_dbg_33_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_33_err);}throw $pyjs_dbg_33_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=68;
			$method = $pyjs__bind_method2('onDetach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '82c3349344a66d3d4014970fde988077') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':68};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=68;
				$pyjs['track']['lineno']=69;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_34_err){if (!$p['isinstance']($pyjs_dbg_34_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_34_err);}throw $pyjs_dbg_34_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})()['selectionChangedEvent']['unregister'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_36_err){if (!$p['isinstance']($pyjs_dbg_36_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_36_err);}throw $pyjs_dbg_36_err;
}})();
				$pyjs['track']['lineno']=70;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_37_err){if (!$p['isinstance']($pyjs_dbg_37_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_37_err);}throw $pyjs_dbg_37_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_38_err){if (!$p['isinstance']($pyjs_dbg_38_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_38_err);}throw $pyjs_dbg_38_err;
}})()['selectionActivatedEvent']['unregister'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_39_err){if (!$p['isinstance']($pyjs_dbg_39_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_39_err);}throw $pyjs_dbg_39_err;
}})();
				$pyjs['track']['lineno']=71;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['EditAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_40_err){if (!$p['isinstance']($pyjs_dbg_40_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_40_err);}throw $pyjs_dbg_40_err;
}})()['onDetach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_41_err){if (!$p['isinstance']($pyjs_dbg_41_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_41_err);}throw $pyjs_dbg_41_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onDetach'] = $method;
			$pyjs['track']['lineno']=73;
			$method = $pyjs__bind_method2('onSelectionChanged', function(table, selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					selection = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '82c3349344a66d3d4014970fde988077') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':73};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=73;
				$pyjs['track']['lineno']=74;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](selection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_42_err){if (!$p['isinstance']($pyjs_dbg_42_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_42_err);}throw $pyjs_dbg_42_err;
}})(), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_43_err){if (!$p['isinstance']($pyjs_dbg_43_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_43_err);}throw $pyjs_dbg_43_err;
}})()) {
					$pyjs['track']['lineno']=75;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'isDisabled'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_44_err){if (!$p['isinstance']($pyjs_dbg_44_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_44_err);}throw $pyjs_dbg_44_err;
}})()) {
						$pyjs['track']['lineno']=76;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', false) : $p['setattr'](self, 'isDisabled', false); 
					}
					$pyjs['track']['lineno']=77;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', false);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_45_err){if (!$p['isinstance']($pyjs_dbg_45_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_45_err);}throw $pyjs_dbg_45_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=79;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool']($p['getattr'](self, 'isDisabled')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_46_err){if (!$p['isinstance']($pyjs_dbg_46_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_46_err);}throw $pyjs_dbg_46_err;
}})()) {
						$pyjs['track']['lineno']=80;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})();
						$pyjs['track']['lineno']=81;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', true) : $p['setattr'](self, 'isDisabled', true); 
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['selection']]);
			$cls_definition['onSelectionChanged'] = $method;
			$pyjs['track']['lineno']=83;
			$method = $pyjs__bind_method2('onSelectionActivated', function(table, selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					selection = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '82c3349344a66d3d4014970fde988077') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and12,$and11;
				$pyjs['track']={'module':'actions.list', 'lineno':83};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=83;
				$pyjs['track']['lineno']=84;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and11=!$p['bool']($p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_48_err){if (!$p['isinstance']($pyjs_dbg_48_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_48_err);}throw $pyjs_dbg_48_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_49_err){if (!$p['isinstance']($pyjs_dbg_49_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_49_err);}throw $pyjs_dbg_49_err;
}})(), 'isSelector')))?$p['op_eq']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](selection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_50_err){if (!$p['isinstance']($pyjs_dbg_50_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_50_err);}throw $pyjs_dbg_50_err;
}})(), $constant_int_1):$and11));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_51_err){if (!$p['isinstance']($pyjs_dbg_51_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_51_err);}throw $pyjs_dbg_51_err;
}})()) {
					$pyjs['track']['lineno']=85;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['openEditor'](selection['__getitem__']($constant_int_0)['__getitem__']('id'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_52_err){if (!$p['isinstance']($pyjs_dbg_52_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_52_err);}throw $pyjs_dbg_52_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['selection']]);
			$cls_definition['onSelectionActivated'] = $method;
			$pyjs['track']['lineno']=88;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var $and22,$and21,$and20,correctHandler,isDisabled,$or5,$or7,$or6,correctAction,$or8,$and13,$and16,$and17,$and14,$and15,$and18,$and19,hasAccess,$add3,$add4;
				$pyjs['track']={'module':'actions.list', 'lineno':88};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=88;
				$pyjs['track']['lineno']=89;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](modul, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_53_err){if (!$p['isinstance']($pyjs_dbg_53_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_53_err);}throw $pyjs_dbg_53_err;
}})()) {
					$pyjs['track']['lineno']=90;
					$pyjs['track']['lineno']=90;
					var $pyjs__ret = false;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=91;
				correctAction = $p['op_eq'](actionName, 'edit');
				$pyjs['track']['lineno']=92;
				correctHandler = ($p['bool']($or5=$p['op_eq'](handler, 'list'))?$or5:(function(){try{try{$pyjs['in_try_except'] += 1;
				return handler['startswith']('list.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_54_err){if (!$p['isinstance']($pyjs_dbg_54_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_54_err);}throw $pyjs_dbg_54_err;
}})());
				$pyjs['track']['lineno']=93;
				hasAccess = ($p['bool']($and13=$m['conf']['__getitem__']('currentUser'))?($p['bool']($or7=$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']('root'))?$or7:$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']($p['__op_add']($add3=modul,$add4='-edit'))):$and13);
				$pyjs['track']['lineno']=94;
				isDisabled = ($p['bool']($and15=!$p['op_is'](modul, null))?($p['bool']($and16=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['__getitem__'](modul)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_55_err){if (!$p['isinstance']($pyjs_dbg_55_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_55_err);}throw $pyjs_dbg_55_err;
}})()['__contains__']('disabledFunctions'))?($p['bool']($and17=$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions'))?$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions')['__contains__']('edit'):$and17):$and16):$and15);
				$pyjs['track']['lineno']=95;
				$pyjs['track']['lineno']=95;
				var $pyjs__ret = ($p['bool']($and19=correctAction)?($p['bool']($and20=correctHandler)?($p['bool']($and21=hasAccess)?!$p['bool'](isDisabled):$and21):$and20):$and19);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=97;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '82c3349344a66d3d4014970fde988077') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];
				var selection,$iter1_nextval,$iter1_idx,$iter1_iter,s,$iter1_array,$pyjs__trackstack_size_1,$iter1_type;
				$pyjs['track']={'module':'actions.list', 'lineno':97};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=97;
				$pyjs['track']['lineno']=98;
				selection = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_56_err){if (!$p['isinstance']($pyjs_dbg_56_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_56_err);}throw $pyjs_dbg_56_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_57_err){if (!$p['isinstance']($pyjs_dbg_57_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_57_err);}throw $pyjs_dbg_57_err;
}})()['getCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_58_err){if (!$p['isinstance']($pyjs_dbg_58_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_58_err);}throw $pyjs_dbg_58_err;
}})();
				$pyjs['track']['lineno']=99;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool'](selection));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_59_err){if (!$p['isinstance']($pyjs_dbg_59_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_59_err);}throw $pyjs_dbg_59_err;
}})()) {
					$pyjs['track']['lineno']=100;
					$pyjs['track']['lineno']=100;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=101;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter1_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return selection;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_60_err){if (!$p['isinstance']($pyjs_dbg_60_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_60_err);}throw $pyjs_dbg_60_err;
}})();
				$iter1_nextval=$p['__iter_prepare']($iter1_iter,false);
				while (typeof($p['__wrapped_next']($iter1_nextval)['$nextval']) != 'undefined') {
					s = $iter1_nextval['$nextval'];
					$pyjs['track']['lineno']=102;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['openEditor'](s['__getitem__']('id'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_61_err){if (!$p['isinstance']($pyjs_dbg_61_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_61_err);}throw $pyjs_dbg_61_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='actions.list';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=104;
			$method = $pyjs__bind_method2('openEditor', function(id) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					id = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '82c3349344a66d3d4014970fde988077') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var edwg,pane;
				$pyjs['track']={'module':'actions.list', 'lineno':104};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=104;
				$pyjs['track']['lineno']=105;
				pane = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['Pane'], null, null, [{'closeable':true, 'iconClasses':(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('modul_%s', $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_63_err){if (!$p['isinstance']($pyjs_dbg_63_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_63_err);}throw $pyjs_dbg_63_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})(), 'modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_65_err){if (!$p['isinstance']($pyjs_dbg_65_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_65_err);}throw $pyjs_dbg_65_err;
}})(), 'apptype_list', 'action_edit']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_66_err){if (!$p['isinstance']($pyjs_dbg_66_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_66_err);}throw $pyjs_dbg_66_err;
}})()}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Edit');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_62_err){if (!$p['isinstance']($pyjs_dbg_62_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_62_err);}throw $pyjs_dbg_62_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})();
				$pyjs['track']['lineno']=106;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['conf']['__getitem__']('mainWindow'), 'stackPane', null, null, [{'focus':true}, pane]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_68_err){if (!$p['isinstance']($pyjs_dbg_68_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_68_err);}throw $pyjs_dbg_68_err;
}})();
				$pyjs['track']['lineno']=107;
				edwg = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['EditWidget'], null, null, [{'key':id}, $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_69_err){if (!$p['isinstance']($pyjs_dbg_69_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_69_err);}throw $pyjs_dbg_69_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_70_err){if (!$p['isinstance']($pyjs_dbg_70_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_70_err);}throw $pyjs_dbg_70_err;
}})(), 'modul'), $p['getattr']($m['EditWidget'], 'appList')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_71_err){if (!$p['isinstance']($pyjs_dbg_71_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_71_err);}throw $pyjs_dbg_71_err;
}})();
				$pyjs['track']['lineno']=108;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return pane['addWidget'](edwg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_72_err){if (!$p['isinstance']($pyjs_dbg_72_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_72_err);}throw $pyjs_dbg_72_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['id']]);
			$cls_definition['openEditor'] = $method;
			$pyjs['track']['lineno']=110;
			$method = $pyjs__bind_method2('resetLoadingState', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '82c3349344a66d3d4014970fde988077') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':110};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=110;
				$pyjs['track']['lineno']=111;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['resetLoadingState'] = $method;
			$pyjs['track']['lineno']=52;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('EditAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=113;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['EditAction'], 'isSuitableFor'), $m['EditAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_73_err){if (!$p['isinstance']($pyjs_dbg_73_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_73_err);}throw $pyjs_dbg_73_err;
}})();
		$pyjs['track']['lineno']=116;
		$m['CloneAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = 'e5c8fca6ef8eb8e90be4e8c97bae11ed';
			$pyjs['track']['lineno']=121;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e5c8fca6ef8eb8e90be4e8c97bae11ed') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':121};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=121;
				$pyjs['track']['lineno']=122;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['CloneAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_74_err){if (!$p['isinstance']($pyjs_dbg_74_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_74_err);}throw $pyjs_dbg_74_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Clone');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})();
				$pyjs['track']['lineno']=123;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon clone');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_77_err){if (!$p['isinstance']($pyjs_dbg_77_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_77_err);}throw $pyjs_dbg_77_err;
}})();
				$pyjs['track']['lineno']=124;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_78_err){if (!$p['isinstance']($pyjs_dbg_78_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_78_err);}throw $pyjs_dbg_78_err;
}})();
				$pyjs['track']['lineno']=125;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', true) : $p['setattr'](self, 'isDisabled', true); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=127;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e5c8fca6ef8eb8e90be4e8c97bae11ed') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':127};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=127;
				$pyjs['track']['lineno']=128;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['CloneAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_79_err){if (!$p['isinstance']($pyjs_dbg_79_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_79_err);}throw $pyjs_dbg_79_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_80_err){if (!$p['isinstance']($pyjs_dbg_80_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_80_err);}throw $pyjs_dbg_80_err;
}})();
				$pyjs['track']['lineno']=129;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_81_err){if (!$p['isinstance']($pyjs_dbg_81_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_81_err);}throw $pyjs_dbg_81_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_82_err){if (!$p['isinstance']($pyjs_dbg_82_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_82_err);}throw $pyjs_dbg_82_err;
}})()['selectionChangedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_83_err){if (!$p['isinstance']($pyjs_dbg_83_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_83_err);}throw $pyjs_dbg_83_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=131;
			$method = $pyjs__bind_method2('onDetach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e5c8fca6ef8eb8e90be4e8c97bae11ed') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':131};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=131;
				$pyjs['track']['lineno']=132;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_84_err){if (!$p['isinstance']($pyjs_dbg_84_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_84_err);}throw $pyjs_dbg_84_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})()['selectionChangedEvent']['unregister'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})();
				$pyjs['track']['lineno']=133;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['CloneAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})()['onDetach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onDetach'] = $method;
			$pyjs['track']['lineno']=135;
			$method = $pyjs__bind_method2('onSelectionChanged', function(table, selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					selection = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e5c8fca6ef8eb8e90be4e8c97bae11ed') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':135};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=135;
				$pyjs['track']['lineno']=136;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](selection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_89_err){if (!$p['isinstance']($pyjs_dbg_89_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_89_err);}throw $pyjs_dbg_89_err;
}})(), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})()) {
					$pyjs['track']['lineno']=137;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'isDisabled'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_91_err){if (!$p['isinstance']($pyjs_dbg_91_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_91_err);}throw $pyjs_dbg_91_err;
}})()) {
						$pyjs['track']['lineno']=138;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', false) : $p['setattr'](self, 'isDisabled', false); 
					}
					$pyjs['track']['lineno']=139;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', false);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_92_err){if (!$p['isinstance']($pyjs_dbg_92_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_92_err);}throw $pyjs_dbg_92_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=141;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool']($p['getattr'](self, 'isDisabled')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_93_err){if (!$p['isinstance']($pyjs_dbg_93_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_93_err);}throw $pyjs_dbg_93_err;
}})()) {
						$pyjs['track']['lineno']=142;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_94_err){if (!$p['isinstance']($pyjs_dbg_94_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_94_err);}throw $pyjs_dbg_94_err;
}})();
						$pyjs['track']['lineno']=143;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', true) : $p['setattr'](self, 'isDisabled', true); 
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['selection']]);
			$cls_definition['onSelectionChanged'] = $method;
			$pyjs['track']['lineno']=146;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var $and29,$and28,$and23,$and27,$and26,$and25,$and24,correctHandler,isDisabled,$add6,correctAction,$or12,$or9,$and30,$and31,$and32,hasAccess,$or11,$or10,$add5;
				$pyjs['track']={'module':'actions.list', 'lineno':146};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=146;
				$pyjs['track']['lineno']=147;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](modul, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_95_err){if (!$p['isinstance']($pyjs_dbg_95_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_95_err);}throw $pyjs_dbg_95_err;
}})()) {
					$pyjs['track']['lineno']=148;
					$pyjs['track']['lineno']=148;
					var $pyjs__ret = false;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=149;
				correctAction = $p['op_eq'](actionName, 'clone');
				$pyjs['track']['lineno']=150;
				correctHandler = ($p['bool']($or9=$p['op_eq'](handler, 'list'))?$or9:(function(){try{try{$pyjs['in_try_except'] += 1;
				return handler['startswith']('list.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_96_err){if (!$p['isinstance']($pyjs_dbg_96_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_96_err);}throw $pyjs_dbg_96_err;
}})());
				$pyjs['track']['lineno']=151;
				hasAccess = ($p['bool']($and23=$m['conf']['__getitem__']('currentUser'))?($p['bool']($or11=$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']('root'))?$or11:$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']($p['__op_add']($add5=modul,$add6='-edit'))):$and23);
				$pyjs['track']['lineno']=152;
				isDisabled = ($p['bool']($and25=!$p['op_is'](modul, null))?($p['bool']($and26=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['__getitem__'](modul)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_97_err){if (!$p['isinstance']($pyjs_dbg_97_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_97_err);}throw $pyjs_dbg_97_err;
}})()['__contains__']('disabledFunctions'))?($p['bool']($and27=$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions'))?$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions')['__contains__']('clone'):$and27):$and26):$and25);
				$pyjs['track']['lineno']=153;
				$pyjs['track']['lineno']=153;
				var $pyjs__ret = ($p['bool']($and29=correctAction)?($p['bool']($and30=correctHandler)?($p['bool']($and31=hasAccess)?!$p['bool'](isDisabled):$and31):$and30):$and29);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=155;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e5c8fca6ef8eb8e90be4e8c97bae11ed') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];
				var $iter2_nextval,$iter2_type,$iter2_iter,s,$iter2_idx,selection,$pyjs__trackstack_size_1,$iter2_array;
				$pyjs['track']={'module':'actions.list', 'lineno':155};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=155;
				$pyjs['track']['lineno']=156;
				selection = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_98_err){if (!$p['isinstance']($pyjs_dbg_98_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_98_err);}throw $pyjs_dbg_98_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_99_err){if (!$p['isinstance']($pyjs_dbg_99_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_99_err);}throw $pyjs_dbg_99_err;
}})()['getCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_100_err){if (!$p['isinstance']($pyjs_dbg_100_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_100_err);}throw $pyjs_dbg_100_err;
}})();
				$pyjs['track']['lineno']=157;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool'](selection));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_101_err){if (!$p['isinstance']($pyjs_dbg_101_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_101_err);}throw $pyjs_dbg_101_err;
}})()) {
					$pyjs['track']['lineno']=158;
					$pyjs['track']['lineno']=158;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=159;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter2_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return selection;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_102_err){if (!$p['isinstance']($pyjs_dbg_102_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_102_err);}throw $pyjs_dbg_102_err;
}})();
				$iter2_nextval=$p['__iter_prepare']($iter2_iter,false);
				while (typeof($p['__wrapped_next']($iter2_nextval)['$nextval']) != 'undefined') {
					s = $iter2_nextval['$nextval'];
					$pyjs['track']['lineno']=160;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['openEditor'](s['__getitem__']('id'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_103_err){if (!$p['isinstance']($pyjs_dbg_103_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_103_err);}throw $pyjs_dbg_103_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='actions.list';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=162;
			$method = $pyjs__bind_method2('openEditor', function(id) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					id = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e5c8fca6ef8eb8e90be4e8c97bae11ed') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var edwg,pane;
				$pyjs['track']={'module':'actions.list', 'lineno':162};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=162;
				$pyjs['track']['lineno']=163;
				pane = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['Pane'], null, null, [{'closeable':true, 'iconClasses':(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('modul_%s', $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_105_err){if (!$p['isinstance']($pyjs_dbg_105_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_105_err);}throw $pyjs_dbg_105_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_106_err){if (!$p['isinstance']($pyjs_dbg_106_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_106_err);}throw $pyjs_dbg_106_err;
}})(), 'modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_107_err){if (!$p['isinstance']($pyjs_dbg_107_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_107_err);}throw $pyjs_dbg_107_err;
}})(), 'apptype_list', 'action_edit']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_108_err){if (!$p['isinstance']($pyjs_dbg_108_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_108_err);}throw $pyjs_dbg_108_err;
}})()}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Clone');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_104_err){if (!$p['isinstance']($pyjs_dbg_104_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_104_err);}throw $pyjs_dbg_104_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_109_err){if (!$p['isinstance']($pyjs_dbg_109_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_109_err);}throw $pyjs_dbg_109_err;
}})();
				$pyjs['track']['lineno']=164;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['stackPane'](pane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_110_err){if (!$p['isinstance']($pyjs_dbg_110_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_110_err);}throw $pyjs_dbg_110_err;
}})();
				$pyjs['track']['lineno']=165;
				edwg = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['EditWidget'], null, null, [{'key':id, 'clone':true}, $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_111_err){if (!$p['isinstance']($pyjs_dbg_111_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_111_err);}throw $pyjs_dbg_111_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_112_err){if (!$p['isinstance']($pyjs_dbg_112_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_112_err);}throw $pyjs_dbg_112_err;
}})(), 'modul'), $p['getattr']($m['EditWidget'], 'appList')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_113_err){if (!$p['isinstance']($pyjs_dbg_113_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_113_err);}throw $pyjs_dbg_113_err;
}})();
				$pyjs['track']['lineno']=166;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return pane['addWidget'](edwg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_114_err){if (!$p['isinstance']($pyjs_dbg_114_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_114_err);}throw $pyjs_dbg_114_err;
}})();
				$pyjs['track']['lineno']=167;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return pane['focus']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_115_err){if (!$p['isinstance']($pyjs_dbg_115_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_115_err);}throw $pyjs_dbg_115_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['id']]);
			$cls_definition['openEditor'] = $method;
			$pyjs['track']['lineno']=169;
			$method = $pyjs__bind_method2('resetLoadingState', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e5c8fca6ef8eb8e90be4e8c97bae11ed') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':169};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=169;
				$pyjs['track']['lineno']=170;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['resetLoadingState'] = $method;
			$pyjs['track']['lineno']=116;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('CloneAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=172;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['CloneAction'], 'isSuitableFor'), $m['CloneAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_116_err){if (!$p['isinstance']($pyjs_dbg_116_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_116_err);}throw $pyjs_dbg_116_err;
}})();
		$pyjs['track']['lineno']=176;
		$m['DeleteAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = 'd4b0d93a6998da6414332dec008ba4b5';
			$pyjs['track']['lineno']=180;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'd4b0d93a6998da6414332dec008ba4b5') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':180};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=180;
				$pyjs['track']['lineno']=181;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['DeleteAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_117_err){if (!$p['isinstance']($pyjs_dbg_117_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_117_err);}throw $pyjs_dbg_117_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Delete');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_118_err){if (!$p['isinstance']($pyjs_dbg_118_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_118_err);}throw $pyjs_dbg_118_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_119_err){if (!$p['isinstance']($pyjs_dbg_119_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_119_err);}throw $pyjs_dbg_119_err;
}})();
				$pyjs['track']['lineno']=182;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon delete');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_120_err){if (!$p['isinstance']($pyjs_dbg_120_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_120_err);}throw $pyjs_dbg_120_err;
}})();
				$pyjs['track']['lineno']=183;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_121_err){if (!$p['isinstance']($pyjs_dbg_121_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_121_err);}throw $pyjs_dbg_121_err;
}})();
				$pyjs['track']['lineno']=184;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', true) : $p['setattr'](self, 'isDisabled', true); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=186;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'd4b0d93a6998da6414332dec008ba4b5') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':186};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=186;
				$pyjs['track']['lineno']=187;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['DeleteAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_122_err){if (!$p['isinstance']($pyjs_dbg_122_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_122_err);}throw $pyjs_dbg_122_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_123_err){if (!$p['isinstance']($pyjs_dbg_123_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_123_err);}throw $pyjs_dbg_123_err;
}})();
				$pyjs['track']['lineno']=188;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_124_err){if (!$p['isinstance']($pyjs_dbg_124_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_124_err);}throw $pyjs_dbg_124_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_125_err){if (!$p['isinstance']($pyjs_dbg_125_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_125_err);}throw $pyjs_dbg_125_err;
}})()['selectionChangedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_126_err){if (!$p['isinstance']($pyjs_dbg_126_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_126_err);}throw $pyjs_dbg_126_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=190;
			$method = $pyjs__bind_method2('onDetach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'd4b0d93a6998da6414332dec008ba4b5') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':190};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=190;
				$pyjs['track']['lineno']=191;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_127_err){if (!$p['isinstance']($pyjs_dbg_127_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_127_err);}throw $pyjs_dbg_127_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_128_err){if (!$p['isinstance']($pyjs_dbg_128_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_128_err);}throw $pyjs_dbg_128_err;
}})()['selectionChangedEvent']['unregister'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_129_err){if (!$p['isinstance']($pyjs_dbg_129_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_129_err);}throw $pyjs_dbg_129_err;
}})();
				$pyjs['track']['lineno']=192;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['DeleteAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_130_err){if (!$p['isinstance']($pyjs_dbg_130_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_130_err);}throw $pyjs_dbg_130_err;
}})()['onDetach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_131_err){if (!$p['isinstance']($pyjs_dbg_131_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_131_err);}throw $pyjs_dbg_131_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onDetach'] = $method;
			$pyjs['track']['lineno']=194;
			$method = $pyjs__bind_method2('onSelectionChanged', function(table, selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					selection = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'd4b0d93a6998da6414332dec008ba4b5') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':194};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=194;
				$pyjs['track']['lineno']=195;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](selection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_132_err){if (!$p['isinstance']($pyjs_dbg_132_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_132_err);}throw $pyjs_dbg_132_err;
}})(), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_133_err){if (!$p['isinstance']($pyjs_dbg_133_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_133_err);}throw $pyjs_dbg_133_err;
}})()) {
					$pyjs['track']['lineno']=196;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'isDisabled'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_134_err){if (!$p['isinstance']($pyjs_dbg_134_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_134_err);}throw $pyjs_dbg_134_err;
}})()) {
						$pyjs['track']['lineno']=197;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', false) : $p['setattr'](self, 'isDisabled', false); 
					}
					$pyjs['track']['lineno']=198;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', false);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_135_err){if (!$p['isinstance']($pyjs_dbg_135_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_135_err);}throw $pyjs_dbg_135_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=200;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool']($p['getattr'](self, 'isDisabled')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_136_err){if (!$p['isinstance']($pyjs_dbg_136_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_136_err);}throw $pyjs_dbg_136_err;
}})()) {
						$pyjs['track']['lineno']=201;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_137_err){if (!$p['isinstance']($pyjs_dbg_137_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_137_err);}throw $pyjs_dbg_137_err;
}})();
						$pyjs['track']['lineno']=202;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', true) : $p['setattr'](self, 'isDisabled', true); 
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['selection']]);
			$cls_definition['onSelectionChanged'] = $method;
			$pyjs['track']['lineno']=205;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var correctHandler,isDisabled,$and41,$and40,$and42,correctAction,$and38,$and39,$and34,$and35,$and36,$and37,$and33,hasAccess,$or15,$or14,$or16,$add7,$or13,$add8;
				$pyjs['track']={'module':'actions.list', 'lineno':205};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=205;
				$pyjs['track']['lineno']=206;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](modul, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_138_err){if (!$p['isinstance']($pyjs_dbg_138_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_138_err);}throw $pyjs_dbg_138_err;
}})()) {
					$pyjs['track']['lineno']=207;
					$pyjs['track']['lineno']=207;
					var $pyjs__ret = false;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=208;
				correctAction = $p['op_eq'](actionName, 'delete');
				$pyjs['track']['lineno']=209;
				correctHandler = ($p['bool']($or13=$p['op_eq'](handler, 'list'))?$or13:(function(){try{try{$pyjs['in_try_except'] += 1;
				return handler['startswith']('list.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_139_err){if (!$p['isinstance']($pyjs_dbg_139_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_139_err);}throw $pyjs_dbg_139_err;
}})());
				$pyjs['track']['lineno']=210;
				hasAccess = ($p['bool']($and33=$m['conf']['__getitem__']('currentUser'))?($p['bool']($or15=$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']('root'))?$or15:$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']($p['__op_add']($add7=modul,$add8='-delete'))):$and33);
				$pyjs['track']['lineno']=211;
				isDisabled = ($p['bool']($and35=!$p['op_is'](modul, null))?($p['bool']($and36=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['__getitem__'](modul)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_140_err){if (!$p['isinstance']($pyjs_dbg_140_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_140_err);}throw $pyjs_dbg_140_err;
}})()['__contains__']('disabledFunctions'))?($p['bool']($and37=$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions'))?$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions')['__contains__']('delete'):$and37):$and36):$and35);
				$pyjs['track']['lineno']=212;
				$pyjs['track']['lineno']=212;
				var $pyjs__ret = ($p['bool']($and39=correctAction)?($p['bool']($and40=correctHandler)?($p['bool']($and41=hasAccess)?!$p['bool'](isDisabled):$and41):$and40):$and39);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=215;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'd4b0d93a6998da6414332dec008ba4b5') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];
				var selection,d;
				$pyjs['track']={'module':'actions.list', 'lineno':215};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=215;
				$pyjs['track']['lineno']=216;
				selection = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_141_err){if (!$p['isinstance']($pyjs_dbg_141_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_141_err);}throw $pyjs_dbg_141_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_142_err){if (!$p['isinstance']($pyjs_dbg_142_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_142_err);}throw $pyjs_dbg_142_err;
}})()['getCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_143_err){if (!$p['isinstance']($pyjs_dbg_143_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_143_err);}throw $pyjs_dbg_143_err;
}})();
				$pyjs['track']['lineno']=217;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool'](selection));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_144_err){if (!$p['isinstance']($pyjs_dbg_144_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_144_err);}throw $pyjs_dbg_144_err;
}})()) {
					$pyjs['track']['lineno']=218;
					$pyjs['track']['lineno']=218;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=219;
				d = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['html5']['ext'], 'YesNoDialog', null, null, [{'title':(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Delete them?');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_147_err){if (!$p['isinstance']($pyjs_dbg_147_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_147_err);}throw $pyjs_dbg_147_err;
}})(), 'yesCallback':$p['getattr'](self, 'doDelete'), 'yesLabel':(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Delete');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_148_err){if (!$p['isinstance']($pyjs_dbg_148_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_148_err);}throw $pyjs_dbg_148_err;
}})(), 'noLabel':(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Keep');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_149_err){if (!$p['isinstance']($pyjs_dbg_149_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_149_err);}throw $pyjs_dbg_149_err;
}})()}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['translate'], null, null, [{'amt':(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](selection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_145_err){if (!$p['isinstance']($pyjs_dbg_145_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_145_err);}throw $pyjs_dbg_145_err;
}})()}, 'Delete {amt} Entries?']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_146_err){if (!$p['isinstance']($pyjs_dbg_146_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_146_err);}throw $pyjs_dbg_146_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_150_err){if (!$p['isinstance']($pyjs_dbg_150_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_150_err);}throw $pyjs_dbg_150_err;
}})();
				$pyjs['track']['lineno']=220;
				d['__is_instance__'] && typeof d['__setattr__'] == 'function' ? d['__setattr__']('deleteList', function(){
					var $iter3_idx,$iter3_nextval,$iter3_type,$collcomp1,$iter3_iter,$iter3_array,x,$pyjs__trackstack_size_1;
	$collcomp1 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter3_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return selection;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_151_err){if (!$p['isinstance']($pyjs_dbg_151_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_151_err);}throw $pyjs_dbg_151_err;
}})();
				$iter3_nextval=$p['__iter_prepare']($iter3_iter,false);
				while (typeof($p['__wrapped_next']($iter3_nextval)['$nextval']) != 'undefined') {
					x = $iter3_nextval['$nextval'];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp1['append'](x['__getitem__']('id'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_152_err){if (!$p['isinstance']($pyjs_dbg_152_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_152_err);}throw $pyjs_dbg_152_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='actions.list';

	return $collcomp1;}()) : $p['setattr'](d, 'deleteList', function(){
					var $iter3_idx,$iter3_nextval,$iter3_type,$collcomp1,$iter3_iter,$iter3_array,x,$pyjs__trackstack_size_1;
	$collcomp1 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter3_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return selection;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_151_err){if (!$p['isinstance']($pyjs_dbg_151_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_151_err);}throw $pyjs_dbg_151_err;
}})();
				$iter3_nextval=$p['__iter_prepare']($iter3_iter,false);
				while (typeof($p['__wrapped_next']($iter3_nextval)['$nextval']) != 'undefined') {
					x = $iter3_nextval['$nextval'];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp1['append'](x['__getitem__']('id'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_152_err){if (!$p['isinstance']($pyjs_dbg_152_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_152_err);}throw $pyjs_dbg_152_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='actions.list';

	return $collcomp1;}()); 
				$pyjs['track']['lineno']=221;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return d['__getitem__']('class')['append']('delete');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_153_err){if (!$p['isinstance']($pyjs_dbg_153_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_153_err);}throw $pyjs_dbg_153_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=223;
			$method = $pyjs__bind_method2('doDelete', function(dialog) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					dialog = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'd4b0d93a6998da6414332dec008ba4b5') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter4_nextval,deleteList,$iter4_idx,$iter4_type,$pyjs__trackstack_size_1,$iter4_array,x,$iter4_iter;
				$pyjs['track']={'module':'actions.list', 'lineno':223};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=223;
				$pyjs['track']['lineno']=224;
				deleteList = $p['getattr'](dialog, 'deleteList');
				$pyjs['track']['lineno']=225;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter4_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return deleteList;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_154_err){if (!$p['isinstance']($pyjs_dbg_154_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_154_err);}throw $pyjs_dbg_154_err;
}})();
				$iter4_nextval=$p['__iter_prepare']($iter4_iter,false);
				while (typeof($p['__wrapped_next']($iter4_nextval)['$nextval']) != 'undefined') {
					x = $iter4_nextval['$nextval'];
					$pyjs['track']['lineno']=226;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'secure':true, 'modifies':true}, $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_155_err){if (!$p['isinstance']($pyjs_dbg_155_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_155_err);}throw $pyjs_dbg_155_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_156_err){if (!$p['isinstance']($pyjs_dbg_156_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_156_err);}throw $pyjs_dbg_156_err;
}})(), 'modul'), 'delete', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([['id', x]]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_157_err){if (!$p['isinstance']($pyjs_dbg_157_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_157_err);}throw $pyjs_dbg_157_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_158_err){if (!$p['isinstance']($pyjs_dbg_158_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_158_err);}throw $pyjs_dbg_158_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='actions.list';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['dialog']]);
			$cls_definition['doDelete'] = $method;
			$pyjs['track']['lineno']=228;
			$method = $pyjs__bind_method2('resetLoadingState', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'd4b0d93a6998da6414332dec008ba4b5') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':228};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=228;
				$pyjs['track']['lineno']=229;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['resetLoadingState'] = $method;
			$pyjs['track']['lineno']=176;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('DeleteAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=231;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['DeleteAction'], 'isSuitableFor'), $m['DeleteAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_159_err){if (!$p['isinstance']($pyjs_dbg_159_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_159_err);}throw $pyjs_dbg_159_err;
}})();
		$pyjs['track']['lineno']=233;
		$m['ListPreviewAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = 'e383432efacdcfac1502d3cdaf29645d';
			$pyjs['track']['lineno']=234;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e383432efacdcfac1502d3cdaf29645d') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var btn;
				$pyjs['track']={'module':'actions.list', 'lineno':234};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=234;
				$pyjs['track']['lineno']=235;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ListPreviewAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_160_err){if (!$p['isinstance']($pyjs_dbg_160_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_160_err);}throw $pyjs_dbg_160_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_161_err){if (!$p['isinstance']($pyjs_dbg_161_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_161_err);}throw $pyjs_dbg_161_err;
}})();
				$pyjs['track']['lineno']=236;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('urlCb', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Select']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_162_err){if (!$p['isinstance']($pyjs_dbg_162_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_162_err);}throw $pyjs_dbg_162_err;
}})()) : $p['setattr'](self, 'urlCb', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Select']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_162_err){if (!$p['isinstance']($pyjs_dbg_162_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_162_err);}throw $pyjs_dbg_162_err;
}})()); 
				$pyjs['track']['lineno']=237;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'urlCb'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_163_err){if (!$p['isinstance']($pyjs_dbg_163_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_163_err);}throw $pyjs_dbg_163_err;
}})();
				$pyjs['track']['lineno']=238;
				btn = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'onClick')}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Preview');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_164_err){if (!$p['isinstance']($pyjs_dbg_164_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_164_err);}throw $pyjs_dbg_164_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_165_err){if (!$p['isinstance']($pyjs_dbg_165_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_165_err);}throw $pyjs_dbg_165_err;
}})();
				$pyjs['track']['lineno']=239;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return btn['__setitem__']('class', 'icon preview');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_166_err){if (!$p['isinstance']($pyjs_dbg_166_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_166_err);}throw $pyjs_dbg_166_err;
}})();
				$pyjs['track']['lineno']=240;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](btn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_167_err){if (!$p['isinstance']($pyjs_dbg_167_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_167_err);}throw $pyjs_dbg_167_err;
}})();
				$pyjs['track']['lineno']=241;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('urls', null) : $p['setattr'](self, 'urls', null); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=243;
			$method = $pyjs__bind_method2('onChange', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e383432efacdcfac1502d3cdaf29645d') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var newUrl;
				$pyjs['track']={'module':'actions.list', 'lineno':243};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=243;
				$pyjs['track']['lineno']=244;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return event['stopPropagation']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_168_err){if (!$p['isinstance']($pyjs_dbg_168_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_168_err);}throw $pyjs_dbg_168_err;
}})();
				$pyjs['track']['lineno']=245;
				newUrl = $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'urlCb')['__getitem__']('options')['item']($p['getattr'](self, 'urlCb')['__getitem__']('selectedIndex'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_169_err){if (!$p['isinstance']($pyjs_dbg_169_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_169_err);}throw $pyjs_dbg_169_err;
}})(), 'value');
				$pyjs['track']['lineno']=246;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['setUrl'](newUrl);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_170_err){if (!$p['isinstance']($pyjs_dbg_170_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_170_err);}throw $pyjs_dbg_170_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onChange'] = $method;
			$pyjs['track']['lineno']=248;
			$method = $pyjs__bind_method2('rebuildCB', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e383432efacdcfac1502d3cdaf29645d') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var $iter5_nextval,$iter5_idx,name,url,$iter5_array,o,$iter5_iter,$iter5_type,$pyjs__trackstack_size_1;
				$pyjs['track']={'module':'actions.list', 'lineno':248};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=248;
				$pyjs['track']['lineno']=249;
				$p['getattr']($p['getattr'](self, 'urlCb'), 'element')['__is_instance__'] && typeof $p['getattr']($p['getattr'](self, 'urlCb'), 'element')['__setattr__'] == 'function' ? $p['getattr']($p['getattr'](self, 'urlCb'), 'element')['__setattr__']('innerHTML', '') : $p['setattr']($p['getattr']($p['getattr'](self, 'urlCb'), 'element'), 'innerHTML', ''); 
				$pyjs['track']['lineno']=251;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance']($p['getattr'](self, 'urls'), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_171_err){if (!$p['isinstance']($pyjs_dbg_171_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_171_err);}throw $pyjs_dbg_171_err;
}})()));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_172_err){if (!$p['isinstance']($pyjs_dbg_172_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_172_err);}throw $pyjs_dbg_172_err;
}})()) {
					$pyjs['track']['lineno']=252;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'urlCb')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_173_err){if (!$p['isinstance']($pyjs_dbg_173_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_173_err);}throw $pyjs_dbg_173_err;
}})();
					$pyjs['track']['lineno']=253;
					$pyjs['track']['lineno']=253;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=255;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter5_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['urls']['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_175_err){if (!$p['isinstance']($pyjs_dbg_175_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_175_err);}throw $pyjs_dbg_175_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_176_err){if (!$p['isinstance']($pyjs_dbg_176_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_176_err);}throw $pyjs_dbg_176_err;
}})();
				$iter5_nextval=$p['__iter_prepare']($iter5_iter,false);
				while (typeof($p['__wrapped_next']($iter5_nextval)['$nextval']) != 'undefined') {
					var $tupleassign1 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter5_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_174_err){if (!$p['isinstance']($pyjs_dbg_174_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_174_err);}throw $pyjs_dbg_174_err;
}})();
					name = $tupleassign1[0];
					url = $tupleassign1[1];
					$pyjs['track']['lineno']=256;
					o = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Option']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_177_err){if (!$p['isinstance']($pyjs_dbg_177_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_177_err);}throw $pyjs_dbg_177_err;
}})();
					$pyjs['track']['lineno']=257;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return o['__setitem__']('value', url);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_178_err){if (!$p['isinstance']($pyjs_dbg_178_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_178_err);}throw $pyjs_dbg_178_err;
}})();
					$pyjs['track']['lineno']=258;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return o['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['TextNode'](name);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_179_err){if (!$p['isinstance']($pyjs_dbg_179_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_179_err);}throw $pyjs_dbg_179_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_180_err){if (!$p['isinstance']($pyjs_dbg_180_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_180_err);}throw $pyjs_dbg_180_err;
}})();
					$pyjs['track']['lineno']=259;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['urlCb']['appendChild'](o);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_181_err){if (!$p['isinstance']($pyjs_dbg_181_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_181_err);}throw $pyjs_dbg_181_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=261;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len']((function(){try{try{$pyjs['in_try_except'] += 1;
				return self['urls']['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_182_err){if (!$p['isinstance']($pyjs_dbg_182_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_182_err);}throw $pyjs_dbg_182_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_183_err){if (!$p['isinstance']($pyjs_dbg_183_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_183_err);}throw $pyjs_dbg_183_err;
}})(), $constant_int_1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_184_err){if (!$p['isinstance']($pyjs_dbg_184_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_184_err);}throw $pyjs_dbg_184_err;
}})()) {
					$pyjs['track']['lineno']=262;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'urlCb')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_185_err){if (!$p['isinstance']($pyjs_dbg_185_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_185_err);}throw $pyjs_dbg_185_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=264;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'urlCb')['__getitem__']('style')['__setitem__']('display', '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_186_err){if (!$p['isinstance']($pyjs_dbg_186_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_186_err);}throw $pyjs_dbg_186_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['rebuildCB'] = $method;
			$pyjs['track']['lineno']=266;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e383432efacdcfac1502d3cdaf29645d') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and44,$and43,modul,modulConfig;
				$pyjs['track']={'module':'actions.list', 'lineno':266};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=266;
				$pyjs['track']['lineno']=267;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ListPreviewAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_187_err){if (!$p['isinstance']($pyjs_dbg_187_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_187_err);}throw $pyjs_dbg_187_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_188_err){if (!$p['isinstance']($pyjs_dbg_188_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_188_err);}throw $pyjs_dbg_188_err;
}})();
				$pyjs['track']['lineno']=268;
				modul = $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_189_err){if (!$p['isinstance']($pyjs_dbg_189_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_189_err);}throw $pyjs_dbg_189_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_190_err){if (!$p['isinstance']($pyjs_dbg_190_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_190_err);}throw $pyjs_dbg_190_err;
}})(), 'modul');
				$pyjs['track']['lineno']=269;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_191_err){if (!$p['isinstance']($pyjs_dbg_191_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_191_err);}throw $pyjs_dbg_191_err;
}})()['__contains__'](modul));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_192_err){if (!$p['isinstance']($pyjs_dbg_192_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_192_err);}throw $pyjs_dbg_192_err;
}})()) {
					$pyjs['track']['lineno']=270;
					modulConfig = $m['conf']['__getitem__']('modules')['__getitem__'](modul);
					$pyjs['track']['lineno']=271;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and43=(function(){try{try{$pyjs['in_try_except'] += 1;
					return modulConfig['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_193_err){if (!$p['isinstance']($pyjs_dbg_193_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_193_err);}throw $pyjs_dbg_193_err;
}})()['__contains__']('previewurls'))?modulConfig['__getitem__']('previewurls'):$and43));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_194_err){if (!$p['isinstance']($pyjs_dbg_194_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_194_err);}throw $pyjs_dbg_194_err;
}})()) {
						$pyjs['track']['lineno']=272;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('urls', modulConfig['__getitem__']('previewurls')) : $p['setattr'](self, 'urls', modulConfig['__getitem__']('previewurls')); 
						$pyjs['track']['lineno']=273;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['rebuildCB']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_195_err){if (!$p['isinstance']($pyjs_dbg_195_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_195_err);}throw $pyjs_dbg_195_err;
}})();
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=276;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e383432efacdcfac1502d3cdaf29645d') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];
				var selection,$iter6_type,$iter6_iter,$iter6_nextval,$iter7_type,$iter6_idx,$iter7_iter,$pyjs__trackstack_size_1,$iter6_array,$add10,$add11,$add12,$iter7_idx,newUrl,$iter7_nextval,k,$iter7_array,$pyjs__trackstack_size_2,v,entry,$add9;
				$pyjs['track']={'module':'actions.list', 'lineno':276};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=276;
				$pyjs['track']['lineno']=277;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is']($p['getattr'](self, 'urls'), null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_196_err){if (!$p['isinstance']($pyjs_dbg_196_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_196_err);}throw $pyjs_dbg_196_err;
}})()) {
					$pyjs['track']['lineno']=278;
					$pyjs['track']['lineno']=278;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=279;
				selection = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_197_err){if (!$p['isinstance']($pyjs_dbg_197_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_197_err);}throw $pyjs_dbg_197_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_198_err){if (!$p['isinstance']($pyjs_dbg_198_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_198_err);}throw $pyjs_dbg_198_err;
}})()['getCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_199_err){if (!$p['isinstance']($pyjs_dbg_199_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_199_err);}throw $pyjs_dbg_199_err;
}})();
				$pyjs['track']['lineno']=280;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool'](selection));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_200_err){if (!$p['isinstance']($pyjs_dbg_200_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_200_err);}throw $pyjs_dbg_200_err;
}})()) {
					$pyjs['track']['lineno']=281;
					$pyjs['track']['lineno']=281;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=283;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter6_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return selection;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_201_err){if (!$p['isinstance']($pyjs_dbg_201_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_201_err);}throw $pyjs_dbg_201_err;
}})();
				$iter6_nextval=$p['__iter_prepare']($iter6_iter,false);
				while (typeof($p['__wrapped_next']($iter6_nextval)['$nextval']) != 'undefined') {
					entry = $iter6_nextval['$nextval'];
					$pyjs['track']['lineno']=284;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance']($p['getattr'](self, 'urls'), $p['str']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_202_err){if (!$p['isinstance']($pyjs_dbg_202_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_202_err);}throw $pyjs_dbg_202_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_203_err){if (!$p['isinstance']($pyjs_dbg_203_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_203_err);}throw $pyjs_dbg_203_err;
}})()) {
						$pyjs['track']['lineno']=285;
						newUrl = $p['getattr'](self, 'urls');
					}
					else if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_eq']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['len']((function(){try{try{$pyjs['in_try_except'] += 1;
					return self['urls']['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_204_err){if (!$p['isinstance']($pyjs_dbg_204_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_204_err);}throw $pyjs_dbg_204_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_205_err){if (!$p['isinstance']($pyjs_dbg_205_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_205_err);}throw $pyjs_dbg_205_err;
}})(), $constant_int_1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_206_err){if (!$p['isinstance']($pyjs_dbg_206_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_206_err);}throw $pyjs_dbg_206_err;
}})()) {
						$pyjs['track']['lineno']=287;
						newUrl = (function(){try{try{$pyjs['in_try_except'] += 1;
						return self['urls']['values']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_207_err){if (!$p['isinstance']($pyjs_dbg_207_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_207_err);}throw $pyjs_dbg_207_err;
}})()['__getitem__']($constant_int_0);
					}
					else {
						$pyjs['track']['lineno']=289;
						newUrl = $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'urlCb')['__getitem__']('options')['item']($p['getattr'](self, 'urlCb')['__getitem__']('selectedIndex'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_208_err){if (!$p['isinstance']($pyjs_dbg_208_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_208_err);}throw $pyjs_dbg_208_err;
}})(), 'value');
					}
					$pyjs['track']['lineno']=291;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['printFunc']([newUrl], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_209_err){if (!$p['isinstance']($pyjs_dbg_209_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_209_err);}throw $pyjs_dbg_209_err;
}})();
					$pyjs['track']['lineno']=293;
					newUrl = (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return newUrl['$$replace']('{{modul}}', $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_210_err){if (!$p['isinstance']($pyjs_dbg_210_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_210_err);}throw $pyjs_dbg_210_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_211_err){if (!$p['isinstance']($pyjs_dbg_211_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_211_err);}throw $pyjs_dbg_211_err;
}})(), 'modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_212_err){if (!$p['isinstance']($pyjs_dbg_212_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_212_err);}throw $pyjs_dbg_212_err;
}})()['$$replace']('{{module}}', $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_213_err){if (!$p['isinstance']($pyjs_dbg_213_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_213_err);}throw $pyjs_dbg_213_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_214_err){if (!$p['isinstance']($pyjs_dbg_214_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_214_err);}throw $pyjs_dbg_214_err;
}})(), 'modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_215_err){if (!$p['isinstance']($pyjs_dbg_215_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_215_err);}throw $pyjs_dbg_215_err;
}})();
					$pyjs['track']['lineno']=297;
					$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
					$iter7_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return entry['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_217_err){if (!$p['isinstance']($pyjs_dbg_217_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_217_err);}throw $pyjs_dbg_217_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_218_err){if (!$p['isinstance']($pyjs_dbg_218_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_218_err);}throw $pyjs_dbg_218_err;
}})();
					$iter7_nextval=$p['__iter_prepare']($iter7_iter,false);
					while (typeof($p['__wrapped_next']($iter7_nextval)['$nextval']) != 'undefined') {
						var $tupleassign2 = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['__ass_unpack']($iter7_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_216_err){if (!$p['isinstance']($pyjs_dbg_216_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_216_err);}throw $pyjs_dbg_216_err;
}})();
						k = $tupleassign2[0];
						v = $tupleassign2[1];
						$pyjs['track']['lineno']=298;
						newUrl = (function(){try{try{$pyjs['in_try_except'] += 1;
						return newUrl['$$replace']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('{{%s}}', k);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_219_err){if (!$p['isinstance']($pyjs_dbg_219_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_219_err);}throw $pyjs_dbg_219_err;
}})(), v);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_220_err){if (!$p['isinstance']($pyjs_dbg_220_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_220_err);}throw $pyjs_dbg_220_err;
}})();
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='actions.list';
					$pyjs['track']['lineno']=300;
					newUrl = (function(){try{try{$pyjs['in_try_except'] += 1;
					return newUrl['$$replace']("'", "\\'");
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_221_err){if (!$p['isinstance']($pyjs_dbg_221_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_221_err);}throw $pyjs_dbg_221_err;
}})();
					$pyjs['track']['lineno']=302;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['printFunc']([newUrl], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_222_err){if (!$p['isinstance']($pyjs_dbg_222_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_222_err);}throw $pyjs_dbg_222_err;
}})();
					$pyjs['track']['lineno']=303;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return (typeof eval == "undefined"?$m['eval']:eval)($p['__op_add']($add11=$p['__op_add']($add9="window.open('",$add10=newUrl),$add12="', 'ViPreview');"));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_223_err){if (!$p['isinstance']($pyjs_dbg_223_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_223_err);}throw $pyjs_dbg_223_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='actions.list';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=308;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var $and51,correctHandler,$and48,$and45,isDisabled,$and46,$or20,modulConfig,correctAction,$add14,$add13,$and49,hasAccess,$and56,$and57,$and54,$and55,$and52,$and53,$and50,$or18,$or17,isAvailable,$or19,$and47;
				$pyjs['track']={'module':'actions.list', 'lineno':308};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=308;
				$pyjs['track']['lineno']=309;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](modul, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_224_err){if (!$p['isinstance']($pyjs_dbg_224_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_224_err);}throw $pyjs_dbg_224_err;
}})()) {
					$pyjs['track']['lineno']=310;
					$pyjs['track']['lineno']=310;
					var $pyjs__ret = false;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=311;
				correctAction = $p['op_eq'](actionName, 'preview');
				$pyjs['track']['lineno']=312;
				correctHandler = ($p['bool']($or17=$p['op_eq'](handler, 'list'))?$or17:(function(){try{try{$pyjs['in_try_except'] += 1;
				return handler['startswith']('list.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_225_err){if (!$p['isinstance']($pyjs_dbg_225_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_225_err);}throw $pyjs_dbg_225_err;
}})());
				$pyjs['track']['lineno']=313;
				hasAccess = ($p['bool']($and45=$m['conf']['__getitem__']('currentUser'))?($p['bool']($or19=$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']('root'))?$or19:$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']($p['__op_add']($add13=modul,$add14='-view'))):$and45);
				$pyjs['track']['lineno']=314;
				isDisabled = ($p['bool']($and47=!$p['op_is'](modul, null))?($p['bool']($and48=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['__getitem__'](modul)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_226_err){if (!$p['isinstance']($pyjs_dbg_226_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_226_err);}throw $pyjs_dbg_226_err;
}})()['__contains__']('disabledFunctions'))?($p['bool']($and49=$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions'))?$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions')['__contains__']('view'):$and49):$and48):$and47);
				$pyjs['track']['lineno']=315;
				isAvailable = false;
				$pyjs['track']['lineno']=316;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_227_err){if (!$p['isinstance']($pyjs_dbg_227_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_227_err);}throw $pyjs_dbg_227_err;
}})()['__contains__'](modul));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_228_err){if (!$p['isinstance']($pyjs_dbg_228_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_228_err);}throw $pyjs_dbg_228_err;
}})()) {
					$pyjs['track']['lineno']=317;
					modulConfig = $m['conf']['__getitem__']('modules')['__getitem__'](modul);
					$pyjs['track']['lineno']=318;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and51=(function(){try{try{$pyjs['in_try_except'] += 1;
					return modulConfig['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_229_err){if (!$p['isinstance']($pyjs_dbg_229_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_229_err);}throw $pyjs_dbg_229_err;
}})()['__contains__']('previewurls'))?modulConfig['__getitem__']('previewurls'):$and51));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_230_err){if (!$p['isinstance']($pyjs_dbg_230_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_230_err);}throw $pyjs_dbg_230_err;
}})()) {
						$pyjs['track']['lineno']=319;
						isAvailable = true;
					}
				}
				$pyjs['track']['lineno']=320;
				$pyjs['track']['lineno']=320;
				var $pyjs__ret = ($p['bool']($and53=correctAction)?($p['bool']($and54=correctHandler)?($p['bool']($and55=hasAccess)?($p['bool']($and56=!$p['bool'](isDisabled))?isAvailable:$and56):$and55):$and54):$and53);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=233;
			var $bases = new Array($p['getattr']($m['html5'], 'Span'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('ListPreviewAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=322;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_2, $p['getattr']($m['ListPreviewAction'], 'isSuitableFor'), $m['ListPreviewAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_231_err){if (!$p['isinstance']($pyjs_dbg_231_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_231_err);}throw $pyjs_dbg_231_err;
}})();
		$pyjs['track']['lineno']=325;
		$m['ListPreviewInlineAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = '983d024028d1140491e0c05c20275738';
			$pyjs['track']['lineno']=326;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '983d024028d1140491e0c05c20275738') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':326};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=326;
				$pyjs['track']['lineno']=327;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ListPreviewInlineAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_232_err){if (!$p['isinstance']($pyjs_dbg_232_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_232_err);}throw $pyjs_dbg_232_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Preview');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_233_err){if (!$p['isinstance']($pyjs_dbg_233_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_233_err);}throw $pyjs_dbg_233_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_234_err){if (!$p['isinstance']($pyjs_dbg_234_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_234_err);}throw $pyjs_dbg_234_err;
}})();
				$pyjs['track']['lineno']=328;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon preview');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_235_err){if (!$p['isinstance']($pyjs_dbg_235_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_235_err);}throw $pyjs_dbg_235_err;
}})();
				$pyjs['track']['lineno']=329;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_236_err){if (!$p['isinstance']($pyjs_dbg_236_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_236_err);}throw $pyjs_dbg_236_err;
}})();
				$pyjs['track']['lineno']=330;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('urls', null) : $p['setattr'](self, 'urls', null); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=332;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '983d024028d1140491e0c05c20275738') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':332};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=332;
				$pyjs['track']['lineno']=333;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ListPreviewInlineAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_237_err){if (!$p['isinstance']($pyjs_dbg_237_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_237_err);}throw $pyjs_dbg_237_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_238_err){if (!$p['isinstance']($pyjs_dbg_238_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_238_err);}throw $pyjs_dbg_238_err;
}})();
				$pyjs['track']['lineno']=334;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_239_err){if (!$p['isinstance']($pyjs_dbg_239_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_239_err);}throw $pyjs_dbg_239_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_240_err){if (!$p['isinstance']($pyjs_dbg_240_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_240_err);}throw $pyjs_dbg_240_err;
}})()['selectionChangedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_241_err){if (!$p['isinstance']($pyjs_dbg_241_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_241_err);}throw $pyjs_dbg_241_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=336;
			$method = $pyjs__bind_method2('onDetach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '983d024028d1140491e0c05c20275738') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':336};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=336;
				$pyjs['track']['lineno']=337;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_242_err){if (!$p['isinstance']($pyjs_dbg_242_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_242_err);}throw $pyjs_dbg_242_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_243_err){if (!$p['isinstance']($pyjs_dbg_243_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_243_err);}throw $pyjs_dbg_243_err;
}})()['selectionChangedEvent']['unregister'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_244_err){if (!$p['isinstance']($pyjs_dbg_244_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_244_err);}throw $pyjs_dbg_244_err;
}})();
				$pyjs['track']['lineno']=338;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ListPreviewInlineAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_245_err){if (!$p['isinstance']($pyjs_dbg_245_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_245_err);}throw $pyjs_dbg_245_err;
}})()['onDetach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_246_err){if (!$p['isinstance']($pyjs_dbg_246_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_246_err);}throw $pyjs_dbg_246_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onDetach'] = $method;
			$pyjs['track']['lineno']=340;
			$method = $pyjs__bind_method2('onSelectionChanged', function(table, selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					selection = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '983d024028d1140491e0c05c20275738') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and58,$and59,module,$and61,$and60,preview;
				$pyjs['track']={'module':'actions.list', 'lineno':340};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=340;
				$pyjs['track']['lineno']=341;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_247_err){if (!$p['isinstance']($pyjs_dbg_247_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_247_err);}throw $pyjs_dbg_247_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_248_err){if (!$p['isinstance']($pyjs_dbg_248_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_248_err);}throw $pyjs_dbg_248_err;
}})(), 'isSelector'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_249_err){if (!$p['isinstance']($pyjs_dbg_249_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_249_err);}throw $pyjs_dbg_249_err;
}})()) {
					$pyjs['track']['lineno']=342;
					$pyjs['track']['lineno']=342;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=345;
				module = $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_250_err){if (!$p['isinstance']($pyjs_dbg_250_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_250_err);}throw $pyjs_dbg_250_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_251_err){if (!$p['isinstance']($pyjs_dbg_251_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_251_err);}throw $pyjs_dbg_251_err;
}})(), 'modul');
				$pyjs['track']['lineno']=346;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and58=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['__getitem__'](module)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_252_err){if (!$p['isinstance']($pyjs_dbg_252_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_252_err);}throw $pyjs_dbg_252_err;
}})()['__contains__']('disableInternalPreview'))?$m['conf']['__getitem__']('modules')['__getitem__'](module)['__getitem__']('disableInternalPreview'):$and58));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_253_err){if (!$p['isinstance']($pyjs_dbg_253_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_253_err);}throw $pyjs_dbg_253_err;
}})()) {
					$pyjs['track']['lineno']=348;
					$pyjs['track']['lineno']=348;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=351;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and60=(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_254_err){if (!$p['isinstance']($pyjs_dbg_254_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_254_err);}throw $pyjs_dbg_254_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_255_err){if (!$p['isinstance']($pyjs_dbg_255_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_255_err);}throw $pyjs_dbg_255_err;
}})()['sideBar']['getWidget']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_256_err){if (!$p['isinstance']($pyjs_dbg_256_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_256_err);}throw $pyjs_dbg_256_err;
}})())?!$p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_257_err){if (!$p['isinstance']($pyjs_dbg_257_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_257_err);}throw $pyjs_dbg_257_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_258_err){if (!$p['isinstance']($pyjs_dbg_258_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_258_err);}throw $pyjs_dbg_258_err;
}})()['sideBar']['getWidget']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_259_err){if (!$p['isinstance']($pyjs_dbg_259_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_259_err);}throw $pyjs_dbg_259_err;
}})(), $m['InternalPreview']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_260_err){if (!$p['isinstance']($pyjs_dbg_260_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_260_err);}throw $pyjs_dbg_260_err;
}})()):$and60));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_261_err){if (!$p['isinstance']($pyjs_dbg_261_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_261_err);}throw $pyjs_dbg_261_err;
}})()) {
					$pyjs['track']['lineno']=353;
					$pyjs['track']['lineno']=353;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=357;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](selection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_262_err){if (!$p['isinstance']($pyjs_dbg_262_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_262_err);}throw $pyjs_dbg_262_err;
}})(), $constant_int_1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_263_err){if (!$p['isinstance']($pyjs_dbg_263_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_263_err);}throw $pyjs_dbg_263_err;
}})()) {
					$pyjs['track']['lineno']=358;
					preview = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['InternalPreview']($p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_264_err){if (!$p['isinstance']($pyjs_dbg_264_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_264_err);}throw $pyjs_dbg_264_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_265_err){if (!$p['isinstance']($pyjs_dbg_265_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_265_err);}throw $pyjs_dbg_265_err;
}})(), 'modul'), $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_266_err){if (!$p['isinstance']($pyjs_dbg_266_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_266_err);}throw $pyjs_dbg_266_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_267_err){if (!$p['isinstance']($pyjs_dbg_267_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_267_err);}throw $pyjs_dbg_267_err;
}})(), '_structure'), selection['__getitem__']($constant_int_0));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_268_err){if (!$p['isinstance']($pyjs_dbg_268_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_268_err);}throw $pyjs_dbg_268_err;
}})();
					$pyjs['track']['lineno']=359;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_269_err){if (!$p['isinstance']($pyjs_dbg_269_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_269_err);}throw $pyjs_dbg_269_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_270_err){if (!$p['isinstance']($pyjs_dbg_270_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_270_err);}throw $pyjs_dbg_270_err;
}})()['sideBar']['setWidget'](preview);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_271_err){if (!$p['isinstance']($pyjs_dbg_271_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_271_err);}throw $pyjs_dbg_271_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=361;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance']((function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_272_err){if (!$p['isinstance']($pyjs_dbg_272_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_272_err);}throw $pyjs_dbg_272_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_273_err){if (!$p['isinstance']($pyjs_dbg_273_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_273_err);}throw $pyjs_dbg_273_err;
}})()['sideBar']['getWidget']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_274_err){if (!$p['isinstance']($pyjs_dbg_274_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_274_err);}throw $pyjs_dbg_274_err;
}})(), $m['InternalPreview']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_275_err){if (!$p['isinstance']($pyjs_dbg_275_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_275_err);}throw $pyjs_dbg_275_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_276_err){if (!$p['isinstance']($pyjs_dbg_276_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_276_err);}throw $pyjs_dbg_276_err;
}})()) {
						$pyjs['track']['lineno']=362;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_277_err){if (!$p['isinstance']($pyjs_dbg_277_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_277_err);}throw $pyjs_dbg_277_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_278_err){if (!$p['isinstance']($pyjs_dbg_278_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_278_err);}throw $pyjs_dbg_278_err;
}})()['sideBar']['setWidget'](null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_279_err){if (!$p['isinstance']($pyjs_dbg_279_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_279_err);}throw $pyjs_dbg_279_err;
}})();
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['selection']]);
			$cls_definition['onSelectionChanged'] = $method;
			$pyjs['track']['lineno']=365;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var $and67,$and66,$and65,$and64,$and63,$and62,$and69,$and68,correctHandler,isDisabled,$or24,$or21,$or22,$or23,correctAction,$add15,$add16,hasAccess,$and70,$and71;
				$pyjs['track']={'module':'actions.list', 'lineno':365};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=365;
				$pyjs['track']['lineno']=366;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](modul, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_280_err){if (!$p['isinstance']($pyjs_dbg_280_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_280_err);}throw $pyjs_dbg_280_err;
}})()) {
					$pyjs['track']['lineno']=367;
					$pyjs['track']['lineno']=367;
					var $pyjs__ret = false;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=368;
				correctAction = $p['op_eq'](actionName, 'preview');
				$pyjs['track']['lineno']=369;
				correctHandler = ($p['bool']($or21=$p['op_eq'](handler, 'list'))?$or21:(function(){try{try{$pyjs['in_try_except'] += 1;
				return handler['startswith']('list.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_281_err){if (!$p['isinstance']($pyjs_dbg_281_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_281_err);}throw $pyjs_dbg_281_err;
}})());
				$pyjs['track']['lineno']=370;
				hasAccess = ($p['bool']($and62=$m['conf']['__getitem__']('currentUser'))?($p['bool']($or23=$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']('root'))?$or23:$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']($p['__op_add']($add15=modul,$add16='-view'))):$and62);
				$pyjs['track']['lineno']=371;
				isDisabled = ($p['bool']($and64=!$p['op_is'](modul, null))?($p['bool']($and65=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['__getitem__'](modul)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_282_err){if (!$p['isinstance']($pyjs_dbg_282_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_282_err);}throw $pyjs_dbg_282_err;
}})()['__contains__']('disabledFunctions'))?($p['bool']($and66=$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions'))?$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions')['__contains__']('view'):$and66):$and65):$and64);
				$pyjs['track']['lineno']=372;
				$pyjs['track']['lineno']=372;
				var $pyjs__ret = ($p['bool']($and68=correctAction)?($p['bool']($and69=correctHandler)?($p['bool']($and70=hasAccess)?!$p['bool'](isDisabled):$and70):$and69):$and68);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=325;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('ListPreviewInlineAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=374;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['ListPreviewInlineAction'], 'isSuitableFor'), $m['ListPreviewInlineAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_283_err){if (!$p['isinstance']($pyjs_dbg_283_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_283_err);}throw $pyjs_dbg_283_err;
}})();
		$pyjs['track']['lineno']=377;
		$m['CloseAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = '0990833832a22cce76518105bf53fd18';
			$pyjs['track']['lineno']=378;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0990833832a22cce76518105bf53fd18') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':378};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=378;
				$pyjs['track']['lineno']=379;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['CloseAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_284_err){if (!$p['isinstance']($pyjs_dbg_284_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_284_err);}throw $pyjs_dbg_284_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Close');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_285_err){if (!$p['isinstance']($pyjs_dbg_285_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_285_err);}throw $pyjs_dbg_285_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_286_err){if (!$p['isinstance']($pyjs_dbg_286_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_286_err);}throw $pyjs_dbg_286_err;
}})();
				$pyjs['track']['lineno']=380;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon close');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_287_err){if (!$p['isinstance']($pyjs_dbg_287_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_287_err);}throw $pyjs_dbg_287_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=382;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0990833832a22cce76518105bf53fd18') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];

				$pyjs['track']={'module':'actions.list', 'lineno':382};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=382;
				$pyjs['track']['lineno']=383;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['removeWidget']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_288_err){if (!$p['isinstance']($pyjs_dbg_288_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_288_err);}throw $pyjs_dbg_288_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_289_err){if (!$p['isinstance']($pyjs_dbg_289_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_289_err);}throw $pyjs_dbg_289_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_290_err){if (!$p['isinstance']($pyjs_dbg_290_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_290_err);}throw $pyjs_dbg_290_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=386;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);

				$pyjs['track']={'module':'actions.list', 'lineno':386};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=386;
				$pyjs['track']['lineno']=387;
				$pyjs['track']['lineno']=387;
				var $pyjs__ret = $p['op_eq'](actionName, 'close');
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=377;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('CloseAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=389;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['CloseAction'], 'isSuitableFor'), $m['CloseAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_291_err){if (!$p['isinstance']($pyjs_dbg_291_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_291_err);}throw $pyjs_dbg_291_err;
}})();
		$pyjs['track']['lineno']=391;
		$m['ActivateSelectionAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = '3ee2d5cbb06cd27dfe0b662ca87d1550';
			$pyjs['track']['lineno']=392;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '3ee2d5cbb06cd27dfe0b662ca87d1550') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':392};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=392;
				$pyjs['track']['lineno']=393;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ActivateSelectionAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_292_err){if (!$p['isinstance']($pyjs_dbg_292_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_292_err);}throw $pyjs_dbg_292_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Select');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_293_err){if (!$p['isinstance']($pyjs_dbg_293_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_293_err);}throw $pyjs_dbg_293_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_294_err){if (!$p['isinstance']($pyjs_dbg_294_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_294_err);}throw $pyjs_dbg_294_err;
}})();
				$pyjs['track']['lineno']=394;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon activateselection');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_295_err){if (!$p['isinstance']($pyjs_dbg_295_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_295_err);}throw $pyjs_dbg_295_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=396;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '3ee2d5cbb06cd27dfe0b662ca87d1550') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];

				$pyjs['track']={'module':'actions.list', 'lineno':396};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=396;
				$pyjs['track']['lineno']=397;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_296_err){if (!$p['isinstance']($pyjs_dbg_296_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_296_err);}throw $pyjs_dbg_296_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_297_err){if (!$p['isinstance']($pyjs_dbg_297_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_297_err);}throw $pyjs_dbg_297_err;
}})()['activateCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_298_err){if (!$p['isinstance']($pyjs_dbg_298_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_298_err);}throw $pyjs_dbg_298_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=400;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);

				$pyjs['track']={'module':'actions.list', 'lineno':400};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=400;
				$pyjs['track']['lineno']=401;
				$pyjs['track']['lineno']=401;
				var $pyjs__ret = $p['op_eq'](actionName, 'select');
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=391;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('ActivateSelectionAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=403;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['ActivateSelectionAction'], 'isSuitableFor'), $m['ActivateSelectionAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_299_err){if (!$p['isinstance']($pyjs_dbg_299_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_299_err);}throw $pyjs_dbg_299_err;
}})();
		$pyjs['track']['lineno']=406;
		$m['SelectFieldsPopup'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = '39282267022dc69f436cbe6650a7e2d9';
			$pyjs['track']['lineno']=407;
			$method = $pyjs__bind_method2('__init__', function(listWdg) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					listWdg = arguments[1];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,2,arguments['length']-1));

					var kwargs = arguments['length'] >= 3 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '39282267022dc69f436cbe6650a7e2d9') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof listWdg != 'undefined') {
						if (listWdg !== null && typeof listWdg['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = listWdg;
							listWdg = arguments[2];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[2];
						}
					} else {
					}
				}
				var $iter8_idx,lbl,$iter8_array,chkBox,li,$iter8_iter,ul,$iter8_nextval,key,div,$pyjs__trackstack_size_1,$iter8_type,bone;
				$pyjs['track']={'module':'actions.list', 'lineno':407};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=407;
				$pyjs['track']['lineno']=408;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](listWdg, '_structure')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_300_err){if (!$p['isinstance']($pyjs_dbg_300_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_300_err);}throw $pyjs_dbg_300_err;
}})()) {
					$pyjs['track']['lineno']=409;
					$pyjs['track']['lineno']=409;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=411;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectFieldsPopup'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_301_err){if (!$p['isinstance']($pyjs_dbg_301_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_301_err);}throw $pyjs_dbg_301_err;
}})(), '__init__', args, kwargs, [{'title':(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Select fields');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_302_err){if (!$p['isinstance']($pyjs_dbg_302_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_302_err);}throw $pyjs_dbg_302_err;
}})()}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_303_err){if (!$p['isinstance']($pyjs_dbg_303_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_303_err);}throw $pyjs_dbg_303_err;
}})();
				$pyjs['track']['lineno']=413;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__getitem__']('class')['append']('selectfields');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_304_err){if (!$p['isinstance']($pyjs_dbg_304_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_304_err);}throw $pyjs_dbg_304_err;
}})();
				$pyjs['track']['lineno']=414;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('listWdg', listWdg) : $p['setattr'](self, 'listWdg', listWdg); 
				$pyjs['track']['lineno']=415;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('checkboxes', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_305_err){if (!$p['isinstance']($pyjs_dbg_305_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_305_err);}throw $pyjs_dbg_305_err;
}})()) : $p['setattr'](self, 'checkboxes', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_305_err){if (!$p['isinstance']($pyjs_dbg_305_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_305_err);}throw $pyjs_dbg_305_err;
}})()); 
				$pyjs['track']['lineno']=417;
				ul = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Ul']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_306_err){if (!$p['isinstance']($pyjs_dbg_306_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_306_err);}throw $pyjs_dbg_306_err;
}})();
				$pyjs['track']['lineno']=418;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](ul);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_307_err){if (!$p['isinstance']($pyjs_dbg_307_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_307_err);}throw $pyjs_dbg_307_err;
}})();
				$pyjs['track']['lineno']=420;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter8_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr']($p['getattr'](self, 'listWdg'), '_structure');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_309_err){if (!$p['isinstance']($pyjs_dbg_309_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_309_err);}throw $pyjs_dbg_309_err;
}})();
				$iter8_nextval=$p['__iter_prepare']($iter8_iter,false);
				while (typeof($p['__wrapped_next']($iter8_nextval)['$nextval']) != 'undefined') {
					var $tupleassign3 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter8_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_308_err){if (!$p['isinstance']($pyjs_dbg_308_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_308_err);}throw $pyjs_dbg_308_err;
}})();
					key = $tupleassign3[0];
					bone = $tupleassign3[1];
					$pyjs['track']['lineno']=421;
					li = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Li']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_310_err){if (!$p['isinstance']($pyjs_dbg_310_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_310_err);}throw $pyjs_dbg_310_err;
}})();
					$pyjs['track']['lineno']=422;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return ul['appendChild'](li);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_311_err){if (!$p['isinstance']($pyjs_dbg_311_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_311_err);}throw $pyjs_dbg_311_err;
}})();
					$pyjs['track']['lineno']=424;
					chkBox = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_312_err){if (!$p['isinstance']($pyjs_dbg_312_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_312_err);}throw $pyjs_dbg_312_err;
}})();
					$pyjs['track']['lineno']=425;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return chkBox['__setitem__']('type', 'checkbox');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_313_err){if (!$p['isinstance']($pyjs_dbg_313_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_313_err);}throw $pyjs_dbg_313_err;
}})();
					$pyjs['track']['lineno']=426;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return chkBox['__setitem__']('value', key);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_314_err){if (!$p['isinstance']($pyjs_dbg_314_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_314_err);}throw $pyjs_dbg_314_err;
}})();
					$pyjs['track']['lineno']=428;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return li['appendChild'](chkBox);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_315_err){if (!$p['isinstance']($pyjs_dbg_315_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_315_err);}throw $pyjs_dbg_315_err;
}})();
					$pyjs['track']['lineno']=429;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['checkboxes']['append'](chkBox);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_316_err){if (!$p['isinstance']($pyjs_dbg_316_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_316_err);}throw $pyjs_dbg_316_err;
}})();
					$pyjs['track']['lineno']=431;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return self['listWdg']['getFields']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_317_err){if (!$p['isinstance']($pyjs_dbg_317_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_317_err);}throw $pyjs_dbg_317_err;
}})()['__contains__'](key));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_318_err){if (!$p['isinstance']($pyjs_dbg_318_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_318_err);}throw $pyjs_dbg_318_err;
}})()) {
						$pyjs['track']['lineno']=432;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return chkBox['__setitem__']('checked', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_319_err){if (!$p['isinstance']($pyjs_dbg_319_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_319_err);}throw $pyjs_dbg_319_err;
}})();
					}
					$pyjs['track']['lineno']=433;
					lbl = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call($m['html5'], 'Label', null, null, [{'forElem':chkBox}, bone['__getitem__']('descr')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_320_err){if (!$p['isinstance']($pyjs_dbg_320_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_320_err);}throw $pyjs_dbg_320_err;
}})();
					$pyjs['track']['lineno']=434;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return li['appendChild'](lbl);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_321_err){if (!$p['isinstance']($pyjs_dbg_321_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_321_err);}throw $pyjs_dbg_321_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=437;
				div = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_322_err){if (!$p['isinstance']($pyjs_dbg_322_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_322_err);}throw $pyjs_dbg_322_err;
}})();
				$pyjs['track']['lineno']=438;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return div['__getitem__']('class')['append']('selectiontools');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_323_err){if (!$p['isinstance']($pyjs_dbg_323_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_323_err);}throw $pyjs_dbg_323_err;
}})();
				$pyjs['track']['lineno']=440;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](div);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_324_err){if (!$p['isinstance']($pyjs_dbg_324_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_324_err);}throw $pyjs_dbg_324_err;
}})();
				$pyjs['track']['lineno']=442;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectAllBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'doSelectAll')}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Select all');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_325_err){if (!$p['isinstance']($pyjs_dbg_325_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_325_err);}throw $pyjs_dbg_325_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_326_err){if (!$p['isinstance']($pyjs_dbg_326_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_326_err);}throw $pyjs_dbg_326_err;
}})()) : $p['setattr'](self, 'selectAllBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'doSelectAll')}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Select all');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_325_err){if (!$p['isinstance']($pyjs_dbg_325_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_325_err);}throw $pyjs_dbg_325_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_326_err){if (!$p['isinstance']($pyjs_dbg_326_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_326_err);}throw $pyjs_dbg_326_err;
}})()); 
				$pyjs['track']['lineno']=443;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'selectAllBtn')['__getitem__']('class')['append']('icon');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_327_err){if (!$p['isinstance']($pyjs_dbg_327_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_327_err);}throw $pyjs_dbg_327_err;
}})();
				$pyjs['track']['lineno']=444;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'selectAllBtn')['__getitem__']('class')['append']('selectall');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_328_err){if (!$p['isinstance']($pyjs_dbg_328_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_328_err);}throw $pyjs_dbg_328_err;
}})();
				$pyjs['track']['lineno']=445;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('unselectAllBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'doUnselectAll')}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Unselect all');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_329_err){if (!$p['isinstance']($pyjs_dbg_329_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_329_err);}throw $pyjs_dbg_329_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_330_err){if (!$p['isinstance']($pyjs_dbg_330_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_330_err);}throw $pyjs_dbg_330_err;
}})()) : $p['setattr'](self, 'unselectAllBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'doUnselectAll')}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Unselect all');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_329_err){if (!$p['isinstance']($pyjs_dbg_329_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_329_err);}throw $pyjs_dbg_329_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_330_err){if (!$p['isinstance']($pyjs_dbg_330_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_330_err);}throw $pyjs_dbg_330_err;
}})()); 
				$pyjs['track']['lineno']=446;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'unselectAllBtn')['__getitem__']('class')['append']('icon');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_331_err){if (!$p['isinstance']($pyjs_dbg_331_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_331_err);}throw $pyjs_dbg_331_err;
}})();
				$pyjs['track']['lineno']=447;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'unselectAllBtn')['__getitem__']('class')['append']('unselectall');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_332_err){if (!$p['isinstance']($pyjs_dbg_332_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_332_err);}throw $pyjs_dbg_332_err;
}})();
				$pyjs['track']['lineno']=448;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('invertSelectionBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'doInvertSelection')}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Invert selection');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_333_err){if (!$p['isinstance']($pyjs_dbg_333_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_333_err);}throw $pyjs_dbg_333_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_334_err){if (!$p['isinstance']($pyjs_dbg_334_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_334_err);}throw $pyjs_dbg_334_err;
}})()) : $p['setattr'](self, 'invertSelectionBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'doInvertSelection')}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Invert selection');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_333_err){if (!$p['isinstance']($pyjs_dbg_333_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_333_err);}throw $pyjs_dbg_333_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_334_err){if (!$p['isinstance']($pyjs_dbg_334_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_334_err);}throw $pyjs_dbg_334_err;
}})()); 
				$pyjs['track']['lineno']=449;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'invertSelectionBtn')['__getitem__']('class')['append']('icon');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_335_err){if (!$p['isinstance']($pyjs_dbg_335_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_335_err);}throw $pyjs_dbg_335_err;
}})();
				$pyjs['track']['lineno']=450;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'invertSelectionBtn')['__getitem__']('class')['append']('selectinvert');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_336_err){if (!$p['isinstance']($pyjs_dbg_336_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_336_err);}throw $pyjs_dbg_336_err;
}})();
				$pyjs['track']['lineno']=452;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return div['appendChild']($p['getattr'](self, 'selectAllBtn'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_337_err){if (!$p['isinstance']($pyjs_dbg_337_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_337_err);}throw $pyjs_dbg_337_err;
}})();
				$pyjs['track']['lineno']=453;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return div['appendChild']($p['getattr'](self, 'unselectAllBtn'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_338_err){if (!$p['isinstance']($pyjs_dbg_338_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_338_err);}throw $pyjs_dbg_338_err;
}})();
				$pyjs['track']['lineno']=454;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return div['appendChild']($p['getattr'](self, 'invertSelectionBtn'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_339_err){if (!$p['isinstance']($pyjs_dbg_339_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_339_err);}throw $pyjs_dbg_339_err;
}})();
				$pyjs['track']['lineno']=457;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('cancelBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'doCancel')}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Cancel');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_340_err){if (!$p['isinstance']($pyjs_dbg_340_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_340_err);}throw $pyjs_dbg_340_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_341_err){if (!$p['isinstance']($pyjs_dbg_341_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_341_err);}throw $pyjs_dbg_341_err;
}})()) : $p['setattr'](self, 'cancelBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'doCancel')}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Cancel');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_340_err){if (!$p['isinstance']($pyjs_dbg_340_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_340_err);}throw $pyjs_dbg_340_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_341_err){if (!$p['isinstance']($pyjs_dbg_341_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_341_err);}throw $pyjs_dbg_341_err;
}})()); 
				$pyjs['track']['lineno']=458;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'cancelBtn')['__getitem__']('class')['append']('btn_no');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_342_err){if (!$p['isinstance']($pyjs_dbg_342_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_342_err);}throw $pyjs_dbg_342_err;
}})();
				$pyjs['track']['lineno']=460;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('applyBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'doApply')}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Apply');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_343_err){if (!$p['isinstance']($pyjs_dbg_343_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_343_err);}throw $pyjs_dbg_343_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_344_err){if (!$p['isinstance']($pyjs_dbg_344_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_344_err);}throw $pyjs_dbg_344_err;
}})()) : $p['setattr'](self, 'applyBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'doApply')}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Apply');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_343_err){if (!$p['isinstance']($pyjs_dbg_343_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_343_err);}throw $pyjs_dbg_343_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_344_err){if (!$p['isinstance']($pyjs_dbg_344_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_344_err);}throw $pyjs_dbg_344_err;
}})()); 
				$pyjs['track']['lineno']=461;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'applyBtn')['__getitem__']('class')['append']('btn_yes');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_345_err){if (!$p['isinstance']($pyjs_dbg_345_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_345_err);}throw $pyjs_dbg_345_err;
}})();
				$pyjs['track']['lineno']=463;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'applyBtn'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_346_err){if (!$p['isinstance']($pyjs_dbg_346_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_346_err);}throw $pyjs_dbg_346_err;
}})();
				$pyjs['track']['lineno']=464;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'cancelBtn'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_347_err){if (!$p['isinstance']($pyjs_dbg_347_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_347_err);}throw $pyjs_dbg_347_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['listWdg']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=466;
			$method = $pyjs__bind_method2('doApply', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '39282267022dc69f436cbe6650a7e2d9') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var c,$iter9_iter,res,$iter9_nextval,$iter9_idx,$iter9_array,$pyjs__trackstack_size_1,$iter9_type;
				$pyjs['track']={'module':'actions.list', 'lineno':466};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=466;
				$pyjs['track']['lineno']=467;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'applyBtn')['__getitem__']('class')['append']('is_loading');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_348_err){if (!$p['isinstance']($pyjs_dbg_348_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_348_err);}throw $pyjs_dbg_348_err;
}})();
				$pyjs['track']['lineno']=468;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'applyBtn')['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_349_err){if (!$p['isinstance']($pyjs_dbg_349_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_349_err);}throw $pyjs_dbg_349_err;
}})();
				$pyjs['track']['lineno']=470;
				res = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_350_err){if (!$p['isinstance']($pyjs_dbg_350_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_350_err);}throw $pyjs_dbg_350_err;
}})();
				$pyjs['track']['lineno']=471;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter9_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'checkboxes');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_351_err){if (!$p['isinstance']($pyjs_dbg_351_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_351_err);}throw $pyjs_dbg_351_err;
}})();
				$iter9_nextval=$p['__iter_prepare']($iter9_iter,false);
				while (typeof($p['__wrapped_next']($iter9_nextval)['$nextval']) != 'undefined') {
					c = $iter9_nextval['$nextval'];
					$pyjs['track']['lineno']=472;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](c['__getitem__']('checked'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_352_err){if (!$p['isinstance']($pyjs_dbg_352_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_352_err);}throw $pyjs_dbg_352_err;
}})()) {
						$pyjs['track']['lineno']=473;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return res['append'](c['__getitem__']('value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_353_err){if (!$p['isinstance']($pyjs_dbg_353_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_353_err);}throw $pyjs_dbg_353_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=475;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['listWdg']['setFields'](res);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_354_err){if (!$p['isinstance']($pyjs_dbg_354_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_354_err);}throw $pyjs_dbg_354_err;
}})();
				$pyjs['track']['lineno']=476;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['close']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_355_err){if (!$p['isinstance']($pyjs_dbg_355_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_355_err);}throw $pyjs_dbg_355_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['doApply'] = $method;
			$pyjs['track']['lineno']=478;
			$method = $pyjs__bind_method2('doCancel', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '39282267022dc69f436cbe6650a7e2d9') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':478};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=478;
				$pyjs['track']['lineno']=479;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['close']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_356_err){if (!$p['isinstance']($pyjs_dbg_356_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_356_err);}throw $pyjs_dbg_356_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['doCancel'] = $method;
			$pyjs['track']['lineno']=481;
			$method = $pyjs__bind_method2('doSelectAll', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '39282267022dc69f436cbe6650a7e2d9') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var $iter10_nextval,cb,$iter10_array,$iter10_type,$iter10_iter,$pyjs__trackstack_size_1,$iter10_idx;
				$pyjs['track']={'module':'actions.list', 'lineno':481};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=481;
				$pyjs['track']['lineno']=482;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter10_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'checkboxes');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_357_err){if (!$p['isinstance']($pyjs_dbg_357_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_357_err);}throw $pyjs_dbg_357_err;
}})();
				$iter10_nextval=$p['__iter_prepare']($iter10_iter,false);
				while (typeof($p['__wrapped_next']($iter10_nextval)['$nextval']) != 'undefined') {
					cb = $iter10_nextval['$nextval'];
					$pyjs['track']['lineno']=483;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_eq'](cb['__getitem__']('checked'), false));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_358_err){if (!$p['isinstance']($pyjs_dbg_358_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_358_err);}throw $pyjs_dbg_358_err;
}})()) {
						$pyjs['track']['lineno']=484;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return cb['__setitem__']('checked', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_359_err){if (!$p['isinstance']($pyjs_dbg_359_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_359_err);}throw $pyjs_dbg_359_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='actions.list';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['doSelectAll'] = $method;
			$pyjs['track']['lineno']=486;
			$method = $pyjs__bind_method2('doUnselectAll', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '39282267022dc69f436cbe6650a7e2d9') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var $iter11_type,$iter11_iter,$iter11_array,$pyjs__trackstack_size_1,$iter11_nextval,$iter11_idx,cb;
				$pyjs['track']={'module':'actions.list', 'lineno':486};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=486;
				$pyjs['track']['lineno']=487;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter11_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'checkboxes');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_360_err){if (!$p['isinstance']($pyjs_dbg_360_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_360_err);}throw $pyjs_dbg_360_err;
}})();
				$iter11_nextval=$p['__iter_prepare']($iter11_iter,false);
				while (typeof($p['__wrapped_next']($iter11_nextval)['$nextval']) != 'undefined') {
					cb = $iter11_nextval['$nextval'];
					$pyjs['track']['lineno']=488;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_eq'](cb['__getitem__']('checked'), true));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_361_err){if (!$p['isinstance']($pyjs_dbg_361_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_361_err);}throw $pyjs_dbg_361_err;
}})()) {
						$pyjs['track']['lineno']=489;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return cb['__setitem__']('checked', false);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_362_err){if (!$p['isinstance']($pyjs_dbg_362_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_362_err);}throw $pyjs_dbg_362_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='actions.list';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['doUnselectAll'] = $method;
			$pyjs['track']['lineno']=491;
			$method = $pyjs__bind_method2('doInvertSelection', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '39282267022dc69f436cbe6650a7e2d9') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var cb,$iter12_type,$iter12_array,$iter12_nextval,$iter12_iter,$pyjs__trackstack_size_1,$iter12_idx;
				$pyjs['track']={'module':'actions.list', 'lineno':491};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=491;
				$pyjs['track']['lineno']=492;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter12_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'checkboxes');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_363_err){if (!$p['isinstance']($pyjs_dbg_363_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_363_err);}throw $pyjs_dbg_363_err;
}})();
				$iter12_nextval=$p['__iter_prepare']($iter12_iter,false);
				while (typeof($p['__wrapped_next']($iter12_nextval)['$nextval']) != 'undefined') {
					cb = $iter12_nextval['$nextval'];
					$pyjs['track']['lineno']=493;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_eq'](cb['__getitem__']('checked'), false));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_364_err){if (!$p['isinstance']($pyjs_dbg_364_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_364_err);}throw $pyjs_dbg_364_err;
}})()) {
						$pyjs['track']['lineno']=494;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return cb['__setitem__']('checked', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_365_err){if (!$p['isinstance']($pyjs_dbg_365_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_365_err);}throw $pyjs_dbg_365_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=496;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return cb['__setitem__']('checked', false);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_366_err){if (!$p['isinstance']($pyjs_dbg_366_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_366_err);}throw $pyjs_dbg_366_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='actions.list';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['doInvertSelection'] = $method;
			$pyjs['track']['lineno']=406;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Popup'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('SelectFieldsPopup', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=498;
		$m['SelectFieldsAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = '303217e39ebb34466d56e16eade60b76';
			$pyjs['track']['lineno']=499;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '303217e39ebb34466d56e16eade60b76') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var $assign1;
				$pyjs['track']={'module':'actions.list', 'lineno':499};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=499;
				$pyjs['track']['lineno']=500;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectFieldsAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_367_err){if (!$p['isinstance']($pyjs_dbg_367_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_367_err);}throw $pyjs_dbg_367_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Select fields');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_368_err){if (!$p['isinstance']($pyjs_dbg_368_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_368_err);}throw $pyjs_dbg_368_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_369_err){if (!$p['isinstance']($pyjs_dbg_369_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_369_err);}throw $pyjs_dbg_369_err;
}})();
				$pyjs['track']['lineno']=501;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon selectfields');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_370_err){if (!$p['isinstance']($pyjs_dbg_370_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_370_err);}throw $pyjs_dbg_370_err;
}})();
				$pyjs['track']['lineno']=502;
				$assign1 = true;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('disabled', $assign1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_371_err){if (!$p['isinstance']($pyjs_dbg_371_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_371_err);}throw $pyjs_dbg_371_err;
}})();
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', $assign1) : $p['setattr'](self, 'isDisabled', $assign1); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=504;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '303217e39ebb34466d56e16eade60b76') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];

				$pyjs['track']={'module':'actions.list', 'lineno':504};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=504;
				$pyjs['track']['lineno']=505;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['SelectFieldsPopup']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_372_err){if (!$p['isinstance']($pyjs_dbg_372_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_372_err);}throw $pyjs_dbg_372_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_373_err){if (!$p['isinstance']($pyjs_dbg_373_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_373_err);}throw $pyjs_dbg_373_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_374_err){if (!$p['isinstance']($pyjs_dbg_374_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_374_err);}throw $pyjs_dbg_374_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=507;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '303217e39ebb34466d56e16eade60b76') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':507};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=507;
				$pyjs['track']['lineno']=508;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectFieldsAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_375_err){if (!$p['isinstance']($pyjs_dbg_375_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_375_err);}throw $pyjs_dbg_375_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_376_err){if (!$p['isinstance']($pyjs_dbg_376_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_376_err);}throw $pyjs_dbg_376_err;
}})();
				$pyjs['track']['lineno']=509;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_377_err){if (!$p['isinstance']($pyjs_dbg_377_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_377_err);}throw $pyjs_dbg_377_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_378_err){if (!$p['isinstance']($pyjs_dbg_378_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_378_err);}throw $pyjs_dbg_378_err;
}})()['tableChangedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_379_err){if (!$p['isinstance']($pyjs_dbg_379_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_379_err);}throw $pyjs_dbg_379_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=511;
			$method = $pyjs__bind_method2('onDetach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '303217e39ebb34466d56e16eade60b76') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':511};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=511;
				$pyjs['track']['lineno']=512;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_380_err){if (!$p['isinstance']($pyjs_dbg_380_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_380_err);}throw $pyjs_dbg_380_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_381_err){if (!$p['isinstance']($pyjs_dbg_381_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_381_err);}throw $pyjs_dbg_381_err;
}})()['tableChangedEvent']['unregister'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_382_err){if (!$p['isinstance']($pyjs_dbg_382_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_382_err);}throw $pyjs_dbg_382_err;
}})();
				$pyjs['track']['lineno']=513;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectFieldsAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_383_err){if (!$p['isinstance']($pyjs_dbg_383_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_383_err);}throw $pyjs_dbg_383_err;
}})()['onDetach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_384_err){if (!$p['isinstance']($pyjs_dbg_384_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_384_err);}throw $pyjs_dbg_384_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onDetach'] = $method;
			$pyjs['track']['lineno']=515;
			$method = $pyjs__bind_method2('onTableChanged', function(table, count) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					count = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '303217e39ebb34466d56e16eade60b76') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $assign3,$assign2;
				$pyjs['track']={'module':'actions.list', 'lineno':515};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=515;
				$pyjs['track']['lineno']=516;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp'](count, $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_385_err){if (!$p['isinstance']($pyjs_dbg_385_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_385_err);}throw $pyjs_dbg_385_err;
}})()) {
					$pyjs['track']['lineno']=517;
					$assign2 = false;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', $assign2);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_386_err){if (!$p['isinstance']($pyjs_dbg_386_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_386_err);}throw $pyjs_dbg_386_err;
}})();
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', $assign2) : $p['setattr'](self, 'isDisabled', $assign2); 
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, 'isDisabled')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_387_err){if (!$p['isinstance']($pyjs_dbg_387_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_387_err);}throw $pyjs_dbg_387_err;
}})()) {
					$pyjs['track']['lineno']=519;
					$assign3 = true;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', $assign3);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_388_err){if (!$p['isinstance']($pyjs_dbg_388_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_388_err);}throw $pyjs_dbg_388_err;
}})();
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', $assign3) : $p['setattr'](self, 'isDisabled', $assign3); 
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['count']]);
			$cls_definition['onTableChanged'] = $method;
			$pyjs['track']['lineno']=522;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);

				$pyjs['track']={'module':'actions.list', 'lineno':522};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=522;
				$pyjs['track']['lineno']=523;
				$pyjs['track']['lineno']=523;
				var $pyjs__ret = $p['op_eq'](actionName, 'selectfields');
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=498;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('SelectFieldsAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=525;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['SelectFieldsAction'], 'isSuitableFor'), $m['SelectFieldsAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_389_err){if (!$p['isinstance']($pyjs_dbg_389_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_389_err);}throw $pyjs_dbg_389_err;
}})();
		$pyjs['track']['lineno']=527;
		$m['ReloadAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = '39fa9cad93d43ba1a94d99acd9e08967';
			$pyjs['track']['lineno']=531;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '39fa9cad93d43ba1a94d99acd9e08967') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':531};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=531;
				$pyjs['track']['lineno']=532;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ReloadAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_390_err){if (!$p['isinstance']($pyjs_dbg_390_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_390_err);}throw $pyjs_dbg_390_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Reload');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_391_err){if (!$p['isinstance']($pyjs_dbg_391_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_391_err);}throw $pyjs_dbg_391_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_392_err){if (!$p['isinstance']($pyjs_dbg_392_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_392_err);}throw $pyjs_dbg_392_err;
}})();
				$pyjs['track']['lineno']=533;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon reload');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_393_err){if (!$p['isinstance']($pyjs_dbg_393_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_393_err);}throw $pyjs_dbg_393_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=536;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var correctHandler,correctAction,$and73,$or25,$or26,$and72;
				$pyjs['track']={'module':'actions.list', 'lineno':536};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=536;
				$pyjs['track']['lineno']=537;
				correctAction = $p['op_eq'](actionName, 'reload');
				$pyjs['track']['lineno']=538;
				correctHandler = ($p['bool']($or25=$p['op_eq'](handler, 'list'))?$or25:(function(){try{try{$pyjs['in_try_except'] += 1;
				return handler['startswith']('list.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_394_err){if (!$p['isinstance']($pyjs_dbg_394_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_394_err);}throw $pyjs_dbg_394_err;
}})());
				$pyjs['track']['lineno']=539;
				$pyjs['track']['lineno']=539;
				var $pyjs__ret = ($p['bool']($and72=correctAction)?correctHandler:$and72);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=541;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '39fa9cad93d43ba1a94d99acd9e08967') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];

				$pyjs['track']={'module':'actions.list', 'lineno':541};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=541;
				$pyjs['track']['lineno']=542;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__getitem__']('class')['append']('is_loading');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_395_err){if (!$p['isinstance']($pyjs_dbg_395_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_395_err);}throw $pyjs_dbg_395_err;
}})();
				$pyjs['track']['lineno']=543;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['notifyChange']($p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_396_err){if (!$p['isinstance']($pyjs_dbg_396_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_396_err);}throw $pyjs_dbg_396_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_397_err){if (!$p['isinstance']($pyjs_dbg_397_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_397_err);}throw $pyjs_dbg_397_err;
}})(), 'modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_398_err){if (!$p['isinstance']($pyjs_dbg_398_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_398_err);}throw $pyjs_dbg_398_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=545;
			$method = $pyjs__bind_method2('resetLoadingState', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '39fa9cad93d43ba1a94d99acd9e08967') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':545};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=545;
				$pyjs['track']['lineno']=546;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](self['__getitem__']('class')['__contains__']('is_loading'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_399_err){if (!$p['isinstance']($pyjs_dbg_399_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_399_err);}throw $pyjs_dbg_399_err;
}})()) {
					$pyjs['track']['lineno']=547;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__getitem__']('class')['remove']('is_loading');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_400_err){if (!$p['isinstance']($pyjs_dbg_400_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_400_err);}throw $pyjs_dbg_400_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['resetLoadingState'] = $method;
			$pyjs['track']['lineno']=527;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('ReloadAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=550;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['ReloadAction'], 'isSuitableFor'), $m['ReloadAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_401_err){if (!$p['isinstance']($pyjs_dbg_401_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_401_err);}throw $pyjs_dbg_401_err;
}})();
		$pyjs['track']['lineno']=553;
		$m['ListSelectFilterAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = '2771ed35e77f74b5c7e34be6da990605';
			$pyjs['track']['lineno']=554;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2771ed35e77f74b5c7e34be6da990605') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':554};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=554;
				$pyjs['track']['lineno']=555;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ListSelectFilterAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_402_err){if (!$p['isinstance']($pyjs_dbg_402_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_402_err);}throw $pyjs_dbg_402_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Select Filter');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_403_err){if (!$p['isinstance']($pyjs_dbg_403_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_403_err);}throw $pyjs_dbg_403_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_404_err){if (!$p['isinstance']($pyjs_dbg_404_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_404_err);}throw $pyjs_dbg_404_err;
}})();
				$pyjs['track']['lineno']=556;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon selectfilter');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_405_err){if (!$p['isinstance']($pyjs_dbg_405_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_405_err);}throw $pyjs_dbg_405_err;
}})();
				$pyjs['track']['lineno']=557;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('urls', null) : $p['setattr'](self, 'urls', null); 
				$pyjs['track']['lineno']=558;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('filterSelector', null) : $p['setattr'](self, 'filterSelector', null); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=560;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2771ed35e77f74b5c7e34be6da990605') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and74,$and75,$and76,$or28,$or27,modul,modulConfig;
				$pyjs['track']={'module':'actions.list', 'lineno':560};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=560;
				$pyjs['track']['lineno']=561;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ListSelectFilterAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_406_err){if (!$p['isinstance']($pyjs_dbg_406_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_406_err);}throw $pyjs_dbg_406_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_407_err){if (!$p['isinstance']($pyjs_dbg_407_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_407_err);}throw $pyjs_dbg_407_err;
}})();
				$pyjs['track']['lineno']=562;
				modul = $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_408_err){if (!$p['isinstance']($pyjs_dbg_408_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_408_err);}throw $pyjs_dbg_408_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_409_err){if (!$p['isinstance']($pyjs_dbg_409_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_409_err);}throw $pyjs_dbg_409_err;
}})(), 'modul');
				$pyjs['track']['lineno']=563;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_410_err){if (!$p['isinstance']($pyjs_dbg_410_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_410_err);}throw $pyjs_dbg_410_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_411_err){if (!$p['isinstance']($pyjs_dbg_411_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_411_err);}throw $pyjs_dbg_411_err;
}})(), 'filterID'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_412_err){if (!$p['isinstance']($pyjs_dbg_412_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_412_err);}throw $pyjs_dbg_412_err;
}})()) {
					$pyjs['track']['lineno']=565;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_413_err){if (!$p['isinstance']($pyjs_dbg_413_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_413_err);}throw $pyjs_dbg_413_err;
}})();
				}
				$pyjs['track']['lineno']=566;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_414_err){if (!$p['isinstance']($pyjs_dbg_414_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_414_err);}throw $pyjs_dbg_414_err;
}})()['__contains__'](modul));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_415_err){if (!$p['isinstance']($pyjs_dbg_415_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_415_err);}throw $pyjs_dbg_415_err;
}})()) {
					$pyjs['track']['lineno']=567;
					modulConfig = $m['conf']['__getitem__']('modules')['__getitem__'](modul);
					$pyjs['track']['lineno']=568;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and74=(function(){try{try{$pyjs['in_try_except'] += 1;
					return modulConfig['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_416_err){if (!$p['isinstance']($pyjs_dbg_416_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_416_err);}throw $pyjs_dbg_416_err;
}})()['__contains__']('disabledFunctions'))?($p['bool']($and75=modulConfig['__getitem__']('disabledFunctions'))?modulConfig['__getitem__']('disabledFunctions')['__contains__']('fulltext-search'):$and75):$and74));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_417_err){if (!$p['isinstance']($pyjs_dbg_417_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_417_err);}throw $pyjs_dbg_417_err;
}})()) {
						$pyjs['track']['lineno']=570;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](($p['bool']($or27=!$p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return modulConfig['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_418_err){if (!$p['isinstance']($pyjs_dbg_418_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_418_err);}throw $pyjs_dbg_418_err;
}})()['__contains__']('views')))?$or27:!$p['bool'](modulConfig['__getitem__']('views'))));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_419_err){if (!$p['isinstance']($pyjs_dbg_419_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_419_err);}throw $pyjs_dbg_419_err;
}})()) {
							$pyjs['track']['lineno']=572;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return self['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_420_err){if (!$p['isinstance']($pyjs_dbg_420_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_420_err);}throw $pyjs_dbg_420_err;
}})();
						}
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=574;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2771ed35e77f74b5c7e34be6da990605') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];

				$pyjs['track']={'module':'actions.list', 'lineno':574};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=574;
				$pyjs['track']['lineno']=575;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_421_err){if (!$p['isinstance']($pyjs_dbg_421_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_421_err);}throw $pyjs_dbg_421_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_422_err){if (!$p['isinstance']($pyjs_dbg_422_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_422_err);}throw $pyjs_dbg_422_err;
}})()['sideBar']['getWidget']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_423_err){if (!$p['isinstance']($pyjs_dbg_423_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_423_err);}throw $pyjs_dbg_423_err;
}})(), $m['FilterSelector']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_424_err){if (!$p['isinstance']($pyjs_dbg_424_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_424_err);}throw $pyjs_dbg_424_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_425_err){if (!$p['isinstance']($pyjs_dbg_425_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_425_err);}throw $pyjs_dbg_425_err;
}})()) {
					$pyjs['track']['lineno']=576;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_426_err){if (!$p['isinstance']($pyjs_dbg_426_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_426_err);}throw $pyjs_dbg_426_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_427_err){if (!$p['isinstance']($pyjs_dbg_427_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_427_err);}throw $pyjs_dbg_427_err;
}})()['sideBar']['setWidget'](null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_428_err){if (!$p['isinstance']($pyjs_dbg_428_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_428_err);}throw $pyjs_dbg_428_err;
}})();
					$pyjs['track']['lineno']=577;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('filterSelector', null) : $p['setattr'](self, 'filterSelector', null); 
				}
				else {
					$pyjs['track']['lineno']=579;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('filterSelector', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['FilterSelector']($p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_429_err){if (!$p['isinstance']($pyjs_dbg_429_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_429_err);}throw $pyjs_dbg_429_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_430_err){if (!$p['isinstance']($pyjs_dbg_430_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_430_err);}throw $pyjs_dbg_430_err;
}})(), 'modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_431_err){if (!$p['isinstance']($pyjs_dbg_431_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_431_err);}throw $pyjs_dbg_431_err;
}})()) : $p['setattr'](self, 'filterSelector', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['FilterSelector']($p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_429_err){if (!$p['isinstance']($pyjs_dbg_429_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_429_err);}throw $pyjs_dbg_429_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_430_err){if (!$p['isinstance']($pyjs_dbg_430_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_430_err);}throw $pyjs_dbg_430_err;
}})(), 'modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_431_err){if (!$p['isinstance']($pyjs_dbg_431_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_431_err);}throw $pyjs_dbg_431_err;
}})()); 
					$pyjs['track']['lineno']=580;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_432_err){if (!$p['isinstance']($pyjs_dbg_432_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_432_err);}throw $pyjs_dbg_432_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_433_err){if (!$p['isinstance']($pyjs_dbg_433_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_433_err);}throw $pyjs_dbg_433_err;
}})()['sideBar']['setWidget']($p['getattr'](self, 'filterSelector'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_434_err){if (!$p['isinstance']($pyjs_dbg_434_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_434_err);}throw $pyjs_dbg_434_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=583;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var $and79,correctHandler,isDisabled,$or29,$and81,$and80,$and83,$and82,$and85,$and84,$and86,correctAction,$add17,$add18,$and78,hasAccess,$and77,$or32,$or31,$or30;
				$pyjs['track']={'module':'actions.list', 'lineno':583};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=583;
				$pyjs['track']['lineno']=584;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](modul, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_435_err){if (!$p['isinstance']($pyjs_dbg_435_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_435_err);}throw $pyjs_dbg_435_err;
}})()) {
					$pyjs['track']['lineno']=585;
					$pyjs['track']['lineno']=585;
					var $pyjs__ret = false;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=586;
				correctAction = $p['op_eq'](actionName, 'selectfilter');
				$pyjs['track']['lineno']=587;
				correctHandler = ($p['bool']($or29=$p['op_eq'](handler, 'list'))?$or29:(function(){try{try{$pyjs['in_try_except'] += 1;
				return handler['startswith']('list.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_436_err){if (!$p['isinstance']($pyjs_dbg_436_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_436_err);}throw $pyjs_dbg_436_err;
}})());
				$pyjs['track']['lineno']=588;
				hasAccess = ($p['bool']($and77=$m['conf']['__getitem__']('currentUser'))?($p['bool']($or31=$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']('root'))?$or31:$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']($p['__op_add']($add17=modul,$add18='-view'))):$and77);
				$pyjs['track']['lineno']=589;
				isDisabled = ($p['bool']($and79=!$p['op_is'](modul, null))?($p['bool']($and80=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['__getitem__'](modul)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_437_err){if (!$p['isinstance']($pyjs_dbg_437_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_437_err);}throw $pyjs_dbg_437_err;
}})()['__contains__']('disabledFunctions'))?($p['bool']($and81=$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions'))?$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions')['__contains__']('view'):$and81):$and80):$and79);
				$pyjs['track']['lineno']=590;
				$pyjs['track']['lineno']=590;
				var $pyjs__ret = ($p['bool']($and83=correctAction)?($p['bool']($and84=correctHandler)?($p['bool']($and85=hasAccess)?!$p['bool'](isDisabled):$and85):$and84):$and83);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=553;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('ListSelectFilterAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=592;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['ListSelectFilterAction'], 'isSuitableFor'), $m['ListSelectFilterAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_438_err){if (!$p['isinstance']($pyjs_dbg_438_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_438_err);}throw $pyjs_dbg_438_err;
}})();
		$pyjs['track']['lineno']=594;
		$m['RecurrentDateAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = '82b74c0ba2c9c2c7ad111ee05c0bbe09';
			$pyjs['track']['lineno']=599;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '82b74c0ba2c9c2c7ad111ee05c0bbe09') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':599};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=599;
				$pyjs['track']['lineno']=600;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['RecurrentDateAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_439_err){if (!$p['isinstance']($pyjs_dbg_439_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_439_err);}throw $pyjs_dbg_439_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Recurrent Events');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_440_err){if (!$p['isinstance']($pyjs_dbg_440_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_440_err);}throw $pyjs_dbg_440_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_441_err){if (!$p['isinstance']($pyjs_dbg_441_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_441_err);}throw $pyjs_dbg_441_err;
}})();
				$pyjs['track']['lineno']=601;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon createrecurrent_small');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_442_err){if (!$p['isinstance']($pyjs_dbg_442_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_442_err);}throw $pyjs_dbg_442_err;
}})();
				$pyjs['track']['lineno']=602;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_443_err){if (!$p['isinstance']($pyjs_dbg_443_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_443_err);}throw $pyjs_dbg_443_err;
}})();
				$pyjs['track']['lineno']=603;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', true) : $p['setattr'](self, 'isDisabled', true); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=605;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '82b74c0ba2c9c2c7ad111ee05c0bbe09') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':605};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=605;
				$pyjs['track']['lineno']=606;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['RecurrentDateAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_444_err){if (!$p['isinstance']($pyjs_dbg_444_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_444_err);}throw $pyjs_dbg_444_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_445_err){if (!$p['isinstance']($pyjs_dbg_445_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_445_err);}throw $pyjs_dbg_445_err;
}})();
				$pyjs['track']['lineno']=607;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_446_err){if (!$p['isinstance']($pyjs_dbg_446_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_446_err);}throw $pyjs_dbg_446_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_447_err){if (!$p['isinstance']($pyjs_dbg_447_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_447_err);}throw $pyjs_dbg_447_err;
}})()['selectionChangedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_448_err){if (!$p['isinstance']($pyjs_dbg_448_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_448_err);}throw $pyjs_dbg_448_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=609;
			$method = $pyjs__bind_method2('onDetach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '82b74c0ba2c9c2c7ad111ee05c0bbe09') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':609};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=609;
				$pyjs['track']['lineno']=610;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_449_err){if (!$p['isinstance']($pyjs_dbg_449_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_449_err);}throw $pyjs_dbg_449_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_450_err){if (!$p['isinstance']($pyjs_dbg_450_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_450_err);}throw $pyjs_dbg_450_err;
}})()['selectionChangedEvent']['unregister'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_451_err){if (!$p['isinstance']($pyjs_dbg_451_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_451_err);}throw $pyjs_dbg_451_err;
}})();
				$pyjs['track']['lineno']=611;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['RecurrentDateAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_452_err){if (!$p['isinstance']($pyjs_dbg_452_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_452_err);}throw $pyjs_dbg_452_err;
}})()['onDetach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_453_err){if (!$p['isinstance']($pyjs_dbg_453_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_453_err);}throw $pyjs_dbg_453_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onDetach'] = $method;
			$pyjs['track']['lineno']=613;
			$method = $pyjs__bind_method2('onSelectionChanged', function(table, selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					selection = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '82b74c0ba2c9c2c7ad111ee05c0bbe09') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':613};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=613;
				$pyjs['track']['lineno']=614;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](selection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_454_err){if (!$p['isinstance']($pyjs_dbg_454_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_454_err);}throw $pyjs_dbg_454_err;
}})(), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_455_err){if (!$p['isinstance']($pyjs_dbg_455_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_455_err);}throw $pyjs_dbg_455_err;
}})()) {
					$pyjs['track']['lineno']=615;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'isDisabled'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_456_err){if (!$p['isinstance']($pyjs_dbg_456_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_456_err);}throw $pyjs_dbg_456_err;
}})()) {
						$pyjs['track']['lineno']=616;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', false) : $p['setattr'](self, 'isDisabled', false); 
					}
					$pyjs['track']['lineno']=617;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', false);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_457_err){if (!$p['isinstance']($pyjs_dbg_457_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_457_err);}throw $pyjs_dbg_457_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=619;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool']($p['getattr'](self, 'isDisabled')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_458_err){if (!$p['isinstance']($pyjs_dbg_458_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_458_err);}throw $pyjs_dbg_458_err;
}})()) {
						$pyjs['track']['lineno']=620;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_459_err){if (!$p['isinstance']($pyjs_dbg_459_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_459_err);}throw $pyjs_dbg_459_err;
}})();
						$pyjs['track']['lineno']=621;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', true) : $p['setattr'](self, 'isDisabled', true); 
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['selection']]);
			$cls_definition['onSelectionChanged'] = $method;
			$pyjs['track']['lineno']=624;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var correctHandler,$add20,isDisabled,$and89,$and88,$and87,correctAction,$add19,hasAccess,$or33,$or36,$or35,$or34,$and92,$and93,$and90,$and91,$and96,$and94,$and95;
				$pyjs['track']={'module':'actions.list', 'lineno':624};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=624;
				$pyjs['track']['lineno']=625;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](modul, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_460_err){if (!$p['isinstance']($pyjs_dbg_460_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_460_err);}throw $pyjs_dbg_460_err;
}})()) {
					$pyjs['track']['lineno']=626;
					$pyjs['track']['lineno']=626;
					var $pyjs__ret = false;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=627;
				correctAction = $p['op_eq'](actionName, 'repeatdate');
				$pyjs['track']['lineno']=628;
				correctHandler = ($p['bool']($or33=$p['op_eq'](handler, 'list.calendar'))?$or33:(function(){try{try{$pyjs['in_try_except'] += 1;
				return handler['startswith']('list.calendar.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_461_err){if (!$p['isinstance']($pyjs_dbg_461_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_461_err);}throw $pyjs_dbg_461_err;
}})());
				$pyjs['track']['lineno']=629;
				hasAccess = ($p['bool']($and87=$m['conf']['__getitem__']('currentUser'))?($p['bool']($or35=$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']('root'))?$or35:$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']($p['__op_add']($add19=modul,$add20='-edit'))):$and87);
				$pyjs['track']['lineno']=630;
				isDisabled = ($p['bool']($and89=!$p['op_is'](modul, null))?($p['bool']($and90=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['__getitem__'](modul)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_462_err){if (!$p['isinstance']($pyjs_dbg_462_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_462_err);}throw $pyjs_dbg_462_err;
}})()['__contains__']('disabledFunctions'))?($p['bool']($and91=$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions'))?$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('disabledFunctions')['__contains__']('edit'):$and91):$and90):$and89);
				$pyjs['track']['lineno']=631;
				$pyjs['track']['lineno']=631;
				var $pyjs__ret = ($p['bool']($and93=correctAction)?($p['bool']($and94=correctHandler)?($p['bool']($and95=hasAccess)?!$p['bool'](isDisabled):$and95):$and94):$and93);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=633;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '82b74c0ba2c9c2c7ad111ee05c0bbe09') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];
				var selection,$iter13_nextval,$iter13_iter,$iter13_array,s,$iter13_idx,$pyjs__trackstack_size_1,$iter13_type;
				$pyjs['track']={'module':'actions.list', 'lineno':633};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=633;
				$pyjs['track']['lineno']=634;
				selection = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_463_err){if (!$p['isinstance']($pyjs_dbg_463_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_463_err);}throw $pyjs_dbg_463_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_464_err){if (!$p['isinstance']($pyjs_dbg_464_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_464_err);}throw $pyjs_dbg_464_err;
}})()['getCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_465_err){if (!$p['isinstance']($pyjs_dbg_465_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_465_err);}throw $pyjs_dbg_465_err;
}})();
				$pyjs['track']['lineno']=635;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool'](selection));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_466_err){if (!$p['isinstance']($pyjs_dbg_466_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_466_err);}throw $pyjs_dbg_466_err;
}})()) {
					$pyjs['track']['lineno']=636;
					$pyjs['track']['lineno']=636;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=637;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter13_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return selection;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_467_err){if (!$p['isinstance']($pyjs_dbg_467_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_467_err);}throw $pyjs_dbg_467_err;
}})();
				$iter13_nextval=$p['__iter_prepare']($iter13_iter,false);
				while (typeof($p['__wrapped_next']($iter13_nextval)['$nextval']) != 'undefined') {
					s = $iter13_nextval['$nextval'];
					$pyjs['track']['lineno']=638;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['openEditor'](s['__getitem__']('id'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_468_err){if (!$p['isinstance']($pyjs_dbg_468_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_468_err);}throw $pyjs_dbg_468_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='actions.list';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=640;
			$method = $pyjs__bind_method2('openEditor', function(id) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					id = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '82b74c0ba2c9c2c7ad111ee05c0bbe09') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var edwg,pane;
				$pyjs['track']={'module':'actions.list', 'lineno':640};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=640;
				$pyjs['track']['lineno']=641;
				pane = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['Pane'], null, null, [{'closeable':true, 'iconClasses':(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('modul_%s', $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_470_err){if (!$p['isinstance']($pyjs_dbg_470_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_470_err);}throw $pyjs_dbg_470_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_471_err){if (!$p['isinstance']($pyjs_dbg_471_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_471_err);}throw $pyjs_dbg_471_err;
}})(), 'modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_472_err){if (!$p['isinstance']($pyjs_dbg_472_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_472_err);}throw $pyjs_dbg_472_err;
}})(), 'apptype_list', 'action_edit']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_473_err){if (!$p['isinstance']($pyjs_dbg_473_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_473_err);}throw $pyjs_dbg_473_err;
}})()}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Recurrent Events');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_469_err){if (!$p['isinstance']($pyjs_dbg_469_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_469_err);}throw $pyjs_dbg_469_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_474_err){if (!$p['isinstance']($pyjs_dbg_474_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_474_err);}throw $pyjs_dbg_474_err;
}})();
				$pyjs['track']['lineno']=642;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['stackPane'](pane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_475_err){if (!$p['isinstance']($pyjs_dbg_475_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_475_err);}throw $pyjs_dbg_475_err;
}})();
				$pyjs['track']['lineno']=643;
				edwg = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['RepeatDatePopup'], null, null, [{'key':id}, $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_476_err){if (!$p['isinstance']($pyjs_dbg_476_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_476_err);}throw $pyjs_dbg_476_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_477_err){if (!$p['isinstance']($pyjs_dbg_477_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_477_err);}throw $pyjs_dbg_477_err;
}})(), 'modul')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_478_err){if (!$p['isinstance']($pyjs_dbg_478_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_478_err);}throw $pyjs_dbg_478_err;
}})();
				$pyjs['track']['lineno']=644;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return pane['addWidget'](edwg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_479_err){if (!$p['isinstance']($pyjs_dbg_479_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_479_err);}throw $pyjs_dbg_479_err;
}})();
				$pyjs['track']['lineno']=645;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return pane['focus']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_480_err){if (!$p['isinstance']($pyjs_dbg_480_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_480_err);}throw $pyjs_dbg_480_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['id']]);
			$cls_definition['openEditor'] = $method;
			$pyjs['track']['lineno']=647;
			$method = $pyjs__bind_method2('resetLoadingState', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '82b74c0ba2c9c2c7ad111ee05c0bbe09') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':647};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=647;
				$pyjs['track']['lineno']=648;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['resetLoadingState'] = $method;
			$pyjs['track']['lineno']=594;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('RecurrentDateAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=650;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['RecurrentDateAction'], 'isSuitableFor'), $m['RecurrentDateAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_481_err){if (!$p['isinstance']($pyjs_dbg_481_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_481_err);}throw $pyjs_dbg_481_err;
}})();
		$pyjs['track']['lineno']=654;
		$m['CreateRecurrentAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = 'cafb54bfe7ea8f860106d3a0c357d9ec';
			$pyjs['track']['lineno']=655;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'cafb54bfe7ea8f860106d3a0c357d9ec') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':655};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=655;
				$pyjs['track']['lineno']=656;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['CreateRecurrentAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_482_err){if (!$p['isinstance']($pyjs_dbg_482_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_482_err);}throw $pyjs_dbg_482_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Save-Close');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_483_err){if (!$p['isinstance']($pyjs_dbg_483_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_483_err);}throw $pyjs_dbg_483_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_484_err){if (!$p['isinstance']($pyjs_dbg_484_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_484_err);}throw $pyjs_dbg_484_err;
}})();
				$pyjs['track']['lineno']=657;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon save close');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_485_err){if (!$p['isinstance']($pyjs_dbg_485_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_485_err);}throw $pyjs_dbg_485_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=660;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);

				$pyjs['track']={'module':'actions.list', 'lineno':660};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=660;
				$pyjs['track']['lineno']=661;
				$pyjs['track']['lineno']=661;
				var $pyjs__ret = $p['op_eq'](actionName, 'create.recurrent');
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=663;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'cafb54bfe7ea8f860106d3a0c357d9ec') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];

				$pyjs['track']={'module':'actions.list', 'lineno':663};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=663;
				$pyjs['track']['lineno']=664;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__getitem__']('class')['append']('is_loading');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_486_err){if (!$p['isinstance']($pyjs_dbg_486_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_486_err);}throw $pyjs_dbg_486_err;
}})();
				$pyjs['track']['lineno']=665;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_487_err){if (!$p['isinstance']($pyjs_dbg_487_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_487_err);}throw $pyjs_dbg_487_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_488_err){if (!$p['isinstance']($pyjs_dbg_488_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_488_err);}throw $pyjs_dbg_488_err;
}})(), 'doSave', null, null, [{'closeOnSuccess':true}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_489_err){if (!$p['isinstance']($pyjs_dbg_489_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_489_err);}throw $pyjs_dbg_489_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=654;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('CreateRecurrentAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=667;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['CreateRecurrentAction'], 'isSuitableFor'), $m['CreateRecurrentAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_490_err){if (!$p['isinstance']($pyjs_dbg_490_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_490_err);}throw $pyjs_dbg_490_err;
}})();
		$pyjs['track']['lineno']=670;
		$m['CsvExportAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = '9e77cdc311648c1db9fa2d8a09d46031';
			$pyjs['track']['lineno']=671;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '9e77cdc311648c1db9fa2d8a09d46031') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':671};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=671;
				$pyjs['track']['lineno']=672;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['CsvExportAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_491_err){if (!$p['isinstance']($pyjs_dbg_491_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_491_err);}throw $pyjs_dbg_491_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Export Csv');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_492_err){if (!$p['isinstance']($pyjs_dbg_492_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_492_err);}throw $pyjs_dbg_492_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_493_err){if (!$p['isinstance']($pyjs_dbg_493_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_493_err);}throw $pyjs_dbg_493_err;
}})();
				$pyjs['track']['lineno']=673;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon download');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_494_err){if (!$p['isinstance']($pyjs_dbg_494_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_494_err);}throw $pyjs_dbg_494_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=676;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var $and98,$or37,$or38,$and97;
				$pyjs['track']={'module':'actions.list', 'lineno':676};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=676;
				$pyjs['track']['lineno']=677;
				$pyjs['track']['lineno']=677;
				var $pyjs__ret = ($p['bool']($and97=$p['op_eq'](actionName, 'exportcsv'))?($p['bool']($or37=$p['op_eq'](handler, 'list'))?$or37:(function(){try{try{$pyjs['in_try_except'] += 1;
				return handler['startswith']('list.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_495_err){if (!$p['isinstance']($pyjs_dbg_495_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_495_err);}throw $pyjs_dbg_495_err;
}})()):$and97);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=679;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '9e77cdc311648c1db9fa2d8a09d46031') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];
				var edwg,pane;
				$pyjs['track']={'module':'actions.list', 'lineno':679};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=679;
				$pyjs['track']['lineno']=680;
				pane = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['Pane'], null, null, [{'closeable':true, 'iconClasses':(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('modul_%s', $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_497_err){if (!$p['isinstance']($pyjs_dbg_497_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_497_err);}throw $pyjs_dbg_497_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_498_err){if (!$p['isinstance']($pyjs_dbg_498_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_498_err);}throw $pyjs_dbg_498_err;
}})(), 'modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_499_err){if (!$p['isinstance']($pyjs_dbg_499_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_499_err);}throw $pyjs_dbg_499_err;
}})(), 'apptype_list', 'exportcsv']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_500_err){if (!$p['isinstance']($pyjs_dbg_500_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_500_err);}throw $pyjs_dbg_500_err;
}})()}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Csv Exporter');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_496_err){if (!$p['isinstance']($pyjs_dbg_496_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_496_err);}throw $pyjs_dbg_496_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_501_err){if (!$p['isinstance']($pyjs_dbg_501_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_501_err);}throw $pyjs_dbg_501_err;
}})();
				$pyjs['track']['lineno']=681;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['stackPane'](pane);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_502_err){if (!$p['isinstance']($pyjs_dbg_502_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_502_err);}throw $pyjs_dbg_502_err;
}})();
				$pyjs['track']['lineno']=682;
				edwg = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['CsvExport']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_503_err){if (!$p['isinstance']($pyjs_dbg_503_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_503_err);}throw $pyjs_dbg_503_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_504_err){if (!$p['isinstance']($pyjs_dbg_504_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_504_err);}throw $pyjs_dbg_504_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_505_err){if (!$p['isinstance']($pyjs_dbg_505_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_505_err);}throw $pyjs_dbg_505_err;
}})();
				$pyjs['track']['lineno']=683;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return pane['addWidget'](edwg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_506_err){if (!$p['isinstance']($pyjs_dbg_506_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_506_err);}throw $pyjs_dbg_506_err;
}})();
				$pyjs['track']['lineno']=684;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return pane['focus']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_507_err){if (!$p['isinstance']($pyjs_dbg_507_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_507_err);}throw $pyjs_dbg_507_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=670;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('CsvExportAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=686;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['CsvExportAction'], 'isSuitableFor'), $m['CsvExportAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_508_err){if (!$p['isinstance']($pyjs_dbg_508_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_508_err);}throw $pyjs_dbg_508_err;
}})();
		$pyjs['track']['lineno']=689;
		$m['SelectAllAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = '0bc961a714ea9de76416e5242713a8b0';
			$pyjs['track']['lineno']=690;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0bc961a714ea9de76416e5242713a8b0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var $assign4;
				$pyjs['track']={'module':'actions.list', 'lineno':690};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=690;
				$pyjs['track']['lineno']=691;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectAllAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_509_err){if (!$p['isinstance']($pyjs_dbg_509_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_509_err);}throw $pyjs_dbg_509_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Select all');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_510_err){if (!$p['isinstance']($pyjs_dbg_510_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_510_err);}throw $pyjs_dbg_510_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_511_err){if (!$p['isinstance']($pyjs_dbg_511_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_511_err);}throw $pyjs_dbg_511_err;
}})();
				$pyjs['track']['lineno']=692;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon selectall');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_512_err){if (!$p['isinstance']($pyjs_dbg_512_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_512_err);}throw $pyjs_dbg_512_err;
}})();
				$pyjs['track']['lineno']=693;
				$assign4 = true;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('disabled', $assign4);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_513_err){if (!$p['isinstance']($pyjs_dbg_513_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_513_err);}throw $pyjs_dbg_513_err;
}})();
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', $assign4) : $p['setattr'](self, 'isDisabled', $assign4); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=696;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var $and100,$and99,$or39,$or40;
				$pyjs['track']={'module':'actions.list', 'lineno':696};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=696;
				$pyjs['track']['lineno']=697;
				$pyjs['track']['lineno']=697;
				var $pyjs__ret = ($p['bool']($and99=$p['op_eq'](actionName, 'selectall'))?($p['bool']($or39=$p['op_eq'](handler, 'list'))?$or39:(function(){try{try{$pyjs['in_try_except'] += 1;
				return handler['startswith']('list.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_514_err){if (!$p['isinstance']($pyjs_dbg_514_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_514_err);}throw $pyjs_dbg_514_err;
}})()):$and99);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=699;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0bc961a714ea9de76416e5242713a8b0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];
				var cnt;
				$pyjs['track']={'module':'actions.list', 'lineno':699};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=699;
				$pyjs['track']['lineno']=700;
				cnt = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_515_err){if (!$p['isinstance']($pyjs_dbg_515_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_515_err);}throw $pyjs_dbg_515_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_516_err){if (!$p['isinstance']($pyjs_dbg_516_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_516_err);}throw $pyjs_dbg_516_err;
}})()['table']['table']['selectAll']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_517_err){if (!$p['isinstance']($pyjs_dbg_517_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_517_err);}throw $pyjs_dbg_517_err;
}})();
				$pyjs['track']['lineno']=701;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['log']('info', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['translate'], null, null, [{'items':cnt}, '{items} items had been selected']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_518_err){if (!$p['isinstance']($pyjs_dbg_518_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_518_err);}throw $pyjs_dbg_518_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_519_err){if (!$p['isinstance']($pyjs_dbg_519_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_519_err);}throw $pyjs_dbg_519_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=703;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0bc961a714ea9de76416e5242713a8b0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':703};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=703;
				$pyjs['track']['lineno']=704;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectAllAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_520_err){if (!$p['isinstance']($pyjs_dbg_520_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_520_err);}throw $pyjs_dbg_520_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_521_err){if (!$p['isinstance']($pyjs_dbg_521_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_521_err);}throw $pyjs_dbg_521_err;
}})();
				$pyjs['track']['lineno']=705;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_522_err){if (!$p['isinstance']($pyjs_dbg_522_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_522_err);}throw $pyjs_dbg_522_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_523_err){if (!$p['isinstance']($pyjs_dbg_523_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_523_err);}throw $pyjs_dbg_523_err;
}})()['tableChangedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_524_err){if (!$p['isinstance']($pyjs_dbg_524_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_524_err);}throw $pyjs_dbg_524_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=707;
			$method = $pyjs__bind_method2('onDetach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0bc961a714ea9de76416e5242713a8b0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':707};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=707;
				$pyjs['track']['lineno']=708;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_525_err){if (!$p['isinstance']($pyjs_dbg_525_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_525_err);}throw $pyjs_dbg_525_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_526_err){if (!$p['isinstance']($pyjs_dbg_526_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_526_err);}throw $pyjs_dbg_526_err;
}})()['tableChangedEvent']['unregister'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_527_err){if (!$p['isinstance']($pyjs_dbg_527_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_527_err);}throw $pyjs_dbg_527_err;
}})();
				$pyjs['track']['lineno']=709;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectAllAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_528_err){if (!$p['isinstance']($pyjs_dbg_528_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_528_err);}throw $pyjs_dbg_528_err;
}})()['onDetach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_529_err){if (!$p['isinstance']($pyjs_dbg_529_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_529_err);}throw $pyjs_dbg_529_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onDetach'] = $method;
			$pyjs['track']['lineno']=711;
			$method = $pyjs__bind_method2('onTableChanged', function(table, count) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					count = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0bc961a714ea9de76416e5242713a8b0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $assign6,$assign5;
				$pyjs['track']={'module':'actions.list', 'lineno':711};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=711;
				$pyjs['track']['lineno']=712;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp'](count, $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_530_err){if (!$p['isinstance']($pyjs_dbg_530_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_530_err);}throw $pyjs_dbg_530_err;
}})()) {
					$pyjs['track']['lineno']=713;
					$assign5 = false;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', $assign5);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_531_err){if (!$p['isinstance']($pyjs_dbg_531_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_531_err);}throw $pyjs_dbg_531_err;
}})();
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', $assign5) : $p['setattr'](self, 'isDisabled', $assign5); 
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, 'isDisabled')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_532_err){if (!$p['isinstance']($pyjs_dbg_532_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_532_err);}throw $pyjs_dbg_532_err;
}})()) {
					$pyjs['track']['lineno']=715;
					$assign6 = true;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', $assign6);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_533_err){if (!$p['isinstance']($pyjs_dbg_533_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_533_err);}throw $pyjs_dbg_533_err;
}})();
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', $assign6) : $p['setattr'](self, 'isDisabled', $assign6); 
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['count']]);
			$cls_definition['onTableChanged'] = $method;
			$pyjs['track']['lineno']=689;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('SelectAllAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=717;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['SelectAllAction'], 'isSuitableFor'), $m['SelectAllAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_534_err){if (!$p['isinstance']($pyjs_dbg_534_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_534_err);}throw $pyjs_dbg_534_err;
}})();
		$pyjs['track']['lineno']=720;
		$m['UnSelectAllAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = 'a0238e1c62697b408437ae917f87e3a2';
			$pyjs['track']['lineno']=721;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a0238e1c62697b408437ae917f87e3a2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var $assign7;
				$pyjs['track']={'module':'actions.list', 'lineno':721};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=721;
				$pyjs['track']['lineno']=722;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['UnSelectAllAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_535_err){if (!$p['isinstance']($pyjs_dbg_535_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_535_err);}throw $pyjs_dbg_535_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Unselect all');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_536_err){if (!$p['isinstance']($pyjs_dbg_536_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_536_err);}throw $pyjs_dbg_536_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_537_err){if (!$p['isinstance']($pyjs_dbg_537_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_537_err);}throw $pyjs_dbg_537_err;
}})();
				$pyjs['track']['lineno']=723;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon unselectall');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_538_err){if (!$p['isinstance']($pyjs_dbg_538_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_538_err);}throw $pyjs_dbg_538_err;
}})();
				$pyjs['track']['lineno']=724;
				$assign7 = true;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('disabled', $assign7);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_539_err){if (!$p['isinstance']($pyjs_dbg_539_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_539_err);}throw $pyjs_dbg_539_err;
}})();
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', $assign7) : $p['setattr'](self, 'isDisabled', $assign7); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=727;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var $and102,$or42,$or41,$and101;
				$pyjs['track']={'module':'actions.list', 'lineno':727};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=727;
				$pyjs['track']['lineno']=728;
				$pyjs['track']['lineno']=728;
				var $pyjs__ret = ($p['bool']($and101=$p['op_eq'](actionName, 'unselectall'))?($p['bool']($or41=$p['op_eq'](handler, 'list'))?$or41:(function(){try{try{$pyjs['in_try_except'] += 1;
				return handler['startswith']('list.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_540_err){if (!$p['isinstance']($pyjs_dbg_540_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_540_err);}throw $pyjs_dbg_540_err;
}})()):$and101);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=730;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a0238e1c62697b408437ae917f87e3a2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];
				var cnt;
				$pyjs['track']={'module':'actions.list', 'lineno':730};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=730;
				$pyjs['track']['lineno']=731;
				cnt = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_541_err){if (!$p['isinstance']($pyjs_dbg_541_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_541_err);}throw $pyjs_dbg_541_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_542_err){if (!$p['isinstance']($pyjs_dbg_542_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_542_err);}throw $pyjs_dbg_542_err;
}})()['table']['table']['unSelectAll']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_543_err){if (!$p['isinstance']($pyjs_dbg_543_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_543_err);}throw $pyjs_dbg_543_err;
}})();
				$pyjs['track']['lineno']=732;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['log']('info', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['translate'], null, null, [{'items':cnt}, '{items} items had been unselected']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_544_err){if (!$p['isinstance']($pyjs_dbg_544_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_544_err);}throw $pyjs_dbg_544_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_545_err){if (!$p['isinstance']($pyjs_dbg_545_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_545_err);}throw $pyjs_dbg_545_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=734;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a0238e1c62697b408437ae917f87e3a2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':734};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=734;
				$pyjs['track']['lineno']=735;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['UnSelectAllAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_546_err){if (!$p['isinstance']($pyjs_dbg_546_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_546_err);}throw $pyjs_dbg_546_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_547_err){if (!$p['isinstance']($pyjs_dbg_547_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_547_err);}throw $pyjs_dbg_547_err;
}})();
				$pyjs['track']['lineno']=736;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_548_err){if (!$p['isinstance']($pyjs_dbg_548_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_548_err);}throw $pyjs_dbg_548_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_549_err){if (!$p['isinstance']($pyjs_dbg_549_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_549_err);}throw $pyjs_dbg_549_err;
}})()['tableChangedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_550_err){if (!$p['isinstance']($pyjs_dbg_550_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_550_err);}throw $pyjs_dbg_550_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=738;
			$method = $pyjs__bind_method2('onDetach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a0238e1c62697b408437ae917f87e3a2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':738};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=738;
				$pyjs['track']['lineno']=739;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_551_err){if (!$p['isinstance']($pyjs_dbg_551_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_551_err);}throw $pyjs_dbg_551_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_552_err){if (!$p['isinstance']($pyjs_dbg_552_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_552_err);}throw $pyjs_dbg_552_err;
}})()['tableChangedEvent']['unregister'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_553_err){if (!$p['isinstance']($pyjs_dbg_553_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_553_err);}throw $pyjs_dbg_553_err;
}})();
				$pyjs['track']['lineno']=740;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['UnSelectAllAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_554_err){if (!$p['isinstance']($pyjs_dbg_554_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_554_err);}throw $pyjs_dbg_554_err;
}})()['onDetach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_555_err){if (!$p['isinstance']($pyjs_dbg_555_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_555_err);}throw $pyjs_dbg_555_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onDetach'] = $method;
			$pyjs['track']['lineno']=742;
			$method = $pyjs__bind_method2('onTableChanged', function(table, count) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					count = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a0238e1c62697b408437ae917f87e3a2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $assign9,$assign8;
				$pyjs['track']={'module':'actions.list', 'lineno':742};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=742;
				$pyjs['track']['lineno']=743;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp'](count, $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_556_err){if (!$p['isinstance']($pyjs_dbg_556_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_556_err);}throw $pyjs_dbg_556_err;
}})()) {
					$pyjs['track']['lineno']=744;
					$assign8 = false;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', $assign8);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_557_err){if (!$p['isinstance']($pyjs_dbg_557_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_557_err);}throw $pyjs_dbg_557_err;
}})();
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', $assign8) : $p['setattr'](self, 'isDisabled', $assign8); 
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, 'isDisabled')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_558_err){if (!$p['isinstance']($pyjs_dbg_558_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_558_err);}throw $pyjs_dbg_558_err;
}})()) {
					$pyjs['track']['lineno']=746;
					$assign9 = true;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', $assign9);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_559_err){if (!$p['isinstance']($pyjs_dbg_559_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_559_err);}throw $pyjs_dbg_559_err;
}})();
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', $assign9) : $p['setattr'](self, 'isDisabled', $assign9); 
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['count']]);
			$cls_definition['onTableChanged'] = $method;
			$pyjs['track']['lineno']=720;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('UnSelectAllAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=748;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['UnSelectAllAction'], 'isSuitableFor'), $m['UnSelectAllAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_560_err){if (!$p['isinstance']($pyjs_dbg_560_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_560_err);}throw $pyjs_dbg_560_err;
}})();
		$pyjs['track']['lineno']=750;
		$m['SelectInvertAction'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'actions.list';
			$cls_definition['__md5__'] = '96e30c115a09d7c18a6dcd34c9dd8fcb';
			$pyjs['track']['lineno']=751;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '96e30c115a09d7c18a6dcd34c9dd8fcb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var $assign10;
				$pyjs['track']={'module':'actions.list', 'lineno':751};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=751;
				$pyjs['track']['lineno']=752;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectInvertAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_561_err){if (!$p['isinstance']($pyjs_dbg_561_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_561_err);}throw $pyjs_dbg_561_err;
}})(), '__init__', args, kwargs, [{}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Invert selection');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_562_err){if (!$p['isinstance']($pyjs_dbg_562_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_562_err);}throw $pyjs_dbg_562_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_563_err){if (!$p['isinstance']($pyjs_dbg_563_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_563_err);}throw $pyjs_dbg_563_err;
}})();
				$pyjs['track']['lineno']=753;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'icon selectinvert');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_564_err){if (!$p['isinstance']($pyjs_dbg_564_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_564_err);}throw $pyjs_dbg_564_err;
}})();
				$pyjs['track']['lineno']=754;
				$assign10 = true;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('disabled', $assign10);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_565_err){if (!$p['isinstance']($pyjs_dbg_565_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_565_err);}throw $pyjs_dbg_565_err;
}})();
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', $assign10) : $p['setattr'](self, 'isDisabled', $assign10); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=757;
			$method = $pyjs__bind_method2('isSuitableFor', function(modul, handler, actionName) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var $and103,$and104,$or43,$or44;
				$pyjs['track']={'module':'actions.list', 'lineno':757};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=757;
				$pyjs['track']['lineno']=758;
				$pyjs['track']['lineno']=758;
				var $pyjs__ret = ($p['bool']($and103=$p['op_eq'](actionName, 'selectinvert'))?($p['bool']($or43=$p['op_eq'](handler, 'list'))?$or43:(function(){try{try{$pyjs['in_try_except'] += 1;
				return handler['startswith']('list.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_566_err){if (!$p['isinstance']($pyjs_dbg_566_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_566_err);}throw $pyjs_dbg_566_err;
}})()):$and103);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['handler'],['actionName']]);
			$cls_definition['isSuitableFor'] = $method;
			$pyjs['track']['lineno']=760;
			$method = $pyjs__bind_method2('onClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '96e30c115a09d7c18a6dcd34c9dd8fcb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];
				var added,$and106,$and105,removed;
				$pyjs['track']={'module':'actions.list', 'lineno':760};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=760;
				$pyjs['track']['lineno']=761;
				var $tupleassign4 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_567_err){if (!$p['isinstance']($pyjs_dbg_567_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_567_err);}throw $pyjs_dbg_567_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_568_err){if (!$p['isinstance']($pyjs_dbg_568_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_568_err);}throw $pyjs_dbg_568_err;
}})()['table']['table']['invertSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_569_err){if (!$p['isinstance']($pyjs_dbg_569_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_569_err);}throw $pyjs_dbg_569_err;
}})(), 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_570_err){if (!$p['isinstance']($pyjs_dbg_570_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_570_err);}throw $pyjs_dbg_570_err;
}})();
				added = $tupleassign4[0];
				removed = $tupleassign4[1];
				$pyjs['track']['lineno']=763;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and105=removed)?added:$and105));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_571_err){if (!$p['isinstance']($pyjs_dbg_571_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_571_err);}throw $pyjs_dbg_571_err;
}})()) {
					$pyjs['track']['lineno']=764;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['conf']['__getitem__']('mainWindow')['log']('info', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call(null, $m['translate'], null, null, [{'added':added, 'removed':removed}, '{added} items selected, {removed} items deselected']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_572_err){if (!$p['isinstance']($pyjs_dbg_572_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_572_err);}throw $pyjs_dbg_572_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_573_err){if (!$p['isinstance']($pyjs_dbg_573_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_573_err);}throw $pyjs_dbg_573_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq'](removed, $constant_int_0));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_574_err){if (!$p['isinstance']($pyjs_dbg_574_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_574_err);}throw $pyjs_dbg_574_err;
}})()) {
					$pyjs['track']['lineno']=767;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['conf']['__getitem__']('mainWindow')['log']('info', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call(null, $m['translate'], null, null, [{'items':added}, '{items} items had been selected']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_575_err){if (!$p['isinstance']($pyjs_dbg_575_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_575_err);}throw $pyjs_dbg_575_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_576_err){if (!$p['isinstance']($pyjs_dbg_576_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_576_err);}throw $pyjs_dbg_576_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq'](added, $constant_int_0));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_577_err){if (!$p['isinstance']($pyjs_dbg_577_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_577_err);}throw $pyjs_dbg_577_err;
}})()) {
					$pyjs['track']['lineno']=769;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['conf']['__getitem__']('mainWindow')['log']('info', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call(null, $m['translate'], null, null, [{'items':removed}, '{items} items had been unselected']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_578_err){if (!$p['isinstance']($pyjs_dbg_578_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_578_err);}throw $pyjs_dbg_578_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_579_err){if (!$p['isinstance']($pyjs_dbg_579_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_579_err);}throw $pyjs_dbg_579_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=771;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '96e30c115a09d7c18a6dcd34c9dd8fcb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':771};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=771;
				$pyjs['track']['lineno']=772;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectInvertAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_580_err){if (!$p['isinstance']($pyjs_dbg_580_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_580_err);}throw $pyjs_dbg_580_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_581_err){if (!$p['isinstance']($pyjs_dbg_581_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_581_err);}throw $pyjs_dbg_581_err;
}})();
				$pyjs['track']['lineno']=773;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_582_err){if (!$p['isinstance']($pyjs_dbg_582_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_582_err);}throw $pyjs_dbg_582_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_583_err){if (!$p['isinstance']($pyjs_dbg_583_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_583_err);}throw $pyjs_dbg_583_err;
}})()['tableChangedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_584_err){if (!$p['isinstance']($pyjs_dbg_584_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_584_err);}throw $pyjs_dbg_584_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=775;
			$method = $pyjs__bind_method2('onDetach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '96e30c115a09d7c18a6dcd34c9dd8fcb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'actions.list', 'lineno':775};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=775;
				$pyjs['track']['lineno']=776;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_585_err){if (!$p['isinstance']($pyjs_dbg_585_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_585_err);}throw $pyjs_dbg_585_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_586_err){if (!$p['isinstance']($pyjs_dbg_586_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_586_err);}throw $pyjs_dbg_586_err;
}})()['tableChangedEvent']['unregister'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_587_err){if (!$p['isinstance']($pyjs_dbg_587_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_587_err);}throw $pyjs_dbg_587_err;
}})();
				$pyjs['track']['lineno']=777;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectInvertAction'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_588_err){if (!$p['isinstance']($pyjs_dbg_588_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_588_err);}throw $pyjs_dbg_588_err;
}})()['onDetach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_589_err){if (!$p['isinstance']($pyjs_dbg_589_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_589_err);}throw $pyjs_dbg_589_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onDetach'] = $method;
			$pyjs['track']['lineno']=779;
			$method = $pyjs__bind_method2('onTableChanged', function(table, count) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					count = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '96e30c115a09d7c18a6dcd34c9dd8fcb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $assign11,$assign12;
				$pyjs['track']={'module':'actions.list', 'lineno':779};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='actions.list';
				$pyjs['track']['lineno']=779;
				$pyjs['track']['lineno']=780;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp'](count, $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_590_err){if (!$p['isinstance']($pyjs_dbg_590_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_590_err);}throw $pyjs_dbg_590_err;
}})()) {
					$pyjs['track']['lineno']=781;
					$assign11 = false;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', $assign11);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_591_err){if (!$p['isinstance']($pyjs_dbg_591_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_591_err);}throw $pyjs_dbg_591_err;
}})();
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', $assign11) : $p['setattr'](self, 'isDisabled', $assign11); 
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, 'isDisabled')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_592_err){if (!$p['isinstance']($pyjs_dbg_592_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_592_err);}throw $pyjs_dbg_592_err;
}})()) {
					$pyjs['track']['lineno']=783;
					$assign12 = true;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', $assign12);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_593_err){if (!$p['isinstance']($pyjs_dbg_593_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_593_err);}throw $pyjs_dbg_593_err;
}})();
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDisabled', $assign12) : $p['setattr'](self, 'isDisabled', $assign12); 
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['count']]);
			$cls_definition['onTableChanged'] = $method;
			$pyjs['track']['lineno']=750;
			var $bases = new Array($p['getattr']($p['getattr']($m['html5'], 'ext'), 'Button'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('SelectInvertAction', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=785;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['actionDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['SelectInvertAction'], 'isSuitableFor'), $m['SelectInvertAction']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_594_err){if (!$p['isinstance']($pyjs_dbg_594_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_594_err);}throw $pyjs_dbg_594_err;
}})();
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end actions.list */


/* end module: actions.list */


/*
PYJS_DEPS: ['html5', 'utils', 'network.NetworkService', 'network', 'priorityqueue.actionDelegateSelector', 'priorityqueue', 'widgets.edit.EditWidget', 'widgets', 'widgets.edit', 'config.conf', 'config', 'pane.Pane', 'pane', 'widgets.repeatdate.RepeatDatePopup', 'widgets.repeatdate', 'widgets.csvexport.CsvExport', 'widgets.csvexport', 'widgets.table.DataTable', 'widgets.table', 'widgets.preview.Preview', 'widgets.preview', 'sidebarwidgets.internalpreview.InternalPreview', 'sidebarwidgets', 'sidebarwidgets.internalpreview', 'sidebarwidgets.filterselector.FilterSelector', 'sidebarwidgets.filterselector', 'i18n.translate', 'i18n']
*/
