/* start module: widgets.repeatdate */
$pyjs['loaded_modules']['widgets.repeatdate'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['widgets.repeatdate']['__was_initialized__']) return $pyjs['loaded_modules']['widgets.repeatdate'];
	if(typeof $pyjs['loaded_modules']['widgets'] == 'undefined' || !$pyjs['loaded_modules']['widgets']['__was_initialized__']) $p['___import___']('widgets', null);
	var $m = $pyjs['loaded_modules']['widgets.repeatdate'];
	$m['__repr__'] = function() { return '<module: widgets.repeatdate>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'widgets.repeatdate';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	$pyjs['loaded_modules']['widgets']['repeatdate'] = $pyjs['loaded_modules']['widgets.repeatdate'];
	try {
		$m.__track_lines__[1] = 'widgets.repeatdate.py, line 1:\n    import html5';
		$m.__track_lines__[3] = 'widgets.repeatdate.py, line 3:\n    from html5.ext.popup import Popup';
		$m.__track_lines__[4] = 'widgets.repeatdate.py, line 4:\n    from html5.keycodes import isReturn';
		$m.__track_lines__[5] = 'widgets.repeatdate.py, line 5:\n    from event import EventDispatcher';
		$m.__track_lines__[6] = 'widgets.repeatdate.py, line 6:\n    from priorityqueue import editBoneSelector';
		$m.__track_lines__[7] = 'widgets.repeatdate.py, line 7:\n    from i18n import translate';
		$m.__track_lines__[8] = 'widgets.repeatdate.py, line 8:\n    from bones import selectmulti, date';
		$m.__track_lines__[9] = 'widgets.repeatdate.py, line 9:\n    from widgets.actionbar import ActionBar';
		$m.__track_lines__[10] = 'widgets.repeatdate.py, line 10:\n    from network import NetworkService';
		$m.__track_lines__[11] = 'widgets.repeatdate.py, line 11:\n    import utils';
		$m.__track_lines__[12] = 'widgets.repeatdate.py, line 12:\n    from config import conf';
		$m.__track_lines__[13] = 'widgets.repeatdate.py, line 13:\n    from widgets.edit import fieldset_A';
		$m.__track_lines__[15] = 'widgets.repeatdate.py, line 15:\n    class RepeatDatePopup(html5.Div):';
		$m.__track_lines__[17] = 'widgets.repeatdate.py, line 17:\n    __editIdx_ = 0';
		$m.__track_lines__[19] = 'widgets.repeatdate.py, line 19:\n    def __init__(self, modul, key):';
		$m.__track_lines__[20] = 'widgets.repeatdate.py, line 20:\n    super(RepeatDatePopup, self).__init__()';
		$m.__track_lines__[21] = 'widgets.repeatdate.py, line 21:\n    self.modul = modul';
		$m.__track_lines__[22] = 'widgets.repeatdate.py, line 22:\n    self.editIdx = RepeatDatePopup.__editIdx_ #Iternal counter to ensure unique ids';
		$m.__track_lines__[23] = 'widgets.repeatdate.py, line 23:\n    RepeatDatePopup.__editIdx_ += 1';
		$m.__track_lines__[24] = 'widgets.repeatdate.py, line 24:\n    self.key = key';
		$m.__track_lines__[25] = 'widgets.repeatdate.py, line 25:\n    self._lastData = {} #Dict of structure and values received';
		$m.__track_lines__[26] = 'widgets.repeatdate.py, line 26:\n    self.closeOnSuccess = False';
		$m.__track_lines__[28] = 'widgets.repeatdate.py, line 28:\n    h3 = html5.H3()';
		$m.__track_lines__[29] = 'widgets.repeatdate.py, line 29:\n    h3["class"].append("modul_%s" % self.modul)';
		$m.__track_lines__[30] = 'widgets.repeatdate.py, line 30:\n    h3["class"].append("apptype_list")';
		$m.__track_lines__[32] = 'widgets.repeatdate.py, line 32:\n    h3.appendChild(html5.TextNode(translate("create recurrent dates")))';
		$m.__track_lines__[34] = 'widgets.repeatdate.py, line 34:\n    self.wasInitialRequest = True';
		$m.__track_lines__[35] = 'widgets.repeatdate.py, line 35:\n    self.actionbar = ActionBar( self.modul, "list", "repeatdate")';
		$m.__track_lines__[36] = 'widgets.repeatdate.py, line 36:\n    self.appendChild( self.actionbar )';
		$m.__track_lines__[37] = 'widgets.repeatdate.py, line 37:\n    self.form = html5.Form()';
		$m.__track_lines__[38] = 'widgets.repeatdate.py, line 38:\n    self.appendChild(self.form)';
		$m.__track_lines__[39] = 'widgets.repeatdate.py, line 39:\n    self.actionbar.setActions(["create.recurrent"])';
		$m.__track_lines__[40] = 'widgets.repeatdate.py, line 40:\n    self.reloadData()';
		$m.__track_lines__[42] = 'widgets.repeatdate.py, line 42:\n    def reloadData(self):';
		$m.__track_lines__[43] = 'widgets.repeatdate.py, line 43:\n    self.save({})';
		$m.__track_lines__[44] = 'widgets.repeatdate.py, line 44:\n    return';
		$m.__track_lines__[46] = 'widgets.repeatdate.py, line 46:\n    def save(self, data):';
		$m.__track_lines__[47] = 'widgets.repeatdate.py, line 47:\n    self.wasInitialRequest = not len(data)>0';
		$m.__track_lines__[48] = 'widgets.repeatdate.py, line 48:\n    if self.modul=="_tasks":';
		$m.__track_lines__[49] = 'widgets.repeatdate.py, line 49:\n    return #FIXME!';
		$m.__track_lines__[51] = 'widgets.repeatdate.py, line 51:\n    if not data:';
		$m.__track_lines__[52] = 'widgets.repeatdate.py, line 52:\n    NetworkService.request(self.modul,"view/%s" % self.key, successHandler=self.setData, failureHandler=self.showErrorMsg)';
		$m.__track_lines__[54] = 'widgets.repeatdate.py, line 54:\n    NetworkService.request(self.modul, "add", data, secure=len(data)>0, successHandler=self.setData, failureHandler=self.showErrorMsg )';
		$m.__track_lines__[56] = 'widgets.repeatdate.py, line 56:\n    def setData( self, request=None, data=None, ignoreMissing=False ):';
		$m.__track_lines__[65] = 'widgets.repeatdate.py, line 65:\n    assert (request or data)';
		$m.__track_lines__[66] = 'widgets.repeatdate.py, line 66:\n    if request:';
		$m.__track_lines__[67] = 'widgets.repeatdate.py, line 67:\n    data = NetworkService.decode( request )';
		$m.__track_lines__[69] = 'widgets.repeatdate.py, line 69:\n    try:';
		$m.__track_lines__[70] = 'widgets.repeatdate.py, line 70:\n    skelStructure = utils.boneListToDict(data["structure"])';
		$m.__track_lines__[72] = 'widgets.repeatdate.py, line 72:\n    NetworkService.notifyChange(self.modul)';
		$m.__track_lines__[73] = 'widgets.repeatdate.py, line 73:\n    conf["mainWindow"].removeWidget( self )';
		$m.__track_lines__[74] = 'widgets.repeatdate.py, line 74:\n    return';
		$m.__track_lines__[76] = 'widgets.repeatdate.py, line 76:\n    print';
		$m.__track_lines__[77] = 'widgets.repeatdate.py, line 77:\n    print("data", data)';
		$m.__track_lines__[78] = 'widgets.repeatdate.py, line 78:\n    print("action", data["action"])';
		$m.__track_lines__[79] = 'widgets.repeatdate.py, line 79:\n    if "action" in data and (data["action"] in ["addSuccess", "editSuccess"]):';
		$m.__track_lines__[80] = 'widgets.repeatdate.py, line 80:\n    NetworkService.notifyChange(self.modul)';
		$m.__track_lines__[81] = 'widgets.repeatdate.py, line 81:\n    logDiv = html5.Div()';
		$m.__track_lines__[82] = 'widgets.repeatdate.py, line 82:\n    logDiv["class"].append("msg")';
		$m.__track_lines__[83] = 'widgets.repeatdate.py, line 83:\n    spanMsg = html5.Span()';
		$m.__track_lines__[84] = 'widgets.repeatdate.py, line 84:\n    spanMsg.appendChild( html5.TextNode( translate("Entry saved!") ))';
		$m.__track_lines__[85] = 'widgets.repeatdate.py, line 85:\n    spanMsg["class"].append("msgspan")';
		$m.__track_lines__[86] = 'widgets.repeatdate.py, line 86:\n    logDiv.appendChild(spanMsg)';
		$m.__track_lines__[87] = 'widgets.repeatdate.py, line 87:\n    if self.modul in conf["modules"].keys():';
		$m.__track_lines__[88] = 'widgets.repeatdate.py, line 88:\n    spanMsg = html5.Span()';
		$m.__track_lines__[89] = 'widgets.repeatdate.py, line 89:\n    spanMsg.appendChild( html5.TextNode( conf["modules"][self.modul]["name"] ))';
		$m.__track_lines__[90] = 'widgets.repeatdate.py, line 90:\n    spanMsg["class"].append("modulspan")';
		$m.__track_lines__[91] = 'widgets.repeatdate.py, line 91:\n    logDiv.appendChild(spanMsg)';
		$m.__track_lines__[92] = 'widgets.repeatdate.py, line 92:\n    if "values" in data.keys() and "name" in data["values"].keys():';
		$m.__track_lines__[93] = 'widgets.repeatdate.py, line 93:\n    spanMsg = html5.Span()';
		$m.__track_lines__[94] = 'widgets.repeatdate.py, line 94:\n    spanMsg.appendChild( html5.TextNode( str(data["values"]["name"]) ))';
		$m.__track_lines__[95] = 'widgets.repeatdate.py, line 95:\n    spanMsg["class"].append("namespan")';
		$m.__track_lines__[96] = 'widgets.repeatdate.py, line 96:\n    logDiv.appendChild(spanMsg)';
		$m.__track_lines__[97] = 'widgets.repeatdate.py, line 97:\n    conf["mainWindow"].log("success",logDiv)';
		$m.__track_lines__[98] = 'widgets.repeatdate.py, line 98:\n    if self.closeOnSuccess:';
		$m.__track_lines__[99] = 'widgets.repeatdate.py, line 99:\n    conf["mainWindow"].removeWidget( self )';
		$m.__track_lines__[100] = 'widgets.repeatdate.py, line 100:\n    return';
		$m.__track_lines__[101] = 'widgets.repeatdate.py, line 101:\n    self.clear()';
		$m.__track_lines__[103] = 'widgets.repeatdate.py, line 103:\n    self.reloadData()';
		$m.__track_lines__[104] = 'widgets.repeatdate.py, line 104:\n    return';
		$m.__track_lines__[106] = 'widgets.repeatdate.py, line 106:\n    self.clear()';
		$m.__track_lines__[107] = 'widgets.repeatdate.py, line 107:\n    self.actionbar.resetLoadingState()';
		$m.__track_lines__[108] = 'widgets.repeatdate.py, line 108:\n    self.dataCache = data';
		$m.__track_lines__[110] = 'widgets.repeatdate.py, line 110:\n    fieldSets = {}';
		$m.__track_lines__[111] = 'widgets.repeatdate.py, line 111:\n    cat = "byweek"';
		$m.__track_lines__[112] = 'widgets.repeatdate.py, line 112:\n    fs = html5.Fieldset()';
		$m.__track_lines__[113] = 'widgets.repeatdate.py, line 113:\n    fs["class"] = cat';
		$m.__track_lines__[114] = 'widgets.repeatdate.py, line 114:\n    if cat=="byweek":';
		$m.__track_lines__[115] = 'widgets.repeatdate.py, line 115:\n    fs["class"].append("active")';
		$m.__track_lines__[117] = 'widgets.repeatdate.py, line 117:\n    fs["name"] = cat';
		$m.__track_lines__[118] = 'widgets.repeatdate.py, line 118:\n    legend = html5.Legend()';
		$m.__track_lines__[119] = 'widgets.repeatdate.py, line 119:\n    fshref = fieldset_A()';
		$m.__track_lines__[120] = 'widgets.repeatdate.py, line 120:\n    fshref.appendChild(html5.TextNode(cat) )';
		$m.__track_lines__[121] = 'widgets.repeatdate.py, line 121:\n    legend.appendChild( fshref )';
		$m.__track_lines__[122] = 'widgets.repeatdate.py, line 122:\n    fs.appendChild(legend)';
		$m.__track_lines__[123] = 'widgets.repeatdate.py, line 123:\n    section = html5.Section()';
		$m.__track_lines__[124] = 'widgets.repeatdate.py, line 124:\n    fs.appendChild(section)';
		$m.__track_lines__[125] = 'widgets.repeatdate.py, line 125:\n    fs._section = section';
		$m.__track_lines__[126] = 'widgets.repeatdate.py, line 126:\n    fieldSets[ cat ] = fs';
		$m.__track_lines__[128] = 'widgets.repeatdate.py, line 128:\n    self.dtstart = data["values"]["startdate"]';
		$m.__track_lines__[129] = 'widgets.repeatdate.py, line 129:\n    startdateLabel = html5.Label("Termin")';
		$m.__track_lines__[130] = 'widgets.repeatdate.py, line 130:\n    startdateLabel["class"].append("termin")';
		$m.__track_lines__[131] = 'widgets.repeatdate.py, line 131:\n    startdateLabel["class"].append("date")';
		$m.__track_lines__[132] = 'widgets.repeatdate.py, line 132:\n    startdate_id = "vi_%s_%s_edit_bn_%s" % ( self.editIdx, self.modul, "repeatdate")';
		$m.__track_lines__[133] = 'widgets.repeatdate.py, line 133:\n    startdateLabel["for"] = startdate_id';
		$m.__track_lines__[134] = 'widgets.repeatdate.py, line 134:\n    startdate = date.DateViewBoneDelegate("termin", "startdate", skelStructure).render(data["values"], "startdate")';
		$m.__track_lines__[135] = 'widgets.repeatdate.py, line 135:\n    startdate["id"] = startdate_id';
		$m.__track_lines__[136] = 'widgets.repeatdate.py, line 136:\n    containerDiv = html5.Div()';
		$m.__track_lines__[137] = 'widgets.repeatdate.py, line 137:\n    containerDiv.appendChild(startdateLabel)';
		$m.__track_lines__[138] = 'widgets.repeatdate.py, line 138:\n    containerDiv.appendChild(startdate)';
		$m.__track_lines__[139] = 'widgets.repeatdate.py, line 139:\n    containerDiv["class"].append("bone")';
		$m.__track_lines__[140] = 'widgets.repeatdate.py, line 140:\n    containerDiv["class"].append("bone_startdate")';
		$m.__track_lines__[141] = 'widgets.repeatdate.py, line 141:\n    containerDiv["class"].append("date")';
		$m.__track_lines__[142] = 'widgets.repeatdate.py, line 142:\n    fieldSets[ cat ]._section.appendChild( containerDiv )';
		$m.__track_lines__[144] = 'widgets.repeatdate.py, line 144:\n    countLabel = html5.Label("Wiederholungen")';
		$m.__track_lines__[145] = 'widgets.repeatdate.py, line 145:\n    countLabel["class"].append("count")';
		$m.__track_lines__[146] = 'widgets.repeatdate.py, line 146:\n    countLabel["class"].append("numeric")';
		$m.__track_lines__[147] = 'widgets.repeatdate.py, line 147:\n    count_id = "vi_%s_%s_edit_bn_%s" % ( self.editIdx, self.modul, "count")';
		$m.__track_lines__[148] = 'widgets.repeatdate.py, line 148:\n    countLabel["for"] = count_id';
		$m.__track_lines__[150] = 'widgets.repeatdate.py, line 150:\n    self.count = html5.Input()';
		$m.__track_lines__[151] = 'widgets.repeatdate.py, line 151:\n    self.count["id"] = count_id';
		$m.__track_lines__[152] = 'widgets.repeatdate.py, line 152:\n    containerDiv2 = html5.Div()';
		$m.__track_lines__[153] = 'widgets.repeatdate.py, line 153:\n    containerDiv2["class"].append("bone")';
		$m.__track_lines__[154] = 'widgets.repeatdate.py, line 154:\n    containerDiv2["class"].append("bone_count")';
		$m.__track_lines__[155] = 'widgets.repeatdate.py, line 155:\n    containerDiv2["class"].append("date")';
		$m.__track_lines__[156] = 'widgets.repeatdate.py, line 156:\n    containerDiv2.appendChild(countLabel)';
		$m.__track_lines__[157] = 'widgets.repeatdate.py, line 157:\n    containerDiv2.appendChild(self.count)';
		$m.__track_lines__[176] = 'widgets.repeatdate.py, line 176:\n    fieldSets[ cat ]._section.appendChild(containerDiv2)';
		$m.__track_lines__[178] = 'widgets.repeatdate.py, line 178:\n    for (k,v) in fieldSets.items():';
		$m.__track_lines__[179] = 'widgets.repeatdate.py, line 179:\n    if not "active" in v["class"]:';
		$m.__track_lines__[180] = 'widgets.repeatdate.py, line 180:\n    v["class"].append("active")';
		$m.__track_lines__[181] = 'widgets.repeatdate.py, line 181:\n    tmpList = [(k,v) for (k,v) in fieldSets.items()]';
		$m.__track_lines__[182] = 'widgets.repeatdate.py, line 182:\n    tmpList.sort( key=lambda x:x[0])';
		$m.__track_lines__[183] = 'widgets.repeatdate.py, line 183:\n    for k,v in tmpList:';
		$m.__track_lines__[184] = 'widgets.repeatdate.py, line 184:\n    self.form.appendChild( v )';
		$m.__track_lines__[185] = 'widgets.repeatdate.py, line 185:\n    v._section = None';
		$m.__track_lines__[188] = 'widgets.repeatdate.py, line 188:\n    def clear(self):';
		$m.__track_lines__[192] = 'widgets.repeatdate.py, line 192:\n    for c in self.form._children[ : ]:';
		$m.__track_lines__[193] = 'widgets.repeatdate.py, line 193:\n    self.form.removeChild( c )';
		$m.__track_lines__[195] = 'widgets.repeatdate.py, line 195:\n    def showErrorMsg(self, req=None, code=None):';
		$m.__track_lines__[199] = 'widgets.repeatdate.py, line 199:\n    self.actionbar["style"]["display"] = "none"';
		$m.__track_lines__[200] = 'widgets.repeatdate.py, line 200:\n    self.form["style"]["display"] = "none"';
		$m.__track_lines__[201] = 'widgets.repeatdate.py, line 201:\n    errorDiv = html5.Div()';
		$m.__track_lines__[202] = 'widgets.repeatdate.py, line 202:\n    errorDiv["class"].append("error_msg")';
		$m.__track_lines__[203] = 'widgets.repeatdate.py, line 203:\n    if code and (code==401 or code==403):';
		$m.__track_lines__[204] = 'widgets.repeatdate.py, line 204:\n    txt = translate("Access denied!")';
		$m.__track_lines__[206] = 'widgets.repeatdate.py, line 206:\n    txt = translate("An unknown error occurred!")';
		$m.__track_lines__[207] = 'widgets.repeatdate.py, line 207:\n    errorDiv["class"].append("error_code_%s" % (code or 0))';
		$m.__track_lines__[208] = 'widgets.repeatdate.py, line 208:\n    errorDiv.appendChild( html5.TextNode( txt ) )';
		$m.__track_lines__[209] = 'widgets.repeatdate.py, line 209:\n    self.appendChild( errorDiv )';
		$m.__track_lines__[211] = 'widgets.repeatdate.py, line 211:\n    def doSave( self, closeOnSuccess=False):';
		$m.__track_lines__[212] = 'widgets.repeatdate.py, line 212:\n    self.closeOnSuccess = closeOnSuccess';
		$m.__track_lines__[213] = 'widgets.repeatdate.py, line 213:\n    data ={"count": self.count._getValue(), "kind" : "2", "dtstart" : self.dtstart';
		$m.__track_lines__[217] = 'widgets.repeatdate.py, line 217:\n    NetworkService.request(self.modul, "addrecurrent/%s" % self.key, data, secure=True, successHandler=self.setData, failureHandler=self.showErrorMsg )';

		var $constant_int_0 = new $p['int'](0);
		var $constant_int_1 = new $p['int'](1);
		var $constant_int_403 = new $p['int'](403);
		var $constant_int_401 = new $p['int'](401);
		$pyjs['track']['module']='widgets.repeatdate';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=1;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['html5'] = $p['___import___']('html5', 'widgets');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=3;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['Popup'] = $p['___import___']('html5.ext.popup.Popup', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=4;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['isReturn'] = $p['___import___']('html5.keycodes.isReturn', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=5;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['EventDispatcher'] = $p['___import___']('event.EventDispatcher', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=6;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['editBoneSelector'] = $p['___import___']('priorityqueue.editBoneSelector', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=7;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['translate'] = $p['___import___']('i18n.translate', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=8;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['selectmulti'] = $p['___import___']('bones.selectmulti', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['date'] = $p['___import___']('bones.date', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=9;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['ActionBar'] = $p['___import___']('widgets.actionbar.ActionBar', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=10;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['NetworkService'] = $p['___import___']('network.NetworkService', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=11;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['utils'] = $p['___import___']('utils', 'widgets');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=12;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['conf'] = $p['___import___']('config.conf', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=13;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['fieldset_A'] = $p['___import___']('widgets.edit.fieldset_A', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=15;
		$m['RepeatDatePopup'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'widgets.repeatdate';
			$cls_definition['__md5__'] = 'af7bce2276888a79c42e2ae13501a845';
			$pyjs['track']['lineno']=17;
			$cls_definition['__editIdx_'] = $constant_int_0;
			$pyjs['track']['lineno']=19;
			$method = $pyjs__bind_method2('__init__', function(modul, key) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					modul = arguments[1];
					key = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'af7bce2276888a79c42e2ae13501a845') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var h3,$add2,$add1;
				$pyjs['track']={'module':'widgets.repeatdate', 'lineno':19};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.repeatdate';
				$pyjs['track']['lineno']=19;
				$pyjs['track']['lineno']=20;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['RepeatDatePopup'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})()['__init__']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})();
				$pyjs['track']['lineno']=21;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=22;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('editIdx', $p['getattr']($m['RepeatDatePopup'], '__editIdx_')) : $p['setattr'](self, 'editIdx', $p['getattr']($m['RepeatDatePopup'], '__editIdx_')); 
				$pyjs['track']['lineno']=23;
				$m['RepeatDatePopup']['__is_instance__'] && typeof $m['RepeatDatePopup']['__setattr__'] == 'function' ? $m['RepeatDatePopup']['__setattr__']('__editIdx_', $p['__op_add']($add1=$p['getattr']($m['RepeatDatePopup'], '__editIdx_'),$add2=$constant_int_1)) : $p['setattr']($m['RepeatDatePopup'], '__editIdx_', $p['__op_add']($add1=$p['getattr']($m['RepeatDatePopup'], '__editIdx_'),$add2=$constant_int_1)); 
				$pyjs['track']['lineno']=24;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('key', key) : $p['setattr'](self, 'key', key); 
				$pyjs['track']['lineno']=25;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_lastData', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})()) : $p['setattr'](self, '_lastData', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})()); 
				$pyjs['track']['lineno']=26;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('closeOnSuccess', false) : $p['setattr'](self, 'closeOnSuccess', false); 
				$pyjs['track']['lineno']=28;
				h3 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['H3']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})();
				$pyjs['track']['lineno']=29;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return h3['__getitem__']('class')['append']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('modul_%s', $p['getattr'](self, 'modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})();
				$pyjs['track']['lineno']=30;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return h3['__getitem__']('class')['append']('apptype_list');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})();
				$pyjs['track']['lineno']=32;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return h3['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['TextNode']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('create recurrent dates');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})();
				$pyjs['track']['lineno']=34;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('wasInitialRequest', true) : $p['setattr'](self, 'wasInitialRequest', true); 
				$pyjs['track']['lineno']=35;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('actionbar', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['ActionBar']($p['getattr'](self, 'modul'), 'list', 'repeatdate');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})()) : $p['setattr'](self, 'actionbar', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['ActionBar']($p['getattr'](self, 'modul'), 'list', 'repeatdate');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})()); 
				$pyjs['track']['lineno']=36;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'actionbar'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})();
				$pyjs['track']['lineno']=37;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('form', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Form']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})()) : $p['setattr'](self, 'form', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Form']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})()); 
				$pyjs['track']['lineno']=38;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'form'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_14_err){if (!$p['isinstance']($pyjs_dbg_14_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_14_err);}throw $pyjs_dbg_14_err;
}})();
				$pyjs['track']['lineno']=39;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['actionbar']['setActions']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list'](['create.recurrent']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_16_err){if (!$p['isinstance']($pyjs_dbg_16_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_16_err);}throw $pyjs_dbg_16_err;
}})();
				$pyjs['track']['lineno']=40;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['reloadData']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_17_err){if (!$p['isinstance']($pyjs_dbg_17_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_17_err);}throw $pyjs_dbg_17_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['modul'],['key']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=42;
			$method = $pyjs__bind_method2('reloadData', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'af7bce2276888a79c42e2ae13501a845') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.repeatdate', 'lineno':42};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.repeatdate';
				$pyjs['track']['lineno']=42;
				$pyjs['track']['lineno']=43;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['save']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_19_err){if (!$p['isinstance']($pyjs_dbg_19_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_19_err);}throw $pyjs_dbg_19_err;
}})();
				$pyjs['track']['lineno']=44;
				$pyjs['track']['lineno']=44;
				var $pyjs__ret = null;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['reloadData'] = $method;
			$pyjs['track']['lineno']=46;
			$method = $pyjs__bind_method2('save', function(data) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'af7bce2276888a79c42e2ae13501a845') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.repeatdate', 'lineno':46};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.repeatdate';
				$pyjs['track']['lineno']=46;
				$pyjs['track']['lineno']=47;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('wasInitialRequest', !$p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})(), $constant_int_0) == 1))) : $p['setattr'](self, 'wasInitialRequest', !$p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})(), $constant_int_0) == 1))); 
				$pyjs['track']['lineno']=48;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](self, 'modul'), '_tasks'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})()) {
					$pyjs['track']['lineno']=49;
					$pyjs['track']['lineno']=49;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				else {
					$pyjs['track']['lineno']=51;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool'](data));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})()) {
						$pyjs['track']['lineno']=52;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'successHandler':$p['getattr'](self, 'setData'), 'failureHandler':$p['getattr'](self, 'showErrorMsg')}, $p['getattr'](self, 'modul'), (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('view/%s', $p['getattr'](self, 'key'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=54;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'secure':($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['len'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})(), $constant_int_0) == 1), 'successHandler':$p['getattr'](self, 'setData'), 'failureHandler':$p['getattr'](self, 'showErrorMsg')}, $p['getattr'](self, 'modul'), 'add', data]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_26_err){if (!$p['isinstance']($pyjs_dbg_26_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_26_err);}throw $pyjs_dbg_26_err;
}})();
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['data']]);
			$cls_definition['save'] = $method;
			$pyjs['track']['lineno']=56;
			$method = $pyjs__bind_method2('setData', function(request, data, ignoreMissing) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 3)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 4, arguments['length']+1);
				} else {
					var self = arguments[0];
					request = arguments[1];
					data = arguments[2];
					ignoreMissing = arguments[3];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 4)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 4, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'af7bce2276888a79c42e2ae13501a845') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof request == 'undefined') request=arguments['callee']['__args__'][3][1];
				if (typeof data == 'undefined') data=arguments['callee']['__args__'][4][1];
				if (typeof ignoreMissing == 'undefined') ignoreMissing=arguments['callee']['__args__'][5][1];
				var $lambda1,containerDiv2,$pyjs__trackstack_size_1,$iter1_iter,tmpList,fshref,$iter1_nextval,containerDiv,skelStructure,$iter3_idx,$iter3_array,countLabel,$pyjs_try_err,$iter1_array,$iter3_iter,fs,$or1,$or2,$and1,$and2,$and3,$and4,logDiv,$iter3_type,count_id,$iter3_nextval,legend,$iter1_type,startdate_id,k,startdateLabel,cat,startdate,$iter1_idx,v,spanMsg,section,fieldSets;
				$pyjs['track']={'module':'widgets.repeatdate', 'lineno':56};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.repeatdate';
				$pyjs['track']['lineno']=56;
				$pyjs['track']['lineno']=65;
				if (!( ($p['bool']($or1=request)?$or1:data) )) {
				   throw $p['AssertionError']();
				 }
				$pyjs['track']['lineno']=66;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](request);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_27_err){if (!$p['isinstance']($pyjs_dbg_27_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_27_err);}throw $pyjs_dbg_27_err;
}})()) {
					$pyjs['track']['lineno']=67;
					data = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['NetworkService']['decode'](request);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_28_err){if (!$p['isinstance']($pyjs_dbg_28_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_28_err);}throw $pyjs_dbg_28_err;
}})();
				}
				$pyjs['track']['lineno']=69;
				var $pyjs__trackstack_size_1 = $pyjs['trackstack']['length'];
				try {
					try {
						$pyjs['in_try_except'] += 1;
						$pyjs['track']['lineno']=70;
						skelStructure = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['utils']['boneListToDict'](data['__getitem__']('structure'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_29_err){if (!$p['isinstance']($pyjs_dbg_29_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_29_err);}throw $pyjs_dbg_29_err;
}})();
					} finally { $pyjs['in_try_except'] -= 1; }
				} catch($pyjs_try_err) {
					$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_1 - 1);
					$pyjs['__active_exception_stack__'] = null;
					$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
					var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
					$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='widgets.repeatdate';
					if (($pyjs_try_err_name == $p['AttributeError']['__name__'])||$p['_isinstance']($pyjs_try_err,$p['AttributeError'])) {
						$pyjs['track']['lineno']=72;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['NetworkService']['notifyChange']($p['getattr'](self, 'modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_30_err){if (!$p['isinstance']($pyjs_dbg_30_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_30_err);}throw $pyjs_dbg_30_err;
}})();
						$pyjs['track']['lineno']=73;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['conf']['__getitem__']('mainWindow')['removeWidget'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_31_err){if (!$p['isinstance']($pyjs_dbg_31_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_31_err);}throw $pyjs_dbg_31_err;
}})();
						$pyjs['track']['lineno']=74;
						$pyjs['track']['lineno']=74;
						var $pyjs__ret = null;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					} else { $pyjs['__active_exception_stack__'] = $pyjs['__last_exception_stack__']; $pyjs['__last_exception_stack__'] = null; throw $pyjs_try_err; }
				}
				$pyjs['track']['lineno']=76;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['printFunc']([], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_32_err){if (!$p['isinstance']($pyjs_dbg_32_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_32_err);}throw $pyjs_dbg_32_err;
}})();
				$pyjs['track']['lineno']=77;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['printFunc']([(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['tuple'](['data', data]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_33_err){if (!$p['isinstance']($pyjs_dbg_33_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_33_err);}throw $pyjs_dbg_33_err;
}})()], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_34_err){if (!$p['isinstance']($pyjs_dbg_34_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_34_err);}throw $pyjs_dbg_34_err;
}})();
				$pyjs['track']['lineno']=78;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['printFunc']([(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['tuple'](['action', data['__getitem__']('action')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})()], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_36_err){if (!$p['isinstance']($pyjs_dbg_36_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_36_err);}throw $pyjs_dbg_36_err;
}})();
				$pyjs['track']['lineno']=79;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and1=data['__contains__']('action'))?(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list'](['addSuccess', 'editSuccess']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_37_err){if (!$p['isinstance']($pyjs_dbg_37_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_37_err);}throw $pyjs_dbg_37_err;
}})()['__contains__'](data['__getitem__']('action')):$and1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_38_err){if (!$p['isinstance']($pyjs_dbg_38_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_38_err);}throw $pyjs_dbg_38_err;
}})()) {
					$pyjs['track']['lineno']=80;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['NetworkService']['notifyChange']($p['getattr'](self, 'modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_39_err){if (!$p['isinstance']($pyjs_dbg_39_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_39_err);}throw $pyjs_dbg_39_err;
}})();
					$pyjs['track']['lineno']=81;
					logDiv = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_40_err){if (!$p['isinstance']($pyjs_dbg_40_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_40_err);}throw $pyjs_dbg_40_err;
}})();
					$pyjs['track']['lineno']=82;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return logDiv['__getitem__']('class')['append']('msg');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_41_err){if (!$p['isinstance']($pyjs_dbg_41_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_41_err);}throw $pyjs_dbg_41_err;
}})();
					$pyjs['track']['lineno']=83;
					spanMsg = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_42_err){if (!$p['isinstance']($pyjs_dbg_42_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_42_err);}throw $pyjs_dbg_42_err;
}})();
					$pyjs['track']['lineno']=84;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return spanMsg['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['TextNode']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Entry saved!');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_43_err){if (!$p['isinstance']($pyjs_dbg_43_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_43_err);}throw $pyjs_dbg_43_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_44_err){if (!$p['isinstance']($pyjs_dbg_44_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_44_err);}throw $pyjs_dbg_44_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_45_err){if (!$p['isinstance']($pyjs_dbg_45_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_45_err);}throw $pyjs_dbg_45_err;
}})();
					$pyjs['track']['lineno']=85;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return spanMsg['__getitem__']('class')['append']('msgspan');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_46_err){if (!$p['isinstance']($pyjs_dbg_46_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_46_err);}throw $pyjs_dbg_46_err;
}})();
					$pyjs['track']['lineno']=86;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return logDiv['appendChild'](spanMsg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})();
					$pyjs['track']['lineno']=87;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['conf']['__getitem__']('modules')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_48_err){if (!$p['isinstance']($pyjs_dbg_48_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_48_err);}throw $pyjs_dbg_48_err;
}})()['__contains__']($p['getattr'](self, 'modul')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_49_err){if (!$p['isinstance']($pyjs_dbg_49_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_49_err);}throw $pyjs_dbg_49_err;
}})()) {
						$pyjs['track']['lineno']=88;
						spanMsg = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_50_err){if (!$p['isinstance']($pyjs_dbg_50_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_50_err);}throw $pyjs_dbg_50_err;
}})();
						$pyjs['track']['lineno']=89;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return spanMsg['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['TextNode']($m['conf']['__getitem__']('modules')['__getitem__']($p['getattr'](self, 'modul'))['__getitem__']('name'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_51_err){if (!$p['isinstance']($pyjs_dbg_51_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_51_err);}throw $pyjs_dbg_51_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_52_err){if (!$p['isinstance']($pyjs_dbg_52_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_52_err);}throw $pyjs_dbg_52_err;
}})();
						$pyjs['track']['lineno']=90;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return spanMsg['__getitem__']('class')['append']('modulspan');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_53_err){if (!$p['isinstance']($pyjs_dbg_53_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_53_err);}throw $pyjs_dbg_53_err;
}})();
						$pyjs['track']['lineno']=91;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return logDiv['appendChild'](spanMsg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_54_err){if (!$p['isinstance']($pyjs_dbg_54_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_54_err);}throw $pyjs_dbg_54_err;
}})();
					}
					$pyjs['track']['lineno']=92;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and3=(function(){try{try{$pyjs['in_try_except'] += 1;
					return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_55_err){if (!$p['isinstance']($pyjs_dbg_55_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_55_err);}throw $pyjs_dbg_55_err;
}})()['__contains__']('values'))?(function(){try{try{$pyjs['in_try_except'] += 1;
					return data['__getitem__']('values')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_56_err){if (!$p['isinstance']($pyjs_dbg_56_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_56_err);}throw $pyjs_dbg_56_err;
}})()['__contains__']('name'):$and3));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_57_err){if (!$p['isinstance']($pyjs_dbg_57_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_57_err);}throw $pyjs_dbg_57_err;
}})()) {
						$pyjs['track']['lineno']=93;
						spanMsg = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_58_err){if (!$p['isinstance']($pyjs_dbg_58_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_58_err);}throw $pyjs_dbg_58_err;
}})();
						$pyjs['track']['lineno']=94;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return spanMsg['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['TextNode']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['str'](data['__getitem__']('values')['__getitem__']('name'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_59_err){if (!$p['isinstance']($pyjs_dbg_59_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_59_err);}throw $pyjs_dbg_59_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_60_err){if (!$p['isinstance']($pyjs_dbg_60_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_60_err);}throw $pyjs_dbg_60_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_61_err){if (!$p['isinstance']($pyjs_dbg_61_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_61_err);}throw $pyjs_dbg_61_err;
}})();
						$pyjs['track']['lineno']=95;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return spanMsg['__getitem__']('class')['append']('namespan');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_62_err){if (!$p['isinstance']($pyjs_dbg_62_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_62_err);}throw $pyjs_dbg_62_err;
}})();
						$pyjs['track']['lineno']=96;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return logDiv['appendChild'](spanMsg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_63_err){if (!$p['isinstance']($pyjs_dbg_63_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_63_err);}throw $pyjs_dbg_63_err;
}})();
					}
					$pyjs['track']['lineno']=97;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['conf']['__getitem__']('mainWindow')['log']('success', logDiv);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})();
					$pyjs['track']['lineno']=98;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'closeOnSuccess'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_65_err){if (!$p['isinstance']($pyjs_dbg_65_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_65_err);}throw $pyjs_dbg_65_err;
}})()) {
						$pyjs['track']['lineno']=99;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['conf']['__getitem__']('mainWindow')['removeWidget'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_66_err){if (!$p['isinstance']($pyjs_dbg_66_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_66_err);}throw $pyjs_dbg_66_err;
}})();
						$pyjs['track']['lineno']=100;
						$pyjs['track']['lineno']=100;
						var $pyjs__ret = null;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
					$pyjs['track']['lineno']=101;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['clear']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})();
					$pyjs['track']['lineno']=103;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['reloadData']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_68_err){if (!$p['isinstance']($pyjs_dbg_68_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_68_err);}throw $pyjs_dbg_68_err;
}})();
					$pyjs['track']['lineno']=104;
					$pyjs['track']['lineno']=104;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=106;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['clear']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_69_err){if (!$p['isinstance']($pyjs_dbg_69_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_69_err);}throw $pyjs_dbg_69_err;
}})();
				$pyjs['track']['lineno']=107;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['actionbar']['resetLoadingState']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_70_err){if (!$p['isinstance']($pyjs_dbg_70_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_70_err);}throw $pyjs_dbg_70_err;
}})();
				$pyjs['track']['lineno']=108;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('dataCache', data) : $p['setattr'](self, 'dataCache', data); 
				$pyjs['track']['lineno']=110;
				fieldSets = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_71_err){if (!$p['isinstance']($pyjs_dbg_71_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_71_err);}throw $pyjs_dbg_71_err;
}})();
				$pyjs['track']['lineno']=111;
				cat = 'byweek';
				$pyjs['track']['lineno']=112;
				fs = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Fieldset']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_72_err){if (!$p['isinstance']($pyjs_dbg_72_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_72_err);}throw $pyjs_dbg_72_err;
}})();
				$pyjs['track']['lineno']=113;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return fs['__setitem__']('class', cat);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_73_err){if (!$p['isinstance']($pyjs_dbg_73_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_73_err);}throw $pyjs_dbg_73_err;
}})();
				$pyjs['track']['lineno']=114;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq'](cat, 'byweek'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_74_err){if (!$p['isinstance']($pyjs_dbg_74_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_74_err);}throw $pyjs_dbg_74_err;
}})()) {
					$pyjs['track']['lineno']=115;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return fs['__getitem__']('class')['append']('active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})();
				}
				$pyjs['track']['lineno']=117;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return fs['__setitem__']('name', cat);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})();
				$pyjs['track']['lineno']=118;
				legend = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Legend']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_77_err){if (!$p['isinstance']($pyjs_dbg_77_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_77_err);}throw $pyjs_dbg_77_err;
}})();
				$pyjs['track']['lineno']=119;
				fshref = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['fieldset_A']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_78_err){if (!$p['isinstance']($pyjs_dbg_78_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_78_err);}throw $pyjs_dbg_78_err;
}})();
				$pyjs['track']['lineno']=120;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return fshref['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['TextNode'](cat);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_79_err){if (!$p['isinstance']($pyjs_dbg_79_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_79_err);}throw $pyjs_dbg_79_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_80_err){if (!$p['isinstance']($pyjs_dbg_80_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_80_err);}throw $pyjs_dbg_80_err;
}})();
				$pyjs['track']['lineno']=121;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return legend['appendChild'](fshref);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_81_err){if (!$p['isinstance']($pyjs_dbg_81_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_81_err);}throw $pyjs_dbg_81_err;
}})();
				$pyjs['track']['lineno']=122;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return fs['appendChild'](legend);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_82_err){if (!$p['isinstance']($pyjs_dbg_82_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_82_err);}throw $pyjs_dbg_82_err;
}})();
				$pyjs['track']['lineno']=123;
				section = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Section']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_83_err){if (!$p['isinstance']($pyjs_dbg_83_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_83_err);}throw $pyjs_dbg_83_err;
}})();
				$pyjs['track']['lineno']=124;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return fs['appendChild'](section);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_84_err){if (!$p['isinstance']($pyjs_dbg_84_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_84_err);}throw $pyjs_dbg_84_err;
}})();
				$pyjs['track']['lineno']=125;
				fs['__is_instance__'] && typeof fs['__setattr__'] == 'function' ? fs['__setattr__']('_section', section) : $p['setattr'](fs, '_section', section); 
				$pyjs['track']['lineno']=126;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return fieldSets['__setitem__'](cat, fs);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})();
				$pyjs['track']['lineno']=128;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('dtstart', data['__getitem__']('values')['__getitem__']('startdate')) : $p['setattr'](self, 'dtstart', data['__getitem__']('values')['__getitem__']('startdate')); 
				$pyjs['track']['lineno']=129;
				startdateLabel = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Label']('Termin');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})();
				$pyjs['track']['lineno']=130;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return startdateLabel['__getitem__']('class')['append']('termin');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})();
				$pyjs['track']['lineno']=131;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return startdateLabel['__getitem__']('class')['append']('date');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})();
				$pyjs['track']['lineno']=132;
				startdate_id = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('vi_%s_%s_edit_bn_%s', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['tuple']([$p['getattr'](self, 'editIdx'), $p['getattr'](self, 'modul'), 'repeatdate']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_89_err){if (!$p['isinstance']($pyjs_dbg_89_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_89_err);}throw $pyjs_dbg_89_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})();
				$pyjs['track']['lineno']=133;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return startdateLabel['__setitem__']('for', startdate_id);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_91_err){if (!$p['isinstance']($pyjs_dbg_91_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_91_err);}throw $pyjs_dbg_91_err;
}})();
				$pyjs['track']['lineno']=134;
				startdate = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['date']['DateViewBoneDelegate']('termin', 'startdate', skelStructure);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_92_err){if (!$p['isinstance']($pyjs_dbg_92_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_92_err);}throw $pyjs_dbg_92_err;
}})()['render'](data['__getitem__']('values'), 'startdate');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_93_err){if (!$p['isinstance']($pyjs_dbg_93_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_93_err);}throw $pyjs_dbg_93_err;
}})();
				$pyjs['track']['lineno']=135;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return startdate['__setitem__']('id', startdate_id);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_94_err){if (!$p['isinstance']($pyjs_dbg_94_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_94_err);}throw $pyjs_dbg_94_err;
}})();
				$pyjs['track']['lineno']=136;
				containerDiv = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_95_err){if (!$p['isinstance']($pyjs_dbg_95_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_95_err);}throw $pyjs_dbg_95_err;
}})();
				$pyjs['track']['lineno']=137;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return containerDiv['appendChild'](startdateLabel);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_96_err){if (!$p['isinstance']($pyjs_dbg_96_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_96_err);}throw $pyjs_dbg_96_err;
}})();
				$pyjs['track']['lineno']=138;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return containerDiv['appendChild'](startdate);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_97_err){if (!$p['isinstance']($pyjs_dbg_97_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_97_err);}throw $pyjs_dbg_97_err;
}})();
				$pyjs['track']['lineno']=139;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return containerDiv['__getitem__']('class')['append']('bone');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_98_err){if (!$p['isinstance']($pyjs_dbg_98_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_98_err);}throw $pyjs_dbg_98_err;
}})();
				$pyjs['track']['lineno']=140;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return containerDiv['__getitem__']('class')['append']('bone_startdate');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_99_err){if (!$p['isinstance']($pyjs_dbg_99_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_99_err);}throw $pyjs_dbg_99_err;
}})();
				$pyjs['track']['lineno']=141;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return containerDiv['__getitem__']('class')['append']('date');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_100_err){if (!$p['isinstance']($pyjs_dbg_100_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_100_err);}throw $pyjs_dbg_100_err;
}})();
				$pyjs['track']['lineno']=142;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return fieldSets['__getitem__'](cat)['_section']['appendChild'](containerDiv);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_101_err){if (!$p['isinstance']($pyjs_dbg_101_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_101_err);}throw $pyjs_dbg_101_err;
}})();
				$pyjs['track']['lineno']=144;
				countLabel = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Label']('Wiederholungen');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_102_err){if (!$p['isinstance']($pyjs_dbg_102_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_102_err);}throw $pyjs_dbg_102_err;
}})();
				$pyjs['track']['lineno']=145;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return countLabel['__getitem__']('class')['append']('count');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_103_err){if (!$p['isinstance']($pyjs_dbg_103_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_103_err);}throw $pyjs_dbg_103_err;
}})();
				$pyjs['track']['lineno']=146;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return countLabel['__getitem__']('class')['append']('numeric');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_104_err){if (!$p['isinstance']($pyjs_dbg_104_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_104_err);}throw $pyjs_dbg_104_err;
}})();
				$pyjs['track']['lineno']=147;
				count_id = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('vi_%s_%s_edit_bn_%s', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['tuple']([$p['getattr'](self, 'editIdx'), $p['getattr'](self, 'modul'), 'count']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_105_err){if (!$p['isinstance']($pyjs_dbg_105_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_105_err);}throw $pyjs_dbg_105_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_106_err){if (!$p['isinstance']($pyjs_dbg_106_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_106_err);}throw $pyjs_dbg_106_err;
}})();
				$pyjs['track']['lineno']=148;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return countLabel['__setitem__']('for', count_id);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_107_err){if (!$p['isinstance']($pyjs_dbg_107_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_107_err);}throw $pyjs_dbg_107_err;
}})();
				$pyjs['track']['lineno']=150;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('count', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_108_err){if (!$p['isinstance']($pyjs_dbg_108_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_108_err);}throw $pyjs_dbg_108_err;
}})()) : $p['setattr'](self, 'count', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_108_err){if (!$p['isinstance']($pyjs_dbg_108_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_108_err);}throw $pyjs_dbg_108_err;
}})()); 
				$pyjs['track']['lineno']=151;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'count')['__setitem__']('id', count_id);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_109_err){if (!$p['isinstance']($pyjs_dbg_109_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_109_err);}throw $pyjs_dbg_109_err;
}})();
				$pyjs['track']['lineno']=152;
				containerDiv2 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_110_err){if (!$p['isinstance']($pyjs_dbg_110_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_110_err);}throw $pyjs_dbg_110_err;
}})();
				$pyjs['track']['lineno']=153;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return containerDiv2['__getitem__']('class')['append']('bone');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_111_err){if (!$p['isinstance']($pyjs_dbg_111_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_111_err);}throw $pyjs_dbg_111_err;
}})();
				$pyjs['track']['lineno']=154;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return containerDiv2['__getitem__']('class')['append']('bone_count');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_112_err){if (!$p['isinstance']($pyjs_dbg_112_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_112_err);}throw $pyjs_dbg_112_err;
}})();
				$pyjs['track']['lineno']=155;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return containerDiv2['__getitem__']('class')['append']('date');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_113_err){if (!$p['isinstance']($pyjs_dbg_113_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_113_err);}throw $pyjs_dbg_113_err;
}})();
				$pyjs['track']['lineno']=156;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return containerDiv2['appendChild'](countLabel);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_114_err){if (!$p['isinstance']($pyjs_dbg_114_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_114_err);}throw $pyjs_dbg_114_err;
}})();
				$pyjs['track']['lineno']=157;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return containerDiv2['appendChild']($p['getattr'](self, 'count'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_115_err){if (!$p['isinstance']($pyjs_dbg_115_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_115_err);}throw $pyjs_dbg_115_err;
}})();
				$pyjs['track']['lineno']=176;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return fieldSets['__getitem__'](cat)['_section']['appendChild'](containerDiv2);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_116_err){if (!$p['isinstance']($pyjs_dbg_116_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_116_err);}throw $pyjs_dbg_116_err;
}})();
				$pyjs['track']['lineno']=178;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter1_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return fieldSets['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_118_err){if (!$p['isinstance']($pyjs_dbg_118_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_118_err);}throw $pyjs_dbg_118_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_119_err){if (!$p['isinstance']($pyjs_dbg_119_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_119_err);}throw $pyjs_dbg_119_err;
}})();
				$iter1_nextval=$p['__iter_prepare']($iter1_iter,false);
				while (typeof($p['__wrapped_next']($iter1_nextval)['$nextval']) != 'undefined') {
					var $tupleassign1 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter1_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_117_err){if (!$p['isinstance']($pyjs_dbg_117_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_117_err);}throw $pyjs_dbg_117_err;
}})();
					k = $tupleassign1[0];
					v = $tupleassign1[1];
					$pyjs['track']['lineno']=179;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool'](v['__getitem__']('class')['__contains__']('active')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_120_err){if (!$p['isinstance']($pyjs_dbg_120_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_120_err);}throw $pyjs_dbg_120_err;
}})()) {
						$pyjs['track']['lineno']=180;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return v['__getitem__']('class')['append']('active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_121_err){if (!$p['isinstance']($pyjs_dbg_121_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_121_err);}throw $pyjs_dbg_121_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.repeatdate';
				$pyjs['track']['lineno']=181;
				tmpList = function(){
					var $iter2_nextval,$iter2_type,$iter2_iter,k,$pyjs__trackstack_size_1,$collcomp1,$iter2_idx,v,$iter2_array;
	$collcomp1 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter2_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return fieldSets['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_123_err){if (!$p['isinstance']($pyjs_dbg_123_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_123_err);}throw $pyjs_dbg_123_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_124_err){if (!$p['isinstance']($pyjs_dbg_124_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_124_err);}throw $pyjs_dbg_124_err;
}})();
				$iter2_nextval=$p['__iter_prepare']($iter2_iter,false);
				while (typeof($p['__wrapped_next']($iter2_nextval)['$nextval']) != 'undefined') {
					var $tupleassign2 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter2_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_122_err){if (!$p['isinstance']($pyjs_dbg_122_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_122_err);}throw $pyjs_dbg_122_err;
}})();
					k = $tupleassign2[0];
					v = $tupleassign2[1];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp1['append']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['tuple']([k, v]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_125_err){if (!$p['isinstance']($pyjs_dbg_125_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_125_err);}throw $pyjs_dbg_125_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_126_err){if (!$p['isinstance']($pyjs_dbg_126_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_126_err);}throw $pyjs_dbg_126_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.repeatdate';

	return $collcomp1;}();
				$pyjs['track']['lineno']=182;
				var 				$lambda1 = function(x) {
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);

					$pyjs['track']={'module':'widgets.repeatdate','lineno':182};$pyjs['trackstack']['push']($pyjs['track']);
					$pyjs['track']['module']='widgets.repeatdate';
					$pyjs['track']['lineno']=182;
					$pyjs['track']['lineno']=182;
					$pyjs['track']['lineno']=182;
					var $pyjs__ret = x['__getitem__']($constant_int_0);
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				};
				$lambda1['__name__'] = '$lambda1';

				$lambda1['__bind_type__'] = 0;
				$lambda1['__args__'] = [null,null,['x']];
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(tmpList, 'sort', null, null, [{'key':$lambda1}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_127_err){if (!$p['isinstance']($pyjs_dbg_127_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_127_err);}throw $pyjs_dbg_127_err;
}})();
				$pyjs['track']['lineno']=183;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter3_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return tmpList;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_129_err){if (!$p['isinstance']($pyjs_dbg_129_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_129_err);}throw $pyjs_dbg_129_err;
}})();
				$iter3_nextval=$p['__iter_prepare']($iter3_iter,false);
				while (typeof($p['__wrapped_next']($iter3_nextval)['$nextval']) != 'undefined') {
					var $tupleassign3 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter3_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_128_err){if (!$p['isinstance']($pyjs_dbg_128_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_128_err);}throw $pyjs_dbg_128_err;
}})();
					k = $tupleassign3[0];
					v = $tupleassign3[1];
					$pyjs['track']['lineno']=184;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['form']['appendChild'](v);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_130_err){if (!$p['isinstance']($pyjs_dbg_130_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_130_err);}throw $pyjs_dbg_130_err;
}})();
					$pyjs['track']['lineno']=185;
					v['__is_instance__'] && typeof v['__setattr__'] == 'function' ? v['__setattr__']('_section', null) : $p['setattr'](v, '_section', null); 
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.repeatdate';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['request', null],['data', null],['ignoreMissing', false]]);
			$cls_definition['setData'] = $method;
			$pyjs['track']['lineno']=188;
			$method = $pyjs__bind_method2('clear', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'af7bce2276888a79c42e2ae13501a845') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var c,$iter4_nextval,$iter4_idx,$iter4_type,$pyjs__trackstack_size_1,$iter4_array,$iter4_iter;
				$pyjs['track']={'module':'widgets.repeatdate', 'lineno':188};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.repeatdate';
				$pyjs['track']['lineno']=188;
				$pyjs['track']['lineno']=192;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter4_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__getslice']($p['getattr']($p['getattr'](self, 'form'), '_children'), 0, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_131_err){if (!$p['isinstance']($pyjs_dbg_131_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_131_err);}throw $pyjs_dbg_131_err;
}})();
				$iter4_nextval=$p['__iter_prepare']($iter4_iter,false);
				while (typeof($p['__wrapped_next']($iter4_nextval)['$nextval']) != 'undefined') {
					c = $iter4_nextval['$nextval'];
					$pyjs['track']['lineno']=193;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['form']['removeChild'](c);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_132_err){if (!$p['isinstance']($pyjs_dbg_132_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_132_err);}throw $pyjs_dbg_132_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.repeatdate';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['clear'] = $method;
			$pyjs['track']['lineno']=195;
			$method = $pyjs__bind_method2('showErrorMsg', function(req, code) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					req = arguments[1];
					code = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 3)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'af7bce2276888a79c42e2ae13501a845') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof req == 'undefined') req=arguments['callee']['__args__'][3][1];
				if (typeof code == 'undefined') code=arguments['callee']['__args__'][4][1];
				var $or5,$or4,$or3,$and5,$and6,errorDiv,txt,$or6;
				$pyjs['track']={'module':'widgets.repeatdate', 'lineno':195};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.repeatdate';
				$pyjs['track']['lineno']=195;
				$pyjs['track']['lineno']=199;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'actionbar')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_133_err){if (!$p['isinstance']($pyjs_dbg_133_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_133_err);}throw $pyjs_dbg_133_err;
}})();
				$pyjs['track']['lineno']=200;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'form')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_134_err){if (!$p['isinstance']($pyjs_dbg_134_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_134_err);}throw $pyjs_dbg_134_err;
}})();
				$pyjs['track']['lineno']=201;
				errorDiv = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_135_err){if (!$p['isinstance']($pyjs_dbg_135_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_135_err);}throw $pyjs_dbg_135_err;
}})();
				$pyjs['track']['lineno']=202;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return errorDiv['__getitem__']('class')['append']('error_msg');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_136_err){if (!$p['isinstance']($pyjs_dbg_136_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_136_err);}throw $pyjs_dbg_136_err;
}})();
				$pyjs['track']['lineno']=203;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and5=code)?($p['bool']($or3=$p['op_eq'](code, $constant_int_401))?$or3:$p['op_eq'](code, $constant_int_403)):$and5));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_137_err){if (!$p['isinstance']($pyjs_dbg_137_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_137_err);}throw $pyjs_dbg_137_err;
}})()) {
					$pyjs['track']['lineno']=204;
					txt = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Access denied!');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_138_err){if (!$p['isinstance']($pyjs_dbg_138_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_138_err);}throw $pyjs_dbg_138_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=206;
					txt = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('An unknown error occurred!');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_139_err){if (!$p['isinstance']($pyjs_dbg_139_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_139_err);}throw $pyjs_dbg_139_err;
}})();
				}
				$pyjs['track']['lineno']=207;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return errorDiv['__getitem__']('class')['append']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('error_code_%s', ($p['bool']($or5=code)?$or5:$constant_int_0));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_140_err){if (!$p['isinstance']($pyjs_dbg_140_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_140_err);}throw $pyjs_dbg_140_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_141_err){if (!$p['isinstance']($pyjs_dbg_141_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_141_err);}throw $pyjs_dbg_141_err;
}})();
				$pyjs['track']['lineno']=208;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return errorDiv['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['TextNode'](txt);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_142_err){if (!$p['isinstance']($pyjs_dbg_142_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_142_err);}throw $pyjs_dbg_142_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_143_err){if (!$p['isinstance']($pyjs_dbg_143_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_143_err);}throw $pyjs_dbg_143_err;
}})();
				$pyjs['track']['lineno']=209;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](errorDiv);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_144_err){if (!$p['isinstance']($pyjs_dbg_144_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_144_err);}throw $pyjs_dbg_144_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['req', null],['code', null]]);
			$cls_definition['showErrorMsg'] = $method;
			$pyjs['track']['lineno']=211;
			$method = $pyjs__bind_method2('doSave', function(closeOnSuccess) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					closeOnSuccess = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'af7bce2276888a79c42e2ae13501a845') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof closeOnSuccess == 'undefined') closeOnSuccess=arguments['callee']['__args__'][3][1];
				var data;
				$pyjs['track']={'module':'widgets.repeatdate', 'lineno':211};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.repeatdate';
				$pyjs['track']['lineno']=211;
				$pyjs['track']['lineno']=212;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('closeOnSuccess', closeOnSuccess) : $p['setattr'](self, 'closeOnSuccess', closeOnSuccess); 
				$pyjs['track']['lineno']=213;
				data = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([['count', (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['count']['_getValue']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_145_err){if (!$p['isinstance']($pyjs_dbg_145_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_145_err);}throw $pyjs_dbg_145_err;
}})()], ['kind', '2'], ['dtstart', $p['getattr'](self, 'dtstart')]]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_146_err){if (!$p['isinstance']($pyjs_dbg_146_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_146_err);}throw $pyjs_dbg_146_err;
}})();
				$pyjs['track']['lineno']=217;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'secure':true, 'successHandler':$p['getattr'](self, 'setData'), 'failureHandler':$p['getattr'](self, 'showErrorMsg')}, $p['getattr'](self, 'modul'), (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('addrecurrent/%s', $p['getattr'](self, 'key'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_147_err){if (!$p['isinstance']($pyjs_dbg_147_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_147_err);}throw $pyjs_dbg_147_err;
}})(), data]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_148_err){if (!$p['isinstance']($pyjs_dbg_148_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_148_err);}throw $pyjs_dbg_148_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['closeOnSuccess', false]]);
			$cls_definition['doSave'] = $method;
			$pyjs['track']['lineno']=15;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('RepeatDatePopup', $p['tuple']($bases), $data);
		})();
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end widgets.repeatdate */


/* end module: widgets.repeatdate */


/*
PYJS_DEPS: ['html5', 'html5.ext.popup.Popup', 'html5.ext', 'html5.ext.popup', 'html5.keycodes.isReturn', 'html5.keycodes', 'event.EventDispatcher', 'event', 'priorityqueue.editBoneSelector', 'priorityqueue', 'i18n.translate', 'i18n', 'bones.selectmulti', 'bones', 'bones.date', 'widgets.actionbar.ActionBar', 'widgets', 'widgets.actionbar', 'network.NetworkService', 'network', 'utils', 'config.conf', 'config', 'widgets.edit.fieldset_A', 'widgets.edit']
*/
