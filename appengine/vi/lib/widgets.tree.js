/* start module: widgets.tree */
$pyjs['loaded_modules']['widgets.tree'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['widgets.tree']['__was_initialized__']) return $pyjs['loaded_modules']['widgets.tree'];
	if(typeof $pyjs['loaded_modules']['widgets'] == 'undefined' || !$pyjs['loaded_modules']['widgets']['__was_initialized__']) $p['___import___']('widgets', null);
	var $m = $pyjs['loaded_modules']['widgets.tree'];
	$m['__repr__'] = function() { return '<module: widgets.tree>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'widgets.tree';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	$pyjs['loaded_modules']['widgets']['tree'] = $pyjs['loaded_modules']['widgets.tree'];
	try {
		$m.__track_lines__[1] = 'widgets.tree.py, line 1:\n    import html5';
		$m.__track_lines__[2] = 'widgets.tree.py, line 2:\n    import pyjd # this is dummy in pyjs.';
		$m.__track_lines__[3] = 'widgets.tree.py, line 3:\n    from network import NetworkService';
		$m.__track_lines__[4] = 'widgets.tree.py, line 4:\n    from widgets.actionbar import ActionBar';
		$m.__track_lines__[5] = 'widgets.tree.py, line 5:\n    from widgets.search import Search';
		$m.__track_lines__[6] = 'widgets.tree.py, line 6:\n    from event import EventDispatcher';
		$m.__track_lines__[7] = 'widgets.tree.py, line 7:\n    from priorityqueue import displayDelegateSelector, viewDelegateSelector';
		$m.__track_lines__[8] = 'widgets.tree.py, line 8:\n    import utils';
		$m.__track_lines__[9] = 'widgets.tree.py, line 9:\n    from html5.keycodes import *';
		$m.__track_lines__[10] = 'widgets.tree.py, line 10:\n    from config import conf';
		$m.__track_lines__[12] = 'widgets.tree.py, line 12:\n    class NodeWidget( html5.Div ):';
		$m.__track_lines__[16] = 'widgets.tree.py, line 16:\n    def __init__(self, modul, data, structure, *args, **kwargs ):';
		$m.__track_lines__[25] = 'widgets.tree.py, line 25:\n    super( NodeWidget, self ).__init__( *args, **kwargs )';
		$m.__track_lines__[26] = 'widgets.tree.py, line 26:\n    self.modul = modul';
		$m.__track_lines__[27] = 'widgets.tree.py, line 27:\n    self.data = data';
		$m.__track_lines__[28] = 'widgets.tree.py, line 28:\n    self.structure = structure';
		$m.__track_lines__[29] = 'widgets.tree.py, line 29:\n    self.buildDescription()';
		$m.__track_lines__[30] = 'widgets.tree.py, line 30:\n    self["class"] = "treeitem node supports_drag supports_drop"';
		$m.__track_lines__[31] = 'widgets.tree.py, line 31:\n    self["draggable"] = True';
		$m.__track_lines__[32] = 'widgets.tree.py, line 32:\n    self.sinkEvent("onDragOver","onDrop","onDragStart", "onDragLeave")';
		$m.__track_lines__[34] = 'widgets.tree.py, line 34:\n    def buildDescription(self):';
		$m.__track_lines__[38] = 'widgets.tree.py, line 38:\n    hasDescr = False';
		$m.__track_lines__[39] = 'widgets.tree.py, line 39:\n    for boneName, boneInfo in self.structure:';
		$m.__track_lines__[40] = 'widgets.tree.py, line 40:\n    if "params" in boneInfo.keys() and isinstance(boneInfo["params"], dict):';
		$m.__track_lines__[41] = 'widgets.tree.py, line 41:\n    params = boneInfo["params"]';
		$m.__track_lines__[42] = 'widgets.tree.py, line 42:\n    if "frontend_default_visible" in params.keys() and params["frontend_default_visible"]:';
		$m.__track_lines__[43] = 'widgets.tree.py, line 43:\n    wdg = viewDelegateSelector.select( self.modul, boneName, utils.boneListToDict(self.structure) )';
		$m.__track_lines__[44] = 'widgets.tree.py, line 44:\n    if wdg is not None:';
		$m.__track_lines__[45] = 'widgets.tree.py, line 45:\n    self.appendChild( wdg(self.modul, boneName, utils.boneListToDict(self.structure) ).render( self.data, boneName ))';
		$m.__track_lines__[46] = 'widgets.tree.py, line 46:\n    hasDescr = True';
		$m.__track_lines__[47] = 'widgets.tree.py, line 47:\n    if not hasDescr:';
		$m.__track_lines__[48] = 'widgets.tree.py, line 48:\n    self.appendChild( html5.TextNode( self.data["name"]))';
		$m.__track_lines__[50] = 'widgets.tree.py, line 50:\n    def onDragOver(self, event):';
		$m.__track_lines__[54] = 'widgets.tree.py, line 54:\n    if not "insert_here" in self["class"]:';
		$m.__track_lines__[55] = 'widgets.tree.py, line 55:\n    self["class"].append("insert_here")';
		$m.__track_lines__[56] = 'widgets.tree.py, line 56:\n    try:';
		$m.__track_lines__[57] = 'widgets.tree.py, line 57:\n    nodeType, srcKey = event.dataTransfer.getData("Text").split("/")';
		$m.__track_lines__[59] = 'widgets.tree.py, line 59:\n    return( super(NodeWidget,self).onDragOver(event) )';
		$m.__track_lines__[60] = 'widgets.tree.py, line 60:\n    event.preventDefault()';
		$m.__track_lines__[61] = 'widgets.tree.py, line 61:\n    event.stopPropagation()';
		$m.__track_lines__[63] = 'widgets.tree.py, line 63:\n    def onDragLeave(self, event):';
		$m.__track_lines__[64] = 'widgets.tree.py, line 64:\n    if "insert_here" in self["class"]:';
		$m.__track_lines__[65] = 'widgets.tree.py, line 65:\n    self["class"].remove("insert_here")';
		$m.__track_lines__[66] = 'widgets.tree.py, line 66:\n    return( super(NodeWidget, self).onDragLeave(event))';
		$m.__track_lines__[68] = 'widgets.tree.py, line 68:\n    def onDragStart(self, event):';
		$m.__track_lines__[72] = 'widgets.tree.py, line 72:\n    event.dataTransfer.setData( "Text", "node/"+self.data["id"] )';
		$m.__track_lines__[73] = 'widgets.tree.py, line 73:\n    event.stopPropagation()';
		$m.__track_lines__[75] = 'widgets.tree.py, line 75:\n    def onDrop(self, event):';
		$m.__track_lines__[79] = 'widgets.tree.py, line 79:\n    try:';
		$m.__track_lines__[80] = 'widgets.tree.py, line 80:\n    nodeType, srcKey = event.dataTransfer.getData("Text").split("/")';
		$m.__track_lines__[82] = 'widgets.tree.py, line 82:\n    return';
		$m.__track_lines__[83] = 'widgets.tree.py, line 83:\n    NetworkService.request(self.modul,"move",{"skelType": nodeType, "id":srcKey, "destNode": self.data["id"]}, modifies=True, secure=True)';
		$m.__track_lines__[84] = 'widgets.tree.py, line 84:\n    event.preventDefault()';
		$m.__track_lines__[85] = 'widgets.tree.py, line 85:\n    event.stopPropagation()';
		$m.__track_lines__[89] = 'widgets.tree.py, line 89:\n    class LeafWidget( html5.Div ):';
		$m.__track_lines__[93] = 'widgets.tree.py, line 93:\n    def __init__(self, modul, data, structure, *args, **kwargs ):';
		$m.__track_lines__[102] = 'widgets.tree.py, line 102:\n    super( LeafWidget, self ).__init__( *args, **kwargs )';
		$m.__track_lines__[103] = 'widgets.tree.py, line 103:\n    self.modul = modul';
		$m.__track_lines__[104] = 'widgets.tree.py, line 104:\n    self.data = data';
		$m.__track_lines__[105] = 'widgets.tree.py, line 105:\n    self.structure = structure';
		$m.__track_lines__[106] = 'widgets.tree.py, line 106:\n    self.buildDescription()';
		$m.__track_lines__[107] = 'widgets.tree.py, line 107:\n    self.element.innerHTML = data["name"]';
		$m.__track_lines__[108] = 'widgets.tree.py, line 108:\n    self["class"] = "treeitem leaf supports_drag"';
		$m.__track_lines__[109] = 'widgets.tree.py, line 109:\n    self["draggable"] = True';
		$m.__track_lines__[110] = 'widgets.tree.py, line 110:\n    self.sinkEvent("onDragStart")';
		$m.__track_lines__[112] = 'widgets.tree.py, line 112:\n    def buildDescription(self):';
		$m.__track_lines__[116] = 'widgets.tree.py, line 116:\n    hasDescr = False';
		$m.__track_lines__[117] = 'widgets.tree.py, line 117:\n    for boneName, boneInfo in self.structure:';
		$m.__track_lines__[118] = 'widgets.tree.py, line 118:\n    if "params" in boneInfo.keys() and isinstance(boneInfo["params"], dict):';
		$m.__track_lines__[119] = 'widgets.tree.py, line 119:\n    params = boneInfo["params"]';
		$m.__track_lines__[120] = 'widgets.tree.py, line 120:\n    if "frontend_default_visible" in params.keys() and params["frontend_default_visible"]:';
		$m.__track_lines__[121] = 'widgets.tree.py, line 121:\n    wdg = viewDelegateSelector.select( self.modul, boneName, utils.boneListToDict(self.structure) )';
		$m.__track_lines__[122] = 'widgets.tree.py, line 122:\n    if wdg is not None:';
		$m.__track_lines__[123] = 'widgets.tree.py, line 123:\n    self.appendChild( wdg(self.modul, boneName, utils.boneListToDict(self.structure) ).render( self.data, boneName ))';
		$m.__track_lines__[124] = 'widgets.tree.py, line 124:\n    hasDescr = True';
		$m.__track_lines__[125] = 'widgets.tree.py, line 125:\n    if not hasDescr:';
		$m.__track_lines__[126] = 'widgets.tree.py, line 126:\n    self.appendChild( html5.TextNode( self.data["name"]))';
		$m.__track_lines__[128] = 'widgets.tree.py, line 128:\n    def onDragStart(self, event):';
		$m.__track_lines__[132] = 'widgets.tree.py, line 132:\n    event.dataTransfer.setData( "Text", "leaf/"+self.data["id"] )';
		$m.__track_lines__[133] = 'widgets.tree.py, line 133:\n    event.stopPropagation()';
		$m.__track_lines__[135] = 'widgets.tree.py, line 135:\n    class SelectableDiv( html5.Div ):';
		$m.__track_lines__[144] = 'widgets.tree.py, line 144:\n    def __init__(self, nodeWidget, leafWidget, selectionType="both", multiSelection=False, *args, **kwargs ):';
		$m.__track_lines__[145] = 'widgets.tree.py, line 145:\n    super( SelectableDiv, self ).__init__(*args, **kwargs)';
		$m.__track_lines__[146] = 'widgets.tree.py, line 146:\n    self["class"].append("selectioncontainer")';
		$m.__track_lines__[147] = 'widgets.tree.py, line 147:\n    self["tabindex"] = 1';
		$m.__track_lines__[148] = 'widgets.tree.py, line 148:\n    self.selectionType = selectionType';
		$m.__track_lines__[149] = 'widgets.tree.py, line 149:\n    self.multiSelection = multiSelection';
		$m.__track_lines__[150] = 'widgets.tree.py, line 150:\n    self.nodeWidget = nodeWidget';
		$m.__track_lines__[151] = 'widgets.tree.py, line 151:\n    self.leafWidget = leafWidget';
		$m.__track_lines__[152] = 'widgets.tree.py, line 152:\n    self.selectionChangedEvent = EventDispatcher("selectionChanged")';
		$m.__track_lines__[153] = 'widgets.tree.py, line 153:\n    self.selectionActivatedEvent = EventDispatcher("selectionActivated")';
		$m.__track_lines__[154] = 'widgets.tree.py, line 154:\n    self.cursorMovedEvent = EventDispatcher("cursorMoved")';
		$m.__track_lines__[155] = 'widgets.tree.py, line 155:\n    self._selectedItems = [] # List of row-indexes currently selected';
		$m.__track_lines__[156] = 'widgets.tree.py, line 156:\n    self._currentItem = None # Rowindex of the cursor row';
		$m.__track_lines__[157] = 'widgets.tree.py, line 157:\n    self._isMouseDown = False # Tracks status of the left mouse button';
		$m.__track_lines__[158] = 'widgets.tree.py, line 158:\n    self._isCtlPressed = False # Tracks status of the ctrl key';
		$m.__track_lines__[159] = 'widgets.tree.py, line 159:\n    self._ctlStartRow = None # Stores the row where a multi-selection (using the ctrl key) started';
		$m.__track_lines__[160] = 'widgets.tree.py, line 160:\n    self.sinkEvent( "onClick","onDblClick", "onMouseMove", "onMouseDown", "onMouseUp", "onKeyDown", "onKeyUp" )';
		$m.__track_lines__[162] = 'widgets.tree.py, line 162:\n    def setCurrentItem(self, item):';
		$m.__track_lines__[167] = 'widgets.tree.py, line 167:\n    if self._currentItem:';
		$m.__track_lines__[168] = 'widgets.tree.py, line 168:\n    self._currentItem["class"].remove("cursor")';
		$m.__track_lines__[169] = 'widgets.tree.py, line 169:\n    self._currentItem = item';
		$m.__track_lines__[170] = 'widgets.tree.py, line 170:\n    if item:';
		$m.__track_lines__[171] = 'widgets.tree.py, line 171:\n    item["class"].append("cursor")';
		$m.__track_lines__[173] = 'widgets.tree.py, line 173:\n    def onClick(self, event):';
		$m.__track_lines__[174] = 'widgets.tree.py, line 174:\n    self.focus()';
		$m.__track_lines__[175] = 'widgets.tree.py, line 175:\n    for child in self._children:';
		$m.__track_lines__[176] = 'widgets.tree.py, line 176:\n    if utils.doesEventHitWidgetOrChildren( event, child ):';
		$m.__track_lines__[177] = 'widgets.tree.py, line 177:\n    self.setCurrentItem( child )';
		$m.__track_lines__[178] = 'widgets.tree.py, line 178:\n    if self._isCtlPressed:';
		$m.__track_lines__[179] = 'widgets.tree.py, line 179:\n    self.addSelectedItem( child )';
		$m.__track_lines__[180] = 'widgets.tree.py, line 180:\n    if not self._isCtlPressed:';
		$m.__track_lines__[181] = 'widgets.tree.py, line 181:\n    self.clearSelection()';
		$m.__track_lines__[182] = 'widgets.tree.py, line 182:\n    if self._selectedItems:';
		$m.__track_lines__[183] = 'widgets.tree.py, line 183:\n    self.selectionChangedEvent.fire( self, self._selectedItems[:])';
		$m.__track_lines__[185] = 'widgets.tree.py, line 185:\n    self.selectionChangedEvent.fire( self, [self._currentItem])';
		$m.__track_lines__[187] = 'widgets.tree.py, line 187:\n    self.selectionChangedEvent.fire( self, [])';
		$m.__track_lines__[189] = 'widgets.tree.py, line 189:\n    def onDblClick(self, event):';
		$m.__track_lines__[190] = 'widgets.tree.py, line 190:\n    for child in self._children:';
		$m.__track_lines__[191] = 'widgets.tree.py, line 191:\n    if utils.doesEventHitWidgetOrChildren( event, child ):';
		$m.__track_lines__[192] = 'widgets.tree.py, line 192:\n    if self.selectionType=="node" and isinstance( child, self.nodeWidget ) or \\';
		$m.__track_lines__[195] = 'widgets.tree.py, line 195:\n    self.selectionActivatedEvent.fire( self, [child] )';
		$m.__track_lines__[197] = 'widgets.tree.py, line 197:\n    def activateCurrentSelection(self):';
		$m.__track_lines__[202] = 'widgets.tree.py, line 202:\n    if len( self._selectedItems )>0:';
		$m.__track_lines__[203] = 'widgets.tree.py, line 203:\n    self.selectionActivatedEvent.fire( self, self._selectedItems )';
		$m.__track_lines__[206] = 'widgets.tree.py, line 206:\n    self.selectionActivatedEvent.fire( self, [self._currentItem] )';
		$m.__track_lines__[208] = 'widgets.tree.py, line 208:\n    def onKeyDown(self, event):';
		$m.__track_lines__[209] = 'widgets.tree.py, line 209:\n    if isReturn(event.keyCode): # Return';
		$m.__track_lines__[210] = 'widgets.tree.py, line 210:\n    self.activateCurrentSelection()';
		$m.__track_lines__[211] = 'widgets.tree.py, line 211:\n    event.preventDefault()';
		$m.__track_lines__[212] = 'widgets.tree.py, line 212:\n    return';
		$m.__track_lines__[214] = 'widgets.tree.py, line 214:\n    self._isCtlPressed = True';
		$m.__track_lines__[216] = 'widgets.tree.py, line 216:\n    def onKeyUp(self, event):';
		$m.__track_lines__[217] = 'widgets.tree.py, line 217:\n    if isSingleSelectionKey(event.keyCode):';
		$m.__track_lines__[218] = 'widgets.tree.py, line 218:\n    self._isCtlPressed = False';
		$m.__track_lines__[220] = 'widgets.tree.py, line 220:\n    def clearSelection(self):';
		$m.__track_lines__[221] = 'widgets.tree.py, line 221:\n    for child in self._children[:]:';
		$m.__track_lines__[222] = 'widgets.tree.py, line 222:\n    self.removeSelectedItem( child )';
		$m.__track_lines__[224] = 'widgets.tree.py, line 224:\n    def addSelectedItem(self, item):';
		$m.__track_lines__[227] = 'widgets.tree.py, line 227:\n    if self.selectionType=="node" and isinstance( item, self.nodeWidget ) or \\';
		$m.__track_lines__[230] = 'widgets.tree.py, line 230:\n    if not item in self._selectedItems:';
		$m.__track_lines__[231] = 'widgets.tree.py, line 231:\n    self._selectedItems.append( item )';
		$m.__track_lines__[232] = 'widgets.tree.py, line 232:\n    item["class"].append("selected")';
		$m.__track_lines__[234] = 'widgets.tree.py, line 234:\n    def removeSelectedItem(self,item):';
		$m.__track_lines__[235] = 'widgets.tree.py, line 235:\n    if not item in self._selectedItems:';
		$m.__track_lines__[236] = 'widgets.tree.py, line 236:\n    return';
		$m.__track_lines__[237] = 'widgets.tree.py, line 237:\n    self._selectedItems.remove( item )';
		$m.__track_lines__[238] = 'widgets.tree.py, line 238:\n    item["class"].remove("selected")';
		$m.__track_lines__[240] = 'widgets.tree.py, line 240:\n    def clear(self):';
		$m.__track_lines__[241] = 'widgets.tree.py, line 241:\n    self.clearSelection()';
		$m.__track_lines__[242] = 'widgets.tree.py, line 242:\n    for child in self._children[:]:';
		$m.__track_lines__[243] = 'widgets.tree.py, line 243:\n    self.removeChild( child )';
		$m.__track_lines__[244] = 'widgets.tree.py, line 244:\n    self.selectionChangedEvent.fire( self, [] )';
		$m.__track_lines__[246] = 'widgets.tree.py, line 246:\n    def getCurrentSelection(self):';
		$m.__track_lines__[247] = 'widgets.tree.py, line 247:\n    if len(self._selectedItems)>0:';
		$m.__track_lines__[248] = 'widgets.tree.py, line 248:\n    return( self._selectedItems[:] )';
		$m.__track_lines__[249] = 'widgets.tree.py, line 249:\n    if self._currentItem:';
		$m.__track_lines__[250] = 'widgets.tree.py, line 250:\n    return( [self._currentItem] )';
		$m.__track_lines__[251] = 'widgets.tree.py, line 251:\n    return( None )';
		$m.__track_lines__[253] = 'widgets.tree.py, line 253:\n    class TreeWidget( html5.Div ):';
		$m.__track_lines__[255] = 'widgets.tree.py, line 255:\n    nodeWidget = NodeWidget';
		$m.__track_lines__[256] = 'widgets.tree.py, line 256:\n    leafWidget = LeafWidget';
		$m.__track_lines__[257] = 'widgets.tree.py, line 257:\n    defaultActions = ["add.node", "add.leaf", "selectrootnode", "edit", "delete", "reload"]';
		$m.__track_lines__[259] = 'widgets.tree.py, line 259:\n    def __init__( self, modul, rootNode=None, node=None, isSelector=False, *args, **kwargs ):';
		$m.__track_lines__[268] = 'widgets.tree.py, line 268:\n    super( TreeWidget, self ).__init__( )';
		$m.__track_lines__[269] = 'widgets.tree.py, line 269:\n    self["class"].append("tree")';
		$m.__track_lines__[270] = 'widgets.tree.py, line 270:\n    self.modul = modul';
		$m.__track_lines__[271] = 'widgets.tree.py, line 271:\n    self.rootNode = rootNode';
		$m.__track_lines__[272] = 'widgets.tree.py, line 272:\n    self.node = node or rootNode';
		$m.__track_lines__[273] = 'widgets.tree.py, line 273:\n    self.actionBar = ActionBar( modul, "tree" )';
		$m.__track_lines__[274] = 'widgets.tree.py, line 274:\n    self.appendChild( self.actionBar )';
		$m.__track_lines__[275] = 'widgets.tree.py, line 275:\n    self.pathList = html5.Div()';
		$m.__track_lines__[276] = 'widgets.tree.py, line 276:\n    self.pathList["class"].append("breadcrumb")';
		$m.__track_lines__[277] = 'widgets.tree.py, line 277:\n    self.appendChild( self.pathList )';
		$m.__track_lines__[278] = 'widgets.tree.py, line 278:\n    self.entryFrame = SelectableDiv( self.nodeWidget, self.leafWidget )';
		$m.__track_lines__[279] = 'widgets.tree.py, line 279:\n    self.appendChild( self.entryFrame )';
		$m.__track_lines__[280] = 'widgets.tree.py, line 280:\n    self.entryFrame.selectionActivatedEvent.register( self )';
		$m.__track_lines__[281] = 'widgets.tree.py, line 281:\n    self._batchSize = 99';
		$m.__track_lines__[282] = 'widgets.tree.py, line 282:\n    self._currentCursor = { "node" : None, "leaf": None }';
		$m.__track_lines__[283] = 'widgets.tree.py, line 283:\n    self._currentRequests = []';
		$m.__track_lines__[284] = 'widgets.tree.py, line 284:\n    self.rootNodeChangedEvent = EventDispatcher("rootNodeChanged")';
		$m.__track_lines__[285] = 'widgets.tree.py, line 285:\n    self.nodeChangedEvent = EventDispatcher("nodeChanged")';
		$m.__track_lines__[286] = 'widgets.tree.py, line 286:\n    self.isSelector = isSelector';
		$m.__track_lines__[287] = 'widgets.tree.py, line 287:\n    if self.rootNode:';
		$m.__track_lines__[288] = 'widgets.tree.py, line 288:\n    self.reloadData()';
		$m.__track_lines__[290] = 'widgets.tree.py, line 290:\n    NetworkService.request(self.modul,"listRootNodes", successHandler=self.onSetDefaultRootNode)';
		$m.__track_lines__[291] = 'widgets.tree.py, line 291:\n    self.path = []';
		$m.__track_lines__[292] = 'widgets.tree.py, line 292:\n    self.sinkEvent( "onClick" )';
		$m.__track_lines__[294] = 'widgets.tree.py, line 294:\n    for f in ["selectionChangedEvent","selectionActivatedEvent","cursorMovedEvent","getCurrentSelection"]:';
		$m.__track_lines__[295] = 'widgets.tree.py, line 295:\n    setattr( self, f, getattr(self.entryFrame,f))';
		$m.__track_lines__[296] = 'widgets.tree.py, line 296:\n    self.actionBar.setActions( self.defaultActions+(["select","close"] if isSelector else []) )';
		$m.__track_lines__[298] = 'widgets.tree.py, line 298:\n    def showErrorMsg(self, req=None, code=None):';
		$m.__track_lines__[302] = 'widgets.tree.py, line 302:\n    self.actionBar["style"]["display"] = "none"';
		$m.__track_lines__[303] = 'widgets.tree.py, line 303:\n    self.entryFrame["style"]["display"] = "none"';
		$m.__track_lines__[304] = 'widgets.tree.py, line 304:\n    errorDiv = html5.Div()';
		$m.__track_lines__[305] = 'widgets.tree.py, line 305:\n    errorDiv["class"].append("error_msg")';
		$m.__track_lines__[306] = 'widgets.tree.py, line 306:\n    if code and (code==401 or code==403):';
		$m.__track_lines__[307] = 'widgets.tree.py, line 307:\n    txt = "Access denied!"';
		$m.__track_lines__[309] = 'widgets.tree.py, line 309:\n    txt = "An unknown error occurred!"';
		$m.__track_lines__[310] = 'widgets.tree.py, line 310:\n    errorDiv["class"].append("error_code_%s" % (code or 0))';
		$m.__track_lines__[311] = 'widgets.tree.py, line 311:\n    errorDiv.appendChild( html5.TextNode( txt ) )';
		$m.__track_lines__[312] = 'widgets.tree.py, line 312:\n    self.appendChild( errorDiv )';
		$m.__track_lines__[314] = 'widgets.tree.py, line 314:\n    def onAttach(self):';
		$m.__track_lines__[315] = 'widgets.tree.py, line 315:\n    super( TreeWidget, self ).onAttach()';
		$m.__track_lines__[316] = 'widgets.tree.py, line 316:\n    NetworkService.registerChangeListener( self )';
		$m.__track_lines__[318] = 'widgets.tree.py, line 318:\n    def onDetach(self):';
		$m.__track_lines__[319] = 'widgets.tree.py, line 319:\n    super( TreeWidget, self ).onDetach()';
		$m.__track_lines__[320] = 'widgets.tree.py, line 320:\n    NetworkService.removeChangeListener( self )';
		$m.__track_lines__[323] = 'widgets.tree.py, line 323:\n    def onDataChanged(self, modul):';
		$m.__track_lines__[324] = 'widgets.tree.py, line 324:\n    if modul != self.modul:';
		$m.__track_lines__[325] = 'widgets.tree.py, line 325:\n    isRootNode = False';
		$m.__track_lines__[326] = 'widgets.tree.py, line 326:\n    for k, v in conf[ "modules" ].items():';
		$m.__track_lines__[327] = 'widgets.tree.py, line 327:\n    if k == modul and v.get( "handler" ) == "list" and v.get( "rootNodeOf" ) == self.modul:';
		$m.__track_lines__[328] = 'widgets.tree.py, line 328:\n    isRootNode = True';
		$m.__track_lines__[329] = 'widgets.tree.py, line 329:\n    break';
		$m.__track_lines__[331] = 'widgets.tree.py, line 331:\n    if not isRootNode:';
		$m.__track_lines__[332] = 'widgets.tree.py, line 332:\n    return';
		$m.__track_lines__[334] = 'widgets.tree.py, line 334:\n    self.actionBar.widgets[ "selectrootnode" ].update()';
		$m.__track_lines__[335] = 'widgets.tree.py, line 335:\n    self.reloadData()';
		$m.__track_lines__[337] = 'widgets.tree.py, line 337:\n    def onSelectionActivated(self, div, selection ):';
		$m.__track_lines__[338] = 'widgets.tree.py, line 338:\n    if len(selection)!=1:';
		$m.__track_lines__[339] = 'widgets.tree.py, line 339:\n    if self.isSelector:';
		$m.__track_lines__[340] = 'widgets.tree.py, line 340:\n    conf["mainWindow"].removeWidget(self)';
		$m.__track_lines__[341] = 'widgets.tree.py, line 341:\n    return';
		$m.__track_lines__[342] = 'widgets.tree.py, line 342:\n    item = selection[0]';
		$m.__track_lines__[343] = 'widgets.tree.py, line 343:\n    if isinstance( item, self.nodeWidget ):';
		$m.__track_lines__[344] = 'widgets.tree.py, line 344:\n    self.path.append( item.data )';
		$m.__track_lines__[345] = 'widgets.tree.py, line 345:\n    self.rebuildPath()';
		$m.__track_lines__[346] = 'widgets.tree.py, line 346:\n    self.setNode( item.data["id"] )';
		$m.__track_lines__[348] = 'widgets.tree.py, line 348:\n    if self.isSelector:';
		$m.__track_lines__[349] = 'widgets.tree.py, line 349:\n    conf["mainWindow"].removeWidget(self)';
		$m.__track_lines__[350] = 'widgets.tree.py, line 350:\n    return';
		$m.__track_lines__[352] = 'widgets.tree.py, line 352:\n    def activateCurrentSelection(self):';
		$m.__track_lines__[353] = 'widgets.tree.py, line 353:\n    return( self.entryFrame.activateCurrentSelection())';
		$m.__track_lines__[355] = 'widgets.tree.py, line 355:\n    def onClick(self, event):';
		$m.__track_lines__[356] = 'widgets.tree.py, line 356:\n    super( TreeWidget, self ).onClick( event )';
		$m.__track_lines__[357] = 'widgets.tree.py, line 357:\n    for c in self.pathList._children:';
		$m.__track_lines__[359] = 'widgets.tree.py, line 359:\n    if utils.doesEventHitWidgetOrParents( event, c ):';
		$m.__track_lines__[360] = 'widgets.tree.py, line 360:\n    self.path = self.path[ : self.pathList._children.index( c )]';
		$m.__track_lines__[361] = 'widgets.tree.py, line 361:\n    self.rebuildPath()';
		$m.__track_lines__[362] = 'widgets.tree.py, line 362:\n    self.setNode( c.data["id"] )';
		$m.__track_lines__[363] = 'widgets.tree.py, line 363:\n    return';
		$m.__track_lines__[365] = 'widgets.tree.py, line 365:\n    def onSetDefaultRootNode(self, req):';
		$m.__track_lines__[366] = 'widgets.tree.py, line 366:\n    data = NetworkService.decode( req )';
		$m.__track_lines__[367] = 'widgets.tree.py, line 367:\n    if len(data)>0:';
		$m.__track_lines__[368] = 'widgets.tree.py, line 368:\n    self.setRootNode( data[0]["key"])';
		$m.__track_lines__[370] = 'widgets.tree.py, line 370:\n    def setRootNode(self, rootNode):';
		$m.__track_lines__[371] = 'widgets.tree.py, line 371:\n    self.rootNode = rootNode';
		$m.__track_lines__[372] = 'widgets.tree.py, line 372:\n    self.node = rootNode';
		$m.__track_lines__[373] = 'widgets.tree.py, line 373:\n    self.rootNodeChangedEvent.fire( rootNode )';
		$m.__track_lines__[374] = 'widgets.tree.py, line 374:\n    self.rebuildPath()';
		$m.__track_lines__[375] = 'widgets.tree.py, line 375:\n    self.reloadData()';
		$m.__track_lines__[377] = 'widgets.tree.py, line 377:\n    def setNode(self, node):';
		$m.__track_lines__[378] = 'widgets.tree.py, line 378:\n    self.node = node';
		$m.__track_lines__[379] = 'widgets.tree.py, line 379:\n    self.nodeChangedEvent.fire( node )';
		$m.__track_lines__[380] = 'widgets.tree.py, line 380:\n    self.reloadData()';
		$m.__track_lines__[382] = 'widgets.tree.py, line 382:\n    def rebuildPath(self):';
		$m.__track_lines__[386] = 'widgets.tree.py, line 386:\n    for c in self.pathList._children[:]:';
		$m.__track_lines__[387] = 'widgets.tree.py, line 387:\n    self.pathList.removeChild( c )';
		$m.__track_lines__[388] = 'widgets.tree.py, line 388:\n    for p in [None]+self.path:';
		$m.__track_lines__[389] = 'widgets.tree.py, line 389:\n    if p is None:';
		$m.__track_lines__[390] = 'widgets.tree.py, line 390:\n    c = NodeWidget( self.modul, {"id":self.rootNode,"name":"root"}, [] )';
		$m.__track_lines__[391] = 'widgets.tree.py, line 391:\n    c["class"].append("is_rootnode")';
		$m.__track_lines__[393] = 'widgets.tree.py, line 393:\n    c = NodeWidget( self.modul, p, [] )';
		$m.__track_lines__[394] = 'widgets.tree.py, line 394:\n    self.pathList.appendChild( c )';
		$m.__track_lines__[398] = 'widgets.tree.py, line 398:\n    def reloadData(self, paramsOverride=None):';
		$m.__track_lines__[399] = 'widgets.tree.py, line 399:\n    assert self.node is not None, "reloadData called while self.node is None"';
		$m.__track_lines__[400] = 'widgets.tree.py, line 400:\n    self.entryFrame.clear()';
		$m.__track_lines__[401] = 'widgets.tree.py, line 401:\n    self._currentRequests = []';
		$m.__track_lines__[402] = 'widgets.tree.py, line 402:\n    if paramsOverride:';
		$m.__track_lines__[403] = 'widgets.tree.py, line 403:\n    params = paramsOverride.copy()';
		$m.__track_lines__[405] = 'widgets.tree.py, line 405:\n    params = { "node":self.node }';
		$m.__track_lines__[407] = 'widgets.tree.py, line 407:\n    if "amount" not in params:';
		$m.__track_lines__[408] = 'widgets.tree.py, line 408:\n    params[ "amount" ] = self._batchSize';
		$m.__track_lines__[410] = 'widgets.tree.py, line 410:\n    r = NetworkService.request(self.modul,"list/node", params,';
		$m.__track_lines__[413] = 'widgets.tree.py, line 413:\n    r.reqType = "node"';
		$m.__track_lines__[414] = 'widgets.tree.py, line 414:\n    self._currentRequests.append( r )';
		$m.__track_lines__[415] = 'widgets.tree.py, line 415:\n    r = NetworkService.request(self.modul,"list/leaf", params,';
		$m.__track_lines__[418] = 'widgets.tree.py, line 418:\n    r.reqType = "leaf"';
		$m.__track_lines__[419] = 'widgets.tree.py, line 419:\n    self._currentRequests.append( r )';
		$m.__track_lines__[421] = 'widgets.tree.py, line 421:\n    def onRequestSucceded(self, req):';
		$m.__track_lines__[422] = 'widgets.tree.py, line 422:\n    if not req in self._currentRequests:';
		$m.__track_lines__[423] = 'widgets.tree.py, line 423:\n    return';
		$m.__track_lines__[424] = 'widgets.tree.py, line 424:\n    self._currentRequests.remove( req )';
		$m.__track_lines__[425] = 'widgets.tree.py, line 425:\n    data = NetworkService.decode( req )';
		$m.__track_lines__[426] = 'widgets.tree.py, line 426:\n    for skel in data["skellist"]:';
		$m.__track_lines__[427] = 'widgets.tree.py, line 427:\n    if req.reqType=="node":';
		$m.__track_lines__[428] = 'widgets.tree.py, line 428:\n    n = self.nodeWidget( self.modul, skel, data["structure"] )';
		$m.__track_lines__[430] = 'widgets.tree.py, line 430:\n    n = self.leafWidget( self.modul, skel, data["structure"] )';
		$m.__track_lines__[432] = 'widgets.tree.py, line 432:\n    self.entryFrame.appendChild(n)';
		$m.__track_lines__[433] = 'widgets.tree.py, line 433:\n    self.entryFrame.sortChildren( self.getChildKey )';
		$m.__track_lines__[435] = 'widgets.tree.py, line 435:\n    if "cursor" in data.keys() and len( data["skellist"] ) == req.params[ "amount" ]:';
		$m.__track_lines__[436] = 'widgets.tree.py, line 436:\n    self._currentCursor[ req.reqType ] = data["cursor"]';
		$m.__track_lines__[438] = 'widgets.tree.py, line 438:\n    req.params[ "cursor" ] = data["cursor"]';
		$m.__track_lines__[439] = 'widgets.tree.py, line 439:\n    r = NetworkService.request(self.modul,"list/%s" % req.reqType, req.params,';
		$m.__track_lines__[442] = 'widgets.tree.py, line 442:\n    r.reqType = req.reqType';
		$m.__track_lines__[443] = 'widgets.tree.py, line 443:\n    self._currentRequests.append( r )';
		$m.__track_lines__[445] = 'widgets.tree.py, line 445:\n    self._currentCursor[ req.reqType ] = None';
		$m.__track_lines__[447] = 'widgets.tree.py, line 447:\n    def getChildKey(self, widget ):';
		$m.__track_lines__[451] = 'widgets.tree.py, line 451:\n    if isinstance( widget, self.nodeWidget ):';
		$m.__track_lines__[452] = 'widgets.tree.py, line 452:\n    return("0-%s" % widget.data["name"].lower())';
		$m.__track_lines__[454] = 'widgets.tree.py, line 454:\n    return("1-%s" % widget.data["name"].lower())';
		$m.__track_lines__[456] = 'widgets.tree.py, line 456:\n    return("2-")';
		$m.__track_lines__[459] = 'widgets.tree.py, line 458:\n    @staticmethod ... def canHandle( modul, modulInfo ):';
		$m.__track_lines__[460] = 'widgets.tree.py, line 460:\n    return( modulInfo["handler"].startswith("tree." ) )';
		$m.__track_lines__[462] = 'widgets.tree.py, line 462:\n    displayDelegateSelector.insert( 1, TreeWidget.canHandle, TreeWidget )';

		var $constant_int_0 = new $p['int'](0);
		var $constant_int_1 = new $p['int'](1);
		var $constant_int_99 = new $p['int'](99);
		var $constant_int_403 = new $p['int'](403);
		var $constant_int_401 = new $p['int'](401);
		$pyjs['track']['module']='widgets.tree';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=1;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['html5'] = $p['___import___']('html5', 'widgets');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=2;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['pyjd'] = $p['___import___']('pyjd', 'widgets');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=3;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['NetworkService'] = $p['___import___']('network.NetworkService', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=4;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['ActionBar'] = $p['___import___']('widgets.actionbar.ActionBar', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=5;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['Search'] = $p['___import___']('widgets.search.Search', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=6;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['EventDispatcher'] = $p['___import___']('event.EventDispatcher', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=7;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['displayDelegateSelector'] = $p['___import___']('priorityqueue.displayDelegateSelector', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['viewDelegateSelector'] = $p['___import___']('priorityqueue.viewDelegateSelector', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=8;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['utils'] = $p['___import___']('utils', 'widgets');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=9;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$p['__import_all__']('html5.keycodes', 'widgets', $m, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=10;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['conf'] = $p['___import___']('config.conf', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=12;
		$m['NodeWidget'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'widgets.tree';
			$cls_definition['__md5__'] = '8d933659f6a763fca71d6c9ca721f917';
			$pyjs['track']['lineno']=16;
			$method = $pyjs__bind_method2('__init__', function(modul, data, structure) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

					var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					modul = arguments[1];
					data = arguments[2];
					structure = arguments[3];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,4,arguments['length']-1));

					var kwargs = arguments['length'] >= 5 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8d933659f6a763fca71d6c9ca721f917') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof structure != 'undefined') {
						if (structure !== null && typeof structure['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = structure;
							structure = arguments[4];
						}
					} else 					if (typeof data != 'undefined') {
						if (data !== null && typeof data['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = data;
							data = arguments[4];
						}
					} else 					if (typeof modul != 'undefined') {
						if (modul !== null && typeof modul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modul;
							modul = arguments[4];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[4];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'widgets.tree', 'lineno':16};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=16;
				$pyjs['track']['lineno']=25;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['NodeWidget'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})();
				$pyjs['track']['lineno']=26;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=27;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('data', data) : $p['setattr'](self, 'data', data); 
				$pyjs['track']['lineno']=28;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('structure', structure) : $p['setattr'](self, 'structure', structure); 
				$pyjs['track']['lineno']=29;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['buildDescription']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})();
				$pyjs['track']['lineno']=30;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'treeitem node supports_drag supports_drop');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})();
				$pyjs['track']['lineno']=31;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('draggable', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})();
				$pyjs['track']['lineno']=32;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['sinkEvent']('onDragOver', 'onDrop', 'onDragStart', 'onDragLeave');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['modul'],['data'],['structure']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=34;
			$method = $pyjs__bind_method2('buildDescription', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8d933659f6a763fca71d6c9ca721f917') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var boneInfo,hasDescr,$iter1_nextval,$iter1_type,boneName,$and1,$and3,$and4,$iter1_iter,params,$iter1_array,$pyjs__trackstack_size_1,wdg,$and2,$iter1_idx;
				$pyjs['track']={'module':'widgets.tree', 'lineno':34};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=34;
				$pyjs['track']['lineno']=38;
				hasDescr = false;
				$pyjs['track']['lineno']=39;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter1_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'structure');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})();
				$iter1_nextval=$p['__iter_prepare']($iter1_iter,false);
				while (typeof($p['__wrapped_next']($iter1_nextval)['$nextval']) != 'undefined') {
					var $tupleassign1 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter1_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})();
					boneName = $tupleassign1[0];
					boneInfo = $tupleassign1[1];
					$pyjs['track']['lineno']=40;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and1=(function(){try{try{$pyjs['in_try_except'] += 1;
					return boneInfo['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})()['__contains__']('params'))?(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](boneInfo['__getitem__']('params'), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})():$and1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})()) {
						$pyjs['track']['lineno']=41;
						params = boneInfo['__getitem__']('params');
						$pyjs['track']['lineno']=42;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](($p['bool']($and3=(function(){try{try{$pyjs['in_try_except'] += 1;
						return params['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})()['__contains__']('frontend_default_visible'))?params['__getitem__']('frontend_default_visible'):$and3));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})()) {
							$pyjs['track']['lineno']=43;
							wdg = (function(){try{try{$pyjs['in_try_except'] += 1;
							return $m['viewDelegateSelector']['select']($p['getattr'](self, 'modul'), boneName, (function(){try{try{$pyjs['in_try_except'] += 1;
							return $m['utils']['boneListToDict']($p['getattr'](self, 'structure'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_14_err){if (!$p['isinstance']($pyjs_dbg_14_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_14_err);}throw $pyjs_dbg_14_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})();
							$pyjs['track']['lineno']=44;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool'](!$p['op_is'](wdg, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_16_err){if (!$p['isinstance']($pyjs_dbg_16_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_16_err);}throw $pyjs_dbg_16_err;
}})()) {
								$pyjs['track']['lineno']=45;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return self['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
								return (function(){try{try{$pyjs['in_try_except'] += 1;
								return wdg($p['getattr'](self, 'modul'), boneName, (function(){try{try{$pyjs['in_try_except'] += 1;
								return $m['utils']['boneListToDict']($p['getattr'](self, 'structure'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_17_err){if (!$p['isinstance']($pyjs_dbg_17_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_17_err);}throw $pyjs_dbg_17_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})()['render']($p['getattr'](self, 'data'), boneName);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_19_err){if (!$p['isinstance']($pyjs_dbg_19_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_19_err);}throw $pyjs_dbg_19_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})();
								$pyjs['track']['lineno']=46;
								hasDescr = true;
							}
						}
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=47;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool'](hasDescr));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})()) {
					$pyjs['track']['lineno']=48;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['TextNode']($p['getattr'](self, 'data')['__getitem__']('name'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['buildDescription'] = $method;
			$pyjs['track']['lineno']=50;
			$method = $pyjs__bind_method2('onDragOver', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8d933659f6a763fca71d6c9ca721f917') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var srcKey,$pyjs_try_err,nodeType;
				$pyjs['track']={'module':'widgets.tree', 'lineno':50};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=50;
				$pyjs['track']['lineno']=54;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool'](self['__getitem__']('class')['__contains__']('insert_here')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})()) {
					$pyjs['track']['lineno']=55;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__getitem__']('class')['append']('insert_here');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})();
				}
				$pyjs['track']['lineno']=56;
				var $pyjs__trackstack_size_1 = $pyjs['trackstack']['length'];
				try {
					try {
						$pyjs['in_try_except'] += 1;
						$pyjs['track']['lineno']=57;
						var $tupleassign2 = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['__ass_unpack']((function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return event['dataTransfer']['getData']('Text');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_26_err){if (!$p['isinstance']($pyjs_dbg_26_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_26_err);}throw $pyjs_dbg_26_err;
}})()['$$split']('/');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_27_err){if (!$p['isinstance']($pyjs_dbg_27_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_27_err);}throw $pyjs_dbg_27_err;
}})(), 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_28_err){if (!$p['isinstance']($pyjs_dbg_28_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_28_err);}throw $pyjs_dbg_28_err;
}})();
						nodeType = $tupleassign2[0];
						srcKey = $tupleassign2[1];
					} finally { $pyjs['in_try_except'] -= 1; }
				} catch($pyjs_try_err) {
					$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_1 - 1);
					$pyjs['__active_exception_stack__'] = null;
					$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
					var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
					$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='widgets.tree';
					if (true) {
						$pyjs['track']['lineno']=59;
						$pyjs['track']['lineno']=59;
						var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['$$super']($m['NodeWidget'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_29_err){if (!$p['isinstance']($pyjs_dbg_29_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_29_err);}throw $pyjs_dbg_29_err;
}})()['onDragOver'](event);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_30_err){if (!$p['isinstance']($pyjs_dbg_30_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_30_err);}throw $pyjs_dbg_30_err;
}})();
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
				}
				$pyjs['track']['lineno']=60;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return event['preventDefault']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_31_err){if (!$p['isinstance']($pyjs_dbg_31_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_31_err);}throw $pyjs_dbg_31_err;
}})();
				$pyjs['track']['lineno']=61;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return event['stopPropagation']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_32_err){if (!$p['isinstance']($pyjs_dbg_32_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_32_err);}throw $pyjs_dbg_32_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onDragOver'] = $method;
			$pyjs['track']['lineno']=63;
			$method = $pyjs__bind_method2('onDragLeave', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8d933659f6a763fca71d6c9ca721f917') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.tree', 'lineno':63};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=63;
				$pyjs['track']['lineno']=64;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](self['__getitem__']('class')['__contains__']('insert_here'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_33_err){if (!$p['isinstance']($pyjs_dbg_33_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_33_err);}throw $pyjs_dbg_33_err;
}})()) {
					$pyjs['track']['lineno']=65;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__getitem__']('class')['remove']('insert_here');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_34_err){if (!$p['isinstance']($pyjs_dbg_34_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_34_err);}throw $pyjs_dbg_34_err;
}})();
				}
				$pyjs['track']['lineno']=66;
				$pyjs['track']['lineno']=66;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['NodeWidget'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})()['onDragLeave'](event);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_36_err){if (!$p['isinstance']($pyjs_dbg_36_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_36_err);}throw $pyjs_dbg_36_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onDragLeave'] = $method;
			$pyjs['track']['lineno']=68;
			$method = $pyjs__bind_method2('onDragStart', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8d933659f6a763fca71d6c9ca721f917') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $add2,$add1;
				$pyjs['track']={'module':'widgets.tree', 'lineno':68};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=68;
				$pyjs['track']['lineno']=72;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return event['dataTransfer']['setData']('Text', $p['__op_add']($add1='node/',$add2=$p['getattr'](self, 'data')['__getitem__']('id')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_37_err){if (!$p['isinstance']($pyjs_dbg_37_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_37_err);}throw $pyjs_dbg_37_err;
}})();
				$pyjs['track']['lineno']=73;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return event['stopPropagation']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_38_err){if (!$p['isinstance']($pyjs_dbg_38_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_38_err);}throw $pyjs_dbg_38_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onDragStart'] = $method;
			$pyjs['track']['lineno']=75;
			$method = $pyjs__bind_method2('onDrop', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8d933659f6a763fca71d6c9ca721f917') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var srcKey,$pyjs_try_err,nodeType;
				$pyjs['track']={'module':'widgets.tree', 'lineno':75};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=75;
				$pyjs['track']['lineno']=79;
				var $pyjs__trackstack_size_1 = $pyjs['trackstack']['length'];
				try {
					try {
						$pyjs['in_try_except'] += 1;
						$pyjs['track']['lineno']=80;
						var $tupleassign3 = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['__ass_unpack']((function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return event['dataTransfer']['getData']('Text');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_39_err){if (!$p['isinstance']($pyjs_dbg_39_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_39_err);}throw $pyjs_dbg_39_err;
}})()['$$split']('/');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_40_err){if (!$p['isinstance']($pyjs_dbg_40_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_40_err);}throw $pyjs_dbg_40_err;
}})(), 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_41_err){if (!$p['isinstance']($pyjs_dbg_41_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_41_err);}throw $pyjs_dbg_41_err;
}})();
						nodeType = $tupleassign3[0];
						srcKey = $tupleassign3[1];
					} finally { $pyjs['in_try_except'] -= 1; }
				} catch($pyjs_try_err) {
					$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_1 - 1);
					$pyjs['__active_exception_stack__'] = null;
					$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
					var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
					$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='widgets.tree';
					if (true) {
						$pyjs['track']['lineno']=82;
						$pyjs['track']['lineno']=82;
						var $pyjs__ret = null;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
				}
				$pyjs['track']['lineno']=83;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'modifies':true, 'secure':true}, $p['getattr'](self, 'modul'), 'move', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([['skelType', nodeType], ['id', srcKey], ['destNode', $p['getattr'](self, 'data')['__getitem__']('id')]]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_42_err){if (!$p['isinstance']($pyjs_dbg_42_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_42_err);}throw $pyjs_dbg_42_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_43_err){if (!$p['isinstance']($pyjs_dbg_43_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_43_err);}throw $pyjs_dbg_43_err;
}})();
				$pyjs['track']['lineno']=84;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return event['preventDefault']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_44_err){if (!$p['isinstance']($pyjs_dbg_44_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_44_err);}throw $pyjs_dbg_44_err;
}})();
				$pyjs['track']['lineno']=85;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return event['stopPropagation']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_45_err){if (!$p['isinstance']($pyjs_dbg_45_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_45_err);}throw $pyjs_dbg_45_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onDrop'] = $method;
			$pyjs['track']['lineno']=12;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('NodeWidget', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=89;
		$m['LeafWidget'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'widgets.tree';
			$cls_definition['__md5__'] = 'c741a382f0f08f87e9184a089a9b8836';
			$pyjs['track']['lineno']=93;
			$method = $pyjs__bind_method2('__init__', function(modul, data, structure) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

					var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					modul = arguments[1];
					data = arguments[2];
					structure = arguments[3];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,4,arguments['length']-1));

					var kwargs = arguments['length'] >= 5 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'c741a382f0f08f87e9184a089a9b8836') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof structure != 'undefined') {
						if (structure !== null && typeof structure['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = structure;
							structure = arguments[4];
						}
					} else 					if (typeof data != 'undefined') {
						if (data !== null && typeof data['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = data;
							data = arguments[4];
						}
					} else 					if (typeof modul != 'undefined') {
						if (modul !== null && typeof modul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modul;
							modul = arguments[4];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[4];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'widgets.tree', 'lineno':93};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=93;
				$pyjs['track']['lineno']=102;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['LeafWidget'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_46_err){if (!$p['isinstance']($pyjs_dbg_46_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_46_err);}throw $pyjs_dbg_46_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})();
				$pyjs['track']['lineno']=103;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=104;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('data', data) : $p['setattr'](self, 'data', data); 
				$pyjs['track']['lineno']=105;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('structure', structure) : $p['setattr'](self, 'structure', structure); 
				$pyjs['track']['lineno']=106;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['buildDescription']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_48_err){if (!$p['isinstance']($pyjs_dbg_48_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_48_err);}throw $pyjs_dbg_48_err;
}})();
				$pyjs['track']['lineno']=107;
				$p['getattr'](self, 'element')['__is_instance__'] && typeof $p['getattr'](self, 'element')['__setattr__'] == 'function' ? $p['getattr'](self, 'element')['__setattr__']('innerHTML', data['__getitem__']('name')) : $p['setattr']($p['getattr'](self, 'element'), 'innerHTML', data['__getitem__']('name')); 
				$pyjs['track']['lineno']=108;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'treeitem leaf supports_drag');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_49_err){if (!$p['isinstance']($pyjs_dbg_49_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_49_err);}throw $pyjs_dbg_49_err;
}})();
				$pyjs['track']['lineno']=109;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('draggable', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_50_err){if (!$p['isinstance']($pyjs_dbg_50_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_50_err);}throw $pyjs_dbg_50_err;
}})();
				$pyjs['track']['lineno']=110;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['sinkEvent']('onDragStart');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_51_err){if (!$p['isinstance']($pyjs_dbg_51_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_51_err);}throw $pyjs_dbg_51_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['modul'],['data'],['structure']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=112;
			$method = $pyjs__bind_method2('buildDescription', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'c741a382f0f08f87e9184a089a9b8836') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and8,boneInfo,$iter2_nextval,$iter2_type,$iter2_iter,boneName,wdg,$and5,$and6,$and7,$iter2_idx,hasDescr,params,$pyjs__trackstack_size_1,$iter2_array;
				$pyjs['track']={'module':'widgets.tree', 'lineno':112};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=112;
				$pyjs['track']['lineno']=116;
				hasDescr = false;
				$pyjs['track']['lineno']=117;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter2_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'structure');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_53_err){if (!$p['isinstance']($pyjs_dbg_53_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_53_err);}throw $pyjs_dbg_53_err;
}})();
				$iter2_nextval=$p['__iter_prepare']($iter2_iter,false);
				while (typeof($p['__wrapped_next']($iter2_nextval)['$nextval']) != 'undefined') {
					var $tupleassign4 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter2_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_52_err){if (!$p['isinstance']($pyjs_dbg_52_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_52_err);}throw $pyjs_dbg_52_err;
}})();
					boneName = $tupleassign4[0];
					boneInfo = $tupleassign4[1];
					$pyjs['track']['lineno']=118;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and5=(function(){try{try{$pyjs['in_try_except'] += 1;
					return boneInfo['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_54_err){if (!$p['isinstance']($pyjs_dbg_54_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_54_err);}throw $pyjs_dbg_54_err;
}})()['__contains__']('params'))?(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](boneInfo['__getitem__']('params'), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_55_err){if (!$p['isinstance']($pyjs_dbg_55_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_55_err);}throw $pyjs_dbg_55_err;
}})():$and5));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_56_err){if (!$p['isinstance']($pyjs_dbg_56_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_56_err);}throw $pyjs_dbg_56_err;
}})()) {
						$pyjs['track']['lineno']=119;
						params = boneInfo['__getitem__']('params');
						$pyjs['track']['lineno']=120;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](($p['bool']($and7=(function(){try{try{$pyjs['in_try_except'] += 1;
						return params['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_57_err){if (!$p['isinstance']($pyjs_dbg_57_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_57_err);}throw $pyjs_dbg_57_err;
}})()['__contains__']('frontend_default_visible'))?params['__getitem__']('frontend_default_visible'):$and7));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_58_err){if (!$p['isinstance']($pyjs_dbg_58_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_58_err);}throw $pyjs_dbg_58_err;
}})()) {
							$pyjs['track']['lineno']=121;
							wdg = (function(){try{try{$pyjs['in_try_except'] += 1;
							return $m['viewDelegateSelector']['select']($p['getattr'](self, 'modul'), boneName, (function(){try{try{$pyjs['in_try_except'] += 1;
							return $m['utils']['boneListToDict']($p['getattr'](self, 'structure'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_59_err){if (!$p['isinstance']($pyjs_dbg_59_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_59_err);}throw $pyjs_dbg_59_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_60_err){if (!$p['isinstance']($pyjs_dbg_60_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_60_err);}throw $pyjs_dbg_60_err;
}})();
							$pyjs['track']['lineno']=122;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool'](!$p['op_is'](wdg, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_61_err){if (!$p['isinstance']($pyjs_dbg_61_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_61_err);}throw $pyjs_dbg_61_err;
}})()) {
								$pyjs['track']['lineno']=123;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return self['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
								return (function(){try{try{$pyjs['in_try_except'] += 1;
								return wdg($p['getattr'](self, 'modul'), boneName, (function(){try{try{$pyjs['in_try_except'] += 1;
								return $m['utils']['boneListToDict']($p['getattr'](self, 'structure'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_62_err){if (!$p['isinstance']($pyjs_dbg_62_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_62_err);}throw $pyjs_dbg_62_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_63_err){if (!$p['isinstance']($pyjs_dbg_63_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_63_err);}throw $pyjs_dbg_63_err;
}})()['render']($p['getattr'](self, 'data'), boneName);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_65_err){if (!$p['isinstance']($pyjs_dbg_65_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_65_err);}throw $pyjs_dbg_65_err;
}})();
								$pyjs['track']['lineno']=124;
								hasDescr = true;
							}
						}
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=125;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool'](hasDescr));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_66_err){if (!$p['isinstance']($pyjs_dbg_66_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_66_err);}throw $pyjs_dbg_66_err;
}})()) {
					$pyjs['track']['lineno']=126;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['TextNode']($p['getattr'](self, 'data')['__getitem__']('name'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_68_err){if (!$p['isinstance']($pyjs_dbg_68_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_68_err);}throw $pyjs_dbg_68_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['buildDescription'] = $method;
			$pyjs['track']['lineno']=128;
			$method = $pyjs__bind_method2('onDragStart', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'c741a382f0f08f87e9184a089a9b8836') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $add3,$add4;
				$pyjs['track']={'module':'widgets.tree', 'lineno':128};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=128;
				$pyjs['track']['lineno']=132;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return event['dataTransfer']['setData']('Text', $p['__op_add']($add3='leaf/',$add4=$p['getattr'](self, 'data')['__getitem__']('id')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_69_err){if (!$p['isinstance']($pyjs_dbg_69_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_69_err);}throw $pyjs_dbg_69_err;
}})();
				$pyjs['track']['lineno']=133;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return event['stopPropagation']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_70_err){if (!$p['isinstance']($pyjs_dbg_70_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_70_err);}throw $pyjs_dbg_70_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onDragStart'] = $method;
			$pyjs['track']['lineno']=89;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('LeafWidget', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=135;
		$m['SelectableDiv'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'widgets.tree';
			$cls_definition['__md5__'] = 'e511818c9f84b8734de3b77d4df29572';
			$pyjs['track']['lineno']=144;
			$method = $pyjs__bind_method2('__init__', function(nodeWidget, leafWidget, selectionType, multiSelection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,4,arguments['length']-1));

					var kwargs = arguments['length'] >= 5 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					nodeWidget = arguments[1];
					leafWidget = arguments[2];
					selectionType = arguments[3];
					multiSelection = arguments[4];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,5,arguments['length']-1));

					var kwargs = arguments['length'] >= 6 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e511818c9f84b8734de3b77d4df29572') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof multiSelection != 'undefined') {
						if (multiSelection !== null && typeof multiSelection['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = multiSelection;
							multiSelection = arguments[5];
						}
					} else 					if (typeof selectionType != 'undefined') {
						if (selectionType !== null && typeof selectionType['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = selectionType;
							selectionType = arguments[5];
						}
					} else 					if (typeof leafWidget != 'undefined') {
						if (leafWidget !== null && typeof leafWidget['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = leafWidget;
							leafWidget = arguments[5];
						}
					} else 					if (typeof nodeWidget != 'undefined') {
						if (nodeWidget !== null && typeof nodeWidget['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = nodeWidget;
							nodeWidget = arguments[5];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[5];
						}
					} else {
					}
				}
				if (typeof selectionType == 'undefined') selectionType=arguments['callee']['__args__'][5][1];
				if (typeof multiSelection == 'undefined') multiSelection=arguments['callee']['__args__'][6][1];

				$pyjs['track']={'module':'widgets.tree', 'lineno':144};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=144;
				$pyjs['track']['lineno']=145;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectableDiv'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_71_err){if (!$p['isinstance']($pyjs_dbg_71_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_71_err);}throw $pyjs_dbg_71_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_72_err){if (!$p['isinstance']($pyjs_dbg_72_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_72_err);}throw $pyjs_dbg_72_err;
}})();
				$pyjs['track']['lineno']=146;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__getitem__']('class')['append']('selectioncontainer');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_73_err){if (!$p['isinstance']($pyjs_dbg_73_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_73_err);}throw $pyjs_dbg_73_err;
}})();
				$pyjs['track']['lineno']=147;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('tabindex', $constant_int_1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_74_err){if (!$p['isinstance']($pyjs_dbg_74_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_74_err);}throw $pyjs_dbg_74_err;
}})();
				$pyjs['track']['lineno']=148;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectionType', selectionType) : $p['setattr'](self, 'selectionType', selectionType); 
				$pyjs['track']['lineno']=149;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('multiSelection', multiSelection) : $p['setattr'](self, 'multiSelection', multiSelection); 
				$pyjs['track']['lineno']=150;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('nodeWidget', nodeWidget) : $p['setattr'](self, 'nodeWidget', nodeWidget); 
				$pyjs['track']['lineno']=151;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('leafWidget', leafWidget) : $p['setattr'](self, 'leafWidget', leafWidget); 
				$pyjs['track']['lineno']=152;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectionChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('selectionChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})()) : $p['setattr'](self, 'selectionChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('selectionChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})()); 
				$pyjs['track']['lineno']=153;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectionActivatedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('selectionActivated');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})()) : $p['setattr'](self, 'selectionActivatedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('selectionActivated');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})()); 
				$pyjs['track']['lineno']=154;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('cursorMovedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('cursorMoved');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_77_err){if (!$p['isinstance']($pyjs_dbg_77_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_77_err);}throw $pyjs_dbg_77_err;
}})()) : $p['setattr'](self, 'cursorMovedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('cursorMoved');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_77_err){if (!$p['isinstance']($pyjs_dbg_77_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_77_err);}throw $pyjs_dbg_77_err;
}})()); 
				$pyjs['track']['lineno']=155;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_selectedItems', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_78_err){if (!$p['isinstance']($pyjs_dbg_78_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_78_err);}throw $pyjs_dbg_78_err;
}})()) : $p['setattr'](self, '_selectedItems', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_78_err){if (!$p['isinstance']($pyjs_dbg_78_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_78_err);}throw $pyjs_dbg_78_err;
}})()); 
				$pyjs['track']['lineno']=156;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_currentItem', null) : $p['setattr'](self, '_currentItem', null); 
				$pyjs['track']['lineno']=157;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isMouseDown', false) : $p['setattr'](self, '_isMouseDown', false); 
				$pyjs['track']['lineno']=158;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isCtlPressed', false) : $p['setattr'](self, '_isCtlPressed', false); 
				$pyjs['track']['lineno']=159;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_ctlStartRow', null) : $p['setattr'](self, '_ctlStartRow', null); 
				$pyjs['track']['lineno']=160;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['sinkEvent']('onClick', 'onDblClick', 'onMouseMove', 'onMouseDown', 'onMouseUp', 'onKeyDown', 'onKeyUp');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_79_err){if (!$p['isinstance']($pyjs_dbg_79_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_79_err);}throw $pyjs_dbg_79_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['nodeWidget'],['leafWidget'],['selectionType', 'both'],['multiSelection', false]]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=162;
			$method = $pyjs__bind_method2('setCurrentItem', function(item) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					item = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e511818c9f84b8734de3b77d4df29572') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.tree', 'lineno':162};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=162;
				$pyjs['track']['lineno']=167;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, '_currentItem'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_80_err){if (!$p['isinstance']($pyjs_dbg_80_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_80_err);}throw $pyjs_dbg_80_err;
}})()) {
					$pyjs['track']['lineno']=168;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, '_currentItem')['__getitem__']('class')['remove']('cursor');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_81_err){if (!$p['isinstance']($pyjs_dbg_81_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_81_err);}throw $pyjs_dbg_81_err;
}})();
				}
				$pyjs['track']['lineno']=169;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_currentItem', item) : $p['setattr'](self, '_currentItem', item); 
				$pyjs['track']['lineno']=170;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](item);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_82_err){if (!$p['isinstance']($pyjs_dbg_82_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_82_err);}throw $pyjs_dbg_82_err;
}})()) {
					$pyjs['track']['lineno']=171;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return item['__getitem__']('class')['append']('cursor');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_83_err){if (!$p['isinstance']($pyjs_dbg_83_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_83_err);}throw $pyjs_dbg_83_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['item']]);
			$cls_definition['setCurrentItem'] = $method;
			$pyjs['track']['lineno']=173;
			$method = $pyjs__bind_method2('onClick', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e511818c9f84b8734de3b77d4df29572') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var child,$iter3_idx,$iter3_type,$iter3_iter,$iter3_array,$pyjs__trackstack_size_1,$iter3_nextval;
				$pyjs['track']={'module':'widgets.tree', 'lineno':173};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=173;
				$pyjs['track']['lineno']=174;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['focus']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_84_err){if (!$p['isinstance']($pyjs_dbg_84_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_84_err);}throw $pyjs_dbg_84_err;
}})();
				$pyjs['track']['lineno']=175;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter3_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})();
				$iter3_nextval=$p['__iter_prepare']($iter3_iter,false);
				while (typeof($p['__wrapped_next']($iter3_nextval)['$nextval']) != 'undefined') {
					child = $iter3_nextval['$nextval'];
					$pyjs['track']['lineno']=176;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['utils']['doesEventHitWidgetOrChildren'](event, child);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})()) {
						$pyjs['track']['lineno']=177;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['setCurrentItem'](child);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})();
						$pyjs['track']['lineno']=178;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']($p['getattr'](self, '_isCtlPressed'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_89_err){if (!$p['isinstance']($pyjs_dbg_89_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_89_err);}throw $pyjs_dbg_89_err;
}})()) {
							$pyjs['track']['lineno']=179;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return self['addSelectedItem'](child);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})();
						}
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=180;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, '_isCtlPressed')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_91_err){if (!$p['isinstance']($pyjs_dbg_91_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_91_err);}throw $pyjs_dbg_91_err;
}})()) {
					$pyjs['track']['lineno']=181;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['clearSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_92_err){if (!$p['isinstance']($pyjs_dbg_92_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_92_err);}throw $pyjs_dbg_92_err;
}})();
				}
				$pyjs['track']['lineno']=182;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, '_selectedItems'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_93_err){if (!$p['isinstance']($pyjs_dbg_93_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_93_err);}throw $pyjs_dbg_93_err;
}})()) {
					$pyjs['track']['lineno']=183;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['selectionChangedEvent']['fire'](self, $p['__getslice']($p['getattr'](self, '_selectedItems'), 0, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_94_err){if (!$p['isinstance']($pyjs_dbg_94_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_94_err);}throw $pyjs_dbg_94_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, '_currentItem'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_95_err){if (!$p['isinstance']($pyjs_dbg_95_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_95_err);}throw $pyjs_dbg_95_err;
}})()) {
					$pyjs['track']['lineno']=185;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['selectionChangedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([$p['getattr'](self, '_currentItem')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_96_err){if (!$p['isinstance']($pyjs_dbg_96_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_96_err);}throw $pyjs_dbg_96_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_97_err){if (!$p['isinstance']($pyjs_dbg_97_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_97_err);}throw $pyjs_dbg_97_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=187;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['selectionChangedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_98_err){if (!$p['isinstance']($pyjs_dbg_98_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_98_err);}throw $pyjs_dbg_98_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_99_err){if (!$p['isinstance']($pyjs_dbg_99_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_99_err);}throw $pyjs_dbg_99_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=189;
			$method = $pyjs__bind_method2('onDblClick', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e511818c9f84b8734de3b77d4df29572') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and9,$and12,$or3,$or2,$iter4_nextval,$pyjs__trackstack_size_1,$iter4_idx,$and10,$and11,$iter4_type,child,$iter4_array,$or1,$iter4_iter;
				$pyjs['track']={'module':'widgets.tree', 'lineno':189};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=189;
				$pyjs['track']['lineno']=190;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter4_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_100_err){if (!$p['isinstance']($pyjs_dbg_100_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_100_err);}throw $pyjs_dbg_100_err;
}})();
				$iter4_nextval=$p['__iter_prepare']($iter4_iter,false);
				while (typeof($p['__wrapped_next']($iter4_nextval)['$nextval']) != 'undefined') {
					child = $iter4_nextval['$nextval'];
					$pyjs['track']['lineno']=191;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['utils']['doesEventHitWidgetOrChildren'](event, child);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_101_err){if (!$p['isinstance']($pyjs_dbg_101_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_101_err);}throw $pyjs_dbg_101_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_102_err){if (!$p['isinstance']($pyjs_dbg_102_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_102_err);}throw $pyjs_dbg_102_err;
}})()) {
						$pyjs['track']['lineno']=192;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](($p['bool']($or1=($p['bool']($and9=$p['op_eq']($p['getattr'](self, 'selectionType'), 'node'))?(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['isinstance'](child, $p['getattr'](self, 'nodeWidget'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_103_err){if (!$p['isinstance']($pyjs_dbg_103_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_103_err);}throw $pyjs_dbg_103_err;
}})():$and9))?$or1:($p['bool']($or2=($p['bool']($and11=$p['op_eq']($p['getattr'](self, 'selectionType'), 'leaf'))?(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['isinstance'](child, $p['getattr'](self, 'leafWidget'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_104_err){if (!$p['isinstance']($pyjs_dbg_104_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_104_err);}throw $pyjs_dbg_104_err;
}})():$and11))?$or2:$p['op_eq']($p['getattr'](self, 'selectionType'), 'both'))));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_105_err){if (!$p['isinstance']($pyjs_dbg_105_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_105_err);}throw $pyjs_dbg_105_err;
}})()) {
							$pyjs['track']['lineno']=195;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return self['selectionActivatedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['list']([child]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_106_err){if (!$p['isinstance']($pyjs_dbg_106_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_106_err);}throw $pyjs_dbg_106_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_107_err){if (!$p['isinstance']($pyjs_dbg_107_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_107_err);}throw $pyjs_dbg_107_err;
}})();
						}
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.tree';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onDblClick'] = $method;
			$pyjs['track']['lineno']=197;
			$method = $pyjs__bind_method2('activateCurrentSelection', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e511818c9f84b8734de3b77d4df29572') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.tree', 'lineno':197};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=197;
				$pyjs['track']['lineno']=202;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len']($p['getattr'](self, '_selectedItems'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_108_err){if (!$p['isinstance']($pyjs_dbg_108_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_108_err);}throw $pyjs_dbg_108_err;
}})(), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_109_err){if (!$p['isinstance']($pyjs_dbg_109_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_109_err);}throw $pyjs_dbg_109_err;
}})()) {
					$pyjs['track']['lineno']=203;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['selectionActivatedEvent']['fire'](self, $p['getattr'](self, '_selectedItems'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_110_err){if (!$p['isinstance']($pyjs_dbg_110_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_110_err);}throw $pyjs_dbg_110_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['op_is']($p['getattr'](self, '_currentItem'), null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_111_err){if (!$p['isinstance']($pyjs_dbg_111_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_111_err);}throw $pyjs_dbg_111_err;
}})()) {
					$pyjs['track']['lineno']=206;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['selectionActivatedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([$p['getattr'](self, '_currentItem')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_112_err){if (!$p['isinstance']($pyjs_dbg_112_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_112_err);}throw $pyjs_dbg_112_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_113_err){if (!$p['isinstance']($pyjs_dbg_113_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_113_err);}throw $pyjs_dbg_113_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['activateCurrentSelection'] = $method;
			$pyjs['track']['lineno']=208;
			$method = $pyjs__bind_method2('onKeyDown', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e511818c9f84b8734de3b77d4df29572') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.tree', 'lineno':208};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=208;
				$pyjs['track']['lineno']=209;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof isReturn == "undefined"?$m['isReturn']:isReturn)($p['getattr'](event, 'keyCode'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_114_err){if (!$p['isinstance']($pyjs_dbg_114_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_114_err);}throw $pyjs_dbg_114_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_115_err){if (!$p['isinstance']($pyjs_dbg_115_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_115_err);}throw $pyjs_dbg_115_err;
}})()) {
					$pyjs['track']['lineno']=210;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['activateCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_116_err){if (!$p['isinstance']($pyjs_dbg_116_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_116_err);}throw $pyjs_dbg_116_err;
}})();
					$pyjs['track']['lineno']=211;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return event['preventDefault']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_117_err){if (!$p['isinstance']($pyjs_dbg_117_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_117_err);}throw $pyjs_dbg_117_err;
}})();
					$pyjs['track']['lineno']=212;
					$pyjs['track']['lineno']=212;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof isSingleSelectionKey == "undefined"?$m['isSingleSelectionKey']:isSingleSelectionKey)($p['getattr'](event, 'keyCode'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_118_err){if (!$p['isinstance']($pyjs_dbg_118_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_118_err);}throw $pyjs_dbg_118_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_119_err){if (!$p['isinstance']($pyjs_dbg_119_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_119_err);}throw $pyjs_dbg_119_err;
}})()) {
					$pyjs['track']['lineno']=214;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isCtlPressed', true) : $p['setattr'](self, '_isCtlPressed', true); 
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onKeyDown'] = $method;
			$pyjs['track']['lineno']=216;
			$method = $pyjs__bind_method2('onKeyUp', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e511818c9f84b8734de3b77d4df29572') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.tree', 'lineno':216};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=216;
				$pyjs['track']['lineno']=217;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof isSingleSelectionKey == "undefined"?$m['isSingleSelectionKey']:isSingleSelectionKey)($p['getattr'](event, 'keyCode'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_120_err){if (!$p['isinstance']($pyjs_dbg_120_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_120_err);}throw $pyjs_dbg_120_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_121_err){if (!$p['isinstance']($pyjs_dbg_121_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_121_err);}throw $pyjs_dbg_121_err;
}})()) {
					$pyjs['track']['lineno']=218;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isCtlPressed', false) : $p['setattr'](self, '_isCtlPressed', false); 
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onKeyUp'] = $method;
			$pyjs['track']['lineno']=220;
			$method = $pyjs__bind_method2('clearSelection', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e511818c9f84b8734de3b77d4df29572') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter5_nextval,$iter5_idx,child,$iter5_iter,$iter5_array,$iter5_type,$pyjs__trackstack_size_1;
				$pyjs['track']={'module':'widgets.tree', 'lineno':220};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=220;
				$pyjs['track']['lineno']=221;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter5_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__getslice']($p['getattr'](self, '_children'), 0, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_122_err){if (!$p['isinstance']($pyjs_dbg_122_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_122_err);}throw $pyjs_dbg_122_err;
}})();
				$iter5_nextval=$p['__iter_prepare']($iter5_iter,false);
				while (typeof($p['__wrapped_next']($iter5_nextval)['$nextval']) != 'undefined') {
					child = $iter5_nextval['$nextval'];
					$pyjs['track']['lineno']=222;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['removeSelectedItem'](child);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_123_err){if (!$p['isinstance']($pyjs_dbg_123_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_123_err);}throw $pyjs_dbg_123_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.tree';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['clearSelection'] = $method;
			$pyjs['track']['lineno']=224;
			$method = $pyjs__bind_method2('addSelectedItem', function(item) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					item = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e511818c9f84b8734de3b77d4df29572') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $or5,$or4,$or6,$and13,$and16,$and14,$and15;
				$pyjs['track']={'module':'widgets.tree', 'lineno':224};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=224;
				$pyjs['track']['lineno']=227;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($or4=($p['bool']($and13=$p['op_eq']($p['getattr'](self, 'selectionType'), 'node'))?(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](item, $p['getattr'](self, 'nodeWidget'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_124_err){if (!$p['isinstance']($pyjs_dbg_124_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_124_err);}throw $pyjs_dbg_124_err;
}})():$and13))?$or4:($p['bool']($or5=($p['bool']($and15=$p['op_eq']($p['getattr'](self, 'selectionType'), 'leaf'))?(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](item, $p['getattr'](self, 'leafWidget'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_125_err){if (!$p['isinstance']($pyjs_dbg_125_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_125_err);}throw $pyjs_dbg_125_err;
}})():$and15))?$or5:$p['op_eq']($p['getattr'](self, 'selectionType'), 'both'))));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_126_err){if (!$p['isinstance']($pyjs_dbg_126_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_126_err);}throw $pyjs_dbg_126_err;
}})()) {
					$pyjs['track']['lineno']=230;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool']($p['getattr'](self, '_selectedItems')['__contains__'](item)));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_127_err){if (!$p['isinstance']($pyjs_dbg_127_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_127_err);}throw $pyjs_dbg_127_err;
}})()) {
						$pyjs['track']['lineno']=231;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['_selectedItems']['append'](item);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_128_err){if (!$p['isinstance']($pyjs_dbg_128_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_128_err);}throw $pyjs_dbg_128_err;
}})();
						$pyjs['track']['lineno']=232;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return item['__getitem__']('class')['append']('selected');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_129_err){if (!$p['isinstance']($pyjs_dbg_129_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_129_err);}throw $pyjs_dbg_129_err;
}})();
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['item']]);
			$cls_definition['addSelectedItem'] = $method;
			$pyjs['track']['lineno']=234;
			$method = $pyjs__bind_method2('removeSelectedItem', function(item) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					item = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e511818c9f84b8734de3b77d4df29572') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.tree', 'lineno':234};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=234;
				$pyjs['track']['lineno']=235;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, '_selectedItems')['__contains__'](item)));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_130_err){if (!$p['isinstance']($pyjs_dbg_130_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_130_err);}throw $pyjs_dbg_130_err;
}})()) {
					$pyjs['track']['lineno']=236;
					$pyjs['track']['lineno']=236;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=237;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_selectedItems']['remove'](item);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_131_err){if (!$p['isinstance']($pyjs_dbg_131_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_131_err);}throw $pyjs_dbg_131_err;
}})();
				$pyjs['track']['lineno']=238;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return item['__getitem__']('class')['remove']('selected');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_132_err){if (!$p['isinstance']($pyjs_dbg_132_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_132_err);}throw $pyjs_dbg_132_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['item']]);
			$cls_definition['removeSelectedItem'] = $method;
			$pyjs['track']['lineno']=240;
			$method = $pyjs__bind_method2('clear', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e511818c9f84b8734de3b77d4df29572') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter6_idx,$iter6_type,$pyjs__trackstack_size_1,$iter6_array,child,$iter6_iter,$iter6_nextval;
				$pyjs['track']={'module':'widgets.tree', 'lineno':240};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=240;
				$pyjs['track']['lineno']=241;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['clearSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_133_err){if (!$p['isinstance']($pyjs_dbg_133_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_133_err);}throw $pyjs_dbg_133_err;
}})();
				$pyjs['track']['lineno']=242;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter6_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__getslice']($p['getattr'](self, '_children'), 0, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_134_err){if (!$p['isinstance']($pyjs_dbg_134_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_134_err);}throw $pyjs_dbg_134_err;
}})();
				$iter6_nextval=$p['__iter_prepare']($iter6_iter,false);
				while (typeof($p['__wrapped_next']($iter6_nextval)['$nextval']) != 'undefined') {
					child = $iter6_nextval['$nextval'];
					$pyjs['track']['lineno']=243;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['removeChild'](child);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_135_err){if (!$p['isinstance']($pyjs_dbg_135_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_135_err);}throw $pyjs_dbg_135_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=244;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionChangedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_136_err){if (!$p['isinstance']($pyjs_dbg_136_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_136_err);}throw $pyjs_dbg_136_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_137_err){if (!$p['isinstance']($pyjs_dbg_137_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_137_err);}throw $pyjs_dbg_137_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['clear'] = $method;
			$pyjs['track']['lineno']=246;
			$method = $pyjs__bind_method2('getCurrentSelection', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'e511818c9f84b8734de3b77d4df29572') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.tree', 'lineno':246};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=246;
				$pyjs['track']['lineno']=247;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len']($p['getattr'](self, '_selectedItems'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_138_err){if (!$p['isinstance']($pyjs_dbg_138_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_138_err);}throw $pyjs_dbg_138_err;
}})(), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_139_err){if (!$p['isinstance']($pyjs_dbg_139_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_139_err);}throw $pyjs_dbg_139_err;
}})()) {
					$pyjs['track']['lineno']=248;
					$pyjs['track']['lineno']=248;
					var $pyjs__ret = $p['__getslice']($p['getattr'](self, '_selectedItems'), 0, null);
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=249;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, '_currentItem'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_140_err){if (!$p['isinstance']($pyjs_dbg_140_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_140_err);}throw $pyjs_dbg_140_err;
}})()) {
					$pyjs['track']['lineno']=250;
					$pyjs['track']['lineno']=250;
					var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([$p['getattr'](self, '_currentItem')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_141_err){if (!$p['isinstance']($pyjs_dbg_141_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_141_err);}throw $pyjs_dbg_141_err;
}})();
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=251;
				$pyjs['track']['lineno']=251;
				var $pyjs__ret = null;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['getCurrentSelection'] = $method;
			$pyjs['track']['lineno']=135;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('SelectableDiv', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=253;
		$m['TreeWidget'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'widgets.tree';
			$cls_definition['__md5__'] = 'a28a740ecda702dad39d2c810f3ef207';
			$pyjs['track']['lineno']=255;
			$cls_definition['nodeWidget'] = $m['NodeWidget'];
			$pyjs['track']['lineno']=256;
			$cls_definition['leafWidget'] = $m['LeafWidget'];
			$pyjs['track']['lineno']=257;
			$cls_definition['defaultActions'] = (function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['list'](['add.node', 'add.leaf', 'selectrootnode', 'edit', 'delete', 'reload']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_142_err){if (!$p['isinstance']($pyjs_dbg_142_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_142_err);}throw $pyjs_dbg_142_err;
}})();
			$pyjs['track']['lineno']=259;
			$method = $pyjs__bind_method2('__init__', function(modul, rootNode, node, isSelector) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,4,arguments['length']-1));

					var kwargs = arguments['length'] >= 5 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					modul = arguments[1];
					rootNode = arguments[2];
					node = arguments[3];
					isSelector = arguments[4];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,5,arguments['length']-1));

					var kwargs = arguments['length'] >= 6 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a28a740ecda702dad39d2c810f3ef207') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof isSelector != 'undefined') {
						if (isSelector !== null && typeof isSelector['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = isSelector;
							isSelector = arguments[5];
						}
					} else 					if (typeof node != 'undefined') {
						if (node !== null && typeof node['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = node;
							node = arguments[5];
						}
					} else 					if (typeof rootNode != 'undefined') {
						if (rootNode !== null && typeof rootNode['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = rootNode;
							rootNode = arguments[5];
						}
					} else 					if (typeof modul != 'undefined') {
						if (modul !== null && typeof modul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modul;
							modul = arguments[5];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[5];
						}
					} else {
					}
				}
				if (typeof rootNode == 'undefined') rootNode=arguments['callee']['__args__'][4][1];
				if (typeof node == 'undefined') node=arguments['callee']['__args__'][5][1];
				if (typeof isSelector == 'undefined') isSelector=arguments['callee']['__args__'][6][1];
				var $or7,$iter7_array,$iter7_nextval,f,$iter7_iter,$or8,$add5,$iter7_idx,$add6,$iter7_type,$pyjs__trackstack_size_1;
				$pyjs['track']={'module':'widgets.tree', 'lineno':259};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=259;
				$pyjs['track']['lineno']=268;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['TreeWidget'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_143_err){if (!$p['isinstance']($pyjs_dbg_143_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_143_err);}throw $pyjs_dbg_143_err;
}})()['__init__']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_144_err){if (!$p['isinstance']($pyjs_dbg_144_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_144_err);}throw $pyjs_dbg_144_err;
}})();
				$pyjs['track']['lineno']=269;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__getitem__']('class')['append']('tree');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_145_err){if (!$p['isinstance']($pyjs_dbg_145_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_145_err);}throw $pyjs_dbg_145_err;
}})();
				$pyjs['track']['lineno']=270;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=271;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('rootNode', rootNode) : $p['setattr'](self, 'rootNode', rootNode); 
				$pyjs['track']['lineno']=272;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('node', ($p['bool']($or7=node)?$or7:rootNode)) : $p['setattr'](self, 'node', ($p['bool']($or7=node)?$or7:rootNode)); 
				$pyjs['track']['lineno']=273;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('actionBar', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['ActionBar'](modul, 'tree');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_146_err){if (!$p['isinstance']($pyjs_dbg_146_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_146_err);}throw $pyjs_dbg_146_err;
}})()) : $p['setattr'](self, 'actionBar', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['ActionBar'](modul, 'tree');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_146_err){if (!$p['isinstance']($pyjs_dbg_146_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_146_err);}throw $pyjs_dbg_146_err;
}})()); 
				$pyjs['track']['lineno']=274;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'actionBar'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_147_err){if (!$p['isinstance']($pyjs_dbg_147_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_147_err);}throw $pyjs_dbg_147_err;
}})();
				$pyjs['track']['lineno']=275;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('pathList', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_148_err){if (!$p['isinstance']($pyjs_dbg_148_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_148_err);}throw $pyjs_dbg_148_err;
}})()) : $p['setattr'](self, 'pathList', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_148_err){if (!$p['isinstance']($pyjs_dbg_148_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_148_err);}throw $pyjs_dbg_148_err;
}})()); 
				$pyjs['track']['lineno']=276;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'pathList')['__getitem__']('class')['append']('breadcrumb');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_149_err){if (!$p['isinstance']($pyjs_dbg_149_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_149_err);}throw $pyjs_dbg_149_err;
}})();
				$pyjs['track']['lineno']=277;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'pathList'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_150_err){if (!$p['isinstance']($pyjs_dbg_150_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_150_err);}throw $pyjs_dbg_150_err;
}})();
				$pyjs['track']['lineno']=278;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('entryFrame', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['SelectableDiv']($p['getattr'](self, 'nodeWidget'), $p['getattr'](self, 'leafWidget'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_151_err){if (!$p['isinstance']($pyjs_dbg_151_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_151_err);}throw $pyjs_dbg_151_err;
}})()) : $p['setattr'](self, 'entryFrame', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['SelectableDiv']($p['getattr'](self, 'nodeWidget'), $p['getattr'](self, 'leafWidget'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_151_err){if (!$p['isinstance']($pyjs_dbg_151_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_151_err);}throw $pyjs_dbg_151_err;
}})()); 
				$pyjs['track']['lineno']=279;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'entryFrame'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_152_err){if (!$p['isinstance']($pyjs_dbg_152_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_152_err);}throw $pyjs_dbg_152_err;
}})();
				$pyjs['track']['lineno']=280;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['entryFrame']['selectionActivatedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_153_err){if (!$p['isinstance']($pyjs_dbg_153_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_153_err);}throw $pyjs_dbg_153_err;
}})();
				$pyjs['track']['lineno']=281;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_batchSize', $constant_int_99) : $p['setattr'](self, '_batchSize', $constant_int_99); 
				$pyjs['track']['lineno']=282;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_currentCursor', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([['node', null], ['leaf', null]]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_154_err){if (!$p['isinstance']($pyjs_dbg_154_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_154_err);}throw $pyjs_dbg_154_err;
}})()) : $p['setattr'](self, '_currentCursor', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([['node', null], ['leaf', null]]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_154_err){if (!$p['isinstance']($pyjs_dbg_154_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_154_err);}throw $pyjs_dbg_154_err;
}})()); 
				$pyjs['track']['lineno']=283;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_currentRequests', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_155_err){if (!$p['isinstance']($pyjs_dbg_155_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_155_err);}throw $pyjs_dbg_155_err;
}})()) : $p['setattr'](self, '_currentRequests', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_155_err){if (!$p['isinstance']($pyjs_dbg_155_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_155_err);}throw $pyjs_dbg_155_err;
}})()); 
				$pyjs['track']['lineno']=284;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('rootNodeChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('rootNodeChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_156_err){if (!$p['isinstance']($pyjs_dbg_156_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_156_err);}throw $pyjs_dbg_156_err;
}})()) : $p['setattr'](self, 'rootNodeChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('rootNodeChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_156_err){if (!$p['isinstance']($pyjs_dbg_156_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_156_err);}throw $pyjs_dbg_156_err;
}})()); 
				$pyjs['track']['lineno']=285;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('nodeChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('nodeChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_157_err){if (!$p['isinstance']($pyjs_dbg_157_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_157_err);}throw $pyjs_dbg_157_err;
}})()) : $p['setattr'](self, 'nodeChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('nodeChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_157_err){if (!$p['isinstance']($pyjs_dbg_157_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_157_err);}throw $pyjs_dbg_157_err;
}})()); 
				$pyjs['track']['lineno']=286;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isSelector', isSelector) : $p['setattr'](self, 'isSelector', isSelector); 
				$pyjs['track']['lineno']=287;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'rootNode'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_158_err){if (!$p['isinstance']($pyjs_dbg_158_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_158_err);}throw $pyjs_dbg_158_err;
}})()) {
					$pyjs['track']['lineno']=288;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['reloadData']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_159_err){if (!$p['isinstance']($pyjs_dbg_159_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_159_err);}throw $pyjs_dbg_159_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=290;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'successHandler':$p['getattr'](self, 'onSetDefaultRootNode')}, $p['getattr'](self, 'modul'), 'listRootNodes']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_160_err){if (!$p['isinstance']($pyjs_dbg_160_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_160_err);}throw $pyjs_dbg_160_err;
}})();
				}
				$pyjs['track']['lineno']=291;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('path', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_161_err){if (!$p['isinstance']($pyjs_dbg_161_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_161_err);}throw $pyjs_dbg_161_err;
}})()) : $p['setattr'](self, 'path', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_161_err){if (!$p['isinstance']($pyjs_dbg_161_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_161_err);}throw $pyjs_dbg_161_err;
}})()); 
				$pyjs['track']['lineno']=292;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['sinkEvent']('onClick');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_162_err){if (!$p['isinstance']($pyjs_dbg_162_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_162_err);}throw $pyjs_dbg_162_err;
}})();
				$pyjs['track']['lineno']=294;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter7_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list'](['selectionChangedEvent', 'selectionActivatedEvent', 'cursorMovedEvent', 'getCurrentSelection']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_163_err){if (!$p['isinstance']($pyjs_dbg_163_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_163_err);}throw $pyjs_dbg_163_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_164_err){if (!$p['isinstance']($pyjs_dbg_164_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_164_err);}throw $pyjs_dbg_164_err;
}})();
				$iter7_nextval=$p['__iter_prepare']($iter7_iter,false);
				while (typeof($p['__wrapped_next']($iter7_nextval)['$nextval']) != 'undefined') {
					f = $iter7_nextval['$nextval'];
					$pyjs['track']['lineno']=295;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['setattr'](self, f, (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr']($p['getattr'](self, 'entryFrame'), f);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_165_err){if (!$p['isinstance']($pyjs_dbg_165_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_165_err);}throw $pyjs_dbg_165_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_166_err){if (!$p['isinstance']($pyjs_dbg_166_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_166_err);}throw $pyjs_dbg_166_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=296;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['actionBar']['setActions']($p['__op_add']($add5=$p['getattr'](self, 'defaultActions'),$add6=($p['bool'](isSelector)? ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list'](['select', 'close']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_167_err){if (!$p['isinstance']($pyjs_dbg_167_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_167_err);}throw $pyjs_dbg_167_err;
}})()) : ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_168_err){if (!$p['isinstance']($pyjs_dbg_168_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_168_err);}throw $pyjs_dbg_168_err;
}})()))));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_169_err){if (!$p['isinstance']($pyjs_dbg_169_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_169_err);}throw $pyjs_dbg_169_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['modul'],['rootNode', null],['node', null],['isSelector', false]]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=298;
			$method = $pyjs__bind_method2('showErrorMsg', function(req, code) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					req = arguments[1];
					code = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 3)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a28a740ecda702dad39d2c810f3ef207') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof req == 'undefined') req=arguments['callee']['__args__'][3][1];
				if (typeof code == 'undefined') code=arguments['callee']['__args__'][4][1];
				var $or9,errorDiv,$or11,$and17,$or12,$and18,txt,$or10;
				$pyjs['track']={'module':'widgets.tree', 'lineno':298};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=298;
				$pyjs['track']['lineno']=302;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'actionBar')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_170_err){if (!$p['isinstance']($pyjs_dbg_170_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_170_err);}throw $pyjs_dbg_170_err;
}})();
				$pyjs['track']['lineno']=303;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'entryFrame')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_171_err){if (!$p['isinstance']($pyjs_dbg_171_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_171_err);}throw $pyjs_dbg_171_err;
}})();
				$pyjs['track']['lineno']=304;
				errorDiv = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_172_err){if (!$p['isinstance']($pyjs_dbg_172_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_172_err);}throw $pyjs_dbg_172_err;
}})();
				$pyjs['track']['lineno']=305;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return errorDiv['__getitem__']('class')['append']('error_msg');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_173_err){if (!$p['isinstance']($pyjs_dbg_173_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_173_err);}throw $pyjs_dbg_173_err;
}})();
				$pyjs['track']['lineno']=306;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and17=code)?($p['bool']($or9=$p['op_eq'](code, $constant_int_401))?$or9:$p['op_eq'](code, $constant_int_403)):$and17));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_174_err){if (!$p['isinstance']($pyjs_dbg_174_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_174_err);}throw $pyjs_dbg_174_err;
}})()) {
					$pyjs['track']['lineno']=307;
					txt = 'Access denied!';
				}
				else {
					$pyjs['track']['lineno']=309;
					txt = 'An unknown error occurred!';
				}
				$pyjs['track']['lineno']=310;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return errorDiv['__getitem__']('class')['append']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('error_code_%s', ($p['bool']($or11=code)?$or11:$constant_int_0));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_175_err){if (!$p['isinstance']($pyjs_dbg_175_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_175_err);}throw $pyjs_dbg_175_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_176_err){if (!$p['isinstance']($pyjs_dbg_176_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_176_err);}throw $pyjs_dbg_176_err;
}})();
				$pyjs['track']['lineno']=311;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return errorDiv['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['TextNode'](txt);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_177_err){if (!$p['isinstance']($pyjs_dbg_177_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_177_err);}throw $pyjs_dbg_177_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_178_err){if (!$p['isinstance']($pyjs_dbg_178_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_178_err);}throw $pyjs_dbg_178_err;
}})();
				$pyjs['track']['lineno']=312;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](errorDiv);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_179_err){if (!$p['isinstance']($pyjs_dbg_179_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_179_err);}throw $pyjs_dbg_179_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['req', null],['code', null]]);
			$cls_definition['showErrorMsg'] = $method;
			$pyjs['track']['lineno']=314;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a28a740ecda702dad39d2c810f3ef207') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.tree', 'lineno':314};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=314;
				$pyjs['track']['lineno']=315;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['TreeWidget'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_180_err){if (!$p['isinstance']($pyjs_dbg_180_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_180_err);}throw $pyjs_dbg_180_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_181_err){if (!$p['isinstance']($pyjs_dbg_181_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_181_err);}throw $pyjs_dbg_181_err;
}})();
				$pyjs['track']['lineno']=316;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['registerChangeListener'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_182_err){if (!$p['isinstance']($pyjs_dbg_182_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_182_err);}throw $pyjs_dbg_182_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=318;
			$method = $pyjs__bind_method2('onDetach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a28a740ecda702dad39d2c810f3ef207') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.tree', 'lineno':318};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=318;
				$pyjs['track']['lineno']=319;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['TreeWidget'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_183_err){if (!$p['isinstance']($pyjs_dbg_183_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_183_err);}throw $pyjs_dbg_183_err;
}})()['onDetach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_184_err){if (!$p['isinstance']($pyjs_dbg_184_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_184_err);}throw $pyjs_dbg_184_err;
}})();
				$pyjs['track']['lineno']=320;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['removeChangeListener'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_185_err){if (!$p['isinstance']($pyjs_dbg_185_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_185_err);}throw $pyjs_dbg_185_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onDetach'] = $method;
			$pyjs['track']['lineno']=323;
			$method = $pyjs__bind_method2('onDataChanged', function(modul) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					modul = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a28a740ecda702dad39d2c810f3ef207') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and20,$iter8_idx,$and21,$pyjs__trackstack_size_1,$iter8_array,isRootNode,$iter8_iter,$iter8_nextval,v,k,$and19,$iter8_type;
				$pyjs['track']={'module':'widgets.tree', 'lineno':323};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=323;
				$pyjs['track']['lineno']=324;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['op_eq'](modul, $p['getattr'](self, 'modul')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_186_err){if (!$p['isinstance']($pyjs_dbg_186_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_186_err);}throw $pyjs_dbg_186_err;
}})()) {
					$pyjs['track']['lineno']=325;
					isRootNode = false;
					$pyjs['track']['lineno']=326;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter8_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['conf']['__getitem__']('modules')['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_188_err){if (!$p['isinstance']($pyjs_dbg_188_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_188_err);}throw $pyjs_dbg_188_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_189_err){if (!$p['isinstance']($pyjs_dbg_189_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_189_err);}throw $pyjs_dbg_189_err;
}})();
					$iter8_nextval=$p['__iter_prepare']($iter8_iter,false);
					while (typeof($p['__wrapped_next']($iter8_nextval)['$nextval']) != 'undefined') {
						var $tupleassign5 = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['__ass_unpack']($iter8_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_187_err){if (!$p['isinstance']($pyjs_dbg_187_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_187_err);}throw $pyjs_dbg_187_err;
}})();
						k = $tupleassign5[0];
						v = $tupleassign5[1];
						$pyjs['track']['lineno']=327;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](($p['bool']($and19=$p['op_eq'](k, modul))?($p['bool']($and20=$p['op_eq']((function(){try{try{$pyjs['in_try_except'] += 1;
						return v['get']('handler');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_190_err){if (!$p['isinstance']($pyjs_dbg_190_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_190_err);}throw $pyjs_dbg_190_err;
}})(), 'list'))?$p['op_eq']((function(){try{try{$pyjs['in_try_except'] += 1;
						return v['get']('rootNodeOf');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_191_err){if (!$p['isinstance']($pyjs_dbg_191_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_191_err);}throw $pyjs_dbg_191_err;
}})(), $p['getattr'](self, 'modul')):$and20):$and19));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_192_err){if (!$p['isinstance']($pyjs_dbg_192_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_192_err);}throw $pyjs_dbg_192_err;
}})()) {
							$pyjs['track']['lineno']=328;
							isRootNode = true;
							$pyjs['track']['lineno']=329;
							break;
						}
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='widgets.tree';
					$pyjs['track']['lineno']=331;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool'](isRootNode));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_193_err){if (!$p['isinstance']($pyjs_dbg_193_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_193_err);}throw $pyjs_dbg_193_err;
}})()) {
						$pyjs['track']['lineno']=332;
						$pyjs['track']['lineno']=332;
						var $pyjs__ret = null;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
				}
				$pyjs['track']['lineno']=334;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr']($p['getattr'](self, 'actionBar'), 'widgets')['__getitem__']('selectrootnode')['update']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_194_err){if (!$p['isinstance']($pyjs_dbg_194_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_194_err);}throw $pyjs_dbg_194_err;
}})();
				$pyjs['track']['lineno']=335;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['reloadData']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_195_err){if (!$p['isinstance']($pyjs_dbg_195_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_195_err);}throw $pyjs_dbg_195_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['modul']]);
			$cls_definition['onDataChanged'] = $method;
			$pyjs['track']['lineno']=337;
			$method = $pyjs__bind_method2('onSelectionActivated', function(div, selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					div = arguments[1];
					selection = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a28a740ecda702dad39d2c810f3ef207') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var item;
				$pyjs['track']={'module':'widgets.tree', 'lineno':337};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=337;
				$pyjs['track']['lineno']=338;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['op_eq']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](selection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_196_err){if (!$p['isinstance']($pyjs_dbg_196_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_196_err);}throw $pyjs_dbg_196_err;
}})(), $constant_int_1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_197_err){if (!$p['isinstance']($pyjs_dbg_197_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_197_err);}throw $pyjs_dbg_197_err;
}})()) {
					$pyjs['track']['lineno']=339;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'isSelector'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_198_err){if (!$p['isinstance']($pyjs_dbg_198_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_198_err);}throw $pyjs_dbg_198_err;
}})()) {
						$pyjs['track']['lineno']=340;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['conf']['__getitem__']('mainWindow')['removeWidget'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_199_err){if (!$p['isinstance']($pyjs_dbg_199_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_199_err);}throw $pyjs_dbg_199_err;
}})();
					}
					$pyjs['track']['lineno']=341;
					$pyjs['track']['lineno']=341;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=342;
				item = selection['__getitem__']($constant_int_0);
				$pyjs['track']['lineno']=343;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](item, $p['getattr'](self, 'nodeWidget'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_200_err){if (!$p['isinstance']($pyjs_dbg_200_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_200_err);}throw $pyjs_dbg_200_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_201_err){if (!$p['isinstance']($pyjs_dbg_201_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_201_err);}throw $pyjs_dbg_201_err;
}})()) {
					$pyjs['track']['lineno']=344;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['path']['append']($p['getattr'](item, 'data'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_202_err){if (!$p['isinstance']($pyjs_dbg_202_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_202_err);}throw $pyjs_dbg_202_err;
}})();
					$pyjs['track']['lineno']=345;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['rebuildPath']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_203_err){if (!$p['isinstance']($pyjs_dbg_203_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_203_err);}throw $pyjs_dbg_203_err;
}})();
					$pyjs['track']['lineno']=346;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['setNode']($p['getattr'](item, 'data')['__getitem__']('id'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_204_err){if (!$p['isinstance']($pyjs_dbg_204_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_204_err);}throw $pyjs_dbg_204_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](item, $p['getattr'](self, 'leafWidget'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_205_err){if (!$p['isinstance']($pyjs_dbg_205_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_205_err);}throw $pyjs_dbg_205_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_206_err){if (!$p['isinstance']($pyjs_dbg_206_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_206_err);}throw $pyjs_dbg_206_err;
}})()) {
					$pyjs['track']['lineno']=348;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'isSelector'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_207_err){if (!$p['isinstance']($pyjs_dbg_207_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_207_err);}throw $pyjs_dbg_207_err;
}})()) {
						$pyjs['track']['lineno']=349;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['conf']['__getitem__']('mainWindow')['removeWidget'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_208_err){if (!$p['isinstance']($pyjs_dbg_208_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_208_err);}throw $pyjs_dbg_208_err;
}})();
						$pyjs['track']['lineno']=350;
						$pyjs['track']['lineno']=350;
						var $pyjs__ret = null;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['div'],['selection']]);
			$cls_definition['onSelectionActivated'] = $method;
			$pyjs['track']['lineno']=352;
			$method = $pyjs__bind_method2('activateCurrentSelection', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a28a740ecda702dad39d2c810f3ef207') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.tree', 'lineno':352};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=352;
				$pyjs['track']['lineno']=353;
				$pyjs['track']['lineno']=353;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['entryFrame']['activateCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_209_err){if (!$p['isinstance']($pyjs_dbg_209_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_209_err);}throw $pyjs_dbg_209_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['activateCurrentSelection'] = $method;
			$pyjs['track']['lineno']=355;
			$method = $pyjs__bind_method2('onClick', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a28a740ecda702dad39d2c810f3ef207') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var c,$iter9_iter,$iter9_nextval,$iter9_idx,$iter9_array,$pyjs__trackstack_size_1,$iter9_type;
				$pyjs['track']={'module':'widgets.tree', 'lineno':355};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=355;
				$pyjs['track']['lineno']=356;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['TreeWidget'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_210_err){if (!$p['isinstance']($pyjs_dbg_210_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_210_err);}throw $pyjs_dbg_210_err;
}})()['onClick'](event);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_211_err){if (!$p['isinstance']($pyjs_dbg_211_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_211_err);}throw $pyjs_dbg_211_err;
}})();
				$pyjs['track']['lineno']=357;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter9_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr']($p['getattr'](self, 'pathList'), '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_212_err){if (!$p['isinstance']($pyjs_dbg_212_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_212_err);}throw $pyjs_dbg_212_err;
}})();
				$iter9_nextval=$p['__iter_prepare']($iter9_iter,false);
				while (typeof($p['__wrapped_next']($iter9_nextval)['$nextval']) != 'undefined') {
					c = $iter9_nextval['$nextval'];
					$pyjs['track']['lineno']=359;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['utils']['doesEventHitWidgetOrParents'](event, c);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_213_err){if (!$p['isinstance']($pyjs_dbg_213_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_213_err);}throw $pyjs_dbg_213_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_214_err){if (!$p['isinstance']($pyjs_dbg_214_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_214_err);}throw $pyjs_dbg_214_err;
}})()) {
						$pyjs['track']['lineno']=360;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('path', $p['__getslice']($p['getattr'](self, 'path'), 0, (function(){try{try{$pyjs['in_try_except'] += 1;
						return self['pathList']['_children']['index'](c);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_215_err){if (!$p['isinstance']($pyjs_dbg_215_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_215_err);}throw $pyjs_dbg_215_err;
}})())) : $p['setattr'](self, 'path', $p['__getslice']($p['getattr'](self, 'path'), 0, (function(){try{try{$pyjs['in_try_except'] += 1;
						return self['pathList']['_children']['index'](c);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_215_err){if (!$p['isinstance']($pyjs_dbg_215_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_215_err);}throw $pyjs_dbg_215_err;
}})())); 
						$pyjs['track']['lineno']=361;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['rebuildPath']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_216_err){if (!$p['isinstance']($pyjs_dbg_216_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_216_err);}throw $pyjs_dbg_216_err;
}})();
						$pyjs['track']['lineno']=362;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['setNode']($p['getattr'](c, 'data')['__getitem__']('id'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_217_err){if (!$p['isinstance']($pyjs_dbg_217_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_217_err);}throw $pyjs_dbg_217_err;
}})();
						$pyjs['track']['lineno']=363;
						$pyjs['track']['lineno']=363;
						var $pyjs__ret = null;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.tree';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=365;
			$method = $pyjs__bind_method2('onSetDefaultRootNode', function(req) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					req = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a28a740ecda702dad39d2c810f3ef207') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var data;
				$pyjs['track']={'module':'widgets.tree', 'lineno':365};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=365;
				$pyjs['track']['lineno']=366;
				data = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['decode'](req);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_218_err){if (!$p['isinstance']($pyjs_dbg_218_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_218_err);}throw $pyjs_dbg_218_err;
}})();
				$pyjs['track']['lineno']=367;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_219_err){if (!$p['isinstance']($pyjs_dbg_219_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_219_err);}throw $pyjs_dbg_219_err;
}})(), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_220_err){if (!$p['isinstance']($pyjs_dbg_220_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_220_err);}throw $pyjs_dbg_220_err;
}})()) {
					$pyjs['track']['lineno']=368;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['setRootNode'](data['__getitem__']($constant_int_0)['__getitem__']('key'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_221_err){if (!$p['isinstance']($pyjs_dbg_221_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_221_err);}throw $pyjs_dbg_221_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['req']]);
			$cls_definition['onSetDefaultRootNode'] = $method;
			$pyjs['track']['lineno']=370;
			$method = $pyjs__bind_method2('setRootNode', function(rootNode) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					rootNode = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a28a740ecda702dad39d2c810f3ef207') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.tree', 'lineno':370};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=370;
				$pyjs['track']['lineno']=371;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('rootNode', rootNode) : $p['setattr'](self, 'rootNode', rootNode); 
				$pyjs['track']['lineno']=372;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('node', rootNode) : $p['setattr'](self, 'node', rootNode); 
				$pyjs['track']['lineno']=373;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['rootNodeChangedEvent']['fire'](rootNode);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_222_err){if (!$p['isinstance']($pyjs_dbg_222_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_222_err);}throw $pyjs_dbg_222_err;
}})();
				$pyjs['track']['lineno']=374;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['rebuildPath']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_223_err){if (!$p['isinstance']($pyjs_dbg_223_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_223_err);}throw $pyjs_dbg_223_err;
}})();
				$pyjs['track']['lineno']=375;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['reloadData']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_224_err){if (!$p['isinstance']($pyjs_dbg_224_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_224_err);}throw $pyjs_dbg_224_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['rootNode']]);
			$cls_definition['setRootNode'] = $method;
			$pyjs['track']['lineno']=377;
			$method = $pyjs__bind_method2('setNode', function(node) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					node = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a28a740ecda702dad39d2c810f3ef207') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.tree', 'lineno':377};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=377;
				$pyjs['track']['lineno']=378;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('node', node) : $p['setattr'](self, 'node', node); 
				$pyjs['track']['lineno']=379;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['nodeChangedEvent']['fire'](node);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_225_err){if (!$p['isinstance']($pyjs_dbg_225_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_225_err);}throw $pyjs_dbg_225_err;
}})();
				$pyjs['track']['lineno']=380;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['reloadData']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_226_err){if (!$p['isinstance']($pyjs_dbg_226_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_226_err);}throw $pyjs_dbg_226_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['node']]);
			$cls_definition['setNode'] = $method;
			$pyjs['track']['lineno']=382;
			$method = $pyjs__bind_method2('rebuildPath', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a28a740ecda702dad39d2c810f3ef207') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter11_nextval,c,$iter10_nextval,$iter11_iter,$iter10_idx,$iter11_type,$add8,$iter10_array,$iter11_array,p,$add7,$pyjs__trackstack_size_1,$iter10_type,$iter10_iter,$iter11_idx;
				$pyjs['track']={'module':'widgets.tree', 'lineno':382};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=382;
				$pyjs['track']['lineno']=386;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter10_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__getslice']($p['getattr']($p['getattr'](self, 'pathList'), '_children'), 0, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_227_err){if (!$p['isinstance']($pyjs_dbg_227_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_227_err);}throw $pyjs_dbg_227_err;
}})();
				$iter10_nextval=$p['__iter_prepare']($iter10_iter,false);
				while (typeof($p['__wrapped_next']($iter10_nextval)['$nextval']) != 'undefined') {
					c = $iter10_nextval['$nextval'];
					$pyjs['track']['lineno']=387;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['pathList']['removeChild'](c);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_228_err){if (!$p['isinstance']($pyjs_dbg_228_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_228_err);}throw $pyjs_dbg_228_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=388;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter11_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__op_add']($add7=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([null]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_229_err){if (!$p['isinstance']($pyjs_dbg_229_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_229_err);}throw $pyjs_dbg_229_err;
}})(),$add8=$p['getattr'](self, 'path'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_230_err){if (!$p['isinstance']($pyjs_dbg_230_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_230_err);}throw $pyjs_dbg_230_err;
}})();
				$iter11_nextval=$p['__iter_prepare']($iter11_iter,false);
				while (typeof($p['__wrapped_next']($iter11_nextval)['$nextval']) != 'undefined') {
					p = $iter11_nextval['$nextval'];
					$pyjs['track']['lineno']=389;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_is'](p, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_231_err){if (!$p['isinstance']($pyjs_dbg_231_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_231_err);}throw $pyjs_dbg_231_err;
}})()) {
						$pyjs['track']['lineno']=390;
						c = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['NodeWidget']($p['getattr'](self, 'modul'), (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['dict']([['id', $p['getattr'](self, 'rootNode')], ['name', 'root']]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_232_err){if (!$p['isinstance']($pyjs_dbg_232_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_232_err);}throw $pyjs_dbg_232_err;
}})(), (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_233_err){if (!$p['isinstance']($pyjs_dbg_233_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_233_err);}throw $pyjs_dbg_233_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_234_err){if (!$p['isinstance']($pyjs_dbg_234_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_234_err);}throw $pyjs_dbg_234_err;
}})();
						$pyjs['track']['lineno']=391;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return c['__getitem__']('class')['append']('is_rootnode');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_235_err){if (!$p['isinstance']($pyjs_dbg_235_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_235_err);}throw $pyjs_dbg_235_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=393;
						c = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['NodeWidget']($p['getattr'](self, 'modul'), p, (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_236_err){if (!$p['isinstance']($pyjs_dbg_236_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_236_err);}throw $pyjs_dbg_236_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_237_err){if (!$p['isinstance']($pyjs_dbg_237_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_237_err);}throw $pyjs_dbg_237_err;
}})();
					}
					$pyjs['track']['lineno']=394;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['pathList']['appendChild'](c);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_238_err){if (!$p['isinstance']($pyjs_dbg_238_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_238_err);}throw $pyjs_dbg_238_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.tree';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['rebuildPath'] = $method;
			$pyjs['track']['lineno']=398;
			$method = $pyjs__bind_method2('reloadData', function(paramsOverride) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					paramsOverride = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a28a740ecda702dad39d2c810f3ef207') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof paramsOverride == 'undefined') paramsOverride=arguments['callee']['__args__'][3][1];
				var params,r;
				$pyjs['track']={'module':'widgets.tree', 'lineno':398};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=398;
				$pyjs['track']['lineno']=399;
				if (!( !$p['op_is']($p['getattr'](self, 'node'), null) )) {
				   throw $p['AssertionError']('reloadData called while self.node is None');
				 }
				$pyjs['track']['lineno']=400;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['entryFrame']['clear']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_239_err){if (!$p['isinstance']($pyjs_dbg_239_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_239_err);}throw $pyjs_dbg_239_err;
}})();
				$pyjs['track']['lineno']=401;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_currentRequests', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_240_err){if (!$p['isinstance']($pyjs_dbg_240_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_240_err);}throw $pyjs_dbg_240_err;
}})()) : $p['setattr'](self, '_currentRequests', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_240_err){if (!$p['isinstance']($pyjs_dbg_240_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_240_err);}throw $pyjs_dbg_240_err;
}})()); 
				$pyjs['track']['lineno']=402;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](paramsOverride);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_241_err){if (!$p['isinstance']($pyjs_dbg_241_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_241_err);}throw $pyjs_dbg_241_err;
}})()) {
					$pyjs['track']['lineno']=403;
					params = (function(){try{try{$pyjs['in_try_except'] += 1;
					return paramsOverride['copy']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_242_err){if (!$p['isinstance']($pyjs_dbg_242_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_242_err);}throw $pyjs_dbg_242_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=405;
					params = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([['node', $p['getattr'](self, 'node')]]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_243_err){if (!$p['isinstance']($pyjs_dbg_243_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_243_err);}throw $pyjs_dbg_243_err;
}})();
				}
				$pyjs['track']['lineno']=407;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!params['__contains__']('amount'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_244_err){if (!$p['isinstance']($pyjs_dbg_244_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_244_err);}throw $pyjs_dbg_244_err;
}})()) {
					$pyjs['track']['lineno']=408;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return params['__setitem__']('amount', $p['getattr'](self, '_batchSize'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_245_err){if (!$p['isinstance']($pyjs_dbg_245_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_245_err);}throw $pyjs_dbg_245_err;
}})();
				}
				$pyjs['track']['lineno']=410;
				r = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'successHandler':$p['getattr'](self, 'onRequestSucceded'), 'failureHandler':$p['getattr'](self, 'showErrorMsg')}, $p['getattr'](self, 'modul'), 'list/node', params]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_246_err){if (!$p['isinstance']($pyjs_dbg_246_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_246_err);}throw $pyjs_dbg_246_err;
}})();
				$pyjs['track']['lineno']=413;
				r['__is_instance__'] && typeof r['__setattr__'] == 'function' ? r['__setattr__']('reqType', 'node') : $p['setattr'](r, 'reqType', 'node'); 
				$pyjs['track']['lineno']=414;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_currentRequests']['append'](r);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_247_err){if (!$p['isinstance']($pyjs_dbg_247_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_247_err);}throw $pyjs_dbg_247_err;
}})();
				$pyjs['track']['lineno']=415;
				r = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'successHandler':$p['getattr'](self, 'onRequestSucceded'), 'failureHandler':$p['getattr'](self, 'showErrorMsg')}, $p['getattr'](self, 'modul'), 'list/leaf', params]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_248_err){if (!$p['isinstance']($pyjs_dbg_248_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_248_err);}throw $pyjs_dbg_248_err;
}})();
				$pyjs['track']['lineno']=418;
				r['__is_instance__'] && typeof r['__setattr__'] == 'function' ? r['__setattr__']('reqType', 'leaf') : $p['setattr'](r, 'reqType', 'leaf'); 
				$pyjs['track']['lineno']=419;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_currentRequests']['append'](r);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_249_err){if (!$p['isinstance']($pyjs_dbg_249_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_249_err);}throw $pyjs_dbg_249_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['paramsOverride', null]]);
			$cls_definition['reloadData'] = $method;
			$pyjs['track']['lineno']=421;
			$method = $pyjs__bind_method2('onRequestSucceded', function(req) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					req = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a28a740ecda702dad39d2c810f3ef207') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter12_idx,$iter12_type,$iter12_array,$iter12_nextval,$iter12_iter,skel,r,$pyjs__trackstack_size_1,$and23,$and22,n,data;
				$pyjs['track']={'module':'widgets.tree', 'lineno':421};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=421;
				$pyjs['track']['lineno']=422;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, '_currentRequests')['__contains__'](req)));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_250_err){if (!$p['isinstance']($pyjs_dbg_250_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_250_err);}throw $pyjs_dbg_250_err;
}})()) {
					$pyjs['track']['lineno']=423;
					$pyjs['track']['lineno']=423;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=424;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_currentRequests']['remove'](req);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_251_err){if (!$p['isinstance']($pyjs_dbg_251_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_251_err);}throw $pyjs_dbg_251_err;
}})();
				$pyjs['track']['lineno']=425;
				data = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['decode'](req);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_252_err){if (!$p['isinstance']($pyjs_dbg_252_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_252_err);}throw $pyjs_dbg_252_err;
}})();
				$pyjs['track']['lineno']=426;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter12_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return data['__getitem__']('skellist');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_253_err){if (!$p['isinstance']($pyjs_dbg_253_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_253_err);}throw $pyjs_dbg_253_err;
}})();
				$iter12_nextval=$p['__iter_prepare']($iter12_iter,false);
				while (typeof($p['__wrapped_next']($iter12_nextval)['$nextval']) != 'undefined') {
					skel = $iter12_nextval['$nextval'];
					$pyjs['track']['lineno']=427;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_eq']($p['getattr'](req, 'reqType'), 'node'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_254_err){if (!$p['isinstance']($pyjs_dbg_254_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_254_err);}throw $pyjs_dbg_254_err;
}})()) {
						$pyjs['track']['lineno']=428;
						n = (function(){try{try{$pyjs['in_try_except'] += 1;
						return self['nodeWidget']($p['getattr'](self, 'modul'), skel, data['__getitem__']('structure'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_255_err){if (!$p['isinstance']($pyjs_dbg_255_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_255_err);}throw $pyjs_dbg_255_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=430;
						n = (function(){try{try{$pyjs['in_try_except'] += 1;
						return self['leafWidget']($p['getattr'](self, 'modul'), skel, data['__getitem__']('structure'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_256_err){if (!$p['isinstance']($pyjs_dbg_256_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_256_err);}throw $pyjs_dbg_256_err;
}})();
					}
					$pyjs['track']['lineno']=432;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['entryFrame']['appendChild'](n);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_257_err){if (!$p['isinstance']($pyjs_dbg_257_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_257_err);}throw $pyjs_dbg_257_err;
}})();
					$pyjs['track']['lineno']=433;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['entryFrame']['sortChildren']($p['getattr'](self, 'getChildKey'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_258_err){if (!$p['isinstance']($pyjs_dbg_258_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_258_err);}throw $pyjs_dbg_258_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=435;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and22=(function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_259_err){if (!$p['isinstance']($pyjs_dbg_259_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_259_err);}throw $pyjs_dbg_259_err;
}})()['__contains__']('cursor'))?$p['op_eq']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](data['__getitem__']('skellist'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_260_err){if (!$p['isinstance']($pyjs_dbg_260_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_260_err);}throw $pyjs_dbg_260_err;
}})(), $p['getattr'](req, 'params')['__getitem__']('amount')):$and22));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_261_err){if (!$p['isinstance']($pyjs_dbg_261_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_261_err);}throw $pyjs_dbg_261_err;
}})()) {
					$pyjs['track']['lineno']=436;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, '_currentCursor')['__setitem__']($p['getattr'](req, 'reqType'), data['__getitem__']('cursor'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_262_err){if (!$p['isinstance']($pyjs_dbg_262_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_262_err);}throw $pyjs_dbg_262_err;
}})();
					$pyjs['track']['lineno']=438;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](req, 'params')['__setitem__']('cursor', data['__getitem__']('cursor'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_263_err){if (!$p['isinstance']($pyjs_dbg_263_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_263_err);}throw $pyjs_dbg_263_err;
}})();
					$pyjs['track']['lineno']=439;
					r = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'successHandler':$p['getattr'](self, 'onRequestSucceded'), 'failureHandler':$p['getattr'](self, 'showErrorMsg')}, $p['getattr'](self, 'modul'), (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('list/%s', $p['getattr'](req, 'reqType'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_264_err){if (!$p['isinstance']($pyjs_dbg_264_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_264_err);}throw $pyjs_dbg_264_err;
}})(), $p['getattr'](req, 'params')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_265_err){if (!$p['isinstance']($pyjs_dbg_265_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_265_err);}throw $pyjs_dbg_265_err;
}})();
					$pyjs['track']['lineno']=442;
					r['__is_instance__'] && typeof r['__setattr__'] == 'function' ? r['__setattr__']('reqType', $p['getattr'](req, 'reqType')) : $p['setattr'](r, 'reqType', $p['getattr'](req, 'reqType')); 
					$pyjs['track']['lineno']=443;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['_currentRequests']['append'](r);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_266_err){if (!$p['isinstance']($pyjs_dbg_266_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_266_err);}throw $pyjs_dbg_266_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=445;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, '_currentCursor')['__setitem__']($p['getattr'](req, 'reqType'), null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_267_err){if (!$p['isinstance']($pyjs_dbg_267_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_267_err);}throw $pyjs_dbg_267_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['req']]);
			$cls_definition['onRequestSucceded'] = $method;
			$pyjs['track']['lineno']=447;
			$method = $pyjs__bind_method2('getChildKey', function(widget) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					widget = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a28a740ecda702dad39d2c810f3ef207') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.tree', 'lineno':447};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=447;
				$pyjs['track']['lineno']=451;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](widget, $p['getattr'](self, 'nodeWidget'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_268_err){if (!$p['isinstance']($pyjs_dbg_268_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_268_err);}throw $pyjs_dbg_268_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_269_err){if (!$p['isinstance']($pyjs_dbg_269_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_269_err);}throw $pyjs_dbg_269_err;
}})()) {
					$pyjs['track']['lineno']=452;
					$pyjs['track']['lineno']=452;
					var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('0-%s', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](widget, 'data')['__getitem__']('name')['lower']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_270_err){if (!$p['isinstance']($pyjs_dbg_270_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_270_err);}throw $pyjs_dbg_270_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_271_err){if (!$p['isinstance']($pyjs_dbg_271_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_271_err);}throw $pyjs_dbg_271_err;
}})();
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](widget, $p['getattr'](self, 'leafWidget'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_272_err){if (!$p['isinstance']($pyjs_dbg_272_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_272_err);}throw $pyjs_dbg_272_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_273_err){if (!$p['isinstance']($pyjs_dbg_273_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_273_err);}throw $pyjs_dbg_273_err;
}})()) {
					$pyjs['track']['lineno']=454;
					$pyjs['track']['lineno']=454;
					var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('1-%s', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](widget, 'data')['__getitem__']('name')['lower']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_274_err){if (!$p['isinstance']($pyjs_dbg_274_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_274_err);}throw $pyjs_dbg_274_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_275_err){if (!$p['isinstance']($pyjs_dbg_275_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_275_err);}throw $pyjs_dbg_275_err;
}})();
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				else {
					$pyjs['track']['lineno']=456;
					$pyjs['track']['lineno']=456;
					var $pyjs__ret = '2-';
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['widget']]);
			$cls_definition['getChildKey'] = $method;
			$pyjs['track']['lineno']=459;
			$method = $pyjs__bind_method2('canHandle', function(modul, modulInfo) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);

				$pyjs['track']={'module':'widgets.tree', 'lineno':459};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.tree';
				$pyjs['track']['lineno']=459;
				$pyjs['track']['lineno']=460;
				$pyjs['track']['lineno']=460;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return modulInfo['__getitem__']('handler')['startswith']('tree.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_276_err){if (!$p['isinstance']($pyjs_dbg_276_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_276_err);}throw $pyjs_dbg_276_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['modulInfo']]);
			$cls_definition['canHandle'] = $method;
			$pyjs['track']['lineno']=253;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('TreeWidget', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=462;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['displayDelegateSelector']['insert']($constant_int_1, $p['getattr']($m['TreeWidget'], 'canHandle'), $m['TreeWidget']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_277_err){if (!$p['isinstance']($pyjs_dbg_277_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_277_err);}throw $pyjs_dbg_277_err;
}})();
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end widgets.tree */


/* end module: widgets.tree */


/*
PYJS_DEPS: ['html5', 'pyjd', 'network.NetworkService', 'network', 'widgets.actionbar.ActionBar', 'widgets', 'widgets.actionbar', 'widgets.search.Search', 'widgets.search', 'event.EventDispatcher', 'event', 'priorityqueue.displayDelegateSelector', 'priorityqueue', 'priorityqueue.viewDelegateSelector', 'utils', 'html5.keycodes', 'config.conf', 'config']
*/
