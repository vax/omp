/* start module: widgets.list */
$pyjs['loaded_modules']['widgets.list'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['widgets.list']['__was_initialized__']) return $pyjs['loaded_modules']['widgets.list'];
	if(typeof $pyjs['loaded_modules']['widgets'] == 'undefined' || !$pyjs['loaded_modules']['widgets']['__was_initialized__']) $p['___import___']('widgets', null);
	var $m = $pyjs['loaded_modules']['widgets.list'];
	$m['__repr__'] = function() { return '<module: widgets.list>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'widgets.list';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	$pyjs['loaded_modules']['widgets']['list'] = $pyjs['loaded_modules']['widgets.list'];
	try {
		$m.__track_lines__[1] = 'widgets.list.py, line 1:\n    import pyjd # this is dummy in pyjs.';
		$m.__track_lines__[2] = 'widgets.list.py, line 2:\n    import json';
		$m.__track_lines__[3] = 'widgets.list.py, line 3:\n    from config import conf';
		$m.__track_lines__[4] = 'widgets.list.py, line 4:\n    from network import NetworkService';
		$m.__track_lines__[5] = 'widgets.list.py, line 5:\n    from priorityqueue import viewDelegateSelector';
		$m.__track_lines__[6] = 'widgets.list.py, line 6:\n    from widgets.table import DataTable';
		$m.__track_lines__[7] = 'widgets.list.py, line 7:\n    from widgets.actionbar import ActionBar';
		$m.__track_lines__[8] = 'widgets.list.py, line 8:\n    from widgets.sidebar import SideBar';
		$m.__track_lines__[9] = 'widgets.list.py, line 9:\n    import html5';
		$m.__track_lines__[10] = 'widgets.list.py, line 10:\n    from sidebarwidgets.filterselector import CompoundFilter';
		$m.__track_lines__[11] = 'widgets.list.py, line 11:\n    from i18n import translate';
		$m.__track_lines__[14] = 'widgets.list.py, line 14:\n    class ListWidget( html5.Div ):';
		$m.__track_lines__[20] = 'widgets.list.py, line 20:\n    def __init__( self, modul, filter=None, columns=None, isSelector=False, filterID=None, filterDescr=None,';
		$m.__track_lines__[26] = 'widgets.list.py, line 26:\n    super( ListWidget, self ).__init__(  )';
		$m.__track_lines__[27] = 'widgets.list.py, line 27:\n    self._batchSize = batchSize or conf["batchSize"]    # How many rows do we fetch at once?';
		$m.__track_lines__[28] = 'widgets.list.py, line 28:\n    self.isDetaching = False #If set, this widget is beeing about to be removed - dont issue nextBatchNeeded requests';
		$m.__track_lines__[29] = 'widgets.list.py, line 29:\n    self.modul = modul';
		$m.__track_lines__[30] = 'widgets.list.py, line 30:\n    self.actionBar = ActionBar( modul, "list", currentAction="list" )';
		$m.__track_lines__[31] = 'widgets.list.py, line 31:\n    self.appendChild( self.actionBar )';
		$m.__track_lines__[32] = 'widgets.list.py, line 32:\n    self.sideBar = SideBar()';
		$m.__track_lines__[33] = 'widgets.list.py, line 33:\n    self.appendChild( self.sideBar )';
		$m.__track_lines__[35] = 'widgets.list.py, line 35:\n    myView = None';
		$m.__track_lines__[37] = 'widgets.list.py, line 37:\n    if filterID:';
		$m.__track_lines__[38] = 'widgets.list.py, line 38:\n    if conf["modules"] and modul in conf["modules"].keys():';
		$m.__track_lines__[39] = 'widgets.list.py, line 39:\n    if "views" in conf["modules"][ modul ].keys() and conf["modules"][ modul ]["views"]:';
		$m.__track_lines__[40] = 'widgets.list.py, line 40:\n    for v in conf["modules"][ modul ]["views"]:';
		$m.__track_lines__[41] = 'widgets.list.py, line 41:\n    if v["__id"] == filterID:';
		$m.__track_lines__[42] = 'widgets.list.py, line 42:\n    myView = v';
		$m.__track_lines__[43] = 'widgets.list.py, line 43:\n    break';
		$m.__track_lines__[44] = 'widgets.list.py, line 44:\n    if myView and "extendedFilters" in myView.keys() and myView["extendedFilters"]:';
		$m.__track_lines__[45] = 'widgets.list.py, line 45:\n    self.appendChild( CompoundFilter(myView, modul, embed=True))';
		$m.__track_lines__[47] = 'widgets.list.py, line 47:\n    checkboxes = (conf["modules"]';
		$m.__track_lines__[51] = 'widgets.list.py, line 51:\n    indexes = (conf["modules"]';
		$m.__track_lines__[56] = 'widgets.list.py, line 56:\n    self.table = DataTable( checkboxes=checkboxes, indexes=indexes, *args, **kwargs )';
		$m.__track_lines__[57] = 'widgets.list.py, line 57:\n    self.appendChild( self.table )';
		$m.__track_lines__[58] = 'widgets.list.py, line 58:\n    self._currentCursor = None';
		$m.__track_lines__[59] = 'widgets.list.py, line 59:\n    self._structure = None';
		$m.__track_lines__[60] = 'widgets.list.py, line 60:\n    self._currentRequests = []';
		$m.__track_lines__[61] = 'widgets.list.py, line 61:\n    self.columns = []';
		$m.__track_lines__[63] = 'widgets.list.py, line 63:\n    if isSelector and filter is None and columns is None:';
		$m.__track_lines__[65] = 'widgets.list.py, line 65:\n    if conf["modules"] and modul in conf["modules"].keys():';
		$m.__track_lines__[66] = 'widgets.list.py, line 66:\n    tmpData = conf["modules"][modul]';
		$m.__track_lines__[67] = 'widgets.list.py, line 67:\n    if "columns" in tmpData.keys():';
		$m.__track_lines__[68] = 'widgets.list.py, line 68:\n    columns = tmpData["columns"]';
		$m.__track_lines__[69] = 'widgets.list.py, line 69:\n    if "filter" in tmpData.keys():';
		$m.__track_lines__[70] = 'widgets.list.py, line 70:\n    filter = tmpData["filter"]';
		$m.__track_lines__[72] = 'widgets.list.py, line 72:\n    self.table.setDataProvider(self)';
		$m.__track_lines__[73] = 'widgets.list.py, line 73:\n    self.filter = filter.copy() if isinstance(filter,dict) else {}';
		$m.__track_lines__[74] = 'widgets.list.py, line 74:\n    self.columns = columns[:] if isinstance(columns,list) else []';
		$m.__track_lines__[75] = 'widgets.list.py, line 75:\n    self.filterID = filterID #Hint for the sidebarwidgets which predefined filter is currently active';
		$m.__track_lines__[76] = 'widgets.list.py, line 76:\n    self.filterDescr = filterDescr #Human-readable description of the current filter';
		$m.__track_lines__[77] = 'widgets.list.py, line 77:\n    self._tableHeaderIsValid = False';
		$m.__track_lines__[78] = 'widgets.list.py, line 78:\n    self.isSelector = isSelector';
		$m.__track_lines__[81] = 'widgets.list.py, line 81:\n    for f in ["selectionChangedEvent",';
		$m.__track_lines__[86] = 'widgets.list.py, line 86:\n    setattr( self, f, getattr(self.table,f))';
		$m.__track_lines__[88] = 'widgets.list.py, line 88:\n    self.actionBar.setActions( self.getDefaultActions( myView ) )';
		$m.__track_lines__[90] = 'widgets.list.py, line 90:\n    if isSelector:';
		$m.__track_lines__[91] = 'widgets.list.py, line 91:\n    self.selectionActivatedEvent.register( self )';
		$m.__track_lines__[93] = 'widgets.list.py, line 93:\n    self.emptyNotificationDiv = html5.Div()';
		$m.__track_lines__[94] = 'widgets.list.py, line 94:\n    self.emptyNotificationDiv.appendChild(html5.TextNode(translate("Currently no entries")))';
		$m.__track_lines__[95] = 'widgets.list.py, line 95:\n    self.emptyNotificationDiv["class"].append("emptynotification")';
		$m.__track_lines__[96] = 'widgets.list.py, line 96:\n    self.appendChild(self.emptyNotificationDiv)';
		$m.__track_lines__[97] = 'widgets.list.py, line 97:\n    self.emptyNotificationDiv["style"]["display"] = "none"';
		$m.__track_lines__[98] = 'widgets.list.py, line 98:\n    self.table["style"]["display"] = "none"';
		$m.__track_lines__[99] = 'widgets.list.py, line 99:\n    self.filterDescriptionSpan = html5.Span()';
		$m.__track_lines__[100] = 'widgets.list.py, line 100:\n    self.appendChild( self.filterDescriptionSpan )';
		$m.__track_lines__[101] = 'widgets.list.py, line 101:\n    self.filterDescriptionSpan["class"].append("filterdescription")';
		$m.__track_lines__[102] = 'widgets.list.py, line 102:\n    self.updateFilterDescription()';
		$m.__track_lines__[103] = 'widgets.list.py, line 103:\n    self.reloadData()';
		$m.__track_lines__[105] = 'widgets.list.py, line 105:\n    def updateFilterDescription(self):';
		$m.__track_lines__[106] = 'widgets.list.py, line 106:\n    self.filterDescriptionSpan.removeAllChildren()';
		$m.__track_lines__[108] = 'widgets.list.py, line 108:\n    if self.filterDescr:';
		$m.__track_lines__[109] = 'widgets.list.py, line 109:\n    self.filterDescriptionSpan.appendChild(html5.TextNode(html5.utils.unescape(self.filterDescr)))';
		$m.__track_lines__[111] = 'widgets.list.py, line 111:\n    def getDefaultActions(self, view = None ):';
		$m.__track_lines__[115] = 'widgets.list.py, line 115:\n    defaultActions = ["add", "edit", "clone", "delete",';
		$m.__track_lines__[120] = 'widgets.list.py, line 120:\n    if view and "actions" in view.keys():';
		$m.__track_lines__[121] = 'widgets.list.py, line 121:\n    if defaultActions[-1] != "|":';
		$m.__track_lines__[122] = 'widgets.list.py, line 122:\n    defaultActions.append( "|" )';
		$m.__track_lines__[124] = 'widgets.list.py, line 124:\n    defaultActions.extend( view[ "actions" ] or [] )';
		$m.__track_lines__[128] = 'widgets.list.py, line 128:\n    cfg = conf["modules"][ self.modul ]';
		$m.__track_lines__[130] = 'widgets.list.py, line 130:\n    if "actions" in cfg.keys() and cfg["actions"]:';
		$m.__track_lines__[131] = 'widgets.list.py, line 131:\n    if defaultActions[-1] != "|":';
		$m.__track_lines__[132] = 'widgets.list.py, line 132:\n    defaultActions.append( "|" )';
		$m.__track_lines__[134] = 'widgets.list.py, line 134:\n    defaultActions.extend( cfg["actions"] )';
		$m.__track_lines__[136] = 'widgets.list.py, line 136:\n    return defaultActions';
		$m.__track_lines__[138] = 'widgets.list.py, line 138:\n    def showErrorMsg(self, req=None, code=None):';
		$m.__track_lines__[142] = 'widgets.list.py, line 142:\n    self.actionBar["style"]["display"] = "none"';
		$m.__track_lines__[143] = 'widgets.list.py, line 143:\n    self.table["style"]["display"] = "none"';
		$m.__track_lines__[144] = 'widgets.list.py, line 144:\n    errorDiv = html5.Div()';
		$m.__track_lines__[145] = 'widgets.list.py, line 145:\n    errorDiv["class"].append("error_msg")';
		$m.__track_lines__[146] = 'widgets.list.py, line 146:\n    if code and (code==401 or code==403):';
		$m.__track_lines__[147] = 'widgets.list.py, line 147:\n    txt = translate("Access denied!")';
		$m.__track_lines__[149] = 'widgets.list.py, line 149:\n    txt = translate("An unknown error occurred!")';
		$m.__track_lines__[150] = 'widgets.list.py, line 150:\n    errorDiv["class"].append("error_code_%s" % (code or 0))';
		$m.__track_lines__[151] = 'widgets.list.py, line 151:\n    errorDiv.appendChild( html5.TextNode( txt ) )';
		$m.__track_lines__[152] = 'widgets.list.py, line 152:\n    self.appendChild( errorDiv )';
		$m.__track_lines__[154] = 'widgets.list.py, line 154:\n    def onNextBatchNeeded(self):';
		$m.__track_lines__[158] = 'widgets.list.py, line 158:\n    if self._currentCursor and not self.isDetaching:';
		$m.__track_lines__[159] = 'widgets.list.py, line 159:\n    filter = self.filter.copy()';
		$m.__track_lines__[160] = 'widgets.list.py, line 160:\n    filter["amount"] = self._batchSize';
		$m.__track_lines__[161] = 'widgets.list.py, line 161:\n    filter["cursor"] = self._currentCursor';
		$m.__track_lines__[162] = 'widgets.list.py, line 162:\n    self._currentRequests.append( NetworkService.request(self.modul, "list", filter,';
		$m.__track_lines__[165] = 'widgets.list.py, line 165:\n    self._currentCursor = None';
		$m.__track_lines__[167] = 'widgets.list.py, line 167:\n    self.table.setDataProvider( None )';
		$m.__track_lines__[169] = 'widgets.list.py, line 169:\n    def onAttach(self):';
		$m.__track_lines__[170] = 'widgets.list.py, line 170:\n    super( ListWidget, self ).onAttach()';
		$m.__track_lines__[171] = 'widgets.list.py, line 171:\n    NetworkService.registerChangeListener( self )';
		$m.__track_lines__[173] = 'widgets.list.py, line 173:\n    def onDetach(self):';
		$m.__track_lines__[174] = 'widgets.list.py, line 174:\n    self.isDetaching = True';
		$m.__track_lines__[175] = 'widgets.list.py, line 175:\n    super( ListWidget, self ).onDetach()';
		$m.__track_lines__[176] = 'widgets.list.py, line 176:\n    NetworkService.removeChangeListener( self )';
		$m.__track_lines__[178] = 'widgets.list.py, line 178:\n    def onDataChanged(self, modul):';
		$m.__track_lines__[182] = 'widgets.list.py, line 182:\n    if modul and modul!=self.modul:';
		$m.__track_lines__[183] = 'widgets.list.py, line 183:\n    return';
		$m.__track_lines__[185] = 'widgets.list.py, line 185:\n    self.reloadData()';
		$m.__track_lines__[187] = 'widgets.list.py, line 187:\n    def reloadData(self):';
		$m.__track_lines__[191] = 'widgets.list.py, line 191:\n    self.table.clear()';
		$m.__track_lines__[192] = 'widgets.list.py, line 192:\n    self._currentCursor = None';
		$m.__track_lines__[193] = 'widgets.list.py, line 193:\n    self._currentRequests = []';
		$m.__track_lines__[194] = 'widgets.list.py, line 194:\n    filter = self.filter.copy()';
		$m.__track_lines__[195] = 'widgets.list.py, line 195:\n    filter["amount"] = self._batchSize';
		$m.__track_lines__[198] = 'widgets.list.py, line 197:\n    self._currentRequests.append( ... NetworkService.request( self.modul, "list", filter,';
		$m.__track_lines__[203] = 'widgets.list.py, line 203:\n    def setFilter(self, filter, filterID=None, filterDescr=None):';
		$m.__track_lines__[207] = 'widgets.list.py, line 207:\n    self.filter = filter';
		$m.__track_lines__[208] = 'widgets.list.py, line 208:\n    self.filterID = filterID';
		$m.__track_lines__[209] = 'widgets.list.py, line 209:\n    self.filterDescr = filterDescr';
		$m.__track_lines__[210] = 'widgets.list.py, line 210:\n    self.updateFilterDescription()';
		$m.__track_lines__[211] = 'widgets.list.py, line 211:\n    self.reloadData()';
		$m.__track_lines__[213] = 'widgets.list.py, line 213:\n    def getFilter(self):';
		$m.__track_lines__[214] = 'widgets.list.py, line 214:\n    if self.filter:';
		$m.__track_lines__[215] = 'widgets.list.py, line 215:\n    return( {k:v for k,v in self.filter.items()})';
		$m.__track_lines__[216] = 'widgets.list.py, line 216:\n    return( {} )';
		$m.__track_lines__[218] = 'widgets.list.py, line 218:\n    def onCompletion(self, req):';
		$m.__track_lines__[223] = 'widgets.list.py, line 223:\n    if not req in self._currentRequests:';
		$m.__track_lines__[224] = 'widgets.list.py, line 224:\n    return';
		$m.__track_lines__[225] = 'widgets.list.py, line 225:\n    self._currentRequests.remove( req )';
		$m.__track_lines__[226] = 'widgets.list.py, line 226:\n    self.actionBar.resetLoadingState()';
		$m.__track_lines__[228] = 'widgets.list.py, line 228:\n    data = NetworkService.decode( req )';
		$m.__track_lines__[230] = 'widgets.list.py, line 230:\n    if data["structure"] is None:';
		$m.__track_lines__[231] = 'widgets.list.py, line 231:\n    if self.table.getRowCount():';
		$m.__track_lines__[232] = 'widgets.list.py, line 232:\n    self.table.setDataProvider(None) #We cant load any more results';
		$m.__track_lines__[234] = 'widgets.list.py, line 234:\n    self.table["style"]["display"] = "none"';
		$m.__track_lines__[235] = 'widgets.list.py, line 235:\n    self.emptyNotificationDiv["style"]["display"] = ""';
		$m.__track_lines__[237] = 'widgets.list.py, line 237:\n    return';
		$m.__track_lines__[239] = 'widgets.list.py, line 239:\n    self.table["style"]["display"] = ""';
		$m.__track_lines__[240] = 'widgets.list.py, line 240:\n    self.emptyNotificationDiv["style"]["display"] = "none"';
		$m.__track_lines__[241] = 'widgets.list.py, line 241:\n    self._structure = data["structure"]';
		$m.__track_lines__[242] = 'widgets.list.py, line 242:\n    tmpDict = {}';
		$m.__track_lines__[244] = 'widgets.list.py, line 244:\n    for key, bone in data["structure"]:';
		$m.__track_lines__[245] = 'widgets.list.py, line 245:\n    tmpDict[ key ] = bone';
		$m.__track_lines__[247] = 'widgets.list.py, line 247:\n    if not self._tableHeaderIsValid:';
		$m.__track_lines__[248] = 'widgets.list.py, line 248:\n    if not self.columns:';
		$m.__track_lines__[249] = 'widgets.list.py, line 249:\n    self.columns = []';
		$m.__track_lines__[250] = 'widgets.list.py, line 250:\n    for boneName, boneInfo in data["structure"]:';
		$m.__track_lines__[251] = 'widgets.list.py, line 251:\n    if boneInfo["visible"]:';
		$m.__track_lines__[252] = 'widgets.list.py, line 252:\n    self.columns.append( boneName )';
		$m.__track_lines__[253] = 'widgets.list.py, line 253:\n    self.setFields( self.columns )';
		$m.__track_lines__[256] = 'widgets.list.py, line 256:\n    if "cursor" in data.keys():';
		$m.__track_lines__[257] = 'widgets.list.py, line 257:\n    self._currentCursor = data["cursor"]';
		$m.__track_lines__[258] = 'widgets.list.py, line 258:\n    self.table.setDataProvider( self )';
		$m.__track_lines__[260] = 'widgets.list.py, line 260:\n    self.table.extend( data["skellist"] )';
		$m.__track_lines__[262] = 'widgets.list.py, line 262:\n    def setFields(self, fields):';
		$m.__track_lines__[263] = 'widgets.list.py, line 263:\n    if not self._structure:';
		$m.__track_lines__[264] = 'widgets.list.py, line 264:\n    self._tableHeaderIsValid = False';
		$m.__track_lines__[265] = 'widgets.list.py, line 265:\n    return';
		$m.__track_lines__[266] = 'widgets.list.py, line 266:\n    boneInfoList = []';
		$m.__track_lines__[267] = 'widgets.list.py, line 267:\n    tmpDict = {}';
		$m.__track_lines__[268] = 'widgets.list.py, line 268:\n    for key, bone in self._structure:';
		$m.__track_lines__[269] = 'widgets.list.py, line 269:\n    tmpDict[ key ] = bone';
		$m.__track_lines__[270] = 'widgets.list.py, line 270:\n    fields = [x for x in fields if x in tmpDict.keys()]';
		$m.__track_lines__[271] = 'widgets.list.py, line 271:\n    self.columns = fields';
		$m.__track_lines__[272] = 'widgets.list.py, line 272:\n    for boneName in fields:';
		$m.__track_lines__[273] = 'widgets.list.py, line 273:\n    boneInfo = tmpDict[boneName]';
		$m.__track_lines__[274] = 'widgets.list.py, line 274:\n    delegateFactory = viewDelegateSelector.select( self.modul, boneName, tmpDict )( self.modul, boneName, tmpDict )';
		$m.__track_lines__[275] = 'widgets.list.py, line 275:\n    self.table.setCellRender( boneName, delegateFactory )';
		$m.__track_lines__[276] = 'widgets.list.py, line 276:\n    boneInfoList.append( boneInfo )';
		$m.__track_lines__[277] = 'widgets.list.py, line 277:\n    self.table.setHeader( [x["descr"] for x in boneInfoList])';
		$m.__track_lines__[278] = 'widgets.list.py, line 278:\n    self.table.setShownFields( fields )';
		$m.__track_lines__[279] = 'widgets.list.py, line 279:\n    rendersDict = {}';
		$m.__track_lines__[280] = 'widgets.list.py, line 280:\n    for boneName in fields:';
		$m.__track_lines__[281] = 'widgets.list.py, line 281:\n    boneInfo = tmpDict[boneName]';
		$m.__track_lines__[282] = 'widgets.list.py, line 282:\n    delegateFactory = viewDelegateSelector.select( self.modul, boneName, tmpDict )( self.modul, boneName, tmpDict )';
		$m.__track_lines__[283] = 'widgets.list.py, line 283:\n    rendersDict[ boneName ] = delegateFactory';
		$m.__track_lines__[284] = 'widgets.list.py, line 284:\n    boneInfoList.append( boneInfo )';
		$m.__track_lines__[285] = 'widgets.list.py, line 285:\n    self.table.setCellRenders( rendersDict )';
		$m.__track_lines__[286] = 'widgets.list.py, line 286:\n    self._tableHeaderIsValid = True';
		$m.__track_lines__[288] = 'widgets.list.py, line 288:\n    def getFields(self):';
		$m.__track_lines__[289] = 'widgets.list.py, line 289:\n    return( self.columns[:] )';
		$m.__track_lines__[291] = 'widgets.list.py, line 291:\n    def onSelectionActivated(self, table, selection):';
		$m.__track_lines__[292] = 'widgets.list.py, line 292:\n    conf["mainWindow"].removeWidget(self)';
		$m.__track_lines__[294] = 'widgets.list.py, line 294:\n    def activateCurrentSelection(self):';
		$m.__track_lines__[299] = 'widgets.list.py, line 299:\n    self.table.activateCurrentSelection()';

		var $constant_int_0 = new $p['int'](0);
		var $constant_int_1 = new $p['int'](1);
		var $constant_int_403 = new $p['int'](403);
		var $constant_int_401 = new $p['int'](401);
		$pyjs['track']['module']='widgets.list';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=1;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['pyjd'] = $p['___import___']('pyjd', 'widgets');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=2;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['json'] = $p['___import___']('json', 'widgets');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=3;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['conf'] = $p['___import___']('config.conf', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=4;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['NetworkService'] = $p['___import___']('network.NetworkService', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=5;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['viewDelegateSelector'] = $p['___import___']('priorityqueue.viewDelegateSelector', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=6;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['DataTable'] = $p['___import___']('widgets.table.DataTable', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=7;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['ActionBar'] = $p['___import___']('widgets.actionbar.ActionBar', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=8;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['SideBar'] = $p['___import___']('widgets.sidebar.SideBar', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=9;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['html5'] = $p['___import___']('html5', 'widgets');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=10;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['CompoundFilter'] = $p['___import___']('sidebarwidgets.filterselector.CompoundFilter', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=11;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['translate'] = $p['___import___']('i18n.translate', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=14;
		$m['ListWidget'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'widgets.list';
			$cls_definition['__md5__'] = '8a614eb21d351b18507e25b93f95b5ac';
			$pyjs['track']['lineno']=20;
			$method = $pyjs__bind_method2('__init__', function(modul, filter, columns, isSelector, filterID, filterDescr, batchSize) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,7,arguments['length']-1));

					var kwargs = arguments['length'] >= 8 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					modul = arguments[1];
					filter = arguments[2];
					columns = arguments[3];
					isSelector = arguments[4];
					filterID = arguments[5];
					filterDescr = arguments[6];
					batchSize = arguments[7];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,8,arguments['length']-1));

					var kwargs = arguments['length'] >= 9 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof batchSize != 'undefined') {
						if (batchSize !== null && typeof batchSize['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = batchSize;
							batchSize = arguments[8];
						}
					} else 					if (typeof filterDescr != 'undefined') {
						if (filterDescr !== null && typeof filterDescr['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = filterDescr;
							filterDescr = arguments[8];
						}
					} else 					if (typeof filterID != 'undefined') {
						if (filterID !== null && typeof filterID['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = filterID;
							filterID = arguments[8];
						}
					} else 					if (typeof isSelector != 'undefined') {
						if (isSelector !== null && typeof isSelector['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = isSelector;
							isSelector = arguments[8];
						}
					} else 					if (typeof columns != 'undefined') {
						if (columns !== null && typeof columns['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = columns;
							columns = arguments[8];
						}
					} else 					if (typeof filter != 'undefined') {
						if (filter !== null && typeof filter['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = filter;
							filter = arguments[8];
						}
					} else 					if (typeof modul != 'undefined') {
						if (modul !== null && typeof modul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modul;
							modul = arguments[8];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[8];
						}
					} else {
					}
				}
				if (typeof filter == 'undefined') filter=arguments['callee']['__args__'][4][1];
				if (typeof columns == 'undefined') columns=arguments['callee']['__args__'][5][1];
				if (typeof isSelector == 'undefined') isSelector=arguments['callee']['__args__'][6][1];
				if (typeof filterID == 'undefined') filterID=arguments['callee']['__args__'][7][1];
				if (typeof filterDescr == 'undefined') filterDescr=arguments['callee']['__args__'][8][1];
				if (typeof batchSize == 'undefined') batchSize=arguments['callee']['__args__'][9][1];
				var $pyjs__trackstack_size_1,$iter1_iter,$and15,$iter1_nextval,$and20,$iter2_iter,$iter2_type,indexes,tmpData,$iter1_array,checkboxes,$and8,$and9,$or1,$or2,$and1,$and2,$and3,$and4,$and5,$and6,$and7,$and12,$and13,$and10,$and11,$and16,$and17,$and14,myView,$and18,$and19,$iter2_nextval,$iter1_type,f,$iter2_idx,$iter1_idx,v,$iter2_array;
				$pyjs['track']={'module':'widgets.list', 'lineno':20};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=20;
				$pyjs['track']['lineno']=26;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ListWidget'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})()['__init__']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})();
				$pyjs['track']['lineno']=27;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_batchSize', ($p['bool']($or1=batchSize)?$or1:$m['conf']['__getitem__']('batchSize'))) : $p['setattr'](self, '_batchSize', ($p['bool']($or1=batchSize)?$or1:$m['conf']['__getitem__']('batchSize'))); 
				$pyjs['track']['lineno']=28;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDetaching', false) : $p['setattr'](self, 'isDetaching', false); 
				$pyjs['track']['lineno']=29;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=30;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('actionBar', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['ActionBar'], null, null, [{'currentAction':'list'}, modul, 'list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})()) : $p['setattr'](self, 'actionBar', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['ActionBar'], null, null, [{'currentAction':'list'}, modul, 'list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})()); 
				$pyjs['track']['lineno']=31;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'actionBar'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})();
				$pyjs['track']['lineno']=32;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('sideBar', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['SideBar']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})()) : $p['setattr'](self, 'sideBar', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['SideBar']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})()); 
				$pyjs['track']['lineno']=33;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'sideBar'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})();
				$pyjs['track']['lineno']=35;
				myView = null;
				$pyjs['track']['lineno']=37;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](filterID);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})()) {
					$pyjs['track']['lineno']=38;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and1=$m['conf']['__getitem__']('modules'))?(function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['conf']['__getitem__']('modules')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})()['__contains__'](modul):$and1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})()) {
						$pyjs['track']['lineno']=39;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](($p['bool']($and3=(function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['conf']['__getitem__']('modules')['__getitem__'](modul)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})()['__contains__']('views'))?$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('views'):$and3));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})()) {
							$pyjs['track']['lineno']=40;
							$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
							$iter1_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
							return $m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('views');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})();
							$iter1_nextval=$p['__iter_prepare']($iter1_iter,false);
							while (typeof($p['__wrapped_next']($iter1_nextval)['$nextval']) != 'undefined') {
								v = $iter1_nextval['$nextval'];
								$pyjs['track']['lineno']=41;
								if ((function(){try{try{$pyjs['in_try_except'] += 1;
									return $p['bool']($p['op_eq'](v['__getitem__']('__id'), filterID));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})()) {
									$pyjs['track']['lineno']=42;
									myView = v;
									$pyjs['track']['lineno']=43;
									break;
								}
							}
							if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
								$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
								$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
							}
							$pyjs['track']['module']='widgets.list';
						}
					}
					$pyjs['track']['lineno']=44;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and5=myView)?($p['bool']($and6=(function(){try{try{$pyjs['in_try_except'] += 1;
					return myView['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_14_err){if (!$p['isinstance']($pyjs_dbg_14_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_14_err);}throw $pyjs_dbg_14_err;
}})()['__contains__']('extendedFilters'))?myView['__getitem__']('extendedFilters'):$and6):$and5));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})()) {
						$pyjs['track']['lineno']=45;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call(null, $m['CompoundFilter'], null, null, [{'embed':true}, myView, modul]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_16_err){if (!$p['isinstance']($pyjs_dbg_16_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_16_err);}throw $pyjs_dbg_16_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_17_err){if (!$p['isinstance']($pyjs_dbg_17_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_17_err);}throw $pyjs_dbg_17_err;
}})();
					}
				}
				$pyjs['track']['lineno']=47;
				checkboxes = ($p['bool']($and8=$m['conf']['__getitem__']('modules'))?($p['bool']($and9=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})()['__contains__'](modul))?($p['bool']($and10=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['__getitem__'](modul)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_19_err){if (!$p['isinstance']($pyjs_dbg_19_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_19_err);}throw $pyjs_dbg_19_err;
}})()['__contains__']('checkboxSelection'))?$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('checkboxSelection'):$and10):$and9):$and8);
				$pyjs['track']['lineno']=51;
				indexes = ($p['bool']($and12=$m['conf']['__getitem__']('modules'))?($p['bool']($and13=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})()['__contains__'](modul))?($p['bool']($and14=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['__getitem__'](modul)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})()['__contains__']('indexes'))?$m['conf']['__getitem__']('modules')['__getitem__'](modul)['__getitem__']('indexes'):$and14):$and13):$and12);
				$pyjs['track']['lineno']=56;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('table', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['DataTable'], args, kwargs, [{'checkboxes':checkboxes, 'indexes':indexes}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})()) : $p['setattr'](self, 'table', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['DataTable'], args, kwargs, [{'checkboxes':checkboxes, 'indexes':indexes}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})()); 
				$pyjs['track']['lineno']=57;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'table'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})();
				$pyjs['track']['lineno']=58;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_currentCursor', null) : $p['setattr'](self, '_currentCursor', null); 
				$pyjs['track']['lineno']=59;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_structure', null) : $p['setattr'](self, '_structure', null); 
				$pyjs['track']['lineno']=60;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_currentRequests', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})()) : $p['setattr'](self, '_currentRequests', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})()); 
				$pyjs['track']['lineno']=61;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('columns', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})()) : $p['setattr'](self, 'columns', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})()); 
				$pyjs['track']['lineno']=63;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and16=isSelector)?($p['bool']($and17=$p['op_is'](filter, null))?$p['op_is'](columns, null):$and17):$and16));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_26_err){if (!$p['isinstance']($pyjs_dbg_26_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_26_err);}throw $pyjs_dbg_26_err;
}})()) {
					$pyjs['track']['lineno']=65;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and19=$m['conf']['__getitem__']('modules'))?(function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['conf']['__getitem__']('modules')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_27_err){if (!$p['isinstance']($pyjs_dbg_27_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_27_err);}throw $pyjs_dbg_27_err;
}})()['__contains__'](modul):$and19));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_28_err){if (!$p['isinstance']($pyjs_dbg_28_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_28_err);}throw $pyjs_dbg_28_err;
}})()) {
						$pyjs['track']['lineno']=66;
						tmpData = $m['conf']['__getitem__']('modules')['__getitem__'](modul);
						$pyjs['track']['lineno']=67;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return tmpData['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_29_err){if (!$p['isinstance']($pyjs_dbg_29_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_29_err);}throw $pyjs_dbg_29_err;
}})()['__contains__']('columns'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_30_err){if (!$p['isinstance']($pyjs_dbg_30_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_30_err);}throw $pyjs_dbg_30_err;
}})()) {
							$pyjs['track']['lineno']=68;
							columns = tmpData['__getitem__']('columns');
						}
						$pyjs['track']['lineno']=69;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return tmpData['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_31_err){if (!$p['isinstance']($pyjs_dbg_31_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_31_err);}throw $pyjs_dbg_31_err;
}})()['__contains__']('filter'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_32_err){if (!$p['isinstance']($pyjs_dbg_32_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_32_err);}throw $pyjs_dbg_32_err;
}})()) {
							$pyjs['track']['lineno']=70;
							filter = tmpData['__getitem__']('filter');
						}
					}
				}
				$pyjs['track']['lineno']=72;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['table']['setDataProvider'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_33_err){if (!$p['isinstance']($pyjs_dbg_33_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_33_err);}throw $pyjs_dbg_33_err;
}})();
				$pyjs['track']['lineno']=73;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('filter', ($p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](filter, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_34_err){if (!$p['isinstance']($pyjs_dbg_34_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_34_err);}throw $pyjs_dbg_34_err;
}})())? ((function(){try{try{$pyjs['in_try_except'] += 1;
				return filter['copy']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})()) : ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_36_err){if (!$p['isinstance']($pyjs_dbg_36_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_36_err);}throw $pyjs_dbg_36_err;
}})()))) : $p['setattr'](self, 'filter', ($p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](filter, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_34_err){if (!$p['isinstance']($pyjs_dbg_34_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_34_err);}throw $pyjs_dbg_34_err;
}})())? ((function(){try{try{$pyjs['in_try_except'] += 1;
				return filter['copy']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})()) : ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_36_err){if (!$p['isinstance']($pyjs_dbg_36_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_36_err);}throw $pyjs_dbg_36_err;
}})()))); 
				$pyjs['track']['lineno']=74;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('columns', ($p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](columns, $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_37_err){if (!$p['isinstance']($pyjs_dbg_37_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_37_err);}throw $pyjs_dbg_37_err;
}})())? ($p['__getslice'](columns, 0, null)) : ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_38_err){if (!$p['isinstance']($pyjs_dbg_38_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_38_err);}throw $pyjs_dbg_38_err;
}})()))) : $p['setattr'](self, 'columns', ($p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](columns, $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_37_err){if (!$p['isinstance']($pyjs_dbg_37_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_37_err);}throw $pyjs_dbg_37_err;
}})())? ($p['__getslice'](columns, 0, null)) : ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_38_err){if (!$p['isinstance']($pyjs_dbg_38_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_38_err);}throw $pyjs_dbg_38_err;
}})()))); 
				$pyjs['track']['lineno']=75;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('filterID', filterID) : $p['setattr'](self, 'filterID', filterID); 
				$pyjs['track']['lineno']=76;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('filterDescr', filterDescr) : $p['setattr'](self, 'filterDescr', filterDescr); 
				$pyjs['track']['lineno']=77;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_tableHeaderIsValid', false) : $p['setattr'](self, '_tableHeaderIsValid', false); 
				$pyjs['track']['lineno']=78;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isSelector', isSelector) : $p['setattr'](self, 'isSelector', isSelector); 
				$pyjs['track']['lineno']=81;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter2_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list'](['selectionChangedEvent', 'selectionActivatedEvent', 'cursorMovedEvent', 'tableChangedEvent', 'getCurrentSelection']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_39_err){if (!$p['isinstance']($pyjs_dbg_39_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_39_err);}throw $pyjs_dbg_39_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_40_err){if (!$p['isinstance']($pyjs_dbg_40_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_40_err);}throw $pyjs_dbg_40_err;
}})();
				$iter2_nextval=$p['__iter_prepare']($iter2_iter,false);
				while (typeof($p['__wrapped_next']($iter2_nextval)['$nextval']) != 'undefined') {
					f = $iter2_nextval['$nextval'];
					$pyjs['track']['lineno']=86;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['setattr'](self, f, (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr']($p['getattr'](self, 'table'), f);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_41_err){if (!$p['isinstance']($pyjs_dbg_41_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_41_err);}throw $pyjs_dbg_41_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_42_err){if (!$p['isinstance']($pyjs_dbg_42_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_42_err);}throw $pyjs_dbg_42_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=88;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['actionBar']['setActions']((function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getDefaultActions'](myView);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_43_err){if (!$p['isinstance']($pyjs_dbg_43_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_43_err);}throw $pyjs_dbg_43_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_44_err){if (!$p['isinstance']($pyjs_dbg_44_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_44_err);}throw $pyjs_dbg_44_err;
}})();
				$pyjs['track']['lineno']=90;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](isSelector);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_45_err){if (!$p['isinstance']($pyjs_dbg_45_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_45_err);}throw $pyjs_dbg_45_err;
}})()) {
					$pyjs['track']['lineno']=91;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['selectionActivatedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_46_err){if (!$p['isinstance']($pyjs_dbg_46_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_46_err);}throw $pyjs_dbg_46_err;
}})();
				}
				$pyjs['track']['lineno']=93;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('emptyNotificationDiv', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})()) : $p['setattr'](self, 'emptyNotificationDiv', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})()); 
				$pyjs['track']['lineno']=94;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['emptyNotificationDiv']['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['TextNode']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Currently no entries');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_48_err){if (!$p['isinstance']($pyjs_dbg_48_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_48_err);}throw $pyjs_dbg_48_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_49_err){if (!$p['isinstance']($pyjs_dbg_49_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_49_err);}throw $pyjs_dbg_49_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_50_err){if (!$p['isinstance']($pyjs_dbg_50_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_50_err);}throw $pyjs_dbg_50_err;
}})();
				$pyjs['track']['lineno']=95;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'emptyNotificationDiv')['__getitem__']('class')['append']('emptynotification');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_51_err){if (!$p['isinstance']($pyjs_dbg_51_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_51_err);}throw $pyjs_dbg_51_err;
}})();
				$pyjs['track']['lineno']=96;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'emptyNotificationDiv'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_52_err){if (!$p['isinstance']($pyjs_dbg_52_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_52_err);}throw $pyjs_dbg_52_err;
}})();
				$pyjs['track']['lineno']=97;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'emptyNotificationDiv')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_53_err){if (!$p['isinstance']($pyjs_dbg_53_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_53_err);}throw $pyjs_dbg_53_err;
}})();
				$pyjs['track']['lineno']=98;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'table')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_54_err){if (!$p['isinstance']($pyjs_dbg_54_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_54_err);}throw $pyjs_dbg_54_err;
}})();
				$pyjs['track']['lineno']=99;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('filterDescriptionSpan', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_55_err){if (!$p['isinstance']($pyjs_dbg_55_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_55_err);}throw $pyjs_dbg_55_err;
}})()) : $p['setattr'](self, 'filterDescriptionSpan', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_55_err){if (!$p['isinstance']($pyjs_dbg_55_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_55_err);}throw $pyjs_dbg_55_err;
}})()); 
				$pyjs['track']['lineno']=100;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'filterDescriptionSpan'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_56_err){if (!$p['isinstance']($pyjs_dbg_56_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_56_err);}throw $pyjs_dbg_56_err;
}})();
				$pyjs['track']['lineno']=101;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'filterDescriptionSpan')['__getitem__']('class')['append']('filterdescription');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_57_err){if (!$p['isinstance']($pyjs_dbg_57_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_57_err);}throw $pyjs_dbg_57_err;
}})();
				$pyjs['track']['lineno']=102;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['updateFilterDescription']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_58_err){if (!$p['isinstance']($pyjs_dbg_58_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_58_err);}throw $pyjs_dbg_58_err;
}})();
				$pyjs['track']['lineno']=103;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['reloadData']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_59_err){if (!$p['isinstance']($pyjs_dbg_59_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_59_err);}throw $pyjs_dbg_59_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['modul'],['filter', null],['columns', null],['isSelector', false],['filterID', null],['filterDescr', null],['batchSize', null]]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=105;
			$method = $pyjs__bind_method2('updateFilterDescription', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.list', 'lineno':105};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=105;
				$pyjs['track']['lineno']=106;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['filterDescriptionSpan']['removeAllChildren']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_60_err){if (!$p['isinstance']($pyjs_dbg_60_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_60_err);}throw $pyjs_dbg_60_err;
}})();
				$pyjs['track']['lineno']=108;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'filterDescr'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_61_err){if (!$p['isinstance']($pyjs_dbg_61_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_61_err);}throw $pyjs_dbg_61_err;
}})()) {
					$pyjs['track']['lineno']=109;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['filterDescriptionSpan']['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['TextNode']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['utils']['unescape']($p['getattr'](self, 'filterDescr'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_62_err){if (!$p['isinstance']($pyjs_dbg_62_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_62_err);}throw $pyjs_dbg_62_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_63_err){if (!$p['isinstance']($pyjs_dbg_63_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_63_err);}throw $pyjs_dbg_63_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['updateFilterDescription'] = $method;
			$pyjs['track']['lineno']=111;
			$method = $pyjs__bind_method2('getDefaultActions', function(view) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					view = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof view == 'undefined') view=arguments['callee']['__args__'][3][1];
				var $or4,$or3,cfg,$and26,$add2,$add3,$add1,$add4,$and23,$and22,$and21,$and24,defaultActions,$and25;
				$pyjs['track']={'module':'widgets.list', 'lineno':111};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=111;
				$pyjs['track']['lineno']=115;
				defaultActions = $p['__op_add']($add3=$p['__op_add']($add1=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list'](['add', 'edit', 'clone', 'delete', '|', 'preview', 'selectfields']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_65_err){if (!$p['isinstance']($pyjs_dbg_65_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_65_err);}throw $pyjs_dbg_65_err;
}})(),$add2=($p['bool']($p['getattr'](self, 'isSelector'))? ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list'](['|', 'select', 'close']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_66_err){if (!$p['isinstance']($pyjs_dbg_66_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_66_err);}throw $pyjs_dbg_66_err;
}})()) : ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})()))),$add4=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list'](['|', 'reload', 'selectfilter']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_68_err){if (!$p['isinstance']($pyjs_dbg_68_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_68_err);}throw $pyjs_dbg_68_err;
}})());
				$pyjs['track']['lineno']=120;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and21=view)?(function(){try{try{$pyjs['in_try_except'] += 1;
				return view['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_69_err){if (!$p['isinstance']($pyjs_dbg_69_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_69_err);}throw $pyjs_dbg_69_err;
}})()['__contains__']('actions'):$and21));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_70_err){if (!$p['isinstance']($pyjs_dbg_70_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_70_err);}throw $pyjs_dbg_70_err;
}})()) {
					$pyjs['track']['lineno']=121;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['op_eq'](defaultActions['__getitem__']((typeof ($usub1=$constant_int_1)=='number'?
						-$usub1:
						$p['op_usub']($usub1))), '|'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_71_err){if (!$p['isinstance']($pyjs_dbg_71_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_71_err);}throw $pyjs_dbg_71_err;
}})()) {
						$pyjs['track']['lineno']=122;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return defaultActions['append']('|');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_72_err){if (!$p['isinstance']($pyjs_dbg_72_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_72_err);}throw $pyjs_dbg_72_err;
}})();
					}
					$pyjs['track']['lineno']=124;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return defaultActions['extend'](($p['bool']($or3=view['__getitem__']('actions'))?$or3:(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_73_err){if (!$p['isinstance']($pyjs_dbg_73_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_73_err);}throw $pyjs_dbg_73_err;
}})()));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_74_err){if (!$p['isinstance']($pyjs_dbg_74_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_74_err);}throw $pyjs_dbg_74_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and23=$m['conf']['__getitem__']('modules'))?(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('modules')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})()['__contains__']($p['getattr'](self, 'modul')):$and23));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})()) {
					$pyjs['track']['lineno']=128;
					cfg = $m['conf']['__getitem__']('modules')['__getitem__']($p['getattr'](self, 'modul'));
					$pyjs['track']['lineno']=130;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and25=(function(){try{try{$pyjs['in_try_except'] += 1;
					return cfg['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_77_err){if (!$p['isinstance']($pyjs_dbg_77_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_77_err);}throw $pyjs_dbg_77_err;
}})()['__contains__']('actions'))?cfg['__getitem__']('actions'):$and25));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_78_err){if (!$p['isinstance']($pyjs_dbg_78_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_78_err);}throw $pyjs_dbg_78_err;
}})()) {
						$pyjs['track']['lineno']=131;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['op_eq'](defaultActions['__getitem__']((typeof ($usub2=$constant_int_1)=='number'?
							-$usub2:
							$p['op_usub']($usub2))), '|'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_79_err){if (!$p['isinstance']($pyjs_dbg_79_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_79_err);}throw $pyjs_dbg_79_err;
}})()) {
							$pyjs['track']['lineno']=132;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return defaultActions['append']('|');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_80_err){if (!$p['isinstance']($pyjs_dbg_80_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_80_err);}throw $pyjs_dbg_80_err;
}})();
						}
						$pyjs['track']['lineno']=134;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return defaultActions['extend'](cfg['__getitem__']('actions'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_81_err){if (!$p['isinstance']($pyjs_dbg_81_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_81_err);}throw $pyjs_dbg_81_err;
}})();
					}
				}
				$pyjs['track']['lineno']=136;
				$pyjs['track']['lineno']=136;
				var $pyjs__ret = defaultActions;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['view', null]]);
			$cls_definition['getDefaultActions'] = $method;
			$pyjs['track']['lineno']=138;
			$method = $pyjs__bind_method2('showErrorMsg', function(req, code) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					req = arguments[1];
					code = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 3)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof req == 'undefined') req=arguments['callee']['__args__'][3][1];
				if (typeof code == 'undefined') code=arguments['callee']['__args__'][4][1];
				var $or5,$or7,$or8,errorDiv,$and28,txt,$and27,$or6;
				$pyjs['track']={'module':'widgets.list', 'lineno':138};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=138;
				$pyjs['track']['lineno']=142;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'actionBar')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_82_err){if (!$p['isinstance']($pyjs_dbg_82_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_82_err);}throw $pyjs_dbg_82_err;
}})();
				$pyjs['track']['lineno']=143;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'table')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_83_err){if (!$p['isinstance']($pyjs_dbg_83_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_83_err);}throw $pyjs_dbg_83_err;
}})();
				$pyjs['track']['lineno']=144;
				errorDiv = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_84_err){if (!$p['isinstance']($pyjs_dbg_84_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_84_err);}throw $pyjs_dbg_84_err;
}})();
				$pyjs['track']['lineno']=145;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return errorDiv['__getitem__']('class')['append']('error_msg');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})();
				$pyjs['track']['lineno']=146;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and27=code)?($p['bool']($or5=$p['op_eq'](code, $constant_int_401))?$or5:$p['op_eq'](code, $constant_int_403)):$and27));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})()) {
					$pyjs['track']['lineno']=147;
					txt = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Access denied!');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=149;
					txt = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('An unknown error occurred!');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})();
				}
				$pyjs['track']['lineno']=150;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return errorDiv['__getitem__']('class')['append']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('error_code_%s', ($p['bool']($or7=code)?$or7:$constant_int_0));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_89_err){if (!$p['isinstance']($pyjs_dbg_89_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_89_err);}throw $pyjs_dbg_89_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})();
				$pyjs['track']['lineno']=151;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return errorDiv['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['TextNode'](txt);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_91_err){if (!$p['isinstance']($pyjs_dbg_91_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_91_err);}throw $pyjs_dbg_91_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_92_err){if (!$p['isinstance']($pyjs_dbg_92_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_92_err);}throw $pyjs_dbg_92_err;
}})();
				$pyjs['track']['lineno']=152;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](errorDiv);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_93_err){if (!$p['isinstance']($pyjs_dbg_93_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_93_err);}throw $pyjs_dbg_93_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['req', null],['code', null]]);
			$cls_definition['showErrorMsg'] = $method;
			$pyjs['track']['lineno']=154;
			$method = $pyjs__bind_method2('onNextBatchNeeded', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var filter,$and30,$and29;
				$pyjs['track']={'module':'widgets.list', 'lineno':154};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=154;
				$pyjs['track']['lineno']=158;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and29=$p['getattr'](self, '_currentCursor'))?!$p['bool']($p['getattr'](self, 'isDetaching')):$and29));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_94_err){if (!$p['isinstance']($pyjs_dbg_94_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_94_err);}throw $pyjs_dbg_94_err;
}})()) {
					$pyjs['track']['lineno']=159;
					filter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['filter']['copy']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_95_err){if (!$p['isinstance']($pyjs_dbg_95_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_95_err);}throw $pyjs_dbg_95_err;
}})();
					$pyjs['track']['lineno']=160;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return filter['__setitem__']('amount', $p['getattr'](self, '_batchSize'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_96_err){if (!$p['isinstance']($pyjs_dbg_96_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_96_err);}throw $pyjs_dbg_96_err;
}})();
					$pyjs['track']['lineno']=161;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return filter['__setitem__']('cursor', $p['getattr'](self, '_currentCursor'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_97_err){if (!$p['isinstance']($pyjs_dbg_97_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_97_err);}throw $pyjs_dbg_97_err;
}})();
					$pyjs['track']['lineno']=162;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['_currentRequests']['append']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'successHandler':$p['getattr'](self, 'onCompletion'), 'failureHandler':$p['getattr'](self, 'showErrorMsg'), 'cacheable':true}, $p['getattr'](self, 'modul'), 'list', filter]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_98_err){if (!$p['isinstance']($pyjs_dbg_98_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_98_err);}throw $pyjs_dbg_98_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_99_err){if (!$p['isinstance']($pyjs_dbg_99_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_99_err);}throw $pyjs_dbg_99_err;
}})();
					$pyjs['track']['lineno']=165;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_currentCursor', null) : $p['setattr'](self, '_currentCursor', null); 
				}
				else {
					$pyjs['track']['lineno']=167;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['table']['setDataProvider'](null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_100_err){if (!$p['isinstance']($pyjs_dbg_100_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_100_err);}throw $pyjs_dbg_100_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onNextBatchNeeded'] = $method;
			$pyjs['track']['lineno']=169;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.list', 'lineno':169};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=169;
				$pyjs['track']['lineno']=170;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ListWidget'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_101_err){if (!$p['isinstance']($pyjs_dbg_101_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_101_err);}throw $pyjs_dbg_101_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_102_err){if (!$p['isinstance']($pyjs_dbg_102_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_102_err);}throw $pyjs_dbg_102_err;
}})();
				$pyjs['track']['lineno']=171;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['registerChangeListener'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_103_err){if (!$p['isinstance']($pyjs_dbg_103_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_103_err);}throw $pyjs_dbg_103_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=173;
			$method = $pyjs__bind_method2('onDetach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.list', 'lineno':173};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=173;
				$pyjs['track']['lineno']=174;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isDetaching', true) : $p['setattr'](self, 'isDetaching', true); 
				$pyjs['track']['lineno']=175;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ListWidget'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_104_err){if (!$p['isinstance']($pyjs_dbg_104_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_104_err);}throw $pyjs_dbg_104_err;
}})()['onDetach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_105_err){if (!$p['isinstance']($pyjs_dbg_105_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_105_err);}throw $pyjs_dbg_105_err;
}})();
				$pyjs['track']['lineno']=176;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['removeChangeListener'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_106_err){if (!$p['isinstance']($pyjs_dbg_106_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_106_err);}throw $pyjs_dbg_106_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onDetach'] = $method;
			$pyjs['track']['lineno']=178;
			$method = $pyjs__bind_method2('onDataChanged', function(modul) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					modul = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and32,$and31;
				$pyjs['track']={'module':'widgets.list', 'lineno':178};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=178;
				$pyjs['track']['lineno']=182;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and31=modul)?!$p['op_eq'](modul, $p['getattr'](self, 'modul')):$and31));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_107_err){if (!$p['isinstance']($pyjs_dbg_107_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_107_err);}throw $pyjs_dbg_107_err;
}})()) {
					$pyjs['track']['lineno']=183;
					$pyjs['track']['lineno']=183;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=185;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['reloadData']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_108_err){if (!$p['isinstance']($pyjs_dbg_108_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_108_err);}throw $pyjs_dbg_108_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['modul']]);
			$cls_definition['onDataChanged'] = $method;
			$pyjs['track']['lineno']=187;
			$method = $pyjs__bind_method2('reloadData', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var filter;
				$pyjs['track']={'module':'widgets.list', 'lineno':187};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=187;
				$pyjs['track']['lineno']=191;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['table']['clear']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_109_err){if (!$p['isinstance']($pyjs_dbg_109_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_109_err);}throw $pyjs_dbg_109_err;
}})();
				$pyjs['track']['lineno']=192;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_currentCursor', null) : $p['setattr'](self, '_currentCursor', null); 
				$pyjs['track']['lineno']=193;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_currentRequests', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_110_err){if (!$p['isinstance']($pyjs_dbg_110_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_110_err);}throw $pyjs_dbg_110_err;
}})()) : $p['setattr'](self, '_currentRequests', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_110_err){if (!$p['isinstance']($pyjs_dbg_110_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_110_err);}throw $pyjs_dbg_110_err;
}})()); 
				$pyjs['track']['lineno']=194;
				filter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['filter']['copy']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_111_err){if (!$p['isinstance']($pyjs_dbg_111_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_111_err);}throw $pyjs_dbg_111_err;
}})();
				$pyjs['track']['lineno']=195;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return filter['__setitem__']('amount', $p['getattr'](self, '_batchSize'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_112_err){if (!$p['isinstance']($pyjs_dbg_112_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_112_err);}throw $pyjs_dbg_112_err;
}})();
				$pyjs['track']['lineno']=198;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_currentRequests']['append']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'successHandler':$p['getattr'](self, 'onCompletion'), 'failureHandler':$p['getattr'](self, 'showErrorMsg'), 'cacheable':true}, $p['getattr'](self, 'modul'), 'list', filter]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_113_err){if (!$p['isinstance']($pyjs_dbg_113_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_113_err);}throw $pyjs_dbg_113_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_114_err){if (!$p['isinstance']($pyjs_dbg_114_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_114_err);}throw $pyjs_dbg_114_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['reloadData'] = $method;
			$pyjs['track']['lineno']=203;
			$method = $pyjs__bind_method2('setFilter', function(filter, filterID, filterDescr) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 3)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 4, arguments['length']+1);
				} else {
					var self = arguments[0];
					filter = arguments[1];
					filterID = arguments[2];
					filterDescr = arguments[3];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 2 || arguments['length'] > 4)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 4, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof filterID == 'undefined') filterID=arguments['callee']['__args__'][4][1];
				if (typeof filterDescr == 'undefined') filterDescr=arguments['callee']['__args__'][5][1];

				$pyjs['track']={'module':'widgets.list', 'lineno':203};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=203;
				$pyjs['track']['lineno']=207;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('filter', filter) : $p['setattr'](self, 'filter', filter); 
				$pyjs['track']['lineno']=208;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('filterID', filterID) : $p['setattr'](self, 'filterID', filterID); 
				$pyjs['track']['lineno']=209;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('filterDescr', filterDescr) : $p['setattr'](self, 'filterDescr', filterDescr); 
				$pyjs['track']['lineno']=210;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['updateFilterDescription']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_115_err){if (!$p['isinstance']($pyjs_dbg_115_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_115_err);}throw $pyjs_dbg_115_err;
}})();
				$pyjs['track']['lineno']=211;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['reloadData']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_116_err){if (!$p['isinstance']($pyjs_dbg_116_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_116_err);}throw $pyjs_dbg_116_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['filter'],['filterID', null],['filterDescr', null]]);
			$cls_definition['setFilter'] = $method;
			$pyjs['track']['lineno']=213;
			$method = $pyjs__bind_method2('getFilter', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.list', 'lineno':213};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=213;
				$pyjs['track']['lineno']=214;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'filter'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_117_err){if (!$p['isinstance']($pyjs_dbg_117_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_117_err);}throw $pyjs_dbg_117_err;
}})()) {
					$pyjs['track']['lineno']=215;
					$pyjs['track']['lineno']=215;
					var $pyjs__ret = function(){
						var $iter3_idx,$iter3_nextval,$iter3_type,v,$collcomp1,$iter3_iter,$iter3_array,$pyjs__trackstack_size_1,k;
	$collcomp1 = $p['dict']();
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter3_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['filter']['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_119_err){if (!$p['isinstance']($pyjs_dbg_119_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_119_err);}throw $pyjs_dbg_119_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_120_err){if (!$p['isinstance']($pyjs_dbg_120_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_120_err);}throw $pyjs_dbg_120_err;
}})();
					$iter3_nextval=$p['__iter_prepare']($iter3_iter,false);
					while (typeof($p['__wrapped_next']($iter3_nextval)['$nextval']) != 'undefined') {
						var $tupleassign1 = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['__ass_unpack']($iter3_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_118_err){if (!$p['isinstance']($pyjs_dbg_118_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_118_err);}throw $pyjs_dbg_118_err;
}})();
						k = $tupleassign1[0];
						v = $tupleassign1[1];
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $collcomp1['__setitem__'](k, v);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_121_err){if (!$p['isinstance']($pyjs_dbg_121_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_121_err);}throw $pyjs_dbg_121_err;
}})();
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='widgets.list';

	return $collcomp1;}();
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=216;
				$pyjs['track']['lineno']=216;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_122_err){if (!$p['isinstance']($pyjs_dbg_122_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_122_err);}throw $pyjs_dbg_122_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['getFilter'] = $method;
			$pyjs['track']['lineno']=218;
			$method = $pyjs__bind_method2('onCompletion', function(req) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					req = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter5_nextval,boneInfo,$iter5_idx,key,boneName,tmpDict,data,$iter5_array,$iter4_idx,$pyjs__trackstack_size_1,$iter5_iter,$iter4_type,$iter4_nextval,$iter4_array,$iter5_type,$iter4_iter,bone;
				$pyjs['track']={'module':'widgets.list', 'lineno':218};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=218;
				$pyjs['track']['lineno']=223;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, '_currentRequests')['__contains__'](req)));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_123_err){if (!$p['isinstance']($pyjs_dbg_123_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_123_err);}throw $pyjs_dbg_123_err;
}})()) {
					$pyjs['track']['lineno']=224;
					$pyjs['track']['lineno']=224;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=225;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_currentRequests']['remove'](req);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_124_err){if (!$p['isinstance']($pyjs_dbg_124_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_124_err);}throw $pyjs_dbg_124_err;
}})();
				$pyjs['track']['lineno']=226;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['actionBar']['resetLoadingState']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_125_err){if (!$p['isinstance']($pyjs_dbg_125_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_125_err);}throw $pyjs_dbg_125_err;
}})();
				$pyjs['track']['lineno']=228;
				data = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['decode'](req);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_126_err){if (!$p['isinstance']($pyjs_dbg_126_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_126_err);}throw $pyjs_dbg_126_err;
}})();
				$pyjs['track']['lineno']=230;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](data['__getitem__']('structure'), null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_127_err){if (!$p['isinstance']($pyjs_dbg_127_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_127_err);}throw $pyjs_dbg_127_err;
}})()) {
					$pyjs['track']['lineno']=231;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return self['table']['getRowCount']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_128_err){if (!$p['isinstance']($pyjs_dbg_128_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_128_err);}throw $pyjs_dbg_128_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_129_err){if (!$p['isinstance']($pyjs_dbg_129_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_129_err);}throw $pyjs_dbg_129_err;
}})()) {
						$pyjs['track']['lineno']=232;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['table']['setDataProvider'](null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_130_err){if (!$p['isinstance']($pyjs_dbg_130_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_130_err);}throw $pyjs_dbg_130_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=234;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'table')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_131_err){if (!$p['isinstance']($pyjs_dbg_131_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_131_err);}throw $pyjs_dbg_131_err;
}})();
						$pyjs['track']['lineno']=235;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'emptyNotificationDiv')['__getitem__']('style')['__setitem__']('display', '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_132_err){if (!$p['isinstance']($pyjs_dbg_132_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_132_err);}throw $pyjs_dbg_132_err;
}})();
					}
					$pyjs['track']['lineno']=237;
					$pyjs['track']['lineno']=237;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=239;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'table')['__getitem__']('style')['__setitem__']('display', '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_133_err){if (!$p['isinstance']($pyjs_dbg_133_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_133_err);}throw $pyjs_dbg_133_err;
}})();
				$pyjs['track']['lineno']=240;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'emptyNotificationDiv')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_134_err){if (!$p['isinstance']($pyjs_dbg_134_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_134_err);}throw $pyjs_dbg_134_err;
}})();
				$pyjs['track']['lineno']=241;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_structure', data['__getitem__']('structure')) : $p['setattr'](self, '_structure', data['__getitem__']('structure')); 
				$pyjs['track']['lineno']=242;
				tmpDict = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_135_err){if (!$p['isinstance']($pyjs_dbg_135_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_135_err);}throw $pyjs_dbg_135_err;
}})();
				$pyjs['track']['lineno']=244;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter4_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return data['__getitem__']('structure');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_137_err){if (!$p['isinstance']($pyjs_dbg_137_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_137_err);}throw $pyjs_dbg_137_err;
}})();
				$iter4_nextval=$p['__iter_prepare']($iter4_iter,false);
				while (typeof($p['__wrapped_next']($iter4_nextval)['$nextval']) != 'undefined') {
					var $tupleassign2 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter4_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_136_err){if (!$p['isinstance']($pyjs_dbg_136_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_136_err);}throw $pyjs_dbg_136_err;
}})();
					key = $tupleassign2[0];
					bone = $tupleassign2[1];
					$pyjs['track']['lineno']=245;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return tmpDict['__setitem__'](key, bone);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_138_err){if (!$p['isinstance']($pyjs_dbg_138_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_138_err);}throw $pyjs_dbg_138_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=247;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, '_tableHeaderIsValid')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_139_err){if (!$p['isinstance']($pyjs_dbg_139_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_139_err);}throw $pyjs_dbg_139_err;
}})()) {
					$pyjs['track']['lineno']=248;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool']($p['getattr'](self, 'columns')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_140_err){if (!$p['isinstance']($pyjs_dbg_140_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_140_err);}throw $pyjs_dbg_140_err;
}})()) {
						$pyjs['track']['lineno']=249;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('columns', (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_141_err){if (!$p['isinstance']($pyjs_dbg_141_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_141_err);}throw $pyjs_dbg_141_err;
}})()) : $p['setattr'](self, 'columns', (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_141_err){if (!$p['isinstance']($pyjs_dbg_141_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_141_err);}throw $pyjs_dbg_141_err;
}})()); 
						$pyjs['track']['lineno']=250;
						$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
						$iter5_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
						return data['__getitem__']('structure');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_143_err){if (!$p['isinstance']($pyjs_dbg_143_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_143_err);}throw $pyjs_dbg_143_err;
}})();
						$iter5_nextval=$p['__iter_prepare']($iter5_iter,false);
						while (typeof($p['__wrapped_next']($iter5_nextval)['$nextval']) != 'undefined') {
							var $tupleassign3 = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['__ass_unpack']($iter5_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_142_err){if (!$p['isinstance']($pyjs_dbg_142_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_142_err);}throw $pyjs_dbg_142_err;
}})();
							boneName = $tupleassign3[0];
							boneInfo = $tupleassign3[1];
							$pyjs['track']['lineno']=251;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool'](boneInfo['__getitem__']('visible'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_144_err){if (!$p['isinstance']($pyjs_dbg_144_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_144_err);}throw $pyjs_dbg_144_err;
}})()) {
								$pyjs['track']['lineno']=252;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return self['columns']['append'](boneName);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_145_err){if (!$p['isinstance']($pyjs_dbg_145_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_145_err);}throw $pyjs_dbg_145_err;
}})();
							}
						}
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='widgets.list';
					}
					$pyjs['track']['lineno']=253;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['setFields']($p['getattr'](self, 'columns'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_146_err){if (!$p['isinstance']($pyjs_dbg_146_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_146_err);}throw $pyjs_dbg_146_err;
}})();
				}
				$pyjs['track']['lineno']=256;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_147_err){if (!$p['isinstance']($pyjs_dbg_147_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_147_err);}throw $pyjs_dbg_147_err;
}})()['__contains__']('cursor'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_148_err){if (!$p['isinstance']($pyjs_dbg_148_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_148_err);}throw $pyjs_dbg_148_err;
}})()) {
					$pyjs['track']['lineno']=257;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_currentCursor', data['__getitem__']('cursor')) : $p['setattr'](self, '_currentCursor', data['__getitem__']('cursor')); 
					$pyjs['track']['lineno']=258;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['table']['setDataProvider'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_149_err){if (!$p['isinstance']($pyjs_dbg_149_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_149_err);}throw $pyjs_dbg_149_err;
}})();
				}
				$pyjs['track']['lineno']=260;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['table']['extend'](data['__getitem__']('skellist'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_150_err){if (!$p['isinstance']($pyjs_dbg_150_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_150_err);}throw $pyjs_dbg_150_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['req']]);
			$cls_definition['onCompletion'] = $method;
			$pyjs['track']['lineno']=262;
			$method = $pyjs__bind_method2('setFields', function(fields) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					fields = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var boneInfo,$iter10_nextval,$iter6_type,$iter8_iter,$iter10_iter,$iter6_iter,delegateFactory,$iter6_nextval,boneInfoList,rendersDict,$iter8_idx,$iter6_idx,tmpDict,$iter8_type,$iter6_array,$iter8_nextval,key,boneName,$iter8_array,$iter10_array,$pyjs__trackstack_size_1,$iter10_type,$iter10_idx,bone;
				$pyjs['track']={'module':'widgets.list', 'lineno':262};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=262;
				$pyjs['track']['lineno']=263;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, '_structure')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_151_err){if (!$p['isinstance']($pyjs_dbg_151_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_151_err);}throw $pyjs_dbg_151_err;
}})()) {
					$pyjs['track']['lineno']=264;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_tableHeaderIsValid', false) : $p['setattr'](self, '_tableHeaderIsValid', false); 
					$pyjs['track']['lineno']=265;
					$pyjs['track']['lineno']=265;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=266;
				boneInfoList = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_152_err){if (!$p['isinstance']($pyjs_dbg_152_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_152_err);}throw $pyjs_dbg_152_err;
}})();
				$pyjs['track']['lineno']=267;
				tmpDict = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_153_err){if (!$p['isinstance']($pyjs_dbg_153_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_153_err);}throw $pyjs_dbg_153_err;
}})();
				$pyjs['track']['lineno']=268;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter6_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, '_structure');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_155_err){if (!$p['isinstance']($pyjs_dbg_155_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_155_err);}throw $pyjs_dbg_155_err;
}})();
				$iter6_nextval=$p['__iter_prepare']($iter6_iter,false);
				while (typeof($p['__wrapped_next']($iter6_nextval)['$nextval']) != 'undefined') {
					var $tupleassign4 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter6_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_154_err){if (!$p['isinstance']($pyjs_dbg_154_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_154_err);}throw $pyjs_dbg_154_err;
}})();
					key = $tupleassign4[0];
					bone = $tupleassign4[1];
					$pyjs['track']['lineno']=269;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return tmpDict['__setitem__'](key, bone);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_156_err){if (!$p['isinstance']($pyjs_dbg_156_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_156_err);}throw $pyjs_dbg_156_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=270;
				fields = function(){
					var $iter7_nextval,$iter7_iter,$iter7_array,$collcomp2,$iter7_idx,x,$iter7_type,$pyjs__trackstack_size_1;
	$collcomp2 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter7_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return fields;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_157_err){if (!$p['isinstance']($pyjs_dbg_157_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_157_err);}throw $pyjs_dbg_157_err;
}})();
				$iter7_nextval=$p['__iter_prepare']($iter7_iter,false);
				while (typeof($p['__wrapped_next']($iter7_nextval)['$nextval']) != 'undefined') {
					x = $iter7_nextval['$nextval'];
					$pyjs['track']['lineno']=270;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return tmpDict['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_158_err){if (!$p['isinstance']($pyjs_dbg_158_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_158_err);}throw $pyjs_dbg_158_err;
}})()['__contains__'](x));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_159_err){if (!$p['isinstance']($pyjs_dbg_159_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_159_err);}throw $pyjs_dbg_159_err;
}})()) {
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $collcomp2['append'](x);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_160_err){if (!$p['isinstance']($pyjs_dbg_160_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_160_err);}throw $pyjs_dbg_160_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.list';

	return $collcomp2;}();
				$pyjs['track']['lineno']=271;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('columns', fields) : $p['setattr'](self, 'columns', fields); 
				$pyjs['track']['lineno']=272;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter8_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return fields;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_161_err){if (!$p['isinstance']($pyjs_dbg_161_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_161_err);}throw $pyjs_dbg_161_err;
}})();
				$iter8_nextval=$p['__iter_prepare']($iter8_iter,false);
				while (typeof($p['__wrapped_next']($iter8_nextval)['$nextval']) != 'undefined') {
					boneName = $iter8_nextval['$nextval'];
					$pyjs['track']['lineno']=273;
					boneInfo = tmpDict['__getitem__'](boneName);
					$pyjs['track']['lineno']=274;
					delegateFactory = (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['viewDelegateSelector']['select']($p['getattr'](self, 'modul'), boneName, tmpDict);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_162_err){if (!$p['isinstance']($pyjs_dbg_162_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_162_err);}throw $pyjs_dbg_162_err;
}})()($p['getattr'](self, 'modul'), boneName, tmpDict);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_163_err){if (!$p['isinstance']($pyjs_dbg_163_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_163_err);}throw $pyjs_dbg_163_err;
}})();
					$pyjs['track']['lineno']=275;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['table']['setCellRender'](boneName, delegateFactory);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_164_err){if (!$p['isinstance']($pyjs_dbg_164_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_164_err);}throw $pyjs_dbg_164_err;
}})();
					$pyjs['track']['lineno']=276;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return boneInfoList['append'](boneInfo);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_165_err){if (!$p['isinstance']($pyjs_dbg_165_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_165_err);}throw $pyjs_dbg_165_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=277;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['table']['setHeader'](function(){
					var $iter9_iter,$collcomp3,$iter9_nextval,$iter9_idx,$iter9_type,$pyjs__trackstack_size_1,x,$iter9_array;
	$collcomp3 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter9_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return boneInfoList;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_166_err){if (!$p['isinstance']($pyjs_dbg_166_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_166_err);}throw $pyjs_dbg_166_err;
}})();
				$iter9_nextval=$p['__iter_prepare']($iter9_iter,false);
				while (typeof($p['__wrapped_next']($iter9_nextval)['$nextval']) != 'undefined') {
					x = $iter9_nextval['$nextval'];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp3['append'](x['__getitem__']('descr'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_167_err){if (!$p['isinstance']($pyjs_dbg_167_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_167_err);}throw $pyjs_dbg_167_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.list';

	return $collcomp3;}());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_168_err){if (!$p['isinstance']($pyjs_dbg_168_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_168_err);}throw $pyjs_dbg_168_err;
}})();
				$pyjs['track']['lineno']=278;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['table']['setShownFields'](fields);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_169_err){if (!$p['isinstance']($pyjs_dbg_169_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_169_err);}throw $pyjs_dbg_169_err;
}})();
				$pyjs['track']['lineno']=279;
				rendersDict = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_170_err){if (!$p['isinstance']($pyjs_dbg_170_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_170_err);}throw $pyjs_dbg_170_err;
}})();
				$pyjs['track']['lineno']=280;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter10_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return fields;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_171_err){if (!$p['isinstance']($pyjs_dbg_171_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_171_err);}throw $pyjs_dbg_171_err;
}})();
				$iter10_nextval=$p['__iter_prepare']($iter10_iter,false);
				while (typeof($p['__wrapped_next']($iter10_nextval)['$nextval']) != 'undefined') {
					boneName = $iter10_nextval['$nextval'];
					$pyjs['track']['lineno']=281;
					boneInfo = tmpDict['__getitem__'](boneName);
					$pyjs['track']['lineno']=282;
					delegateFactory = (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['viewDelegateSelector']['select']($p['getattr'](self, 'modul'), boneName, tmpDict);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_172_err){if (!$p['isinstance']($pyjs_dbg_172_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_172_err);}throw $pyjs_dbg_172_err;
}})()($p['getattr'](self, 'modul'), boneName, tmpDict);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_173_err){if (!$p['isinstance']($pyjs_dbg_173_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_173_err);}throw $pyjs_dbg_173_err;
}})();
					$pyjs['track']['lineno']=283;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return rendersDict['__setitem__'](boneName, delegateFactory);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_174_err){if (!$p['isinstance']($pyjs_dbg_174_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_174_err);}throw $pyjs_dbg_174_err;
}})();
					$pyjs['track']['lineno']=284;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return boneInfoList['append'](boneInfo);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_175_err){if (!$p['isinstance']($pyjs_dbg_175_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_175_err);}throw $pyjs_dbg_175_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=285;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['table']['setCellRenders'](rendersDict);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_176_err){if (!$p['isinstance']($pyjs_dbg_176_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_176_err);}throw $pyjs_dbg_176_err;
}})();
				$pyjs['track']['lineno']=286;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_tableHeaderIsValid', true) : $p['setattr'](self, '_tableHeaderIsValid', true); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['fields']]);
			$cls_definition['setFields'] = $method;
			$pyjs['track']['lineno']=288;
			$method = $pyjs__bind_method2('getFields', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.list', 'lineno':288};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=288;
				$pyjs['track']['lineno']=289;
				$pyjs['track']['lineno']=289;
				var $pyjs__ret = $p['__getslice']($p['getattr'](self, 'columns'), 0, null);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['getFields'] = $method;
			$pyjs['track']['lineno']=291;
			$method = $pyjs__bind_method2('onSelectionActivated', function(table, selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					selection = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.list', 'lineno':291};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=291;
				$pyjs['track']['lineno']=292;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['removeWidget'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_177_err){if (!$p['isinstance']($pyjs_dbg_177_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_177_err);}throw $pyjs_dbg_177_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['selection']]);
			$cls_definition['onSelectionActivated'] = $method;
			$pyjs['track']['lineno']=294;
			$method = $pyjs__bind_method2('activateCurrentSelection', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8a614eb21d351b18507e25b93f95b5ac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.list', 'lineno':294};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.list';
				$pyjs['track']['lineno']=294;
				$pyjs['track']['lineno']=299;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['table']['activateCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_178_err){if (!$p['isinstance']($pyjs_dbg_178_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_178_err);}throw $pyjs_dbg_178_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['activateCurrentSelection'] = $method;
			$pyjs['track']['lineno']=14;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('ListWidget', $p['tuple']($bases), $data);
		})();
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end widgets.list */


/* end module: widgets.list */


/*
PYJS_DEPS: ['pyjd', 'json', 'config.conf', 'config', 'network.NetworkService', 'network', 'priorityqueue.viewDelegateSelector', 'priorityqueue', 'widgets.table.DataTable', 'widgets', 'widgets.table', 'widgets.actionbar.ActionBar', 'widgets.actionbar', 'widgets.sidebar.SideBar', 'widgets.sidebar', 'html5', 'sidebarwidgets.filterselector.CompoundFilter', 'sidebarwidgets', 'sidebarwidgets.filterselector', 'i18n.translate', 'i18n']
*/
