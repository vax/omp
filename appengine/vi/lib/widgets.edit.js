/* start module: widgets.edit */
$pyjs['loaded_modules']['widgets.edit'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['widgets.edit']['__was_initialized__']) return $pyjs['loaded_modules']['widgets.edit'];
	if(typeof $pyjs['loaded_modules']['widgets'] == 'undefined' || !$pyjs['loaded_modules']['widgets']['__was_initialized__']) $p['___import___']('widgets', null);
	var $m = $pyjs['loaded_modules']['widgets.edit'];
	$m['__repr__'] = function() { return '<module: widgets.edit>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'widgets.edit';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	$pyjs['loaded_modules']['widgets']['edit'] = $pyjs['loaded_modules']['widgets.edit'];
	try {
		$m.__track_lines__[1] = 'widgets.edit.py, line 1:\n    # -*- coding: utf-8 -*-';
		$m.__track_lines__[3] = 'widgets.edit.py, line 3:\n    import html5';
		$m.__track_lines__[4] = 'widgets.edit.py, line 4:\n    from html5.a import A';
		$m.__track_lines__[5] = 'widgets.edit.py, line 5:\n    from html5.form import Fieldset';
		$m.__track_lines__[6] = 'widgets.edit.py, line 6:\n    from html5.ext import YesNoDialog';
		$m.__track_lines__[7] = 'widgets.edit.py, line 7:\n    from network import NetworkService';
		$m.__track_lines__[8] = 'widgets.edit.py, line 8:\n    from config import conf';
		$m.__track_lines__[9] = 'widgets.edit.py, line 9:\n    from priorityqueue import editBoneSelector';
		$m.__track_lines__[10] = 'widgets.edit.py, line 10:\n    from widgets.tooltip import ToolTip';
		$m.__track_lines__[11] = 'widgets.edit.py, line 11:\n    from priorityqueue import protocolWrapperInstanceSelector';
		$m.__track_lines__[12] = 'widgets.edit.py, line 12:\n    from widgets.actionbar import ActionBar';
		$m.__track_lines__[13] = 'widgets.edit.py, line 13:\n    import utils';
		$m.__track_lines__[14] = 'widgets.edit.py, line 14:\n    from i18n import translate';
		$m.__track_lines__[17] = 'widgets.edit.py, line 17:\n    class InvalidBoneValueException(ValueError):';
		$m.__track_lines__[18] = 'widgets.edit.py, line 18:\n    pass';
		$m.__track_lines__[20] = 'widgets.edit.py, line 20:\n    class InternalEdit( html5.Div ):';
		$m.__track_lines__[22] = 'widgets.edit.py, line 22:\n    def __init__(self, skelStructure, values=None, errorInformation=None, readOnly=False):';
		$m.__track_lines__[23] = 'widgets.edit.py, line 23:\n    super( InternalEdit, self ).__init__()';
		$m.__track_lines__[24] = 'widgets.edit.py, line 24:\n    self.editIdx = 1';
		$m.__track_lines__[25] = 'widgets.edit.py, line 25:\n    self.skelStructure = skelStructure';
		$m.__track_lines__[26] = 'widgets.edit.py, line 26:\n    self.values = values';
		$m.__track_lines__[27] = 'widgets.edit.py, line 27:\n    self.errorInformation = errorInformation';
		$m.__track_lines__[28] = 'widgets.edit.py, line 28:\n    self.form = html5.Form()';
		$m.__track_lines__[29] = 'widgets.edit.py, line 29:\n    self.appendChild(self.form)';
		$m.__track_lines__[30] = 'widgets.edit.py, line 30:\n    self.renderStructure(readOnly=readOnly)';
		$m.__track_lines__[31] = 'widgets.edit.py, line 31:\n    if values:';
		$m.__track_lines__[32] = 'widgets.edit.py, line 32:\n    self.unserialize( values )';
		$m.__track_lines__[35] = 'widgets.edit.py, line 35:\n    def renderStructure(self, readOnly = False):';
		$m.__track_lines__[38] = 'widgets.edit.py, line 38:\n    self.bones = {}';
		$m.__track_lines__[41] = 'widgets.edit.py, line 41:\n    tmpDict = utils.boneListToDict( self.skelStructure )';
		$m.__track_lines__[42] = 'widgets.edit.py, line 42:\n    fieldSets = {}';
		$m.__track_lines__[43] = 'widgets.edit.py, line 43:\n    currRow = 0';
		$m.__track_lines__[44] = 'widgets.edit.py, line 44:\n    hasMissing = False';
		$m.__track_lines__[45] = 'widgets.edit.py, line 45:\n    for key, bone in self.skelStructure:';
		$m.__track_lines__[48] = 'widgets.edit.py, line 48:\n    if not bone["visible"]:';
		$m.__track_lines__[49] = 'widgets.edit.py, line 49:\n    continue';
		$m.__track_lines__[52] = 'widgets.edit.py, line 52:\n    if readOnly:';
		$m.__track_lines__[53] = 'widgets.edit.py, line 53:\n    tmpDict[key]["readonly"] = True';
		$m.__track_lines__[55] = 'widgets.edit.py, line 55:\n    cat = "default"';
		$m.__track_lines__[57] = 'widgets.edit.py, line 57:\n    if ("params" in bone.keys()';
		$m.__track_lines__[60] = 'widgets.edit.py, line 60:\n    cat = bone["params"]["category"]';
		$m.__track_lines__[62] = 'widgets.edit.py, line 62:\n    if not cat in fieldSets.keys():';
		$m.__track_lines__[63] = 'widgets.edit.py, line 63:\n    fs = html5.Fieldset()';
		$m.__track_lines__[64] = 'widgets.edit.py, line 64:\n    fs["class"] = cat';
		$m.__track_lines__[65] = 'widgets.edit.py, line 65:\n    if cat=="default":';
		$m.__track_lines__[66] = 'widgets.edit.py, line 66:\n    fs["class"].append("active")';
		$m.__track_lines__[68] = 'widgets.edit.py, line 68:\n    fs["name"] = cat';
		$m.__track_lines__[69] = 'widgets.edit.py, line 69:\n    legend = html5.Legend()';
		$m.__track_lines__[71] = 'widgets.edit.py, line 71:\n    fshref = fieldset_A()';
		$m.__track_lines__[73] = 'widgets.edit.py, line 73:\n    fshref.appendChild(html5.TextNode(cat) )';
		$m.__track_lines__[74] = 'widgets.edit.py, line 74:\n    legend.appendChild( fshref )';
		$m.__track_lines__[75] = 'widgets.edit.py, line 75:\n    fs.appendChild(legend)';
		$m.__track_lines__[76] = 'widgets.edit.py, line 76:\n    section = html5.Section()';
		$m.__track_lines__[77] = 'widgets.edit.py, line 77:\n    fs.appendChild(section)';
		$m.__track_lines__[78] = 'widgets.edit.py, line 78:\n    fs._section = section';
		$m.__track_lines__[79] = 'widgets.edit.py, line 79:\n    fieldSets[ cat ] = fs';
		$m.__track_lines__[81] = 'widgets.edit.py, line 81:\n    if "params" in bone.keys() and bone["params"] and "category" in bone["params"].keys():';
		$m.__track_lines__[82] = 'widgets.edit.py, line 82:\n    tabName = bone["params"]["category"]';
		$m.__track_lines__[84] = 'widgets.edit.py, line 84:\n    tabName = "Test"';
		$m.__track_lines__[86] = 'widgets.edit.py, line 86:\n    wdgGen = editBoneSelector.select( None, key, tmpDict )';
		$m.__track_lines__[87] = 'widgets.edit.py, line 87:\n    widget = wdgGen.fromSkelStructure( None, key, tmpDict )';
		$m.__track_lines__[88] = 'widgets.edit.py, line 88:\n    widget["id"] = "vi_%s_%s_%s_%s_bn_%s" % (self.editIdx, None, "internal", cat, key)';
		$m.__track_lines__[93] = 'widgets.edit.py, line 93:\n    descrLbl = html5.Label(bone["descr"])';
		$m.__track_lines__[94] = 'widgets.edit.py, line 94:\n    descrLbl["class"].append(key)';
		$m.__track_lines__[95] = 'widgets.edit.py, line 95:\n    descrLbl["class"].append(bone["type"].replace(".","_"))';
		$m.__track_lines__[96] = 'widgets.edit.py, line 96:\n    descrLbl["for"] = "vi_%s_%s_%s_%s_bn_%s" % ( self.editIdx, None, "internal", cat, key)';
		$m.__track_lines__[98] = 'widgets.edit.py, line 98:\n    if bone["required"]:';
		$m.__track_lines__[99] = 'widgets.edit.py, line 99:\n    descrLbl["class"].append("is_required")';
		$m.__track_lines__[101] = 'widgets.edit.py, line 101:\n    if (bone["required"]';
		$m.__track_lines__[104] = 'widgets.edit.py, line 104:\n    descrLbl["class"].append("is_invalid")';
		$m.__track_lines__[105] = 'widgets.edit.py, line 105:\n    if bone["error"]:';
		$m.__track_lines__[106] = 'widgets.edit.py, line 106:\n    descrLbl["title"] = bone["error"]';
		$m.__track_lines__[108] = 'widgets.edit.py, line 108:\n    descrLbl["title"] = self.errorInformation[ key ]';
		$m.__track_lines__[109] = 'widgets.edit.py, line 109:\n    fieldSets[ cat ]["class"].append("is_incomplete")';
		$m.__track_lines__[110] = 'widgets.edit.py, line 110:\n    hasMissing = True';
		$m.__track_lines__[112] = 'widgets.edit.py, line 112:\n    if bone["required"] and not (bone["error"] is not None or (self.errorInformation and key in self.errorInformation.keys())):';
		$m.__track_lines__[113] = 'widgets.edit.py, line 113:\n    descrLbl["class"].append("is_valid")';
		$m.__track_lines__[115] = 'widgets.edit.py, line 115:\n    if "params" in bone.keys() and isinstance(bone["params"], dict) and "tooltip" in bone["params"].keys():';
		$m.__track_lines__[116] = 'widgets.edit.py, line 116:\n    tmp = html5.Span()';
		$m.__track_lines__[117] = 'widgets.edit.py, line 117:\n    tmp.appendChild(descrLbl)';
		$m.__track_lines__[118] = 'widgets.edit.py, line 118:\n    tmp.appendChild( ToolTip(longText=bone["params"]["tooltip"]) )';
		$m.__track_lines__[119] = 'widgets.edit.py, line 119:\n    descrLbl = tmp';
		$m.__track_lines__[121] = 'widgets.edit.py, line 121:\n    containerDiv = html5.Div()';
		$m.__track_lines__[122] = 'widgets.edit.py, line 122:\n    containerDiv.appendChild( descrLbl )';
		$m.__track_lines__[123] = 'widgets.edit.py, line 123:\n    containerDiv.appendChild( widget )';
		$m.__track_lines__[124] = 'widgets.edit.py, line 124:\n    fieldSets[ cat ]._section.appendChild( containerDiv )';
		$m.__track_lines__[126] = 'widgets.edit.py, line 126:\n    containerDiv["class"].append("bone")';
		$m.__track_lines__[127] = 'widgets.edit.py, line 127:\n    containerDiv["class"].append("bone_"+key)';
		$m.__track_lines__[128] = 'widgets.edit.py, line 128:\n    containerDiv["class"].append( bone["type"].replace(".","_") )';
		$m.__track_lines__[130] = 'widgets.edit.py, line 130:\n    if "." in bone["type"]:';
		$m.__track_lines__[131] = 'widgets.edit.py, line 131:\n    for t in bone["type"].split("."):';
		$m.__track_lines__[132] = 'widgets.edit.py, line 132:\n    containerDiv["class"].append(t)';
		$m.__track_lines__[137] = 'widgets.edit.py, line 137:\n    currRow += 1';
		$m.__track_lines__[138] = 'widgets.edit.py, line 138:\n    self.bones[ key ] = widget';
		$m.__track_lines__[140] = 'widgets.edit.py, line 140:\n    if len(fieldSets)==1:';
		$m.__track_lines__[141] = 'widgets.edit.py, line 141:\n    for (k,v) in fieldSets.items():';
		$m.__track_lines__[142] = 'widgets.edit.py, line 142:\n    if not "active" in v["class"]:';
		$m.__track_lines__[143] = 'widgets.edit.py, line 143:\n    v["class"].append("active")';
		$m.__track_lines__[145] = 'widgets.edit.py, line 145:\n    tmpList = [(k,v) for (k,v) in fieldSets.items()]';
		$m.__track_lines__[146] = 'widgets.edit.py, line 146:\n    tmpList.sort( key=lambda x:x[0])';
		$m.__track_lines__[148] = 'widgets.edit.py, line 148:\n    for k,v in tmpList:';
		$m.__track_lines__[149] = 'widgets.edit.py, line 149:\n    self.form.appendChild( v )';
		$m.__track_lines__[150] = 'widgets.edit.py, line 150:\n    v._section = None';
		$m.__track_lines__[152] = 'widgets.edit.py, line 152:\n    def doSave( self, closeOnSuccess=False, *args, **kwargs ):';
		$m.__track_lines__[156] = 'widgets.edit.py, line 156:\n    self.closeOnSuccess = closeOnSuccess';
		$m.__track_lines__[157] = 'widgets.edit.py, line 157:\n    res = {}';
		$m.__track_lines__[158] = 'widgets.edit.py, line 158:\n    for key, bone in self.bones.items():';
		$m.__track_lines__[159] = 'widgets.edit.py, line 159:\n    try:';
		$m.__track_lines__[160] = 'widgets.edit.py, line 160:\n    res.update( bone.serializeForPost( ) )';
		$m.__track_lines__[163] = 'widgets.edit.py, line 163:\n    lbl = bone.parent()._children[0]';
		$m.__track_lines__[164] = 'widgets.edit.py, line 164:\n    if "is_valid" in lbl["class"]:';
		$m.__track_lines__[165] = 'widgets.edit.py, line 165:\n    lbl["class"].remove("is_valid")';
		$m.__track_lines__[166] = 'widgets.edit.py, line 166:\n    lbl["class"].append("is_invalid")';
		$m.__track_lines__[167] = 'widgets.edit.py, line 167:\n    self.actionbar.resetLoadingState()';
		$m.__track_lines__[168] = 'widgets.edit.py, line 168:\n    return';
		$m.__track_lines__[169] = 'widgets.edit.py, line 169:\n    return( res )';
		$m.__track_lines__[171] = 'widgets.edit.py, line 171:\n    def unserialize(self, data):';
		$m.__track_lines__[175] = 'widgets.edit.py, line 175:\n    for bone in self.bones.values():';
		$m.__track_lines__[176] = 'widgets.edit.py, line 176:\n    bone.unserialize( data )';
		$m.__track_lines__[178] = 'widgets.edit.py, line 178:\n    def parseHashParameters( src, prefix="" ):';
		$m.__track_lines__[190] = 'widgets.edit.py, line 190:\n    res = {}';
		$m.__track_lines__[191] = 'widgets.edit.py, line 191:\n    processedPrefixes = [] #Dont process a prefix twice';
		$m.__track_lines__[192] = 'widgets.edit.py, line 192:\n    for k,v in src.items():';
		$m.__track_lines__[193] = 'widgets.edit.py, line 193:\n    if prefix and not k.startswith( prefix ):';
		$m.__track_lines__[194] = 'widgets.edit.py, line 194:\n    continue';
		$m.__track_lines__[195] = 'widgets.edit.py, line 195:\n    if prefix:';
		$m.__track_lines__[196] = 'widgets.edit.py, line 196:\n    k = k.replace(prefix,"")';
		$m.__track_lines__[197] = 'widgets.edit.py, line 197:\n    if not "." in k:';
		$m.__track_lines__[198] = 'widgets.edit.py, line 198:\n    if k in res.keys():';
		$m.__track_lines__[199] = 'widgets.edit.py, line 199:\n    if not isinstance( res[k], list ):';
		$m.__track_lines__[200] = 'widgets.edit.py, line 200:\n    res[k] = [ res[k] ]';
		$m.__track_lines__[201] = 'widgets.edit.py, line 201:\n    res[k].append( v )';
		$m.__track_lines__[203] = 'widgets.edit.py, line 203:\n    res[ k ] = v';
		$m.__track_lines__[205] = 'widgets.edit.py, line 205:\n    newPrefix = k[ :k.find(".")  ]';
		$m.__track_lines__[206] = 'widgets.edit.py, line 206:\n    if newPrefix in processedPrefixes: #We did this already';
		$m.__track_lines__[207] = 'widgets.edit.py, line 207:\n    continue';
		$m.__track_lines__[208] = 'widgets.edit.py, line 208:\n    processedPrefixes.append( newPrefix )';
		$m.__track_lines__[209] = 'widgets.edit.py, line 209:\n    if newPrefix in res.keys():';
		$m.__track_lines__[210] = 'widgets.edit.py, line 210:\n    if not isinstance( res[ newPrefix ], list ):';
		$m.__track_lines__[211] = 'widgets.edit.py, line 211:\n    res[ newPrefix ] = [ res[ newPrefix ] ]';
		$m.__track_lines__[212] = 'widgets.edit.py, line 212:\n    res[ newPrefix ].append( parseHashParameters( src, prefix="%s%s." %(prefix,newPrefix)) )';
		$m.__track_lines__[214] = 'widgets.edit.py, line 214:\n    res[ newPrefix ] = parseHashParameters( src, prefix="%s%s." %(prefix,newPrefix))';
		$m.__track_lines__[215] = 'widgets.edit.py, line 215:\n    if all( [x.isdigit() for x in res.keys()]):';
		$m.__track_lines__[216] = 'widgets.edit.py, line 216:\n    newRes = []';
		$m.__track_lines__[217] = 'widgets.edit.py, line 217:\n    keys = [int(x) for x in res.keys()]';
		$m.__track_lines__[218] = 'widgets.edit.py, line 218:\n    keys.sort()';
		$m.__track_lines__[219] = 'widgets.edit.py, line 219:\n    for k in keys:';
		$m.__track_lines__[220] = 'widgets.edit.py, line 220:\n    newRes.append( res[str(k)] )';
		$m.__track_lines__[221] = 'widgets.edit.py, line 221:\n    return( newRes )';
		$m.__track_lines__[222] = 'widgets.edit.py, line 222:\n    return( res )';
		$m.__track_lines__[225] = 'widgets.edit.py, line 225:\n    class EditWidget( html5.Div ):';
		$m.__track_lines__[226] = 'widgets.edit.py, line 226:\n    appList = "list"';
		$m.__track_lines__[227] = 'widgets.edit.py, line 227:\n    appHierarchy = "hierarchy"';
		$m.__track_lines__[228] = 'widgets.edit.py, line 228:\n    appTree = "tree"';
		$m.__track_lines__[229] = 'widgets.edit.py, line 229:\n    appSingleton = "singleton"';
		$m.__track_lines__[230] = 'widgets.edit.py, line 230:\n    __editIdx_ = 0 #Internal counter to ensure unique ids';
		$m.__track_lines__[232] = 'widgets.edit.py, line 232:\n    def __init__(self, modul, applicationType, key=0, node=None, skelType=None, clone=False,';
		$m.__track_lines__[252] = 'widgets.edit.py, line 252:\n    super( EditWidget, self ).__init__( *args, **kwargs )';
		$m.__track_lines__[253] = 'widgets.edit.py, line 253:\n    self.modul = modul';
		$m.__track_lines__[255] = 'widgets.edit.py, line 255:\n    assert applicationType in [ EditWidget.appList, EditWidget.appHierarchy, EditWidget.appTree, EditWidget.appSingleton ] #Invalid Application-Type?';
		$m.__track_lines__[257] = 'widgets.edit.py, line 257:\n    if applicationType==EditWidget.appHierarchy or applicationType==EditWidget.appTree:';
		$m.__track_lines__[258] = 'widgets.edit.py, line 258:\n    assert id is not None or node is not None #Need either an id or an node';
		$m.__track_lines__[260] = 'widgets.edit.py, line 260:\n    if clone:';
		$m.__track_lines__[261] = 'widgets.edit.py, line 261:\n    assert id is not None #Need an id if we should clone an entry';
		$m.__track_lines__[262] = 'widgets.edit.py, line 262:\n    assert not applicationType==EditWidget.appSingleton # We cant clone a singleton';
		$m.__track_lines__[263] = 'widgets.edit.py, line 263:\n    if applicationType==EditWidget.appHierarchy or applicationType==EditWidget.appTree:';
		$m.__track_lines__[264] = 'widgets.edit.py, line 264:\n    assert node is not None #We still need a rootNode for cloning';
		$m.__track_lines__[265] = 'widgets.edit.py, line 265:\n    if applicationType==EditWidget.appTree:';
		$m.__track_lines__[266] = 'widgets.edit.py, line 266:\n    assert node is not None #We still need a path for cloning #FIXME';
		$m.__track_lines__[268] = 'widgets.edit.py, line 268:\n    self.clone_of = key';
		$m.__track_lines__[270] = 'widgets.edit.py, line 270:\n    self.clone_of = None';
		$m.__track_lines__[273] = 'widgets.edit.py, line 273:\n    self.editIdx = EditWidget.__editIdx_ #Iternal counter to ensure unique ids';
		$m.__track_lines__[274] = 'widgets.edit.py, line 274:\n    EditWidget.__editIdx_ += 1';
		$m.__track_lines__[275] = 'widgets.edit.py, line 275:\n    self.applicationType = applicationType';
		$m.__track_lines__[276] = 'widgets.edit.py, line 276:\n    self.key = key';
		$m.__track_lines__[277] = 'widgets.edit.py, line 277:\n    self.node = node';
		$m.__track_lines__[278] = 'widgets.edit.py, line 278:\n    self.skelType = skelType';
		$m.__track_lines__[279] = 'widgets.edit.py, line 279:\n    self.clone = clone';
		$m.__track_lines__[280] = 'widgets.edit.py, line 280:\n    self.bones = {}';
		$m.__track_lines__[281] = 'widgets.edit.py, line 281:\n    self.closeOnSuccess = False';
		$m.__track_lines__[282] = 'widgets.edit.py, line 282:\n    self.logaction = logaction';
		$m.__track_lines__[284] = 'widgets.edit.py, line 284:\n    self._lastData = {} #Dict of structure and values received';
		$m.__track_lines__[285] = 'widgets.edit.py, line 285:\n    if hashArgs:';
		$m.__track_lines__[286] = 'widgets.edit.py, line 286:\n    self._hashArgs = parseHashParameters( hashArgs )';
		$m.__track_lines__[287] = 'widgets.edit.py, line 287:\n    warningNode = html5.TextNode("Warning: Values shown below got overriden by the Link you clicked on and do NOT represent the actual values!\\n"+\\';
		$m.__track_lines__[289] = 'widgets.edit.py, line 289:\n    warningSpan = html5.Span()';
		$m.__track_lines__[290] = 'widgets.edit.py, line 290:\n    warningSpan["class"].append("warning")';
		$m.__track_lines__[291] = 'widgets.edit.py, line 291:\n    warningSpan.appendChild( warningNode )';
		$m.__track_lines__[292] = 'widgets.edit.py, line 292:\n    self.appendChild( warningSpan )';
		$m.__track_lines__[294] = 'widgets.edit.py, line 294:\n    self._hashArgs = None';
		$m.__track_lines__[296] = 'widgets.edit.py, line 296:\n    self.editTaskID = None';
		$m.__track_lines__[297] = 'widgets.edit.py, line 297:\n    self.wasInitialRequest = True #Wherever the last request attempted to save data or just fetched the form';
		$m.__track_lines__[298] = 'widgets.edit.py, line 298:\n    self.actionbar = ActionBar( self.modul, self.applicationType, "edit" if self.key else "add" )';
		$m.__track_lines__[299] = 'widgets.edit.py, line 299:\n    self.appendChild( self.actionbar )';
		$m.__track_lines__[300] = 'widgets.edit.py, line 300:\n    self.form = html5.Form()';
		$m.__track_lines__[301] = 'widgets.edit.py, line 301:\n    self.appendChild(self.form)';
		$m.__track_lines__[302] = 'widgets.edit.py, line 302:\n    self.actionbar.setActions(["save.close","save.continue","reset"])';
		$m.__track_lines__[303] = 'widgets.edit.py, line 303:\n    self.reloadData()';
		$m.__track_lines__[305] = 'widgets.edit.py, line 305:\n    def showErrorMsg(self, req=None, code=None):';
		$m.__track_lines__[309] = 'widgets.edit.py, line 309:\n    self.actionbar["style"]["display"] = "none"';
		$m.__track_lines__[310] = 'widgets.edit.py, line 310:\n    self.form["style"]["display"] = "none"';
		$m.__track_lines__[311] = 'widgets.edit.py, line 311:\n    errorDiv = html5.Div()';
		$m.__track_lines__[312] = 'widgets.edit.py, line 312:\n    errorDiv["class"].append("error_msg")';
		$m.__track_lines__[313] = 'widgets.edit.py, line 313:\n    if code and (code==401 or code==403):';
		$m.__track_lines__[314] = 'widgets.edit.py, line 314:\n    txt = translate("Access denied!")';
		$m.__track_lines__[316] = 'widgets.edit.py, line 316:\n    txt = translate("An unknown error occurred!")';
		$m.__track_lines__[317] = 'widgets.edit.py, line 317:\n    errorDiv["class"].append("error_code_%s" % (code or 0))';
		$m.__track_lines__[318] = 'widgets.edit.py, line 318:\n    errorDiv.appendChild( html5.TextNode( txt ) )';
		$m.__track_lines__[319] = 'widgets.edit.py, line 319:\n    self.appendChild( errorDiv )';
		$m.__track_lines__[321] = 'widgets.edit.py, line 321:\n    def reloadData(self):';
		$m.__track_lines__[322] = 'widgets.edit.py, line 322:\n    self.save( {} )';
		$m.__track_lines__[323] = 'widgets.edit.py, line 323:\n    return';
		$m.__track_lines__[325] = 'widgets.edit.py, line 325:\n    def save(self, data ):';
		$m.__track_lines__[334] = 'widgets.edit.py, line 334:\n    self.wasInitialRequest = not len(data)>0';
		$m.__track_lines__[335] = 'widgets.edit.py, line 335:\n    if self.modul=="_tasks":';
		$m.__track_lines__[336] = 'widgets.edit.py, line 336:\n    NetworkService.request( None, "/admin/%s/execute/%s" % ( self.modul, self.key ), data,';
		$m.__track_lines__[341] = 'widgets.edit.py, line 341:\n    if self.key and (not self.clone or not data):';
		$m.__track_lines__[342] = 'widgets.edit.py, line 342:\n    NetworkService.request(self.modul,"edit/%s" % self.key, data,';
		$m.__track_lines__[347] = 'widgets.edit.py, line 347:\n    NetworkService.request(self.modul, "add", data,';
		$m.__track_lines__[352] = 'widgets.edit.py, line 352:\n    if self.key and (not self.clone or not data):';
		$m.__track_lines__[353] = 'widgets.edit.py, line 353:\n    NetworkService.request(self.modul,"edit/%s" % self.key, data,';
		$m.__track_lines__[358] = 'widgets.edit.py, line 358:\n    NetworkService.request(self.modul, "add/%s" % self.node, data,';
		$m.__track_lines__[363] = 'widgets.edit.py, line 363:\n    if self.key and not self.clone:';
		$m.__track_lines__[364] = 'widgets.edit.py, line 364:\n    NetworkService.request(self.modul,"edit/%s/%s" % (self.skelType,self.key), data,';
		$m.__track_lines__[369] = 'widgets.edit.py, line 369:\n    NetworkService.request(self.modul,"add/%s/%s" % (self.skelType,self.node), data,';
		$m.__track_lines__[374] = 'widgets.edit.py, line 374:\n    NetworkService.request(self.modul,"edit", data,';
		$m.__track_lines__[379] = 'widgets.edit.py, line 379:\n    raise NotImplementedError() #Should never reach this';
		$m.__track_lines__[381] = 'widgets.edit.py, line 381:\n    def clear(self):';
		$m.__track_lines__[385] = 'widgets.edit.py, line 385:\n    for c in self.form._children[ : ]:';
		$m.__track_lines__[386] = 'widgets.edit.py, line 386:\n    self.form.removeChild( c )';
		$m.__track_lines__[388] = 'widgets.edit.py, line 388:\n    def closeOrContinue(self, sender=None ):';
		$m.__track_lines__[389] = 'widgets.edit.py, line 389:\n    if self.closeOnSuccess:';
		$m.__track_lines__[390] = 'widgets.edit.py, line 390:\n    if self.modul == "_tasks":';
		$m.__track_lines__[391] = 'widgets.edit.py, line 391:\n    self.parent().close()';
		$m.__track_lines__[392] = 'widgets.edit.py, line 392:\n    return';
		$m.__track_lines__[394] = 'widgets.edit.py, line 394:\n    conf["mainWindow"].removeWidget( self )';
		$m.__track_lines__[395] = 'widgets.edit.py, line 395:\n    NetworkService.notifyChange(self.modul)';
		$m.__track_lines__[396] = 'widgets.edit.py, line 396:\n    return';
		$m.__track_lines__[398] = 'widgets.edit.py, line 398:\n    self.clear()';
		$m.__track_lines__[399] = 'widgets.edit.py, line 399:\n    self.bones = {}';
		$m.__track_lines__[400] = 'widgets.edit.py, line 400:\n    self.reloadData()';
		$m.__track_lines__[402] = 'widgets.edit.py, line 402:\n    def doCloneHierarchy(self, sender=None ):';
		$m.__track_lines__[403] = 'widgets.edit.py, line 403:\n    if self.applicationType == EditWidget.appHierarchy:';
		$m.__track_lines__[404] = 'widgets.edit.py, line 404:\n    NetworkService.request( self.modul, "clone",';
		$m.__track_lines__[409] = 'widgets.edit.py, line 409:\n    NetworkService.request( conf[ "modules" ][ self.modul ][ "rootNodeOf" ], "clone",';
		$m.__track_lines__[413] = 'widgets.edit.py, line 413:\n    def cloneComplete(self, request):';
		$m.__track_lines__[414] = 'widgets.edit.py, line 414:\n    logDiv = html5.Div()';
		$m.__track_lines__[415] = 'widgets.edit.py, line 415:\n    logDiv["class"].append("msg")';
		$m.__track_lines__[416] = 'widgets.edit.py, line 416:\n    spanMsg = html5.Span()';
		$m.__track_lines__[417] = 'widgets.edit.py, line 417:\n    spanMsg.appendChild( html5.TextNode( translate( u"The hierarchy will be cloned in the background." ) ) )';
		$m.__track_lines__[418] = 'widgets.edit.py, line 418:\n    spanMsg["class"].append("msgspan")';
		$m.__track_lines__[419] = 'widgets.edit.py, line 419:\n    logDiv.appendChild(spanMsg)';
		$m.__track_lines__[421] = 'widgets.edit.py, line 421:\n    conf["mainWindow"].log("success",logDiv)';
		$m.__track_lines__[422] = 'widgets.edit.py, line 422:\n    self.closeOrContinue()';
		$m.__track_lines__[424] = 'widgets.edit.py, line 424:\n    def setData(self, request=None, data=None, ignoreMissing=False, askHierarchyCloning=True):';
		$m.__track_lines__[433] = 'widgets.edit.py, line 433:\n    assert (request or data)';
		$m.__track_lines__[435] = 'widgets.edit.py, line 435:\n    if request:';
		$m.__track_lines__[436] = 'widgets.edit.py, line 436:\n    data = NetworkService.decode( request )';
		$m.__track_lines__[438] = 'widgets.edit.py, line 438:\n    if "action" in data and (data["action"] == "addSuccess" or data["action"] == "editSuccess"):';
		$m.__track_lines__[439] = 'widgets.edit.py, line 439:\n    logDiv = html5.Div()';
		$m.__track_lines__[440] = 'widgets.edit.py, line 440:\n    logDiv["class"].append("msg")';
		$m.__track_lines__[441] = 'widgets.edit.py, line 441:\n    spanMsg = html5.Span()';
		$m.__track_lines__[443] = 'widgets.edit.py, line 443:\n    spanMsg.appendChild( html5.TextNode( translate( self.logaction ) ) )';
		$m.__track_lines__[444] = 'widgets.edit.py, line 444:\n    spanMsg["class"].append("msgspan")';
		$m.__track_lines__[445] = 'widgets.edit.py, line 445:\n    logDiv.appendChild(spanMsg)';
		$m.__track_lines__[447] = 'widgets.edit.py, line 447:\n    if self.modul in conf["modules"].keys():';
		$m.__track_lines__[448] = 'widgets.edit.py, line 448:\n    spanMsg = html5.Span()';
		$m.__track_lines__[449] = 'widgets.edit.py, line 449:\n    if self.modul.startswith( "_" ):';
		$m.__track_lines__[450] = 'widgets.edit.py, line 450:\n    spanMsg.appendChild( html5.TextNode( self.key ) )';
		$m.__track_lines__[452] = 'widgets.edit.py, line 452:\n    spanMsg.appendChild( html5.TextNode( conf["modules"][self.modul]["name"] ))';
		$m.__track_lines__[453] = 'widgets.edit.py, line 453:\n    spanMsg["class"].append("modulspan")';
		$m.__track_lines__[454] = 'widgets.edit.py, line 454:\n    logDiv.appendChild(spanMsg)';
		$m.__track_lines__[456] = 'widgets.edit.py, line 456:\n    if "values" in data.keys() and "name" in data["values"].keys():';
		$m.__track_lines__[457] = 'widgets.edit.py, line 457:\n    spanMsg = html5.Span()';
		$m.__track_lines__[459] = 'widgets.edit.py, line 459:\n    name = data["values"]["name"]';
		$m.__track_lines__[460] = 'widgets.edit.py, line 460:\n    if isinstance(name, dict):';
		$m.__track_lines__[461] = 'widgets.edit.py, line 461:\n    if conf["currentlanguage"] in name.keys():';
		$m.__track_lines__[462] = 'widgets.edit.py, line 462:\n    name = name[conf["currentlanguage"]]';
		$m.__track_lines__[464] = 'widgets.edit.py, line 464:\n    name = name.values()';
		$m.__track_lines__[466] = 'widgets.edit.py, line 466:\n    if isinstance(name, list):';
		$m.__track_lines__[467] = 'widgets.edit.py, line 467:\n    name = ", ".join(name)';
		$m.__track_lines__[469] = 'widgets.edit.py, line 469:\n    spanMsg.appendChild(html5.TextNode(str(html5.utils.unescape(name))))';
		$m.__track_lines__[470] = 'widgets.edit.py, line 470:\n    spanMsg["class"].append("namespan")';
		$m.__track_lines__[471] = 'widgets.edit.py, line 471:\n    logDiv.appendChild(spanMsg)';
		$m.__track_lines__[473] = 'widgets.edit.py, line 473:\n    conf["mainWindow"].log("success",logDiv)';
		$m.__track_lines__[475] = 'widgets.edit.py, line 475:\n    if askHierarchyCloning and self.clone:';
		$m.__track_lines__[477] = 'widgets.edit.py, line 477:\n    if self.applicationType == EditWidget.appList and "rootNodeOf" in conf[ "modules" ][ self.modul ]:';
		$m.__track_lines__[478] = 'widgets.edit.py, line 478:\n    self.key = data[ "values" ][ "id" ]';
		$m.__track_lines__[479] = 'widgets.edit.py, line 479:\n    YesNoDialog( translate( u"Do you want to clone the entire hierarchy?" ),';
		$m.__track_lines__[481] = 'widgets.edit.py, line 481:\n    return';
		$m.__track_lines__[484] = 'widgets.edit.py, line 484:\n    self.key = data[ "values" ][ "id" ]';
		$m.__track_lines__[485] = 'widgets.edit.py, line 485:\n    YesNoDialog( translate( u"Do you want to clone all subentries of this item?" ),';
		$m.__track_lines__[487] = 'widgets.edit.py, line 487:\n    return';
		$m.__track_lines__[489] = 'widgets.edit.py, line 489:\n    self.closeOrContinue()';
		$m.__track_lines__[490] = 'widgets.edit.py, line 490:\n    return';
		$m.__track_lines__[493] = 'widgets.edit.py, line 493:\n    self.clear()';
		$m.__track_lines__[494] = 'widgets.edit.py, line 494:\n    self.bones = {}';
		$m.__track_lines__[495] = 'widgets.edit.py, line 495:\n    self.actionbar.resetLoadingState()';
		$m.__track_lines__[496] = 'widgets.edit.py, line 496:\n    self.dataCache = data';
		$m.__track_lines__[498] = 'widgets.edit.py, line 498:\n    tmpDict = utils.boneListToDict( data["structure"] )';
		$m.__track_lines__[499] = 'widgets.edit.py, line 499:\n    fieldSets = {}';
		$m.__track_lines__[500] = 'widgets.edit.py, line 500:\n    currRow = 0';
		$m.__track_lines__[501] = 'widgets.edit.py, line 501:\n    hasMissing = False';
		$m.__track_lines__[503] = 'widgets.edit.py, line 503:\n    for key, bone in data["structure"]:';
		$m.__track_lines__[504] = 'widgets.edit.py, line 504:\n    if not bone["visible"]:';
		$m.__track_lines__[505] = 'widgets.edit.py, line 505:\n    continue';
		$m.__track_lines__[507] = 'widgets.edit.py, line 507:\n    cat = "default"';
		$m.__track_lines__[509] = 'widgets.edit.py, line 509:\n    if ("params" in bone.keys()';
		$m.__track_lines__[512] = 'widgets.edit.py, line 512:\n    cat = bone["params"]["category"]';
		$m.__track_lines__[514] = 'widgets.edit.py, line 514:\n    if not cat in fieldSets.keys():';
		$m.__track_lines__[515] = 'widgets.edit.py, line 515:\n    fs = html5.Fieldset()';
		$m.__track_lines__[516] = 'widgets.edit.py, line 516:\n    fs["class"] = cat';
		$m.__track_lines__[518] = 'widgets.edit.py, line 518:\n    if cat=="default":';
		$m.__track_lines__[519] = 'widgets.edit.py, line 519:\n    fs["class"].append("active")';
		$m.__track_lines__[522] = 'widgets.edit.py, line 522:\n    fs["name"] = cat';
		$m.__track_lines__[523] = 'widgets.edit.py, line 523:\n    legend = html5.Legend()';
		$m.__track_lines__[525] = 'widgets.edit.py, line 525:\n    fshref = fieldset_A()';
		$m.__track_lines__[527] = 'widgets.edit.py, line 527:\n    fshref.appendChild(html5.TextNode(cat) )';
		$m.__track_lines__[528] = 'widgets.edit.py, line 528:\n    legend.appendChild( fshref )';
		$m.__track_lines__[529] = 'widgets.edit.py, line 529:\n    fs.appendChild(legend)';
		$m.__track_lines__[530] = 'widgets.edit.py, line 530:\n    section = html5.Section()';
		$m.__track_lines__[531] = 'widgets.edit.py, line 531:\n    fs.appendChild(section)';
		$m.__track_lines__[532] = 'widgets.edit.py, line 532:\n    fs._section = section';
		$m.__track_lines__[533] = 'widgets.edit.py, line 533:\n    fieldSets[ cat ] = fs';
		$m.__track_lines__[535] = 'widgets.edit.py, line 535:\n    if "params" in bone.keys() and bone["params"] and "category" in bone["params"].keys():';
		$m.__track_lines__[536] = 'widgets.edit.py, line 536:\n    tabName = bone["params"]["category"]';
		$m.__track_lines__[538] = 'widgets.edit.py, line 538:\n    tabName = "Test"#QtCore.QCoreApplication.translate("EditWidget", "General")';
		$m.__track_lines__[540] = 'widgets.edit.py, line 540:\n    wdgGen = editBoneSelector.select( self.modul, key, tmpDict )';
		$m.__track_lines__[541] = 'widgets.edit.py, line 541:\n    widget = wdgGen.fromSkelStructure( self.modul, key, tmpDict )';
		$m.__track_lines__[542] = 'widgets.edit.py, line 542:\n    widget["id"] = "vi_%s_%s_%s_%s_bn_%s" % (self.editIdx, self.modul,';
		$m.__track_lines__[549] = 'widgets.edit.py, line 549:\n    descrLbl = html5.Label(bone["descr"])';
		$m.__track_lines__[550] = 'widgets.edit.py, line 550:\n    descrLbl["class"].append(key)';
		$m.__track_lines__[551] = 'widgets.edit.py, line 551:\n    descrLbl["class"].append(bone["type"].replace(".","_"))';
		$m.__track_lines__[552] = 'widgets.edit.py, line 552:\n    descrLbl["for"] = "vi_%s_%s_%s_%s_bn_%s" % ( self.editIdx, self.modul,';
		$m.__track_lines__[554] = 'widgets.edit.py, line 554:\n    print(key, bone["required"], bone["error"])';
		$m.__track_lines__[555] = 'widgets.edit.py, line 555:\n    if bone["required"]:';
		$m.__track_lines__[556] = 'widgets.edit.py, line 556:\n    descrLbl["class"].append("is_required")';
		$m.__track_lines__[558] = 'widgets.edit.py, line 558:\n    if bone["error"] is not None:';
		$m.__track_lines__[559] = 'widgets.edit.py, line 559:\n    descrLbl["class"].append("is_invalid")';
		$m.__track_lines__[560] = 'widgets.edit.py, line 560:\n    descrLbl["title"] = bone["error"]';
		$m.__track_lines__[561] = 'widgets.edit.py, line 561:\n    fieldSets[ cat ]["class"].append("is_incomplete")';
		$m.__track_lines__[562] = 'widgets.edit.py, line 562:\n    hasMissing = True';
		$m.__track_lines__[564] = 'widgets.edit.py, line 564:\n    descrLbl["class"].append("is_valid")';
		$m.__track_lines__[566] = 'widgets.edit.py, line 566:\n    if isinstance(bone["error"], dict):';
		$m.__track_lines__[567] = 'widgets.edit.py, line 567:\n    widget.setExtendedErrorInformation(bone["error"])';
		$m.__track_lines__[569] = 'widgets.edit.py, line 569:\n    containerDiv = html5.Div()';
		$m.__track_lines__[570] = 'widgets.edit.py, line 570:\n    containerDiv.appendChild( descrLbl )';
		$m.__track_lines__[571] = 'widgets.edit.py, line 571:\n    containerDiv.appendChild( widget )';
		$m.__track_lines__[573] = 'widgets.edit.py, line 573:\n    if ("params" in bone.keys()';
		$m.__track_lines__[576] = 'widgets.edit.py, line 576:\n    containerDiv.appendChild( ToolTip(longText=bone["params"]["tooltip"]) )';
		$m.__track_lines__[578] = 'widgets.edit.py, line 578:\n    fieldSets[ cat ]._section.appendChild( containerDiv )';
		$m.__track_lines__[579] = 'widgets.edit.py, line 579:\n    containerDiv["class"].append("bone")';
		$m.__track_lines__[580] = 'widgets.edit.py, line 580:\n    containerDiv["class"].append("bone_"+key)';
		$m.__track_lines__[581] = 'widgets.edit.py, line 581:\n    containerDiv["class"].append( bone["type"].replace(".","_") )';
		$m.__track_lines__[583] = 'widgets.edit.py, line 583:\n    if "." in bone["type"]:';
		$m.__track_lines__[584] = 'widgets.edit.py, line 584:\n    for t in bone["type"].split("."):';
		$m.__track_lines__[585] = 'widgets.edit.py, line 585:\n    containerDiv["class"].append(t)';
		$m.__track_lines__[590] = 'widgets.edit.py, line 590:\n    currRow += 1';
		$m.__track_lines__[591] = 'widgets.edit.py, line 591:\n    self.bones[ key ] = widget';
		$m.__track_lines__[593] = 'widgets.edit.py, line 593:\n    if len(fieldSets)==1:';
		$m.__track_lines__[594] = 'widgets.edit.py, line 594:\n    for (k,v) in fieldSets.items():';
		$m.__track_lines__[595] = 'widgets.edit.py, line 595:\n    if not "active" in v["class"]:';
		$m.__track_lines__[596] = 'widgets.edit.py, line 596:\n    v["class"].append("active")';
		$m.__track_lines__[598] = 'widgets.edit.py, line 598:\n    tmpList = [(k,v) for (k,v) in fieldSets.items()]';
		$m.__track_lines__[599] = 'widgets.edit.py, line 599:\n    tmpList.sort( key=lambda x:x[0])';
		$m.__track_lines__[601] = 'widgets.edit.py, line 601:\n    for k,v in tmpList:';
		$m.__track_lines__[602] = 'widgets.edit.py, line 602:\n    self.form.appendChild( v )';
		$m.__track_lines__[603] = 'widgets.edit.py, line 603:\n    v._section = None';
		$m.__track_lines__[605] = 'widgets.edit.py, line 605:\n    self.unserialize( data["values"] )';
		$m.__track_lines__[607] = 'widgets.edit.py, line 607:\n    if self._hashArgs: #Apply the default values (if any)';
		$m.__track_lines__[608] = 'widgets.edit.py, line 608:\n    self.unserialize( self._hashArgs )';
		$m.__track_lines__[609] = 'widgets.edit.py, line 609:\n    self._hashArgs = None';
		$m.__track_lines__[611] = 'widgets.edit.py, line 611:\n    self._lastData = data';
		$m.__track_lines__[613] = 'widgets.edit.py, line 613:\n    if hasMissing and not self.wasInitialRequest:';
		$m.__track_lines__[614] = 'widgets.edit.py, line 614:\n    conf["mainWindow"].log("warning",translate("Could not save entry!"))';
		$m.__track_lines__[616] = 'widgets.edit.py, line 616:\n    def unserialize(self, data):';
		$m.__track_lines__[620] = 'widgets.edit.py, line 620:\n    for bone in self.bones.values():';
		$m.__track_lines__[621] = 'widgets.edit.py, line 621:\n    bone.unserialize( data )';
		$m.__track_lines__[623] = 'widgets.edit.py, line 623:\n    def doSave( self, closeOnSuccess=False, *args, **kwargs ):';
		$m.__track_lines__[627] = 'widgets.edit.py, line 627:\n    self.closeOnSuccess = closeOnSuccess';
		$m.__track_lines__[628] = 'widgets.edit.py, line 628:\n    res = {}';
		$m.__track_lines__[629] = 'widgets.edit.py, line 629:\n    for key, bone in self.bones.items():';
		$m.__track_lines__[630] = 'widgets.edit.py, line 630:\n    try:';
		$m.__track_lines__[631] = 'widgets.edit.py, line 631:\n    res.update( bone.serializeForPost( ) )';
		$m.__track_lines__[634] = 'widgets.edit.py, line 634:\n    lbl = bone.parent()._children[0]';
		$m.__track_lines__[635] = 'widgets.edit.py, line 635:\n    if "is_valid" in lbl["class"]:';
		$m.__track_lines__[636] = 'widgets.edit.py, line 636:\n    lbl["class"].remove("is_valid")';
		$m.__track_lines__[637] = 'widgets.edit.py, line 637:\n    lbl["class"].append("is_invalid")';
		$m.__track_lines__[638] = 'widgets.edit.py, line 638:\n    self.actionbar.resetLoadingState()';
		$m.__track_lines__[639] = 'widgets.edit.py, line 639:\n    return';
		$m.__track_lines__[640] = 'widgets.edit.py, line 640:\n    self.save( res )';
		$m.__track_lines__[642] = 'widgets.edit.py, line 642:\n    class fieldset_A(A):';
		$m.__track_lines__[643] = 'widgets.edit.py, line 643:\n    _baseClass = "a"';
		$m.__track_lines__[645] = 'widgets.edit.py, line 645:\n    def __init__(self, *args, **kwargs):';
		$m.__track_lines__[646] = 'widgets.edit.py, line 646:\n    super(fieldset_A,self).__init__(*args, **kwargs )';
		$m.__track_lines__[647] = 'widgets.edit.py, line 647:\n    self.sinkEvent("onClick")';
		$m.__track_lines__[649] = 'widgets.edit.py, line 649:\n    def onClick(self,event):';
		$m.__track_lines__[650] = 'widgets.edit.py, line 650:\n    for element in self.parent().parent().parent()._children:';
		$m.__track_lines__[651] = 'widgets.edit.py, line 651:\n    if isinstance(element,Fieldset):';
		$m.__track_lines__[652] = 'widgets.edit.py, line 652:\n    if utils.doesEventHitWidgetOrChildren( event, element ):';
		$m.__track_lines__[653] = 'widgets.edit.py, line 653:\n    if not "active" in element["class"]:';
		$m.__track_lines__[654] = 'widgets.edit.py, line 654:\n    element["class"].append("active")';
		$m.__track_lines__[655] = 'widgets.edit.py, line 655:\n    element["class"].remove("inactive")';
		$m.__track_lines__[657] = 'widgets.edit.py, line 657:\n    if not "inactive" in element["class"]:';
		$m.__track_lines__[658] = 'widgets.edit.py, line 658:\n    element["class"].append("inactive")';
		$m.__track_lines__[659] = 'widgets.edit.py, line 659:\n    element["class"].remove("active")';
		$m.__track_lines__[661] = 'widgets.edit.py, line 661:\n    if not "inactive" in element["class"] and isinstance(element,fieldset_A):';
		$m.__track_lines__[662] = 'widgets.edit.py, line 662:\n    element["class"].append("inactive")';
		$m.__track_lines__[663] = 'widgets.edit.py, line 663:\n    element["class"].remove("active")';
		$m.__track_lines__[664] = 'widgets.edit.py, line 664:\n    if len(element._children)>0 and isinstance(element,fieldset_A) and hasattr(element._children[1],"_children"): #subelement crawler';
		$m.__track_lines__[665] = 'widgets.edit.py, line 665:\n    for sube in element._children[1]._children:';
		$m.__track_lines__[666] = 'widgets.edit.py, line 666:\n    if isinstance(sube,fieldset_A):';
		$m.__track_lines__[667] = 'widgets.edit.py, line 667:\n    if not "inactive" in sube["class"]:';
		$m.__track_lines__[668] = 'widgets.edit.py, line 668:\n    sube.parent["class"].append("inactive")';
		$m.__track_lines__[669] = 'widgets.edit.py, line 669:\n    sube["class"].remove("active")';

		var $constant_int_0 = new $p['int'](0);
		var $constant_int_1 = new $p['int'](1);
		var $constant_int_403 = new $p['int'](403);
		var $constant_int_401 = new $p['int'](401);
		$pyjs['track']['module']='widgets.edit';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=3;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['html5'] = $p['___import___']('html5', 'widgets');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=4;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['A'] = $p['___import___']('html5.a.A', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=5;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['Fieldset'] = $p['___import___']('html5.form.Fieldset', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=6;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['YesNoDialog'] = $p['___import___']('html5.ext.YesNoDialog', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=7;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['NetworkService'] = $p['___import___']('network.NetworkService', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=8;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['conf'] = $p['___import___']('config.conf', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=9;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['editBoneSelector'] = $p['___import___']('priorityqueue.editBoneSelector', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=10;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['ToolTip'] = $p['___import___']('widgets.tooltip.ToolTip', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=11;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['protocolWrapperInstanceSelector'] = $p['___import___']('priorityqueue.protocolWrapperInstanceSelector', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=12;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['ActionBar'] = $p['___import___']('widgets.actionbar.ActionBar', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=13;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['utils'] = $p['___import___']('utils', 'widgets');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=14;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['translate'] = $p['___import___']('i18n.translate', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=17;
		$m['InvalidBoneValueException'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'widgets.edit';
			$cls_definition['__md5__'] = '7b0abb3d9aa29f9486d97b0bfd7a8054';
			$pyjs['track']['lineno']=18;
			$pyjs['track']['lineno']=17;
			var $bases = new Array($p['ValueError']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('InvalidBoneValueException', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=20;
		$m['InternalEdit'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'widgets.edit';
			$cls_definition['__md5__'] = '0c233152b063d881f43f698ef7b05ddf';
			$pyjs['track']['lineno']=22;
			$method = $pyjs__bind_method2('__init__', function(skelStructure, values, errorInformation, readOnly) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 4)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 5, arguments['length']+1);
				} else {
					var self = arguments[0];
					skelStructure = arguments[1];
					values = arguments[2];
					errorInformation = arguments[3];
					readOnly = arguments[4];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 2 || arguments['length'] > 5)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 5, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0c233152b063d881f43f698ef7b05ddf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof values == 'undefined') values=arguments['callee']['__args__'][4][1];
				if (typeof errorInformation == 'undefined') errorInformation=arguments['callee']['__args__'][5][1];
				if (typeof readOnly == 'undefined') readOnly=arguments['callee']['__args__'][6][1];

				$pyjs['track']={'module':'widgets.edit', 'lineno':22};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=22;
				$pyjs['track']['lineno']=23;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['InternalEdit'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})()['__init__']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})();
				$pyjs['track']['lineno']=24;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('editIdx', $constant_int_1) : $p['setattr'](self, 'editIdx', $constant_int_1); 
				$pyjs['track']['lineno']=25;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('skelStructure', skelStructure) : $p['setattr'](self, 'skelStructure', skelStructure); 
				$pyjs['track']['lineno']=26;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('values', values) : $p['setattr'](self, 'values', values); 
				$pyjs['track']['lineno']=27;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('errorInformation', errorInformation) : $p['setattr'](self, 'errorInformation', errorInformation); 
				$pyjs['track']['lineno']=28;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('form', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Form']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})()) : $p['setattr'](self, 'form', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Form']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})()); 
				$pyjs['track']['lineno']=29;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'form'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})();
				$pyjs['track']['lineno']=30;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(self, 'renderStructure', null, null, [{'readOnly':readOnly}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})();
				$pyjs['track']['lineno']=31;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](values);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})()) {
					$pyjs['track']['lineno']=32;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['unserialize'](values);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['skelStructure'],['values', null],['errorInformation', null],['readOnly', false]]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=35;
			$method = $pyjs__bind_method2('renderStructure', function(readOnly) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					readOnly = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0c233152b063d881f43f698ef7b05ddf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof readOnly == 'undefined') readOnly=arguments['callee']['__args__'][3][1];
				var $iter5_nextval,descrLbl,$and14,$lambda1,$or4,$iter5_array,$iter3_type,widget,$iter1_iter,tmpList,$iter5_iter,fshref,$iter5_type,wdgGen,containerDiv,$or1,tmp,$iter3_idx,$iter2_iter,v,$iter2_type,$iter3_iter,t,$add1,$iter1_array,$iter5_idx,$iter3_array,$iter1_idx,$and8,$and9,hasMissing,fs,$iter1_nextval,$or3,$or2,tmpDict,$and1,$and2,$and3,$and4,$and5,$and6,$and7,$and12,$and13,tabName,$and16,$and17,key,$and15,$iter3_nextval,legend,$iter2_array,$iter2_nextval,$iter1_type,k,$iter2_idx,$and10,cat,$add2,$add3,$and11,$pyjs__trackstack_size_2,fieldSets,$add4,$pyjs__trackstack_size_1,currRow,section,bone;
				$pyjs['track']={'module':'widgets.edit', 'lineno':35};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=35;
				$pyjs['track']['lineno']=38;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('bones', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})()) : $p['setattr'](self, 'bones', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})()); 
				$pyjs['track']['lineno']=41;
				tmpDict = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['utils']['boneListToDict']($p['getattr'](self, 'skelStructure'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})();
				$pyjs['track']['lineno']=42;
				fieldSets = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})();
				$pyjs['track']['lineno']=43;
				currRow = $constant_int_0;
				$pyjs['track']['lineno']=44;
				hasMissing = false;
				$pyjs['track']['lineno']=45;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter1_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'skelStructure');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})();
				$iter1_nextval=$p['__iter_prepare']($iter1_iter,false);
				while (typeof($p['__wrapped_next']($iter1_nextval)['$nextval']) != 'undefined') {
					var $tupleassign1 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter1_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})();
					key = $tupleassign1[0];
					bone = $tupleassign1[1];
					$pyjs['track']['lineno']=48;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool'](bone['__getitem__']('visible')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})()) {
						$pyjs['track']['lineno']=49;
						continue;
					}
					$pyjs['track']['lineno']=52;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](readOnly);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_14_err){if (!$p['isinstance']($pyjs_dbg_14_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_14_err);}throw $pyjs_dbg_14_err;
}})()) {
						$pyjs['track']['lineno']=53;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return tmpDict['__getitem__'](key)['__setitem__']('readonly', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})();
					}
					$pyjs['track']['lineno']=55;
					cat = 'default';
					$pyjs['track']['lineno']=57;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and1=(function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_16_err){if (!$p['isinstance']($pyjs_dbg_16_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_16_err);}throw $pyjs_dbg_16_err;
}})()['__contains__']('params'))?($p['bool']($and2=(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](bone['__getitem__']('params'), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_17_err){if (!$p['isinstance']($pyjs_dbg_17_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_17_err);}throw $pyjs_dbg_17_err;
}})())?(function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['__getitem__']('params')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})()['__contains__']('category'):$and2):$and1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_19_err){if (!$p['isinstance']($pyjs_dbg_19_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_19_err);}throw $pyjs_dbg_19_err;
}})()) {
						$pyjs['track']['lineno']=60;
						cat = bone['__getitem__']('params')['__getitem__']('category');
					}
					$pyjs['track']['lineno']=62;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return fieldSets['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})()['__contains__'](cat)));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})()) {
						$pyjs['track']['lineno']=63;
						fs = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Fieldset']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})();
						$pyjs['track']['lineno']=64;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return fs['__setitem__']('class', cat);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})();
						$pyjs['track']['lineno']=65;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']($p['op_eq'](cat, 'default'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})()) {
							$pyjs['track']['lineno']=66;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return fs['__getitem__']('class')['append']('active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})();
						}
						$pyjs['track']['lineno']=68;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return fs['__setitem__']('name', cat);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_26_err){if (!$p['isinstance']($pyjs_dbg_26_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_26_err);}throw $pyjs_dbg_26_err;
}})();
						$pyjs['track']['lineno']=69;
						legend = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Legend']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_27_err){if (!$p['isinstance']($pyjs_dbg_27_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_27_err);}throw $pyjs_dbg_27_err;
}})();
						$pyjs['track']['lineno']=71;
						fshref = (function(){try{try{$pyjs['in_try_except'] += 1;
						return (typeof fieldset_A == "undefined"?$m['fieldset_A']:fieldset_A)();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_28_err){if (!$p['isinstance']($pyjs_dbg_28_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_28_err);}throw $pyjs_dbg_28_err;
}})();
						$pyjs['track']['lineno']=73;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return fshref['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['TextNode'](cat);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_29_err){if (!$p['isinstance']($pyjs_dbg_29_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_29_err);}throw $pyjs_dbg_29_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_30_err){if (!$p['isinstance']($pyjs_dbg_30_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_30_err);}throw $pyjs_dbg_30_err;
}})();
						$pyjs['track']['lineno']=74;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return legend['appendChild'](fshref);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_31_err){if (!$p['isinstance']($pyjs_dbg_31_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_31_err);}throw $pyjs_dbg_31_err;
}})();
						$pyjs['track']['lineno']=75;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return fs['appendChild'](legend);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_32_err){if (!$p['isinstance']($pyjs_dbg_32_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_32_err);}throw $pyjs_dbg_32_err;
}})();
						$pyjs['track']['lineno']=76;
						section = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Section']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_33_err){if (!$p['isinstance']($pyjs_dbg_33_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_33_err);}throw $pyjs_dbg_33_err;
}})();
						$pyjs['track']['lineno']=77;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return fs['appendChild'](section);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_34_err){if (!$p['isinstance']($pyjs_dbg_34_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_34_err);}throw $pyjs_dbg_34_err;
}})();
						$pyjs['track']['lineno']=78;
						fs['__is_instance__'] && typeof fs['__setattr__'] == 'function' ? fs['__setattr__']('_section', section) : $p['setattr'](fs, '_section', section); 
						$pyjs['track']['lineno']=79;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return fieldSets['__setitem__'](cat, fs);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})();
					}
					$pyjs['track']['lineno']=81;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and4=(function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_36_err){if (!$p['isinstance']($pyjs_dbg_36_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_36_err);}throw $pyjs_dbg_36_err;
}})()['__contains__']('params'))?($p['bool']($and5=bone['__getitem__']('params'))?(function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['__getitem__']('params')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_37_err){if (!$p['isinstance']($pyjs_dbg_37_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_37_err);}throw $pyjs_dbg_37_err;
}})()['__contains__']('category'):$and5):$and4));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_38_err){if (!$p['isinstance']($pyjs_dbg_38_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_38_err);}throw $pyjs_dbg_38_err;
}})()) {
						$pyjs['track']['lineno']=82;
						tabName = bone['__getitem__']('params')['__getitem__']('category');
					}
					else {
						$pyjs['track']['lineno']=84;
						tabName = 'Test';
					}
					$pyjs['track']['lineno']=86;
					wdgGen = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['editBoneSelector']['select'](null, key, tmpDict);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_39_err){if (!$p['isinstance']($pyjs_dbg_39_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_39_err);}throw $pyjs_dbg_39_err;
}})();
					$pyjs['track']['lineno']=87;
					widget = (function(){try{try{$pyjs['in_try_except'] += 1;
					return wdgGen['fromSkelStructure'](null, key, tmpDict);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_40_err){if (!$p['isinstance']($pyjs_dbg_40_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_40_err);}throw $pyjs_dbg_40_err;
}})();
					$pyjs['track']['lineno']=88;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return widget['__setitem__']('id', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('vi_%s_%s_%s_%s_bn_%s', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['tuple']([$p['getattr'](self, 'editIdx'), null, 'internal', cat, key]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_41_err){if (!$p['isinstance']($pyjs_dbg_41_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_41_err);}throw $pyjs_dbg_41_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_42_err){if (!$p['isinstance']($pyjs_dbg_42_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_42_err);}throw $pyjs_dbg_42_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_43_err){if (!$p['isinstance']($pyjs_dbg_43_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_43_err);}throw $pyjs_dbg_43_err;
}})();
					$pyjs['track']['lineno']=93;
					descrLbl = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Label'](bone['__getitem__']('descr'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_44_err){if (!$p['isinstance']($pyjs_dbg_44_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_44_err);}throw $pyjs_dbg_44_err;
}})();
					$pyjs['track']['lineno']=94;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return descrLbl['__getitem__']('class')['append'](key);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_45_err){if (!$p['isinstance']($pyjs_dbg_45_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_45_err);}throw $pyjs_dbg_45_err;
}})();
					$pyjs['track']['lineno']=95;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return descrLbl['__getitem__']('class')['append']((function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['__getitem__']('type')['$$replace']('.', '_');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_46_err){if (!$p['isinstance']($pyjs_dbg_46_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_46_err);}throw $pyjs_dbg_46_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})();
					$pyjs['track']['lineno']=96;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return descrLbl['__setitem__']('for', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('vi_%s_%s_%s_%s_bn_%s', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['tuple']([$p['getattr'](self, 'editIdx'), null, 'internal', cat, key]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_48_err){if (!$p['isinstance']($pyjs_dbg_48_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_48_err);}throw $pyjs_dbg_48_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_49_err){if (!$p['isinstance']($pyjs_dbg_49_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_49_err);}throw $pyjs_dbg_49_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_50_err){if (!$p['isinstance']($pyjs_dbg_50_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_50_err);}throw $pyjs_dbg_50_err;
}})();
					$pyjs['track']['lineno']=98;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](bone['__getitem__']('required'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_51_err){if (!$p['isinstance']($pyjs_dbg_51_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_51_err);}throw $pyjs_dbg_51_err;
}})()) {
						$pyjs['track']['lineno']=99;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return descrLbl['__getitem__']('class')['append']('is_required');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_52_err){if (!$p['isinstance']($pyjs_dbg_52_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_52_err);}throw $pyjs_dbg_52_err;
}})();
					}
					$pyjs['track']['lineno']=101;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and7=bone['__getitem__']('required'))?($p['bool']($or1=!$p['op_is'](bone['__getitem__']('error'), null))?$or1:($p['bool']($and9=$p['getattr'](self, 'errorInformation'))?(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['errorInformation']['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_53_err){if (!$p['isinstance']($pyjs_dbg_53_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_53_err);}throw $pyjs_dbg_53_err;
}})()['__contains__'](key):$and9)):$and7));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_54_err){if (!$p['isinstance']($pyjs_dbg_54_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_54_err);}throw $pyjs_dbg_54_err;
}})()) {
						$pyjs['track']['lineno']=104;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return descrLbl['__getitem__']('class')['append']('is_invalid');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_55_err){if (!$p['isinstance']($pyjs_dbg_55_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_55_err);}throw $pyjs_dbg_55_err;
}})();
						$pyjs['track']['lineno']=105;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](bone['__getitem__']('error'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_56_err){if (!$p['isinstance']($pyjs_dbg_56_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_56_err);}throw $pyjs_dbg_56_err;
}})()) {
							$pyjs['track']['lineno']=106;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return descrLbl['__setitem__']('title', bone['__getitem__']('error'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_57_err){if (!$p['isinstance']($pyjs_dbg_57_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_57_err);}throw $pyjs_dbg_57_err;
}})();
						}
						else {
							$pyjs['track']['lineno']=108;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return descrLbl['__setitem__']('title', $p['getattr'](self, 'errorInformation')['__getitem__'](key));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_58_err){if (!$p['isinstance']($pyjs_dbg_58_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_58_err);}throw $pyjs_dbg_58_err;
}})();
						}
						$pyjs['track']['lineno']=109;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return fieldSets['__getitem__'](cat)['__getitem__']('class')['append']('is_incomplete');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_59_err){if (!$p['isinstance']($pyjs_dbg_59_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_59_err);}throw $pyjs_dbg_59_err;
}})();
						$pyjs['track']['lineno']=110;
						hasMissing = true;
					}
					$pyjs['track']['lineno']=112;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and11=bone['__getitem__']('required'))?!$p['bool'](($p['bool']($or3=!$p['op_is'](bone['__getitem__']('error'), null))?$or3:($p['bool']($and13=$p['getattr'](self, 'errorInformation'))?(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['errorInformation']['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_60_err){if (!$p['isinstance']($pyjs_dbg_60_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_60_err);}throw $pyjs_dbg_60_err;
}})()['__contains__'](key):$and13))):$and11));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_61_err){if (!$p['isinstance']($pyjs_dbg_61_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_61_err);}throw $pyjs_dbg_61_err;
}})()) {
						$pyjs['track']['lineno']=113;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return descrLbl['__getitem__']('class')['append']('is_valid');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_62_err){if (!$p['isinstance']($pyjs_dbg_62_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_62_err);}throw $pyjs_dbg_62_err;
}})();
					}
					$pyjs['track']['lineno']=115;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and15=(function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_63_err){if (!$p['isinstance']($pyjs_dbg_63_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_63_err);}throw $pyjs_dbg_63_err;
}})()['__contains__']('params'))?($p['bool']($and16=(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](bone['__getitem__']('params'), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})())?(function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['__getitem__']('params')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_65_err){if (!$p['isinstance']($pyjs_dbg_65_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_65_err);}throw $pyjs_dbg_65_err;
}})()['__contains__']('tooltip'):$and16):$and15));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_66_err){if (!$p['isinstance']($pyjs_dbg_66_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_66_err);}throw $pyjs_dbg_66_err;
}})()) {
						$pyjs['track']['lineno']=116;
						tmp = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})();
						$pyjs['track']['lineno']=117;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return tmp['appendChild'](descrLbl);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_68_err){if (!$p['isinstance']($pyjs_dbg_68_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_68_err);}throw $pyjs_dbg_68_err;
}})();
						$pyjs['track']['lineno']=118;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return tmp['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call(null, $m['ToolTip'], null, null, [{'longText':bone['__getitem__']('params')['__getitem__']('tooltip')}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_69_err){if (!$p['isinstance']($pyjs_dbg_69_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_69_err);}throw $pyjs_dbg_69_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_70_err){if (!$p['isinstance']($pyjs_dbg_70_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_70_err);}throw $pyjs_dbg_70_err;
}})();
						$pyjs['track']['lineno']=119;
						descrLbl = tmp;
					}
					$pyjs['track']['lineno']=121;
					containerDiv = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_71_err){if (!$p['isinstance']($pyjs_dbg_71_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_71_err);}throw $pyjs_dbg_71_err;
}})();
					$pyjs['track']['lineno']=122;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return containerDiv['appendChild'](descrLbl);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_72_err){if (!$p['isinstance']($pyjs_dbg_72_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_72_err);}throw $pyjs_dbg_72_err;
}})();
					$pyjs['track']['lineno']=123;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return containerDiv['appendChild'](widget);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_73_err){if (!$p['isinstance']($pyjs_dbg_73_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_73_err);}throw $pyjs_dbg_73_err;
}})();
					$pyjs['track']['lineno']=124;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return fieldSets['__getitem__'](cat)['_section']['appendChild'](containerDiv);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_74_err){if (!$p['isinstance']($pyjs_dbg_74_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_74_err);}throw $pyjs_dbg_74_err;
}})();
					$pyjs['track']['lineno']=126;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return containerDiv['__getitem__']('class')['append']('bone');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})();
					$pyjs['track']['lineno']=127;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return containerDiv['__getitem__']('class')['append']($p['__op_add']($add1='bone_',$add2=key));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})();
					$pyjs['track']['lineno']=128;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return containerDiv['__getitem__']('class')['append']((function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['__getitem__']('type')['$$replace']('.', '_');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_77_err){if (!$p['isinstance']($pyjs_dbg_77_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_77_err);}throw $pyjs_dbg_77_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_78_err){if (!$p['isinstance']($pyjs_dbg_78_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_78_err);}throw $pyjs_dbg_78_err;
}})();
					$pyjs['track']['lineno']=130;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](bone['__getitem__']('type')['__contains__']('.'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_79_err){if (!$p['isinstance']($pyjs_dbg_79_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_79_err);}throw $pyjs_dbg_79_err;
}})()) {
						$pyjs['track']['lineno']=131;
						$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
						$iter2_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return bone['__getitem__']('type')['$$split']('.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_80_err){if (!$p['isinstance']($pyjs_dbg_80_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_80_err);}throw $pyjs_dbg_80_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_81_err){if (!$p['isinstance']($pyjs_dbg_81_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_81_err);}throw $pyjs_dbg_81_err;
}})();
						$iter2_nextval=$p['__iter_prepare']($iter2_iter,false);
						while (typeof($p['__wrapped_next']($iter2_nextval)['$nextval']) != 'undefined') {
							t = $iter2_nextval['$nextval'];
							$pyjs['track']['lineno']=132;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return containerDiv['__getitem__']('class')['append'](t);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_82_err){if (!$p['isinstance']($pyjs_dbg_82_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_82_err);}throw $pyjs_dbg_82_err;
}})();
						}
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='widgets.edit';
					}
					$pyjs['track']['lineno']=137;
					currRow = $p['__op_add']($add3=currRow,$add4=$constant_int_1);
					$pyjs['track']['lineno']=138;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'bones')['__setitem__'](key, widget);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_83_err){if (!$p['isinstance']($pyjs_dbg_83_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_83_err);}throw $pyjs_dbg_83_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=140;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](fieldSets);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_84_err){if (!$p['isinstance']($pyjs_dbg_84_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_84_err);}throw $pyjs_dbg_84_err;
}})(), $constant_int_1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})()) {
					$pyjs['track']['lineno']=141;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter3_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return fieldSets['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})();
					$iter3_nextval=$p['__iter_prepare']($iter3_iter,false);
					while (typeof($p['__wrapped_next']($iter3_nextval)['$nextval']) != 'undefined') {
						var $tupleassign2 = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['__ass_unpack']($iter3_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})();
						k = $tupleassign2[0];
						v = $tupleassign2[1];
						$pyjs['track']['lineno']=142;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['bool'](v['__getitem__']('class')['__contains__']('active')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_89_err){if (!$p['isinstance']($pyjs_dbg_89_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_89_err);}throw $pyjs_dbg_89_err;
}})()) {
							$pyjs['track']['lineno']=143;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return v['__getitem__']('class')['append']('active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})();
						}
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='widgets.edit';
				}
				$pyjs['track']['lineno']=145;
				tmpList = function(){
					var $iter4_nextval,k,$pyjs__trackstack_size_1,$collcomp1,$iter4_idx,$iter4_type,v,$iter4_array,$iter4_iter;
	$collcomp1 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter4_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return fieldSets['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_92_err){if (!$p['isinstance']($pyjs_dbg_92_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_92_err);}throw $pyjs_dbg_92_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_93_err){if (!$p['isinstance']($pyjs_dbg_93_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_93_err);}throw $pyjs_dbg_93_err;
}})();
				$iter4_nextval=$p['__iter_prepare']($iter4_iter,false);
				while (typeof($p['__wrapped_next']($iter4_nextval)['$nextval']) != 'undefined') {
					var $tupleassign3 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter4_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_91_err){if (!$p['isinstance']($pyjs_dbg_91_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_91_err);}throw $pyjs_dbg_91_err;
}})();
					k = $tupleassign3[0];
					v = $tupleassign3[1];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp1['append']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['tuple']([k, v]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_94_err){if (!$p['isinstance']($pyjs_dbg_94_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_94_err);}throw $pyjs_dbg_94_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_95_err){if (!$p['isinstance']($pyjs_dbg_95_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_95_err);}throw $pyjs_dbg_95_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.edit';

	return $collcomp1;}();
				$pyjs['track']['lineno']=146;
				var 				$lambda1 = function(x) {
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);

					$pyjs['track']={'module':'widgets.edit','lineno':146};$pyjs['trackstack']['push']($pyjs['track']);
					$pyjs['track']['module']='widgets.edit';
					$pyjs['track']['lineno']=146;
					$pyjs['track']['lineno']=146;
					$pyjs['track']['lineno']=146;
					var $pyjs__ret = x['__getitem__']($constant_int_0);
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				};
				$lambda1['__name__'] = '$lambda1';

				$lambda1['__bind_type__'] = 0;
				$lambda1['__args__'] = [null,null,['x']];
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(tmpList, 'sort', null, null, [{'key':$lambda1}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_96_err){if (!$p['isinstance']($pyjs_dbg_96_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_96_err);}throw $pyjs_dbg_96_err;
}})();
				$pyjs['track']['lineno']=148;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter5_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return tmpList;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_98_err){if (!$p['isinstance']($pyjs_dbg_98_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_98_err);}throw $pyjs_dbg_98_err;
}})();
				$iter5_nextval=$p['__iter_prepare']($iter5_iter,false);
				while (typeof($p['__wrapped_next']($iter5_nextval)['$nextval']) != 'undefined') {
					var $tupleassign4 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter5_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_97_err){if (!$p['isinstance']($pyjs_dbg_97_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_97_err);}throw $pyjs_dbg_97_err;
}})();
					k = $tupleassign4[0];
					v = $tupleassign4[1];
					$pyjs['track']['lineno']=149;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['form']['appendChild'](v);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_99_err){if (!$p['isinstance']($pyjs_dbg_99_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_99_err);}throw $pyjs_dbg_99_err;
}})();
					$pyjs['track']['lineno']=150;
					v['__is_instance__'] && typeof v['__setattr__'] == 'function' ? v['__setattr__']('_section', null) : $p['setattr'](v, '_section', null); 
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.edit';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['readOnly', false]]);
			$cls_definition['renderStructure'] = $method;
			$pyjs['track']['lineno']=152;
			$method = $pyjs__bind_method2('doSave', function(closeOnSuccess) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					closeOnSuccess = arguments[1];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,2,arguments['length']-1));

					var kwargs = arguments['length'] >= 3 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0c233152b063d881f43f698ef7b05ddf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof closeOnSuccess != 'undefined') {
						if (closeOnSuccess !== null && typeof closeOnSuccess['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = closeOnSuccess;
							closeOnSuccess = arguments[2];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[2];
						}
					} else {
					}
				}
				if (typeof closeOnSuccess == 'undefined') closeOnSuccess=arguments['callee']['__args__'][3][1];
				var key,$iter6_idx,$iter6_type,res,$iter6_array,$pyjs_try_err,$iter6_iter,$pyjs__trackstack_size_1,lbl,bone,$iter6_nextval;
				$pyjs['track']={'module':'widgets.edit', 'lineno':152};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=152;
				$pyjs['track']['lineno']=156;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('closeOnSuccess', closeOnSuccess) : $p['setattr'](self, 'closeOnSuccess', closeOnSuccess); 
				$pyjs['track']['lineno']=157;
				res = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_100_err){if (!$p['isinstance']($pyjs_dbg_100_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_100_err);}throw $pyjs_dbg_100_err;
}})();
				$pyjs['track']['lineno']=158;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter6_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['bones']['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_102_err){if (!$p['isinstance']($pyjs_dbg_102_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_102_err);}throw $pyjs_dbg_102_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_103_err){if (!$p['isinstance']($pyjs_dbg_103_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_103_err);}throw $pyjs_dbg_103_err;
}})();
				$iter6_nextval=$p['__iter_prepare']($iter6_iter,false);
				while (typeof($p['__wrapped_next']($iter6_nextval)['$nextval']) != 'undefined') {
					var $tupleassign5 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter6_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_101_err){if (!$p['isinstance']($pyjs_dbg_101_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_101_err);}throw $pyjs_dbg_101_err;
}})();
					key = $tupleassign5[0];
					bone = $tupleassign5[1];
					$pyjs['track']['lineno']=159;
					var $pyjs__trackstack_size_2 = $pyjs['trackstack']['length'];
					try {
						try {
							$pyjs['in_try_except'] += 1;
							$pyjs['track']['lineno']=160;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return res['update']((function(){try{try{$pyjs['in_try_except'] += 1;
							return bone['serializeForPost']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_104_err){if (!$p['isinstance']($pyjs_dbg_104_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_104_err);}throw $pyjs_dbg_104_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_105_err){if (!$p['isinstance']($pyjs_dbg_105_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_105_err);}throw $pyjs_dbg_105_err;
}})();
						} finally { $pyjs['in_try_except'] -= 1; }
					} catch($pyjs_try_err) {
						$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_2 - 1);
						$pyjs['__active_exception_stack__'] = null;
						$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
						var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
						$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='widgets.edit';
						if (($pyjs_try_err_name == $m['InvalidBoneValueException']['__name__'])||$p['_isinstance']($pyjs_try_err,$m['InvalidBoneValueException'])) {
							$pyjs['track']['lineno']=163;
							lbl = $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
							return bone['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_106_err){if (!$p['isinstance']($pyjs_dbg_106_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_106_err);}throw $pyjs_dbg_106_err;
}})(), '_children')['__getitem__']($constant_int_0);
							$pyjs['track']['lineno']=164;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool'](lbl['__getitem__']('class')['__contains__']('is_valid'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_107_err){if (!$p['isinstance']($pyjs_dbg_107_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_107_err);}throw $pyjs_dbg_107_err;
}})()) {
								$pyjs['track']['lineno']=165;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return lbl['__getitem__']('class')['remove']('is_valid');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_108_err){if (!$p['isinstance']($pyjs_dbg_108_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_108_err);}throw $pyjs_dbg_108_err;
}})();
							}
							$pyjs['track']['lineno']=166;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return lbl['__getitem__']('class')['append']('is_invalid');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_109_err){if (!$p['isinstance']($pyjs_dbg_109_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_109_err);}throw $pyjs_dbg_109_err;
}})();
							$pyjs['track']['lineno']=167;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return self['actionbar']['resetLoadingState']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_110_err){if (!$p['isinstance']($pyjs_dbg_110_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_110_err);}throw $pyjs_dbg_110_err;
}})();
							$pyjs['track']['lineno']=168;
							$pyjs['track']['lineno']=168;
							var $pyjs__ret = null;
							$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
							return $pyjs__ret;
						} else { $pyjs['__active_exception_stack__'] = $pyjs['__last_exception_stack__']; $pyjs['__last_exception_stack__'] = null; throw $pyjs_try_err; }
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=169;
				$pyjs['track']['lineno']=169;
				var $pyjs__ret = res;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, ['args',['kwargs'],['self'],['closeOnSuccess', false]]);
			$cls_definition['doSave'] = $method;
			$pyjs['track']['lineno']=171;
			$method = $pyjs__bind_method2('unserialize', function(data) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0c233152b063d881f43f698ef7b05ddf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter7_nextval,$iter7_iter,$iter7_array,$pyjs__trackstack_size_1,$iter7_idx,$iter7_type,bone;
				$pyjs['track']={'module':'widgets.edit', 'lineno':171};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=171;
				$pyjs['track']['lineno']=175;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter7_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['bones']['values']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_111_err){if (!$p['isinstance']($pyjs_dbg_111_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_111_err);}throw $pyjs_dbg_111_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_112_err){if (!$p['isinstance']($pyjs_dbg_112_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_112_err);}throw $pyjs_dbg_112_err;
}})();
				$iter7_nextval=$p['__iter_prepare']($iter7_iter,false);
				while (typeof($p['__wrapped_next']($iter7_nextval)['$nextval']) != 'undefined') {
					bone = $iter7_nextval['$nextval'];
					$pyjs['track']['lineno']=176;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['unserialize'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_113_err){if (!$p['isinstance']($pyjs_dbg_113_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_113_err);}throw $pyjs_dbg_113_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.edit';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['data']]);
			$cls_definition['unserialize'] = $method;
			$pyjs['track']['lineno']=20;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('InternalEdit', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=178;
		$m['parseHashParameters'] = function(src, prefix) {
			if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
			if (typeof prefix == 'undefined') prefix=arguments['callee']['__args__'][3][1];
			var newRes,$iter8_iter,$and18,res,$iter11_idx,$iter8_idx,keys,$iter11_iter,$iter8_type,v,$iter8_nextval,$iter11_array,$iter11_nextval,$and19,processedPrefixes,k,$iter11_type,$iter8_array,$pyjs__trackstack_size_1,newPrefix;
			$pyjs['track']={'module':'widgets.edit','lineno':178};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='widgets.edit';
			$pyjs['track']['lineno']=178;
			$pyjs['track']['lineno']=190;
			res = (function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_114_err){if (!$p['isinstance']($pyjs_dbg_114_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_114_err);}throw $pyjs_dbg_114_err;
}})();
			$pyjs['track']['lineno']=191;
			processedPrefixes = (function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_115_err){if (!$p['isinstance']($pyjs_dbg_115_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_115_err);}throw $pyjs_dbg_115_err;
}})();
			$pyjs['track']['lineno']=192;
			$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
			$iter8_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
			return (function(){try{try{$pyjs['in_try_except'] += 1;
			return src['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_117_err){if (!$p['isinstance']($pyjs_dbg_117_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_117_err);}throw $pyjs_dbg_117_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_118_err){if (!$p['isinstance']($pyjs_dbg_118_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_118_err);}throw $pyjs_dbg_118_err;
}})();
			$iter8_nextval=$p['__iter_prepare']($iter8_iter,false);
			while (typeof($p['__wrapped_next']($iter8_nextval)['$nextval']) != 'undefined') {
				var $tupleassign6 = (function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['__ass_unpack']($iter8_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_116_err){if (!$p['isinstance']($pyjs_dbg_116_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_116_err);}throw $pyjs_dbg_116_err;
}})();
				k = $tupleassign6[0];
				v = $tupleassign6[1];
				$pyjs['track']['lineno']=193;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and18=prefix)?!$p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return k['startswith'](prefix);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_119_err){if (!$p['isinstance']($pyjs_dbg_119_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_119_err);}throw $pyjs_dbg_119_err;
}})()):$and18));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_120_err){if (!$p['isinstance']($pyjs_dbg_120_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_120_err);}throw $pyjs_dbg_120_err;
}})()) {
					$pyjs['track']['lineno']=194;
					continue;
				}
				$pyjs['track']['lineno']=195;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](prefix);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_121_err){if (!$p['isinstance']($pyjs_dbg_121_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_121_err);}throw $pyjs_dbg_121_err;
}})()) {
					$pyjs['track']['lineno']=196;
					k = (function(){try{try{$pyjs['in_try_except'] += 1;
					return k['$$replace'](prefix, '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_122_err){if (!$p['isinstance']($pyjs_dbg_122_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_122_err);}throw $pyjs_dbg_122_err;
}})();
				}
				$pyjs['track']['lineno']=197;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool'](k['__contains__']('.')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_123_err){if (!$p['isinstance']($pyjs_dbg_123_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_123_err);}throw $pyjs_dbg_123_err;
}})()) {
					$pyjs['track']['lineno']=198;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return res['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_124_err){if (!$p['isinstance']($pyjs_dbg_124_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_124_err);}throw $pyjs_dbg_124_err;
}})()['__contains__'](k));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_125_err){if (!$p['isinstance']($pyjs_dbg_125_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_125_err);}throw $pyjs_dbg_125_err;
}})()) {
						$pyjs['track']['lineno']=199;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['isinstance'](res['__getitem__'](k), $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_126_err){if (!$p['isinstance']($pyjs_dbg_126_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_126_err);}throw $pyjs_dbg_126_err;
}})()));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_127_err){if (!$p['isinstance']($pyjs_dbg_127_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_127_err);}throw $pyjs_dbg_127_err;
}})()) {
							$pyjs['track']['lineno']=200;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return res['__setitem__'](k, (function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['list']([res['__getitem__'](k)]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_128_err){if (!$p['isinstance']($pyjs_dbg_128_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_128_err);}throw $pyjs_dbg_128_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_129_err){if (!$p['isinstance']($pyjs_dbg_129_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_129_err);}throw $pyjs_dbg_129_err;
}})();
						}
						$pyjs['track']['lineno']=201;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return res['__getitem__'](k)['append'](v);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_130_err){if (!$p['isinstance']($pyjs_dbg_130_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_130_err);}throw $pyjs_dbg_130_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=203;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return res['__setitem__'](k, v);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_131_err){if (!$p['isinstance']($pyjs_dbg_131_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_131_err);}throw $pyjs_dbg_131_err;
}})();
					}
				}
				else {
					$pyjs['track']['lineno']=205;
					newPrefix = $p['__getslice'](k, 0, (function(){try{try{$pyjs['in_try_except'] += 1;
					return k['find']('.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_132_err){if (!$p['isinstance']($pyjs_dbg_132_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_132_err);}throw $pyjs_dbg_132_err;
}})());
					$pyjs['track']['lineno']=206;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](processedPrefixes['__contains__'](newPrefix));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_133_err){if (!$p['isinstance']($pyjs_dbg_133_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_133_err);}throw $pyjs_dbg_133_err;
}})()) {
						$pyjs['track']['lineno']=207;
						continue;
					}
					$pyjs['track']['lineno']=208;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return processedPrefixes['append'](newPrefix);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_134_err){if (!$p['isinstance']($pyjs_dbg_134_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_134_err);}throw $pyjs_dbg_134_err;
}})();
					$pyjs['track']['lineno']=209;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return res['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_135_err){if (!$p['isinstance']($pyjs_dbg_135_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_135_err);}throw $pyjs_dbg_135_err;
}})()['__contains__'](newPrefix));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_136_err){if (!$p['isinstance']($pyjs_dbg_136_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_136_err);}throw $pyjs_dbg_136_err;
}})()) {
						$pyjs['track']['lineno']=210;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['isinstance'](res['__getitem__'](newPrefix), $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_137_err){if (!$p['isinstance']($pyjs_dbg_137_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_137_err);}throw $pyjs_dbg_137_err;
}})()));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_138_err){if (!$p['isinstance']($pyjs_dbg_138_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_138_err);}throw $pyjs_dbg_138_err;
}})()) {
							$pyjs['track']['lineno']=211;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return res['__setitem__'](newPrefix, (function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['list']([res['__getitem__'](newPrefix)]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_139_err){if (!$p['isinstance']($pyjs_dbg_139_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_139_err);}throw $pyjs_dbg_139_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_140_err){if (!$p['isinstance']($pyjs_dbg_140_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_140_err);}throw $pyjs_dbg_140_err;
}})();
						}
						$pyjs['track']['lineno']=212;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return res['__getitem__'](newPrefix)['append']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call(null, $m['parseHashParameters'], null, null, [{'prefix':(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('%s%s.', (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['tuple']([prefix, newPrefix]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_141_err){if (!$p['isinstance']($pyjs_dbg_141_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_141_err);}throw $pyjs_dbg_141_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_142_err){if (!$p['isinstance']($pyjs_dbg_142_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_142_err);}throw $pyjs_dbg_142_err;
}})()}, src]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_143_err){if (!$p['isinstance']($pyjs_dbg_143_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_143_err);}throw $pyjs_dbg_143_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_144_err){if (!$p['isinstance']($pyjs_dbg_144_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_144_err);}throw $pyjs_dbg_144_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=214;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return res['__setitem__'](newPrefix, (function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call(null, $m['parseHashParameters'], null, null, [{'prefix':(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('%s%s.', (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['tuple']([prefix, newPrefix]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_145_err){if (!$p['isinstance']($pyjs_dbg_145_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_145_err);}throw $pyjs_dbg_145_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_146_err){if (!$p['isinstance']($pyjs_dbg_146_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_146_err);}throw $pyjs_dbg_146_err;
}})()}, src]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_147_err){if (!$p['isinstance']($pyjs_dbg_147_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_147_err);}throw $pyjs_dbg_147_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_148_err){if (!$p['isinstance']($pyjs_dbg_148_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_148_err);}throw $pyjs_dbg_148_err;
}})();
					}
				}
			}
			if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
				$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
				$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
			}
			$pyjs['track']['module']='widgets.edit';
			$pyjs['track']['lineno']=215;
			if ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['all'](function(){
				var $iter9_iter,$iter9_nextval,$collcomp2,$iter9_array,$iter9_idx,$pyjs__trackstack_size_1,x,$iter9_type;
	$collcomp2 = $p['list']();
			$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
			$iter9_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
			return (function(){try{try{$pyjs['in_try_except'] += 1;
			return res['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_149_err){if (!$p['isinstance']($pyjs_dbg_149_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_149_err);}throw $pyjs_dbg_149_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_150_err){if (!$p['isinstance']($pyjs_dbg_150_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_150_err);}throw $pyjs_dbg_150_err;
}})();
			$iter9_nextval=$p['__iter_prepare']($iter9_iter,false);
			while (typeof($p['__wrapped_next']($iter9_nextval)['$nextval']) != 'undefined') {
				x = $iter9_nextval['$nextval'];
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $collcomp2['append']((function(){try{try{$pyjs['in_try_except'] += 1;
				return x['isdigit']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_151_err){if (!$p['isinstance']($pyjs_dbg_151_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_151_err);}throw $pyjs_dbg_151_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_152_err){if (!$p['isinstance']($pyjs_dbg_152_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_152_err);}throw $pyjs_dbg_152_err;
}})();
			}
			if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
				$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
				$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
			}
			$pyjs['track']['module']='widgets.edit';

	return $collcomp2;}());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_153_err){if (!$p['isinstance']($pyjs_dbg_153_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_153_err);}throw $pyjs_dbg_153_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_154_err){if (!$p['isinstance']($pyjs_dbg_154_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_154_err);}throw $pyjs_dbg_154_err;
}})()) {
				$pyjs['track']['lineno']=216;
				newRes = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_155_err){if (!$p['isinstance']($pyjs_dbg_155_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_155_err);}throw $pyjs_dbg_155_err;
}})();
				$pyjs['track']['lineno']=217;
				keys = function(){
					var $iter10_nextval,$iter10_idx,$collcomp3,$iter10_array,$pyjs__trackstack_size_1,$iter10_type,x,$iter10_iter;
	$collcomp3 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter10_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return res['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_156_err){if (!$p['isinstance']($pyjs_dbg_156_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_156_err);}throw $pyjs_dbg_156_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_157_err){if (!$p['isinstance']($pyjs_dbg_157_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_157_err);}throw $pyjs_dbg_157_err;
}})();
				$iter10_nextval=$p['__iter_prepare']($iter10_iter,false);
				while (typeof($p['__wrapped_next']($iter10_nextval)['$nextval']) != 'undefined') {
					x = $iter10_nextval['$nextval'];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp3['append']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['int'](x);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_158_err){if (!$p['isinstance']($pyjs_dbg_158_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_158_err);}throw $pyjs_dbg_158_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_159_err){if (!$p['isinstance']($pyjs_dbg_159_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_159_err);}throw $pyjs_dbg_159_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.edit';

	return $collcomp3;}();
				$pyjs['track']['lineno']=218;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return keys['sort']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_160_err){if (!$p['isinstance']($pyjs_dbg_160_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_160_err);}throw $pyjs_dbg_160_err;
}})();
				$pyjs['track']['lineno']=219;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter11_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return keys;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_161_err){if (!$p['isinstance']($pyjs_dbg_161_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_161_err);}throw $pyjs_dbg_161_err;
}})();
				$iter11_nextval=$p['__iter_prepare']($iter11_iter,false);
				while (typeof($p['__wrapped_next']($iter11_nextval)['$nextval']) != 'undefined') {
					k = $iter11_nextval['$nextval'];
					$pyjs['track']['lineno']=220;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return newRes['append'](res['__getitem__']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['str'](k);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_162_err){if (!$p['isinstance']($pyjs_dbg_162_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_162_err);}throw $pyjs_dbg_162_err;
}})()));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_163_err){if (!$p['isinstance']($pyjs_dbg_163_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_163_err);}throw $pyjs_dbg_163_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=221;
				$pyjs['track']['lineno']=221;
				var $pyjs__ret = newRes;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
			$pyjs['track']['lineno']=222;
			$pyjs['track']['lineno']=222;
			var $pyjs__ret = res;
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['parseHashParameters']['__name__'] = 'parseHashParameters';

		$m['parseHashParameters']['__bind_type__'] = 0;
		$m['parseHashParameters']['__args__'] = [null,null,['src'],['prefix', '']];
		$pyjs['track']['lineno']=225;
		$m['EditWidget'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'widgets.edit';
			$cls_definition['__md5__'] = '72afed7bc698473860a949b394dea93e';
			$pyjs['track']['lineno']=226;
			$cls_definition['appList'] = 'list';
			$pyjs['track']['lineno']=227;
			$cls_definition['appHierarchy'] = 'hierarchy';
			$pyjs['track']['lineno']=228;
			$cls_definition['appTree'] = 'tree';
			$pyjs['track']['lineno']=229;
			$cls_definition['appSingleton'] = 'singleton';
			$pyjs['track']['lineno']=230;
			$cls_definition['__editIdx_'] = $constant_int_0;
			$pyjs['track']['lineno']=232;
			$method = $pyjs__bind_method2('__init__', function(modul, applicationType, key, node, skelType, clone, hashArgs, logaction) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,8,arguments['length']-1));

					var kwargs = arguments['length'] >= 9 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					modul = arguments[1];
					applicationType = arguments[2];
					key = arguments[3];
					node = arguments[4];
					skelType = arguments[5];
					clone = arguments[6];
					hashArgs = arguments[7];
					logaction = arguments[8];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,9,arguments['length']-1));

					var kwargs = arguments['length'] >= 10 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '72afed7bc698473860a949b394dea93e') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof logaction != 'undefined') {
						if (logaction !== null && typeof logaction['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = logaction;
							logaction = arguments[9];
						}
					} else 					if (typeof hashArgs != 'undefined') {
						if (hashArgs !== null && typeof hashArgs['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = hashArgs;
							hashArgs = arguments[9];
						}
					} else 					if (typeof clone != 'undefined') {
						if (clone !== null && typeof clone['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = clone;
							clone = arguments[9];
						}
					} else 					if (typeof skelType != 'undefined') {
						if (skelType !== null && typeof skelType['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = skelType;
							skelType = arguments[9];
						}
					} else 					if (typeof node != 'undefined') {
						if (node !== null && typeof node['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = node;
							node = arguments[9];
						}
					} else 					if (typeof key != 'undefined') {
						if (key !== null && typeof key['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = key;
							key = arguments[9];
						}
					} else 					if (typeof applicationType != 'undefined') {
						if (applicationType !== null && typeof applicationType['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = applicationType;
							applicationType = arguments[9];
						}
					} else 					if (typeof modul != 'undefined') {
						if (modul !== null && typeof modul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modul;
							modul = arguments[9];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[9];
						}
					} else {
					}
				}
				if (typeof key == 'undefined') key=arguments['callee']['__args__'][5][1];
				if (typeof node == 'undefined') node=arguments['callee']['__args__'][6][1];
				if (typeof skelType == 'undefined') skelType=arguments['callee']['__args__'][7][1];
				if (typeof clone == 'undefined') clone=arguments['callee']['__args__'][8][1];
				if (typeof hashArgs == 'undefined') hashArgs=arguments['callee']['__args__'][9][1];
				if (typeof logaction == 'undefined') logaction=arguments['callee']['__args__'][10][1];
				var $or5,warningNode,$or10,$or7,$or6,$or9,$or8,warningSpan,$add6,$add7,$add5,$add8;
				$pyjs['track']={'module':'widgets.edit', 'lineno':232};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=232;
				$pyjs['track']['lineno']=252;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['EditWidget'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_164_err){if (!$p['isinstance']($pyjs_dbg_164_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_164_err);}throw $pyjs_dbg_164_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_165_err){if (!$p['isinstance']($pyjs_dbg_165_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_165_err);}throw $pyjs_dbg_165_err;
}})();
				$pyjs['track']['lineno']=253;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=255;
				if (!( (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([$p['getattr']($m['EditWidget'], 'appList'), $p['getattr']($m['EditWidget'], 'appHierarchy'), $p['getattr']($m['EditWidget'], 'appTree'), $p['getattr']($m['EditWidget'], 'appSingleton')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_166_err){if (!$p['isinstance']($pyjs_dbg_166_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_166_err);}throw $pyjs_dbg_166_err;
}})()['__contains__'](applicationType) )) {
				   throw $p['AssertionError']();
				 }
				$pyjs['track']['lineno']=257;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($or5=$p['op_eq'](applicationType, $p['getattr']($m['EditWidget'], 'appHierarchy')))?$or5:$p['op_eq'](applicationType, $p['getattr']($m['EditWidget'], 'appTree'))));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_167_err){if (!$p['isinstance']($pyjs_dbg_167_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_167_err);}throw $pyjs_dbg_167_err;
}})()) {
					$pyjs['track']['lineno']=258;
					if (!( ($p['bool']($or7=!$p['op_is']((typeof id == "undefined"?$m['id']:id), null))?$or7:!$p['op_is'](node, null)) )) {
					   throw $p['AssertionError']();
					 }
				}
				$pyjs['track']['lineno']=260;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](clone);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_168_err){if (!$p['isinstance']($pyjs_dbg_168_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_168_err);}throw $pyjs_dbg_168_err;
}})()) {
					$pyjs['track']['lineno']=261;
					if (!( !$p['op_is']((typeof id == "undefined"?$m['id']:id), null) )) {
					   throw $p['AssertionError']();
					 }
					$pyjs['track']['lineno']=262;
					if (!( !$p['bool']($p['op_eq'](applicationType, $p['getattr']($m['EditWidget'], 'appSingleton'))) )) {
					   throw $p['AssertionError']();
					 }
					$pyjs['track']['lineno']=263;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($or9=$p['op_eq'](applicationType, $p['getattr']($m['EditWidget'], 'appHierarchy')))?$or9:$p['op_eq'](applicationType, $p['getattr']($m['EditWidget'], 'appTree'))));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_169_err){if (!$p['isinstance']($pyjs_dbg_169_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_169_err);}throw $pyjs_dbg_169_err;
}})()) {
						$pyjs['track']['lineno']=264;
						if (!( !$p['op_is'](node, null) )) {
						   throw $p['AssertionError']();
						 }
					}
					$pyjs['track']['lineno']=265;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_eq'](applicationType, $p['getattr']($m['EditWidget'], 'appTree')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_170_err){if (!$p['isinstance']($pyjs_dbg_170_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_170_err);}throw $pyjs_dbg_170_err;
}})()) {
						$pyjs['track']['lineno']=266;
						if (!( !$p['op_is'](node, null) )) {
						   throw $p['AssertionError']();
						 }
					}
					$pyjs['track']['lineno']=268;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('clone_of', key) : $p['setattr'](self, 'clone_of', key); 
				}
				else {
					$pyjs['track']['lineno']=270;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('clone_of', null) : $p['setattr'](self, 'clone_of', null); 
				}
				$pyjs['track']['lineno']=273;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('editIdx', $p['getattr']($m['EditWidget'], '__editIdx_')) : $p['setattr'](self, 'editIdx', $p['getattr']($m['EditWidget'], '__editIdx_')); 
				$pyjs['track']['lineno']=274;
				$m['EditWidget']['__is_instance__'] && typeof $m['EditWidget']['__setattr__'] == 'function' ? $m['EditWidget']['__setattr__']('__editIdx_', $p['__op_add']($add5=$p['getattr']($m['EditWidget'], '__editIdx_'),$add6=$constant_int_1)) : $p['setattr']($m['EditWidget'], '__editIdx_', $p['__op_add']($add5=$p['getattr']($m['EditWidget'], '__editIdx_'),$add6=$constant_int_1)); 
				$pyjs['track']['lineno']=275;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('applicationType', applicationType) : $p['setattr'](self, 'applicationType', applicationType); 
				$pyjs['track']['lineno']=276;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('key', key) : $p['setattr'](self, 'key', key); 
				$pyjs['track']['lineno']=277;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('node', node) : $p['setattr'](self, 'node', node); 
				$pyjs['track']['lineno']=278;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('skelType', skelType) : $p['setattr'](self, 'skelType', skelType); 
				$pyjs['track']['lineno']=279;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('clone', clone) : $p['setattr'](self, 'clone', clone); 
				$pyjs['track']['lineno']=280;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('bones', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_171_err){if (!$p['isinstance']($pyjs_dbg_171_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_171_err);}throw $pyjs_dbg_171_err;
}})()) : $p['setattr'](self, 'bones', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_171_err){if (!$p['isinstance']($pyjs_dbg_171_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_171_err);}throw $pyjs_dbg_171_err;
}})()); 
				$pyjs['track']['lineno']=281;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('closeOnSuccess', false) : $p['setattr'](self, 'closeOnSuccess', false); 
				$pyjs['track']['lineno']=282;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('logaction', logaction) : $p['setattr'](self, 'logaction', logaction); 
				$pyjs['track']['lineno']=284;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_lastData', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_172_err){if (!$p['isinstance']($pyjs_dbg_172_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_172_err);}throw $pyjs_dbg_172_err;
}})()) : $p['setattr'](self, '_lastData', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_172_err){if (!$p['isinstance']($pyjs_dbg_172_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_172_err);}throw $pyjs_dbg_172_err;
}})()); 
				$pyjs['track']['lineno']=285;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](hashArgs);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_173_err){if (!$p['isinstance']($pyjs_dbg_173_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_173_err);}throw $pyjs_dbg_173_err;
}})()) {
					$pyjs['track']['lineno']=286;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_hashArgs', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['parseHashParameters'](hashArgs);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_174_err){if (!$p['isinstance']($pyjs_dbg_174_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_174_err);}throw $pyjs_dbg_174_err;
}})()) : $p['setattr'](self, '_hashArgs', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['parseHashParameters'](hashArgs);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_174_err){if (!$p['isinstance']($pyjs_dbg_174_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_174_err);}throw $pyjs_dbg_174_err;
}})()); 
					$pyjs['track']['lineno']=287;
					warningNode = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['TextNode']($p['__op_add']($add7='Warning: Values shown below got overriden by the Link you clicked on and do NOT represent the actual values!\n',$add8='Doublecheck them before saving!'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_175_err){if (!$p['isinstance']($pyjs_dbg_175_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_175_err);}throw $pyjs_dbg_175_err;
}})();
					$pyjs['track']['lineno']=289;
					warningSpan = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_176_err){if (!$p['isinstance']($pyjs_dbg_176_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_176_err);}throw $pyjs_dbg_176_err;
}})();
					$pyjs['track']['lineno']=290;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return warningSpan['__getitem__']('class')['append']('warning');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_177_err){if (!$p['isinstance']($pyjs_dbg_177_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_177_err);}throw $pyjs_dbg_177_err;
}})();
					$pyjs['track']['lineno']=291;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return warningSpan['appendChild'](warningNode);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_178_err){if (!$p['isinstance']($pyjs_dbg_178_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_178_err);}throw $pyjs_dbg_178_err;
}})();
					$pyjs['track']['lineno']=292;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild'](warningSpan);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_179_err){if (!$p['isinstance']($pyjs_dbg_179_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_179_err);}throw $pyjs_dbg_179_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=294;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_hashArgs', null) : $p['setattr'](self, '_hashArgs', null); 
				}
				$pyjs['track']['lineno']=296;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('editTaskID', null) : $p['setattr'](self, 'editTaskID', null); 
				$pyjs['track']['lineno']=297;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('wasInitialRequest', true) : $p['setattr'](self, 'wasInitialRequest', true); 
				$pyjs['track']['lineno']=298;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('actionbar', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['ActionBar']($p['getattr'](self, 'modul'), $p['getattr'](self, 'applicationType'), ($p['bool']($p['getattr'](self, 'key'))? ('edit') : ('add')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_180_err){if (!$p['isinstance']($pyjs_dbg_180_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_180_err);}throw $pyjs_dbg_180_err;
}})()) : $p['setattr'](self, 'actionbar', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['ActionBar']($p['getattr'](self, 'modul'), $p['getattr'](self, 'applicationType'), ($p['bool']($p['getattr'](self, 'key'))? ('edit') : ('add')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_180_err){if (!$p['isinstance']($pyjs_dbg_180_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_180_err);}throw $pyjs_dbg_180_err;
}})()); 
				$pyjs['track']['lineno']=299;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'actionbar'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_181_err){if (!$p['isinstance']($pyjs_dbg_181_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_181_err);}throw $pyjs_dbg_181_err;
}})();
				$pyjs['track']['lineno']=300;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('form', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Form']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_182_err){if (!$p['isinstance']($pyjs_dbg_182_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_182_err);}throw $pyjs_dbg_182_err;
}})()) : $p['setattr'](self, 'form', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Form']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_182_err){if (!$p['isinstance']($pyjs_dbg_182_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_182_err);}throw $pyjs_dbg_182_err;
}})()); 
				$pyjs['track']['lineno']=301;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'form'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_183_err){if (!$p['isinstance']($pyjs_dbg_183_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_183_err);}throw $pyjs_dbg_183_err;
}})();
				$pyjs['track']['lineno']=302;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['actionbar']['setActions']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list'](['save.close', 'save.continue', 'reset']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_184_err){if (!$p['isinstance']($pyjs_dbg_184_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_184_err);}throw $pyjs_dbg_184_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_185_err){if (!$p['isinstance']($pyjs_dbg_185_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_185_err);}throw $pyjs_dbg_185_err;
}})();
				$pyjs['track']['lineno']=303;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['reloadData']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_186_err){if (!$p['isinstance']($pyjs_dbg_186_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_186_err);}throw $pyjs_dbg_186_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['modul'],['applicationType'],['key', $constant_int_0],['node', null],['skelType', null],['clone', false],['hashArgs', null],['logaction', 'Entry saved!']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=305;
			$method = $pyjs__bind_method2('showErrorMsg', function(req, code) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					req = arguments[1];
					code = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 3)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '72afed7bc698473860a949b394dea93e') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof req == 'undefined') req=arguments['callee']['__args__'][3][1];
				if (typeof code == 'undefined') code=arguments['callee']['__args__'][4][1];
				var $or14,errorDiv,$or11,$or13,$or12,txt,$and21,$and20;
				$pyjs['track']={'module':'widgets.edit', 'lineno':305};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=305;
				$pyjs['track']['lineno']=309;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'actionbar')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_187_err){if (!$p['isinstance']($pyjs_dbg_187_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_187_err);}throw $pyjs_dbg_187_err;
}})();
				$pyjs['track']['lineno']=310;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'form')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_188_err){if (!$p['isinstance']($pyjs_dbg_188_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_188_err);}throw $pyjs_dbg_188_err;
}})();
				$pyjs['track']['lineno']=311;
				errorDiv = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_189_err){if (!$p['isinstance']($pyjs_dbg_189_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_189_err);}throw $pyjs_dbg_189_err;
}})();
				$pyjs['track']['lineno']=312;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return errorDiv['__getitem__']('class')['append']('error_msg');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_190_err){if (!$p['isinstance']($pyjs_dbg_190_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_190_err);}throw $pyjs_dbg_190_err;
}})();
				$pyjs['track']['lineno']=313;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and20=code)?($p['bool']($or11=$p['op_eq'](code, $constant_int_401))?$or11:$p['op_eq'](code, $constant_int_403)):$and20));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_191_err){if (!$p['isinstance']($pyjs_dbg_191_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_191_err);}throw $pyjs_dbg_191_err;
}})()) {
					$pyjs['track']['lineno']=314;
					txt = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Access denied!');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_192_err){if (!$p['isinstance']($pyjs_dbg_192_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_192_err);}throw $pyjs_dbg_192_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=316;
					txt = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('An unknown error occurred!');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_193_err){if (!$p['isinstance']($pyjs_dbg_193_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_193_err);}throw $pyjs_dbg_193_err;
}})();
				}
				$pyjs['track']['lineno']=317;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return errorDiv['__getitem__']('class')['append']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('error_code_%s', ($p['bool']($or13=code)?$or13:$constant_int_0));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_194_err){if (!$p['isinstance']($pyjs_dbg_194_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_194_err);}throw $pyjs_dbg_194_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_195_err){if (!$p['isinstance']($pyjs_dbg_195_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_195_err);}throw $pyjs_dbg_195_err;
}})();
				$pyjs['track']['lineno']=318;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return errorDiv['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['TextNode'](txt);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_196_err){if (!$p['isinstance']($pyjs_dbg_196_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_196_err);}throw $pyjs_dbg_196_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_197_err){if (!$p['isinstance']($pyjs_dbg_197_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_197_err);}throw $pyjs_dbg_197_err;
}})();
				$pyjs['track']['lineno']=319;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](errorDiv);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_198_err){if (!$p['isinstance']($pyjs_dbg_198_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_198_err);}throw $pyjs_dbg_198_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['req', null],['code', null]]);
			$cls_definition['showErrorMsg'] = $method;
			$pyjs['track']['lineno']=321;
			$method = $pyjs__bind_method2('reloadData', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '72afed7bc698473860a949b394dea93e') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.edit', 'lineno':321};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=321;
				$pyjs['track']['lineno']=322;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['save']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_199_err){if (!$p['isinstance']($pyjs_dbg_199_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_199_err);}throw $pyjs_dbg_199_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_200_err){if (!$p['isinstance']($pyjs_dbg_200_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_200_err);}throw $pyjs_dbg_200_err;
}})();
				$pyjs['track']['lineno']=323;
				$pyjs['track']['lineno']=323;
				var $pyjs__ret = null;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['reloadData'] = $method;
			$pyjs['track']['lineno']=325;
			$method = $pyjs__bind_method2('save', function(data) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '72afed7bc698473860a949b394dea93e') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and27,$and26,$or15,$or17,$or16,$and23,$and22,$or18,$and25,$and24;
				$pyjs['track']={'module':'widgets.edit', 'lineno':325};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=325;
				$pyjs['track']['lineno']=334;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('wasInitialRequest', !$p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_201_err){if (!$p['isinstance']($pyjs_dbg_201_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_201_err);}throw $pyjs_dbg_201_err;
}})(), $constant_int_0) == 1))) : $p['setattr'](self, 'wasInitialRequest', !$p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_201_err){if (!$p['isinstance']($pyjs_dbg_201_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_201_err);}throw $pyjs_dbg_201_err;
}})(), $constant_int_0) == 1))); 
				$pyjs['track']['lineno']=335;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](self, 'modul'), '_tasks'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_202_err){if (!$p['isinstance']($pyjs_dbg_202_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_202_err);}throw $pyjs_dbg_202_err;
}})()) {
					$pyjs['track']['lineno']=336;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'secure':($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['len'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_205_err){if (!$p['isinstance']($pyjs_dbg_205_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_205_err);}throw $pyjs_dbg_205_err;
}})(), $constant_int_0) == 1), 'successHandler':$p['getattr'](self, 'setData'), 'failureHandler':$p['getattr'](self, 'showErrorMsg')}, null, (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('/admin/%s/execute/%s', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['tuple']([$p['getattr'](self, 'modul'), $p['getattr'](self, 'key')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_203_err){if (!$p['isinstance']($pyjs_dbg_203_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_203_err);}throw $pyjs_dbg_203_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_204_err){if (!$p['isinstance']($pyjs_dbg_204_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_204_err);}throw $pyjs_dbg_204_err;
}})(), data]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_206_err){if (!$p['isinstance']($pyjs_dbg_206_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_206_err);}throw $pyjs_dbg_206_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](self, 'applicationType'), $p['getattr']($m['EditWidget'], 'appList')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_207_err){if (!$p['isinstance']($pyjs_dbg_207_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_207_err);}throw $pyjs_dbg_207_err;
}})()) {
					$pyjs['track']['lineno']=341;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and22=$p['getattr'](self, 'key'))?($p['bool']($or15=!$p['bool']($p['getattr'](self, 'clone')))?$or15:!$p['bool'](data)):$and22));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_208_err){if (!$p['isinstance']($pyjs_dbg_208_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_208_err);}throw $pyjs_dbg_208_err;
}})()) {
						$pyjs['track']['lineno']=342;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'secure':($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['len'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_210_err){if (!$p['isinstance']($pyjs_dbg_210_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_210_err);}throw $pyjs_dbg_210_err;
}})(), $constant_int_0) == 1), 'successHandler':$p['getattr'](self, 'setData'), 'failureHandler':$p['getattr'](self, 'showErrorMsg')}, $p['getattr'](self, 'modul'), (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('edit/%s', $p['getattr'](self, 'key'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_209_err){if (!$p['isinstance']($pyjs_dbg_209_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_209_err);}throw $pyjs_dbg_209_err;
}})(), data]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_211_err){if (!$p['isinstance']($pyjs_dbg_211_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_211_err);}throw $pyjs_dbg_211_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=347;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'secure':($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['len'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_212_err){if (!$p['isinstance']($pyjs_dbg_212_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_212_err);}throw $pyjs_dbg_212_err;
}})(), $constant_int_0) == 1), 'successHandler':$p['getattr'](self, 'setData'), 'failureHandler':$p['getattr'](self, 'showErrorMsg')}, $p['getattr'](self, 'modul'), 'add', data]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_213_err){if (!$p['isinstance']($pyjs_dbg_213_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_213_err);}throw $pyjs_dbg_213_err;
}})();
					}
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](self, 'applicationType'), $p['getattr']($m['EditWidget'], 'appHierarchy')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_214_err){if (!$p['isinstance']($pyjs_dbg_214_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_214_err);}throw $pyjs_dbg_214_err;
}})()) {
					$pyjs['track']['lineno']=352;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and24=$p['getattr'](self, 'key'))?($p['bool']($or17=!$p['bool']($p['getattr'](self, 'clone')))?$or17:!$p['bool'](data)):$and24));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_215_err){if (!$p['isinstance']($pyjs_dbg_215_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_215_err);}throw $pyjs_dbg_215_err;
}})()) {
						$pyjs['track']['lineno']=353;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'secure':($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['len'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_217_err){if (!$p['isinstance']($pyjs_dbg_217_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_217_err);}throw $pyjs_dbg_217_err;
}})(), $constant_int_0) == 1), 'successHandler':$p['getattr'](self, 'setData'), 'failureHandler':$p['getattr'](self, 'showErrorMsg')}, $p['getattr'](self, 'modul'), (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('edit/%s', $p['getattr'](self, 'key'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_216_err){if (!$p['isinstance']($pyjs_dbg_216_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_216_err);}throw $pyjs_dbg_216_err;
}})(), data]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_218_err){if (!$p['isinstance']($pyjs_dbg_218_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_218_err);}throw $pyjs_dbg_218_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=358;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'secure':($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['len'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_220_err){if (!$p['isinstance']($pyjs_dbg_220_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_220_err);}throw $pyjs_dbg_220_err;
}})(), $constant_int_0) == 1), 'successHandler':$p['getattr'](self, 'setData'), 'failureHandler':$p['getattr'](self, 'showErrorMsg')}, $p['getattr'](self, 'modul'), (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('add/%s', $p['getattr'](self, 'node'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_219_err){if (!$p['isinstance']($pyjs_dbg_219_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_219_err);}throw $pyjs_dbg_219_err;
}})(), data]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_221_err){if (!$p['isinstance']($pyjs_dbg_221_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_221_err);}throw $pyjs_dbg_221_err;
}})();
					}
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](self, 'applicationType'), $p['getattr']($m['EditWidget'], 'appTree')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_222_err){if (!$p['isinstance']($pyjs_dbg_222_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_222_err);}throw $pyjs_dbg_222_err;
}})()) {
					$pyjs['track']['lineno']=363;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and26=$p['getattr'](self, 'key'))?!$p['bool']($p['getattr'](self, 'clone')):$and26));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_223_err){if (!$p['isinstance']($pyjs_dbg_223_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_223_err);}throw $pyjs_dbg_223_err;
}})()) {
						$pyjs['track']['lineno']=364;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'secure':($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['len'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_226_err){if (!$p['isinstance']($pyjs_dbg_226_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_226_err);}throw $pyjs_dbg_226_err;
}})(), $constant_int_0) == 1), 'successHandler':$p['getattr'](self, 'setData'), 'failureHandler':$p['getattr'](self, 'showErrorMsg')}, $p['getattr'](self, 'modul'), (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('edit/%s/%s', (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['tuple']([$p['getattr'](self, 'skelType'), $p['getattr'](self, 'key')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_224_err){if (!$p['isinstance']($pyjs_dbg_224_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_224_err);}throw $pyjs_dbg_224_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_225_err){if (!$p['isinstance']($pyjs_dbg_225_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_225_err);}throw $pyjs_dbg_225_err;
}})(), data]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_227_err){if (!$p['isinstance']($pyjs_dbg_227_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_227_err);}throw $pyjs_dbg_227_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=369;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'secure':($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['len'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_230_err){if (!$p['isinstance']($pyjs_dbg_230_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_230_err);}throw $pyjs_dbg_230_err;
}})(), $constant_int_0) == 1), 'successHandler':$p['getattr'](self, 'setData'), 'failureHandler':$p['getattr'](self, 'showErrorMsg')}, $p['getattr'](self, 'modul'), (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('add/%s/%s', (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['tuple']([$p['getattr'](self, 'skelType'), $p['getattr'](self, 'node')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_228_err){if (!$p['isinstance']($pyjs_dbg_228_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_228_err);}throw $pyjs_dbg_228_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_229_err){if (!$p['isinstance']($pyjs_dbg_229_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_229_err);}throw $pyjs_dbg_229_err;
}})(), data]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_231_err){if (!$p['isinstance']($pyjs_dbg_231_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_231_err);}throw $pyjs_dbg_231_err;
}})();
					}
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](self, 'applicationType'), $p['getattr']($m['EditWidget'], 'appSingleton')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_232_err){if (!$p['isinstance']($pyjs_dbg_232_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_232_err);}throw $pyjs_dbg_232_err;
}})()) {
					$pyjs['track']['lineno']=374;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'secure':($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['len'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_233_err){if (!$p['isinstance']($pyjs_dbg_233_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_233_err);}throw $pyjs_dbg_233_err;
}})(), $constant_int_0) == 1), 'successHandler':$p['getattr'](self, 'setData'), 'failureHandler':$p['getattr'](self, 'showErrorMsg')}, $p['getattr'](self, 'modul'), 'edit', data]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_234_err){if (!$p['isinstance']($pyjs_dbg_234_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_234_err);}throw $pyjs_dbg_234_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=379;
					$pyjs['__active_exception_stack__'] = null;
					throw ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['NotImplementedError']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_235_err){if (!$p['isinstance']($pyjs_dbg_235_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_235_err);}throw $pyjs_dbg_235_err;
}})());
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['data']]);
			$cls_definition['save'] = $method;
			$pyjs['track']['lineno']=381;
			$method = $pyjs__bind_method2('clear', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '72afed7bc698473860a949b394dea93e') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var c,$iter12_type,$iter12_array,$iter12_nextval,$iter12_iter,$pyjs__trackstack_size_1,$iter12_idx;
				$pyjs['track']={'module':'widgets.edit', 'lineno':381};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=381;
				$pyjs['track']['lineno']=385;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter12_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__getslice']($p['getattr']($p['getattr'](self, 'form'), '_children'), 0, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_236_err){if (!$p['isinstance']($pyjs_dbg_236_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_236_err);}throw $pyjs_dbg_236_err;
}})();
				$iter12_nextval=$p['__iter_prepare']($iter12_iter,false);
				while (typeof($p['__wrapped_next']($iter12_nextval)['$nextval']) != 'undefined') {
					c = $iter12_nextval['$nextval'];
					$pyjs['track']['lineno']=386;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['form']['removeChild'](c);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_237_err){if (!$p['isinstance']($pyjs_dbg_237_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_237_err);}throw $pyjs_dbg_237_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.edit';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['clear'] = $method;
			$pyjs['track']['lineno']=388;
			$method = $pyjs__bind_method2('closeOrContinue', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '72afed7bc698473860a949b394dea93e') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];

				$pyjs['track']={'module':'widgets.edit', 'lineno':388};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=388;
				$pyjs['track']['lineno']=389;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'closeOnSuccess'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_238_err){if (!$p['isinstance']($pyjs_dbg_238_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_238_err);}throw $pyjs_dbg_238_err;
}})()) {
					$pyjs['track']['lineno']=390;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_eq']($p['getattr'](self, 'modul'), '_tasks'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_239_err){if (!$p['isinstance']($pyjs_dbg_239_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_239_err);}throw $pyjs_dbg_239_err;
}})()) {
						$pyjs['track']['lineno']=391;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_240_err){if (!$p['isinstance']($pyjs_dbg_240_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_240_err);}throw $pyjs_dbg_240_err;
}})()['close']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_241_err){if (!$p['isinstance']($pyjs_dbg_241_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_241_err);}throw $pyjs_dbg_241_err;
}})();
						$pyjs['track']['lineno']=392;
						$pyjs['track']['lineno']=392;
						var $pyjs__ret = null;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
					$pyjs['track']['lineno']=394;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['conf']['__getitem__']('mainWindow')['removeWidget'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_242_err){if (!$p['isinstance']($pyjs_dbg_242_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_242_err);}throw $pyjs_dbg_242_err;
}})();
					$pyjs['track']['lineno']=395;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['NetworkService']['notifyChange']($p['getattr'](self, 'modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_243_err){if (!$p['isinstance']($pyjs_dbg_243_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_243_err);}throw $pyjs_dbg_243_err;
}})();
					$pyjs['track']['lineno']=396;
					$pyjs['track']['lineno']=396;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=398;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['clear']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_244_err){if (!$p['isinstance']($pyjs_dbg_244_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_244_err);}throw $pyjs_dbg_244_err;
}})();
				$pyjs['track']['lineno']=399;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('bones', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_245_err){if (!$p['isinstance']($pyjs_dbg_245_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_245_err);}throw $pyjs_dbg_245_err;
}})()) : $p['setattr'](self, 'bones', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_245_err){if (!$p['isinstance']($pyjs_dbg_245_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_245_err);}throw $pyjs_dbg_245_err;
}})()); 
				$pyjs['track']['lineno']=400;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['reloadData']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_246_err){if (!$p['isinstance']($pyjs_dbg_246_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_246_err);}throw $pyjs_dbg_246_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['closeOrContinue'] = $method;
			$pyjs['track']['lineno']=402;
			$method = $pyjs__bind_method2('doCloneHierarchy', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '72afed7bc698473860a949b394dea93e') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];

				$pyjs['track']={'module':'widgets.edit', 'lineno':402};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=402;
				$pyjs['track']['lineno']=403;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](self, 'applicationType'), $p['getattr']($m['EditWidget'], 'appHierarchy')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_247_err){if (!$p['isinstance']($pyjs_dbg_247_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_247_err);}throw $pyjs_dbg_247_err;
}})()) {
					$pyjs['track']['lineno']=404;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'secure':true, 'successHandler':$p['getattr'](self, 'cloneComplete')}, $p['getattr'](self, 'modul'), 'clone', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([['fromRepo', $p['getattr'](self, 'node')], ['toRepo', $p['getattr'](self, 'node')], ['fromParent', $p['getattr'](self, 'clone_of')], ['toParent', $p['getattr'](self, 'key')]]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_248_err){if (!$p['isinstance']($pyjs_dbg_248_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_248_err);}throw $pyjs_dbg_248_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_249_err){if (!$p['isinstance']($pyjs_dbg_249_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_249_err);}throw $pyjs_dbg_249_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=409;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'secure':true, 'successHandler':$p['getattr'](self, 'cloneComplete')}, $m['conf']['__getitem__']('modules')['__getitem__']($p['getattr'](self, 'modul'))['__getitem__']('rootNodeOf'), 'clone', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([['fromRepo', $p['getattr'](self, 'clone_of')], ['toRepo', $p['getattr'](self, 'key')]]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_250_err){if (!$p['isinstance']($pyjs_dbg_250_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_250_err);}throw $pyjs_dbg_250_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_251_err){if (!$p['isinstance']($pyjs_dbg_251_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_251_err);}throw $pyjs_dbg_251_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['doCloneHierarchy'] = $method;
			$pyjs['track']['lineno']=413;
			$method = $pyjs__bind_method2('cloneComplete', function(request) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					request = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '72afed7bc698473860a949b394dea93e') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var spanMsg,logDiv;
				$pyjs['track']={'module':'widgets.edit', 'lineno':413};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=413;
				$pyjs['track']['lineno']=414;
				logDiv = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_252_err){if (!$p['isinstance']($pyjs_dbg_252_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_252_err);}throw $pyjs_dbg_252_err;
}})();
				$pyjs['track']['lineno']=415;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return logDiv['__getitem__']('class')['append']('msg');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_253_err){if (!$p['isinstance']($pyjs_dbg_253_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_253_err);}throw $pyjs_dbg_253_err;
}})();
				$pyjs['track']['lineno']=416;
				spanMsg = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_254_err){if (!$p['isinstance']($pyjs_dbg_254_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_254_err);}throw $pyjs_dbg_254_err;
}})();
				$pyjs['track']['lineno']=417;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return spanMsg['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['TextNode']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('The hierarchy will be cloned in the background.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_255_err){if (!$p['isinstance']($pyjs_dbg_255_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_255_err);}throw $pyjs_dbg_255_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_256_err){if (!$p['isinstance']($pyjs_dbg_256_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_256_err);}throw $pyjs_dbg_256_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_257_err){if (!$p['isinstance']($pyjs_dbg_257_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_257_err);}throw $pyjs_dbg_257_err;
}})();
				$pyjs['track']['lineno']=418;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return spanMsg['__getitem__']('class')['append']('msgspan');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_258_err){if (!$p['isinstance']($pyjs_dbg_258_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_258_err);}throw $pyjs_dbg_258_err;
}})();
				$pyjs['track']['lineno']=419;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return logDiv['appendChild'](spanMsg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_259_err){if (!$p['isinstance']($pyjs_dbg_259_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_259_err);}throw $pyjs_dbg_259_err;
}})();
				$pyjs['track']['lineno']=421;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['log']('success', logDiv);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_260_err){if (!$p['isinstance']($pyjs_dbg_260_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_260_err);}throw $pyjs_dbg_260_err;
}})();
				$pyjs['track']['lineno']=422;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['closeOrContinue']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_261_err){if (!$p['isinstance']($pyjs_dbg_261_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_261_err);}throw $pyjs_dbg_261_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['request']]);
			$cls_definition['cloneComplete'] = $method;
			$pyjs['track']['lineno']=424;
			$method = $pyjs__bind_method2('setData', function(request, data, ignoreMissing, askHierarchyCloning) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 4)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 5, arguments['length']+1);
				} else {
					var self = arguments[0];
					request = arguments[1];
					data = arguments[2];
					ignoreMissing = arguments[3];
					askHierarchyCloning = arguments[4];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 5)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 5, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '72afed7bc698473860a949b394dea93e') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof request == 'undefined') request=arguments['callee']['__args__'][3][1];
				if (typeof data == 'undefined') data=arguments['callee']['__args__'][4][1];
				if (typeof ignoreMissing == 'undefined') ignoreMissing=arguments['callee']['__args__'][5][1];
				if (typeof askHierarchyCloning == 'undefined') askHierarchyCloning=arguments['callee']['__args__'][6][1];
				var $lambda2,descrLbl,$iter13_type,widget,$iter15_array,$and29,$and28,fshref,$iter13_idx,$iter17_type,$iter14_type,containerDiv,cat,$iter13_array,$and44,$iter14_array,$add12,$iter17_array,$iter15_iter,v,$and45,$and46,$and41,$and40,$and43,legend,$and42,$iter14_idx,$iter14_iter,$add11,$iter14_nextval,$or20,$or21,$or22,t,$iter17_nextval,hasMissing,fs,wdgGen,$and48,tmpDict,$iter15_type,$iter17_iter,logDiv,tabName,$and38,$and39,key,$iter17_idx,$and34,$and35,$and36,$and37,$and30,$and31,$and32,$and33,tmpList,$and47,$add9,$iter13_nextval,name,$iter13_iter,spanMsg,k,$iter15_idx,$pyjs__trackstack_size_2,bone,$iter15_nextval,$pyjs__trackstack_size_1,$add10,currRow,$or19,section,fieldSets;
				$pyjs['track']={'module':'widgets.edit', 'lineno':424};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=424;
				$pyjs['track']['lineno']=433;
				if (!( ($p['bool']($or19=request)?$or19:data) )) {
				   throw $p['AssertionError']();
				 }
				$pyjs['track']['lineno']=435;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](request);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_262_err){if (!$p['isinstance']($pyjs_dbg_262_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_262_err);}throw $pyjs_dbg_262_err;
}})()) {
					$pyjs['track']['lineno']=436;
					data = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['NetworkService']['decode'](request);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_263_err){if (!$p['isinstance']($pyjs_dbg_263_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_263_err);}throw $pyjs_dbg_263_err;
}})();
				}
				$pyjs['track']['lineno']=438;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and28=data['__contains__']('action'))?($p['bool']($or21=$p['op_eq'](data['__getitem__']('action'), 'addSuccess'))?$or21:$p['op_eq'](data['__getitem__']('action'), 'editSuccess')):$and28));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_264_err){if (!$p['isinstance']($pyjs_dbg_264_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_264_err);}throw $pyjs_dbg_264_err;
}})()) {
					$pyjs['track']['lineno']=439;
					logDiv = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_265_err){if (!$p['isinstance']($pyjs_dbg_265_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_265_err);}throw $pyjs_dbg_265_err;
}})();
					$pyjs['track']['lineno']=440;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return logDiv['__getitem__']('class')['append']('msg');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_266_err){if (!$p['isinstance']($pyjs_dbg_266_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_266_err);}throw $pyjs_dbg_266_err;
}})();
					$pyjs['track']['lineno']=441;
					spanMsg = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_267_err){if (!$p['isinstance']($pyjs_dbg_267_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_267_err);}throw $pyjs_dbg_267_err;
}})();
					$pyjs['track']['lineno']=443;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return spanMsg['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['TextNode']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']($p['getattr'](self, 'logaction'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_268_err){if (!$p['isinstance']($pyjs_dbg_268_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_268_err);}throw $pyjs_dbg_268_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_269_err){if (!$p['isinstance']($pyjs_dbg_269_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_269_err);}throw $pyjs_dbg_269_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_270_err){if (!$p['isinstance']($pyjs_dbg_270_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_270_err);}throw $pyjs_dbg_270_err;
}})();
					$pyjs['track']['lineno']=444;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return spanMsg['__getitem__']('class')['append']('msgspan');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_271_err){if (!$p['isinstance']($pyjs_dbg_271_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_271_err);}throw $pyjs_dbg_271_err;
}})();
					$pyjs['track']['lineno']=445;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return logDiv['appendChild'](spanMsg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_272_err){if (!$p['isinstance']($pyjs_dbg_272_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_272_err);}throw $pyjs_dbg_272_err;
}})();
					$pyjs['track']['lineno']=447;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['conf']['__getitem__']('modules')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_273_err){if (!$p['isinstance']($pyjs_dbg_273_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_273_err);}throw $pyjs_dbg_273_err;
}})()['__contains__']($p['getattr'](self, 'modul')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_274_err){if (!$p['isinstance']($pyjs_dbg_274_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_274_err);}throw $pyjs_dbg_274_err;
}})()) {
						$pyjs['track']['lineno']=448;
						spanMsg = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_275_err){if (!$p['isinstance']($pyjs_dbg_275_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_275_err);}throw $pyjs_dbg_275_err;
}})();
						$pyjs['track']['lineno']=449;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return self['modul']['startswith']('_');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_276_err){if (!$p['isinstance']($pyjs_dbg_276_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_276_err);}throw $pyjs_dbg_276_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_277_err){if (!$p['isinstance']($pyjs_dbg_277_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_277_err);}throw $pyjs_dbg_277_err;
}})()) {
							$pyjs['track']['lineno']=450;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return spanMsg['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
							return $m['html5']['TextNode']($p['getattr'](self, 'key'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_278_err){if (!$p['isinstance']($pyjs_dbg_278_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_278_err);}throw $pyjs_dbg_278_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_279_err){if (!$p['isinstance']($pyjs_dbg_279_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_279_err);}throw $pyjs_dbg_279_err;
}})();
						}
						else {
							$pyjs['track']['lineno']=452;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return spanMsg['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
							return $m['html5']['TextNode']($m['conf']['__getitem__']('modules')['__getitem__']($p['getattr'](self, 'modul'))['__getitem__']('name'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_280_err){if (!$p['isinstance']($pyjs_dbg_280_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_280_err);}throw $pyjs_dbg_280_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_281_err){if (!$p['isinstance']($pyjs_dbg_281_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_281_err);}throw $pyjs_dbg_281_err;
}})();
						}
						$pyjs['track']['lineno']=453;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return spanMsg['__getitem__']('class')['append']('modulspan');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_282_err){if (!$p['isinstance']($pyjs_dbg_282_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_282_err);}throw $pyjs_dbg_282_err;
}})();
						$pyjs['track']['lineno']=454;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return logDiv['appendChild'](spanMsg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_283_err){if (!$p['isinstance']($pyjs_dbg_283_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_283_err);}throw $pyjs_dbg_283_err;
}})();
					}
					$pyjs['track']['lineno']=456;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and30=(function(){try{try{$pyjs['in_try_except'] += 1;
					return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_284_err){if (!$p['isinstance']($pyjs_dbg_284_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_284_err);}throw $pyjs_dbg_284_err;
}})()['__contains__']('values'))?(function(){try{try{$pyjs['in_try_except'] += 1;
					return data['__getitem__']('values')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_285_err){if (!$p['isinstance']($pyjs_dbg_285_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_285_err);}throw $pyjs_dbg_285_err;
}})()['__contains__']('name'):$and30));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_286_err){if (!$p['isinstance']($pyjs_dbg_286_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_286_err);}throw $pyjs_dbg_286_err;
}})()) {
						$pyjs['track']['lineno']=457;
						spanMsg = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_287_err){if (!$p['isinstance']($pyjs_dbg_287_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_287_err);}throw $pyjs_dbg_287_err;
}})();
						$pyjs['track']['lineno']=459;
						name = data['__getitem__']('values')['__getitem__']('name');
						$pyjs['track']['lineno']=460;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['isinstance'](name, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_288_err){if (!$p['isinstance']($pyjs_dbg_288_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_288_err);}throw $pyjs_dbg_288_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_289_err){if (!$p['isinstance']($pyjs_dbg_289_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_289_err);}throw $pyjs_dbg_289_err;
}})()) {
							$pyjs['track']['lineno']=461;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
							return name['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_290_err){if (!$p['isinstance']($pyjs_dbg_290_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_290_err);}throw $pyjs_dbg_290_err;
}})()['__contains__']($m['conf']['__getitem__']('currentlanguage')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_291_err){if (!$p['isinstance']($pyjs_dbg_291_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_291_err);}throw $pyjs_dbg_291_err;
}})()) {
								$pyjs['track']['lineno']=462;
								name = name['__getitem__']($m['conf']['__getitem__']('currentlanguage'));
							}
							else {
								$pyjs['track']['lineno']=464;
								name = (function(){try{try{$pyjs['in_try_except'] += 1;
								return name['values']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_292_err){if (!$p['isinstance']($pyjs_dbg_292_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_292_err);}throw $pyjs_dbg_292_err;
}})();
							}
						}
						$pyjs['track']['lineno']=466;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['isinstance'](name, $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_293_err){if (!$p['isinstance']($pyjs_dbg_293_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_293_err);}throw $pyjs_dbg_293_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_294_err){if (!$p['isinstance']($pyjs_dbg_294_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_294_err);}throw $pyjs_dbg_294_err;
}})()) {
							$pyjs['track']['lineno']=467;
							name = (function(){try{try{$pyjs['in_try_except'] += 1;
							return ', '['join'](name);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_295_err){if (!$p['isinstance']($pyjs_dbg_295_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_295_err);}throw $pyjs_dbg_295_err;
}})();
						}
						$pyjs['track']['lineno']=469;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return spanMsg['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['TextNode']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['str']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['utils']['unescape'](name);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_296_err){if (!$p['isinstance']($pyjs_dbg_296_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_296_err);}throw $pyjs_dbg_296_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_297_err){if (!$p['isinstance']($pyjs_dbg_297_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_297_err);}throw $pyjs_dbg_297_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_298_err){if (!$p['isinstance']($pyjs_dbg_298_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_298_err);}throw $pyjs_dbg_298_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_299_err){if (!$p['isinstance']($pyjs_dbg_299_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_299_err);}throw $pyjs_dbg_299_err;
}})();
						$pyjs['track']['lineno']=470;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return spanMsg['__getitem__']('class')['append']('namespan');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_300_err){if (!$p['isinstance']($pyjs_dbg_300_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_300_err);}throw $pyjs_dbg_300_err;
}})();
						$pyjs['track']['lineno']=471;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return logDiv['appendChild'](spanMsg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_301_err){if (!$p['isinstance']($pyjs_dbg_301_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_301_err);}throw $pyjs_dbg_301_err;
}})();
					}
					$pyjs['track']['lineno']=473;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['conf']['__getitem__']('mainWindow')['log']('success', logDiv);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_302_err){if (!$p['isinstance']($pyjs_dbg_302_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_302_err);}throw $pyjs_dbg_302_err;
}})();
					$pyjs['track']['lineno']=475;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and32=askHierarchyCloning)?$p['getattr'](self, 'clone'):$and32));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_303_err){if (!$p['isinstance']($pyjs_dbg_303_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_303_err);}throw $pyjs_dbg_303_err;
}})()) {
						$pyjs['track']['lineno']=477;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](($p['bool']($and34=$p['op_eq']($p['getattr'](self, 'applicationType'), $p['getattr']($m['EditWidget'], 'appList')))?$m['conf']['__getitem__']('modules')['__getitem__']($p['getattr'](self, 'modul'))['__contains__']('rootNodeOf'):$and34));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_304_err){if (!$p['isinstance']($pyjs_dbg_304_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_304_err);}throw $pyjs_dbg_304_err;
}})()) {
							$pyjs['track']['lineno']=478;
							self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('key', data['__getitem__']('values')['__getitem__']('id')) : $p['setattr'](self, 'key', data['__getitem__']('values')['__getitem__']('id')); 
							$pyjs['track']['lineno']=479;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $pyjs_kwargs_call(null, $m['YesNoDialog'], null, null, [{'yesCallback':$p['getattr'](self, 'doCloneHierarchy'), 'noCallback':$p['getattr'](self, 'closeOrContinue')}, (function(){try{try{$pyjs['in_try_except'] += 1;
							return $m['translate']('Do you want to clone the entire hierarchy?');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_305_err){if (!$p['isinstance']($pyjs_dbg_305_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_305_err);}throw $pyjs_dbg_305_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_306_err){if (!$p['isinstance']($pyjs_dbg_306_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_306_err);}throw $pyjs_dbg_306_err;
}})();
							$pyjs['track']['lineno']=481;
							$pyjs['track']['lineno']=481;
							var $pyjs__ret = null;
							$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
							return $pyjs__ret;
						}
						else if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']($p['op_eq']($p['getattr'](self, 'applicationType'), $p['getattr']($m['EditWidget'], 'appHierarchy')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_307_err){if (!$p['isinstance']($pyjs_dbg_307_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_307_err);}throw $pyjs_dbg_307_err;
}})()) {
							$pyjs['track']['lineno']=484;
							self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('key', data['__getitem__']('values')['__getitem__']('id')) : $p['setattr'](self, 'key', data['__getitem__']('values')['__getitem__']('id')); 
							$pyjs['track']['lineno']=485;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $pyjs_kwargs_call(null, $m['YesNoDialog'], null, null, [{'yesCallback':$p['getattr'](self, 'doCloneHierarchy'), 'noCallback':$p['getattr'](self, 'closeOrContinue')}, (function(){try{try{$pyjs['in_try_except'] += 1;
							return $m['translate']('Do you want to clone all subentries of this item?');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_308_err){if (!$p['isinstance']($pyjs_dbg_308_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_308_err);}throw $pyjs_dbg_308_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_309_err){if (!$p['isinstance']($pyjs_dbg_309_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_309_err);}throw $pyjs_dbg_309_err;
}})();
							$pyjs['track']['lineno']=487;
							$pyjs['track']['lineno']=487;
							var $pyjs__ret = null;
							$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
							return $pyjs__ret;
						}
					}
					$pyjs['track']['lineno']=489;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['closeOrContinue']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_310_err){if (!$p['isinstance']($pyjs_dbg_310_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_310_err);}throw $pyjs_dbg_310_err;
}})();
					$pyjs['track']['lineno']=490;
					$pyjs['track']['lineno']=490;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=493;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['clear']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_311_err){if (!$p['isinstance']($pyjs_dbg_311_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_311_err);}throw $pyjs_dbg_311_err;
}})();
				$pyjs['track']['lineno']=494;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('bones', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_312_err){if (!$p['isinstance']($pyjs_dbg_312_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_312_err);}throw $pyjs_dbg_312_err;
}})()) : $p['setattr'](self, 'bones', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_312_err){if (!$p['isinstance']($pyjs_dbg_312_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_312_err);}throw $pyjs_dbg_312_err;
}})()); 
				$pyjs['track']['lineno']=495;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['actionbar']['resetLoadingState']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_313_err){if (!$p['isinstance']($pyjs_dbg_313_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_313_err);}throw $pyjs_dbg_313_err;
}})();
				$pyjs['track']['lineno']=496;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('dataCache', data) : $p['setattr'](self, 'dataCache', data); 
				$pyjs['track']['lineno']=498;
				tmpDict = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['utils']['boneListToDict'](data['__getitem__']('structure'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_314_err){if (!$p['isinstance']($pyjs_dbg_314_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_314_err);}throw $pyjs_dbg_314_err;
}})();
				$pyjs['track']['lineno']=499;
				fieldSets = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_315_err){if (!$p['isinstance']($pyjs_dbg_315_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_315_err);}throw $pyjs_dbg_315_err;
}})();
				$pyjs['track']['lineno']=500;
				currRow = $constant_int_0;
				$pyjs['track']['lineno']=501;
				hasMissing = false;
				$pyjs['track']['lineno']=503;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter13_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return data['__getitem__']('structure');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_317_err){if (!$p['isinstance']($pyjs_dbg_317_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_317_err);}throw $pyjs_dbg_317_err;
}})();
				$iter13_nextval=$p['__iter_prepare']($iter13_iter,false);
				while (typeof($p['__wrapped_next']($iter13_nextval)['$nextval']) != 'undefined') {
					var $tupleassign7 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter13_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_316_err){if (!$p['isinstance']($pyjs_dbg_316_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_316_err);}throw $pyjs_dbg_316_err;
}})();
					key = $tupleassign7[0];
					bone = $tupleassign7[1];
					$pyjs['track']['lineno']=504;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool'](bone['__getitem__']('visible')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_318_err){if (!$p['isinstance']($pyjs_dbg_318_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_318_err);}throw $pyjs_dbg_318_err;
}})()) {
						$pyjs['track']['lineno']=505;
						continue;
					}
					$pyjs['track']['lineno']=507;
					cat = 'default';
					$pyjs['track']['lineno']=509;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and36=(function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_319_err){if (!$p['isinstance']($pyjs_dbg_319_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_319_err);}throw $pyjs_dbg_319_err;
}})()['__contains__']('params'))?($p['bool']($and37=(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](bone['__getitem__']('params'), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_320_err){if (!$p['isinstance']($pyjs_dbg_320_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_320_err);}throw $pyjs_dbg_320_err;
}})())?(function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['__getitem__']('params')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_321_err){if (!$p['isinstance']($pyjs_dbg_321_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_321_err);}throw $pyjs_dbg_321_err;
}})()['__contains__']('category'):$and37):$and36));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_322_err){if (!$p['isinstance']($pyjs_dbg_322_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_322_err);}throw $pyjs_dbg_322_err;
}})()) {
						$pyjs['track']['lineno']=512;
						cat = bone['__getitem__']('params')['__getitem__']('category');
					}
					$pyjs['track']['lineno']=514;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return fieldSets['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_323_err){if (!$p['isinstance']($pyjs_dbg_323_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_323_err);}throw $pyjs_dbg_323_err;
}})()['__contains__'](cat)));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_324_err){if (!$p['isinstance']($pyjs_dbg_324_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_324_err);}throw $pyjs_dbg_324_err;
}})()) {
						$pyjs['track']['lineno']=515;
						fs = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Fieldset']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_325_err){if (!$p['isinstance']($pyjs_dbg_325_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_325_err);}throw $pyjs_dbg_325_err;
}})();
						$pyjs['track']['lineno']=516;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return fs['__setitem__']('class', cat);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_326_err){if (!$p['isinstance']($pyjs_dbg_326_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_326_err);}throw $pyjs_dbg_326_err;
}})();
						$pyjs['track']['lineno']=518;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']($p['op_eq'](cat, 'default'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_327_err){if (!$p['isinstance']($pyjs_dbg_327_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_327_err);}throw $pyjs_dbg_327_err;
}})()) {
							$pyjs['track']['lineno']=519;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return fs['__getitem__']('class')['append']('active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_328_err){if (!$p['isinstance']($pyjs_dbg_328_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_328_err);}throw $pyjs_dbg_328_err;
}})();
						}
						$pyjs['track']['lineno']=522;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return fs['__setitem__']('name', cat);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_329_err){if (!$p['isinstance']($pyjs_dbg_329_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_329_err);}throw $pyjs_dbg_329_err;
}})();
						$pyjs['track']['lineno']=523;
						legend = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Legend']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_330_err){if (!$p['isinstance']($pyjs_dbg_330_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_330_err);}throw $pyjs_dbg_330_err;
}})();
						$pyjs['track']['lineno']=525;
						fshref = (function(){try{try{$pyjs['in_try_except'] += 1;
						return (typeof fieldset_A == "undefined"?$m['fieldset_A']:fieldset_A)();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_331_err){if (!$p['isinstance']($pyjs_dbg_331_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_331_err);}throw $pyjs_dbg_331_err;
}})();
						$pyjs['track']['lineno']=527;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return fshref['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['TextNode'](cat);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_332_err){if (!$p['isinstance']($pyjs_dbg_332_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_332_err);}throw $pyjs_dbg_332_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_333_err){if (!$p['isinstance']($pyjs_dbg_333_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_333_err);}throw $pyjs_dbg_333_err;
}})();
						$pyjs['track']['lineno']=528;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return legend['appendChild'](fshref);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_334_err){if (!$p['isinstance']($pyjs_dbg_334_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_334_err);}throw $pyjs_dbg_334_err;
}})();
						$pyjs['track']['lineno']=529;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return fs['appendChild'](legend);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_335_err){if (!$p['isinstance']($pyjs_dbg_335_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_335_err);}throw $pyjs_dbg_335_err;
}})();
						$pyjs['track']['lineno']=530;
						section = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Section']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_336_err){if (!$p['isinstance']($pyjs_dbg_336_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_336_err);}throw $pyjs_dbg_336_err;
}})();
						$pyjs['track']['lineno']=531;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return fs['appendChild'](section);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_337_err){if (!$p['isinstance']($pyjs_dbg_337_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_337_err);}throw $pyjs_dbg_337_err;
}})();
						$pyjs['track']['lineno']=532;
						fs['__is_instance__'] && typeof fs['__setattr__'] == 'function' ? fs['__setattr__']('_section', section) : $p['setattr'](fs, '_section', section); 
						$pyjs['track']['lineno']=533;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return fieldSets['__setitem__'](cat, fs);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_338_err){if (!$p['isinstance']($pyjs_dbg_338_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_338_err);}throw $pyjs_dbg_338_err;
}})();
					}
					$pyjs['track']['lineno']=535;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and39=(function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_339_err){if (!$p['isinstance']($pyjs_dbg_339_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_339_err);}throw $pyjs_dbg_339_err;
}})()['__contains__']('params'))?($p['bool']($and40=bone['__getitem__']('params'))?(function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['__getitem__']('params')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_340_err){if (!$p['isinstance']($pyjs_dbg_340_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_340_err);}throw $pyjs_dbg_340_err;
}})()['__contains__']('category'):$and40):$and39));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_341_err){if (!$p['isinstance']($pyjs_dbg_341_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_341_err);}throw $pyjs_dbg_341_err;
}})()) {
						$pyjs['track']['lineno']=536;
						tabName = bone['__getitem__']('params')['__getitem__']('category');
					}
					else {
						$pyjs['track']['lineno']=538;
						tabName = 'Test';
					}
					$pyjs['track']['lineno']=540;
					wdgGen = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['editBoneSelector']['select']($p['getattr'](self, 'modul'), key, tmpDict);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_342_err){if (!$p['isinstance']($pyjs_dbg_342_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_342_err);}throw $pyjs_dbg_342_err;
}})();
					$pyjs['track']['lineno']=541;
					widget = (function(){try{try{$pyjs['in_try_except'] += 1;
					return wdgGen['fromSkelStructure']($p['getattr'](self, 'modul'), key, tmpDict);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_343_err){if (!$p['isinstance']($pyjs_dbg_343_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_343_err);}throw $pyjs_dbg_343_err;
}})();
					$pyjs['track']['lineno']=542;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return widget['__setitem__']('id', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('vi_%s_%s_%s_%s_bn_%s', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['tuple']([$p['getattr'](self, 'editIdx'), $p['getattr'](self, 'modul'), ($p['bool']($p['getattr'](self, 'key'))? ('edit') : ('add')), cat, key]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_344_err){if (!$p['isinstance']($pyjs_dbg_344_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_344_err);}throw $pyjs_dbg_344_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_345_err){if (!$p['isinstance']($pyjs_dbg_345_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_345_err);}throw $pyjs_dbg_345_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_346_err){if (!$p['isinstance']($pyjs_dbg_346_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_346_err);}throw $pyjs_dbg_346_err;
}})();
					$pyjs['track']['lineno']=549;
					descrLbl = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Label'](bone['__getitem__']('descr'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_347_err){if (!$p['isinstance']($pyjs_dbg_347_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_347_err);}throw $pyjs_dbg_347_err;
}})();
					$pyjs['track']['lineno']=550;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return descrLbl['__getitem__']('class')['append'](key);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_348_err){if (!$p['isinstance']($pyjs_dbg_348_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_348_err);}throw $pyjs_dbg_348_err;
}})();
					$pyjs['track']['lineno']=551;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return descrLbl['__getitem__']('class')['append']((function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['__getitem__']('type')['$$replace']('.', '_');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_349_err){if (!$p['isinstance']($pyjs_dbg_349_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_349_err);}throw $pyjs_dbg_349_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_350_err){if (!$p['isinstance']($pyjs_dbg_350_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_350_err);}throw $pyjs_dbg_350_err;
}})();
					$pyjs['track']['lineno']=552;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return descrLbl['__setitem__']('for', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('vi_%s_%s_%s_%s_bn_%s', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['tuple']([$p['getattr'](self, 'editIdx'), $p['getattr'](self, 'modul'), ($p['bool']($p['getattr'](self, 'key'))? ('edit') : ('add')), cat, key]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_351_err){if (!$p['isinstance']($pyjs_dbg_351_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_351_err);}throw $pyjs_dbg_351_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_352_err){if (!$p['isinstance']($pyjs_dbg_352_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_352_err);}throw $pyjs_dbg_352_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_353_err){if (!$p['isinstance']($pyjs_dbg_353_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_353_err);}throw $pyjs_dbg_353_err;
}})();
					$pyjs['track']['lineno']=554;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['printFunc']([(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['tuple']([key, bone['__getitem__']('required'), bone['__getitem__']('error')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_354_err){if (!$p['isinstance']($pyjs_dbg_354_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_354_err);}throw $pyjs_dbg_354_err;
}})()], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_355_err){if (!$p['isinstance']($pyjs_dbg_355_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_355_err);}throw $pyjs_dbg_355_err;
}})();
					$pyjs['track']['lineno']=555;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](bone['__getitem__']('required'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_356_err){if (!$p['isinstance']($pyjs_dbg_356_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_356_err);}throw $pyjs_dbg_356_err;
}})()) {
						$pyjs['track']['lineno']=556;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return descrLbl['__getitem__']('class')['append']('is_required');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_357_err){if (!$p['isinstance']($pyjs_dbg_357_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_357_err);}throw $pyjs_dbg_357_err;
}})();
						$pyjs['track']['lineno']=558;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['op_is'](bone['__getitem__']('error'), null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_358_err){if (!$p['isinstance']($pyjs_dbg_358_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_358_err);}throw $pyjs_dbg_358_err;
}})()) {
							$pyjs['track']['lineno']=559;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return descrLbl['__getitem__']('class')['append']('is_invalid');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_359_err){if (!$p['isinstance']($pyjs_dbg_359_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_359_err);}throw $pyjs_dbg_359_err;
}})();
							$pyjs['track']['lineno']=560;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return descrLbl['__setitem__']('title', bone['__getitem__']('error'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_360_err){if (!$p['isinstance']($pyjs_dbg_360_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_360_err);}throw $pyjs_dbg_360_err;
}})();
							$pyjs['track']['lineno']=561;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return fieldSets['__getitem__'](cat)['__getitem__']('class')['append']('is_incomplete');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_361_err){if (!$p['isinstance']($pyjs_dbg_361_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_361_err);}throw $pyjs_dbg_361_err;
}})();
							$pyjs['track']['lineno']=562;
							hasMissing = true;
						}
						else if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](($p['bool']($and42=$p['op_is'](bone['__getitem__']('error'), null))?!$p['bool']($p['getattr'](self, 'wasInitialRequest')):$and42));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_362_err){if (!$p['isinstance']($pyjs_dbg_362_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_362_err);}throw $pyjs_dbg_362_err;
}})()) {
							$pyjs['track']['lineno']=564;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return descrLbl['__getitem__']('class')['append']('is_valid');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_363_err){if (!$p['isinstance']($pyjs_dbg_363_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_363_err);}throw $pyjs_dbg_363_err;
}})();
						}
					}
					$pyjs['track']['lineno']=566;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](bone['__getitem__']('error'), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_364_err){if (!$p['isinstance']($pyjs_dbg_364_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_364_err);}throw $pyjs_dbg_364_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_365_err){if (!$p['isinstance']($pyjs_dbg_365_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_365_err);}throw $pyjs_dbg_365_err;
}})()) {
						$pyjs['track']['lineno']=567;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return widget['setExtendedErrorInformation'](bone['__getitem__']('error'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_366_err){if (!$p['isinstance']($pyjs_dbg_366_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_366_err);}throw $pyjs_dbg_366_err;
}})();
					}
					$pyjs['track']['lineno']=569;
					containerDiv = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_367_err){if (!$p['isinstance']($pyjs_dbg_367_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_367_err);}throw $pyjs_dbg_367_err;
}})();
					$pyjs['track']['lineno']=570;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return containerDiv['appendChild'](descrLbl);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_368_err){if (!$p['isinstance']($pyjs_dbg_368_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_368_err);}throw $pyjs_dbg_368_err;
}})();
					$pyjs['track']['lineno']=571;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return containerDiv['appendChild'](widget);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_369_err){if (!$p['isinstance']($pyjs_dbg_369_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_369_err);}throw $pyjs_dbg_369_err;
}})();
					$pyjs['track']['lineno']=573;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and44=(function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_370_err){if (!$p['isinstance']($pyjs_dbg_370_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_370_err);}throw $pyjs_dbg_370_err;
}})()['__contains__']('params'))?($p['bool']($and45=(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](bone['__getitem__']('params'), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_371_err){if (!$p['isinstance']($pyjs_dbg_371_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_371_err);}throw $pyjs_dbg_371_err;
}})())?(function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['__getitem__']('params')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_372_err){if (!$p['isinstance']($pyjs_dbg_372_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_372_err);}throw $pyjs_dbg_372_err;
}})()['__contains__']('tooltip'):$and45):$and44));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_373_err){if (!$p['isinstance']($pyjs_dbg_373_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_373_err);}throw $pyjs_dbg_373_err;
}})()) {
						$pyjs['track']['lineno']=576;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return containerDiv['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call(null, $m['ToolTip'], null, null, [{'longText':bone['__getitem__']('params')['__getitem__']('tooltip')}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_374_err){if (!$p['isinstance']($pyjs_dbg_374_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_374_err);}throw $pyjs_dbg_374_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_375_err){if (!$p['isinstance']($pyjs_dbg_375_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_375_err);}throw $pyjs_dbg_375_err;
}})();
					}
					$pyjs['track']['lineno']=578;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return fieldSets['__getitem__'](cat)['_section']['appendChild'](containerDiv);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_376_err){if (!$p['isinstance']($pyjs_dbg_376_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_376_err);}throw $pyjs_dbg_376_err;
}})();
					$pyjs['track']['lineno']=579;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return containerDiv['__getitem__']('class')['append']('bone');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_377_err){if (!$p['isinstance']($pyjs_dbg_377_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_377_err);}throw $pyjs_dbg_377_err;
}})();
					$pyjs['track']['lineno']=580;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return containerDiv['__getitem__']('class')['append']($p['__op_add']($add9='bone_',$add10=key));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_378_err){if (!$p['isinstance']($pyjs_dbg_378_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_378_err);}throw $pyjs_dbg_378_err;
}})();
					$pyjs['track']['lineno']=581;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return containerDiv['__getitem__']('class')['append']((function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['__getitem__']('type')['$$replace']('.', '_');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_379_err){if (!$p['isinstance']($pyjs_dbg_379_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_379_err);}throw $pyjs_dbg_379_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_380_err){if (!$p['isinstance']($pyjs_dbg_380_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_380_err);}throw $pyjs_dbg_380_err;
}})();
					$pyjs['track']['lineno']=583;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](bone['__getitem__']('type')['__contains__']('.'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_381_err){if (!$p['isinstance']($pyjs_dbg_381_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_381_err);}throw $pyjs_dbg_381_err;
}})()) {
						$pyjs['track']['lineno']=584;
						$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
						$iter14_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return bone['__getitem__']('type')['$$split']('.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_382_err){if (!$p['isinstance']($pyjs_dbg_382_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_382_err);}throw $pyjs_dbg_382_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_383_err){if (!$p['isinstance']($pyjs_dbg_383_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_383_err);}throw $pyjs_dbg_383_err;
}})();
						$iter14_nextval=$p['__iter_prepare']($iter14_iter,false);
						while (typeof($p['__wrapped_next']($iter14_nextval)['$nextval']) != 'undefined') {
							t = $iter14_nextval['$nextval'];
							$pyjs['track']['lineno']=585;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return containerDiv['__getitem__']('class')['append'](t);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_384_err){if (!$p['isinstance']($pyjs_dbg_384_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_384_err);}throw $pyjs_dbg_384_err;
}})();
						}
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='widgets.edit';
					}
					$pyjs['track']['lineno']=590;
					currRow = $p['__op_add']($add11=currRow,$add12=$constant_int_1);
					$pyjs['track']['lineno']=591;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'bones')['__setitem__'](key, widget);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_385_err){if (!$p['isinstance']($pyjs_dbg_385_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_385_err);}throw $pyjs_dbg_385_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=593;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](fieldSets);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_386_err){if (!$p['isinstance']($pyjs_dbg_386_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_386_err);}throw $pyjs_dbg_386_err;
}})(), $constant_int_1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_387_err){if (!$p['isinstance']($pyjs_dbg_387_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_387_err);}throw $pyjs_dbg_387_err;
}})()) {
					$pyjs['track']['lineno']=594;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter15_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return fieldSets['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_389_err){if (!$p['isinstance']($pyjs_dbg_389_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_389_err);}throw $pyjs_dbg_389_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_390_err){if (!$p['isinstance']($pyjs_dbg_390_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_390_err);}throw $pyjs_dbg_390_err;
}})();
					$iter15_nextval=$p['__iter_prepare']($iter15_iter,false);
					while (typeof($p['__wrapped_next']($iter15_nextval)['$nextval']) != 'undefined') {
						var $tupleassign8 = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['__ass_unpack']($iter15_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_388_err){if (!$p['isinstance']($pyjs_dbg_388_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_388_err);}throw $pyjs_dbg_388_err;
}})();
						k = $tupleassign8[0];
						v = $tupleassign8[1];
						$pyjs['track']['lineno']=595;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['bool'](v['__getitem__']('class')['__contains__']('active')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_391_err){if (!$p['isinstance']($pyjs_dbg_391_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_391_err);}throw $pyjs_dbg_391_err;
}})()) {
							$pyjs['track']['lineno']=596;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return v['__getitem__']('class')['append']('active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_392_err){if (!$p['isinstance']($pyjs_dbg_392_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_392_err);}throw $pyjs_dbg_392_err;
}})();
						}
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='widgets.edit';
				}
				$pyjs['track']['lineno']=598;
				tmpList = function(){
					var $iter16_array,$iter16_type,k,$collcomp4,$iter16_idx,$pyjs__trackstack_size_1,v,$iter16_nextval,$iter16_iter;
	$collcomp4 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter16_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return fieldSets['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_394_err){if (!$p['isinstance']($pyjs_dbg_394_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_394_err);}throw $pyjs_dbg_394_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_395_err){if (!$p['isinstance']($pyjs_dbg_395_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_395_err);}throw $pyjs_dbg_395_err;
}})();
				$iter16_nextval=$p['__iter_prepare']($iter16_iter,false);
				while (typeof($p['__wrapped_next']($iter16_nextval)['$nextval']) != 'undefined') {
					var $tupleassign9 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter16_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_393_err){if (!$p['isinstance']($pyjs_dbg_393_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_393_err);}throw $pyjs_dbg_393_err;
}})();
					k = $tupleassign9[0];
					v = $tupleassign9[1];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp4['append']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['tuple']([k, v]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_396_err){if (!$p['isinstance']($pyjs_dbg_396_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_396_err);}throw $pyjs_dbg_396_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_397_err){if (!$p['isinstance']($pyjs_dbg_397_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_397_err);}throw $pyjs_dbg_397_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.edit';

	return $collcomp4;}();
				$pyjs['track']['lineno']=599;
				var 				$lambda2 = function(x) {
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);

					$pyjs['track']={'module':'widgets.edit','lineno':599};$pyjs['trackstack']['push']($pyjs['track']);
					$pyjs['track']['module']='widgets.edit';
					$pyjs['track']['lineno']=599;
					$pyjs['track']['lineno']=599;
					$pyjs['track']['lineno']=599;
					var $pyjs__ret = x['__getitem__']($constant_int_0);
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				};
				$lambda2['__name__'] = '$lambda2';

				$lambda2['__bind_type__'] = 0;
				$lambda2['__args__'] = [null,null,['x']];
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(tmpList, 'sort', null, null, [{'key':$lambda2}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_398_err){if (!$p['isinstance']($pyjs_dbg_398_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_398_err);}throw $pyjs_dbg_398_err;
}})();
				$pyjs['track']['lineno']=601;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter17_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return tmpList;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_400_err){if (!$p['isinstance']($pyjs_dbg_400_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_400_err);}throw $pyjs_dbg_400_err;
}})();
				$iter17_nextval=$p['__iter_prepare']($iter17_iter,false);
				while (typeof($p['__wrapped_next']($iter17_nextval)['$nextval']) != 'undefined') {
					var $tupleassign10 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter17_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_399_err){if (!$p['isinstance']($pyjs_dbg_399_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_399_err);}throw $pyjs_dbg_399_err;
}})();
					k = $tupleassign10[0];
					v = $tupleassign10[1];
					$pyjs['track']['lineno']=602;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['form']['appendChild'](v);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_401_err){if (!$p['isinstance']($pyjs_dbg_401_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_401_err);}throw $pyjs_dbg_401_err;
}})();
					$pyjs['track']['lineno']=603;
					v['__is_instance__'] && typeof v['__setattr__'] == 'function' ? v['__setattr__']('_section', null) : $p['setattr'](v, '_section', null); 
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=605;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['unserialize'](data['__getitem__']('values'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_402_err){if (!$p['isinstance']($pyjs_dbg_402_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_402_err);}throw $pyjs_dbg_402_err;
}})();
				$pyjs['track']['lineno']=607;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, '_hashArgs'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_403_err){if (!$p['isinstance']($pyjs_dbg_403_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_403_err);}throw $pyjs_dbg_403_err;
}})()) {
					$pyjs['track']['lineno']=608;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['unserialize']($p['getattr'](self, '_hashArgs'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_404_err){if (!$p['isinstance']($pyjs_dbg_404_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_404_err);}throw $pyjs_dbg_404_err;
}})();
					$pyjs['track']['lineno']=609;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_hashArgs', null) : $p['setattr'](self, '_hashArgs', null); 
				}
				$pyjs['track']['lineno']=611;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_lastData', data) : $p['setattr'](self, '_lastData', data); 
				$pyjs['track']['lineno']=613;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and47=hasMissing)?!$p['bool']($p['getattr'](self, 'wasInitialRequest')):$and47));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_405_err){if (!$p['isinstance']($pyjs_dbg_405_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_405_err);}throw $pyjs_dbg_405_err;
}})()) {
					$pyjs['track']['lineno']=614;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['conf']['__getitem__']('mainWindow')['log']('warning', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Could not save entry!');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_406_err){if (!$p['isinstance']($pyjs_dbg_406_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_406_err);}throw $pyjs_dbg_406_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_407_err){if (!$p['isinstance']($pyjs_dbg_407_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_407_err);}throw $pyjs_dbg_407_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['request', null],['data', null],['ignoreMissing', false],['askHierarchyCloning', true]]);
			$cls_definition['setData'] = $method;
			$pyjs['track']['lineno']=616;
			$method = $pyjs__bind_method2('unserialize', function(data) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '72afed7bc698473860a949b394dea93e') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter18_type,$iter18_iter,$iter18_array,$iter18_idx,$pyjs__trackstack_size_1,$iter18_nextval,bone;
				$pyjs['track']={'module':'widgets.edit', 'lineno':616};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=616;
				$pyjs['track']['lineno']=620;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter18_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['bones']['values']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_408_err){if (!$p['isinstance']($pyjs_dbg_408_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_408_err);}throw $pyjs_dbg_408_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_409_err){if (!$p['isinstance']($pyjs_dbg_409_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_409_err);}throw $pyjs_dbg_409_err;
}})();
				$iter18_nextval=$p['__iter_prepare']($iter18_iter,false);
				while (typeof($p['__wrapped_next']($iter18_nextval)['$nextval']) != 'undefined') {
					bone = $iter18_nextval['$nextval'];
					$pyjs['track']['lineno']=621;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['unserialize'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_410_err){if (!$p['isinstance']($pyjs_dbg_410_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_410_err);}throw $pyjs_dbg_410_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.edit';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['data']]);
			$cls_definition['unserialize'] = $method;
			$pyjs['track']['lineno']=623;
			$method = $pyjs__bind_method2('doSave', function(closeOnSuccess) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					closeOnSuccess = arguments[1];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,2,arguments['length']-1));

					var kwargs = arguments['length'] >= 3 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '72afed7bc698473860a949b394dea93e') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof closeOnSuccess != 'undefined') {
						if (closeOnSuccess !== null && typeof closeOnSuccess['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = closeOnSuccess;
							closeOnSuccess = arguments[2];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[2];
						}
					} else {
					}
				}
				if (typeof closeOnSuccess == 'undefined') closeOnSuccess=arguments['callee']['__args__'][3][1];
				var $iter19_idx,key,res,$pyjs_try_err,$iter19_array,$iter19_iter,$iter19_nextval,$iter19_type,$pyjs__trackstack_size_1,lbl,bone;
				$pyjs['track']={'module':'widgets.edit', 'lineno':623};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=623;
				$pyjs['track']['lineno']=627;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('closeOnSuccess', closeOnSuccess) : $p['setattr'](self, 'closeOnSuccess', closeOnSuccess); 
				$pyjs['track']['lineno']=628;
				res = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_411_err){if (!$p['isinstance']($pyjs_dbg_411_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_411_err);}throw $pyjs_dbg_411_err;
}})();
				$pyjs['track']['lineno']=629;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter19_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['bones']['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_413_err){if (!$p['isinstance']($pyjs_dbg_413_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_413_err);}throw $pyjs_dbg_413_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_414_err){if (!$p['isinstance']($pyjs_dbg_414_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_414_err);}throw $pyjs_dbg_414_err;
}})();
				$iter19_nextval=$p['__iter_prepare']($iter19_iter,false);
				while (typeof($p['__wrapped_next']($iter19_nextval)['$nextval']) != 'undefined') {
					var $tupleassign11 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter19_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_412_err){if (!$p['isinstance']($pyjs_dbg_412_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_412_err);}throw $pyjs_dbg_412_err;
}})();
					key = $tupleassign11[0];
					bone = $tupleassign11[1];
					$pyjs['track']['lineno']=630;
					var $pyjs__trackstack_size_2 = $pyjs['trackstack']['length'];
					try {
						try {
							$pyjs['in_try_except'] += 1;
							$pyjs['track']['lineno']=631;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return res['update']((function(){try{try{$pyjs['in_try_except'] += 1;
							return bone['serializeForPost']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_415_err){if (!$p['isinstance']($pyjs_dbg_415_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_415_err);}throw $pyjs_dbg_415_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_416_err){if (!$p['isinstance']($pyjs_dbg_416_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_416_err);}throw $pyjs_dbg_416_err;
}})();
						} finally { $pyjs['in_try_except'] -= 1; }
					} catch($pyjs_try_err) {
						$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_2 - 1);
						$pyjs['__active_exception_stack__'] = null;
						$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
						var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
						$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='widgets.edit';
						if (($pyjs_try_err_name == $m['InvalidBoneValueException']['__name__'])||$p['_isinstance']($pyjs_try_err,$m['InvalidBoneValueException'])) {
							$pyjs['track']['lineno']=634;
							lbl = $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
							return bone['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_417_err){if (!$p['isinstance']($pyjs_dbg_417_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_417_err);}throw $pyjs_dbg_417_err;
}})(), '_children')['__getitem__']($constant_int_0);
							$pyjs['track']['lineno']=635;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool'](lbl['__getitem__']('class')['__contains__']('is_valid'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_418_err){if (!$p['isinstance']($pyjs_dbg_418_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_418_err);}throw $pyjs_dbg_418_err;
}})()) {
								$pyjs['track']['lineno']=636;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return lbl['__getitem__']('class')['remove']('is_valid');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_419_err){if (!$p['isinstance']($pyjs_dbg_419_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_419_err);}throw $pyjs_dbg_419_err;
}})();
							}
							$pyjs['track']['lineno']=637;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return lbl['__getitem__']('class')['append']('is_invalid');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_420_err){if (!$p['isinstance']($pyjs_dbg_420_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_420_err);}throw $pyjs_dbg_420_err;
}})();
							$pyjs['track']['lineno']=638;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return self['actionbar']['resetLoadingState']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_421_err){if (!$p['isinstance']($pyjs_dbg_421_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_421_err);}throw $pyjs_dbg_421_err;
}})();
							$pyjs['track']['lineno']=639;
							$pyjs['track']['lineno']=639;
							var $pyjs__ret = null;
							$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
							return $pyjs__ret;
						} else { $pyjs['__active_exception_stack__'] = $pyjs['__last_exception_stack__']; $pyjs['__last_exception_stack__'] = null; throw $pyjs_try_err; }
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=640;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['save'](res);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_422_err){if (!$p['isinstance']($pyjs_dbg_422_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_422_err);}throw $pyjs_dbg_422_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['closeOnSuccess', false]]);
			$cls_definition['doSave'] = $method;
			$pyjs['track']['lineno']=225;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('EditWidget', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=642;
		$m['fieldset_A'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'widgets.edit';
			$cls_definition['__md5__'] = 'd8c1bc60cef4cbb79fcf0eb682268bc4';
			$pyjs['track']['lineno']=643;
			$cls_definition['_baseClass'] = 'a';
			$pyjs['track']['lineno']=645;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'd8c1bc60cef4cbb79fcf0eb682268bc4') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'widgets.edit', 'lineno':645};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=645;
				$pyjs['track']['lineno']=646;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['fieldset_A'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_423_err){if (!$p['isinstance']($pyjs_dbg_423_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_423_err);}throw $pyjs_dbg_423_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_424_err){if (!$p['isinstance']($pyjs_dbg_424_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_424_err);}throw $pyjs_dbg_424_err;
}})();
				$pyjs['track']['lineno']=647;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['sinkEvent']('onClick');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_425_err){if (!$p['isinstance']($pyjs_dbg_425_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_425_err);}throw $pyjs_dbg_425_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=649;
			$method = $pyjs__bind_method2('onClick', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'd8c1bc60cef4cbb79fcf0eb682268bc4') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter20_iter,$iter20_nextval,$iter20_type,$and49,$iter21_nextval,$and52,$and53,$and50,element,$iter21_type,$iter21_iter,$iter20_idx,$pyjs__trackstack_size_2,$iter21_array,$pyjs__trackstack_size_1,sube,$iter21_idx,$and51,$iter20_array;
				$pyjs['track']={'module':'widgets.edit', 'lineno':649};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.edit';
				$pyjs['track']['lineno']=649;
				$pyjs['track']['lineno']=650;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter20_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_426_err){if (!$p['isinstance']($pyjs_dbg_426_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_426_err);}throw $pyjs_dbg_426_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_427_err){if (!$p['isinstance']($pyjs_dbg_427_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_427_err);}throw $pyjs_dbg_427_err;
}})()['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_428_err){if (!$p['isinstance']($pyjs_dbg_428_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_428_err);}throw $pyjs_dbg_428_err;
}})(), '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_429_err){if (!$p['isinstance']($pyjs_dbg_429_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_429_err);}throw $pyjs_dbg_429_err;
}})();
				$iter20_nextval=$p['__iter_prepare']($iter20_iter,false);
				while (typeof($p['__wrapped_next']($iter20_nextval)['$nextval']) != 'undefined') {
					element = $iter20_nextval['$nextval'];
					$pyjs['track']['lineno']=651;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](element, $m['Fieldset']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_430_err){if (!$p['isinstance']($pyjs_dbg_430_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_430_err);}throw $pyjs_dbg_430_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_431_err){if (!$p['isinstance']($pyjs_dbg_431_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_431_err);}throw $pyjs_dbg_431_err;
}})()) {
						$pyjs['track']['lineno']=652;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['utils']['doesEventHitWidgetOrChildren'](event, element);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_432_err){if (!$p['isinstance']($pyjs_dbg_432_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_432_err);}throw $pyjs_dbg_432_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_433_err){if (!$p['isinstance']($pyjs_dbg_433_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_433_err);}throw $pyjs_dbg_433_err;
}})()) {
							$pyjs['track']['lineno']=653;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool'](!$p['bool'](element['__getitem__']('class')['__contains__']('active')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_434_err){if (!$p['isinstance']($pyjs_dbg_434_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_434_err);}throw $pyjs_dbg_434_err;
}})()) {
								$pyjs['track']['lineno']=654;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return element['__getitem__']('class')['append']('active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_435_err){if (!$p['isinstance']($pyjs_dbg_435_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_435_err);}throw $pyjs_dbg_435_err;
}})();
								$pyjs['track']['lineno']=655;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return element['__getitem__']('class')['remove']('inactive');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_436_err){if (!$p['isinstance']($pyjs_dbg_436_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_436_err);}throw $pyjs_dbg_436_err;
}})();
							}
							else {
								$pyjs['track']['lineno']=657;
								if ((function(){try{try{$pyjs['in_try_except'] += 1;
									return $p['bool'](!$p['bool'](element['__getitem__']('class')['__contains__']('inactive')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_437_err){if (!$p['isinstance']($pyjs_dbg_437_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_437_err);}throw $pyjs_dbg_437_err;
}})()) {
									$pyjs['track']['lineno']=658;
									(function(){try{try{$pyjs['in_try_except'] += 1;
									return element['__getitem__']('class')['append']('inactive');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_438_err){if (!$p['isinstance']($pyjs_dbg_438_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_438_err);}throw $pyjs_dbg_438_err;
}})();
								}
								$pyjs['track']['lineno']=659;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return element['__getitem__']('class')['remove']('active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_439_err){if (!$p['isinstance']($pyjs_dbg_439_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_439_err);}throw $pyjs_dbg_439_err;
}})();
							}
						}
						else {
							$pyjs['track']['lineno']=661;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool'](($p['bool']($and49=!$p['bool'](element['__getitem__']('class')['__contains__']('inactive')))?(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['isinstance'](element, $m['fieldset_A']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_440_err){if (!$p['isinstance']($pyjs_dbg_440_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_440_err);}throw $pyjs_dbg_440_err;
}})():$and49));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_441_err){if (!$p['isinstance']($pyjs_dbg_441_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_441_err);}throw $pyjs_dbg_441_err;
}})()) {
								$pyjs['track']['lineno']=662;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return element['__getitem__']('class')['append']('inactive');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_442_err){if (!$p['isinstance']($pyjs_dbg_442_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_442_err);}throw $pyjs_dbg_442_err;
}})();
							}
							$pyjs['track']['lineno']=663;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return element['__getitem__']('class')['remove']('active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_443_err){if (!$p['isinstance']($pyjs_dbg_443_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_443_err);}throw $pyjs_dbg_443_err;
}})();
							$pyjs['track']['lineno']=664;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool'](($p['bool']($and51=($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['len']($p['getattr'](element, '_children'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_444_err){if (!$p['isinstance']($pyjs_dbg_444_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_444_err);}throw $pyjs_dbg_444_err;
}})(), $constant_int_0) == 1))?($p['bool']($and52=(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['isinstance'](element, $m['fieldset_A']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_445_err){if (!$p['isinstance']($pyjs_dbg_445_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_445_err);}throw $pyjs_dbg_445_err;
}})())?(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['hasattr']($p['getattr'](element, '_children')['__getitem__']($constant_int_1), '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_446_err){if (!$p['isinstance']($pyjs_dbg_446_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_446_err);}throw $pyjs_dbg_446_err;
}})():$and52):$and51));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_447_err){if (!$p['isinstance']($pyjs_dbg_447_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_447_err);}throw $pyjs_dbg_447_err;
}})()) {
								$pyjs['track']['lineno']=665;
								$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
								$iter21_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['getattr']($p['getattr'](element, '_children')['__getitem__']($constant_int_1), '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_448_err){if (!$p['isinstance']($pyjs_dbg_448_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_448_err);}throw $pyjs_dbg_448_err;
}})();
								$iter21_nextval=$p['__iter_prepare']($iter21_iter,false);
								while (typeof($p['__wrapped_next']($iter21_nextval)['$nextval']) != 'undefined') {
									sube = $iter21_nextval['$nextval'];
									$pyjs['track']['lineno']=666;
									if ((function(){try{try{$pyjs['in_try_except'] += 1;
										return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
									return $p['isinstance'](sube, $m['fieldset_A']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_449_err){if (!$p['isinstance']($pyjs_dbg_449_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_449_err);}throw $pyjs_dbg_449_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_450_err){if (!$p['isinstance']($pyjs_dbg_450_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_450_err);}throw $pyjs_dbg_450_err;
}})()) {
										$pyjs['track']['lineno']=667;
										if ((function(){try{try{$pyjs['in_try_except'] += 1;
											return $p['bool'](!$p['bool'](sube['__getitem__']('class')['__contains__']('inactive')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_451_err){if (!$p['isinstance']($pyjs_dbg_451_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_451_err);}throw $pyjs_dbg_451_err;
}})()) {
											$pyjs['track']['lineno']=668;
											(function(){try{try{$pyjs['in_try_except'] += 1;
											return $p['getattr'](sube, 'parent')['__getitem__']('class')['append']('inactive');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_452_err){if (!$p['isinstance']($pyjs_dbg_452_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_452_err);}throw $pyjs_dbg_452_err;
}})();
										}
										$pyjs['track']['lineno']=669;
										(function(){try{try{$pyjs['in_try_except'] += 1;
										return sube['__getitem__']('class')['remove']('active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_453_err){if (!$p['isinstance']($pyjs_dbg_453_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_453_err);}throw $pyjs_dbg_453_err;
}})();
									}
								}
								if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
									$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
									$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
								}
								$pyjs['track']['module']='widgets.edit';
							}
						}
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.edit';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=642;
			var $bases = new Array($m['A']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('fieldset_A', $p['tuple']($bases), $data);
		})();
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end widgets.edit */


/* end module: widgets.edit */


/*
PYJS_DEPS: ['html5', 'html5.a.A', 'html5.a', 'html5.form.Fieldset', 'html5.form', 'html5.ext.YesNoDialog', 'html5.ext', 'network.NetworkService', 'network', 'config.conf', 'config', 'priorityqueue.editBoneSelector', 'priorityqueue', 'widgets.tooltip.ToolTip', 'widgets', 'widgets.tooltip', 'priorityqueue.protocolWrapperInstanceSelector', 'widgets.actionbar.ActionBar', 'widgets.actionbar', 'utils', 'i18n.translate', 'i18n']
*/
