/* start module: widgets */
$pyjs['loaded_modules']['widgets'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['widgets']['__was_initialized__']) return $pyjs['loaded_modules']['widgets'];
	var $m = $pyjs['loaded_modules']['widgets'];
	$m['__repr__'] = function() { return '<module: widgets>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'widgets';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	try {
		$m.__track_lines__[1] = 'widgets.py, line 1:\n    from topbar import TopBarWidget';
		$m.__track_lines__[2] = 'widgets.py, line 2:\n    from list import ListWidget';
		$m.__track_lines__[3] = 'widgets.py, line 3:\n    from edit import EditWidget';
		$m.__track_lines__[4] = 'widgets.py, line 4:\n    from table import DataTable';
		$m.__track_lines__[5] = 'widgets.py, line 5:\n    from actionbar import ActionBar';
		$m.__track_lines__[6] = 'widgets.py, line 6:\n    from tree import TreeWidget';
		$m.__track_lines__[7] = 'widgets.py, line 7:\n    from hierarchy import HierarchyWidget';
		$m.__track_lines__[8] = 'widgets.py, line 8:\n    from file import FileWidget';
		$m.__track_lines__[9] = 'widgets.py, line 9:\n    from preview import Preview';
		$m.__track_lines__[10] = 'widgets.py, line 10:\n    from search import Search';
		$m.__track_lines__[11] = 'widgets.py, line 11:\n    from sidebar import SideBar';
		$m.__track_lines__[12] = 'widgets.py, line 12:\n    from userlogoutmsg import UserLogoutMsg';
		$m.__track_lines__[13] = 'widgets.py, line 13:\n    from task import TaskWidget, TaskSelectWidget';


		$pyjs['track']['module']='widgets';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=1;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['TopBarWidget'] = $p['___import___']('topbar.TopBarWidget', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=2;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['ListWidget'] = $p['___import___']('list.ListWidget', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=3;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['EditWidget'] = $p['___import___']('edit.EditWidget', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=4;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['DataTable'] = $p['___import___']('table.DataTable', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=5;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['ActionBar'] = $p['___import___']('actionbar.ActionBar', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=6;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['TreeWidget'] = $p['___import___']('tree.TreeWidget', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=7;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['HierarchyWidget'] = $p['___import___']('hierarchy.HierarchyWidget', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=8;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['FileWidget'] = $p['___import___']('file.FileWidget', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=9;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['Preview'] = $p['___import___']('preview.Preview', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=10;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['Search'] = $p['___import___']('search.Search', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=11;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['SideBar'] = $p['___import___']('sidebar.SideBar', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=12;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['UserLogoutMsg'] = $p['___import___']('userlogoutmsg.UserLogoutMsg', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=13;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['TaskWidget'] = $p['___import___']('task.TaskWidget', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['TaskSelectWidget'] = $p['___import___']('task.TaskSelectWidget', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end widgets */


/* end module: widgets */


/*
PYJS_DEPS: ['topbar.TopBarWidget', 'topbar', 'list.ListWidget', 'list', 'edit.EditWidget', 'edit', 'table.DataTable', 'table', 'actionbar.ActionBar', 'actionbar', 'tree.TreeWidget', 'tree', 'hierarchy.HierarchyWidget', 'hierarchy', 'file.FileWidget', 'file', 'preview.Preview', 'preview', 'search.Search', 'search', 'sidebar.SideBar', 'sidebar', 'userlogoutmsg.UserLogoutMsg', 'userlogoutmsg', 'task.TaskWidget', 'task', 'task.TaskSelectWidget']
*/
