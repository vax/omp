/* start module: bones.text */
$pyjs['loaded_modules']['bones.text'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['bones.text']['__was_initialized__']) return $pyjs['loaded_modules']['bones.text'];
	if(typeof $pyjs['loaded_modules']['bones'] == 'undefined' || !$pyjs['loaded_modules']['bones']['__was_initialized__']) $p['___import___']('bones', null);
	var $m = $pyjs['loaded_modules']['bones.text'];
	$m['__repr__'] = function() { return '<module: bones.text>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'bones.text';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	$pyjs['loaded_modules']['bones']['text'] = $pyjs['loaded_modules']['bones.text'];
	try {
		$m.__track_lines__[1] = 'bones.text.py, line 1:\n    #!/usr/bin/env python2';
		$m.__track_lines__[3] = 'bones.text.py, line 3:\n    import html5';
		$m.__track_lines__[4] = 'bones.text.py, line 4:\n    from priorityqueue import editBoneSelector, viewDelegateSelector, extractorDelegateSelector';
		$m.__track_lines__[5] = 'bones.text.py, line 5:\n    from config import conf';
		$m.__track_lines__[6] = 'bones.text.py, line 6:\n    from widgets.wysiwyg import Wysiwyg';
		$m.__track_lines__[7] = 'bones.text.py, line 7:\n    import utils';
		$m.__track_lines__[8] = 'bones.text.py, line 8:\n    from i18n import translate';
		$m.__track_lines__[11] = 'bones.text.py, line 11:\n    class TextBoneExtractor(object):';
		$m.__track_lines__[12] = 'bones.text.py, line 12:\n    def __init__(self, modulName, boneName, skelStructure, *args, **kwargs):';
		$m.__track_lines__[13] = 'bones.text.py, line 13:\n    super(TextBoneExtractor, self).__init__()';
		$m.__track_lines__[14] = 'bones.text.py, line 14:\n    self.skelStructure = skelStructure';
		$m.__track_lines__[15] = 'bones.text.py, line 15:\n    self.boneName = boneName';
		$m.__track_lines__[16] = 'bones.text.py, line 16:\n    self.modulName = modulName';
		$m.__track_lines__[18] = 'bones.text.py, line 18:\n    def render(self, data, field):';
		$m.__track_lines__[19] = 'bones.text.py, line 19:\n    if field in data.keys():';
		$m.__track_lines__[21] = 'bones.text.py, line 21:\n    if isinstance(data[field], dict):';
		$m.__track_lines__[22] = 'bones.text.py, line 22:\n    resstr = ""';
		$m.__track_lines__[23] = 'bones.text.py, line 23:\n    if "currentlanguage" in conf.keys():';
		$m.__track_lines__[24] = 'bones.text.py, line 24:\n    if conf["currentlanguage"] in data[field].keys():';
		$m.__track_lines__[25] = 'bones.text.py, line 25:\n    resstr = data[field][conf["currentlanguage"]].replace("&quot;", "").replace(";", " ").replace(\'"\', "\'")';
		$m.__track_lines__[27] = 'bones.text.py, line 27:\n    if data[field].keys().length > 0:';
		$m.__track_lines__[28] = 'bones.text.py, line 28:\n    resstr = data[field][data[field].keys()[0]].replace("&quot;", "").replace(";", " ").replace(\'"\', "\'")';
		$m.__track_lines__[29] = 'bones.text.py, line 29:\n    return \'"%s"\' % resstr';
		$m.__track_lines__[32] = 'bones.text.py, line 32:\n    return str(\'"%s"\' % data[field].replace("&quot;", "").replace(";", " ").replace(\'"\', "\'"))';
		$m.__track_lines__[33] = 'bones.text.py, line 33:\n    return conf["empty_value"]';
		$m.__track_lines__[36] = 'bones.text.py, line 36:\n    class TextViewBoneDelegate( object ):';
		$m.__track_lines__[37] = 'bones.text.py, line 37:\n    def __init__(self, modulName, boneName, skelStructure, *args, **kwargs ):';
		$m.__track_lines__[38] = 'bones.text.py, line 38:\n    super( TextViewBoneDelegate, self ).__init__()';
		$m.__track_lines__[39] = 'bones.text.py, line 39:\n    self.skelStructure = skelStructure';
		$m.__track_lines__[40] = 'bones.text.py, line 40:\n    self.boneName = boneName';
		$m.__track_lines__[41] = 'bones.text.py, line 41:\n    self.modulName=modulName';
		$m.__track_lines__[43] = 'bones.text.py, line 43:\n    def render( self, data, field ):';
		$m.__track_lines__[44] = 'bones.text.py, line 44:\n    if field in data.keys():';
		$m.__track_lines__[46] = 'bones.text.py, line 46:\n    if isinstance(data[field],dict):';
		$m.__track_lines__[47] = 'bones.text.py, line 47:\n    resstr=""';
		$m.__track_lines__[48] = 'bones.text.py, line 48:\n    if "currentlanguage" in conf.keys():';
		$m.__track_lines__[49] = 'bones.text.py, line 49:\n    if conf["currentlanguage"] in data[field].keys():';
		$m.__track_lines__[50] = 'bones.text.py, line 50:\n    resstr=data[field][conf["currentlanguage"]]';
		$m.__track_lines__[52] = 'bones.text.py, line 52:\n    if data[field].keys().length>0:';
		$m.__track_lines__[53] = 'bones.text.py, line 53:\n    resstr=data[field][data[field].keys()[0]]';
		$m.__track_lines__[54] = 'bones.text.py, line 54:\n    aspan=html5.Span()';
		$m.__track_lines__[55] = 'bones.text.py, line 55:\n    aspan.appendChild(html5.TextNode(resstr))';
		$m.__track_lines__[56] = 'bones.text.py, line 56:\n    aspan["Title"]=str( data[field])';
		$m.__track_lines__[57] = 'bones.text.py, line 57:\n    return (aspan)';
		$m.__track_lines__[60] = 'bones.text.py, line 60:\n    return( html5.Label(str( data[field])))';
		$m.__track_lines__[61] = 'bones.text.py, line 61:\n    return( html5.Label( conf[ "empty_value" ] ) )';
		$m.__track_lines__[63] = 'bones.text.py, line 63:\n    class TextEditBone( html5.Div ):';
		$m.__track_lines__[64] = 'bones.text.py, line 64:\n    def __init__(self, modulName, boneName,readOnly, isPlainText, languages=None, descrHint=None, *args, **kwargs ):';
		$m.__track_lines__[65] = 'bones.text.py, line 65:\n    super( TextEditBone,  self ).__init__( *args, **kwargs )';
		$m.__track_lines__[66] = 'bones.text.py, line 66:\n    self.boneName = boneName';
		$m.__track_lines__[67] = 'bones.text.py, line 67:\n    self.readOnly = readOnly';
		$m.__track_lines__[68] = 'bones.text.py, line 68:\n    self.selectedLang=False';
		$m.__track_lines__[69] = 'bones.text.py, line 69:\n    self.isPlainText = isPlainText';
		$m.__track_lines__[70] = 'bones.text.py, line 70:\n    self.languages = languages';
		$m.__track_lines__[71] = 'bones.text.py, line 71:\n    self.descrHint = descrHint';
		$m.__track_lines__[72] = 'bones.text.py, line 72:\n    self.currentEditor = None';
		$m.__track_lines__[73] = 'bones.text.py, line 73:\n    self.valuesdict = dict()';
		$m.__track_lines__[76] = 'bones.text.py, line 76:\n    if self.languages:';
		$m.__track_lines__[77] = 'bones.text.py, line 77:\n    if "currentlanguage" in conf and conf["currentlanguage"] in self.languages:';
		$m.__track_lines__[78] = 'bones.text.py, line 78:\n    self.selectedLang=conf["currentlanguage"]';
		$m.__track_lines__[80] = 'bones.text.py, line 80:\n    self.selectedLang=self.languages[0]';
		$m.__track_lines__[82] = 'bones.text.py, line 82:\n    self.langButContainer=html5.Div()';
		$m.__track_lines__[83] = 'bones.text.py, line 83:\n    self.langButContainer["class"].append("languagebuttons")';
		$m.__track_lines__[85] = 'bones.text.py, line 85:\n    for lang in self.languages:';
		$m.__track_lines__[86] = 'bones.text.py, line 86:\n    abut=html5.ext.Button(lang,self.changeLang)';
		$m.__track_lines__[87] = 'bones.text.py, line 87:\n    abut["value"]=lang';
		$m.__track_lines__[88] = 'bones.text.py, line 88:\n    self.langButContainer.appendChild(abut)';
		$m.__track_lines__[90] = 'bones.text.py, line 90:\n    self.appendChild(self.langButContainer)';
		$m.__track_lines__[91] = 'bones.text.py, line 91:\n    self.refreshLangButContainer()';
		$m.__track_lines__[93] = 'bones.text.py, line 93:\n    self.input=html5.Textarea()';
		$m.__track_lines__[94] = 'bones.text.py, line 94:\n    self.appendChild(self.input)';
		$m.__track_lines__[95] = 'bones.text.py, line 95:\n    self.previewDiv = html5.Div()';
		$m.__track_lines__[96] = 'bones.text.py, line 96:\n    self.previewDiv["class"].append("preview")';
		$m.__track_lines__[97] = 'bones.text.py, line 97:\n    self.appendChild(self.previewDiv)';
		$m.__track_lines__[99] = 'bones.text.py, line 99:\n    if self.isPlainText:';
		$m.__track_lines__[100] = 'bones.text.py, line 100:\n    self.previewDiv["style"]["display"] = "none"';
		$m.__track_lines__[102] = 'bones.text.py, line 102:\n    self.input["style"]["display"] = "none"';
		$m.__track_lines__[104] = 'bones.text.py, line 104:\n    if readOnly:';
		$m.__track_lines__[105] = 'bones.text.py, line 105:\n    self.input["readonly"] = True';
		$m.__track_lines__[108] = 'bones.text.py, line 108:\n    openEditorBtn = html5.ext.Button(translate("Edit Text"), self.openTxt )';
		$m.__track_lines__[109] = 'bones.text.py, line 109:\n    openEditorBtn["class"].append("textedit")';
		$m.__track_lines__[110] = 'bones.text.py, line 110:\n    openEditorBtn["class"].append("icon")';
		$m.__track_lines__[111] = 'bones.text.py, line 111:\n    self.appendChild( openEditorBtn )';
		$m.__track_lines__[113] = 'bones.text.py, line 113:\n    self.sinkEvent("onClick")';
		$m.__track_lines__[115] = 'bones.text.py, line 115:\n    def _setDisabled(self, disable):';
		$m.__track_lines__[119] = 'bones.text.py, line 119:\n    super(TextEditBone, self)._setDisabled( disable )';
		$m.__track_lines__[120] = 'bones.text.py, line 120:\n    if not disable and not self._disabledState and "is_active" in self.parent()["class"]:';
		$m.__track_lines__[121] = 'bones.text.py, line 121:\n    self.parent()["class"].remove("is_active")';
		$m.__track_lines__[123] = 'bones.text.py, line 123:\n    def openTxt(self, *args, **kwargs):';
		$m.__track_lines__[124] = 'bones.text.py, line 124:\n    assert self.currentEditor is None';
		$m.__track_lines__[125] = 'bones.text.py, line 125:\n    actionBarHint = self.boneName';
		$m.__track_lines__[126] = 'bones.text.py, line 126:\n    if self.descrHint:';
		$m.__track_lines__[127] = 'bones.text.py, line 127:\n    actionBarHint = self.descrHint';
		$m.__track_lines__[128] = 'bones.text.py, line 128:\n    self.currentEditor = Wysiwyg( self.input["value"], actionBarHint=actionBarHint )';
		$m.__track_lines__[129] = 'bones.text.py, line 129:\n    self.currentEditor.saveTextEvent.register( self )';
		$m.__track_lines__[130] = 'bones.text.py, line 130:\n    self.currentEditor.abortTextEvent.register(self)';
		$m.__track_lines__[131] = 'bones.text.py, line 131:\n    conf["mainWindow"].stackWidget( self.currentEditor )';
		$m.__track_lines__[132] = 'bones.text.py, line 132:\n    self.parent()["class"].append("is_active")';
		$m.__track_lines__[134] = 'bones.text.py, line 134:\n    def closeEditor(self):';
		$m.__track_lines__[135] = 'bones.text.py, line 135:\n    if not self.currentEditor:';
		$m.__track_lines__[136] = 'bones.text.py, line 136:\n    return';
		$m.__track_lines__[138] = 'bones.text.py, line 138:\n    conf["mainWindow"].removeWidget( self.currentEditor )';
		$m.__track_lines__[139] = 'bones.text.py, line 139:\n    self.currentEditor = None';
		$m.__track_lines__[141] = 'bones.text.py, line 141:\n    def onSaveText(self, editor, txt ):';
		$m.__track_lines__[142] = 'bones.text.py, line 142:\n    assert self.currentEditor is not None';
		$m.__track_lines__[144] = 'bones.text.py, line 144:\n    self.input["value"] = txt';
		$m.__track_lines__[146] = 'bones.text.py, line 146:\n    if not self.isPlainText:';
		$m.__track_lines__[147] = 'bones.text.py, line 147:\n    self.previewDiv.element.innerHTML = self.input["value"]';
		$m.__track_lines__[149] = 'bones.text.py, line 149:\n    self.closeEditor()';
		$m.__track_lines__[151] = 'bones.text.py, line 151:\n    def onAbortText(self, editor):';
		$m.__track_lines__[152] = 'bones.text.py, line 152:\n    assert self.currentEditor is not None';
		$m.__track_lines__[153] = 'bones.text.py, line 153:\n    self.closeEditor()';
		$m.__track_lines__[155] = 'bones.text.py, line 155:\n    def changeLang(self,btn):';
		$m.__track_lines__[156] = 'bones.text.py, line 156:\n    self.valuesdict[self.selectedLang]=self.input["value"]';
		$m.__track_lines__[157] = 'bones.text.py, line 157:\n    self.selectedLang=btn["value"]';
		$m.__track_lines__[158] = 'bones.text.py, line 158:\n    if self.selectedLang in self.valuesdict.keys():';
		$m.__track_lines__[159] = 'bones.text.py, line 159:\n    self.input["value"]=self.valuesdict[self.selectedLang]';
		$m.__track_lines__[161] = 'bones.text.py, line 161:\n    self.input["value"] = ""';
		$m.__track_lines__[162] = 'bones.text.py, line 162:\n    if not self.isPlainText:';
		$m.__track_lines__[163] = 'bones.text.py, line 163:\n    self.previewDiv.element.innerHTML = self.input["value"]';
		$m.__track_lines__[164] = 'bones.text.py, line 164:\n    self.refreshLangButContainer()';
		$m.__track_lines__[166] = 'bones.text.py, line 166:\n    def refreshLangButContainer(self):';
		$m.__track_lines__[167] = 'bones.text.py, line 167:\n    for abut in self.langButContainer._children:';
		$m.__track_lines__[169] = 'bones.text.py, line 169:\n    if abut["value"] in self.valuesdict and self.valuesdict[abut["value"]]:';
		$m.__track_lines__[170] = 'bones.text.py, line 170:\n    if not "is_filled" in abut["class"]:';
		$m.__track_lines__[171] = 'bones.text.py, line 171:\n    abut["class"].append("is_filled")';
		$m.__track_lines__[173] = 'bones.text.py, line 173:\n    if not "is_unfilled" in abut["class"]:';
		$m.__track_lines__[174] = 'bones.text.py, line 174:\n    abut["class"].append("is_unfilled")';
		$m.__track_lines__[176] = 'bones.text.py, line 176:\n    if abut["value"]==self.selectedLang:';
		$m.__track_lines__[177] = 'bones.text.py, line 177:\n    if not "is_active" in abut["class"]:';
		$m.__track_lines__[178] = 'bones.text.py, line 178:\n    abut["class"].append("is_active")';
		$m.__track_lines__[180] = 'bones.text.py, line 180:\n    abut["class"].remove("is_active")';
		$m.__track_lines__[183] = 'bones.text.py, line 182:\n    @staticmethod ... def fromSkelStructure( modulName, boneName, skelStructure ):';
		$m.__track_lines__[184] = 'bones.text.py, line 184:\n    readOnly = "readonly" in skelStructure[ boneName ].keys() and skelStructure[ boneName ]["readonly"]';
		$m.__track_lines__[185] = 'bones.text.py, line 185:\n    isPlainText = "validHtml" in skelStructure[ boneName ].keys() and not skelStructure[ boneName ]["validHtml"]';
		$m.__track_lines__[186] = 'bones.text.py, line 186:\n    langs = skelStructure[ boneName ]["languages"] if ("languages" in skelStructure[ boneName ].keys() and skelStructure[ boneName ]["languages"]) else None';
		$m.__track_lines__[187] = 'bones.text.py, line 187:\n    descr = skelStructure[ boneName ]["descr"] if "descr" in skelStructure[ boneName ].keys() else None';
		$m.__track_lines__[188] = 'bones.text.py, line 188:\n    return( TextEditBone( modulName, boneName, readOnly, isPlainText, langs, descrHint=descr ) )';
		$m.__track_lines__[190] = 'bones.text.py, line 190:\n    def unserialize(self, data):';
		$m.__track_lines__[191] = 'bones.text.py, line 191:\n    self.valuesdict.clear()';
		$m.__track_lines__[192] = 'bones.text.py, line 192:\n    if self.boneName in data.keys():';
		$m.__track_lines__[193] = 'bones.text.py, line 193:\n    if self.languages:';
		$m.__track_lines__[194] = 'bones.text.py, line 194:\n    for lang in self.languages:';
		$m.__track_lines__[195] = 'bones.text.py, line 195:\n    if self.boneName in data.keys() and isinstance(data[self.boneName],dict) and lang in data[ self.boneName ].keys():';
		$m.__track_lines__[196] = 'bones.text.py, line 196:\n    self.valuesdict[lang]=data[ self.boneName ][lang]';
		$m.__track_lines__[198] = 'bones.text.py, line 198:\n    self.valuesdict[lang]=""';
		$m.__track_lines__[199] = 'bones.text.py, line 199:\n    self.input["value"] = self.valuesdict[self.selectedLang]';
		$m.__track_lines__[201] = 'bones.text.py, line 201:\n    self.input["value"] = data[ self.boneName ] if data[ self.boneName ] else ""';
		$m.__track_lines__[202] = 'bones.text.py, line 202:\n    if not self.isPlainText:';
		$m.__track_lines__[203] = 'bones.text.py, line 203:\n    self.previewDiv.element.innerHTML = self.input["value"]';
		$m.__track_lines__[205] = 'bones.text.py, line 205:\n    def serializeForPost(self):';
		$m.__track_lines__[206] = 'bones.text.py, line 206:\n    if self.selectedLang:';
		$m.__track_lines__[207] = 'bones.text.py, line 207:\n    self.valuesdict[self.selectedLang]=self.input["value"]';
		$m.__track_lines__[208] = 'bones.text.py, line 208:\n    return( { self.boneName: self.valuesdict } )';
		$m.__track_lines__[210] = 'bones.text.py, line 210:\n    return( { self.boneName: self.input["value"] } )';
		$m.__track_lines__[212] = 'bones.text.py, line 212:\n    def onClick(self, event):';
		$m.__track_lines__[213] = 'bones.text.py, line 213:\n    if utils.doesEventHitWidgetOrChildren( event, self.previewDiv ):';
		$m.__track_lines__[214] = 'bones.text.py, line 214:\n    event.stopPropagation()';
		$m.__track_lines__[215] = 'bones.text.py, line 215:\n    event.preventDefault()';
		$m.__track_lines__[216] = 'bones.text.py, line 216:\n    if not self.readOnly:';
		$m.__track_lines__[217] = 'bones.text.py, line 217:\n    self.openTxt()';
		$m.__track_lines__[219] = 'bones.text.py, line 219:\n    def setExtendedErrorInformation(self, errorInfo ):';
		$m.__track_lines__[220] = 'bones.text.py, line 220:\n    pass';
		$m.__track_lines__[223] = 'bones.text.py, line 223:\n    def CheckForTextBone(  modulName, boneName, skelStucture, *args, **kwargs ):';
		$m.__track_lines__[224] = 'bones.text.py, line 224:\n    return( skelStucture[boneName]["type"]=="text" )';
		$m.__track_lines__[227] = 'bones.text.py, line 227:\n    editBoneSelector.insert( 3, CheckForTextBone, TextEditBone)';
		$m.__track_lines__[228] = 'bones.text.py, line 228:\n    viewDelegateSelector.insert( 3, CheckForTextBone, TextViewBoneDelegate)';
		$m.__track_lines__[229] = 'bones.text.py, line 229:\n    extractorDelegateSelector.insert(3, CheckForTextBone, TextBoneExtractor)';

		var $constant_int_0 = new $p['int'](0);
		var $constant_int_3 = new $p['int'](3);
		$pyjs['track']['module']='bones.text';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=3;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['html5'] = $p['___import___']('html5', 'bones');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=4;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['editBoneSelector'] = $p['___import___']('priorityqueue.editBoneSelector', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['viewDelegateSelector'] = $p['___import___']('priorityqueue.viewDelegateSelector', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['extractorDelegateSelector'] = $p['___import___']('priorityqueue.extractorDelegateSelector', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=5;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['conf'] = $p['___import___']('config.conf', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=6;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['Wysiwyg'] = $p['___import___']('widgets.wysiwyg.Wysiwyg', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=7;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['utils'] = $p['___import___']('utils', 'bones');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=8;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['translate'] = $p['___import___']('i18n.translate', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=11;
		$m['TextBoneExtractor'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.text';
			$cls_definition['__md5__'] = '5b702a3ed745e3a96f92b586263fa219';
			$pyjs['track']['lineno']=12;
			$method = $pyjs__bind_method2('__init__', function(modulName, boneName, skelStructure) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

					var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					modulName = arguments[1];
					boneName = arguments[2];
					skelStructure = arguments[3];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,4,arguments['length']-1));

					var kwargs = arguments['length'] >= 5 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '5b702a3ed745e3a96f92b586263fa219') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof skelStructure != 'undefined') {
						if (skelStructure !== null && typeof skelStructure['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = skelStructure;
							skelStructure = arguments[4];
						}
					} else 					if (typeof boneName != 'undefined') {
						if (boneName !== null && typeof boneName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = boneName;
							boneName = arguments[4];
						}
					} else 					if (typeof modulName != 'undefined') {
						if (modulName !== null && typeof modulName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modulName;
							modulName = arguments[4];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[4];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'bones.text', 'lineno':12};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=12;
				$pyjs['track']['lineno']=13;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['TextBoneExtractor'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})()['__init__']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})();
				$pyjs['track']['lineno']=14;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('skelStructure', skelStructure) : $p['setattr'](self, 'skelStructure', skelStructure); 
				$pyjs['track']['lineno']=15;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('boneName', boneName) : $p['setattr'](self, 'boneName', boneName); 
				$pyjs['track']['lineno']=16;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modulName', modulName) : $p['setattr'](self, 'modulName', modulName); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['modulName'],['boneName'],['skelStructure']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=18;
			$method = $pyjs__bind_method2('render', function(data, field) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					field = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '5b702a3ed745e3a96f92b586263fa219') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var resstr;
				$pyjs['track']={'module':'bones.text', 'lineno':18};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=18;
				$pyjs['track']['lineno']=19;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})()['__contains__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})()) {
					$pyjs['track']['lineno']=21;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](data['__getitem__'](field), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})()) {
						$pyjs['track']['lineno']=22;
						resstr = '';
						$pyjs['track']['lineno']=23;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['conf']['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})()['__contains__']('currentlanguage'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})()) {
							$pyjs['track']['lineno']=24;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
							return data['__getitem__'](field)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})()['__contains__']($m['conf']['__getitem__']('currentlanguage')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})()) {
								$pyjs['track']['lineno']=25;
								resstr = (function(){try{try{$pyjs['in_try_except'] += 1;
								return (function(){try{try{$pyjs['in_try_except'] += 1;
								return (function(){try{try{$pyjs['in_try_except'] += 1;
								return data['__getitem__'](field)['__getitem__']($m['conf']['__getitem__']('currentlanguage'))['$$replace']('&quot;', '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})()['$$replace'](';', ' ');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})()['$$replace']('"', "'");
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})();
							}
							else {
								$pyjs['track']['lineno']=27;
								if ((function(){try{try{$pyjs['in_try_except'] += 1;
									return $p['bool'](($p['cmp']($p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
								return data['__getitem__'](field)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_14_err){if (!$p['isinstance']($pyjs_dbg_14_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_14_err);}throw $pyjs_dbg_14_err;
}})(), 'length'), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})()) {
									$pyjs['track']['lineno']=28;
									resstr = (function(){try{try{$pyjs['in_try_except'] += 1;
									return (function(){try{try{$pyjs['in_try_except'] += 1;
									return (function(){try{try{$pyjs['in_try_except'] += 1;
									return data['__getitem__'](field)['__getitem__']((function(){try{try{$pyjs['in_try_except'] += 1;
									return data['__getitem__'](field)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_16_err){if (!$p['isinstance']($pyjs_dbg_16_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_16_err);}throw $pyjs_dbg_16_err;
}})()['__getitem__']($constant_int_0))['$$replace']('&quot;', '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_17_err){if (!$p['isinstance']($pyjs_dbg_17_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_17_err);}throw $pyjs_dbg_17_err;
}})()['$$replace'](';', ' ');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})()['$$replace']('"', "'");
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_19_err){if (!$p['isinstance']($pyjs_dbg_19_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_19_err);}throw $pyjs_dbg_19_err;
}})();
								}
							}
						}
						$pyjs['track']['lineno']=29;
						$pyjs['track']['lineno']=29;
						var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('"%s"', resstr);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})();
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
					else {
						$pyjs['track']['lineno']=32;
						$pyjs['track']['lineno']=32;
						var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['str']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('"%s"', (function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return data['__getitem__'](field)['$$replace']('&quot;', '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})()['$$replace'](';', ' ');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})()['$$replace']('"', "'");
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})();
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
				}
				$pyjs['track']['lineno']=33;
				$pyjs['track']['lineno']=33;
				var $pyjs__ret = $m['conf']['__getitem__']('empty_value');
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['data'],['field']]);
			$cls_definition['render'] = $method;
			$pyjs['track']['lineno']=11;
			var $bases = new Array($p['object']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('TextBoneExtractor', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=36;
		$m['TextViewBoneDelegate'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.text';
			$cls_definition['__md5__'] = '1aba47fb13ed76cba696bfe06d366fbc';
			$pyjs['track']['lineno']=37;
			$method = $pyjs__bind_method2('__init__', function(modulName, boneName, skelStructure) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

					var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					modulName = arguments[1];
					boneName = arguments[2];
					skelStructure = arguments[3];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,4,arguments['length']-1));

					var kwargs = arguments['length'] >= 5 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '1aba47fb13ed76cba696bfe06d366fbc') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof skelStructure != 'undefined') {
						if (skelStructure !== null && typeof skelStructure['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = skelStructure;
							skelStructure = arguments[4];
						}
					} else 					if (typeof boneName != 'undefined') {
						if (boneName !== null && typeof boneName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = boneName;
							boneName = arguments[4];
						}
					} else 					if (typeof modulName != 'undefined') {
						if (modulName !== null && typeof modulName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modulName;
							modulName = arguments[4];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[4];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'bones.text', 'lineno':37};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=37;
				$pyjs['track']['lineno']=38;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['TextViewBoneDelegate'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_26_err){if (!$p['isinstance']($pyjs_dbg_26_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_26_err);}throw $pyjs_dbg_26_err;
}})()['__init__']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_27_err){if (!$p['isinstance']($pyjs_dbg_27_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_27_err);}throw $pyjs_dbg_27_err;
}})();
				$pyjs['track']['lineno']=39;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('skelStructure', skelStructure) : $p['setattr'](self, 'skelStructure', skelStructure); 
				$pyjs['track']['lineno']=40;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('boneName', boneName) : $p['setattr'](self, 'boneName', boneName); 
				$pyjs['track']['lineno']=41;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modulName', modulName) : $p['setattr'](self, 'modulName', modulName); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['modulName'],['boneName'],['skelStructure']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=43;
			$method = $pyjs__bind_method2('render', function(data, field) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					field = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '1aba47fb13ed76cba696bfe06d366fbc') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var aspan,resstr;
				$pyjs['track']={'module':'bones.text', 'lineno':43};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=43;
				$pyjs['track']['lineno']=44;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_28_err){if (!$p['isinstance']($pyjs_dbg_28_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_28_err);}throw $pyjs_dbg_28_err;
}})()['__contains__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_29_err){if (!$p['isinstance']($pyjs_dbg_29_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_29_err);}throw $pyjs_dbg_29_err;
}})()) {
					$pyjs['track']['lineno']=46;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](data['__getitem__'](field), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_30_err){if (!$p['isinstance']($pyjs_dbg_30_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_30_err);}throw $pyjs_dbg_30_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_31_err){if (!$p['isinstance']($pyjs_dbg_31_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_31_err);}throw $pyjs_dbg_31_err;
}})()) {
						$pyjs['track']['lineno']=47;
						resstr = '';
						$pyjs['track']['lineno']=48;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['conf']['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_32_err){if (!$p['isinstance']($pyjs_dbg_32_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_32_err);}throw $pyjs_dbg_32_err;
}})()['__contains__']('currentlanguage'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_33_err){if (!$p['isinstance']($pyjs_dbg_33_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_33_err);}throw $pyjs_dbg_33_err;
}})()) {
							$pyjs['track']['lineno']=49;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
							return data['__getitem__'](field)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_34_err){if (!$p['isinstance']($pyjs_dbg_34_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_34_err);}throw $pyjs_dbg_34_err;
}})()['__contains__']($m['conf']['__getitem__']('currentlanguage')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})()) {
								$pyjs['track']['lineno']=50;
								resstr = data['__getitem__'](field)['__getitem__']($m['conf']['__getitem__']('currentlanguage'));
							}
							else {
								$pyjs['track']['lineno']=52;
								if ((function(){try{try{$pyjs['in_try_except'] += 1;
									return $p['bool'](($p['cmp']($p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
								return data['__getitem__'](field)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_36_err){if (!$p['isinstance']($pyjs_dbg_36_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_36_err);}throw $pyjs_dbg_36_err;
}})(), 'length'), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_37_err){if (!$p['isinstance']($pyjs_dbg_37_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_37_err);}throw $pyjs_dbg_37_err;
}})()) {
									$pyjs['track']['lineno']=53;
									resstr = data['__getitem__'](field)['__getitem__']((function(){try{try{$pyjs['in_try_except'] += 1;
									return data['__getitem__'](field)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_38_err){if (!$p['isinstance']($pyjs_dbg_38_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_38_err);}throw $pyjs_dbg_38_err;
}})()['__getitem__']($constant_int_0));
								}
							}
						}
						$pyjs['track']['lineno']=54;
						aspan = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_39_err){if (!$p['isinstance']($pyjs_dbg_39_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_39_err);}throw $pyjs_dbg_39_err;
}})();
						$pyjs['track']['lineno']=55;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return aspan['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['TextNode'](resstr);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_40_err){if (!$p['isinstance']($pyjs_dbg_40_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_40_err);}throw $pyjs_dbg_40_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_41_err){if (!$p['isinstance']($pyjs_dbg_41_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_41_err);}throw $pyjs_dbg_41_err;
}})();
						$pyjs['track']['lineno']=56;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return aspan['__setitem__']('Title', (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['str'](data['__getitem__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_42_err){if (!$p['isinstance']($pyjs_dbg_42_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_42_err);}throw $pyjs_dbg_42_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_43_err){if (!$p['isinstance']($pyjs_dbg_43_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_43_err);}throw $pyjs_dbg_43_err;
}})();
						$pyjs['track']['lineno']=57;
						$pyjs['track']['lineno']=57;
						var $pyjs__ret = aspan;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
					else {
						$pyjs['track']['lineno']=60;
						$pyjs['track']['lineno']=60;
						var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Label']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['str'](data['__getitem__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_44_err){if (!$p['isinstance']($pyjs_dbg_44_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_44_err);}throw $pyjs_dbg_44_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_45_err){if (!$p['isinstance']($pyjs_dbg_45_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_45_err);}throw $pyjs_dbg_45_err;
}})();
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
				}
				$pyjs['track']['lineno']=61;
				$pyjs['track']['lineno']=61;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Label']($m['conf']['__getitem__']('empty_value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_46_err){if (!$p['isinstance']($pyjs_dbg_46_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_46_err);}throw $pyjs_dbg_46_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['data'],['field']]);
			$cls_definition['render'] = $method;
			$pyjs['track']['lineno']=36;
			var $bases = new Array($p['object']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('TextViewBoneDelegate', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=63;
		$m['TextEditBone'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.text';
			$cls_definition['__md5__'] = '4a705ca8f8c26eb25df01d4ed8c671fc';
			$pyjs['track']['lineno']=64;
			$method = $pyjs__bind_method2('__init__', function(modulName, boneName, readOnly, isPlainText, languages, descrHint) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,6,arguments['length']-1));

					var kwargs = arguments['length'] >= 7 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 5, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					modulName = arguments[1];
					boneName = arguments[2];
					readOnly = arguments[3];
					isPlainText = arguments[4];
					languages = arguments[5];
					descrHint = arguments[6];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,7,arguments['length']-1));

					var kwargs = arguments['length'] >= 8 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 5) $pyjs__exception_func_param(arguments['callee']['__name__'], 5, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '4a705ca8f8c26eb25df01d4ed8c671fc') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof descrHint != 'undefined') {
						if (descrHint !== null && typeof descrHint['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = descrHint;
							descrHint = arguments[7];
						}
					} else 					if (typeof languages != 'undefined') {
						if (languages !== null && typeof languages['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = languages;
							languages = arguments[7];
						}
					} else 					if (typeof isPlainText != 'undefined') {
						if (isPlainText !== null && typeof isPlainText['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = isPlainText;
							isPlainText = arguments[7];
						}
					} else 					if (typeof readOnly != 'undefined') {
						if (readOnly !== null && typeof readOnly['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = readOnly;
							readOnly = arguments[7];
						}
					} else 					if (typeof boneName != 'undefined') {
						if (boneName !== null && typeof boneName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = boneName;
							boneName = arguments[7];
						}
					} else 					if (typeof modulName != 'undefined') {
						if (modulName !== null && typeof modulName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modulName;
							modulName = arguments[7];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[7];
						}
					} else {
					}
				}
				if (typeof languages == 'undefined') languages=arguments['callee']['__args__'][7][1];
				if (typeof descrHint == 'undefined') descrHint=arguments['callee']['__args__'][8][1];
				var $iter1_iter,$iter1_array,$iter1_nextval,$and1,$and2,$and3,$and4,abut,lang,$iter1_type,openEditorBtn,$iter1_idx,$pyjs__trackstack_size_1;
				$pyjs['track']={'module':'bones.text', 'lineno':64};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=64;
				$pyjs['track']['lineno']=65;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['TextEditBone'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_48_err){if (!$p['isinstance']($pyjs_dbg_48_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_48_err);}throw $pyjs_dbg_48_err;
}})();
				$pyjs['track']['lineno']=66;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('boneName', boneName) : $p['setattr'](self, 'boneName', boneName); 
				$pyjs['track']['lineno']=67;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('readOnly', readOnly) : $p['setattr'](self, 'readOnly', readOnly); 
				$pyjs['track']['lineno']=68;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectedLang', false) : $p['setattr'](self, 'selectedLang', false); 
				$pyjs['track']['lineno']=69;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('isPlainText', isPlainText) : $p['setattr'](self, 'isPlainText', isPlainText); 
				$pyjs['track']['lineno']=70;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('languages', languages) : $p['setattr'](self, 'languages', languages); 
				$pyjs['track']['lineno']=71;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('descrHint', descrHint) : $p['setattr'](self, 'descrHint', descrHint); 
				$pyjs['track']['lineno']=72;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentEditor', null) : $p['setattr'](self, 'currentEditor', null); 
				$pyjs['track']['lineno']=73;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('valuesdict', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_49_err){if (!$p['isinstance']($pyjs_dbg_49_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_49_err);}throw $pyjs_dbg_49_err;
}})()) : $p['setattr'](self, 'valuesdict', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_49_err){if (!$p['isinstance']($pyjs_dbg_49_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_49_err);}throw $pyjs_dbg_49_err;
}})()); 
				$pyjs['track']['lineno']=76;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'languages'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_50_err){if (!$p['isinstance']($pyjs_dbg_50_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_50_err);}throw $pyjs_dbg_50_err;
}})()) {
					$pyjs['track']['lineno']=77;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and1=$m['conf']['__contains__']('currentlanguage'))?$p['getattr'](self, 'languages')['__contains__']($m['conf']['__getitem__']('currentlanguage')):$and1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_51_err){if (!$p['isinstance']($pyjs_dbg_51_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_51_err);}throw $pyjs_dbg_51_err;
}})()) {
						$pyjs['track']['lineno']=78;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectedLang', $m['conf']['__getitem__']('currentlanguage')) : $p['setattr'](self, 'selectedLang', $m['conf']['__getitem__']('currentlanguage')); 
					}
					else if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['len']($p['getattr'](self, 'languages'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_52_err){if (!$p['isinstance']($pyjs_dbg_52_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_52_err);}throw $pyjs_dbg_52_err;
}})(), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_53_err){if (!$p['isinstance']($pyjs_dbg_53_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_53_err);}throw $pyjs_dbg_53_err;
}})()) {
						$pyjs['track']['lineno']=80;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectedLang', $p['getattr'](self, 'languages')['__getitem__']($constant_int_0)) : $p['setattr'](self, 'selectedLang', $p['getattr'](self, 'languages')['__getitem__']($constant_int_0)); 
					}
					$pyjs['track']['lineno']=82;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('langButContainer', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_54_err){if (!$p['isinstance']($pyjs_dbg_54_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_54_err);}throw $pyjs_dbg_54_err;
}})()) : $p['setattr'](self, 'langButContainer', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_54_err){if (!$p['isinstance']($pyjs_dbg_54_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_54_err);}throw $pyjs_dbg_54_err;
}})()); 
					$pyjs['track']['lineno']=83;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'langButContainer')['__getitem__']('class')['append']('languagebuttons');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_55_err){if (!$p['isinstance']($pyjs_dbg_55_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_55_err);}throw $pyjs_dbg_55_err;
}})();
					$pyjs['track']['lineno']=85;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter1_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'languages');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_56_err){if (!$p['isinstance']($pyjs_dbg_56_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_56_err);}throw $pyjs_dbg_56_err;
}})();
					$iter1_nextval=$p['__iter_prepare']($iter1_iter,false);
					while (typeof($p['__wrapped_next']($iter1_nextval)['$nextval']) != 'undefined') {
						lang = $iter1_nextval['$nextval'];
						$pyjs['track']['lineno']=86;
						abut = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['ext']['Button'](lang, $p['getattr'](self, 'changeLang'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_57_err){if (!$p['isinstance']($pyjs_dbg_57_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_57_err);}throw $pyjs_dbg_57_err;
}})();
						$pyjs['track']['lineno']=87;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return abut['__setitem__']('value', lang);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_58_err){if (!$p['isinstance']($pyjs_dbg_58_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_58_err);}throw $pyjs_dbg_58_err;
}})();
						$pyjs['track']['lineno']=88;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['langButContainer']['appendChild'](abut);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_59_err){if (!$p['isinstance']($pyjs_dbg_59_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_59_err);}throw $pyjs_dbg_59_err;
}})();
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.text';
					$pyjs['track']['lineno']=90;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'langButContainer'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_60_err){if (!$p['isinstance']($pyjs_dbg_60_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_60_err);}throw $pyjs_dbg_60_err;
}})();
					$pyjs['track']['lineno']=91;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['refreshLangButContainer']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_61_err){if (!$p['isinstance']($pyjs_dbg_61_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_61_err);}throw $pyjs_dbg_61_err;
}})();
				}
				$pyjs['track']['lineno']=93;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('input', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Textarea']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_62_err){if (!$p['isinstance']($pyjs_dbg_62_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_62_err);}throw $pyjs_dbg_62_err;
}})()) : $p['setattr'](self, 'input', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Textarea']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_62_err){if (!$p['isinstance']($pyjs_dbg_62_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_62_err);}throw $pyjs_dbg_62_err;
}})()); 
				$pyjs['track']['lineno']=94;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'input'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_63_err){if (!$p['isinstance']($pyjs_dbg_63_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_63_err);}throw $pyjs_dbg_63_err;
}})();
				$pyjs['track']['lineno']=95;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('previewDiv', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})()) : $p['setattr'](self, 'previewDiv', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})()); 
				$pyjs['track']['lineno']=96;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'previewDiv')['__getitem__']('class')['append']('preview');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_65_err){if (!$p['isinstance']($pyjs_dbg_65_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_65_err);}throw $pyjs_dbg_65_err;
}})();
				$pyjs['track']['lineno']=97;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'previewDiv'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_66_err){if (!$p['isinstance']($pyjs_dbg_66_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_66_err);}throw $pyjs_dbg_66_err;
}})();
				$pyjs['track']['lineno']=99;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'isPlainText'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})()) {
					$pyjs['track']['lineno']=100;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'previewDiv')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_68_err){if (!$p['isinstance']($pyjs_dbg_68_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_68_err);}throw $pyjs_dbg_68_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=102;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'input')['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_69_err){if (!$p['isinstance']($pyjs_dbg_69_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_69_err);}throw $pyjs_dbg_69_err;
}})();
				}
				$pyjs['track']['lineno']=104;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](readOnly);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_70_err){if (!$p['isinstance']($pyjs_dbg_70_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_70_err);}throw $pyjs_dbg_70_err;
}})()) {
					$pyjs['track']['lineno']=105;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'input')['__setitem__']('readonly', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_71_err){if (!$p['isinstance']($pyjs_dbg_71_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_71_err);}throw $pyjs_dbg_71_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and3=!$p['bool'](readOnly))?!$p['bool']($p['getattr'](self, 'isPlainText')):$and3));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_72_err){if (!$p['isinstance']($pyjs_dbg_72_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_72_err);}throw $pyjs_dbg_72_err;
}})()) {
					$pyjs['track']['lineno']=108;
					openEditorBtn = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['ext']['Button']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Edit Text');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_73_err){if (!$p['isinstance']($pyjs_dbg_73_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_73_err);}throw $pyjs_dbg_73_err;
}})(), $p['getattr'](self, 'openTxt'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_74_err){if (!$p['isinstance']($pyjs_dbg_74_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_74_err);}throw $pyjs_dbg_74_err;
}})();
					$pyjs['track']['lineno']=109;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return openEditorBtn['__getitem__']('class')['append']('textedit');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})();
					$pyjs['track']['lineno']=110;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return openEditorBtn['__getitem__']('class')['append']('icon');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})();
					$pyjs['track']['lineno']=111;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild'](openEditorBtn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_77_err){if (!$p['isinstance']($pyjs_dbg_77_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_77_err);}throw $pyjs_dbg_77_err;
}})();
				}
				$pyjs['track']['lineno']=113;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['sinkEvent']('onClick');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_78_err){if (!$p['isinstance']($pyjs_dbg_78_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_78_err);}throw $pyjs_dbg_78_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['modulName'],['boneName'],['readOnly'],['isPlainText'],['languages', null],['descrHint', null]]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=115;
			$method = $pyjs__bind_method2('_setDisabled', function(disable) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					disable = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '4a705ca8f8c26eb25df01d4ed8c671fc') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and5,$and6,$and7;
				$pyjs['track']={'module':'bones.text', 'lineno':115};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=115;
				$pyjs['track']['lineno']=119;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['TextEditBone'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_79_err){if (!$p['isinstance']($pyjs_dbg_79_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_79_err);}throw $pyjs_dbg_79_err;
}})()['_setDisabled'](disable);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_80_err){if (!$p['isinstance']($pyjs_dbg_80_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_80_err);}throw $pyjs_dbg_80_err;
}})();
				$pyjs['track']['lineno']=120;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and5=!$p['bool'](disable))?($p['bool']($and6=!$p['bool']($p['getattr'](self, '_disabledState')))?(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_81_err){if (!$p['isinstance']($pyjs_dbg_81_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_81_err);}throw $pyjs_dbg_81_err;
}})()['__getitem__']('class')['__contains__']('is_active'):$and6):$and5));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_82_err){if (!$p['isinstance']($pyjs_dbg_82_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_82_err);}throw $pyjs_dbg_82_err;
}})()) {
					$pyjs['track']['lineno']=121;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_83_err){if (!$p['isinstance']($pyjs_dbg_83_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_83_err);}throw $pyjs_dbg_83_err;
}})()['__getitem__']('class')['remove']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_84_err){if (!$p['isinstance']($pyjs_dbg_84_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_84_err);}throw $pyjs_dbg_84_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['disable']]);
			$cls_definition['_setDisabled'] = $method;
			$pyjs['track']['lineno']=123;
			$method = $pyjs__bind_method2('openTxt', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '4a705ca8f8c26eb25df01d4ed8c671fc') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var actionBarHint;
				$pyjs['track']={'module':'bones.text', 'lineno':123};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=123;
				$pyjs['track']['lineno']=124;
				if (!( $p['op_is']($p['getattr'](self, 'currentEditor'), null) )) {
				   throw $p['AssertionError']();
				 }
				$pyjs['track']['lineno']=125;
				actionBarHint = $p['getattr'](self, 'boneName');
				$pyjs['track']['lineno']=126;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'descrHint'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})()) {
					$pyjs['track']['lineno']=127;
					actionBarHint = $p['getattr'](self, 'descrHint');
				}
				$pyjs['track']['lineno']=128;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentEditor', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['Wysiwyg'], null, null, [{'actionBarHint':actionBarHint}, $p['getattr'](self, 'input')['__getitem__']('value')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})()) : $p['setattr'](self, 'currentEditor', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['Wysiwyg'], null, null, [{'actionBarHint':actionBarHint}, $p['getattr'](self, 'input')['__getitem__']('value')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})()); 
				$pyjs['track']['lineno']=129;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['currentEditor']['saveTextEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})();
				$pyjs['track']['lineno']=130;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['currentEditor']['abortTextEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})();
				$pyjs['track']['lineno']=131;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['stackWidget']($p['getattr'](self, 'currentEditor'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_89_err){if (!$p['isinstance']($pyjs_dbg_89_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_89_err);}throw $pyjs_dbg_89_err;
}})();
				$pyjs['track']['lineno']=132;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})()['__getitem__']('class')['append']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_91_err){if (!$p['isinstance']($pyjs_dbg_91_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_91_err);}throw $pyjs_dbg_91_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['openTxt'] = $method;
			$pyjs['track']['lineno']=134;
			$method = $pyjs__bind_method2('closeEditor', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '4a705ca8f8c26eb25df01d4ed8c671fc') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.text', 'lineno':134};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=134;
				$pyjs['track']['lineno']=135;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, 'currentEditor')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_92_err){if (!$p['isinstance']($pyjs_dbg_92_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_92_err);}throw $pyjs_dbg_92_err;
}})()) {
					$pyjs['track']['lineno']=136;
					$pyjs['track']['lineno']=136;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=138;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['removeWidget']($p['getattr'](self, 'currentEditor'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_93_err){if (!$p['isinstance']($pyjs_dbg_93_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_93_err);}throw $pyjs_dbg_93_err;
}})();
				$pyjs['track']['lineno']=139;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentEditor', null) : $p['setattr'](self, 'currentEditor', null); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['closeEditor'] = $method;
			$pyjs['track']['lineno']=141;
			$method = $pyjs__bind_method2('onSaveText', function(editor, txt) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					editor = arguments[1];
					txt = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '4a705ca8f8c26eb25df01d4ed8c671fc') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.text', 'lineno':141};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=141;
				$pyjs['track']['lineno']=142;
				if (!( !$p['op_is']($p['getattr'](self, 'currentEditor'), null) )) {
				   throw $p['AssertionError']();
				 }
				$pyjs['track']['lineno']=144;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'input')['__setitem__']('value', txt);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_94_err){if (!$p['isinstance']($pyjs_dbg_94_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_94_err);}throw $pyjs_dbg_94_err;
}})();
				$pyjs['track']['lineno']=146;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, 'isPlainText')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_95_err){if (!$p['isinstance']($pyjs_dbg_95_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_95_err);}throw $pyjs_dbg_95_err;
}})()) {
					$pyjs['track']['lineno']=147;
					$p['getattr']($p['getattr'](self, 'previewDiv'), 'element')['__is_instance__'] && typeof $p['getattr']($p['getattr'](self, 'previewDiv'), 'element')['__setattr__'] == 'function' ? $p['getattr']($p['getattr'](self, 'previewDiv'), 'element')['__setattr__']('innerHTML', $p['getattr'](self, 'input')['__getitem__']('value')) : $p['setattr']($p['getattr']($p['getattr'](self, 'previewDiv'), 'element'), 'innerHTML', $p['getattr'](self, 'input')['__getitem__']('value')); 
				}
				$pyjs['track']['lineno']=149;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['closeEditor']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_96_err){if (!$p['isinstance']($pyjs_dbg_96_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_96_err);}throw $pyjs_dbg_96_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['editor'],['txt']]);
			$cls_definition['onSaveText'] = $method;
			$pyjs['track']['lineno']=151;
			$method = $pyjs__bind_method2('onAbortText', function(editor) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					editor = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '4a705ca8f8c26eb25df01d4ed8c671fc') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.text', 'lineno':151};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=151;
				$pyjs['track']['lineno']=152;
				if (!( !$p['op_is']($p['getattr'](self, 'currentEditor'), null) )) {
				   throw $p['AssertionError']();
				 }
				$pyjs['track']['lineno']=153;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['closeEditor']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_97_err){if (!$p['isinstance']($pyjs_dbg_97_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_97_err);}throw $pyjs_dbg_97_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['editor']]);
			$cls_definition['onAbortText'] = $method;
			$pyjs['track']['lineno']=155;
			$method = $pyjs__bind_method2('changeLang', function(btn) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					btn = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '4a705ca8f8c26eb25df01d4ed8c671fc') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.text', 'lineno':155};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=155;
				$pyjs['track']['lineno']=156;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'valuesdict')['__setitem__']($p['getattr'](self, 'selectedLang'), $p['getattr'](self, 'input')['__getitem__']('value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_98_err){if (!$p['isinstance']($pyjs_dbg_98_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_98_err);}throw $pyjs_dbg_98_err;
}})();
				$pyjs['track']['lineno']=157;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectedLang', btn['__getitem__']('value')) : $p['setattr'](self, 'selectedLang', btn['__getitem__']('value')); 
				$pyjs['track']['lineno']=158;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return self['valuesdict']['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_99_err){if (!$p['isinstance']($pyjs_dbg_99_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_99_err);}throw $pyjs_dbg_99_err;
}})()['__contains__']($p['getattr'](self, 'selectedLang')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_100_err){if (!$p['isinstance']($pyjs_dbg_100_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_100_err);}throw $pyjs_dbg_100_err;
}})()) {
					$pyjs['track']['lineno']=159;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'input')['__setitem__']('value', $p['getattr'](self, 'valuesdict')['__getitem__']($p['getattr'](self, 'selectedLang')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_101_err){if (!$p['isinstance']($pyjs_dbg_101_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_101_err);}throw $pyjs_dbg_101_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=161;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'input')['__setitem__']('value', '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_102_err){if (!$p['isinstance']($pyjs_dbg_102_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_102_err);}throw $pyjs_dbg_102_err;
}})();
				}
				$pyjs['track']['lineno']=162;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, 'isPlainText')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_103_err){if (!$p['isinstance']($pyjs_dbg_103_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_103_err);}throw $pyjs_dbg_103_err;
}})()) {
					$pyjs['track']['lineno']=163;
					$p['getattr']($p['getattr'](self, 'previewDiv'), 'element')['__is_instance__'] && typeof $p['getattr']($p['getattr'](self, 'previewDiv'), 'element')['__setattr__'] == 'function' ? $p['getattr']($p['getattr'](self, 'previewDiv'), 'element')['__setattr__']('innerHTML', $p['getattr'](self, 'input')['__getitem__']('value')) : $p['setattr']($p['getattr']($p['getattr'](self, 'previewDiv'), 'element'), 'innerHTML', $p['getattr'](self, 'input')['__getitem__']('value')); 
				}
				$pyjs['track']['lineno']=164;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['refreshLangButContainer']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_104_err){if (!$p['isinstance']($pyjs_dbg_104_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_104_err);}throw $pyjs_dbg_104_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['btn']]);
			$cls_definition['changeLang'] = $method;
			$pyjs['track']['lineno']=166;
			$method = $pyjs__bind_method2('refreshLangButContainer', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '4a705ca8f8c26eb25df01d4ed8c671fc') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and8,$and9,$iter2_nextval,$iter2_type,$iter2_iter,abut,$iter2_idx,$pyjs__trackstack_size_1,$iter2_array;
				$pyjs['track']={'module':'bones.text', 'lineno':166};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=166;
				$pyjs['track']['lineno']=167;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter2_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr']($p['getattr'](self, 'langButContainer'), '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_105_err){if (!$p['isinstance']($pyjs_dbg_105_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_105_err);}throw $pyjs_dbg_105_err;
}})();
				$iter2_nextval=$p['__iter_prepare']($iter2_iter,false);
				while (typeof($p['__wrapped_next']($iter2_nextval)['$nextval']) != 'undefined') {
					abut = $iter2_nextval['$nextval'];
					$pyjs['track']['lineno']=169;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and8=$p['getattr'](self, 'valuesdict')['__contains__'](abut['__getitem__']('value')))?$p['getattr'](self, 'valuesdict')['__getitem__'](abut['__getitem__']('value')):$and8));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_106_err){if (!$p['isinstance']($pyjs_dbg_106_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_106_err);}throw $pyjs_dbg_106_err;
}})()) {
						$pyjs['track']['lineno']=170;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['bool'](abut['__getitem__']('class')['__contains__']('is_filled')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_107_err){if (!$p['isinstance']($pyjs_dbg_107_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_107_err);}throw $pyjs_dbg_107_err;
}})()) {
							$pyjs['track']['lineno']=171;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return abut['__getitem__']('class')['append']('is_filled');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_108_err){if (!$p['isinstance']($pyjs_dbg_108_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_108_err);}throw $pyjs_dbg_108_err;
}})();
						}
					}
					else {
						$pyjs['track']['lineno']=173;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['bool'](abut['__getitem__']('class')['__contains__']('is_unfilled')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_109_err){if (!$p['isinstance']($pyjs_dbg_109_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_109_err);}throw $pyjs_dbg_109_err;
}})()) {
							$pyjs['track']['lineno']=174;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return abut['__getitem__']('class')['append']('is_unfilled');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_110_err){if (!$p['isinstance']($pyjs_dbg_110_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_110_err);}throw $pyjs_dbg_110_err;
}})();
						}
					}
					$pyjs['track']['lineno']=176;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_eq'](abut['__getitem__']('value'), $p['getattr'](self, 'selectedLang')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_111_err){if (!$p['isinstance']($pyjs_dbg_111_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_111_err);}throw $pyjs_dbg_111_err;
}})()) {
						$pyjs['track']['lineno']=177;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['bool'](abut['__getitem__']('class')['__contains__']('is_active')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_112_err){if (!$p['isinstance']($pyjs_dbg_112_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_112_err);}throw $pyjs_dbg_112_err;
}})()) {
							$pyjs['track']['lineno']=178;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return abut['__getitem__']('class')['append']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_113_err){if (!$p['isinstance']($pyjs_dbg_113_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_113_err);}throw $pyjs_dbg_113_err;
}})();
						}
					}
					else {
						$pyjs['track']['lineno']=180;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return abut['__getitem__']('class')['remove']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_114_err){if (!$p['isinstance']($pyjs_dbg_114_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_114_err);}throw $pyjs_dbg_114_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='bones.text';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['refreshLangButContainer'] = $method;
			$pyjs['track']['lineno']=183;
			$method = $pyjs__bind_method2('fromSkelStructure', function(modulName, boneName, skelStructure) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var descr,readOnly,$and12,$and13,$and10,$and14,$and15,langs,isPlainText,$and11;
				$pyjs['track']={'module':'bones.text', 'lineno':183};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=183;
				$pyjs['track']['lineno']=184;
				readOnly = ($p['bool']($and10=(function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_115_err){if (!$p['isinstance']($pyjs_dbg_115_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_115_err);}throw $pyjs_dbg_115_err;
}})()['__contains__']('readonly'))?skelStructure['__getitem__'](boneName)['__getitem__']('readonly'):$and10);
				$pyjs['track']['lineno']=185;
				isPlainText = ($p['bool']($and12=(function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_116_err){if (!$p['isinstance']($pyjs_dbg_116_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_116_err);}throw $pyjs_dbg_116_err;
}})()['__contains__']('validHtml'))?!$p['bool'](skelStructure['__getitem__'](boneName)['__getitem__']('validHtml')):$and12);
				$pyjs['track']['lineno']=186;
				langs = ($p['bool'](($p['bool']($and14=(function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_117_err){if (!$p['isinstance']($pyjs_dbg_117_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_117_err);}throw $pyjs_dbg_117_err;
}})()['__contains__']('languages'))?skelStructure['__getitem__'](boneName)['__getitem__']('languages'):$and14))? (skelStructure['__getitem__'](boneName)['__getitem__']('languages')) : (null));
				$pyjs['track']['lineno']=187;
				descr = ($p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_118_err){if (!$p['isinstance']($pyjs_dbg_118_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_118_err);}throw $pyjs_dbg_118_err;
}})()['__contains__']('descr'))? (skelStructure['__getitem__'](boneName)['__getitem__']('descr')) : (null));
				$pyjs['track']['lineno']=188;
				$pyjs['track']['lineno']=188;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['TextEditBone'], null, null, [{'descrHint':descr}, modulName, boneName, readOnly, isPlainText, langs]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_119_err){if (!$p['isinstance']($pyjs_dbg_119_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_119_err);}throw $pyjs_dbg_119_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modulName'],['boneName'],['skelStructure']]);
			$cls_definition['fromSkelStructure'] = $method;
			$pyjs['track']['lineno']=190;
			$method = $pyjs__bind_method2('unserialize', function(data) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '4a705ca8f8c26eb25df01d4ed8c671fc') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var lang,$iter3_idx,$iter3_type,$pyjs__trackstack_size_1,$and16,$iter3_iter,$iter3_array,$and18,$iter3_nextval,$and17;
				$pyjs['track']={'module':'bones.text', 'lineno':190};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=190;
				$pyjs['track']['lineno']=191;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['valuesdict']['clear']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_120_err){if (!$p['isinstance']($pyjs_dbg_120_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_120_err);}throw $pyjs_dbg_120_err;
}})();
				$pyjs['track']['lineno']=192;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_121_err){if (!$p['isinstance']($pyjs_dbg_121_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_121_err);}throw $pyjs_dbg_121_err;
}})()['__contains__']($p['getattr'](self, 'boneName')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_122_err){if (!$p['isinstance']($pyjs_dbg_122_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_122_err);}throw $pyjs_dbg_122_err;
}})()) {
					$pyjs['track']['lineno']=193;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'languages'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_123_err){if (!$p['isinstance']($pyjs_dbg_123_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_123_err);}throw $pyjs_dbg_123_err;
}})()) {
						$pyjs['track']['lineno']=194;
						$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
						$iter3_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'languages');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_124_err){if (!$p['isinstance']($pyjs_dbg_124_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_124_err);}throw $pyjs_dbg_124_err;
}})();
						$iter3_nextval=$p['__iter_prepare']($iter3_iter,false);
						while (typeof($p['__wrapped_next']($iter3_nextval)['$nextval']) != 'undefined') {
							lang = $iter3_nextval['$nextval'];
							$pyjs['track']['lineno']=195;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool'](($p['bool']($and16=(function(){try{try{$pyjs['in_try_except'] += 1;
							return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_125_err){if (!$p['isinstance']($pyjs_dbg_125_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_125_err);}throw $pyjs_dbg_125_err;
}})()['__contains__']($p['getattr'](self, 'boneName')))?($p['bool']($and17=(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['isinstance'](data['__getitem__']($p['getattr'](self, 'boneName')), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_126_err){if (!$p['isinstance']($pyjs_dbg_126_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_126_err);}throw $pyjs_dbg_126_err;
}})())?(function(){try{try{$pyjs['in_try_except'] += 1;
							return data['__getitem__']($p['getattr'](self, 'boneName'))['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_127_err){if (!$p['isinstance']($pyjs_dbg_127_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_127_err);}throw $pyjs_dbg_127_err;
}})()['__contains__'](lang):$and17):$and16));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_128_err){if (!$p['isinstance']($pyjs_dbg_128_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_128_err);}throw $pyjs_dbg_128_err;
}})()) {
								$pyjs['track']['lineno']=196;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['getattr'](self, 'valuesdict')['__setitem__'](lang, data['__getitem__']($p['getattr'](self, 'boneName'))['__getitem__'](lang));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_129_err){if (!$p['isinstance']($pyjs_dbg_129_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_129_err);}throw $pyjs_dbg_129_err;
}})();
							}
							else {
								$pyjs['track']['lineno']=198;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['getattr'](self, 'valuesdict')['__setitem__'](lang, '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_130_err){if (!$p['isinstance']($pyjs_dbg_130_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_130_err);}throw $pyjs_dbg_130_err;
}})();
							}
						}
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='bones.text';
						$pyjs['track']['lineno']=199;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'input')['__setitem__']('value', $p['getattr'](self, 'valuesdict')['__getitem__']($p['getattr'](self, 'selectedLang')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_131_err){if (!$p['isinstance']($pyjs_dbg_131_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_131_err);}throw $pyjs_dbg_131_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=201;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'input')['__setitem__']('value', ($p['bool'](data['__getitem__']($p['getattr'](self, 'boneName')))? (data['__getitem__']($p['getattr'](self, 'boneName'))) : ('')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_132_err){if (!$p['isinstance']($pyjs_dbg_132_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_132_err);}throw $pyjs_dbg_132_err;
}})();
					}
				}
				$pyjs['track']['lineno']=202;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, 'isPlainText')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_133_err){if (!$p['isinstance']($pyjs_dbg_133_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_133_err);}throw $pyjs_dbg_133_err;
}})()) {
					$pyjs['track']['lineno']=203;
					$p['getattr']($p['getattr'](self, 'previewDiv'), 'element')['__is_instance__'] && typeof $p['getattr']($p['getattr'](self, 'previewDiv'), 'element')['__setattr__'] == 'function' ? $p['getattr']($p['getattr'](self, 'previewDiv'), 'element')['__setattr__']('innerHTML', $p['getattr'](self, 'input')['__getitem__']('value')) : $p['setattr']($p['getattr']($p['getattr'](self, 'previewDiv'), 'element'), 'innerHTML', $p['getattr'](self, 'input')['__getitem__']('value')); 
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['data']]);
			$cls_definition['unserialize'] = $method;
			$pyjs['track']['lineno']=205;
			$method = $pyjs__bind_method2('serializeForPost', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '4a705ca8f8c26eb25df01d4ed8c671fc') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.text', 'lineno':205};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=205;
				$pyjs['track']['lineno']=206;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'selectedLang'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_134_err){if (!$p['isinstance']($pyjs_dbg_134_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_134_err);}throw $pyjs_dbg_134_err;
}})()) {
					$pyjs['track']['lineno']=207;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'valuesdict')['__setitem__']($p['getattr'](self, 'selectedLang'), $p['getattr'](self, 'input')['__getitem__']('value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_135_err){if (!$p['isinstance']($pyjs_dbg_135_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_135_err);}throw $pyjs_dbg_135_err;
}})();
					$pyjs['track']['lineno']=208;
					$pyjs['track']['lineno']=208;
					var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([[$p['getattr'](self, 'boneName'), $p['getattr'](self, 'valuesdict')]]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_136_err){if (!$p['isinstance']($pyjs_dbg_136_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_136_err);}throw $pyjs_dbg_136_err;
}})();
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				else {
					$pyjs['track']['lineno']=210;
					$pyjs['track']['lineno']=210;
					var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([[$p['getattr'](self, 'boneName'), $p['getattr'](self, 'input')['__getitem__']('value')]]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_137_err){if (!$p['isinstance']($pyjs_dbg_137_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_137_err);}throw $pyjs_dbg_137_err;
}})();
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['serializeForPost'] = $method;
			$pyjs['track']['lineno']=212;
			$method = $pyjs__bind_method2('onClick', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '4a705ca8f8c26eb25df01d4ed8c671fc') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.text', 'lineno':212};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=212;
				$pyjs['track']['lineno']=213;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['utils']['doesEventHitWidgetOrChildren'](event, $p['getattr'](self, 'previewDiv'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_138_err){if (!$p['isinstance']($pyjs_dbg_138_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_138_err);}throw $pyjs_dbg_138_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_139_err){if (!$p['isinstance']($pyjs_dbg_139_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_139_err);}throw $pyjs_dbg_139_err;
}})()) {
					$pyjs['track']['lineno']=214;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return event['stopPropagation']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_140_err){if (!$p['isinstance']($pyjs_dbg_140_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_140_err);}throw $pyjs_dbg_140_err;
}})();
					$pyjs['track']['lineno']=215;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return event['preventDefault']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_141_err){if (!$p['isinstance']($pyjs_dbg_141_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_141_err);}throw $pyjs_dbg_141_err;
}})();
					$pyjs['track']['lineno']=216;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool']($p['getattr'](self, 'readOnly')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_142_err){if (!$p['isinstance']($pyjs_dbg_142_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_142_err);}throw $pyjs_dbg_142_err;
}})()) {
						$pyjs['track']['lineno']=217;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['openTxt']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_143_err){if (!$p['isinstance']($pyjs_dbg_143_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_143_err);}throw $pyjs_dbg_143_err;
}})();
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=219;
			$method = $pyjs__bind_method2('setExtendedErrorInformation', function(errorInfo) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					errorInfo = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '4a705ca8f8c26eb25df01d4ed8c671fc') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.text', 'lineno':219};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.text';
				$pyjs['track']['lineno']=219;
				$pyjs['track']['lineno']=220;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['errorInfo']]);
			$cls_definition['setExtendedErrorInformation'] = $method;
			$pyjs['track']['lineno']=63;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('TextEditBone', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=223;
		$m['CheckForTextBone'] = function(modulName, boneName, skelStucture) {
			if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, null, arguments['length']);
			var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

			var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
			if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
				if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
				kwargs = arguments[arguments['length']+1];
			} else {
				delete kwargs['$pyjs_is_kwarg'];
			}
			if (typeof kwargs == 'undefined') {
				kwargs = $p['__empty_dict']();
				if (typeof skelStucture != 'undefined') {
					if (skelStucture !== null && typeof skelStucture['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = skelStucture;
						skelStucture = arguments[3];
					}
				} else 				if (typeof boneName != 'undefined') {
					if (boneName !== null && typeof boneName['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = boneName;
						boneName = arguments[3];
					}
				} else 				if (typeof modulName != 'undefined') {
					if (modulName !== null && typeof modulName['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = modulName;
						modulName = arguments[3];
					}
				} else {
				}
			}

			$pyjs['track']={'module':'bones.text','lineno':223};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='bones.text';
			$pyjs['track']['lineno']=223;
			$pyjs['track']['lineno']=224;
			$pyjs['track']['lineno']=224;
			var $pyjs__ret = $p['op_eq'](skelStucture['__getitem__'](boneName)['__getitem__']('type'), 'text');
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['CheckForTextBone']['__name__'] = 'CheckForTextBone';

		$m['CheckForTextBone']['__bind_type__'] = 0;
		$m['CheckForTextBone']['__args__'] = ['args',['kwargs'],['modulName'],['boneName'],['skelStucture']];
		$pyjs['track']['lineno']=227;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['editBoneSelector']['insert']($constant_int_3, $m['CheckForTextBone'], $m['TextEditBone']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_144_err){if (!$p['isinstance']($pyjs_dbg_144_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_144_err);}throw $pyjs_dbg_144_err;
}})();
		$pyjs['track']['lineno']=228;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['viewDelegateSelector']['insert']($constant_int_3, $m['CheckForTextBone'], $m['TextViewBoneDelegate']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_145_err){if (!$p['isinstance']($pyjs_dbg_145_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_145_err);}throw $pyjs_dbg_145_err;
}})();
		$pyjs['track']['lineno']=229;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['extractorDelegateSelector']['insert']($constant_int_3, $m['CheckForTextBone'], $m['TextBoneExtractor']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_146_err){if (!$p['isinstance']($pyjs_dbg_146_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_146_err);}throw $pyjs_dbg_146_err;
}})();
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end bones.text */


/* end module: bones.text */


/*
PYJS_DEPS: ['html5', 'priorityqueue.editBoneSelector', 'priorityqueue', 'priorityqueue.viewDelegateSelector', 'priorityqueue.extractorDelegateSelector', 'config.conf', 'config', 'widgets.wysiwyg.Wysiwyg', 'widgets', 'widgets.wysiwyg', 'utils', 'i18n.translate', 'i18n']
*/
