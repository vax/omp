/* start module: bones.relational */
$pyjs['loaded_modules']['bones.relational'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['bones.relational']['__was_initialized__']) return $pyjs['loaded_modules']['bones.relational'];
	if(typeof $pyjs['loaded_modules']['bones'] == 'undefined' || !$pyjs['loaded_modules']['bones']['__was_initialized__']) $p['___import___']('bones', null);
	var $m = $pyjs['loaded_modules']['bones.relational'];
	$m['__repr__'] = function() { return '<module: bones.relational>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'bones.relational';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	$pyjs['loaded_modules']['bones']['relational'] = $pyjs['loaded_modules']['bones.relational'];
	try {
		$m.__track_lines__[1] = 'bones.relational.py, line 1:\n    #!/usr/bin/env python3';
		$m.__track_lines__[4] = 'bones.relational.py, line 4:\n    import os.path';
		$m.__track_lines__[5] = 'bones.relational.py, line 5:\n    import re';
		$m.__track_lines__[6] = 'bones.relational.py, line 6:\n    import html5';
		$m.__track_lines__[7] = 'bones.relational.py, line 7:\n    from priorityqueue import editBoneSelector, viewDelegateSelector, extendedSearchWidgetSelector, extractorDelegateSelector';
		$m.__track_lines__[8] = 'bones.relational.py, line 8:\n    from event import EventDispatcher';
		$m.__track_lines__[9] = 'bones.relational.py, line 9:\n    from utils import formatString';
		$m.__track_lines__[10] = 'bones.relational.py, line 10:\n    from widgets.list import ListWidget';
		$m.__track_lines__[11] = 'bones.relational.py, line 11:\n    from config import conf';
		$m.__track_lines__[12] = 'bones.relational.py, line 12:\n    from i18n import translate';
		$m.__track_lines__[13] = 'bones.relational.py, line 13:\n    from network import NetworkService';
		$m.__track_lines__[14] = 'bones.relational.py, line 14:\n    from widgets.edit import EditWidget';
		$m.__track_lines__[15] = 'bones.relational.py, line 15:\n    from pane import Pane';
		$m.__track_lines__[17] = 'bones.relational.py, line 17:\n    class RelationalBoneExtractor(object):';
		$m.__track_lines__[18] = 'bones.relational.py, line 18:\n    def __init__(self, modul, boneName, structure):';
		$m.__track_lines__[19] = 'bones.relational.py, line 19:\n    super(RelationalBoneExtractor, self).__init__()';
		$m.__track_lines__[20] = 'bones.relational.py, line 20:\n    self.format = "$(name)"';
		$m.__track_lines__[21] = 'bones.relational.py, line 21:\n    if "format" in structure[boneName].keys():';
		$m.__track_lines__[22] = 'bones.relational.py, line 22:\n    self.format = structure[boneName]["format"]';
		$m.__track_lines__[23] = 'bones.relational.py, line 23:\n    try:';
		$m.__track_lines__[24] = 'bones.relational.py, line 24:\n    self.format = re.match("\\$\\((.*?)\\)", self.format).group(1)';
		$m.__track_lines__[26] = 'bones.relational.py, line 26:\n    pass';
		$m.__track_lines__[28] = 'bones.relational.py, line 28:\n    try:';
		$m.__track_lines__[29] = 'bones.relational.py, line 29:\n    self.format, blah = os.path.splitext(self.format)';
		$m.__track_lines__[31] = 'bones.relational.py, line 31:\n    pass';
		$m.__track_lines__[33] = 'bones.relational.py, line 33:\n    self.modul = modul';
		$m.__track_lines__[34] = 'bones.relational.py, line 34:\n    self.structure = structure';
		$m.__track_lines__[35] = 'bones.relational.py, line 35:\n    self.boneName = boneName';
		$m.__track_lines__[37] = 'bones.relational.py, line 37:\n    def render(self, data, field ):';
		$m.__track_lines__[38] = 'bones.relational.py, line 38:\n    def localizedRender(val, format):';
		$m.__track_lines__[39] = 'bones.relational.py, line 39:\n    if "currentlanguage" in conf:';
		$m.__track_lines__[40] = 'bones.relational.py, line 40:\n    i18n_val = "%s.%s" % (self.format, conf["currentlanguage"])';
		$m.__track_lines__[42] = 'bones.relational.py, line 42:\n    if i18n_val in val:';
		$m.__track_lines__[43] = 'bones.relational.py, line 43:\n    val = html5.utils.unescape(val[i18n_val])';
		$m.__track_lines__[45] = 'bones.relational.py, line 45:\n    val = html5.utils.unescape(str(val[format]))';
		$m.__track_lines__[47] = 'bones.relational.py, line 47:\n    val = val["id"]';
		$m.__track_lines__[50] = 'bones.relational.py, line 50:\n    val = val["id"]';
		$m.__track_lines__[52] = 'bones.relational.py, line 52:\n    return val';
		$m.__track_lines__[54] = 'bones.relational.py, line 54:\n    assert field == self.boneName, "render() was called with field %s, expected %s" % (field,self.boneName)';
		$m.__track_lines__[55] = 'bones.relational.py, line 55:\n    if field in data.keys():';
		$m.__track_lines__[56] = 'bones.relational.py, line 56:\n    val = data[field]';
		$m.__track_lines__[58] = 'bones.relational.py, line 58:\n    val = ""';
		$m.__track_lines__[60] = 'bones.relational.py, line 60:\n    if isinstance(val, list):';
		$m.__track_lines__[61] = 'bones.relational.py, line 61:\n    result = list()';
		$m.__track_lines__[62] = 'bones.relational.py, line 62:\n    for x in val:';
		$m.__track_lines__[63] = 'bones.relational.py, line 63:\n    result.append(localizedRender(x, self.format))';
		$m.__track_lines__[65] = 'bones.relational.py, line 65:\n    return ", ".join(result)';
		$m.__track_lines__[68] = 'bones.relational.py, line 68:\n    val = localizedRender(val, self.format)';
		$m.__track_lines__[71] = 'bones.relational.py, line 71:\n    print("warning type:", val, type(val))';
		$m.__track_lines__[73] = 'bones.relational.py, line 73:\n    return html5.utils.unescape(val)';
		$m.__track_lines__[76] = 'bones.relational.py, line 76:\n    class RelationalViewBoneDelegate( object ):';
		$m.__track_lines__[77] = 'bones.relational.py, line 77:\n    cantSort = True';
		$m.__track_lines__[78] = 'bones.relational.py, line 78:\n    def __init__(self, modul, boneName, structure):';
		$m.__track_lines__[79] = 'bones.relational.py, line 79:\n    super(RelationalViewBoneDelegate, self).__init__()';
		$m.__track_lines__[80] = 'bones.relational.py, line 80:\n    self.format = "$(name)"';
		$m.__track_lines__[81] = 'bones.relational.py, line 81:\n    if "format" in structure[boneName].keys():';
		$m.__track_lines__[82] = 'bones.relational.py, line 82:\n    self.format = structure[boneName]["format"]';
		$m.__track_lines__[83] = 'bones.relational.py, line 83:\n    self.modul = modul';
		$m.__track_lines__[84] = 'bones.relational.py, line 84:\n    self.structure = structure';
		$m.__track_lines__[85] = 'bones.relational.py, line 85:\n    self.boneName = boneName';
		$m.__track_lines__[87] = 'bones.relational.py, line 87:\n    def render(self, data, field ):';
		$m.__track_lines__[88] = 'bones.relational.py, line 88:\n    assert field == self.boneName, "render() was called with field %s, expected %s" % (field,self.boneName)';
		$m.__track_lines__[90] = 'bones.relational.py, line 90:\n    if field in data.keys():';
		$m.__track_lines__[91] = 'bones.relational.py, line 91:\n    val = data[field]';
		$m.__track_lines__[93] = 'bones.relational.py, line 93:\n    val = ""';
		$m.__track_lines__[95] = 'bones.relational.py, line 95:\n    if isinstance(val,list):';
		$m.__track_lines__[96] = 'bones.relational.py, line 96:\n    if len(val)<5:';
		$m.__track_lines__[97] = 'bones.relational.py, line 97:\n    res = ", ".join( [ (formatString(self.format, self.structure, x, unescape= True) or x["id"]) for x in val] )';
		$m.__track_lines__[99] = 'bones.relational.py, line 99:\n    res = ", ".join( [ (formatString(self.format, self.structure, x, unescape= True) or x["id"]) for x in val[:4]] )';
		$m.__track_lines__[100] = 'bones.relational.py, line 100:\n    res += " "+translate("and {count} more",count=len(val)-4)';
		$m.__track_lines__[103] = 'bones.relational.py, line 103:\n    res = formatString(self.format,self.structure, val, unescape= True) or val["id"]';
		$m.__track_lines__[105] = 'bones.relational.py, line 105:\n    return( html5.Label( res ) )';
		$m.__track_lines__[109] = 'bones.relational.py, line 109:\n    class RelationalSingleSelectionBone( html5.Div ):';
		$m.__track_lines__[114] = 'bones.relational.py, line 114:\n    def __init__(self, srcModul, boneName, readOnly, destModul, format="$(name)", required=False, *args, **kwargs ):';
		$m.__track_lines__[127] = 'bones.relational.py, line 127:\n    super( RelationalSingleSelectionBone,  self ).__init__( *args, **kwargs )';
		$m.__track_lines__[128] = 'bones.relational.py, line 128:\n    self.srcModul = srcModul';
		$m.__track_lines__[129] = 'bones.relational.py, line 129:\n    self.boneName = boneName';
		$m.__track_lines__[130] = 'bones.relational.py, line 130:\n    self.readOnly = readOnly';
		$m.__track_lines__[131] = 'bones.relational.py, line 131:\n    self.destModul = destModul';
		$m.__track_lines__[132] = 'bones.relational.py, line 132:\n    self.format = format';
		$m.__track_lines__[133] = 'bones.relational.py, line 133:\n    self.selection = None';
		$m.__track_lines__[134] = 'bones.relational.py, line 134:\n    self.selectionTxt = html5.Input()';
		$m.__track_lines__[135] = 'bones.relational.py, line 135:\n    self.selectionTxt["type"] = "text"';
		$m.__track_lines__[136] = 'bones.relational.py, line 136:\n    self.selectionTxt["readonly"] = True';
		$m.__track_lines__[137] = 'bones.relational.py, line 137:\n    self.appendChild( self.selectionTxt )';
		$m.__track_lines__[142] = 'bones.relational.py, line 142:\n    if ( "root" in conf[ "currentUser" ][ "access" ]';
		$m.__track_lines__[144] = 'bones.relational.py, line 144:\n    self.selectBtn = html5.ext.Button(translate("Select"), self.onShowSelector)';
		$m.__track_lines__[145] = 'bones.relational.py, line 145:\n    self.selectBtn["class"].append("icon")';
		$m.__track_lines__[146] = 'bones.relational.py, line 146:\n    self.selectBtn["class"].append("select")';
		$m.__track_lines__[147] = 'bones.relational.py, line 147:\n    self.appendChild( self.selectBtn )';
		$m.__track_lines__[149] = 'bones.relational.py, line 149:\n    self.selectBtn = None';
		$m.__track_lines__[152] = 'bones.relational.py, line 152:\n    if ( "root" in conf[ "currentUser" ][ "access" ]';
		$m.__track_lines__[154] = 'bones.relational.py, line 154:\n    self.editBtn = html5.ext.Button(translate("Edit"), self.onEdit )';
		$m.__track_lines__[155] = 'bones.relational.py, line 155:\n    self.editBtn["class"].append("icon")';
		$m.__track_lines__[156] = 'bones.relational.py, line 156:\n    self.editBtn["class"].append("edit")';
		$m.__track_lines__[157] = 'bones.relational.py, line 157:\n    self.appendChild( self.editBtn )';
		$m.__track_lines__[159] = 'bones.relational.py, line 159:\n    self.editBtn = None';
		$m.__track_lines__[162] = 'bones.relational.py, line 162:\n    if ( not required and not readOnly';
		$m.__track_lines__[168] = 'bones.relational.py, line 168:\n    self.remBtn = html5.ext.Button(translate("Remove"), self.onRemove )';
		$m.__track_lines__[169] = 'bones.relational.py, line 169:\n    self.remBtn["class"].append("icon")';
		$m.__track_lines__[170] = 'bones.relational.py, line 170:\n    self.remBtn["class"].append("cancel")';
		$m.__track_lines__[171] = 'bones.relational.py, line 171:\n    self.appendChild( self.remBtn )';
		$m.__track_lines__[173] = 'bones.relational.py, line 173:\n    self.remBtn = None';
		$m.__track_lines__[175] = 'bones.relational.py, line 175:\n    if self.readOnly:';
		$m.__track_lines__[176] = 'bones.relational.py, line 176:\n    self["disabled"] = True';
		$m.__track_lines__[180] = 'bones.relational.py, line 180:\n    def _setDisabled(self, disable):';
		$m.__track_lines__[184] = 'bones.relational.py, line 184:\n    super(RelationalSingleSelectionBone, self)._setDisabled( disable )';
		$m.__track_lines__[185] = 'bones.relational.py, line 185:\n    if not disable and not self._disabledState and "is_active" in self.parent()["class"]:';
		$m.__track_lines__[186] = 'bones.relational.py, line 186:\n    self.parent()["class"].remove("is_active")';
		$m.__track_lines__[189] = 'bones.relational.py, line 188:\n    @classmethod ... def fromSkelStructure( cls, modulName, boneName, skelStructure ):';
		$m.__track_lines__[199] = 'bones.relational.py, line 199:\n    readOnly = "readonly" in skelStructure[ boneName ].keys() and skelStructure[ boneName ]["readonly"]';
		$m.__track_lines__[200] = 'bones.relational.py, line 200:\n    multiple = skelStructure[boneName]["multiple"]';
		$m.__track_lines__[201] = 'bones.relational.py, line 201:\n    if "required" in skelStructure[boneName].keys() and skelStructure[boneName]["required"]:';
		$m.__track_lines__[202] = 'bones.relational.py, line 202:\n    required=True';
		$m.__track_lines__[204] = 'bones.relational.py, line 204:\n    required=False';
		$m.__track_lines__[205] = 'bones.relational.py, line 205:\n    if "modul" in skelStructure[ boneName ].keys():';
		$m.__track_lines__[206] = 'bones.relational.py, line 206:\n    destModul = skelStructure[ boneName ][ "modul" ]';
		$m.__track_lines__[208] = 'bones.relational.py, line 208:\n    destModul = skelStructure[ boneName ]["type"].split(".")[1]';
		$m.__track_lines__[209] = 'bones.relational.py, line 209:\n    format= "$(name)"';
		$m.__track_lines__[210] = 'bones.relational.py, line 210:\n    if "format" in skelStructure[ boneName ].keys():';
		$m.__track_lines__[211] = 'bones.relational.py, line 211:\n    format = skelStructure[ boneName ]["format"]';
		$m.__track_lines__[212] = 'bones.relational.py, line 212:\n    return( cls( modulName, boneName, readOnly, destModul=destModul, format=format, required=required ) )';
		$m.__track_lines__[214] = 'bones.relational.py, line 214:\n    def onEdit(self, *args, **kwargs):';
		$m.__track_lines__[218] = 'bones.relational.py, line 218:\n    if not self.selection:';
		$m.__track_lines__[219] = 'bones.relational.py, line 219:\n    return';
		$m.__track_lines__[221] = 'bones.relational.py, line 221:\n    pane = Pane( translate("Edit"), closeable=True, iconClasses=[ "modul_%s" % self.destModul,';
		$m.__track_lines__[223] = 'bones.relational.py, line 223:\n    conf["mainWindow"].stackPane( pane, focus=True )';
		$m.__track_lines__[224] = 'bones.relational.py, line 224:\n    edwg = EditWidget( self.destModul, EditWidget.appList, key=self.selection[ "id" ] )';
		$m.__track_lines__[225] = 'bones.relational.py, line 225:\n    pane.addWidget( edwg )';
		$m.__track_lines__[227] = 'bones.relational.py, line 227:\n    def onRemove(self, *args, **kwargs):';
		$m.__track_lines__[228] = 'bones.relational.py, line 228:\n    self.setSelection( None )';
		$m.__track_lines__[230] = 'bones.relational.py, line 230:\n    def unserialize(self, data):';
		$m.__track_lines__[236] = 'bones.relational.py, line 236:\n    if self.boneName in data.keys():';
		$m.__track_lines__[237] = 'bones.relational.py, line 237:\n    val = data[ self.boneName ]';
		$m.__track_lines__[238] = 'bones.relational.py, line 238:\n    if isinstance( val, list ):';
		$m.__track_lines__[239] = 'bones.relational.py, line 239:\n    if len(val)>0:';
		$m.__track_lines__[240] = 'bones.relational.py, line 240:\n    val = val[0]';
		$m.__track_lines__[242] = 'bones.relational.py, line 242:\n    val = None';
		$m.__track_lines__[243] = 'bones.relational.py, line 243:\n    if isinstance( val, dict ):';
		$m.__track_lines__[244] = 'bones.relational.py, line 244:\n    self.setSelection( val )';
		$m.__track_lines__[246] = 'bones.relational.py, line 246:\n    self.setSelection( None )';
		$m.__track_lines__[250] = 'bones.relational.py, line 250:\n    def serializeForPost(self):';
		$m.__track_lines__[255] = 'bones.relational.py, line 255:\n    return( { self.boneName: self.selection["id"] if self.selection is not None else "" } )';
		$m.__track_lines__[257] = 'bones.relational.py, line 257:\n    def serializeForDocument(self):';
		$m.__track_lines__[258] = 'bones.relational.py, line 258:\n    return( self.serialize( ) )';
		$m.__track_lines__[260] = 'bones.relational.py, line 260:\n    def onShowSelector(self, *args, **kwargs):';
		$m.__track_lines__[264] = 'bones.relational.py, line 264:\n    currentSelector = ListWidget( self.destModul, isSelector=True )';
		$m.__track_lines__[265] = 'bones.relational.py, line 265:\n    currentSelector.selectionActivatedEvent.register( self )';
		$m.__track_lines__[266] = 'bones.relational.py, line 266:\n    conf["mainWindow"].stackWidget( currentSelector )';
		$m.__track_lines__[267] = 'bones.relational.py, line 267:\n    self.parent()["class"].append("is_active")';
		$m.__track_lines__[269] = 'bones.relational.py, line 269:\n    def onSelectionActivated(self, table, selection ):';
		$m.__track_lines__[273] = 'bones.relational.py, line 273:\n    if selection:';
		$m.__track_lines__[274] = 'bones.relational.py, line 274:\n    self.setSelection( selection[0] )';
		$m.__track_lines__[276] = 'bones.relational.py, line 276:\n    self.setSelection( None )';
		$m.__track_lines__[278] = 'bones.relational.py, line 278:\n    def setSelection(self, selection):';
		$m.__track_lines__[284] = 'bones.relational.py, line 284:\n    self.selection = selection';
		$m.__track_lines__[285] = 'bones.relational.py, line 285:\n    if selection:';
		$m.__track_lines__[286] = 'bones.relational.py, line 286:\n    NetworkService.request( self.destModul, "view/"+selection["id"],';
		$m.__track_lines__[288] = 'bones.relational.py, line 288:\n    self.selectionTxt["value"] = translate("Loading...")';
		$m.__track_lines__[290] = 'bones.relational.py, line 290:\n    self.selectionTxt["value"] = ""';
		$m.__track_lines__[292] = 'bones.relational.py, line 292:\n    self.updateButtons()';
		$m.__track_lines__[294] = 'bones.relational.py, line 294:\n    def updateButtons(self):';
		$m.__track_lines__[298] = 'bones.relational.py, line 298:\n    if self.selection:';
		$m.__track_lines__[299] = 'bones.relational.py, line 299:\n    if self.editBtn:';
		$m.__track_lines__[300] = 'bones.relational.py, line 300:\n    self.editBtn[ "disabled" ] = False';
		$m.__track_lines__[301] = 'bones.relational.py, line 301:\n    if self.remBtn:';
		$m.__track_lines__[302] = 'bones.relational.py, line 302:\n    self.remBtn[ "disabled"] = False';
		$m.__track_lines__[304] = 'bones.relational.py, line 304:\n    if self.editBtn:';
		$m.__track_lines__[305] = 'bones.relational.py, line 305:\n    self.editBtn[ "disabled" ] = True';
		$m.__track_lines__[306] = 'bones.relational.py, line 306:\n    if self.remBtn:';
		$m.__track_lines__[307] = 'bones.relational.py, line 307:\n    self.remBtn[ "disabled"] = True';
		$m.__track_lines__[309] = 'bones.relational.py, line 309:\n    def onAttach(self):';
		$m.__track_lines__[310] = 'bones.relational.py, line 310:\n    super( RelationalSingleSelectionBone,  self ).onAttach()';
		$m.__track_lines__[311] = 'bones.relational.py, line 311:\n    NetworkService.registerChangeListener( self )';
		$m.__track_lines__[313] = 'bones.relational.py, line 313:\n    def onDetach(self):';
		$m.__track_lines__[314] = 'bones.relational.py, line 314:\n    NetworkService.removeChangeListener( self )';
		$m.__track_lines__[315] = 'bones.relational.py, line 315:\n    super( RelationalSingleSelectionBone,  self ).onDetach()';
		$m.__track_lines__[317] = 'bones.relational.py, line 317:\n    def onDataChanged(self, modul):';
		$m.__track_lines__[318] = 'bones.relational.py, line 318:\n    if modul == self.destModul:';
		$m.__track_lines__[319] = 'bones.relational.py, line 319:\n    self.setSelection(self.selection)';
		$m.__track_lines__[321] = 'bones.relational.py, line 321:\n    def onSelectionDataAviable(self, req):';
		$m.__track_lines__[325] = 'bones.relational.py, line 325:\n    data = NetworkService.decode( req )';
		$m.__track_lines__[326] = 'bones.relational.py, line 326:\n    assert self.selection["id"]==data["values"]["id"]';
		$m.__track_lines__[327] = 'bones.relational.py, line 327:\n    self.selectionTxt["value"] = formatString( self.format ,data["structure"],data["values"] )';
		$m.__track_lines__[329] = 'bones.relational.py, line 329:\n    class RelationalMultiSelectionBoneEntry( html5.Div ):';
		$m.__track_lines__[335] = 'bones.relational.py, line 335:\n    def __init__(self, parent, modul, data, format="$(name)", *args, **kwargs ):';
		$m.__track_lines__[344] = 'bones.relational.py, line 344:\n    super( RelationalMultiSelectionBoneEntry, self ).__init__( *args, **kwargs )';
		$m.__track_lines__[345] = 'bones.relational.py, line 345:\n    self.parent = parent';
		$m.__track_lines__[346] = 'bones.relational.py, line 346:\n    self.modul = modul';
		$m.__track_lines__[347] = 'bones.relational.py, line 347:\n    self.data = data';
		$m.__track_lines__[348] = 'bones.relational.py, line 348:\n    self.format = format';
		$m.__track_lines__[349] = 'bones.relational.py, line 349:\n    self.selectionTxt = html5.Span(  )';
		$m.__track_lines__[350] = 'bones.relational.py, line 350:\n    self.selectionTxt.appendChild( html5.TextNode(translate("Loading...")) )';
		$m.__track_lines__[351] = 'bones.relational.py, line 351:\n    self.selectionTxt["class"].append("entrydescription")';
		$m.__track_lines__[352] = 'bones.relational.py, line 352:\n    self.appendChild( self.selectionTxt )';
		$m.__track_lines__[355] = 'bones.relational.py, line 355:\n    if ( "root" in conf[ "currentUser" ][ "access" ]';
		$m.__track_lines__[357] = 'bones.relational.py, line 357:\n    editBtn = html5.ext.Button("Edit", self.onEdit )';
		$m.__track_lines__[358] = 'bones.relational.py, line 358:\n    editBtn["class"].append("icon")';
		$m.__track_lines__[359] = 'bones.relational.py, line 359:\n    editBtn["class"].append("edit")';
		$m.__track_lines__[360] = 'bones.relational.py, line 360:\n    self.appendChild( editBtn )';
		$m.__track_lines__[363] = 'bones.relational.py, line 363:\n    if (not parent.readOnly';
		$m.__track_lines__[368] = 'bones.relational.py, line 368:\n    remBtn = html5.ext.Button("Remove", self.onRemove )';
		$m.__track_lines__[369] = 'bones.relational.py, line 369:\n    remBtn["class"].append("icon")';
		$m.__track_lines__[370] = 'bones.relational.py, line 370:\n    remBtn["class"].append("cancel")';
		$m.__track_lines__[371] = 'bones.relational.py, line 371:\n    self.appendChild( remBtn )';
		$m.__track_lines__[373] = 'bones.relational.py, line 373:\n    self.fetchEntry( self.data["id"] )';
		$m.__track_lines__[375] = 'bones.relational.py, line 375:\n    def fetchEntry(self, id):';
		$m.__track_lines__[376] = 'bones.relational.py, line 376:\n    NetworkService.request(self.modul,"view/"+id, successHandler=self.onSelectionDataAviable, cacheable=True)';
		$m.__track_lines__[378] = 'bones.relational.py, line 378:\n    def onEdit(self, *args, **kwargs):';
		$m.__track_lines__[382] = 'bones.relational.py, line 382:\n    pane = Pane( translate("Edit"), closeable=True, iconClasses=[ "modul_%s" % self.parent.destModul,';
		$m.__track_lines__[384] = 'bones.relational.py, line 384:\n    conf["mainWindow"].stackPane( pane, focus=True )';
		$m.__track_lines__[385] = 'bones.relational.py, line 385:\n    edwg = EditWidget( self.parent.destModul, EditWidget.appList, key=self.data[ "id" ] )';
		$m.__track_lines__[386] = 'bones.relational.py, line 386:\n    pane.addWidget( edwg )';
		$m.__track_lines__[388] = 'bones.relational.py, line 388:\n    def onRemove(self, *args, **kwargs):';
		$m.__track_lines__[389] = 'bones.relational.py, line 389:\n    self.parent.removeEntry( self )';
		$m.__track_lines__[391] = 'bones.relational.py, line 391:\n    def onSelectionDataAviable(self, req):';
		$m.__track_lines__[395] = 'bones.relational.py, line 395:\n    data = NetworkService.decode( req )';
		$m.__track_lines__[396] = 'bones.relational.py, line 396:\n    assert self.data["id"]==data["values"]["id"]';
		$m.__track_lines__[397] = 'bones.relational.py, line 397:\n    for c in self.selectionTxt._children[:]:';
		$m.__track_lines__[398] = 'bones.relational.py, line 398:\n    self.selectionTxt.removeChild( c )';
		$m.__track_lines__[399] = 'bones.relational.py, line 399:\n    self.selectionTxt.appendChild( html5.TextNode( formatString( self.format ,data["structure"],data["values"] ) ) )';
		$m.__track_lines__[402] = 'bones.relational.py, line 402:\n    class RelationalMultiSelectionBone( html5.Div ):';
		$m.__track_lines__[407] = 'bones.relational.py, line 407:\n    def __init__(self, srcModul, boneName, readOnly, destModul, format="$(name)", *args, **kwargs ):';
		$m.__track_lines__[420] = 'bones.relational.py, line 420:\n    super( RelationalMultiSelectionBone,  self ).__init__( *args, **kwargs )';
		$m.__track_lines__[423] = 'bones.relational.py, line 423:\n    self.srcModul = srcModul';
		$m.__track_lines__[424] = 'bones.relational.py, line 424:\n    self.boneName = boneName';
		$m.__track_lines__[425] = 'bones.relational.py, line 425:\n    self.readOnly = readOnly';
		$m.__track_lines__[426] = 'bones.relational.py, line 426:\n    self.destModul = destModul';
		$m.__track_lines__[427] = 'bones.relational.py, line 427:\n    self.format = format';
		$m.__track_lines__[428] = 'bones.relational.py, line 428:\n    self.entries = []';
		$m.__track_lines__[429] = 'bones.relational.py, line 429:\n    self.selectionDiv = html5.Div()';
		$m.__track_lines__[430] = 'bones.relational.py, line 430:\n    self.selectionDiv["class"].append("selectioncontainer")';
		$m.__track_lines__[431] = 'bones.relational.py, line 431:\n    self.appendChild( self.selectionDiv )';
		$m.__track_lines__[433] = 'bones.relational.py, line 433:\n    if ( "root" in conf[ "currentUser" ][ "access" ]';
		$m.__track_lines__[435] = 'bones.relational.py, line 435:\n    self.selectBtn = html5.ext.Button(translate("Select"), self.onShowSelector)';
		$m.__track_lines__[436] = 'bones.relational.py, line 436:\n    self.selectBtn["class"].append("icon")';
		$m.__track_lines__[437] = 'bones.relational.py, line 437:\n    self.selectBtn["class"].append("select")';
		$m.__track_lines__[438] = 'bones.relational.py, line 438:\n    self.appendChild( self.selectBtn )';
		$m.__track_lines__[440] = 'bones.relational.py, line 440:\n    self.selectBtn = None';
		$m.__track_lines__[442] = 'bones.relational.py, line 442:\n    if self.readOnly:';
		$m.__track_lines__[443] = 'bones.relational.py, line 443:\n    self["disabled"] = True';
		$m.__track_lines__[445] = 'bones.relational.py, line 445:\n    def _setDisabled(self, disable):';
		$m.__track_lines__[449] = 'bones.relational.py, line 449:\n    super(RelationalMultiSelectionBone, self)._setDisabled( disable )';
		$m.__track_lines__[450] = 'bones.relational.py, line 450:\n    if not disable and not self._disabledState and "is_active" in self.parent()["class"]:';
		$m.__track_lines__[451] = 'bones.relational.py, line 451:\n    self.parent()["class"].remove("is_active")';
		$m.__track_lines__[455] = 'bones.relational.py, line 454:\n    @classmethod ... def fromSkelStructure( cls, modulName, boneName, skelStructure ):';
		$m.__track_lines__[465] = 'bones.relational.py, line 465:\n    readOnly = "readonly" in skelStructure[ boneName ].keys() and skelStructure[ boneName ]["readonly"]';
		$m.__track_lines__[466] = 'bones.relational.py, line 466:\n    multiple = skelStructure[boneName]["multiple"]';
		$m.__track_lines__[467] = 'bones.relational.py, line 467:\n    if "modul" in skelStructure[ boneName ].keys():';
		$m.__track_lines__[468] = 'bones.relational.py, line 468:\n    destModul = skelStructure[ boneName ][ "modul" ]';
		$m.__track_lines__[470] = 'bones.relational.py, line 470:\n    destModul = skelStructure[ boneName ]["type"].split(".")[1]';
		$m.__track_lines__[471] = 'bones.relational.py, line 471:\n    format= "$(name)"';
		$m.__track_lines__[472] = 'bones.relational.py, line 472:\n    if "format" in skelStructure[ boneName ].keys():';
		$m.__track_lines__[473] = 'bones.relational.py, line 473:\n    format = skelStructure[ boneName ]["format"]';
		$m.__track_lines__[474] = 'bones.relational.py, line 474:\n    return( cls( modulName, boneName, readOnly, destModul=destModul, format=format ) )';
		$m.__track_lines__[476] = 'bones.relational.py, line 476:\n    def unserialize(self, data):';
		$m.__track_lines__[482] = 'bones.relational.py, line 482:\n    if self.boneName in data.keys():';
		$m.__track_lines__[483] = 'bones.relational.py, line 483:\n    val = data[ self.boneName ]';
		$m.__track_lines__[484] = 'bones.relational.py, line 484:\n    if isinstance( val, dict ):';
		$m.__track_lines__[485] = 'bones.relational.py, line 485:\n    val = [ val ]';
		$m.__track_lines__[486] = 'bones.relational.py, line 486:\n    self.setSelection( val )';
		$m.__track_lines__[490] = 'bones.relational.py, line 490:\n    def serializeForPost(self):';
		$m.__track_lines__[495] = 'bones.relational.py, line 495:\n    return( { self.boneName: [x.data["id"] for x in self.entries]} )';
		$m.__track_lines__[497] = 'bones.relational.py, line 497:\n    def serializeForDocument(self):';
		$m.__track_lines__[498] = 'bones.relational.py, line 498:\n    return( self.serialize( ) )';
		$m.__track_lines__[500] = 'bones.relational.py, line 500:\n    def onShowSelector(self, *args, **kwargs):';
		$m.__track_lines__[504] = 'bones.relational.py, line 504:\n    currentSelector = ListWidget( self.destModul, isSelector=True )';
		$m.__track_lines__[505] = 'bones.relational.py, line 505:\n    currentSelector.selectionActivatedEvent.register( self )';
		$m.__track_lines__[506] = 'bones.relational.py, line 506:\n    conf["mainWindow"].stackWidget( currentSelector )';
		$m.__track_lines__[507] = 'bones.relational.py, line 507:\n    self.parent()["class"].append("is_active")';
		$m.__track_lines__[509] = 'bones.relational.py, line 509:\n    def onSelectionActivated(self, table, selection ):';
		$m.__track_lines__[513] = 'bones.relational.py, line 513:\n    self.setSelection( selection )';
		$m.__track_lines__[515] = 'bones.relational.py, line 515:\n    def setSelection(self, selection):';
		$m.__track_lines__[521] = 'bones.relational.py, line 521:\n    print( selection )';
		$m.__track_lines__[522] = 'bones.relational.py, line 522:\n    if selection is None:';
		$m.__track_lines__[523] = 'bones.relational.py, line 523:\n    return';
		$m.__track_lines__[524] = 'bones.relational.py, line 524:\n    for data in selection:';
		$m.__track_lines__[525] = 'bones.relational.py, line 525:\n    entry = RelationalMultiSelectionBoneEntry( self, self.destModul, data, self.format)';
		$m.__track_lines__[526] = 'bones.relational.py, line 526:\n    self.addEntry( entry )';
		$m.__track_lines__[528] = 'bones.relational.py, line 528:\n    def addEntry(self, entry):';
		$m.__track_lines__[533] = 'bones.relational.py, line 533:\n    self.entries.append( entry )';
		$m.__track_lines__[534] = 'bones.relational.py, line 534:\n    self.selectionDiv.appendChild( entry )';
		$m.__track_lines__[536] = 'bones.relational.py, line 536:\n    def removeEntry(self, entry ):';
		$m.__track_lines__[541] = 'bones.relational.py, line 541:\n    assert entry in self.entries, "Cannot remove unknown entry %s from realtionalBone" % str(entry)';
		$m.__track_lines__[542] = 'bones.relational.py, line 542:\n    self.selectionDiv.removeChild( entry )';
		$m.__track_lines__[543] = 'bones.relational.py, line 543:\n    self.entries.remove( entry )';
		$m.__track_lines__[545] = 'bones.relational.py, line 545:\n    def onAttach(self):';
		$m.__track_lines__[546] = 'bones.relational.py, line 546:\n    super( RelationalMultiSelectionBone,  self ).onAttach()';
		$m.__track_lines__[547] = 'bones.relational.py, line 547:\n    NetworkService.registerChangeListener( self )';
		$m.__track_lines__[549] = 'bones.relational.py, line 549:\n    def onDetach(self):';
		$m.__track_lines__[550] = 'bones.relational.py, line 550:\n    NetworkService.removeChangeListener( self )';
		$m.__track_lines__[551] = 'bones.relational.py, line 551:\n    super( RelationalMultiSelectionBone,  self ).onDetach()';
		$m.__track_lines__[553] = 'bones.relational.py, line 553:\n    def onDataChanged(self, modul):';
		$m.__track_lines__[554] = 'bones.relational.py, line 554:\n    if modul == self.destModul:';
		$m.__track_lines__[555] = 'bones.relational.py, line 555:\n    selection = [ x.data for x in self.entries ]';
		$m.__track_lines__[556] = 'bones.relational.py, line 556:\n    for e in self.entries[:]:';
		$m.__track_lines__[557] = 'bones.relational.py, line 557:\n    self.removeEntry( e )';
		$m.__track_lines__[558] = 'bones.relational.py, line 558:\n    self.setSelection( selection )';
		$m.__track_lines__[560] = 'bones.relational.py, line 560:\n    class ExtendedRelationalSearch( html5.Div ):';
		$m.__track_lines__[561] = 'bones.relational.py, line 561:\n    def __init__(self, extension, view, modul, *args, **kwargs ):';
		$m.__track_lines__[562] = 'bones.relational.py, line 562:\n    super( ExtendedRelationalSearch, self ).__init__( *args, **kwargs )';
		$m.__track_lines__[563] = 'bones.relational.py, line 563:\n    self.view = view';
		$m.__track_lines__[564] = 'bones.relational.py, line 564:\n    self.extension = extension';
		$m.__track_lines__[565] = 'bones.relational.py, line 565:\n    self.modul = modul';
		$m.__track_lines__[566] = 'bones.relational.py, line 566:\n    self.currentSelection = None';
		$m.__track_lines__[567] = 'bones.relational.py, line 567:\n    self.filterChangedEvent = EventDispatcher("filterChanged")';
		$m.__track_lines__[568] = 'bones.relational.py, line 568:\n    tmpSpan = html5.Span()';
		$m.__track_lines__[569] = 'bones.relational.py, line 569:\n    tmpSpan.appendChild( html5.TextNode(extension["name"]))';
		$m.__track_lines__[570] = 'bones.relational.py, line 570:\n    self.appendChild(tmpSpan)';
		$m.__track_lines__[571] = 'bones.relational.py, line 571:\n    self.currentEntry = html5.Span()';
		$m.__track_lines__[573] = 'bones.relational.py, line 573:\n    btn = html5.ext.Button(translate("Select"), self.openSelector)';
		$m.__track_lines__[574] = 'bones.relational.py, line 574:\n    btn["class"].append("icon select")';
		$m.__track_lines__[575] = 'bones.relational.py, line 575:\n    self.appendChild( btn )';
		$m.__track_lines__[576] = 'bones.relational.py, line 576:\n    btn = html5.ext.Button(translate("Clear"), self.clearSelection)';
		$m.__track_lines__[577] = 'bones.relational.py, line 577:\n    btn["class"].append("icon cancel")';
		$m.__track_lines__[578] = 'bones.relational.py, line 578:\n    self.appendChild( btn )';
		$m.__track_lines__[580] = 'bones.relational.py, line 580:\n    def clearSelection(self, *args, **kwargs):';
		$m.__track_lines__[581] = 'bones.relational.py, line 581:\n    self.currentSelection = None';
		$m.__track_lines__[582] = 'bones.relational.py, line 582:\n    self.filterChangedEvent.fire()';
		$m.__track_lines__[584] = 'bones.relational.py, line 584:\n    def openSelector(self, *args, **kwargs):';
		$m.__track_lines__[585] = 'bones.relational.py, line 585:\n    currentSelector = ListWidget( self.extension["modul"], isSelector=True )';
		$m.__track_lines__[586] = 'bones.relational.py, line 586:\n    currentSelector.selectionActivatedEvent.register( self )';
		$m.__track_lines__[587] = 'bones.relational.py, line 587:\n    conf["mainWindow"].stackWidget( currentSelector )';
		$m.__track_lines__[589] = 'bones.relational.py, line 589:\n    def onSelectionActivated(self, table,selection):';
		$m.__track_lines__[590] = 'bones.relational.py, line 590:\n    self.currentSelection = selection';
		$m.__track_lines__[591] = 'bones.relational.py, line 591:\n    self.filterChangedEvent.fire()';
		$m.__track_lines__[594] = 'bones.relational.py, line 594:\n    def updateFilter(self, filter):';
		$m.__track_lines__[595] = 'bones.relational.py, line 595:\n    if self.currentSelection:';
		$m.__track_lines__[596] = 'bones.relational.py, line 596:\n    self.currentEntry.element.innerHTML = self.currentSelection[0]["name"]';
		$m.__track_lines__[597] = 'bones.relational.py, line 597:\n    newId = self.currentSelection[0]["id"]';
		$m.__track_lines__[598] = 'bones.relational.py, line 598:\n    filter[ self.extension["target"]+".id" ] = newId';
		$m.__track_lines__[600] = 'bones.relational.py, line 600:\n    self.currentEntry.element.innerHTML = ""';
		$m.__track_lines__[601] = 'bones.relational.py, line 601:\n    return( filter )';
		$m.__track_lines__[604] = 'bones.relational.py, line 603:\n    @staticmethod ... def canHandleExtension( extension, view, modul ):';
		$m.__track_lines__[605] = 'bones.relational.py, line 605:\n    return( isinstance( extension, dict) and "type" in extension.keys() and (extension["type"]=="relational" or extension["type"].startswith("relational.") ) )';
		$m.__track_lines__[607] = 'bones.relational.py, line 607:\n    def CheckForRelationalBoneSingleSelection( modulName, boneName, skelStructure, *args, **kwargs ):';
		$m.__track_lines__[608] = 'bones.relational.py, line 608:\n    isMultiple = "multiple" in skelStructure[boneName].keys() and skelStructure[boneName]["multiple"]';
		$m.__track_lines__[609] = 'bones.relational.py, line 609:\n    return CheckForRelationalBone( modulName, boneName, skelStructure ) and not isMultiple';
		$m.__track_lines__[611] = 'bones.relational.py, line 611:\n    def CheckForRelationalBoneMultiSelection( modulName, boneName, skelStructure, *args, **kwargs ):';
		$m.__track_lines__[612] = 'bones.relational.py, line 612:\n    isMultiple = "multiple" in skelStructure[boneName].keys() and skelStructure[boneName]["multiple"]';
		$m.__track_lines__[613] = 'bones.relational.py, line 613:\n    return CheckForRelationalBone( modulName, boneName, skelStructure ) and isMultiple';
		$m.__track_lines__[615] = 'bones.relational.py, line 615:\n    def CheckForRelationalBone(  modulName, boneName, skelStucture, *args, **kwargs ):';
		$m.__track_lines__[616] = 'bones.relational.py, line 616:\n    return( skelStucture[boneName]["type"].startswith("relational.") )';
		$m.__track_lines__[619] = 'bones.relational.py, line 619:\n    editBoneSelector.insert( 3, CheckForRelationalBoneSingleSelection, RelationalSingleSelectionBone)';
		$m.__track_lines__[620] = 'bones.relational.py, line 620:\n    editBoneSelector.insert( 3, CheckForRelationalBoneMultiSelection, RelationalMultiSelectionBone)';
		$m.__track_lines__[621] = 'bones.relational.py, line 621:\n    viewDelegateSelector.insert( 3, CheckForRelationalBone, RelationalViewBoneDelegate)';
		$m.__track_lines__[622] = 'bones.relational.py, line 622:\n    extendedSearchWidgetSelector.insert( 1, ExtendedRelationalSearch.canHandleExtension, ExtendedRelationalSearch )';
		$m.__track_lines__[623] = 'bones.relational.py, line 623:\n    extractorDelegateSelector.insert(3, CheckForRelationalBone, RelationalBoneExtractor)';

		var $constant_int_0 = new $p['int'](0);
		var $constant_int_1 = new $p['int'](1);
		var $constant_int_3 = new $p['int'](3);
		var $constant_int_4 = new $p['int'](4);
		var $constant_int_5 = new $p['int'](5);
		$pyjs['track']['module']='bones.relational';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=4;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['os'] = $p['___import___']('os.path', 'bones');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=5;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['re'] = $p['___import___']('re', 'bones');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=6;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['html5'] = $p['___import___']('html5', 'bones');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=7;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['editBoneSelector'] = $p['___import___']('priorityqueue.editBoneSelector', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['viewDelegateSelector'] = $p['___import___']('priorityqueue.viewDelegateSelector', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['extendedSearchWidgetSelector'] = $p['___import___']('priorityqueue.extendedSearchWidgetSelector', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['extractorDelegateSelector'] = $p['___import___']('priorityqueue.extractorDelegateSelector', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=8;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['EventDispatcher'] = $p['___import___']('event.EventDispatcher', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=9;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['formatString'] = $p['___import___']('utils.formatString', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=10;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['ListWidget'] = $p['___import___']('widgets.list.ListWidget', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=11;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['conf'] = $p['___import___']('config.conf', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=12;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['translate'] = $p['___import___']('i18n.translate', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=13;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['NetworkService'] = $p['___import___']('network.NetworkService', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=14;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['EditWidget'] = $p['___import___']('widgets.edit.EditWidget', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=15;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['Pane'] = $p['___import___']('pane.Pane', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=17;
		$m['RelationalBoneExtractor'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.relational';
			$cls_definition['__md5__'] = '5a86e2b984d743be0e22e96270f69554';
			$pyjs['track']['lineno']=18;
			$method = $pyjs__bind_method2('__init__', function(modul, boneName, structure) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 4, arguments['length']+1);
				} else {
					var self = arguments[0];
					modul = arguments[1];
					boneName = arguments[2];
					structure = arguments[3];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 4, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '5a86e2b984d743be0e22e96270f69554') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var blah,$pyjs_try_err;
				$pyjs['track']={'module':'bones.relational', 'lineno':18};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=18;
				$pyjs['track']['lineno']=19;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['RelationalBoneExtractor'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})()['__init__']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})();
				$pyjs['track']['lineno']=20;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('format', '$(name)') : $p['setattr'](self, 'format', '$(name)'); 
				$pyjs['track']['lineno']=21;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return structure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})()['__contains__']('format'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})()) {
					$pyjs['track']['lineno']=22;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('format', structure['__getitem__'](boneName)['__getitem__']('format')) : $p['setattr'](self, 'format', structure['__getitem__'](boneName)['__getitem__']('format')); 
				}
				$pyjs['track']['lineno']=23;
				var $pyjs__trackstack_size_1 = $pyjs['trackstack']['length'];
				try {
					try {
						$pyjs['in_try_except'] += 1;
						$pyjs['track']['lineno']=24;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('format', (function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['re']['match']('\\$\\((.*?)\\)', $p['getattr'](self, 'format'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})()['group']($constant_int_1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})()) : $p['setattr'](self, 'format', (function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['re']['match']('\\$\\((.*?)\\)', $p['getattr'](self, 'format'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})()['group']($constant_int_1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})()); 
					} finally { $pyjs['in_try_except'] -= 1; }
				} catch($pyjs_try_err) {
					$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_1 - 1);
					$pyjs['__active_exception_stack__'] = null;
					$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
					var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
					$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.relational';
					if (true) {
						$pyjs['track']['lineno']=26;
					}
				}
				$pyjs['track']['lineno']=28;
				var $pyjs__trackstack_size_1 = $pyjs['trackstack']['length'];
				try {
					try {
						$pyjs['in_try_except'] += 1;
						$pyjs['track']['lineno']=29;
						var $tupleassign1 = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['__ass_unpack']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['os']['path']['splitext']($p['getattr'](self, 'format'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})(), 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})();
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('format', $tupleassign1[0]) : $p['setattr'](self, 'format', $tupleassign1[0]); 
						blah = $tupleassign1[1];
					} finally { $pyjs['in_try_except'] -= 1; }
				} catch($pyjs_try_err) {
					$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_1 - 1);
					$pyjs['__active_exception_stack__'] = null;
					$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
					var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
					$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.relational';
					if (($pyjs_try_err_name == $p['ValueError']['__name__'])||$p['_isinstance']($pyjs_try_err,$p['ValueError'])) {
						$pyjs['track']['lineno']=31;
					} else { $pyjs['__active_exception_stack__'] = $pyjs['__last_exception_stack__']; $pyjs['__last_exception_stack__'] = null; throw $pyjs_try_err; }
				}
				$pyjs['track']['lineno']=33;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=34;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('structure', structure) : $p['setattr'](self, 'structure', structure); 
				$pyjs['track']['lineno']=35;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('boneName', boneName) : $p['setattr'](self, 'boneName', boneName); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['modul'],['boneName'],['structure']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=37;
			$method = $pyjs__bind_method2('render', function(data, field) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					field = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '5a86e2b984d743be0e22e96270f69554') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter1_nextval,$iter1_type,val,$iter1_idx,$iter1_iter,localizedRender,result,$pyjs__trackstack_size_1,x,$iter1_array;
				$pyjs['track']={'module':'bones.relational', 'lineno':37};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=37;
				$pyjs['track']['lineno']=38;
				localizedRender = function(val, format) {
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
					var i18n_val;
					$pyjs['track']={'module':'bones.relational','lineno':38};$pyjs['trackstack']['push']($pyjs['track']);
					$pyjs['track']['module']='bones.relational';
					$pyjs['track']['lineno']=38;
					$pyjs['track']['lineno']=39;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($m['conf']['__contains__']('currentlanguage'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})()) {
						$pyjs['track']['lineno']=40;
						i18n_val = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('%s.%s', (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['tuple']([$p['getattr'](self, 'format'), $m['conf']['__getitem__']('currentlanguage')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})();
						$pyjs['track']['lineno']=42;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](val['__contains__'](i18n_val));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})()) {
							$pyjs['track']['lineno']=43;
							val = (function(){try{try{$pyjs['in_try_except'] += 1;
							return $m['html5']['utils']['unescape'](val['__getitem__'](i18n_val));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})();
						}
						else if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](val['__contains__'](format));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_14_err){if (!$p['isinstance']($pyjs_dbg_14_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_14_err);}throw $pyjs_dbg_14_err;
}})()) {
							$pyjs['track']['lineno']=45;
							val = (function(){try{try{$pyjs['in_try_except'] += 1;
							return $m['html5']['utils']['unescape']((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['str'](val['__getitem__'](format));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_16_err){if (!$p['isinstance']($pyjs_dbg_16_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_16_err);}throw $pyjs_dbg_16_err;
}})();
						}
						else if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](val);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_17_err){if (!$p['isinstance']($pyjs_dbg_17_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_17_err);}throw $pyjs_dbg_17_err;
}})()) {
							$pyjs['track']['lineno']=47;
							val = val['__getitem__']('id');
						}
					}
					else {
						$pyjs['track']['lineno']=50;
						val = val['__getitem__']('id');
					}
					$pyjs['track']['lineno']=52;
					$pyjs['track']['lineno']=52;
					var $pyjs__ret = val;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				};
				localizedRender['__name__'] = 'localizedRender';

				localizedRender['__bind_type__'] = 0;
				localizedRender['__args__'] = [null,null,['val'],['format']];
				$pyjs['track']['lineno']=54;
				if (!( $p['op_eq'](field, $p['getattr'](self, 'boneName')) )) {
				   throw $p['AssertionError']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('render() was called with field %s, expected %s', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['tuple']([field, $p['getattr'](self, 'boneName')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_19_err){if (!$p['isinstance']($pyjs_dbg_19_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_19_err);}throw $pyjs_dbg_19_err;
}})());
				 }
				$pyjs['track']['lineno']=55;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})()['__contains__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})()) {
					$pyjs['track']['lineno']=56;
					val = data['__getitem__'](field);
				}
				else {
					$pyjs['track']['lineno']=58;
					val = '';
				}
				$pyjs['track']['lineno']=60;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](val, $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})()) {
					$pyjs['track']['lineno']=61;
					result = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})();
					$pyjs['track']['lineno']=62;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter1_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return val;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})();
					$iter1_nextval=$p['__iter_prepare']($iter1_iter,false);
					while (typeof($p['__wrapped_next']($iter1_nextval)['$nextval']) != 'undefined') {
						x = $iter1_nextval['$nextval'];
						$pyjs['track']['lineno']=63;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return result['append']((function(){try{try{$pyjs['in_try_except'] += 1;
						return localizedRender(x, $p['getattr'](self, 'format'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_26_err){if (!$p['isinstance']($pyjs_dbg_26_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_26_err);}throw $pyjs_dbg_26_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_27_err){if (!$p['isinstance']($pyjs_dbg_27_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_27_err);}throw $pyjs_dbg_27_err;
}})();
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.relational';
					$pyjs['track']['lineno']=65;
					$pyjs['track']['lineno']=65;
					var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
					return ', '['join'](result);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_28_err){if (!$p['isinstance']($pyjs_dbg_28_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_28_err);}throw $pyjs_dbg_28_err;
}})();
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](val, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_29_err){if (!$p['isinstance']($pyjs_dbg_29_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_29_err);}throw $pyjs_dbg_29_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_30_err){if (!$p['isinstance']($pyjs_dbg_30_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_30_err);}throw $pyjs_dbg_30_err;
}})()) {
					$pyjs['track']['lineno']=68;
					val = (function(){try{try{$pyjs['in_try_except'] += 1;
					return localizedRender(val, $p['getattr'](self, 'format'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_31_err){if (!$p['isinstance']($pyjs_dbg_31_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_31_err);}throw $pyjs_dbg_31_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=71;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['printFunc']([(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['tuple'](['warning type:', val, (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['type'](val);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_32_err){if (!$p['isinstance']($pyjs_dbg_32_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_32_err);}throw $pyjs_dbg_32_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_33_err){if (!$p['isinstance']($pyjs_dbg_33_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_33_err);}throw $pyjs_dbg_33_err;
}})()], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_34_err){if (!$p['isinstance']($pyjs_dbg_34_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_34_err);}throw $pyjs_dbg_34_err;
}})();
				}
				$pyjs['track']['lineno']=73;
				$pyjs['track']['lineno']=73;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['utils']['unescape'](val);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['data'],['field']]);
			$cls_definition['render'] = $method;
			$pyjs['track']['lineno']=17;
			var $bases = new Array($p['object']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('RelationalBoneExtractor', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=76;
		$m['RelationalViewBoneDelegate'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.relational';
			$cls_definition['__md5__'] = 'bc3aaaec0e1d46485596c223b78d3055';
			$pyjs['track']['lineno']=77;
			$cls_definition['cantSort'] = true;
			$pyjs['track']['lineno']=78;
			$method = $pyjs__bind_method2('__init__', function(modul, boneName, structure) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 4, arguments['length']+1);
				} else {
					var self = arguments[0];
					modul = arguments[1];
					boneName = arguments[2];
					structure = arguments[3];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 4, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'bc3aaaec0e1d46485596c223b78d3055') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':78};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=78;
				$pyjs['track']['lineno']=79;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['RelationalViewBoneDelegate'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_36_err){if (!$p['isinstance']($pyjs_dbg_36_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_36_err);}throw $pyjs_dbg_36_err;
}})()['__init__']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_37_err){if (!$p['isinstance']($pyjs_dbg_37_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_37_err);}throw $pyjs_dbg_37_err;
}})();
				$pyjs['track']['lineno']=80;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('format', '$(name)') : $p['setattr'](self, 'format', '$(name)'); 
				$pyjs['track']['lineno']=81;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return structure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_38_err){if (!$p['isinstance']($pyjs_dbg_38_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_38_err);}throw $pyjs_dbg_38_err;
}})()['__contains__']('format'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_39_err){if (!$p['isinstance']($pyjs_dbg_39_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_39_err);}throw $pyjs_dbg_39_err;
}})()) {
					$pyjs['track']['lineno']=82;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('format', structure['__getitem__'](boneName)['__getitem__']('format')) : $p['setattr'](self, 'format', structure['__getitem__'](boneName)['__getitem__']('format')); 
				}
				$pyjs['track']['lineno']=83;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=84;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('structure', structure) : $p['setattr'](self, 'structure', structure); 
				$pyjs['track']['lineno']=85;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('boneName', boneName) : $p['setattr'](self, 'boneName', boneName); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['modul'],['boneName'],['structure']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=87;
			$method = $pyjs__bind_method2('render', function(data, field) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					field = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'bc3aaaec0e1d46485596c223b78d3055') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $or5,$or6,val,$add3,res,$add2,$add1,$add4,$sub2,$sub1;
				$pyjs['track']={'module':'bones.relational', 'lineno':87};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=87;
				$pyjs['track']['lineno']=88;
				if (!( $p['op_eq'](field, $p['getattr'](self, 'boneName')) )) {
				   throw $p['AssertionError']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('render() was called with field %s, expected %s', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['tuple']([field, $p['getattr'](self, 'boneName')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_40_err){if (!$p['isinstance']($pyjs_dbg_40_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_40_err);}throw $pyjs_dbg_40_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_41_err){if (!$p['isinstance']($pyjs_dbg_41_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_41_err);}throw $pyjs_dbg_41_err;
}})());
				 }
				$pyjs['track']['lineno']=90;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_42_err){if (!$p['isinstance']($pyjs_dbg_42_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_42_err);}throw $pyjs_dbg_42_err;
}})()['__contains__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_43_err){if (!$p['isinstance']($pyjs_dbg_43_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_43_err);}throw $pyjs_dbg_43_err;
}})()) {
					$pyjs['track']['lineno']=91;
					val = data['__getitem__'](field);
				}
				else {
					$pyjs['track']['lineno']=93;
					val = '';
				}
				$pyjs['track']['lineno']=95;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](val, $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_44_err){if (!$p['isinstance']($pyjs_dbg_44_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_44_err);}throw $pyjs_dbg_44_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_45_err){if (!$p['isinstance']($pyjs_dbg_45_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_45_err);}throw $pyjs_dbg_45_err;
}})()) {
					$pyjs['track']['lineno']=96;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['len'](val);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_46_err){if (!$p['isinstance']($pyjs_dbg_46_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_46_err);}throw $pyjs_dbg_46_err;
}})(), $constant_int_5) == -1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})()) {
						$pyjs['track']['lineno']=97;
						res = (function(){try{try{$pyjs['in_try_except'] += 1;
						return ', '['join'](function(){
							var $iter2_nextval,$iter2_type,$iter2_iter,$or2,$collcomp1,$iter2_idx,$pyjs__trackstack_size_1,x,$or1,$iter2_array;
	$collcomp1 = $p['list']();
						$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
						$iter2_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
						return val;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_48_err){if (!$p['isinstance']($pyjs_dbg_48_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_48_err);}throw $pyjs_dbg_48_err;
}})();
						$iter2_nextval=$p['__iter_prepare']($iter2_iter,false);
						while (typeof($p['__wrapped_next']($iter2_nextval)['$nextval']) != 'undefined') {
							x = $iter2_nextval['$nextval'];
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $collcomp1['append'](($p['bool']($or1=(function(){try{try{$pyjs['in_try_except'] += 1;
							return $pyjs_kwargs_call(null, $m['formatString'], null, null, [{'unescape':true}, $p['getattr'](self, 'format'), $p['getattr'](self, 'structure'), x]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_49_err){if (!$p['isinstance']($pyjs_dbg_49_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_49_err);}throw $pyjs_dbg_49_err;
}})())?$or1:x['__getitem__']('id')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_50_err){if (!$p['isinstance']($pyjs_dbg_50_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_50_err);}throw $pyjs_dbg_50_err;
}})();
						}
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='bones.relational';

	return $collcomp1;}());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_51_err){if (!$p['isinstance']($pyjs_dbg_51_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_51_err);}throw $pyjs_dbg_51_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=99;
						res = (function(){try{try{$pyjs['in_try_except'] += 1;
						return ', '['join'](function(){
							var $or4,$iter3_idx,$or3,$iter3_nextval,$iter3_array,$collcomp2,$iter3_iter,$iter3_type,x,$pyjs__trackstack_size_1;
	$collcomp2 = $p['list']();
						$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
						$iter3_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['__getslice'](val, 0, $constant_int_4);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_52_err){if (!$p['isinstance']($pyjs_dbg_52_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_52_err);}throw $pyjs_dbg_52_err;
}})();
						$iter3_nextval=$p['__iter_prepare']($iter3_iter,false);
						while (typeof($p['__wrapped_next']($iter3_nextval)['$nextval']) != 'undefined') {
							x = $iter3_nextval['$nextval'];
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $collcomp2['append'](($p['bool']($or3=(function(){try{try{$pyjs['in_try_except'] += 1;
							return $pyjs_kwargs_call(null, $m['formatString'], null, null, [{'unescape':true}, $p['getattr'](self, 'format'), $p['getattr'](self, 'structure'), x]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_53_err){if (!$p['isinstance']($pyjs_dbg_53_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_53_err);}throw $pyjs_dbg_53_err;
}})())?$or3:x['__getitem__']('id')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_54_err){if (!$p['isinstance']($pyjs_dbg_54_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_54_err);}throw $pyjs_dbg_54_err;
}})();
						}
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='bones.relational';

	return $collcomp2;}());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_55_err){if (!$p['isinstance']($pyjs_dbg_55_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_55_err);}throw $pyjs_dbg_55_err;
}})();
						$pyjs['track']['lineno']=100;
						res = $p['__op_add']($add3=res,$add4=$p['__op_add']($add1=' ',$add2=(function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call(null, $m['translate'], null, null, [{'count':$p['__op_sub']($sub1=(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['len'](val);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_56_err){if (!$p['isinstance']($pyjs_dbg_56_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_56_err);}throw $pyjs_dbg_56_err;
}})(),$sub2=$constant_int_4)}, 'and {count} more']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_57_err){if (!$p['isinstance']($pyjs_dbg_57_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_57_err);}throw $pyjs_dbg_57_err;
}})()));
					}
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](val, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_58_err){if (!$p['isinstance']($pyjs_dbg_58_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_58_err);}throw $pyjs_dbg_58_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_59_err){if (!$p['isinstance']($pyjs_dbg_59_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_59_err);}throw $pyjs_dbg_59_err;
}})()) {
					$pyjs['track']['lineno']=103;
					res = ($p['bool']($or5=(function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call(null, $m['formatString'], null, null, [{'unescape':true}, $p['getattr'](self, 'format'), $p['getattr'](self, 'structure'), val]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_60_err){if (!$p['isinstance']($pyjs_dbg_60_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_60_err);}throw $pyjs_dbg_60_err;
}})())?$or5:val['__getitem__']('id'));
				}
				$pyjs['track']['lineno']=105;
				$pyjs['track']['lineno']=105;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Label'](res);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_61_err){if (!$p['isinstance']($pyjs_dbg_61_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_61_err);}throw $pyjs_dbg_61_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['data'],['field']]);
			$cls_definition['render'] = $method;
			$pyjs['track']['lineno']=76;
			var $bases = new Array($p['object']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('RelationalViewBoneDelegate', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=109;
		$m['RelationalSingleSelectionBone'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.relational';
			$cls_definition['__md5__'] = '2c58640263e5a44057fad104c8eebfaf';
			$pyjs['track']['lineno']=114;
			$method = $pyjs__bind_method2('__init__', function(srcModul, boneName, readOnly, destModul, format, required) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,6,arguments['length']-1));

					var kwargs = arguments['length'] >= 7 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 5, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					srcModul = arguments[1];
					boneName = arguments[2];
					readOnly = arguments[3];
					destModul = arguments[4];
					format = arguments[5];
					required = arguments[6];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,7,arguments['length']-1));

					var kwargs = arguments['length'] >= 8 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 5) $pyjs__exception_func_param(arguments['callee']['__name__'], 5, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2c58640263e5a44057fad104c8eebfaf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof required != 'undefined') {
						if (required !== null && typeof required['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = required;
							required = arguments[7];
						}
					} else 					if (typeof format != 'undefined') {
						if (format !== null && typeof format['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = format;
							format = arguments[7];
						}
					} else 					if (typeof destModul != 'undefined') {
						if (destModul !== null && typeof destModul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = destModul;
							destModul = arguments[7];
						}
					} else 					if (typeof readOnly != 'undefined') {
						if (readOnly !== null && typeof readOnly['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = readOnly;
							readOnly = arguments[7];
						}
					} else 					if (typeof boneName != 'undefined') {
						if (boneName !== null && typeof boneName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = boneName;
							boneName = arguments[7];
						}
					} else 					if (typeof srcModul != 'undefined') {
						if (srcModul !== null && typeof srcModul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = srcModul;
							srcModul = arguments[7];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[7];
						}
					} else {
					}
				}
				if (typeof format == 'undefined') format=arguments['callee']['__args__'][7][1];
				if (typeof required == 'undefined') required=arguments['callee']['__args__'][8][1];
				var $or11,$add7,$or7,$and1,$and2,$and3,$or9,$or8,$add10,$add5,$add6,$or10,$or12,$add8,$add9;
				$pyjs['track']={'module':'bones.relational', 'lineno':114};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=114;
				$pyjs['track']['lineno']=127;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['RelationalSingleSelectionBone'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_62_err){if (!$p['isinstance']($pyjs_dbg_62_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_62_err);}throw $pyjs_dbg_62_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_63_err){if (!$p['isinstance']($pyjs_dbg_63_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_63_err);}throw $pyjs_dbg_63_err;
}})();
				$pyjs['track']['lineno']=128;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('srcModul', srcModul) : $p['setattr'](self, 'srcModul', srcModul); 
				$pyjs['track']['lineno']=129;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('boneName', boneName) : $p['setattr'](self, 'boneName', boneName); 
				$pyjs['track']['lineno']=130;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('readOnly', readOnly) : $p['setattr'](self, 'readOnly', readOnly); 
				$pyjs['track']['lineno']=131;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('destModul', destModul) : $p['setattr'](self, 'destModul', destModul); 
				$pyjs['track']['lineno']=132;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('format', format) : $p['setattr'](self, 'format', format); 
				$pyjs['track']['lineno']=133;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selection', null) : $p['setattr'](self, 'selection', null); 
				$pyjs['track']['lineno']=134;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectionTxt', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})()) : $p['setattr'](self, 'selectionTxt', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})()); 
				$pyjs['track']['lineno']=135;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'selectionTxt')['__setitem__']('type', 'text');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_65_err){if (!$p['isinstance']($pyjs_dbg_65_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_65_err);}throw $pyjs_dbg_65_err;
}})();
				$pyjs['track']['lineno']=136;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'selectionTxt')['__setitem__']('readonly', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_66_err){if (!$p['isinstance']($pyjs_dbg_66_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_66_err);}throw $pyjs_dbg_66_err;
}})();
				$pyjs['track']['lineno']=137;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'selectionTxt'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})();
				$pyjs['track']['lineno']=142;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($or7=$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']('root'))?$or7:$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']($p['__op_add']($add5=destModul,$add6='-view'))));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_68_err){if (!$p['isinstance']($pyjs_dbg_68_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_68_err);}throw $pyjs_dbg_68_err;
}})()) {
					$pyjs['track']['lineno']=144;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['ext']['Button']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Select');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_69_err){if (!$p['isinstance']($pyjs_dbg_69_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_69_err);}throw $pyjs_dbg_69_err;
}})(), $p['getattr'](self, 'onShowSelector'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_70_err){if (!$p['isinstance']($pyjs_dbg_70_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_70_err);}throw $pyjs_dbg_70_err;
}})()) : $p['setattr'](self, 'selectBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['ext']['Button']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Select');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_69_err){if (!$p['isinstance']($pyjs_dbg_69_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_69_err);}throw $pyjs_dbg_69_err;
}})(), $p['getattr'](self, 'onShowSelector'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_70_err){if (!$p['isinstance']($pyjs_dbg_70_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_70_err);}throw $pyjs_dbg_70_err;
}})()); 
					$pyjs['track']['lineno']=145;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'selectBtn')['__getitem__']('class')['append']('icon');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_71_err){if (!$p['isinstance']($pyjs_dbg_71_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_71_err);}throw $pyjs_dbg_71_err;
}})();
					$pyjs['track']['lineno']=146;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'selectBtn')['__getitem__']('class')['append']('select');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_72_err){if (!$p['isinstance']($pyjs_dbg_72_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_72_err);}throw $pyjs_dbg_72_err;
}})();
					$pyjs['track']['lineno']=147;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'selectBtn'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_73_err){if (!$p['isinstance']($pyjs_dbg_73_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_73_err);}throw $pyjs_dbg_73_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=149;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectBtn', null) : $p['setattr'](self, 'selectBtn', null); 
				}
				$pyjs['track']['lineno']=152;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($or9=$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']('root'))?$or9:$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']($p['__op_add']($add7=destModul,$add8='-edit'))));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_74_err){if (!$p['isinstance']($pyjs_dbg_74_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_74_err);}throw $pyjs_dbg_74_err;
}})()) {
					$pyjs['track']['lineno']=154;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('editBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['ext']['Button']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Edit');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})(), $p['getattr'](self, 'onEdit'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})()) : $p['setattr'](self, 'editBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['ext']['Button']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Edit');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})(), $p['getattr'](self, 'onEdit'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})()); 
					$pyjs['track']['lineno']=155;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'editBtn')['__getitem__']('class')['append']('icon');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_77_err){if (!$p['isinstance']($pyjs_dbg_77_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_77_err);}throw $pyjs_dbg_77_err;
}})();
					$pyjs['track']['lineno']=156;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'editBtn')['__getitem__']('class')['append']('edit');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_78_err){if (!$p['isinstance']($pyjs_dbg_78_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_78_err);}throw $pyjs_dbg_78_err;
}})();
					$pyjs['track']['lineno']=157;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'editBtn'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_79_err){if (!$p['isinstance']($pyjs_dbg_79_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_79_err);}throw $pyjs_dbg_79_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=159;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('editBtn', null) : $p['setattr'](self, 'editBtn', null); 
				}
				$pyjs['track']['lineno']=162;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and1=!$p['bool'](required))?($p['bool']($and2=!$p['bool'](readOnly))?($p['bool']($or11=$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']('root'))?$or11:$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']($p['__op_add']($add9=destModul,$add10='-view'))):$and2):$and1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_80_err){if (!$p['isinstance']($pyjs_dbg_80_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_80_err);}throw $pyjs_dbg_80_err;
}})()) {
					$pyjs['track']['lineno']=168;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('remBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['ext']['Button']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Remove');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_81_err){if (!$p['isinstance']($pyjs_dbg_81_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_81_err);}throw $pyjs_dbg_81_err;
}})(), $p['getattr'](self, 'onRemove'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_82_err){if (!$p['isinstance']($pyjs_dbg_82_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_82_err);}throw $pyjs_dbg_82_err;
}})()) : $p['setattr'](self, 'remBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['ext']['Button']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Remove');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_81_err){if (!$p['isinstance']($pyjs_dbg_81_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_81_err);}throw $pyjs_dbg_81_err;
}})(), $p['getattr'](self, 'onRemove'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_82_err){if (!$p['isinstance']($pyjs_dbg_82_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_82_err);}throw $pyjs_dbg_82_err;
}})()); 
					$pyjs['track']['lineno']=169;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'remBtn')['__getitem__']('class')['append']('icon');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_83_err){if (!$p['isinstance']($pyjs_dbg_83_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_83_err);}throw $pyjs_dbg_83_err;
}})();
					$pyjs['track']['lineno']=170;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'remBtn')['__getitem__']('class')['append']('cancel');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_84_err){if (!$p['isinstance']($pyjs_dbg_84_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_84_err);}throw $pyjs_dbg_84_err;
}})();
					$pyjs['track']['lineno']=171;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'remBtn'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=173;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('remBtn', null) : $p['setattr'](self, 'remBtn', null); 
				}
				$pyjs['track']['lineno']=175;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'readOnly'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})()) {
					$pyjs['track']['lineno']=176;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['srcModul'],['boneName'],['readOnly'],['destModul'],['format', '$(name)'],['required', false]]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=180;
			$method = $pyjs__bind_method2('_setDisabled', function(disable) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					disable = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2c58640263e5a44057fad104c8eebfaf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and4,$and5,$and6;
				$pyjs['track']={'module':'bones.relational', 'lineno':180};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=180;
				$pyjs['track']['lineno']=184;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['RelationalSingleSelectionBone'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})()['_setDisabled'](disable);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_89_err){if (!$p['isinstance']($pyjs_dbg_89_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_89_err);}throw $pyjs_dbg_89_err;
}})();
				$pyjs['track']['lineno']=185;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and4=!$p['bool'](disable))?($p['bool']($and5=!$p['bool']($p['getattr'](self, '_disabledState')))?(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})()['__getitem__']('class')['__contains__']('is_active'):$and5):$and4));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_91_err){if (!$p['isinstance']($pyjs_dbg_91_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_91_err);}throw $pyjs_dbg_91_err;
}})()) {
					$pyjs['track']['lineno']=186;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_92_err){if (!$p['isinstance']($pyjs_dbg_92_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_92_err);}throw $pyjs_dbg_92_err;
}})()['__getitem__']('class')['remove']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_93_err){if (!$p['isinstance']($pyjs_dbg_93_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_93_err);}throw $pyjs_dbg_93_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['disable']]);
			$cls_definition['_setDisabled'] = $method;
			$pyjs['track']['lineno']=189;
			$method = $pyjs__bind_method2('fromSkelStructure', function(modulName, boneName, skelStructure) {
    if ($pyjs['options']['arg_is_instance'] && this['__is_instance__'] !== true && this['__is_instance__'] !== false) $pyjs__exception_func_class_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__']);
    if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 3, arguments['length']);
    var cls = this['prototype'];
				var $and8,destModul,multiple,$and9,format,required,$and10,$and7,readOnly;
				$pyjs['track']={'module':'bones.relational', 'lineno':189};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=189;
				$pyjs['track']['lineno']=199;
				readOnly = ($p['bool']($and7=(function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_94_err){if (!$p['isinstance']($pyjs_dbg_94_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_94_err);}throw $pyjs_dbg_94_err;
}})()['__contains__']('readonly'))?skelStructure['__getitem__'](boneName)['__getitem__']('readonly'):$and7);
				$pyjs['track']['lineno']=200;
				multiple = skelStructure['__getitem__'](boneName)['__getitem__']('multiple');
				$pyjs['track']['lineno']=201;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and9=(function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_95_err){if (!$p['isinstance']($pyjs_dbg_95_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_95_err);}throw $pyjs_dbg_95_err;
}})()['__contains__']('required'))?skelStructure['__getitem__'](boneName)['__getitem__']('required'):$and9));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_96_err){if (!$p['isinstance']($pyjs_dbg_96_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_96_err);}throw $pyjs_dbg_96_err;
}})()) {
					$pyjs['track']['lineno']=202;
					required = true;
				}
				else {
					$pyjs['track']['lineno']=204;
					required = false;
				}
				$pyjs['track']['lineno']=205;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_97_err){if (!$p['isinstance']($pyjs_dbg_97_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_97_err);}throw $pyjs_dbg_97_err;
}})()['__contains__']('modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_98_err){if (!$p['isinstance']($pyjs_dbg_98_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_98_err);}throw $pyjs_dbg_98_err;
}})()) {
					$pyjs['track']['lineno']=206;
					destModul = skelStructure['__getitem__'](boneName)['__getitem__']('modul');
				}
				else {
					$pyjs['track']['lineno']=208;
					destModul = (function(){try{try{$pyjs['in_try_except'] += 1;
					return skelStructure['__getitem__'](boneName)['__getitem__']('type')['$$split']('.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_99_err){if (!$p['isinstance']($pyjs_dbg_99_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_99_err);}throw $pyjs_dbg_99_err;
}})()['__getitem__']($constant_int_1);
				}
				$pyjs['track']['lineno']=209;
				format = '$(name)';
				$pyjs['track']['lineno']=210;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_100_err){if (!$p['isinstance']($pyjs_dbg_100_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_100_err);}throw $pyjs_dbg_100_err;
}})()['__contains__']('format'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_101_err){if (!$p['isinstance']($pyjs_dbg_101_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_101_err);}throw $pyjs_dbg_101_err;
}})()) {
					$pyjs['track']['lineno']=211;
					format = skelStructure['__getitem__'](boneName)['__getitem__']('format');
				}
				$pyjs['track']['lineno']=212;
				$pyjs['track']['lineno']=212;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, cls, null, null, [{'destModul':destModul, 'format':format, 'required':required}, modulName, boneName, readOnly]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_102_err){if (!$p['isinstance']($pyjs_dbg_102_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_102_err);}throw $pyjs_dbg_102_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 2, [null,null,['cls'],['modulName'],['boneName'],['skelStructure']]);
			$cls_definition['fromSkelStructure'] = $method;
			$pyjs['track']['lineno']=214;
			$method = $pyjs__bind_method2('onEdit', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2c58640263e5a44057fad104c8eebfaf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var edwg,pane;
				$pyjs['track']={'module':'bones.relational', 'lineno':214};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=214;
				$pyjs['track']['lineno']=218;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, 'selection')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_103_err){if (!$p['isinstance']($pyjs_dbg_103_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_103_err);}throw $pyjs_dbg_103_err;
}})()) {
					$pyjs['track']['lineno']=219;
					$pyjs['track']['lineno']=219;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=221;
				pane = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['Pane'], null, null, [{'closeable':true, 'iconClasses':(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('modul_%s', $p['getattr'](self, 'destModul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_105_err){if (!$p['isinstance']($pyjs_dbg_105_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_105_err);}throw $pyjs_dbg_105_err;
}})(), 'apptype_list', 'action_edit']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_106_err){if (!$p['isinstance']($pyjs_dbg_106_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_106_err);}throw $pyjs_dbg_106_err;
}})()}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Edit');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_104_err){if (!$p['isinstance']($pyjs_dbg_104_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_104_err);}throw $pyjs_dbg_104_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_107_err){if (!$p['isinstance']($pyjs_dbg_107_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_107_err);}throw $pyjs_dbg_107_err;
}})();
				$pyjs['track']['lineno']=223;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['conf']['__getitem__']('mainWindow'), 'stackPane', null, null, [{'focus':true}, pane]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_108_err){if (!$p['isinstance']($pyjs_dbg_108_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_108_err);}throw $pyjs_dbg_108_err;
}})();
				$pyjs['track']['lineno']=224;
				edwg = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['EditWidget'], null, null, [{'key':$p['getattr'](self, 'selection')['__getitem__']('id')}, $p['getattr'](self, 'destModul'), $p['getattr']($m['EditWidget'], 'appList')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_109_err){if (!$p['isinstance']($pyjs_dbg_109_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_109_err);}throw $pyjs_dbg_109_err;
}})();
				$pyjs['track']['lineno']=225;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return pane['addWidget'](edwg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_110_err){if (!$p['isinstance']($pyjs_dbg_110_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_110_err);}throw $pyjs_dbg_110_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['onEdit'] = $method;
			$pyjs['track']['lineno']=227;
			$method = $pyjs__bind_method2('onRemove', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2c58640263e5a44057fad104c8eebfaf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':227};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=227;
				$pyjs['track']['lineno']=228;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['setSelection'](null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_111_err){if (!$p['isinstance']($pyjs_dbg_111_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_111_err);}throw $pyjs_dbg_111_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['onRemove'] = $method;
			$pyjs['track']['lineno']=230;
			$method = $pyjs__bind_method2('unserialize', function(data) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2c58640263e5a44057fad104c8eebfaf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var val;
				$pyjs['track']={'module':'bones.relational', 'lineno':230};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=230;
				$pyjs['track']['lineno']=236;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_112_err){if (!$p['isinstance']($pyjs_dbg_112_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_112_err);}throw $pyjs_dbg_112_err;
}})()['__contains__']($p['getattr'](self, 'boneName')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_113_err){if (!$p['isinstance']($pyjs_dbg_113_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_113_err);}throw $pyjs_dbg_113_err;
}})()) {
					$pyjs['track']['lineno']=237;
					val = data['__getitem__']($p['getattr'](self, 'boneName'));
					$pyjs['track']['lineno']=238;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](val, $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_114_err){if (!$p['isinstance']($pyjs_dbg_114_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_114_err);}throw $pyjs_dbg_114_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_115_err){if (!$p['isinstance']($pyjs_dbg_115_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_115_err);}throw $pyjs_dbg_115_err;
}})()) {
						$pyjs['track']['lineno']=239;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['len'](val);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_116_err){if (!$p['isinstance']($pyjs_dbg_116_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_116_err);}throw $pyjs_dbg_116_err;
}})(), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_117_err){if (!$p['isinstance']($pyjs_dbg_117_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_117_err);}throw $pyjs_dbg_117_err;
}})()) {
							$pyjs['track']['lineno']=240;
							val = val['__getitem__']($constant_int_0);
						}
						else {
							$pyjs['track']['lineno']=242;
							val = null;
						}
					}
					$pyjs['track']['lineno']=243;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](val, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_118_err){if (!$p['isinstance']($pyjs_dbg_118_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_118_err);}throw $pyjs_dbg_118_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_119_err){if (!$p['isinstance']($pyjs_dbg_119_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_119_err);}throw $pyjs_dbg_119_err;
}})()) {
						$pyjs['track']['lineno']=244;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['setSelection'](val);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_120_err){if (!$p['isinstance']($pyjs_dbg_120_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_120_err);}throw $pyjs_dbg_120_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=246;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['setSelection'](null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_121_err){if (!$p['isinstance']($pyjs_dbg_121_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_121_err);}throw $pyjs_dbg_121_err;
}})();
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['data']]);
			$cls_definition['unserialize'] = $method;
			$pyjs['track']['lineno']=250;
			$method = $pyjs__bind_method2('serializeForPost', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2c58640263e5a44057fad104c8eebfaf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':250};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=250;
				$pyjs['track']['lineno']=255;
				$pyjs['track']['lineno']=255;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([[$p['getattr'](self, 'boneName'), ($p['bool'](!$p['op_is']($p['getattr'](self, 'selection'), null))? ($p['getattr'](self, 'selection')['__getitem__']('id')) : (''))]]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_122_err){if (!$p['isinstance']($pyjs_dbg_122_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_122_err);}throw $pyjs_dbg_122_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['serializeForPost'] = $method;
			$pyjs['track']['lineno']=257;
			$method = $pyjs__bind_method2('serializeForDocument', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2c58640263e5a44057fad104c8eebfaf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':257};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=257;
				$pyjs['track']['lineno']=258;
				$pyjs['track']['lineno']=258;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['serialize']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_123_err){if (!$p['isinstance']($pyjs_dbg_123_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_123_err);}throw $pyjs_dbg_123_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['serializeForDocument'] = $method;
			$pyjs['track']['lineno']=260;
			$method = $pyjs__bind_method2('onShowSelector', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2c58640263e5a44057fad104c8eebfaf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var currentSelector;
				$pyjs['track']={'module':'bones.relational', 'lineno':260};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=260;
				$pyjs['track']['lineno']=264;
				currentSelector = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['ListWidget'], null, null, [{'isSelector':true}, $p['getattr'](self, 'destModul')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_124_err){if (!$p['isinstance']($pyjs_dbg_124_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_124_err);}throw $pyjs_dbg_124_err;
}})();
				$pyjs['track']['lineno']=265;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return currentSelector['selectionActivatedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_125_err){if (!$p['isinstance']($pyjs_dbg_125_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_125_err);}throw $pyjs_dbg_125_err;
}})();
				$pyjs['track']['lineno']=266;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['stackWidget'](currentSelector);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_126_err){if (!$p['isinstance']($pyjs_dbg_126_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_126_err);}throw $pyjs_dbg_126_err;
}})();
				$pyjs['track']['lineno']=267;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_127_err){if (!$p['isinstance']($pyjs_dbg_127_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_127_err);}throw $pyjs_dbg_127_err;
}})()['__getitem__']('class')['append']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_128_err){if (!$p['isinstance']($pyjs_dbg_128_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_128_err);}throw $pyjs_dbg_128_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['onShowSelector'] = $method;
			$pyjs['track']['lineno']=269;
			$method = $pyjs__bind_method2('onSelectionActivated', function(table, selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					selection = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2c58640263e5a44057fad104c8eebfaf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':269};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=269;
				$pyjs['track']['lineno']=273;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](selection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_129_err){if (!$p['isinstance']($pyjs_dbg_129_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_129_err);}throw $pyjs_dbg_129_err;
}})()) {
					$pyjs['track']['lineno']=274;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['setSelection'](selection['__getitem__']($constant_int_0));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_130_err){if (!$p['isinstance']($pyjs_dbg_130_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_130_err);}throw $pyjs_dbg_130_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=276;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['setSelection'](null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_131_err){if (!$p['isinstance']($pyjs_dbg_131_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_131_err);}throw $pyjs_dbg_131_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['selection']]);
			$cls_definition['onSelectionActivated'] = $method;
			$pyjs['track']['lineno']=278;
			$method = $pyjs__bind_method2('setSelection', function(selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					selection = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2c58640263e5a44057fad104c8eebfaf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $add11,$add12;
				$pyjs['track']={'module':'bones.relational', 'lineno':278};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=278;
				$pyjs['track']['lineno']=284;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selection', selection) : $p['setattr'](self, 'selection', selection); 
				$pyjs['track']['lineno']=285;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](selection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_132_err){if (!$p['isinstance']($pyjs_dbg_132_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_132_err);}throw $pyjs_dbg_132_err;
}})()) {
					$pyjs['track']['lineno']=286;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'successHandler':$p['getattr'](self, 'onSelectionDataAviable'), 'cacheable':true}, $p['getattr'](self, 'destModul'), $p['__op_add']($add11='view/',$add12=selection['__getitem__']('id'))]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_133_err){if (!$p['isinstance']($pyjs_dbg_133_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_133_err);}throw $pyjs_dbg_133_err;
}})();
					$pyjs['track']['lineno']=288;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'selectionTxt')['__setitem__']('value', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Loading...');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_134_err){if (!$p['isinstance']($pyjs_dbg_134_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_134_err);}throw $pyjs_dbg_134_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_135_err){if (!$p['isinstance']($pyjs_dbg_135_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_135_err);}throw $pyjs_dbg_135_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=290;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'selectionTxt')['__setitem__']('value', '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_136_err){if (!$p['isinstance']($pyjs_dbg_136_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_136_err);}throw $pyjs_dbg_136_err;
}})();
				}
				$pyjs['track']['lineno']=292;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['updateButtons']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_137_err){if (!$p['isinstance']($pyjs_dbg_137_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_137_err);}throw $pyjs_dbg_137_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['selection']]);
			$cls_definition['setSelection'] = $method;
			$pyjs['track']['lineno']=294;
			$method = $pyjs__bind_method2('updateButtons', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2c58640263e5a44057fad104c8eebfaf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':294};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=294;
				$pyjs['track']['lineno']=298;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'selection'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_138_err){if (!$p['isinstance']($pyjs_dbg_138_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_138_err);}throw $pyjs_dbg_138_err;
}})()) {
					$pyjs['track']['lineno']=299;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'editBtn'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_139_err){if (!$p['isinstance']($pyjs_dbg_139_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_139_err);}throw $pyjs_dbg_139_err;
}})()) {
						$pyjs['track']['lineno']=300;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'editBtn')['__setitem__']('disabled', false);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_140_err){if (!$p['isinstance']($pyjs_dbg_140_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_140_err);}throw $pyjs_dbg_140_err;
}})();
					}
					$pyjs['track']['lineno']=301;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'remBtn'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_141_err){if (!$p['isinstance']($pyjs_dbg_141_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_141_err);}throw $pyjs_dbg_141_err;
}})()) {
						$pyjs['track']['lineno']=302;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'remBtn')['__setitem__']('disabled', false);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_142_err){if (!$p['isinstance']($pyjs_dbg_142_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_142_err);}throw $pyjs_dbg_142_err;
}})();
					}
				}
				else {
					$pyjs['track']['lineno']=304;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'editBtn'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_143_err){if (!$p['isinstance']($pyjs_dbg_143_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_143_err);}throw $pyjs_dbg_143_err;
}})()) {
						$pyjs['track']['lineno']=305;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'editBtn')['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_144_err){if (!$p['isinstance']($pyjs_dbg_144_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_144_err);}throw $pyjs_dbg_144_err;
}})();
					}
					$pyjs['track']['lineno']=306;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'remBtn'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_145_err){if (!$p['isinstance']($pyjs_dbg_145_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_145_err);}throw $pyjs_dbg_145_err;
}})()) {
						$pyjs['track']['lineno']=307;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'remBtn')['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_146_err){if (!$p['isinstance']($pyjs_dbg_146_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_146_err);}throw $pyjs_dbg_146_err;
}})();
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['updateButtons'] = $method;
			$pyjs['track']['lineno']=309;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2c58640263e5a44057fad104c8eebfaf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':309};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=309;
				$pyjs['track']['lineno']=310;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['RelationalSingleSelectionBone'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_147_err){if (!$p['isinstance']($pyjs_dbg_147_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_147_err);}throw $pyjs_dbg_147_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_148_err){if (!$p['isinstance']($pyjs_dbg_148_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_148_err);}throw $pyjs_dbg_148_err;
}})();
				$pyjs['track']['lineno']=311;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['registerChangeListener'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_149_err){if (!$p['isinstance']($pyjs_dbg_149_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_149_err);}throw $pyjs_dbg_149_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=313;
			$method = $pyjs__bind_method2('onDetach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2c58640263e5a44057fad104c8eebfaf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':313};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=313;
				$pyjs['track']['lineno']=314;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['removeChangeListener'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_150_err){if (!$p['isinstance']($pyjs_dbg_150_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_150_err);}throw $pyjs_dbg_150_err;
}})();
				$pyjs['track']['lineno']=315;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['RelationalSingleSelectionBone'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_151_err){if (!$p['isinstance']($pyjs_dbg_151_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_151_err);}throw $pyjs_dbg_151_err;
}})()['onDetach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_152_err){if (!$p['isinstance']($pyjs_dbg_152_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_152_err);}throw $pyjs_dbg_152_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onDetach'] = $method;
			$pyjs['track']['lineno']=317;
			$method = $pyjs__bind_method2('onDataChanged', function(modul) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					modul = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2c58640263e5a44057fad104c8eebfaf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':317};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=317;
				$pyjs['track']['lineno']=318;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq'](modul, $p['getattr'](self, 'destModul')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_153_err){if (!$p['isinstance']($pyjs_dbg_153_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_153_err);}throw $pyjs_dbg_153_err;
}})()) {
					$pyjs['track']['lineno']=319;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['setSelection']($p['getattr'](self, 'selection'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_154_err){if (!$p['isinstance']($pyjs_dbg_154_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_154_err);}throw $pyjs_dbg_154_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['modul']]);
			$cls_definition['onDataChanged'] = $method;
			$pyjs['track']['lineno']=321;
			$method = $pyjs__bind_method2('onSelectionDataAviable', function(req) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					req = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '2c58640263e5a44057fad104c8eebfaf') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var data;
				$pyjs['track']={'module':'bones.relational', 'lineno':321};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=321;
				$pyjs['track']['lineno']=325;
				data = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['decode'](req);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_155_err){if (!$p['isinstance']($pyjs_dbg_155_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_155_err);}throw $pyjs_dbg_155_err;
}})();
				$pyjs['track']['lineno']=326;
				if (!( $p['op_eq']($p['getattr'](self, 'selection')['__getitem__']('id'), data['__getitem__']('values')['__getitem__']('id')) )) {
				   throw $p['AssertionError']();
				 }
				$pyjs['track']['lineno']=327;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'selectionTxt')['__setitem__']('value', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['formatString']($p['getattr'](self, 'format'), data['__getitem__']('structure'), data['__getitem__']('values'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_156_err){if (!$p['isinstance']($pyjs_dbg_156_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_156_err);}throw $pyjs_dbg_156_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_157_err){if (!$p['isinstance']($pyjs_dbg_157_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_157_err);}throw $pyjs_dbg_157_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['req']]);
			$cls_definition['onSelectionDataAviable'] = $method;
			$pyjs['track']['lineno']=109;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('RelationalSingleSelectionBone', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=329;
		$m['RelationalMultiSelectionBoneEntry'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.relational';
			$cls_definition['__md5__'] = 'c842967cd6a1e54b3f5cf623234f0ad0';
			$pyjs['track']['lineno']=335;
			$method = $pyjs__bind_method2('__init__', function(parent, modul, data, format) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,4,arguments['length']-1));

					var kwargs = arguments['length'] >= 5 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					parent = arguments[1];
					modul = arguments[2];
					data = arguments[3];
					format = arguments[4];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,5,arguments['length']-1));

					var kwargs = arguments['length'] >= 6 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'c842967cd6a1e54b3f5cf623234f0ad0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof format != 'undefined') {
						if (format !== null && typeof format['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = format;
							format = arguments[5];
						}
					} else 					if (typeof data != 'undefined') {
						if (data !== null && typeof data['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = data;
							data = arguments[5];
						}
					} else 					if (typeof modul != 'undefined') {
						if (modul !== null && typeof modul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modul;
							modul = arguments[5];
						}
					} else 					if (typeof parent != 'undefined') {
						if (parent !== null && typeof parent['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = parent;
							parent = arguments[5];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[5];
						}
					} else {
					}
				}
				if (typeof format == 'undefined') format=arguments['callee']['__args__'][6][1];
				var $add14,$add15,$and12,remBtn,$or16,$add13,$or14,$add16,$and11,$or15,$or13,editBtn;
				$pyjs['track']={'module':'bones.relational', 'lineno':335};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=335;
				$pyjs['track']['lineno']=344;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['RelationalMultiSelectionBoneEntry'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_158_err){if (!$p['isinstance']($pyjs_dbg_158_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_158_err);}throw $pyjs_dbg_158_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_159_err){if (!$p['isinstance']($pyjs_dbg_159_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_159_err);}throw $pyjs_dbg_159_err;
}})();
				$pyjs['track']['lineno']=345;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('parent', parent) : $p['setattr'](self, 'parent', parent); 
				$pyjs['track']['lineno']=346;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=347;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('data', data) : $p['setattr'](self, 'data', data); 
				$pyjs['track']['lineno']=348;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('format', format) : $p['setattr'](self, 'format', format); 
				$pyjs['track']['lineno']=349;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectionTxt', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_160_err){if (!$p['isinstance']($pyjs_dbg_160_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_160_err);}throw $pyjs_dbg_160_err;
}})()) : $p['setattr'](self, 'selectionTxt', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_160_err){if (!$p['isinstance']($pyjs_dbg_160_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_160_err);}throw $pyjs_dbg_160_err;
}})()); 
				$pyjs['track']['lineno']=350;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionTxt']['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['TextNode']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Loading...');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_161_err){if (!$p['isinstance']($pyjs_dbg_161_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_161_err);}throw $pyjs_dbg_161_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_162_err){if (!$p['isinstance']($pyjs_dbg_162_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_162_err);}throw $pyjs_dbg_162_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_163_err){if (!$p['isinstance']($pyjs_dbg_163_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_163_err);}throw $pyjs_dbg_163_err;
}})();
				$pyjs['track']['lineno']=351;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'selectionTxt')['__getitem__']('class')['append']('entrydescription');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_164_err){if (!$p['isinstance']($pyjs_dbg_164_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_164_err);}throw $pyjs_dbg_164_err;
}})();
				$pyjs['track']['lineno']=352;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'selectionTxt'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_165_err){if (!$p['isinstance']($pyjs_dbg_165_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_165_err);}throw $pyjs_dbg_165_err;
}})();
				$pyjs['track']['lineno']=355;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($or13=$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']('root'))?$or13:$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']($p['__op_add']($add13=modul,$add14='-edit'))));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_166_err){if (!$p['isinstance']($pyjs_dbg_166_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_166_err);}throw $pyjs_dbg_166_err;
}})()) {
					$pyjs['track']['lineno']=357;
					editBtn = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['ext']['Button']('Edit', $p['getattr'](self, 'onEdit'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_167_err){if (!$p['isinstance']($pyjs_dbg_167_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_167_err);}throw $pyjs_dbg_167_err;
}})();
					$pyjs['track']['lineno']=358;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return editBtn['__getitem__']('class')['append']('icon');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_168_err){if (!$p['isinstance']($pyjs_dbg_168_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_168_err);}throw $pyjs_dbg_168_err;
}})();
					$pyjs['track']['lineno']=359;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return editBtn['__getitem__']('class')['append']('edit');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_169_err){if (!$p['isinstance']($pyjs_dbg_169_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_169_err);}throw $pyjs_dbg_169_err;
}})();
					$pyjs['track']['lineno']=360;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild'](editBtn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_170_err){if (!$p['isinstance']($pyjs_dbg_170_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_170_err);}throw $pyjs_dbg_170_err;
}})();
				}
				$pyjs['track']['lineno']=363;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and11=!$p['bool']($p['getattr'](parent, 'readOnly')))?($p['bool']($or15=$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']('root'))?$or15:$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']($p['__op_add']($add15=modul,$add16='-view'))):$and11));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_171_err){if (!$p['isinstance']($pyjs_dbg_171_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_171_err);}throw $pyjs_dbg_171_err;
}})()) {
					$pyjs['track']['lineno']=368;
					remBtn = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['ext']['Button']('Remove', $p['getattr'](self, 'onRemove'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_172_err){if (!$p['isinstance']($pyjs_dbg_172_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_172_err);}throw $pyjs_dbg_172_err;
}})();
					$pyjs['track']['lineno']=369;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return remBtn['__getitem__']('class')['append']('icon');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_173_err){if (!$p['isinstance']($pyjs_dbg_173_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_173_err);}throw $pyjs_dbg_173_err;
}})();
					$pyjs['track']['lineno']=370;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return remBtn['__getitem__']('class')['append']('cancel');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_174_err){if (!$p['isinstance']($pyjs_dbg_174_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_174_err);}throw $pyjs_dbg_174_err;
}})();
					$pyjs['track']['lineno']=371;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild'](remBtn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_175_err){if (!$p['isinstance']($pyjs_dbg_175_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_175_err);}throw $pyjs_dbg_175_err;
}})();
				}
				$pyjs['track']['lineno']=373;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['fetchEntry']($p['getattr'](self, 'data')['__getitem__']('id'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_176_err){if (!$p['isinstance']($pyjs_dbg_176_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_176_err);}throw $pyjs_dbg_176_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['parent'],['modul'],['data'],['format', '$(name)']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=375;
			$method = $pyjs__bind_method2('fetchEntry', function(id) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					id = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'c842967cd6a1e54b3f5cf623234f0ad0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $add17,$add18;
				$pyjs['track']={'module':'bones.relational', 'lineno':375};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=375;
				$pyjs['track']['lineno']=376;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['NetworkService'], 'request', null, null, [{'successHandler':$p['getattr'](self, 'onSelectionDataAviable'), 'cacheable':true}, $p['getattr'](self, 'modul'), $p['__op_add']($add17='view/',$add18=id)]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_177_err){if (!$p['isinstance']($pyjs_dbg_177_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_177_err);}throw $pyjs_dbg_177_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['id']]);
			$cls_definition['fetchEntry'] = $method;
			$pyjs['track']['lineno']=378;
			$method = $pyjs__bind_method2('onEdit', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'c842967cd6a1e54b3f5cf623234f0ad0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var edwg,pane;
				$pyjs['track']={'module':'bones.relational', 'lineno':378};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=378;
				$pyjs['track']['lineno']=382;
				pane = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['Pane'], null, null, [{'closeable':true, 'iconClasses':(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('modul_%s', $p['getattr']($p['getattr'](self, 'parent'), 'destModul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_179_err){if (!$p['isinstance']($pyjs_dbg_179_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_179_err);}throw $pyjs_dbg_179_err;
}})(), 'apptype_list', 'action_edit']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_180_err){if (!$p['isinstance']($pyjs_dbg_180_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_180_err);}throw $pyjs_dbg_180_err;
}})()}, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Edit');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_178_err){if (!$p['isinstance']($pyjs_dbg_178_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_178_err);}throw $pyjs_dbg_178_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_181_err){if (!$p['isinstance']($pyjs_dbg_181_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_181_err);}throw $pyjs_dbg_181_err;
}})();
				$pyjs['track']['lineno']=384;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['conf']['__getitem__']('mainWindow'), 'stackPane', null, null, [{'focus':true}, pane]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_182_err){if (!$p['isinstance']($pyjs_dbg_182_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_182_err);}throw $pyjs_dbg_182_err;
}})();
				$pyjs['track']['lineno']=385;
				edwg = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['EditWidget'], null, null, [{'key':$p['getattr'](self, 'data')['__getitem__']('id')}, $p['getattr']($p['getattr'](self, 'parent'), 'destModul'), $p['getattr']($m['EditWidget'], 'appList')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_183_err){if (!$p['isinstance']($pyjs_dbg_183_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_183_err);}throw $pyjs_dbg_183_err;
}})();
				$pyjs['track']['lineno']=386;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return pane['addWidget'](edwg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_184_err){if (!$p['isinstance']($pyjs_dbg_184_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_184_err);}throw $pyjs_dbg_184_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['onEdit'] = $method;
			$pyjs['track']['lineno']=388;
			$method = $pyjs__bind_method2('onRemove', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'c842967cd6a1e54b3f5cf623234f0ad0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':388};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=388;
				$pyjs['track']['lineno']=389;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']['removeEntry'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_185_err){if (!$p['isinstance']($pyjs_dbg_185_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_185_err);}throw $pyjs_dbg_185_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['onRemove'] = $method;
			$pyjs['track']['lineno']=391;
			$method = $pyjs__bind_method2('onSelectionDataAviable', function(req) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					req = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'c842967cd6a1e54b3f5cf623234f0ad0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var c,data,$iter4_idx,$pyjs__trackstack_size_1,$iter4_type,$iter4_nextval,$iter4_array,$iter4_iter;
				$pyjs['track']={'module':'bones.relational', 'lineno':391};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=391;
				$pyjs['track']['lineno']=395;
				data = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['decode'](req);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_186_err){if (!$p['isinstance']($pyjs_dbg_186_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_186_err);}throw $pyjs_dbg_186_err;
}})();
				$pyjs['track']['lineno']=396;
				if (!( $p['op_eq']($p['getattr'](self, 'data')['__getitem__']('id'), data['__getitem__']('values')['__getitem__']('id')) )) {
				   throw $p['AssertionError']();
				 }
				$pyjs['track']['lineno']=397;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter4_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__getslice']($p['getattr']($p['getattr'](self, 'selectionTxt'), '_children'), 0, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_187_err){if (!$p['isinstance']($pyjs_dbg_187_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_187_err);}throw $pyjs_dbg_187_err;
}})();
				$iter4_nextval=$p['__iter_prepare']($iter4_iter,false);
				while (typeof($p['__wrapped_next']($iter4_nextval)['$nextval']) != 'undefined') {
					c = $iter4_nextval['$nextval'];
					$pyjs['track']['lineno']=398;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['selectionTxt']['removeChild'](c);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_188_err){if (!$p['isinstance']($pyjs_dbg_188_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_188_err);}throw $pyjs_dbg_188_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=399;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionTxt']['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['TextNode']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['formatString']($p['getattr'](self, 'format'), data['__getitem__']('structure'), data['__getitem__']('values'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_189_err){if (!$p['isinstance']($pyjs_dbg_189_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_189_err);}throw $pyjs_dbg_189_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_190_err){if (!$p['isinstance']($pyjs_dbg_190_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_190_err);}throw $pyjs_dbg_190_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_191_err){if (!$p['isinstance']($pyjs_dbg_191_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_191_err);}throw $pyjs_dbg_191_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['req']]);
			$cls_definition['onSelectionDataAviable'] = $method;
			$pyjs['track']['lineno']=329;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('RelationalMultiSelectionBoneEntry', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=402;
		$m['RelationalMultiSelectionBone'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.relational';
			$cls_definition['__md5__'] = '0f08b2b3daf2bd59a96db56b1998b9a8';
			$pyjs['track']['lineno']=407;
			$method = $pyjs__bind_method2('__init__', function(srcModul, boneName, readOnly, destModul, format) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,5,arguments['length']-1));

					var kwargs = arguments['length'] >= 6 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 5, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					srcModul = arguments[1];
					boneName = arguments[2];
					readOnly = arguments[3];
					destModul = arguments[4];
					format = arguments[5];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,6,arguments['length']-1));

					var kwargs = arguments['length'] >= 7 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 5) $pyjs__exception_func_param(arguments['callee']['__name__'], 5, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0f08b2b3daf2bd59a96db56b1998b9a8') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof format != 'undefined') {
						if (format !== null && typeof format['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = format;
							format = arguments[6];
						}
					} else 					if (typeof destModul != 'undefined') {
						if (destModul !== null && typeof destModul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = destModul;
							destModul = arguments[6];
						}
					} else 					if (typeof readOnly != 'undefined') {
						if (readOnly !== null && typeof readOnly['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = readOnly;
							readOnly = arguments[6];
						}
					} else 					if (typeof boneName != 'undefined') {
						if (boneName !== null && typeof boneName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = boneName;
							boneName = arguments[6];
						}
					} else 					if (typeof srcModul != 'undefined') {
						if (srcModul !== null && typeof srcModul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = srcModul;
							srcModul = arguments[6];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[6];
						}
					} else {
					}
				}
				if (typeof format == 'undefined') format=arguments['callee']['__args__'][7][1];
				var $add20,$or18,$add19,$or17;
				$pyjs['track']={'module':'bones.relational', 'lineno':407};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=407;
				$pyjs['track']['lineno']=420;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['RelationalMultiSelectionBone'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_192_err){if (!$p['isinstance']($pyjs_dbg_192_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_192_err);}throw $pyjs_dbg_192_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_193_err){if (!$p['isinstance']($pyjs_dbg_193_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_193_err);}throw $pyjs_dbg_193_err;
}})();
				$pyjs['track']['lineno']=423;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('srcModul', srcModul) : $p['setattr'](self, 'srcModul', srcModul); 
				$pyjs['track']['lineno']=424;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('boneName', boneName) : $p['setattr'](self, 'boneName', boneName); 
				$pyjs['track']['lineno']=425;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('readOnly', readOnly) : $p['setattr'](self, 'readOnly', readOnly); 
				$pyjs['track']['lineno']=426;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('destModul', destModul) : $p['setattr'](self, 'destModul', destModul); 
				$pyjs['track']['lineno']=427;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('format', format) : $p['setattr'](self, 'format', format); 
				$pyjs['track']['lineno']=428;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('entries', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_194_err){if (!$p['isinstance']($pyjs_dbg_194_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_194_err);}throw $pyjs_dbg_194_err;
}})()) : $p['setattr'](self, 'entries', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_194_err){if (!$p['isinstance']($pyjs_dbg_194_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_194_err);}throw $pyjs_dbg_194_err;
}})()); 
				$pyjs['track']['lineno']=429;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectionDiv', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_195_err){if (!$p['isinstance']($pyjs_dbg_195_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_195_err);}throw $pyjs_dbg_195_err;
}})()) : $p['setattr'](self, 'selectionDiv', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_195_err){if (!$p['isinstance']($pyjs_dbg_195_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_195_err);}throw $pyjs_dbg_195_err;
}})()); 
				$pyjs['track']['lineno']=430;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'selectionDiv')['__getitem__']('class')['append']('selectioncontainer');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_196_err){if (!$p['isinstance']($pyjs_dbg_196_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_196_err);}throw $pyjs_dbg_196_err;
}})();
				$pyjs['track']['lineno']=431;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'selectionDiv'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_197_err){if (!$p['isinstance']($pyjs_dbg_197_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_197_err);}throw $pyjs_dbg_197_err;
}})();
				$pyjs['track']['lineno']=433;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($or17=$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']('root'))?$or17:$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']($p['__op_add']($add19=destModul,$add20='-view'))));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_198_err){if (!$p['isinstance']($pyjs_dbg_198_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_198_err);}throw $pyjs_dbg_198_err;
}})()) {
					$pyjs['track']['lineno']=435;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['ext']['Button']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Select');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_199_err){if (!$p['isinstance']($pyjs_dbg_199_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_199_err);}throw $pyjs_dbg_199_err;
}})(), $p['getattr'](self, 'onShowSelector'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_200_err){if (!$p['isinstance']($pyjs_dbg_200_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_200_err);}throw $pyjs_dbg_200_err;
}})()) : $p['setattr'](self, 'selectBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['ext']['Button']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Select');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_199_err){if (!$p['isinstance']($pyjs_dbg_199_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_199_err);}throw $pyjs_dbg_199_err;
}})(), $p['getattr'](self, 'onShowSelector'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_200_err){if (!$p['isinstance']($pyjs_dbg_200_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_200_err);}throw $pyjs_dbg_200_err;
}})()); 
					$pyjs['track']['lineno']=436;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'selectBtn')['__getitem__']('class')['append']('icon');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_201_err){if (!$p['isinstance']($pyjs_dbg_201_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_201_err);}throw $pyjs_dbg_201_err;
}})();
					$pyjs['track']['lineno']=437;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'selectBtn')['__getitem__']('class')['append']('select');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_202_err){if (!$p['isinstance']($pyjs_dbg_202_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_202_err);}throw $pyjs_dbg_202_err;
}})();
					$pyjs['track']['lineno']=438;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'selectBtn'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_203_err){if (!$p['isinstance']($pyjs_dbg_203_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_203_err);}throw $pyjs_dbg_203_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=440;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectBtn', null) : $p['setattr'](self, 'selectBtn', null); 
				}
				$pyjs['track']['lineno']=442;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'readOnly'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_204_err){if (!$p['isinstance']($pyjs_dbg_204_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_204_err);}throw $pyjs_dbg_204_err;
}})()) {
					$pyjs['track']['lineno']=443;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_205_err){if (!$p['isinstance']($pyjs_dbg_205_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_205_err);}throw $pyjs_dbg_205_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['srcModul'],['boneName'],['readOnly'],['destModul'],['format', '$(name)']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=445;
			$method = $pyjs__bind_method2('_setDisabled', function(disable) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					disable = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0f08b2b3daf2bd59a96db56b1998b9a8') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and13,$and14,$and15;
				$pyjs['track']={'module':'bones.relational', 'lineno':445};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=445;
				$pyjs['track']['lineno']=449;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['RelationalMultiSelectionBone'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_206_err){if (!$p['isinstance']($pyjs_dbg_206_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_206_err);}throw $pyjs_dbg_206_err;
}})()['_setDisabled'](disable);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_207_err){if (!$p['isinstance']($pyjs_dbg_207_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_207_err);}throw $pyjs_dbg_207_err;
}})();
				$pyjs['track']['lineno']=450;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and13=!$p['bool'](disable))?($p['bool']($and14=!$p['bool']($p['getattr'](self, '_disabledState')))?(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_208_err){if (!$p['isinstance']($pyjs_dbg_208_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_208_err);}throw $pyjs_dbg_208_err;
}})()['__getitem__']('class')['__contains__']('is_active'):$and14):$and13));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_209_err){if (!$p['isinstance']($pyjs_dbg_209_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_209_err);}throw $pyjs_dbg_209_err;
}})()) {
					$pyjs['track']['lineno']=451;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_210_err){if (!$p['isinstance']($pyjs_dbg_210_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_210_err);}throw $pyjs_dbg_210_err;
}})()['__getitem__']('class')['remove']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_211_err){if (!$p['isinstance']($pyjs_dbg_211_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_211_err);}throw $pyjs_dbg_211_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['disable']]);
			$cls_definition['_setDisabled'] = $method;
			$pyjs['track']['lineno']=455;
			$method = $pyjs__bind_method2('fromSkelStructure', function(modulName, boneName, skelStructure) {
    if ($pyjs['options']['arg_is_instance'] && this['__is_instance__'] !== true && this['__is_instance__'] !== false) $pyjs__exception_func_class_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__']);
    if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 3, arguments['length']);
    var cls = this['prototype'];
				var multiple,format,destModul,readOnly,$and16,$and17;
				$pyjs['track']={'module':'bones.relational', 'lineno':455};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=455;
				$pyjs['track']['lineno']=465;
				readOnly = ($p['bool']($and16=(function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_212_err){if (!$p['isinstance']($pyjs_dbg_212_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_212_err);}throw $pyjs_dbg_212_err;
}})()['__contains__']('readonly'))?skelStructure['__getitem__'](boneName)['__getitem__']('readonly'):$and16);
				$pyjs['track']['lineno']=466;
				multiple = skelStructure['__getitem__'](boneName)['__getitem__']('multiple');
				$pyjs['track']['lineno']=467;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_213_err){if (!$p['isinstance']($pyjs_dbg_213_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_213_err);}throw $pyjs_dbg_213_err;
}})()['__contains__']('modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_214_err){if (!$p['isinstance']($pyjs_dbg_214_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_214_err);}throw $pyjs_dbg_214_err;
}})()) {
					$pyjs['track']['lineno']=468;
					destModul = skelStructure['__getitem__'](boneName)['__getitem__']('modul');
				}
				else {
					$pyjs['track']['lineno']=470;
					destModul = (function(){try{try{$pyjs['in_try_except'] += 1;
					return skelStructure['__getitem__'](boneName)['__getitem__']('type')['$$split']('.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_215_err){if (!$p['isinstance']($pyjs_dbg_215_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_215_err);}throw $pyjs_dbg_215_err;
}})()['__getitem__']($constant_int_1);
				}
				$pyjs['track']['lineno']=471;
				format = '$(name)';
				$pyjs['track']['lineno']=472;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_216_err){if (!$p['isinstance']($pyjs_dbg_216_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_216_err);}throw $pyjs_dbg_216_err;
}})()['__contains__']('format'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_217_err){if (!$p['isinstance']($pyjs_dbg_217_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_217_err);}throw $pyjs_dbg_217_err;
}})()) {
					$pyjs['track']['lineno']=473;
					format = skelStructure['__getitem__'](boneName)['__getitem__']('format');
				}
				$pyjs['track']['lineno']=474;
				$pyjs['track']['lineno']=474;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, cls, null, null, [{'destModul':destModul, 'format':format}, modulName, boneName, readOnly]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_218_err){if (!$p['isinstance']($pyjs_dbg_218_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_218_err);}throw $pyjs_dbg_218_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 2, [null,null,['cls'],['modulName'],['boneName'],['skelStructure']]);
			$cls_definition['fromSkelStructure'] = $method;
			$pyjs['track']['lineno']=476;
			$method = $pyjs__bind_method2('unserialize', function(data) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0f08b2b3daf2bd59a96db56b1998b9a8') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var val;
				$pyjs['track']={'module':'bones.relational', 'lineno':476};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=476;
				$pyjs['track']['lineno']=482;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_219_err){if (!$p['isinstance']($pyjs_dbg_219_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_219_err);}throw $pyjs_dbg_219_err;
}})()['__contains__']($p['getattr'](self, 'boneName')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_220_err){if (!$p['isinstance']($pyjs_dbg_220_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_220_err);}throw $pyjs_dbg_220_err;
}})()) {
					$pyjs['track']['lineno']=483;
					val = data['__getitem__']($p['getattr'](self, 'boneName'));
					$pyjs['track']['lineno']=484;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](val, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_221_err){if (!$p['isinstance']($pyjs_dbg_221_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_221_err);}throw $pyjs_dbg_221_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_222_err){if (!$p['isinstance']($pyjs_dbg_222_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_222_err);}throw $pyjs_dbg_222_err;
}})()) {
						$pyjs['track']['lineno']=485;
						val = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['list']([val]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_223_err){if (!$p['isinstance']($pyjs_dbg_223_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_223_err);}throw $pyjs_dbg_223_err;
}})();
					}
					$pyjs['track']['lineno']=486;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['setSelection'](val);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_224_err){if (!$p['isinstance']($pyjs_dbg_224_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_224_err);}throw $pyjs_dbg_224_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['data']]);
			$cls_definition['unserialize'] = $method;
			$pyjs['track']['lineno']=490;
			$method = $pyjs__bind_method2('serializeForPost', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0f08b2b3daf2bd59a96db56b1998b9a8') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':490};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=490;
				$pyjs['track']['lineno']=495;
				$pyjs['track']['lineno']=495;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([[$p['getattr'](self, 'boneName'), function(){
					var $iter5_nextval,x,$iter5_idx,$pyjs__trackstack_size_1,$collcomp3,$iter5_iter,$iter5_array,$iter5_type;
	$collcomp3 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter5_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'entries');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_225_err){if (!$p['isinstance']($pyjs_dbg_225_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_225_err);}throw $pyjs_dbg_225_err;
}})();
				$iter5_nextval=$p['__iter_prepare']($iter5_iter,false);
				while (typeof($p['__wrapped_next']($iter5_nextval)['$nextval']) != 'undefined') {
					x = $iter5_nextval['$nextval'];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp3['append']($p['getattr'](x, 'data')['__getitem__']('id'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_226_err){if (!$p['isinstance']($pyjs_dbg_226_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_226_err);}throw $pyjs_dbg_226_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='bones.relational';

	return $collcomp3;}()]]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_227_err){if (!$p['isinstance']($pyjs_dbg_227_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_227_err);}throw $pyjs_dbg_227_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['serializeForPost'] = $method;
			$pyjs['track']['lineno']=497;
			$method = $pyjs__bind_method2('serializeForDocument', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0f08b2b3daf2bd59a96db56b1998b9a8') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':497};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=497;
				$pyjs['track']['lineno']=498;
				$pyjs['track']['lineno']=498;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['serialize']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_228_err){if (!$p['isinstance']($pyjs_dbg_228_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_228_err);}throw $pyjs_dbg_228_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['serializeForDocument'] = $method;
			$pyjs['track']['lineno']=500;
			$method = $pyjs__bind_method2('onShowSelector', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0f08b2b3daf2bd59a96db56b1998b9a8') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var currentSelector;
				$pyjs['track']={'module':'bones.relational', 'lineno':500};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=500;
				$pyjs['track']['lineno']=504;
				currentSelector = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['ListWidget'], null, null, [{'isSelector':true}, $p['getattr'](self, 'destModul')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_229_err){if (!$p['isinstance']($pyjs_dbg_229_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_229_err);}throw $pyjs_dbg_229_err;
}})();
				$pyjs['track']['lineno']=505;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return currentSelector['selectionActivatedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_230_err){if (!$p['isinstance']($pyjs_dbg_230_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_230_err);}throw $pyjs_dbg_230_err;
}})();
				$pyjs['track']['lineno']=506;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['stackWidget'](currentSelector);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_231_err){if (!$p['isinstance']($pyjs_dbg_231_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_231_err);}throw $pyjs_dbg_231_err;
}})();
				$pyjs['track']['lineno']=507;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_232_err){if (!$p['isinstance']($pyjs_dbg_232_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_232_err);}throw $pyjs_dbg_232_err;
}})()['__getitem__']('class')['append']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_233_err){if (!$p['isinstance']($pyjs_dbg_233_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_233_err);}throw $pyjs_dbg_233_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['onShowSelector'] = $method;
			$pyjs['track']['lineno']=509;
			$method = $pyjs__bind_method2('onSelectionActivated', function(table, selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					selection = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0f08b2b3daf2bd59a96db56b1998b9a8') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':509};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=509;
				$pyjs['track']['lineno']=513;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['setSelection'](selection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_234_err){if (!$p['isinstance']($pyjs_dbg_234_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_234_err);}throw $pyjs_dbg_234_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['selection']]);
			$cls_definition['onSelectionActivated'] = $method;
			$pyjs['track']['lineno']=515;
			$method = $pyjs__bind_method2('setSelection', function(selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					selection = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0f08b2b3daf2bd59a96db56b1998b9a8') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter6_idx,$iter6_type,$iter6_array,$pyjs__trackstack_size_1,entry,$iter6_iter,data,$iter6_nextval;
				$pyjs['track']={'module':'bones.relational', 'lineno':515};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=515;
				$pyjs['track']['lineno']=521;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['printFunc']([selection], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_235_err){if (!$p['isinstance']($pyjs_dbg_235_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_235_err);}throw $pyjs_dbg_235_err;
}})();
				$pyjs['track']['lineno']=522;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](selection, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_236_err){if (!$p['isinstance']($pyjs_dbg_236_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_236_err);}throw $pyjs_dbg_236_err;
}})()) {
					$pyjs['track']['lineno']=523;
					$pyjs['track']['lineno']=523;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=524;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter6_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return selection;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_237_err){if (!$p['isinstance']($pyjs_dbg_237_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_237_err);}throw $pyjs_dbg_237_err;
}})();
				$iter6_nextval=$p['__iter_prepare']($iter6_iter,false);
				while (typeof($p['__wrapped_next']($iter6_nextval)['$nextval']) != 'undefined') {
					data = $iter6_nextval['$nextval'];
					$pyjs['track']['lineno']=525;
					entry = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['RelationalMultiSelectionBoneEntry'](self, $p['getattr'](self, 'destModul'), data, $p['getattr'](self, 'format'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_238_err){if (!$p['isinstance']($pyjs_dbg_238_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_238_err);}throw $pyjs_dbg_238_err;
}})();
					$pyjs['track']['lineno']=526;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['addEntry'](entry);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_239_err){if (!$p['isinstance']($pyjs_dbg_239_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_239_err);}throw $pyjs_dbg_239_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='bones.relational';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['selection']]);
			$cls_definition['setSelection'] = $method;
			$pyjs['track']['lineno']=528;
			$method = $pyjs__bind_method2('addEntry', function(entry) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					entry = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0f08b2b3daf2bd59a96db56b1998b9a8') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':528};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=528;
				$pyjs['track']['lineno']=533;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['entries']['append'](entry);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_240_err){if (!$p['isinstance']($pyjs_dbg_240_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_240_err);}throw $pyjs_dbg_240_err;
}})();
				$pyjs['track']['lineno']=534;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionDiv']['appendChild'](entry);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_241_err){if (!$p['isinstance']($pyjs_dbg_241_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_241_err);}throw $pyjs_dbg_241_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['entry']]);
			$cls_definition['addEntry'] = $method;
			$pyjs['track']['lineno']=536;
			$method = $pyjs__bind_method2('removeEntry', function(entry) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					entry = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0f08b2b3daf2bd59a96db56b1998b9a8') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':536};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=536;
				$pyjs['track']['lineno']=541;
				if (!( $p['getattr'](self, 'entries')['__contains__'](entry) )) {
				   throw $p['AssertionError']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('Cannot remove unknown entry %s from realtionalBone', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['str'](entry);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_242_err){if (!$p['isinstance']($pyjs_dbg_242_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_242_err);}throw $pyjs_dbg_242_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_243_err){if (!$p['isinstance']($pyjs_dbg_243_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_243_err);}throw $pyjs_dbg_243_err;
}})());
				 }
				$pyjs['track']['lineno']=542;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionDiv']['removeChild'](entry);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_244_err){if (!$p['isinstance']($pyjs_dbg_244_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_244_err);}throw $pyjs_dbg_244_err;
}})();
				$pyjs['track']['lineno']=543;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['entries']['remove'](entry);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_245_err){if (!$p['isinstance']($pyjs_dbg_245_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_245_err);}throw $pyjs_dbg_245_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['entry']]);
			$cls_definition['removeEntry'] = $method;
			$pyjs['track']['lineno']=545;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0f08b2b3daf2bd59a96db56b1998b9a8') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':545};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=545;
				$pyjs['track']['lineno']=546;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['RelationalMultiSelectionBone'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_246_err){if (!$p['isinstance']($pyjs_dbg_246_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_246_err);}throw $pyjs_dbg_246_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_247_err){if (!$p['isinstance']($pyjs_dbg_247_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_247_err);}throw $pyjs_dbg_247_err;
}})();
				$pyjs['track']['lineno']=547;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['registerChangeListener'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_248_err){if (!$p['isinstance']($pyjs_dbg_248_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_248_err);}throw $pyjs_dbg_248_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=549;
			$method = $pyjs__bind_method2('onDetach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0f08b2b3daf2bd59a96db56b1998b9a8') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':549};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=549;
				$pyjs['track']['lineno']=550;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['removeChangeListener'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_249_err){if (!$p['isinstance']($pyjs_dbg_249_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_249_err);}throw $pyjs_dbg_249_err;
}})();
				$pyjs['track']['lineno']=551;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['RelationalMultiSelectionBone'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_250_err){if (!$p['isinstance']($pyjs_dbg_250_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_250_err);}throw $pyjs_dbg_250_err;
}})()['onDetach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_251_err){if (!$p['isinstance']($pyjs_dbg_251_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_251_err);}throw $pyjs_dbg_251_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onDetach'] = $method;
			$pyjs['track']['lineno']=553;
			$method = $pyjs__bind_method2('onDataChanged', function(modul) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					modul = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0f08b2b3daf2bd59a96db56b1998b9a8') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var selection,$iter8_idx,$iter8_array,$iter8_iter,$iter8_nextval,$pyjs__trackstack_size_1,$iter8_type,e;
				$pyjs['track']={'module':'bones.relational', 'lineno':553};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=553;
				$pyjs['track']['lineno']=554;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq'](modul, $p['getattr'](self, 'destModul')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_252_err){if (!$p['isinstance']($pyjs_dbg_252_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_252_err);}throw $pyjs_dbg_252_err;
}})()) {
					$pyjs['track']['lineno']=555;
					selection = function(){
						var $iter7_array,$iter7_nextval,$iter7_iter,$collcomp4,$iter7_idx,x,$iter7_type,$pyjs__trackstack_size_1;
	$collcomp4 = $p['list']();
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter7_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'entries');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_253_err){if (!$p['isinstance']($pyjs_dbg_253_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_253_err);}throw $pyjs_dbg_253_err;
}})();
					$iter7_nextval=$p['__iter_prepare']($iter7_iter,false);
					while (typeof($p['__wrapped_next']($iter7_nextval)['$nextval']) != 'undefined') {
						x = $iter7_nextval['$nextval'];
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $collcomp4['append']($p['getattr'](x, 'data'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_254_err){if (!$p['isinstance']($pyjs_dbg_254_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_254_err);}throw $pyjs_dbg_254_err;
}})();
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.relational';

	return $collcomp4;}();
					$pyjs['track']['lineno']=556;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter8_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['__getslice']($p['getattr'](self, 'entries'), 0, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_255_err){if (!$p['isinstance']($pyjs_dbg_255_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_255_err);}throw $pyjs_dbg_255_err;
}})();
					$iter8_nextval=$p['__iter_prepare']($iter8_iter,false);
					while (typeof($p['__wrapped_next']($iter8_nextval)['$nextval']) != 'undefined') {
						e = $iter8_nextval['$nextval'];
						$pyjs['track']['lineno']=557;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['removeEntry'](e);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_256_err){if (!$p['isinstance']($pyjs_dbg_256_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_256_err);}throw $pyjs_dbg_256_err;
}})();
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.relational';
					$pyjs['track']['lineno']=558;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['setSelection'](selection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_257_err){if (!$p['isinstance']($pyjs_dbg_257_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_257_err);}throw $pyjs_dbg_257_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['modul']]);
			$cls_definition['onDataChanged'] = $method;
			$pyjs['track']['lineno']=402;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('RelationalMultiSelectionBone', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=560;
		$m['ExtendedRelationalSearch'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.relational';
			$cls_definition['__md5__'] = '06cf2149e4b4fc199a0d5727daa5dc39';
			$pyjs['track']['lineno']=561;
			$method = $pyjs__bind_method2('__init__', function(extension, view, modul) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

					var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					extension = arguments[1];
					view = arguments[2];
					modul = arguments[3];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,4,arguments['length']-1));

					var kwargs = arguments['length'] >= 5 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '06cf2149e4b4fc199a0d5727daa5dc39') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof modul != 'undefined') {
						if (modul !== null && typeof modul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modul;
							modul = arguments[4];
						}
					} else 					if (typeof view != 'undefined') {
						if (view !== null && typeof view['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = view;
							view = arguments[4];
						}
					} else 					if (typeof extension != 'undefined') {
						if (extension !== null && typeof extension['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = extension;
							extension = arguments[4];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[4];
						}
					} else {
					}
				}
				var tmpSpan,btn;
				$pyjs['track']={'module':'bones.relational', 'lineno':561};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=561;
				$pyjs['track']['lineno']=562;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ExtendedRelationalSearch'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_258_err){if (!$p['isinstance']($pyjs_dbg_258_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_258_err);}throw $pyjs_dbg_258_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_259_err){if (!$p['isinstance']($pyjs_dbg_259_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_259_err);}throw $pyjs_dbg_259_err;
}})();
				$pyjs['track']['lineno']=563;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('view', view) : $p['setattr'](self, 'view', view); 
				$pyjs['track']['lineno']=564;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('extension', extension) : $p['setattr'](self, 'extension', extension); 
				$pyjs['track']['lineno']=565;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=566;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentSelection', null) : $p['setattr'](self, 'currentSelection', null); 
				$pyjs['track']['lineno']=567;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('filterChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('filterChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_260_err){if (!$p['isinstance']($pyjs_dbg_260_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_260_err);}throw $pyjs_dbg_260_err;
}})()) : $p['setattr'](self, 'filterChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('filterChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_260_err){if (!$p['isinstance']($pyjs_dbg_260_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_260_err);}throw $pyjs_dbg_260_err;
}})()); 
				$pyjs['track']['lineno']=568;
				tmpSpan = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_261_err){if (!$p['isinstance']($pyjs_dbg_261_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_261_err);}throw $pyjs_dbg_261_err;
}})();
				$pyjs['track']['lineno']=569;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return tmpSpan['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['TextNode'](extension['__getitem__']('name'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_262_err){if (!$p['isinstance']($pyjs_dbg_262_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_262_err);}throw $pyjs_dbg_262_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_263_err){if (!$p['isinstance']($pyjs_dbg_263_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_263_err);}throw $pyjs_dbg_263_err;
}})();
				$pyjs['track']['lineno']=570;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](tmpSpan);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_264_err){if (!$p['isinstance']($pyjs_dbg_264_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_264_err);}throw $pyjs_dbg_264_err;
}})();
				$pyjs['track']['lineno']=571;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentEntry', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_265_err){if (!$p['isinstance']($pyjs_dbg_265_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_265_err);}throw $pyjs_dbg_265_err;
}})()) : $p['setattr'](self, 'currentEntry', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_265_err){if (!$p['isinstance']($pyjs_dbg_265_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_265_err);}throw $pyjs_dbg_265_err;
}})()); 
				$pyjs['track']['lineno']=573;
				btn = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['ext']['Button']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Select');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_266_err){if (!$p['isinstance']($pyjs_dbg_266_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_266_err);}throw $pyjs_dbg_266_err;
}})(), $p['getattr'](self, 'openSelector'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_267_err){if (!$p['isinstance']($pyjs_dbg_267_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_267_err);}throw $pyjs_dbg_267_err;
}})();
				$pyjs['track']['lineno']=574;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return btn['__getitem__']('class')['append']('icon select');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_268_err){if (!$p['isinstance']($pyjs_dbg_268_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_268_err);}throw $pyjs_dbg_268_err;
}})();
				$pyjs['track']['lineno']=575;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](btn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_269_err){if (!$p['isinstance']($pyjs_dbg_269_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_269_err);}throw $pyjs_dbg_269_err;
}})();
				$pyjs['track']['lineno']=576;
				btn = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['ext']['Button']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['translate']('Clear');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_270_err){if (!$p['isinstance']($pyjs_dbg_270_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_270_err);}throw $pyjs_dbg_270_err;
}})(), $p['getattr'](self, 'clearSelection'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_271_err){if (!$p['isinstance']($pyjs_dbg_271_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_271_err);}throw $pyjs_dbg_271_err;
}})();
				$pyjs['track']['lineno']=577;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return btn['__getitem__']('class')['append']('icon cancel');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_272_err){if (!$p['isinstance']($pyjs_dbg_272_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_272_err);}throw $pyjs_dbg_272_err;
}})();
				$pyjs['track']['lineno']=578;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](btn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_273_err){if (!$p['isinstance']($pyjs_dbg_273_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_273_err);}throw $pyjs_dbg_273_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['extension'],['view'],['modul']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=580;
			$method = $pyjs__bind_method2('clearSelection', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '06cf2149e4b4fc199a0d5727daa5dc39') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':580};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=580;
				$pyjs['track']['lineno']=581;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentSelection', null) : $p['setattr'](self, 'currentSelection', null); 
				$pyjs['track']['lineno']=582;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['filterChangedEvent']['fire']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_274_err){if (!$p['isinstance']($pyjs_dbg_274_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_274_err);}throw $pyjs_dbg_274_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['clearSelection'] = $method;
			$pyjs['track']['lineno']=584;
			$method = $pyjs__bind_method2('openSelector', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '06cf2149e4b4fc199a0d5727daa5dc39') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var currentSelector;
				$pyjs['track']={'module':'bones.relational', 'lineno':584};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=584;
				$pyjs['track']['lineno']=585;
				currentSelector = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['ListWidget'], null, null, [{'isSelector':true}, $p['getattr'](self, 'extension')['__getitem__']('modul')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_275_err){if (!$p['isinstance']($pyjs_dbg_275_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_275_err);}throw $pyjs_dbg_275_err;
}})();
				$pyjs['track']['lineno']=586;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return currentSelector['selectionActivatedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_276_err){if (!$p['isinstance']($pyjs_dbg_276_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_276_err);}throw $pyjs_dbg_276_err;
}})();
				$pyjs['track']['lineno']=587;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['stackWidget'](currentSelector);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_277_err){if (!$p['isinstance']($pyjs_dbg_277_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_277_err);}throw $pyjs_dbg_277_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['openSelector'] = $method;
			$pyjs['track']['lineno']=589;
			$method = $pyjs__bind_method2('onSelectionActivated', function(table, selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					selection = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '06cf2149e4b4fc199a0d5727daa5dc39') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.relational', 'lineno':589};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=589;
				$pyjs['track']['lineno']=590;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentSelection', selection) : $p['setattr'](self, 'currentSelection', selection); 
				$pyjs['track']['lineno']=591;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['filterChangedEvent']['fire']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_278_err){if (!$p['isinstance']($pyjs_dbg_278_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_278_err);}throw $pyjs_dbg_278_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['selection']]);
			$cls_definition['onSelectionActivated'] = $method;
			$pyjs['track']['lineno']=594;
			$method = $pyjs__bind_method2('updateFilter', function(filter) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					filter = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '06cf2149e4b4fc199a0d5727daa5dc39') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $add21,$add22,newId;
				$pyjs['track']={'module':'bones.relational', 'lineno':594};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=594;
				$pyjs['track']['lineno']=595;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'currentSelection'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_279_err){if (!$p['isinstance']($pyjs_dbg_279_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_279_err);}throw $pyjs_dbg_279_err;
}})()) {
					$pyjs['track']['lineno']=596;
					$p['getattr']($p['getattr'](self, 'currentEntry'), 'element')['__is_instance__'] && typeof $p['getattr']($p['getattr'](self, 'currentEntry'), 'element')['__setattr__'] == 'function' ? $p['getattr']($p['getattr'](self, 'currentEntry'), 'element')['__setattr__']('innerHTML', $p['getattr'](self, 'currentSelection')['__getitem__']($constant_int_0)['__getitem__']('name')) : $p['setattr']($p['getattr']($p['getattr'](self, 'currentEntry'), 'element'), 'innerHTML', $p['getattr'](self, 'currentSelection')['__getitem__']($constant_int_0)['__getitem__']('name')); 
					$pyjs['track']['lineno']=597;
					newId = $p['getattr'](self, 'currentSelection')['__getitem__']($constant_int_0)['__getitem__']('id');
					$pyjs['track']['lineno']=598;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return filter['__setitem__']($p['__op_add']($add21=$p['getattr'](self, 'extension')['__getitem__']('target'),$add22='.id'), newId);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_280_err){if (!$p['isinstance']($pyjs_dbg_280_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_280_err);}throw $pyjs_dbg_280_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=600;
					$p['getattr']($p['getattr'](self, 'currentEntry'), 'element')['__is_instance__'] && typeof $p['getattr']($p['getattr'](self, 'currentEntry'), 'element')['__setattr__'] == 'function' ? $p['getattr']($p['getattr'](self, 'currentEntry'), 'element')['__setattr__']('innerHTML', '') : $p['setattr']($p['getattr']($p['getattr'](self, 'currentEntry'), 'element'), 'innerHTML', ''); 
				}
				$pyjs['track']['lineno']=601;
				$pyjs['track']['lineno']=601;
				var $pyjs__ret = filter;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['filter']]);
			$cls_definition['updateFilter'] = $method;
			$pyjs['track']['lineno']=604;
			$method = $pyjs__bind_method2('canHandleExtension', function(extension, view, modul) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var $and20,$or20,$and18,$and19,$or19;
				$pyjs['track']={'module':'bones.relational', 'lineno':604};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.relational';
				$pyjs['track']['lineno']=604;
				$pyjs['track']['lineno']=605;
				$pyjs['track']['lineno']=605;
				var $pyjs__ret = ($p['bool']($and18=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](extension, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_281_err){if (!$p['isinstance']($pyjs_dbg_281_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_281_err);}throw $pyjs_dbg_281_err;
}})())?($p['bool']($and19=(function(){try{try{$pyjs['in_try_except'] += 1;
				return extension['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_282_err){if (!$p['isinstance']($pyjs_dbg_282_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_282_err);}throw $pyjs_dbg_282_err;
}})()['__contains__']('type'))?($p['bool']($or19=$p['op_eq'](extension['__getitem__']('type'), 'relational'))?$or19:(function(){try{try{$pyjs['in_try_except'] += 1;
				return extension['__getitem__']('type')['startswith']('relational.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_283_err){if (!$p['isinstance']($pyjs_dbg_283_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_283_err);}throw $pyjs_dbg_283_err;
}})()):$and19):$and18);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['extension'],['view'],['modul']]);
			$cls_definition['canHandleExtension'] = $method;
			$pyjs['track']['lineno']=560;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('ExtendedRelationalSearch', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=607;
		$m['CheckForRelationalBoneSingleSelection'] = function(modulName, boneName, skelStructure) {
			if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, null, arguments['length']);
			var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

			var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
			if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
				if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
				kwargs = arguments[arguments['length']+1];
			} else {
				delete kwargs['$pyjs_is_kwarg'];
			}
			if (typeof kwargs == 'undefined') {
				kwargs = $p['__empty_dict']();
				if (typeof skelStructure != 'undefined') {
					if (skelStructure !== null && typeof skelStructure['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = skelStructure;
						skelStructure = arguments[3];
					}
				} else 				if (typeof boneName != 'undefined') {
					if (boneName !== null && typeof boneName['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = boneName;
						boneName = arguments[3];
					}
				} else 				if (typeof modulName != 'undefined') {
					if (modulName !== null && typeof modulName['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = modulName;
						modulName = arguments[3];
					}
				} else {
				}
			}
			var $and23,$and22,$and21,isMultiple,$and24;
			$pyjs['track']={'module':'bones.relational','lineno':607};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='bones.relational';
			$pyjs['track']['lineno']=607;
			$pyjs['track']['lineno']=608;
			isMultiple = ($p['bool']($and21=(function(){try{try{$pyjs['in_try_except'] += 1;
			return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_284_err){if (!$p['isinstance']($pyjs_dbg_284_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_284_err);}throw $pyjs_dbg_284_err;
}})()['__contains__']('multiple'))?skelStructure['__getitem__'](boneName)['__getitem__']('multiple'):$and21);
			$pyjs['track']['lineno']=609;
			$pyjs['track']['lineno']=609;
			var $pyjs__ret = ($p['bool']($and23=(function(){try{try{$pyjs['in_try_except'] += 1;
			return (typeof CheckForRelationalBone == "undefined"?$m['CheckForRelationalBone']:CheckForRelationalBone)(modulName, boneName, skelStructure);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_285_err){if (!$p['isinstance']($pyjs_dbg_285_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_285_err);}throw $pyjs_dbg_285_err;
}})())?!$p['bool'](isMultiple):$and23);
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['CheckForRelationalBoneSingleSelection']['__name__'] = 'CheckForRelationalBoneSingleSelection';

		$m['CheckForRelationalBoneSingleSelection']['__bind_type__'] = 0;
		$m['CheckForRelationalBoneSingleSelection']['__args__'] = ['args',['kwargs'],['modulName'],['boneName'],['skelStructure']];
		$pyjs['track']['lineno']=611;
		$m['CheckForRelationalBoneMultiSelection'] = function(modulName, boneName, skelStructure) {
			if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, null, arguments['length']);
			var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

			var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
			if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
				if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
				kwargs = arguments[arguments['length']+1];
			} else {
				delete kwargs['$pyjs_is_kwarg'];
			}
			if (typeof kwargs == 'undefined') {
				kwargs = $p['__empty_dict']();
				if (typeof skelStructure != 'undefined') {
					if (skelStructure !== null && typeof skelStructure['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = skelStructure;
						skelStructure = arguments[3];
					}
				} else 				if (typeof boneName != 'undefined') {
					if (boneName !== null && typeof boneName['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = boneName;
						boneName = arguments[3];
					}
				} else 				if (typeof modulName != 'undefined') {
					if (modulName !== null && typeof modulName['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = modulName;
						modulName = arguments[3];
					}
				} else {
				}
			}
			var $and28,isMultiple,$and27,$and26,$and25;
			$pyjs['track']={'module':'bones.relational','lineno':611};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='bones.relational';
			$pyjs['track']['lineno']=611;
			$pyjs['track']['lineno']=612;
			isMultiple = ($p['bool']($and25=(function(){try{try{$pyjs['in_try_except'] += 1;
			return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_286_err){if (!$p['isinstance']($pyjs_dbg_286_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_286_err);}throw $pyjs_dbg_286_err;
}})()['__contains__']('multiple'))?skelStructure['__getitem__'](boneName)['__getitem__']('multiple'):$and25);
			$pyjs['track']['lineno']=613;
			$pyjs['track']['lineno']=613;
			var $pyjs__ret = ($p['bool']($and27=(function(){try{try{$pyjs['in_try_except'] += 1;
			return (typeof CheckForRelationalBone == "undefined"?$m['CheckForRelationalBone']:CheckForRelationalBone)(modulName, boneName, skelStructure);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_287_err){if (!$p['isinstance']($pyjs_dbg_287_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_287_err);}throw $pyjs_dbg_287_err;
}})())?isMultiple:$and27);
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['CheckForRelationalBoneMultiSelection']['__name__'] = 'CheckForRelationalBoneMultiSelection';

		$m['CheckForRelationalBoneMultiSelection']['__bind_type__'] = 0;
		$m['CheckForRelationalBoneMultiSelection']['__args__'] = ['args',['kwargs'],['modulName'],['boneName'],['skelStructure']];
		$pyjs['track']['lineno']=615;
		$m['CheckForRelationalBone'] = function(modulName, boneName, skelStucture) {
			if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, null, arguments['length']);
			var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

			var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
			if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
				if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
				kwargs = arguments[arguments['length']+1];
			} else {
				delete kwargs['$pyjs_is_kwarg'];
			}
			if (typeof kwargs == 'undefined') {
				kwargs = $p['__empty_dict']();
				if (typeof skelStucture != 'undefined') {
					if (skelStucture !== null && typeof skelStucture['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = skelStucture;
						skelStucture = arguments[3];
					}
				} else 				if (typeof boneName != 'undefined') {
					if (boneName !== null && typeof boneName['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = boneName;
						boneName = arguments[3];
					}
				} else 				if (typeof modulName != 'undefined') {
					if (modulName !== null && typeof modulName['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = modulName;
						modulName = arguments[3];
					}
				} else {
				}
			}

			$pyjs['track']={'module':'bones.relational','lineno':615};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='bones.relational';
			$pyjs['track']['lineno']=615;
			$pyjs['track']['lineno']=616;
			$pyjs['track']['lineno']=616;
			var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
			return skelStucture['__getitem__'](boneName)['__getitem__']('type')['startswith']('relational.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_288_err){if (!$p['isinstance']($pyjs_dbg_288_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_288_err);}throw $pyjs_dbg_288_err;
}})();
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['CheckForRelationalBone']['__name__'] = 'CheckForRelationalBone';

		$m['CheckForRelationalBone']['__bind_type__'] = 0;
		$m['CheckForRelationalBone']['__args__'] = ['args',['kwargs'],['modulName'],['boneName'],['skelStucture']];
		$pyjs['track']['lineno']=619;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['editBoneSelector']['insert']($constant_int_3, $m['CheckForRelationalBoneSingleSelection'], $m['RelationalSingleSelectionBone']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_289_err){if (!$p['isinstance']($pyjs_dbg_289_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_289_err);}throw $pyjs_dbg_289_err;
}})();
		$pyjs['track']['lineno']=620;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['editBoneSelector']['insert']($constant_int_3, $m['CheckForRelationalBoneMultiSelection'], $m['RelationalMultiSelectionBone']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_290_err){if (!$p['isinstance']($pyjs_dbg_290_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_290_err);}throw $pyjs_dbg_290_err;
}})();
		$pyjs['track']['lineno']=621;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['viewDelegateSelector']['insert']($constant_int_3, $m['CheckForRelationalBone'], $m['RelationalViewBoneDelegate']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_291_err){if (!$p['isinstance']($pyjs_dbg_291_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_291_err);}throw $pyjs_dbg_291_err;
}})();
		$pyjs['track']['lineno']=622;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['extendedSearchWidgetSelector']['insert']($constant_int_1, $p['getattr']($m['ExtendedRelationalSearch'], 'canHandleExtension'), $m['ExtendedRelationalSearch']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_292_err){if (!$p['isinstance']($pyjs_dbg_292_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_292_err);}throw $pyjs_dbg_292_err;
}})();
		$pyjs['track']['lineno']=623;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['extractorDelegateSelector']['insert']($constant_int_3, $m['CheckForRelationalBone'], $m['RelationalBoneExtractor']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_293_err){if (!$p['isinstance']($pyjs_dbg_293_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_293_err);}throw $pyjs_dbg_293_err;
}})();
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end bones.relational */


/* end module: bones.relational */


/*
PYJS_DEPS: ['os.path', 'os', 're', 'html5', 'priorityqueue.editBoneSelector', 'priorityqueue', 'priorityqueue.viewDelegateSelector', 'priorityqueue.extendedSearchWidgetSelector', 'priorityqueue.extractorDelegateSelector', 'event.EventDispatcher', 'event', 'utils.formatString', 'utils', 'widgets.list.ListWidget', 'widgets', 'widgets.list', 'config.conf', 'config', 'i18n.translate', 'i18n', 'network.NetworkService', 'network', 'widgets.edit.EditWidget', 'widgets.edit', 'pane.Pane', 'pane']
*/
