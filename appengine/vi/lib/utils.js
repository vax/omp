/* start module: utils */
$pyjs['loaded_modules']['utils'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['utils']['__was_initialized__']) return $pyjs['loaded_modules']['utils'];
	var $m = $pyjs['loaded_modules']['utils'];
	$m['__repr__'] = function() { return '<module: utils>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'utils';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	try {
		$m.__track_lines__[1] = 'utils.py, line 1:\n    import html5';
		$m.__track_lines__[2] = 'utils.py, line 2:\n    from config import conf';
		$m.__track_lines__[131] = 'utils.py, line 131:\n    if event.target==widget.element:';
		$m.__track_lines__[4] = 'utils.py, line 4:\n    def formatString( format, skelStructure, data, prefix=None, unescape=False ):';
		$m.__track_lines__[133] = 'utils.py, line 133:\n    for child in widget._children:';
		$m.__track_lines__[134] = 'utils.py, line 134:\n    if doesEventHitWidgetOrChildren( event, child ):';
		$m.__track_lines__[135] = 'utils.py, line 135:\n    return( True )';
		$m.__track_lines__[136] = 'utils.py, line 136:\n    return( False )';
		$m.__track_lines__[138] = 'utils.py, line 138:\n    def getImagePreview( data ):';
		$m.__track_lines__[139] = 'utils.py, line 139:\n    if "mimetype" in data.keys() and isinstance(data["mimetype"], str) and data["mimetype"].startswith("image/svg"):';
		$m.__track_lines__[140] = 'utils.py, line 140:\n    return "/file/download/%s" % data["dlkey"]';
		$m.__track_lines__[142] = 'utils.py, line 142:\n    if data["servingurl"]:';
		$m.__track_lines__[143] = 'utils.py, line 143:\n    return data["servingurl"] + "=s150"';
		$m.__track_lines__[145] = 'utils.py, line 145:\n    return ""';
		$m.__track_lines__[147] = 'utils.py, line 147:\n    return None';
		$m.__track_lines__[132] = 'utils.py, line 132:\n    return( True )';
		$m.__track_lines__[32] = 'utils.py, line 32:\n    def chooseLang( value, prefs, key ): #FIXME: Copy&Paste from bones/string';
		$m.__track_lines__[38] = 'utils.py, line 38:\n    if not isinstance( value, dict ):';
		$m.__track_lines__[39] = 'utils.py, line 39:\n    return( str(value) )';
		$m.__track_lines__[41] = 'utils.py, line 41:\n    try:';
		$m.__track_lines__[42] = 'utils.py, line 42:\n    lang = "%s.%s" % (key,conf["currentlanguage"])';
		$m.__track_lines__[44] = 'utils.py, line 44:\n    lang = ""';
		$m.__track_lines__[45] = 'utils.py, line 45:\n    if lang in value.keys() and value[ lang ]:';
		$m.__track_lines__[46] = 'utils.py, line 46:\n    return( value[ lang ] )';
		$m.__track_lines__[47] = 'utils.py, line 47:\n    for lang in prefs:';
		$m.__track_lines__[48] = 'utils.py, line 48:\n    if "%s.%s" % (key,lang) in value.keys():';
		$m.__track_lines__[49] = 'utils.py, line 49:\n    if value[ "%s.%s" % (key,lang) ]:';
		$m.__track_lines__[50] = 'utils.py, line 50:\n    return( value[ "%s.%s" % (key,lang) ] )';
		$m.__track_lines__[52] = 'utils.py, line 52:\n    if key in value.keys() and isinstance( value[ key ], dict ):';
		$m.__track_lines__[53] = 'utils.py, line 53:\n    langDict = value[ key ]';
		$m.__track_lines__[54] = 'utils.py, line 54:\n    try:';
		$m.__track_lines__[55] = 'utils.py, line 55:\n    lang = conf["currentlanguage"]';
		$m.__track_lines__[57] = 'utils.py, line 57:\n    lang = ""';
		$m.__track_lines__[58] = 'utils.py, line 58:\n    if lang in langDict.keys():';
		$m.__track_lines__[59] = 'utils.py, line 59:\n    return( langDict[ lang ] )';
		$m.__track_lines__[60] = 'utils.py, line 60:\n    for lang in prefs:';
		$m.__track_lines__[61] = 'utils.py, line 61:\n    if lang in langDict.keys():';
		$m.__track_lines__[62] = 'utils.py, line 62:\n    if langDict[ lang ]:';
		$m.__track_lines__[63] = 'utils.py, line 63:\n    return( langDict[ lang ] )';
		$m.__track_lines__[64] = 'utils.py, line 64:\n    return( "" )';
		$m.__track_lines__[66] = 'utils.py, line 66:\n    if isinstance( skelStructure, list):';
		$m.__track_lines__[70] = 'utils.py, line 70:\n    tmpDict = {}';
		$m.__track_lines__[71] = 'utils.py, line 71:\n    for key, bone in skelStructure:';
		$m.__track_lines__[72] = 'utils.py, line 72:\n    tmpDict[ key ] = bone';
		$m.__track_lines__[73] = 'utils.py, line 73:\n    skelStructure = tmpDict';
		$m.__track_lines__[75] = 'utils.py, line 75:\n    prefix = prefix or []';
		$m.__track_lines__[76] = 'utils.py, line 76:\n    if isinstance( data,  list ):';
		$m.__track_lines__[77] = 'utils.py, line 77:\n    return(", ".join( [ formatString( format, skelStructure, x, prefix, unescape ) for x in data ] ) )';
		$m.__track_lines__[79] = 'utils.py, line 79:\n    res = format';
		$m.__track_lines__[81] = 'utils.py, line 81:\n    if isinstance( data, str ):';
		$m.__track_lines__[82] = 'utils.py, line 82:\n    return data';
		$m.__track_lines__[84] = 'utils.py, line 84:\n    if not data:';
		$m.__track_lines__[85] = 'utils.py, line 85:\n    return res';
		$m.__track_lines__[87] = 'utils.py, line 87:\n    for key in data.keys():';
		$m.__track_lines__[88] = 'utils.py, line 88:\n    if isinstance( data[ key ], dict ):';
		$m.__track_lines__[89] = 'utils.py, line 89:\n    res = formatString( res, skelStructure, data[key], prefix + [key] )';
		$m.__track_lines__[91] = 'utils.py, line 91:\n    res = formatString( res, skelStructure, data[key][0], prefix + [key] )';
		$m.__track_lines__[93] = 'utils.py, line 93:\n    tok = key.split(".")';
		$m.__track_lines__[94] = 'utils.py, line 94:\n    if "." in key and "$(%s)" % tok[0] in res and tok[1] == conf["currentlanguage"]:';
		$m.__track_lines__[95] = 'utils.py, line 95:\n    res = res.replace("$(%s)" % tok[0], str(data[key]))';
		$m.__track_lines__[97] = 'utils.py, line 97:\n    res = res.replace( "$(%s)" % (".".join( prefix + [key] ) ), str(data[key]) )';
		$m.__track_lines__[100] = 'utils.py, line 100:\n    if not prefix:';
		$m.__track_lines__[101] = 'utils.py, line 101:\n    for key, bone in skelStructure.items():';
		$m.__track_lines__[102] = 'utils.py, line 102:\n    if "languages" in bone.keys() and bone[ "languages" ]:';
		$m.__track_lines__[103] = 'utils.py, line 103:\n    res = res.replace( "$(%s)" % key, str(chooseLang( data, bone[ "languages" ], key) ) )';
		$m.__track_lines__[106] = 'utils.py, line 106:\n    if unescape:';
		$m.__track_lines__[107] = 'utils.py, line 107:\n    return html5.utils.unescape(res)';
		$m.__track_lines__[109] = 'utils.py, line 109:\n    return res';
		$m.__track_lines__[111] = 'utils.py, line 111:\n    def boneListToDict( l ):';
		$m.__track_lines__[112] = 'utils.py, line 112:\n    res = {}';
		$m.__track_lines__[113] = 'utils.py, line 113:\n    for key, bone in l:';
		$m.__track_lines__[114] = 'utils.py, line 114:\n    res[ key ] = bone';
		$m.__track_lines__[115] = 'utils.py, line 115:\n    return( res )';
		$m.__track_lines__[117] = 'utils.py, line 117:\n    def doesEventHitWidgetOrParents( event, widget ):';
		$m.__track_lines__[121] = 'utils.py, line 121:\n    while widget:';
		$m.__track_lines__[122] = 'utils.py, line 122:\n    if event.target == widget.element:';
		$m.__track_lines__[123] = 'utils.py, line 123:\n    return( True )';
		$m.__track_lines__[124] = 'utils.py, line 124:\n    widget = widget.parent()';
		$m.__track_lines__[125] = 'utils.py, line 125:\n    return( False )';
		$m.__track_lines__[127] = 'utils.py, line 127:\n    def doesEventHitWidgetOrChildren( event, widget ):';

		var $constant_int_0 = new $p['int'](0);
		var $constant_int_1 = new $p['int'](1);
		$pyjs['track']['module']='utils';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=1;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['html5'] = $p['___import___']('html5', null);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=2;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['conf'] = $p['___import___']('config.conf', null, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=4;
		$m['formatString'] = function(format, skelStructure, data, prefix, unescape) {
			if ($pyjs['options']['arg_count'] && (arguments['length'] < 3 || arguments['length'] > 5)) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 5, arguments['length']);
			if (typeof prefix == 'undefined') prefix=arguments['callee']['__args__'][5][1];
			if (typeof unescape == 'undefined') unescape=arguments['callee']['__args__'][6][1];
			var $iter5_nextval,$iter6_type,$iter5_array,$iter3_type,$iter5_iter,$iter5_type,$iter6_iter,$iter6_nextval,$iter3_idx,$add3,res,$iter3_nextval,$or2,tok,$iter3_iter,$iter5_idx,$and8,$and9,$or1,$iter6_idx,tmpDict,$add5,$and5,$and6,$and7,$and12,$and10,$and11,key,$iter3_array,$iter6_array,$add2,chooseLang,$add1,$add6,$add4,$pyjs__trackstack_size_1,bone;
			$pyjs['track']={'module':'utils','lineno':4};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='utils';
			$pyjs['track']['lineno']=4;
			$pyjs['track']['lineno']=32;
			chooseLang = function(value, prefs, key) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var lang,$iter2_array,$iter2_nextval,$iter1_nextval,$iter1_type,$iter2_iter,$and1,$and2,$and3,$and4,$iter1_iter,$iter2_idx,$pyjs_try_err,$iter1_array,$pyjs__trackstack_size_1,$iter2_type,langDict,$iter1_idx;
				$pyjs['track']={'module':'utils','lineno':32};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='utils';
				$pyjs['track']['lineno']=32;
				$pyjs['track']['lineno']=38;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](value, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})()));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})()) {
					$pyjs['track']['lineno']=39;
					$pyjs['track']['lineno']=39;
					var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['str'](value);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})();
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=41;
				var $pyjs__trackstack_size_1 = $pyjs['trackstack']['length'];
				try {
					try {
						$pyjs['in_try_except'] += 1;
						$pyjs['track']['lineno']=42;
						lang = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('%s.%s', (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['tuple']([key, $m['conf']['__getitem__']('currentlanguage')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})();
					} finally { $pyjs['in_try_except'] -= 1; }
				} catch($pyjs_try_err) {
					$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_1 - 1);
					$pyjs['__active_exception_stack__'] = null;
					$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
					var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
					$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='utils';
					if (true) {
						$pyjs['track']['lineno']=44;
						lang = '';
					}
				}
				$pyjs['track']['lineno']=45;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and1=(function(){try{try{$pyjs['in_try_except'] += 1;
				return value['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})()['__contains__'](lang))?value['__getitem__'](lang):$and1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})()) {
					$pyjs['track']['lineno']=46;
					$pyjs['track']['lineno']=46;
					var $pyjs__ret = value['__getitem__'](lang);
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=47;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter1_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return prefs;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})();
				$iter1_nextval=$p['__iter_prepare']($iter1_iter,false);
				while (typeof($p['__wrapped_next']($iter1_nextval)['$nextval']) != 'undefined') {
					lang = $iter1_nextval['$nextval'];
					$pyjs['track']['lineno']=48;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return value['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})()['__contains__']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('%s.%s', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['tuple']([key, lang]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})()));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})()) {
						$pyjs['track']['lineno']=49;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](value['__getitem__']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('%s.%s', (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['tuple']([key, lang]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_14_err){if (!$p['isinstance']($pyjs_dbg_14_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_14_err);}throw $pyjs_dbg_14_err;
}})()));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})()) {
							$pyjs['track']['lineno']=50;
							$pyjs['track']['lineno']=50;
							var $pyjs__ret = value['__getitem__']((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['sprintf']('%s.%s', (function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['tuple']([key, lang]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_16_err){if (!$p['isinstance']($pyjs_dbg_16_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_16_err);}throw $pyjs_dbg_16_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_17_err){if (!$p['isinstance']($pyjs_dbg_17_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_17_err);}throw $pyjs_dbg_17_err;
}})());
							$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
							return $pyjs__ret;
						}
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='utils';
				$pyjs['track']['lineno']=52;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and3=(function(){try{try{$pyjs['in_try_except'] += 1;
				return value['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})()['__contains__'](key))?(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](value['__getitem__'](key), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_19_err){if (!$p['isinstance']($pyjs_dbg_19_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_19_err);}throw $pyjs_dbg_19_err;
}})():$and3));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})()) {
					$pyjs['track']['lineno']=53;
					langDict = value['__getitem__'](key);
					$pyjs['track']['lineno']=54;
					var $pyjs__trackstack_size_1 = $pyjs['trackstack']['length'];
					try {
						try {
							$pyjs['in_try_except'] += 1;
							$pyjs['track']['lineno']=55;
							lang = $m['conf']['__getitem__']('currentlanguage');
						} finally { $pyjs['in_try_except'] -= 1; }
					} catch($pyjs_try_err) {
						$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_1 - 1);
						$pyjs['__active_exception_stack__'] = null;
						$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
						var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
						$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='utils';
						if (true) {
							$pyjs['track']['lineno']=57;
							lang = '';
						}
					}
					$pyjs['track']['lineno']=58;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return langDict['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})()['__contains__'](lang));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})()) {
						$pyjs['track']['lineno']=59;
						$pyjs['track']['lineno']=59;
						var $pyjs__ret = langDict['__getitem__'](lang);
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
					$pyjs['track']['lineno']=60;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter2_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return prefs;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})();
					$iter2_nextval=$p['__iter_prepare']($iter2_iter,false);
					while (typeof($p['__wrapped_next']($iter2_nextval)['$nextval']) != 'undefined') {
						lang = $iter2_nextval['$nextval'];
						$pyjs['track']['lineno']=61;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return langDict['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})()['__contains__'](lang));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})()) {
							$pyjs['track']['lineno']=62;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool'](langDict['__getitem__'](lang));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_26_err){if (!$p['isinstance']($pyjs_dbg_26_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_26_err);}throw $pyjs_dbg_26_err;
}})()) {
								$pyjs['track']['lineno']=63;
								$pyjs['track']['lineno']=63;
								var $pyjs__ret = langDict['__getitem__'](lang);
								$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
								return $pyjs__ret;
							}
						}
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='utils';
				}
				$pyjs['track']['lineno']=64;
				$pyjs['track']['lineno']=64;
				var $pyjs__ret = '';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			};
			chooseLang['__name__'] = 'chooseLang';

			chooseLang['__bind_type__'] = 0;
			chooseLang['__args__'] = [null,null,['value'],['prefs'],['key']];
			$pyjs['track']['lineno']=66;
			if ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['isinstance'](skelStructure, $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_27_err){if (!$p['isinstance']($pyjs_dbg_27_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_27_err);}throw $pyjs_dbg_27_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_28_err){if (!$p['isinstance']($pyjs_dbg_28_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_28_err);}throw $pyjs_dbg_28_err;
}})()) {
				$pyjs['track']['lineno']=70;
				tmpDict = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_29_err){if (!$p['isinstance']($pyjs_dbg_29_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_29_err);}throw $pyjs_dbg_29_err;
}})();
				$pyjs['track']['lineno']=71;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter3_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_31_err){if (!$p['isinstance']($pyjs_dbg_31_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_31_err);}throw $pyjs_dbg_31_err;
}})();
				$iter3_nextval=$p['__iter_prepare']($iter3_iter,false);
				while (typeof($p['__wrapped_next']($iter3_nextval)['$nextval']) != 'undefined') {
					var $tupleassign1 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter3_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_30_err){if (!$p['isinstance']($pyjs_dbg_30_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_30_err);}throw $pyjs_dbg_30_err;
}})();
					key = $tupleassign1[0];
					bone = $tupleassign1[1];
					$pyjs['track']['lineno']=72;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return tmpDict['__setitem__'](key, bone);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_32_err){if (!$p['isinstance']($pyjs_dbg_32_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_32_err);}throw $pyjs_dbg_32_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='utils';
				$pyjs['track']['lineno']=73;
				skelStructure = tmpDict;
			}
			$pyjs['track']['lineno']=75;
			prefix = ($p['bool']($or1=prefix)?$or1:(function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_33_err){if (!$p['isinstance']($pyjs_dbg_33_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_33_err);}throw $pyjs_dbg_33_err;
}})());
			$pyjs['track']['lineno']=76;
			if ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['isinstance'](data, $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_34_err){if (!$p['isinstance']($pyjs_dbg_34_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_34_err);}throw $pyjs_dbg_34_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})()) {
				$pyjs['track']['lineno']=77;
				$pyjs['track']['lineno']=77;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return ', '['join'](function(){
					var $iter4_nextval,$collcomp1,$iter4_idx,$iter4_type,$pyjs__trackstack_size_1,$iter4_array,x,$iter4_iter;
	$collcomp1 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter4_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return data;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_36_err){if (!$p['isinstance']($pyjs_dbg_36_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_36_err);}throw $pyjs_dbg_36_err;
}})();
				$iter4_nextval=$p['__iter_prepare']($iter4_iter,false);
				while (typeof($p['__wrapped_next']($iter4_nextval)['$nextval']) != 'undefined') {
					x = $iter4_nextval['$nextval'];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp1['append']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['formatString'](format, skelStructure, x, prefix, unescape);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_37_err){if (!$p['isinstance']($pyjs_dbg_37_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_37_err);}throw $pyjs_dbg_37_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_38_err){if (!$p['isinstance']($pyjs_dbg_38_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_38_err);}throw $pyjs_dbg_38_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='utils';

	return $collcomp1;}());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_39_err){if (!$p['isinstance']($pyjs_dbg_39_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_39_err);}throw $pyjs_dbg_39_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
			$pyjs['track']['lineno']=79;
			res = format;
			$pyjs['track']['lineno']=81;
			if ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['isinstance'](data, $p['str']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_40_err){if (!$p['isinstance']($pyjs_dbg_40_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_40_err);}throw $pyjs_dbg_40_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_41_err){if (!$p['isinstance']($pyjs_dbg_41_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_41_err);}throw $pyjs_dbg_41_err;
}})()) {
				$pyjs['track']['lineno']=82;
				$pyjs['track']['lineno']=82;
				var $pyjs__ret = data;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
			$pyjs['track']['lineno']=84;
			if ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['bool'](!$p['bool'](data));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_42_err){if (!$p['isinstance']($pyjs_dbg_42_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_42_err);}throw $pyjs_dbg_42_err;
}})()) {
				$pyjs['track']['lineno']=85;
				$pyjs['track']['lineno']=85;
				var $pyjs__ret = res;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
			$pyjs['track']['lineno']=87;
			$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
			$iter5_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
			return (function(){try{try{$pyjs['in_try_except'] += 1;
			return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_43_err){if (!$p['isinstance']($pyjs_dbg_43_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_43_err);}throw $pyjs_dbg_43_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_44_err){if (!$p['isinstance']($pyjs_dbg_44_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_44_err);}throw $pyjs_dbg_44_err;
}})();
			$iter5_nextval=$p['__iter_prepare']($iter5_iter,false);
			while (typeof($p['__wrapped_next']($iter5_nextval)['$nextval']) != 'undefined') {
				key = $iter5_nextval['$nextval'];
				$pyjs['track']['lineno']=88;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](data['__getitem__'](key), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_45_err){if (!$p['isinstance']($pyjs_dbg_45_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_45_err);}throw $pyjs_dbg_45_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_46_err){if (!$p['isinstance']($pyjs_dbg_46_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_46_err);}throw $pyjs_dbg_46_err;
}})()) {
					$pyjs['track']['lineno']=89;
					res = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['formatString'](res, skelStructure, data['__getitem__'](key), $p['__op_add']($add1=prefix,$add2=(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([key]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})()));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_48_err){if (!$p['isinstance']($pyjs_dbg_48_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_48_err);}throw $pyjs_dbg_48_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and5=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](data['__getitem__'](key), $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_49_err){if (!$p['isinstance']($pyjs_dbg_49_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_49_err);}throw $pyjs_dbg_49_err;
}})())?($p['bool']($and6=($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](data['__getitem__'](key));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_50_err){if (!$p['isinstance']($pyjs_dbg_50_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_50_err);}throw $pyjs_dbg_50_err;
}})(), $constant_int_0) == 1))?(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](data['__getitem__'](key)['__getitem__']($constant_int_0), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_51_err){if (!$p['isinstance']($pyjs_dbg_51_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_51_err);}throw $pyjs_dbg_51_err;
}})():$and6):$and5));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_52_err){if (!$p['isinstance']($pyjs_dbg_52_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_52_err);}throw $pyjs_dbg_52_err;
}})()) {
					$pyjs['track']['lineno']=91;
					res = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['formatString'](res, skelStructure, data['__getitem__'](key)['__getitem__']($constant_int_0), $p['__op_add']($add3=prefix,$add4=(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([key]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_53_err){if (!$p['isinstance']($pyjs_dbg_53_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_53_err);}throw $pyjs_dbg_53_err;
}})()));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_54_err){if (!$p['isinstance']($pyjs_dbg_54_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_54_err);}throw $pyjs_dbg_54_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=93;
					tok = (function(){try{try{$pyjs['in_try_except'] += 1;
					return key['$$split']('.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_55_err){if (!$p['isinstance']($pyjs_dbg_55_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_55_err);}throw $pyjs_dbg_55_err;
}})();
					$pyjs['track']['lineno']=94;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and8=key['__contains__']('.'))?($p['bool']($and9=res['__contains__']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('$(%s)', tok['__getitem__']($constant_int_0));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_56_err){if (!$p['isinstance']($pyjs_dbg_56_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_56_err);}throw $pyjs_dbg_56_err;
}})()))?$p['op_eq'](tok['__getitem__']($constant_int_1), $m['conf']['__getitem__']('currentlanguage')):$and9):$and8));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_57_err){if (!$p['isinstance']($pyjs_dbg_57_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_57_err);}throw $pyjs_dbg_57_err;
}})()) {
						$pyjs['track']['lineno']=95;
						res = (function(){try{try{$pyjs['in_try_except'] += 1;
						return res['$$replace']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('$(%s)', tok['__getitem__']($constant_int_0));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_58_err){if (!$p['isinstance']($pyjs_dbg_58_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_58_err);}throw $pyjs_dbg_58_err;
}})(), (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['str'](data['__getitem__'](key));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_59_err){if (!$p['isinstance']($pyjs_dbg_59_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_59_err);}throw $pyjs_dbg_59_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_60_err){if (!$p['isinstance']($pyjs_dbg_60_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_60_err);}throw $pyjs_dbg_60_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=97;
						res = (function(){try{try{$pyjs['in_try_except'] += 1;
						return res['$$replace']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('$(%s)', (function(){try{try{$pyjs['in_try_except'] += 1;
						return '.'['join']($p['__op_add']($add5=prefix,$add6=(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['list']([key]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_61_err){if (!$p['isinstance']($pyjs_dbg_61_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_61_err);}throw $pyjs_dbg_61_err;
}})()));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_62_err){if (!$p['isinstance']($pyjs_dbg_62_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_62_err);}throw $pyjs_dbg_62_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_63_err){if (!$p['isinstance']($pyjs_dbg_63_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_63_err);}throw $pyjs_dbg_63_err;
}})(), (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['str'](data['__getitem__'](key));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_65_err){if (!$p['isinstance']($pyjs_dbg_65_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_65_err);}throw $pyjs_dbg_65_err;
}})();
					}
				}
			}
			if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
				$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
				$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
			}
			$pyjs['track']['module']='utils';
			$pyjs['track']['lineno']=100;
			if ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['bool'](!$p['bool'](prefix));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_66_err){if (!$p['isinstance']($pyjs_dbg_66_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_66_err);}throw $pyjs_dbg_66_err;
}})()) {
				$pyjs['track']['lineno']=101;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter6_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_68_err){if (!$p['isinstance']($pyjs_dbg_68_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_68_err);}throw $pyjs_dbg_68_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_69_err){if (!$p['isinstance']($pyjs_dbg_69_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_69_err);}throw $pyjs_dbg_69_err;
}})();
				$iter6_nextval=$p['__iter_prepare']($iter6_iter,false);
				while (typeof($p['__wrapped_next']($iter6_nextval)['$nextval']) != 'undefined') {
					var $tupleassign2 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter6_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})();
					key = $tupleassign2[0];
					bone = $tupleassign2[1];
					$pyjs['track']['lineno']=102;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and11=(function(){try{try{$pyjs['in_try_except'] += 1;
					return bone['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_70_err){if (!$p['isinstance']($pyjs_dbg_70_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_70_err);}throw $pyjs_dbg_70_err;
}})()['__contains__']('languages'))?bone['__getitem__']('languages'):$and11));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_71_err){if (!$p['isinstance']($pyjs_dbg_71_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_71_err);}throw $pyjs_dbg_71_err;
}})()) {
						$pyjs['track']['lineno']=103;
						res = (function(){try{try{$pyjs['in_try_except'] += 1;
						return res['$$replace']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('$(%s)', key);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_72_err){if (!$p['isinstance']($pyjs_dbg_72_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_72_err);}throw $pyjs_dbg_72_err;
}})(), (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['str']((function(){try{try{$pyjs['in_try_except'] += 1;
						return chooseLang(data, bone['__getitem__']('languages'), key);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_73_err){if (!$p['isinstance']($pyjs_dbg_73_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_73_err);}throw $pyjs_dbg_73_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_74_err){if (!$p['isinstance']($pyjs_dbg_74_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_74_err);}throw $pyjs_dbg_74_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='utils';
			}
			$pyjs['track']['lineno']=106;
			if ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['bool'](unescape);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})()) {
				$pyjs['track']['lineno']=107;
				$pyjs['track']['lineno']=107;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['utils']['unescape'](res);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_77_err){if (!$p['isinstance']($pyjs_dbg_77_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_77_err);}throw $pyjs_dbg_77_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
			$pyjs['track']['lineno']=109;
			$pyjs['track']['lineno']=109;
			var $pyjs__ret = res;
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['formatString']['__name__'] = 'formatString';

		$m['formatString']['__bind_type__'] = 0;
		$m['formatString']['__args__'] = [null,null,['format'],['skelStructure'],['data'],['prefix', null],['unescape', false]];
		$pyjs['track']['lineno']=111;
		$m['boneListToDict'] = function(l) {
			if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
			var key,$iter7_nextval,res,$iter7_array,$iter7_idx,$iter7_iter,$iter7_type,$pyjs__trackstack_size_1,bone;
			$pyjs['track']={'module':'utils','lineno':111};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='utils';
			$pyjs['track']['lineno']=111;
			$pyjs['track']['lineno']=112;
			res = (function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_78_err){if (!$p['isinstance']($pyjs_dbg_78_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_78_err);}throw $pyjs_dbg_78_err;
}})();
			$pyjs['track']['lineno']=113;
			$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
			$iter7_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
			return l;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_80_err){if (!$p['isinstance']($pyjs_dbg_80_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_80_err);}throw $pyjs_dbg_80_err;
}})();
			$iter7_nextval=$p['__iter_prepare']($iter7_iter,false);
			while (typeof($p['__wrapped_next']($iter7_nextval)['$nextval']) != 'undefined') {
				var $tupleassign3 = (function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['__ass_unpack']($iter7_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_79_err){if (!$p['isinstance']($pyjs_dbg_79_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_79_err);}throw $pyjs_dbg_79_err;
}})();
				key = $tupleassign3[0];
				bone = $tupleassign3[1];
				$pyjs['track']['lineno']=114;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return res['__setitem__'](key, bone);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_81_err){if (!$p['isinstance']($pyjs_dbg_81_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_81_err);}throw $pyjs_dbg_81_err;
}})();
			}
			if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
				$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
				$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
			}
			$pyjs['track']['module']='utils';
			$pyjs['track']['lineno']=115;
			$pyjs['track']['lineno']=115;
			var $pyjs__ret = res;
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['boneListToDict']['__name__'] = 'boneListToDict';

		$m['boneListToDict']['__bind_type__'] = 0;
		$m['boneListToDict']['__args__'] = [null,null,['l']];
		$pyjs['track']['lineno']=117;
		$m['doesEventHitWidgetOrParents'] = function(event, widget) {
			if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);

			$pyjs['track']={'module':'utils','lineno':117};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='utils';
			$pyjs['track']['lineno']=117;
			$pyjs['track']['lineno']=121;
			while ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['bool'](widget);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_82_err){if (!$p['isinstance']($pyjs_dbg_82_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_82_err);}throw $pyjs_dbg_82_err;
}})()) {
				$pyjs['track']['lineno']=122;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](event, 'target'), $p['getattr'](widget, 'element')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_83_err){if (!$p['isinstance']($pyjs_dbg_83_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_83_err);}throw $pyjs_dbg_83_err;
}})()) {
					$pyjs['track']['lineno']=123;
					$pyjs['track']['lineno']=123;
					var $pyjs__ret = true;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=124;
				widget = (function(){try{try{$pyjs['in_try_except'] += 1;
				return widget['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_84_err){if (!$p['isinstance']($pyjs_dbg_84_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_84_err);}throw $pyjs_dbg_84_err;
}})();
			}
			$pyjs['track']['lineno']=125;
			$pyjs['track']['lineno']=125;
			var $pyjs__ret = false;
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['doesEventHitWidgetOrParents']['__name__'] = 'doesEventHitWidgetOrParents';

		$m['doesEventHitWidgetOrParents']['__bind_type__'] = 0;
		$m['doesEventHitWidgetOrParents']['__args__'] = [null,null,['event'],['widget']];
		$pyjs['track']['lineno']=127;
		$m['doesEventHitWidgetOrChildren'] = function(event, widget) {
			if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
			var $iter8_idx,$iter8_type,$pyjs__trackstack_size_1,$iter8_array,$iter8_iter,$iter8_nextval,child;
			$pyjs['track']={'module':'utils','lineno':127};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='utils';
			$pyjs['track']['lineno']=127;
			$pyjs['track']['lineno']=131;
			if ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['bool']($p['op_eq']($p['getattr'](event, 'target'), $p['getattr'](widget, 'element')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})()) {
				$pyjs['track']['lineno']=132;
				$pyjs['track']['lineno']=132;
				var $pyjs__ret = true;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
			$pyjs['track']['lineno']=133;
			$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
			$iter8_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['getattr'](widget, '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})();
			$iter8_nextval=$p['__iter_prepare']($iter8_iter,false);
			while (typeof($p['__wrapped_next']($iter8_nextval)['$nextval']) != 'undefined') {
				child = $iter8_nextval['$nextval'];
				$pyjs['track']['lineno']=134;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['doesEventHitWidgetOrChildren'](event, child);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})()) {
					$pyjs['track']['lineno']=135;
					$pyjs['track']['lineno']=135;
					var $pyjs__ret = true;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
			}
			if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
				$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
				$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
			}
			$pyjs['track']['module']='utils';
			$pyjs['track']['lineno']=136;
			$pyjs['track']['lineno']=136;
			var $pyjs__ret = false;
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['doesEventHitWidgetOrChildren']['__name__'] = 'doesEventHitWidgetOrChildren';

		$m['doesEventHitWidgetOrChildren']['__bind_type__'] = 0;
		$m['doesEventHitWidgetOrChildren']['__args__'] = [null,null,['event'],['widget']];
		$pyjs['track']['lineno']=138;
		$m['getImagePreview'] = function(data) {
			if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
			var $and13,$add7,$and14,$and15,$add8;
			$pyjs['track']={'module':'utils','lineno':138};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='utils';
			$pyjs['track']['lineno']=138;
			$pyjs['track']['lineno']=139;
			if ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['bool'](($p['bool']($and13=(function(){try{try{$pyjs['in_try_except'] += 1;
			return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_89_err){if (!$p['isinstance']($pyjs_dbg_89_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_89_err);}throw $pyjs_dbg_89_err;
}})()['__contains__']('mimetype'))?($p['bool']($and14=(function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['isinstance'](data['__getitem__']('mimetype'), $p['str']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})())?(function(){try{try{$pyjs['in_try_except'] += 1;
			return data['__getitem__']('mimetype')['startswith']('image/svg');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_91_err){if (!$p['isinstance']($pyjs_dbg_91_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_91_err);}throw $pyjs_dbg_91_err;
}})():$and14):$and13));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_92_err){if (!$p['isinstance']($pyjs_dbg_92_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_92_err);}throw $pyjs_dbg_92_err;
}})()) {
				$pyjs['track']['lineno']=140;
				$pyjs['track']['lineno']=140;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('/file/download/%s', data['__getitem__']('dlkey'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_93_err){if (!$p['isinstance']($pyjs_dbg_93_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_93_err);}throw $pyjs_dbg_93_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
			else if ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
			return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_94_err){if (!$p['isinstance']($pyjs_dbg_94_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_94_err);}throw $pyjs_dbg_94_err;
}})()['__contains__']('servingurl'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_95_err){if (!$p['isinstance']($pyjs_dbg_95_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_95_err);}throw $pyjs_dbg_95_err;
}})()) {
				$pyjs['track']['lineno']=142;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](data['__getitem__']('servingurl'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_96_err){if (!$p['isinstance']($pyjs_dbg_96_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_96_err);}throw $pyjs_dbg_96_err;
}})()) {
					$pyjs['track']['lineno']=143;
					$pyjs['track']['lineno']=143;
					var $pyjs__ret = $p['__op_add']($add7=data['__getitem__']('servingurl'),$add8='=s150');
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=145;
				$pyjs['track']['lineno']=145;
				var $pyjs__ret = '';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
			$pyjs['track']['lineno']=147;
			$pyjs['track']['lineno']=147;
			var $pyjs__ret = null;
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['getImagePreview']['__name__'] = 'getImagePreview';

		$m['getImagePreview']['__bind_type__'] = 0;
		$m['getImagePreview']['__args__'] = [null,null,['data']];
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end utils */


/* end module: utils */


/*
PYJS_DEPS: ['html5', 'config.conf', 'config']
*/
