/* start module: handler.singleton */
$pyjs['loaded_modules']['handler.singleton'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['handler.singleton']['__was_initialized__']) return $pyjs['loaded_modules']['handler.singleton'];
	if(typeof $pyjs['loaded_modules']['handler'] == 'undefined' || !$pyjs['loaded_modules']['handler']['__was_initialized__']) $p['___import___']('handler', null);
	var $m = $pyjs['loaded_modules']['handler.singleton'];
	$m['__repr__'] = function() { return '<module: handler.singleton>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'handler.singleton';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	$pyjs['loaded_modules']['handler']['singleton'] = $pyjs['loaded_modules']['handler.singleton'];
	try {
		$m.__track_lines__[1] = 'handler.singleton.py, line 1:\n    from priorityqueue import HandlerClassSelector, displayDelegateSelector, initialHashHandler';
		$m.__track_lines__[2] = 'handler.singleton.py, line 2:\n    from widgets import EditWidget';
		$m.__track_lines__[3] = 'handler.singleton.py, line 3:\n    from config import conf';
		$m.__track_lines__[4] = 'handler.singleton.py, line 4:\n    from pane import Pane';
		$m.__track_lines__[7] = 'handler.singleton.py, line 7:\n    class SingletonHandler( Pane ):';
		$m.__track_lines__[8] = 'handler.singleton.py, line 8:\n    def __init__(self, modulName, modulInfo, groupName=None, *args, **kwargs):';
		$m.__track_lines__[9] = 'handler.singleton.py, line 9:\n    icon = "icons/modules/singleton.svg"';
		$m.__track_lines__[10] = 'handler.singleton.py, line 10:\n    if "icon" in modulInfo.keys():';
		$m.__track_lines__[11] = 'handler.singleton.py, line 11:\n    icon = modulInfo["icon"]';
		$m.__track_lines__[12] = 'handler.singleton.py, line 12:\n    if groupName:';
		$m.__track_lines__[13] = 'handler.singleton.py, line 13:\n    myDescr = modulInfo["name"].replace( groupName, "")';
		$m.__track_lines__[15] = 'handler.singleton.py, line 15:\n    myDescr = modulInfo["name"]';
		$m.__track_lines__[16] = 'handler.singleton.py, line 16:\n    super( SingletonHandler, self ).__init__( myDescr, icon )';
		$m.__track_lines__[17] = 'handler.singleton.py, line 17:\n    self.modulName = modulName';
		$m.__track_lines__[18] = 'handler.singleton.py, line 18:\n    self.modulInfo = modulInfo';
		$m.__track_lines__[19] = 'handler.singleton.py, line 19:\n    if "hideInMainBar" in modulInfo.keys() and modulInfo["hideInMainBar"]:';
		$m.__track_lines__[20] = 'handler.singleton.py, line 20:\n    self["style"]["display"] = "none"';
		$m.__track_lines__[21] = 'handler.singleton.py, line 21:\n    initialHashHandler.insert( 1, self.canHandleInitialHash, self.handleInitialHash)';
		$m.__track_lines__[23] = 'handler.singleton.py, line 23:\n    def canHandleInitialHash(self, pathList, params ):';
		$m.__track_lines__[24] = 'handler.singleton.py, line 24:\n    if len(pathList)>1:';
		$m.__track_lines__[25] = 'handler.singleton.py, line 25:\n    if pathList[0]==self.modulName and pathList[1]=="edit":';
		$m.__track_lines__[26] = 'handler.singleton.py, line 26:\n    return( True )';
		$m.__track_lines__[27] = 'handler.singleton.py, line 27:\n    return( False )';
		$m.__track_lines__[29] = 'handler.singleton.py, line 29:\n    def handleInitialHash(self, pathList, params):';
		$m.__track_lines__[30] = 'handler.singleton.py, line 30:\n    assert self.canHandleInitialHash( pathList, params )';
		$m.__track_lines__[31] = 'handler.singleton.py, line 31:\n    edwg = EditWidget( self.modulName, EditWidget.appSingleton, hashArgs=(params or None))';
		$m.__track_lines__[32] = 'handler.singleton.py, line 32:\n    self.addWidget( edwg )';
		$m.__track_lines__[33] = 'handler.singleton.py, line 33:\n    self.focus()';
		$m.__track_lines__[36] = 'handler.singleton.py, line 35:\n    @staticmethod ... def canHandle( modulName, modulInfo ):';
		$m.__track_lines__[37] = 'handler.singleton.py, line 37:\n    return( modulInfo["handler"]=="singleton" or modulInfo["handler"].startswith("singleton."))';
		$m.__track_lines__[39] = 'handler.singleton.py, line 39:\n    def onClick(self, *args, **kwargs ):';
		$m.__track_lines__[40] = 'handler.singleton.py, line 40:\n    if not len(self.widgetsDomElm._children):';
		$m.__track_lines__[41] = 'handler.singleton.py, line 41:\n    self.addWidget( EditWidget( modul=self.modulName, applicationType=EditWidget.appSingleton ) )';
		$m.__track_lines__[42] = 'handler.singleton.py, line 42:\n    super( SingletonHandler, self ).onClick( *args, **kwargs )';
		$m.__track_lines__[45] = 'handler.singleton.py, line 45:\n    HandlerClassSelector.insert( 3, SingletonHandler.canHandle, SingletonHandler )';

		var $constant_int_0 = new $p['int'](0);
		var $constant_int_1 = new $p['int'](1);
		var $constant_int_3 = new $p['int'](3);
		$pyjs['track']['module']='handler.singleton';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=1;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['HandlerClassSelector'] = $p['___import___']('priorityqueue.HandlerClassSelector', 'handler', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['displayDelegateSelector'] = $p['___import___']('priorityqueue.displayDelegateSelector', 'handler', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['initialHashHandler'] = $p['___import___']('priorityqueue.initialHashHandler', 'handler', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=2;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['EditWidget'] = $p['___import___']('widgets.EditWidget', 'handler', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=3;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['conf'] = $p['___import___']('config.conf', 'handler', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=4;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['Pane'] = $p['___import___']('pane.Pane', 'handler', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=7;
		$m['SingletonHandler'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'handler.singleton';
			$cls_definition['__md5__'] = '11b81ea7fef5a7e9120d0af90c664093';
			$pyjs['track']['lineno']=8;
			$method = $pyjs__bind_method2('__init__', function(modulName, modulInfo, groupName) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

					var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					modulName = arguments[1];
					modulInfo = arguments[2];
					groupName = arguments[3];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,4,arguments['length']-1));

					var kwargs = arguments['length'] >= 5 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '11b81ea7fef5a7e9120d0af90c664093') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof groupName != 'undefined') {
						if (groupName !== null && typeof groupName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = groupName;
							groupName = arguments[4];
						}
					} else 					if (typeof modulInfo != 'undefined') {
						if (modulInfo !== null && typeof modulInfo['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modulInfo;
							modulInfo = arguments[4];
						}
					} else 					if (typeof modulName != 'undefined') {
						if (modulName !== null && typeof modulName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modulName;
							modulName = arguments[4];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[4];
						}
					} else {
					}
				}
				if (typeof groupName == 'undefined') groupName=arguments['callee']['__args__'][5][1];
				var $and1,myDescr,$and2,icon;
				$pyjs['track']={'module':'handler.singleton', 'lineno':8};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='handler.singleton';
				$pyjs['track']['lineno']=8;
				$pyjs['track']['lineno']=9;
				icon = 'icons/modules/singleton.svg';
				$pyjs['track']['lineno']=10;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return modulInfo['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})()['__contains__']('icon'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})()) {
					$pyjs['track']['lineno']=11;
					icon = modulInfo['__getitem__']('icon');
				}
				$pyjs['track']['lineno']=12;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](groupName);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})()) {
					$pyjs['track']['lineno']=13;
					myDescr = (function(){try{try{$pyjs['in_try_except'] += 1;
					return modulInfo['__getitem__']('name')['$$replace'](groupName, '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=15;
					myDescr = modulInfo['__getitem__']('name');
				}
				$pyjs['track']['lineno']=16;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SingletonHandler'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})()['__init__'](myDescr, icon);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})();
				$pyjs['track']['lineno']=17;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modulName', modulName) : $p['setattr'](self, 'modulName', modulName); 
				$pyjs['track']['lineno']=18;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modulInfo', modulInfo) : $p['setattr'](self, 'modulInfo', modulInfo); 
				$pyjs['track']['lineno']=19;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and1=(function(){try{try{$pyjs['in_try_except'] += 1;
				return modulInfo['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})()['__contains__']('hideInMainBar'))?modulInfo['__getitem__']('hideInMainBar'):$and1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})()) {
					$pyjs['track']['lineno']=20;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})();
				}
				$pyjs['track']['lineno']=21;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['initialHashHandler']['insert']($constant_int_1, $p['getattr'](self, 'canHandleInitialHash'), $p['getattr'](self, 'handleInitialHash'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['modulName'],['modulInfo'],['groupName', null]]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=23;
			$method = $pyjs__bind_method2('canHandleInitialHash', function(pathList, params) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					pathList = arguments[1];
					params = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '11b81ea7fef5a7e9120d0af90c664093') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and4,$and3;
				$pyjs['track']={'module':'handler.singleton', 'lineno':23};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='handler.singleton';
				$pyjs['track']['lineno']=23;
				$pyjs['track']['lineno']=24;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](pathList);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})(), $constant_int_1) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})()) {
					$pyjs['track']['lineno']=25;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and3=$p['op_eq'](pathList['__getitem__']($constant_int_0), $p['getattr'](self, 'modulName')))?$p['op_eq'](pathList['__getitem__']($constant_int_1), 'edit'):$and3));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})()) {
						$pyjs['track']['lineno']=26;
						$pyjs['track']['lineno']=26;
						var $pyjs__ret = true;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
				}
				$pyjs['track']['lineno']=27;
				$pyjs['track']['lineno']=27;
				var $pyjs__ret = false;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['pathList'],['params']]);
			$cls_definition['canHandleInitialHash'] = $method;
			$pyjs['track']['lineno']=29;
			$method = $pyjs__bind_method2('handleInitialHash', function(pathList, params) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					pathList = arguments[1];
					params = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '11b81ea7fef5a7e9120d0af90c664093') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var edwg,$or1,$or2;
				$pyjs['track']={'module':'handler.singleton', 'lineno':29};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='handler.singleton';
				$pyjs['track']['lineno']=29;
				$pyjs['track']['lineno']=30;
				if (!( (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['canHandleInitialHash'](pathList, params);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_14_err){if (!$p['isinstance']($pyjs_dbg_14_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_14_err);}throw $pyjs_dbg_14_err;
}})() )) {
				   throw $p['AssertionError']();
				 }
				$pyjs['track']['lineno']=31;
				edwg = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['EditWidget'], null, null, [{'hashArgs':($p['bool']($or1=params)?$or1:null)}, $p['getattr'](self, 'modulName'), $p['getattr']($m['EditWidget'], 'appSingleton')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})();
				$pyjs['track']['lineno']=32;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['addWidget'](edwg);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_16_err){if (!$p['isinstance']($pyjs_dbg_16_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_16_err);}throw $pyjs_dbg_16_err;
}})();
				$pyjs['track']['lineno']=33;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['focus']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_17_err){if (!$p['isinstance']($pyjs_dbg_17_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_17_err);}throw $pyjs_dbg_17_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['pathList'],['params']]);
			$cls_definition['handleInitialHash'] = $method;
			$pyjs['track']['lineno']=36;
			$method = $pyjs__bind_method2('canHandle', function(modulName, modulInfo) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				var $or4,$or3;
				$pyjs['track']={'module':'handler.singleton', 'lineno':36};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='handler.singleton';
				$pyjs['track']['lineno']=36;
				$pyjs['track']['lineno']=37;
				$pyjs['track']['lineno']=37;
				var $pyjs__ret = ($p['bool']($or3=$p['op_eq'](modulInfo['__getitem__']('handler'), 'singleton'))?$or3:(function(){try{try{$pyjs['in_try_except'] += 1;
				return modulInfo['__getitem__']('handler')['startswith']('singleton.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})());
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modulName'],['modulInfo']]);
			$cls_definition['canHandle'] = $method;
			$pyjs['track']['lineno']=39;
			$method = $pyjs__bind_method2('onClick', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '11b81ea7fef5a7e9120d0af90c664093') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'handler.singleton', 'lineno':39};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='handler.singleton';
				$pyjs['track']['lineno']=39;
				$pyjs['track']['lineno']=40;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len']($p['getattr']($p['getattr'](self, 'widgetsDomElm'), '_children'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_19_err){if (!$p['isinstance']($pyjs_dbg_19_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_19_err);}throw $pyjs_dbg_19_err;
}})()));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})()) {
					$pyjs['track']['lineno']=41;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['addWidget']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call(null, $m['EditWidget'], null, null, [{'modul':$p['getattr'](self, 'modulName'), 'applicationType':$p['getattr']($m['EditWidget'], 'appSingleton')}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})();
				}
				$pyjs['track']['lineno']=42;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SingletonHandler'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})(), 'onClick', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['onClick'] = $method;
			$pyjs['track']['lineno']=7;
			var $bases = new Array($m['Pane']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('SingletonHandler', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=45;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['HandlerClassSelector']['insert']($constant_int_3, $p['getattr']($m['SingletonHandler'], 'canHandle'), $m['SingletonHandler']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})();
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end handler.singleton */


/* end module: handler.singleton */


/*
PYJS_DEPS: ['priorityqueue.HandlerClassSelector', 'priorityqueue', 'priorityqueue.displayDelegateSelector', 'priorityqueue.initialHashHandler', 'widgets.EditWidget', 'widgets', 'config.conf', 'config', 'pane.Pane', 'pane']
*/
