/* start module: html5.ext.popup */
$pyjs['loaded_modules']['html5.ext.popup'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['html5.ext.popup']['__was_initialized__']) return $pyjs['loaded_modules']['html5.ext.popup'];
	if(typeof $pyjs['loaded_modules']['html5.ext'] == 'undefined' || !$pyjs['loaded_modules']['html5.ext']['__was_initialized__']) $p['___import___']('html5.ext', null);
	var $m = $pyjs['loaded_modules']['html5.ext.popup'];
	$m['__repr__'] = function() { return '<module: html5.ext.popup>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'html5.ext.popup';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	$pyjs['loaded_modules']['html5.ext']['popup'] = $pyjs['loaded_modules']['html5.ext.popup'];
	try {
		$m.__track_lines__[1] = 'html5.ext.popup.py, line 1:\n    import html5';
		$m.__track_lines__[3] = 'html5.ext.popup.py, line 3:\n    class Popup( html5.Div ):';
		$m.__track_lines__[4] = 'html5.ext.popup.py, line 4:\n    def __init__(self, title=None, *args, **kwargs ):';
		$m.__track_lines__[5] = 'html5.ext.popup.py, line 5:\n    super( Popup, self ).__init__(*args, **kwargs)';
		$m.__track_lines__[7] = 'html5.ext.popup.py, line 7:\n    self["class"] = "alertbox"';
		$m.__track_lines__[8] = 'html5.ext.popup.py, line 8:\n    if title:';
		$m.__track_lines__[9] = 'html5.ext.popup.py, line 9:\n    lbl = html5.Span()';
		$m.__track_lines__[10] = 'html5.ext.popup.py, line 10:\n    lbl["class"].append("title")';
		$m.__track_lines__[11] = 'html5.ext.popup.py, line 11:\n    lbl.appendChild( html5.TextNode( title ) )';
		$m.__track_lines__[12] = 'html5.ext.popup.py, line 12:\n    self.appendChild( lbl )';
		$m.__track_lines__[14] = 'html5.ext.popup.py, line 14:\n    self.frameDiv = html5.Div()';
		$m.__track_lines__[15] = 'html5.ext.popup.py, line 15:\n    self.frameDiv["class"] = "popup"';
		$m.__track_lines__[17] = 'html5.ext.popup.py, line 17:\n    self.frameDiv.appendChild( self )';
		$m.__track_lines__[18] = 'html5.ext.popup.py, line 18:\n    html5.Body().appendChild( self.frameDiv )';
		$m.__track_lines__[20] = 'html5.ext.popup.py, line 20:\n    def close(self, *args, **kwargs):';
		$m.__track_lines__[21] = 'html5.ext.popup.py, line 21:\n    html5.Body().removeChild( self.frameDiv )';
		$m.__track_lines__[22] = 'html5.ext.popup.py, line 22:\n    self.frameDiv = None';
		$m.__track_lines__[24] = 'html5.ext.popup.py, line 24:\n    class YesNoDialog( Popup ):';
		$m.__track_lines__[25] = 'html5.ext.popup.py, line 25:\n    def __init__(self, question, title=None, yesCallback=None, noCallback=None, yesLabel="Yes", noLabel="No", *args, **kwargs):';
		$m.__track_lines__[26] = 'html5.ext.popup.py, line 26:\n    super( YesNoDialog, self ).__init__( title, *args, **kwargs )';
		$m.__track_lines__[27] = 'html5.ext.popup.py, line 27:\n    self["class"].append("yesnodialog")';
		$m.__track_lines__[28] = 'html5.ext.popup.py, line 28:\n    self.yesCallback = yesCallback';
		$m.__track_lines__[29] = 'html5.ext.popup.py, line 29:\n    self.noCallback = noCallback';
		$m.__track_lines__[30] = 'html5.ext.popup.py, line 30:\n    lbl = html5.Span()';
		$m.__track_lines__[31] = 'html5.ext.popup.py, line 31:\n    lbl["class"].append("question")';
		$m.__track_lines__[32] = 'html5.ext.popup.py, line 32:\n    lbl.appendChild( html5.TextNode( question ) )';
		$m.__track_lines__[33] = 'html5.ext.popup.py, line 33:\n    self.appendChild( lbl )';
		$m.__track_lines__[34] = 'html5.ext.popup.py, line 34:\n    btnYes = html5.ext.Button(yesLabel, callback=self.onYesClicked )';
		$m.__track_lines__[35] = 'html5.ext.popup.py, line 35:\n    btnYes["class"].append("btn_yes")';
		$m.__track_lines__[36] = 'html5.ext.popup.py, line 36:\n    self.appendChild(btnYes)';
		$m.__track_lines__[37] = 'html5.ext.popup.py, line 37:\n    btnNo = html5.ext.Button(noLabel, callback=self.onNoClicked )';
		$m.__track_lines__[38] = 'html5.ext.popup.py, line 38:\n    btnNo["class"].append("btn_no")';
		$m.__track_lines__[39] = 'html5.ext.popup.py, line 39:\n    self.appendChild(btnNo)';
		$m.__track_lines__[41] = 'html5.ext.popup.py, line 41:\n    def drop(self):';
		$m.__track_lines__[42] = 'html5.ext.popup.py, line 42:\n    self.yesCallback = None';
		$m.__track_lines__[43] = 'html5.ext.popup.py, line 43:\n    self.noCallback = None';
		$m.__track_lines__[44] = 'html5.ext.popup.py, line 44:\n    self.close()';
		$m.__track_lines__[46] = 'html5.ext.popup.py, line 46:\n    def onYesClicked(self, *args, **kwargs ):';
		$m.__track_lines__[47] = 'html5.ext.popup.py, line 47:\n    if self.yesCallback:';
		$m.__track_lines__[48] = 'html5.ext.popup.py, line 48:\n    self.yesCallback( self )';
		$m.__track_lines__[50] = 'html5.ext.popup.py, line 50:\n    self.drop()';
		$m.__track_lines__[52] = 'html5.ext.popup.py, line 52:\n    def onNoClicked(self, *args, **kwargs ):';
		$m.__track_lines__[53] = 'html5.ext.popup.py, line 53:\n    if self.noCallback:';
		$m.__track_lines__[54] = 'html5.ext.popup.py, line 54:\n    self.noCallback( self )';
		$m.__track_lines__[56] = 'html5.ext.popup.py, line 56:\n    self.drop()';
		$m.__track_lines__[58] = 'html5.ext.popup.py, line 58:\n    class SelectDialog( Popup ):';
		$m.__track_lines__[60] = 'html5.ext.popup.py, line 60:\n    def __init__( self, prompt, items=None, title=None, okBtn="OK", cancelBtn="Cancel", forceSelect=False, *args, **kwargs ):';
		$m.__track_lines__[61] = 'html5.ext.popup.py, line 61:\n    super( SelectDialog, self ).__init__( title, *args, **kwargs )';
		$m.__track_lines__[62] = 'html5.ext.popup.py, line 62:\n    self["class"].append("selectdialog")';
		$m.__track_lines__[65] = 'html5.ext.popup.py, line 65:\n    if prompt:';
		$m.__track_lines__[66] = 'html5.ext.popup.py, line 66:\n    lbl = html5.Span()';
		$m.__track_lines__[67] = 'html5.ext.popup.py, line 67:\n    lbl[ "class" ].append( "prompt" )';
		$m.__track_lines__[68] = 'html5.ext.popup.py, line 68:\n    lbl.appendChild( html5.TextNode( prompt ) )';
		$m.__track_lines__[69] = 'html5.ext.popup.py, line 69:\n    self.appendChild( lbl )';
		$m.__track_lines__[72] = 'html5.ext.popup.py, line 72:\n    self.items = []';
		$m.__track_lines__[74] = 'html5.ext.popup.py, line 74:\n    if not forceSelect and len( items ) <= 3:';
		$m.__track_lines__[75] = 'html5.ext.popup.py, line 75:\n    for item in items:';
		$m.__track_lines__[76] = 'html5.ext.popup.py, line 76:\n    btn = html5.ext.Button( item.get( "title" ), callback=self.onAnyBtnClick )';
		$m.__track_lines__[77] = 'html5.ext.popup.py, line 77:\n    btn._callback = item.get( "callback" )';
		$m.__track_lines__[79] = 'html5.ext.popup.py, line 79:\n    if item.get( "class" ):';
		$m.__track_lines__[80] = 'html5.ext.popup.py, line 80:\n    btn[ "class" ].extend( item[ "class" ] )';
		$m.__track_lines__[82] = 'html5.ext.popup.py, line 82:\n    self.appendChild( btn )';
		$m.__track_lines__[83] = 'html5.ext.popup.py, line 83:\n    self.items.append( btn )';
		$m.__track_lines__[85] = 'html5.ext.popup.py, line 85:\n    self.select = html5.Select()';
		$m.__track_lines__[86] = 'html5.ext.popup.py, line 86:\n    self.appendChild( self.select )';
		$m.__track_lines__[88] = 'html5.ext.popup.py, line 88:\n    for i, item in enumerate( items ):';
		$m.__track_lines__[89] = 'html5.ext.popup.py, line 89:\n    opt = html5.Option()';
		$m.__track_lines__[91] = 'html5.ext.popup.py, line 91:\n    opt[ "value" ] = str( i )';
		$m.__track_lines__[92] = 'html5.ext.popup.py, line 92:\n    opt._callback = item.get( "callback" )';
		$m.__track_lines__[93] = 'html5.ext.popup.py, line 93:\n    opt.appendChild( html5.TextNode( item.get( "title" ) ) )';
		$m.__track_lines__[95] = 'html5.ext.popup.py, line 95:\n    self.select.appendChild( opt )';
		$m.__track_lines__[96] = 'html5.ext.popup.py, line 96:\n    self.items.append( opt )';
		$m.__track_lines__[98] = 'html5.ext.popup.py, line 98:\n    if okBtn:';
		$m.__track_lines__[99] = 'html5.ext.popup.py, line 99:\n    self.appendChild( html5.ext.Button( okBtn, callback=self.onOkClick ) )';
		$m.__track_lines__[101] = 'html5.ext.popup.py, line 101:\n    if cancelBtn:';
		$m.__track_lines__[102] = 'html5.ext.popup.py, line 102:\n    self.appendChild( html5.ext.Button( cancelBtn, callback=self.onCancelClick ) )';
		$m.__track_lines__[104] = 'html5.ext.popup.py, line 104:\n    def onAnyBtnClick( self, sender = None ):';
		$m.__track_lines__[105] = 'html5.ext.popup.py, line 105:\n    for btn in self.items:';
		$m.__track_lines__[106] = 'html5.ext.popup.py, line 106:\n    if btn == sender:';
		$m.__track_lines__[107] = 'html5.ext.popup.py, line 107:\n    if btn._callback:';
		$m.__track_lines__[108] = 'html5.ext.popup.py, line 108:\n    btn._callback( self )';
		$m.__track_lines__[109] = 'html5.ext.popup.py, line 109:\n    break';
		$m.__track_lines__[111] = 'html5.ext.popup.py, line 111:\n    self.items = None';
		$m.__track_lines__[112] = 'html5.ext.popup.py, line 112:\n    self.close()';
		$m.__track_lines__[114] = 'html5.ext.popup.py, line 114:\n    def onCancelClick(self, sender = None ):';
		$m.__track_lines__[115] = 'html5.ext.popup.py, line 115:\n    self.close()';
		$m.__track_lines__[117] = 'html5.ext.popup.py, line 117:\n    def onOkClick(self, sender = None ):';
		$m.__track_lines__[118] = 'html5.ext.popup.py, line 118:\n    if self.select[ "selectedIndex" ] < 0:';
		$m.__track_lines__[119] = 'html5.ext.popup.py, line 119:\n    return';
		$m.__track_lines__[121] = 'html5.ext.popup.py, line 121:\n    item = self.items[ int( self.select[ "options" ].item( self.select[ "selectedIndex" ] ).value ) ]';
		$m.__track_lines__[122] = 'html5.ext.popup.py, line 122:\n    if item._callback:';
		$m.__track_lines__[123] = 'html5.ext.popup.py, line 123:\n    item._callback( self )';
		$m.__track_lines__[125] = 'html5.ext.popup.py, line 125:\n    self.items = None';
		$m.__track_lines__[126] = 'html5.ext.popup.py, line 126:\n    self.select = None';
		$m.__track_lines__[127] = 'html5.ext.popup.py, line 127:\n    self.close()';

		var $constant_int_0 = new $p['int'](0);
		var $constant_int_3 = new $p['int'](3);
		$pyjs['track']['module']='html5.ext.popup';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=1;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['html5'] = $p['___import___']('html5', 'html5.ext');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=3;
		$m['Popup'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'html5.ext.popup';
			$cls_definition['__md5__'] = '50c47a82856ef71325c6c736d2033728';
			$pyjs['track']['lineno']=4;
			$method = $pyjs__bind_method2('__init__', function(title) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					title = arguments[1];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,2,arguments['length']-1));

					var kwargs = arguments['length'] >= 3 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50c47a82856ef71325c6c736d2033728') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof title != 'undefined') {
						if (title !== null && typeof title['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = title;
							title = arguments[2];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[2];
						}
					} else {
					}
				}
				if (typeof title == 'undefined') title=arguments['callee']['__args__'][3][1];
				var lbl;
				$pyjs['track']={'module':'html5.ext.popup', 'lineno':4};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='html5.ext.popup';
				$pyjs['track']['lineno']=4;
				$pyjs['track']['lineno']=5;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['Popup'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})();
				$pyjs['track']['lineno']=7;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('class', 'alertbox');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})();
				$pyjs['track']['lineno']=8;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](title);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})()) {
					$pyjs['track']['lineno']=9;
					lbl = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})();
					$pyjs['track']['lineno']=10;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return lbl['__getitem__']('class')['append']('title');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})();
					$pyjs['track']['lineno']=11;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return lbl['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['TextNode'](title);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})();
					$pyjs['track']['lineno']=12;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild'](lbl);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})();
				}
				$pyjs['track']['lineno']=14;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('frameDiv', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})()) : $p['setattr'](self, 'frameDiv', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})()); 
				$pyjs['track']['lineno']=15;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'frameDiv')['__setitem__']('class', 'popup');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})();
				$pyjs['track']['lineno']=17;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['frameDiv']['appendChild'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})();
				$pyjs['track']['lineno']=18;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Body']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})()['appendChild']($p['getattr'](self, 'frameDiv'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_14_err){if (!$p['isinstance']($pyjs_dbg_14_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_14_err);}throw $pyjs_dbg_14_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['title', null]]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=20;
			$method = $pyjs__bind_method2('close', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50c47a82856ef71325c6c736d2033728') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'html5.ext.popup', 'lineno':20};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='html5.ext.popup';
				$pyjs['track']['lineno']=20;
				$pyjs['track']['lineno']=21;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Body']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})()['removeChild']($p['getattr'](self, 'frameDiv'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_16_err){if (!$p['isinstance']($pyjs_dbg_16_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_16_err);}throw $pyjs_dbg_16_err;
}})();
				$pyjs['track']['lineno']=22;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('frameDiv', null) : $p['setattr'](self, 'frameDiv', null); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['close'] = $method;
			$pyjs['track']['lineno']=3;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('Popup', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=24;
		$m['YesNoDialog'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'html5.ext.popup';
			$cls_definition['__md5__'] = '7269b0adae0a92860a3c50dc2a9affb7';
			$pyjs['track']['lineno']=25;
			$method = $pyjs__bind_method2('__init__', function(question, title, yesCallback, noCallback, yesLabel, noLabel) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,6,arguments['length']-1));

					var kwargs = arguments['length'] >= 7 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					question = arguments[1];
					title = arguments[2];
					yesCallback = arguments[3];
					noCallback = arguments[4];
					yesLabel = arguments[5];
					noLabel = arguments[6];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,7,arguments['length']-1));

					var kwargs = arguments['length'] >= 8 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '7269b0adae0a92860a3c50dc2a9affb7') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof noLabel != 'undefined') {
						if (noLabel !== null && typeof noLabel['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = noLabel;
							noLabel = arguments[7];
						}
					} else 					if (typeof yesLabel != 'undefined') {
						if (yesLabel !== null && typeof yesLabel['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = yesLabel;
							yesLabel = arguments[7];
						}
					} else 					if (typeof noCallback != 'undefined') {
						if (noCallback !== null && typeof noCallback['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = noCallback;
							noCallback = arguments[7];
						}
					} else 					if (typeof yesCallback != 'undefined') {
						if (yesCallback !== null && typeof yesCallback['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = yesCallback;
							yesCallback = arguments[7];
						}
					} else 					if (typeof title != 'undefined') {
						if (title !== null && typeof title['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = title;
							title = arguments[7];
						}
					} else 					if (typeof question != 'undefined') {
						if (question !== null && typeof question['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = question;
							question = arguments[7];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[7];
						}
					} else {
					}
				}
				if (typeof title == 'undefined') title=arguments['callee']['__args__'][4][1];
				if (typeof yesCallback == 'undefined') yesCallback=arguments['callee']['__args__'][5][1];
				if (typeof noCallback == 'undefined') noCallback=arguments['callee']['__args__'][6][1];
				if (typeof yesLabel == 'undefined') yesLabel=arguments['callee']['__args__'][7][1];
				if (typeof noLabel == 'undefined') noLabel=arguments['callee']['__args__'][8][1];
				var lbl,btnYes,btnNo;
				$pyjs['track']={'module':'html5.ext.popup', 'lineno':25};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='html5.ext.popup';
				$pyjs['track']['lineno']=25;
				$pyjs['track']['lineno']=26;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['YesNoDialog'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_17_err){if (!$p['isinstance']($pyjs_dbg_17_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_17_err);}throw $pyjs_dbg_17_err;
}})(), '__init__', args, kwargs, [{}, title]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})();
				$pyjs['track']['lineno']=27;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__getitem__']('class')['append']('yesnodialog');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_19_err){if (!$p['isinstance']($pyjs_dbg_19_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_19_err);}throw $pyjs_dbg_19_err;
}})();
				$pyjs['track']['lineno']=28;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('yesCallback', yesCallback) : $p['setattr'](self, 'yesCallback', yesCallback); 
				$pyjs['track']['lineno']=29;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('noCallback', noCallback) : $p['setattr'](self, 'noCallback', noCallback); 
				$pyjs['track']['lineno']=30;
				lbl = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})();
				$pyjs['track']['lineno']=31;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return lbl['__getitem__']('class')['append']('question');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})();
				$pyjs['track']['lineno']=32;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return lbl['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['TextNode'](question);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})();
				$pyjs['track']['lineno']=33;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](lbl);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})();
				$pyjs['track']['lineno']=34;
				btnYes = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'onYesClicked')}, yesLabel]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})();
				$pyjs['track']['lineno']=35;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return btnYes['__getitem__']('class')['append']('btn_yes');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_26_err){if (!$p['isinstance']($pyjs_dbg_26_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_26_err);}throw $pyjs_dbg_26_err;
}})();
				$pyjs['track']['lineno']=36;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](btnYes);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_27_err){if (!$p['isinstance']($pyjs_dbg_27_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_27_err);}throw $pyjs_dbg_27_err;
}})();
				$pyjs['track']['lineno']=37;
				btnNo = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'onNoClicked')}, noLabel]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_28_err){if (!$p['isinstance']($pyjs_dbg_28_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_28_err);}throw $pyjs_dbg_28_err;
}})();
				$pyjs['track']['lineno']=38;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return btnNo['__getitem__']('class')['append']('btn_no');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_29_err){if (!$p['isinstance']($pyjs_dbg_29_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_29_err);}throw $pyjs_dbg_29_err;
}})();
				$pyjs['track']['lineno']=39;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](btnNo);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_30_err){if (!$p['isinstance']($pyjs_dbg_30_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_30_err);}throw $pyjs_dbg_30_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['question'],['title', null],['yesCallback', null],['noCallback', null],['yesLabel', 'Yes'],['noLabel', 'No']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=41;
			$method = $pyjs__bind_method2('drop', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '7269b0adae0a92860a3c50dc2a9affb7') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'html5.ext.popup', 'lineno':41};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='html5.ext.popup';
				$pyjs['track']['lineno']=41;
				$pyjs['track']['lineno']=42;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('yesCallback', null) : $p['setattr'](self, 'yesCallback', null); 
				$pyjs['track']['lineno']=43;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('noCallback', null) : $p['setattr'](self, 'noCallback', null); 
				$pyjs['track']['lineno']=44;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['close']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_31_err){if (!$p['isinstance']($pyjs_dbg_31_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_31_err);}throw $pyjs_dbg_31_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['drop'] = $method;
			$pyjs['track']['lineno']=46;
			$method = $pyjs__bind_method2('onYesClicked', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '7269b0adae0a92860a3c50dc2a9affb7') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'html5.ext.popup', 'lineno':46};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='html5.ext.popup';
				$pyjs['track']['lineno']=46;
				$pyjs['track']['lineno']=47;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'yesCallback'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_32_err){if (!$p['isinstance']($pyjs_dbg_32_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_32_err);}throw $pyjs_dbg_32_err;
}})()) {
					$pyjs['track']['lineno']=48;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['yesCallback'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_33_err){if (!$p['isinstance']($pyjs_dbg_33_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_33_err);}throw $pyjs_dbg_33_err;
}})();
				}
				$pyjs['track']['lineno']=50;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['drop']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_34_err){if (!$p['isinstance']($pyjs_dbg_34_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_34_err);}throw $pyjs_dbg_34_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['onYesClicked'] = $method;
			$pyjs['track']['lineno']=52;
			$method = $pyjs__bind_method2('onNoClicked', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '7269b0adae0a92860a3c50dc2a9affb7') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'html5.ext.popup', 'lineno':52};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='html5.ext.popup';
				$pyjs['track']['lineno']=52;
				$pyjs['track']['lineno']=53;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'noCallback'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})()) {
					$pyjs['track']['lineno']=54;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['noCallback'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_36_err){if (!$p['isinstance']($pyjs_dbg_36_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_36_err);}throw $pyjs_dbg_36_err;
}})();
				}
				$pyjs['track']['lineno']=56;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['drop']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_37_err){if (!$p['isinstance']($pyjs_dbg_37_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_37_err);}throw $pyjs_dbg_37_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['onNoClicked'] = $method;
			$pyjs['track']['lineno']=24;
			var $bases = new Array($m['Popup']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('YesNoDialog', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=58;
		$m['SelectDialog'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'html5.ext.popup';
			$cls_definition['__md5__'] = '3a1c0c9991248d5562c4ca7284fb498b';
			$pyjs['track']['lineno']=60;
			$method = $pyjs__bind_method2('__init__', function(prompt, items, title, okBtn, cancelBtn, forceSelect) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,6,arguments['length']-1));

					var kwargs = arguments['length'] >= 7 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					prompt = arguments[1];
					items = arguments[2];
					title = arguments[3];
					okBtn = arguments[4];
					cancelBtn = arguments[5];
					forceSelect = arguments[6];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,7,arguments['length']-1));

					var kwargs = arguments['length'] >= 8 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '3a1c0c9991248d5562c4ca7284fb498b') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof forceSelect != 'undefined') {
						if (forceSelect !== null && typeof forceSelect['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = forceSelect;
							forceSelect = arguments[7];
						}
					} else 					if (typeof cancelBtn != 'undefined') {
						if (cancelBtn !== null && typeof cancelBtn['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = cancelBtn;
							cancelBtn = arguments[7];
						}
					} else 					if (typeof okBtn != 'undefined') {
						if (okBtn !== null && typeof okBtn['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = okBtn;
							okBtn = arguments[7];
						}
					} else 					if (typeof title != 'undefined') {
						if (title !== null && typeof title['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = title;
							title = arguments[7];
						}
					} else 					if (typeof items != 'undefined') {
						if (items !== null && typeof items['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = items;
							items = arguments[7];
						}
					} else 					if (typeof prompt != 'undefined') {
						if (prompt !== null && typeof prompt['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = prompt;
							prompt = arguments[7];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[7];
						}
					} else {
					}
				}
				if (typeof items == 'undefined') items=arguments['callee']['__args__'][4][1];
				if (typeof title == 'undefined') title=arguments['callee']['__args__'][5][1];
				if (typeof okBtn == 'undefined') okBtn=arguments['callee']['__args__'][6][1];
				if (typeof cancelBtn == 'undefined') cancelBtn=arguments['callee']['__args__'][7][1];
				if (typeof forceSelect == 'undefined') forceSelect=arguments['callee']['__args__'][8][1];
				var $iter1_iter,$iter2_type,btn,lbl,$iter2_iter,$iter1_array,opt,$iter1_nextval,$and1,$and2,$iter2_idx,$iter2_nextval,$iter1_type,i,item,$iter1_idx,$pyjs__trackstack_size_1,$iter2_array;
				$pyjs['track']={'module':'html5.ext.popup', 'lineno':60};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='html5.ext.popup';
				$pyjs['track']['lineno']=60;
				$pyjs['track']['lineno']=61;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectDialog'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_38_err){if (!$p['isinstance']($pyjs_dbg_38_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_38_err);}throw $pyjs_dbg_38_err;
}})(), '__init__', args, kwargs, [{}, title]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_39_err){if (!$p['isinstance']($pyjs_dbg_39_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_39_err);}throw $pyjs_dbg_39_err;
}})();
				$pyjs['track']['lineno']=62;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__getitem__']('class')['append']('selectdialog');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_40_err){if (!$p['isinstance']($pyjs_dbg_40_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_40_err);}throw $pyjs_dbg_40_err;
}})();
				$pyjs['track']['lineno']=65;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](prompt);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_41_err){if (!$p['isinstance']($pyjs_dbg_41_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_41_err);}throw $pyjs_dbg_41_err;
}})()) {
					$pyjs['track']['lineno']=66;
					lbl = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_42_err){if (!$p['isinstance']($pyjs_dbg_42_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_42_err);}throw $pyjs_dbg_42_err;
}})();
					$pyjs['track']['lineno']=67;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return lbl['__getitem__']('class')['append']('prompt');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_43_err){if (!$p['isinstance']($pyjs_dbg_43_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_43_err);}throw $pyjs_dbg_43_err;
}})();
					$pyjs['track']['lineno']=68;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return lbl['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['TextNode'](prompt);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_44_err){if (!$p['isinstance']($pyjs_dbg_44_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_44_err);}throw $pyjs_dbg_44_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_45_err){if (!$p['isinstance']($pyjs_dbg_45_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_45_err);}throw $pyjs_dbg_45_err;
}})();
					$pyjs['track']['lineno']=69;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild'](lbl);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_46_err){if (!$p['isinstance']($pyjs_dbg_46_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_46_err);}throw $pyjs_dbg_46_err;
}})();
				}
				$pyjs['track']['lineno']=72;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('items', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})()) : $p['setattr'](self, 'items', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})()); 
				$pyjs['track']['lineno']=74;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and1=!$p['bool'](forceSelect))?($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](items);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_48_err){if (!$p['isinstance']($pyjs_dbg_48_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_48_err);}throw $pyjs_dbg_48_err;
}})(), $constant_int_3) < 1):$and1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_49_err){if (!$p['isinstance']($pyjs_dbg_49_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_49_err);}throw $pyjs_dbg_49_err;
}})()) {
					$pyjs['track']['lineno']=75;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter1_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return items;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_50_err){if (!$p['isinstance']($pyjs_dbg_50_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_50_err);}throw $pyjs_dbg_50_err;
}})();
					$iter1_nextval=$p['__iter_prepare']($iter1_iter,false);
					while (typeof($p['__wrapped_next']($iter1_nextval)['$nextval']) != 'undefined') {
						item = $iter1_nextval['$nextval'];
						$pyjs['track']['lineno']=76;
						btn = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'onAnyBtnClick')}, (function(){try{try{$pyjs['in_try_except'] += 1;
						return item['get']('title');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_51_err){if (!$p['isinstance']($pyjs_dbg_51_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_51_err);}throw $pyjs_dbg_51_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_52_err){if (!$p['isinstance']($pyjs_dbg_52_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_52_err);}throw $pyjs_dbg_52_err;
}})();
						$pyjs['track']['lineno']=77;
						btn['__is_instance__'] && typeof btn['__setattr__'] == 'function' ? btn['__setattr__']('_callback', (function(){try{try{$pyjs['in_try_except'] += 1;
						return item['get']('callback');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_53_err){if (!$p['isinstance']($pyjs_dbg_53_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_53_err);}throw $pyjs_dbg_53_err;
}})()) : $p['setattr'](btn, '_callback', (function(){try{try{$pyjs['in_try_except'] += 1;
						return item['get']('callback');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_53_err){if (!$p['isinstance']($pyjs_dbg_53_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_53_err);}throw $pyjs_dbg_53_err;
}})()); 
						$pyjs['track']['lineno']=79;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return item['get']('class');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_54_err){if (!$p['isinstance']($pyjs_dbg_54_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_54_err);}throw $pyjs_dbg_54_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_55_err){if (!$p['isinstance']($pyjs_dbg_55_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_55_err);}throw $pyjs_dbg_55_err;
}})()) {
							$pyjs['track']['lineno']=80;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return btn['__getitem__']('class')['extend'](item['__getitem__']('class'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_56_err){if (!$p['isinstance']($pyjs_dbg_56_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_56_err);}throw $pyjs_dbg_56_err;
}})();
						}
						$pyjs['track']['lineno']=82;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['appendChild'](btn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_57_err){if (!$p['isinstance']($pyjs_dbg_57_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_57_err);}throw $pyjs_dbg_57_err;
}})();
						$pyjs['track']['lineno']=83;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['items']['append'](btn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_58_err){if (!$p['isinstance']($pyjs_dbg_58_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_58_err);}throw $pyjs_dbg_58_err;
}})();
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='html5.ext.popup';
				}
				else {
					$pyjs['track']['lineno']=85;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('select', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Select']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_59_err){if (!$p['isinstance']($pyjs_dbg_59_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_59_err);}throw $pyjs_dbg_59_err;
}})()) : $p['setattr'](self, 'select', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Select']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_59_err){if (!$p['isinstance']($pyjs_dbg_59_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_59_err);}throw $pyjs_dbg_59_err;
}})()); 
					$pyjs['track']['lineno']=86;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'select'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_60_err){if (!$p['isinstance']($pyjs_dbg_60_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_60_err);}throw $pyjs_dbg_60_err;
}})();
					$pyjs['track']['lineno']=88;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter2_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['enumerate'](items);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_62_err){if (!$p['isinstance']($pyjs_dbg_62_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_62_err);}throw $pyjs_dbg_62_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_63_err){if (!$p['isinstance']($pyjs_dbg_63_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_63_err);}throw $pyjs_dbg_63_err;
}})();
					$iter2_nextval=$p['__iter_prepare']($iter2_iter,false);
					while (typeof($p['__wrapped_next']($iter2_nextval)['$nextval']) != 'undefined') {
						var $tupleassign1 = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['__ass_unpack']($iter2_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_61_err){if (!$p['isinstance']($pyjs_dbg_61_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_61_err);}throw $pyjs_dbg_61_err;
}})();
						i = $tupleassign1[0];
						item = $tupleassign1[1];
						$pyjs['track']['lineno']=89;
						opt = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Option']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})();
						$pyjs['track']['lineno']=91;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return opt['__setitem__']('value', (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['str'](i);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_65_err){if (!$p['isinstance']($pyjs_dbg_65_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_65_err);}throw $pyjs_dbg_65_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_66_err){if (!$p['isinstance']($pyjs_dbg_66_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_66_err);}throw $pyjs_dbg_66_err;
}})();
						$pyjs['track']['lineno']=92;
						opt['__is_instance__'] && typeof opt['__setattr__'] == 'function' ? opt['__setattr__']('_callback', (function(){try{try{$pyjs['in_try_except'] += 1;
						return item['get']('callback');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})()) : $p['setattr'](opt, '_callback', (function(){try{try{$pyjs['in_try_except'] += 1;
						return item['get']('callback');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})()); 
						$pyjs['track']['lineno']=93;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return opt['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['TextNode']((function(){try{try{$pyjs['in_try_except'] += 1;
						return item['get']('title');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_68_err){if (!$p['isinstance']($pyjs_dbg_68_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_68_err);}throw $pyjs_dbg_68_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_69_err){if (!$p['isinstance']($pyjs_dbg_69_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_69_err);}throw $pyjs_dbg_69_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_70_err){if (!$p['isinstance']($pyjs_dbg_70_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_70_err);}throw $pyjs_dbg_70_err;
}})();
						$pyjs['track']['lineno']=95;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['select']['appendChild'](opt);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_71_err){if (!$p['isinstance']($pyjs_dbg_71_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_71_err);}throw $pyjs_dbg_71_err;
}})();
						$pyjs['track']['lineno']=96;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['items']['append'](opt);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_72_err){if (!$p['isinstance']($pyjs_dbg_72_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_72_err);}throw $pyjs_dbg_72_err;
}})();
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='html5.ext.popup';
					$pyjs['track']['lineno']=98;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](okBtn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_73_err){if (!$p['isinstance']($pyjs_dbg_73_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_73_err);}throw $pyjs_dbg_73_err;
}})()) {
						$pyjs['track']['lineno']=99;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'onOkClick')}, okBtn]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_74_err){if (!$p['isinstance']($pyjs_dbg_74_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_74_err);}throw $pyjs_dbg_74_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})();
					}
					$pyjs['track']['lineno']=101;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](cancelBtn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})()) {
						$pyjs['track']['lineno']=102;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'onCancelClick')}, cancelBtn]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_77_err){if (!$p['isinstance']($pyjs_dbg_77_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_77_err);}throw $pyjs_dbg_77_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_78_err){if (!$p['isinstance']($pyjs_dbg_78_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_78_err);}throw $pyjs_dbg_78_err;
}})();
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['prompt'],['items', null],['title', null],['okBtn', 'OK'],['cancelBtn', 'Cancel'],['forceSelect', false]]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=104;
			$method = $pyjs__bind_method2('onAnyBtnClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '3a1c0c9991248d5562c4ca7284fb498b') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];
				var $iter3_idx,$iter3_type,$iter3_iter,$iter3_array,btn,$pyjs__trackstack_size_1,$iter3_nextval;
				$pyjs['track']={'module':'html5.ext.popup', 'lineno':104};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='html5.ext.popup';
				$pyjs['track']['lineno']=104;
				$pyjs['track']['lineno']=105;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter3_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'items');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_79_err){if (!$p['isinstance']($pyjs_dbg_79_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_79_err);}throw $pyjs_dbg_79_err;
}})();
				$iter3_nextval=$p['__iter_prepare']($iter3_iter,false);
				while (typeof($p['__wrapped_next']($iter3_nextval)['$nextval']) != 'undefined') {
					btn = $iter3_nextval['$nextval'];
					$pyjs['track']['lineno']=106;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_eq'](btn, sender));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_80_err){if (!$p['isinstance']($pyjs_dbg_80_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_80_err);}throw $pyjs_dbg_80_err;
}})()) {
						$pyjs['track']['lineno']=107;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']($p['getattr'](btn, '_callback'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_81_err){if (!$p['isinstance']($pyjs_dbg_81_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_81_err);}throw $pyjs_dbg_81_err;
}})()) {
							$pyjs['track']['lineno']=108;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return btn['_callback'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_82_err){if (!$p['isinstance']($pyjs_dbg_82_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_82_err);}throw $pyjs_dbg_82_err;
}})();
						}
						$pyjs['track']['lineno']=109;
						break;
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='html5.ext.popup';
				$pyjs['track']['lineno']=111;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('items', null) : $p['setattr'](self, 'items', null); 
				$pyjs['track']['lineno']=112;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['close']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_83_err){if (!$p['isinstance']($pyjs_dbg_83_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_83_err);}throw $pyjs_dbg_83_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onAnyBtnClick'] = $method;
			$pyjs['track']['lineno']=114;
			$method = $pyjs__bind_method2('onCancelClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '3a1c0c9991248d5562c4ca7284fb498b') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];

				$pyjs['track']={'module':'html5.ext.popup', 'lineno':114};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='html5.ext.popup';
				$pyjs['track']['lineno']=114;
				$pyjs['track']['lineno']=115;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['close']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_84_err){if (!$p['isinstance']($pyjs_dbg_84_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_84_err);}throw $pyjs_dbg_84_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onCancelClick'] = $method;
			$pyjs['track']['lineno']=117;
			$method = $pyjs__bind_method2('onOkClick', function(sender) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					sender = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '3a1c0c9991248d5562c4ca7284fb498b') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof sender == 'undefined') sender=arguments['callee']['__args__'][3][1];
				var item;
				$pyjs['track']={'module':'html5.ext.popup', 'lineno':117};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='html5.ext.popup';
				$pyjs['track']['lineno']=117;
				$pyjs['track']['lineno']=118;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp']($p['getattr'](self, 'select')['__getitem__']('selectedIndex'), $constant_int_0) == -1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})()) {
					$pyjs['track']['lineno']=119;
					$pyjs['track']['lineno']=119;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=121;
				item = $p['getattr'](self, 'items')['__getitem__']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['int']($p['getattr']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'select')['__getitem__']('options')['item']($p['getattr'](self, 'select')['__getitem__']('selectedIndex'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})(), 'value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})());
				$pyjs['track']['lineno']=122;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](item, '_callback'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})()) {
					$pyjs['track']['lineno']=123;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return item['_callback'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_89_err){if (!$p['isinstance']($pyjs_dbg_89_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_89_err);}throw $pyjs_dbg_89_err;
}})();
				}
				$pyjs['track']['lineno']=125;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('items', null) : $p['setattr'](self, 'items', null); 
				$pyjs['track']['lineno']=126;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('select', null) : $p['setattr'](self, 'select', null); 
				$pyjs['track']['lineno']=127;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['close']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['sender', null]]);
			$cls_definition['onOkClick'] = $method;
			$pyjs['track']['lineno']=58;
			var $bases = new Array($m['Popup']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('SelectDialog', $p['tuple']($bases), $data);
		})();
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end html5.ext.popup */


/* end module: html5.ext.popup */


/*
PYJS_DEPS: ['html5']
*/
