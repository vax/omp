/* start module: widgets.table */
$pyjs['loaded_modules']['widgets.table'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['widgets.table']['__was_initialized__']) return $pyjs['loaded_modules']['widgets.table'];
	if(typeof $pyjs['loaded_modules']['widgets'] == 'undefined' || !$pyjs['loaded_modules']['widgets']['__was_initialized__']) $p['___import___']('widgets', null);
	var $m = $pyjs['loaded_modules']['widgets.table'];
	$m['__repr__'] = function() { return '<module: widgets.table>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'widgets.table';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	$pyjs['loaded_modules']['widgets']['table'] = $pyjs['loaded_modules']['widgets.table'];
	try {
		$m.__track_lines__[1] = 'widgets.table.py, line 1:\n    import html5, utils';
		$m.__track_lines__[2] = 'widgets.table.py, line 2:\n    from event import EventDispatcher';
		$m.__track_lines__[3] = 'widgets.table.py, line 3:\n    from html5.keycodes import *';
		$m.__track_lines__[5] = 'widgets.table.py, line 5:\n    class SelectTable( html5.Table ):';
		$m.__track_lines__[21] = 'widgets.table.py, line 21:\n    def __init__(self, checkboxes=False, indexes=False, *args, **kwargs):';
		$m.__track_lines__[22] = 'widgets.table.py, line 22:\n    super(SelectTable,self).__init__(*args,**kwargs)';
		$m.__track_lines__[25] = 'widgets.table.py, line 25:\n    self.selectionChangedEvent = EventDispatcher("selectionChanged")';
		$m.__track_lines__[26] = 'widgets.table.py, line 26:\n    self.selectionActivatedEvent = EventDispatcher("selectionActivated")';
		$m.__track_lines__[27] = 'widgets.table.py, line 27:\n    self.cursorMovedEvent = EventDispatcher("cursorMoved")';
		$m.__track_lines__[28] = 'widgets.table.py, line 28:\n    self.tableChangedEvent = EventDispatcher("tableChanged")';
		$m.__track_lines__[30] = 'widgets.table.py, line 30:\n    self.sinkEvent( "onDblClick", "onMouseMove", "onMouseDown",';
		$m.__track_lines__[34] = 'widgets.table.py, line 34:\n    self["tabindex"] = 1';
		$m.__track_lines__[36] = 'widgets.table.py, line 36:\n    self._selectedRows = [] # List of row-indexes currently selected';
		$m.__track_lines__[37] = 'widgets.table.py, line 37:\n    self._currentRow = None # Rowindex of the cursor row';
		$m.__track_lines__[38] = 'widgets.table.py, line 38:\n    self._isMouseDown = False # Tracks status of the left mouse button';
		$m.__track_lines__[39] = 'widgets.table.py, line 39:\n    self._isCtlPressed = False # Tracks status of the ctrl key';
		$m.__track_lines__[40] = 'widgets.table.py, line 40:\n    self._ctlStartRow = None # Stores the row where a multi-selection (using the ctrl key) started';
		$m.__track_lines__[41] = 'widgets.table.py, line 41:\n    self._selectionChangedListener = [] # All objects getting informed when the selection changes';
		$m.__track_lines__[42] = 'widgets.table.py, line 42:\n    self._selectionActivatedListeners = [] # All objects getting informed when items are selected';
		$m.__track_lines__[43] = 'widgets.table.py, line 43:\n    self._cursorMovedListeners = [] # All objects getting informed when the cursor moves';
		$m.__track_lines__[45] = 'widgets.table.py, line 45:\n    self.indexes = indexes';
		$m.__track_lines__[46] = 'widgets.table.py, line 46:\n    self.indexes_col = 0 if indexes else -1';
		$m.__track_lines__[48] = 'widgets.table.py, line 48:\n    self._checkboxes = {} # The checkbox items per row (better to use a dict!)';
		$m.__track_lines__[49] = 'widgets.table.py, line 49:\n    self.checkboxes = checkboxes';
		$m.__track_lines__[50] = 'widgets.table.py, line 50:\n    self.checkboxes_col = (self.indexes_col + 1) if checkboxes else -1';
		$m.__track_lines__[52] = 'widgets.table.py, line 52:\n    def onAttach(self):';
		$m.__track_lines__[53] = 'widgets.table.py, line 53:\n    super(SelectTable, self).onAttach()';
		$m.__track_lines__[54] = 'widgets.table.py, line 54:\n    self.focus()';
		$m.__track_lines__[56] = 'widgets.table.py, line 56:\n    def setHeader(self, headers):';
		$m.__track_lines__[63] = 'widgets.table.py, line 63:\n    tr = html5.Tr()';
		$m.__track_lines__[66] = 'widgets.table.py, line 66:\n    if self.indexes:';
		$m.__track_lines__[67] = 'widgets.table.py, line 67:\n    th = html5.Th()';
		$m.__track_lines__[68] = 'widgets.table.py, line 68:\n    th[ "class" ] = "index"';
		$m.__track_lines__[69] = 'widgets.table.py, line 69:\n    tr.appendChild( th )';
		$m.__track_lines__[72] = 'widgets.table.py, line 72:\n    if self.checkboxes:';
		$m.__track_lines__[73] = 'widgets.table.py, line 73:\n    th = html5.Th() # fixme..';
		$m.__track_lines__[74] = 'widgets.table.py, line 74:\n    th[ "class" ] = "check"';
		$m.__track_lines__[75] = 'widgets.table.py, line 75:\n    tr.appendChild( th )';
		$m.__track_lines__[78] = 'widgets.table.py, line 78:\n    for head in headers:';
		$m.__track_lines__[79] = 'widgets.table.py, line 79:\n    th = html5.Th()';
		$m.__track_lines__[80] = 'widgets.table.py, line 80:\n    th.appendChild( html5.TextNode( head ) )';
		$m.__track_lines__[81] = 'widgets.table.py, line 81:\n    tr.appendChild( th )';
		$m.__track_lines__[83] = 'widgets.table.py, line 83:\n    self.head.removeAllChildren()';
		$m.__track_lines__[84] = 'widgets.table.py, line 84:\n    self.head.appendChild( tr )';
		$m.__track_lines__[86] = 'widgets.table.py, line 86:\n    def getTrByIndex(self, idx):';
		$m.__track_lines__[93] = 'widgets.table.py, line 93:\n    for c in self.body._children:';
		$m.__track_lines__[94] = 'widgets.table.py, line 94:\n    if idx <= 0:';
		$m.__track_lines__[95] = 'widgets.table.py, line 95:\n    return c';
		$m.__track_lines__[96] = 'widgets.table.py, line 96:\n    idx -= c["rowspan"]';
		$m.__track_lines__[98] = 'widgets.table.py, line 98:\n    return None';
		$m.__track_lines__[100] = 'widgets.table.py, line 100:\n    def getIndexByTr(self,tr):';
		$m.__track_lines__[108] = 'widgets.table.py, line 108:\n    idx = 0';
		$m.__track_lines__[109] = 'widgets.table.py, line 109:\n    for c in self.body._children:';
		$m.__track_lines__[110] = 'widgets.table.py, line 110:\n    if c.element == tr:';
		$m.__track_lines__[111] = 'widgets.table.py, line 111:\n    return( idx )';
		$m.__track_lines__[112] = 'widgets.table.py, line 112:\n    idx += c["rowspan"]';
		$m.__track_lines__[113] = 'widgets.table.py, line 113:\n    return( idx )';
		$m.__track_lines__[115] = 'widgets.table.py, line 115:\n    def _rowForEvent(self, event ):';
		$m.__track_lines__[119] = 'widgets.table.py, line 119:\n    try:';
		$m.__track_lines__[121] = 'widgets.table.py, line 121:\n    tc = event.srcElement';
		$m.__track_lines__[124] = 'widgets.table.py, line 124:\n    tc = event.target';
		$m.__track_lines__[126] = 'widgets.table.py, line 126:\n    if tc is None:';
		$m.__track_lines__[127] = 'widgets.table.py, line 127:\n    return( None )';
		$m.__track_lines__[129] = 'widgets.table.py, line 129:\n    tr = tc.parentElement';
		$m.__track_lines__[131] = 'widgets.table.py, line 131:\n    while( tr.parentElement is not None ):';
		$m.__track_lines__[132] = 'widgets.table.py, line 132:\n    if tr.parentElement == self.body.element:';
		$m.__track_lines__[133] = 'widgets.table.py, line 133:\n    return( tr )';
		$m.__track_lines__[134] = 'widgets.table.py, line 134:\n    tr = tr.parentElement';
		$m.__track_lines__[136] = 'widgets.table.py, line 136:\n    return( None )';
		$m.__track_lines__[138] = 'widgets.table.py, line 138:\n    def onChange(self, event):';
		$m.__track_lines__[139] = 'widgets.table.py, line 139:\n    tr = self._rowForEvent( event )';
		$m.__track_lines__[140] = 'widgets.table.py, line 140:\n    if tr is None:';
		$m.__track_lines__[141] = 'widgets.table.py, line 141:\n    return';
		$m.__track_lines__[143] = 'widgets.table.py, line 143:\n    row = self.getIndexByTr( tr )';
		$m.__track_lines__[145] = 'widgets.table.py, line 145:\n    if ( self.checkboxes';
		$m.__track_lines__[148] = 'widgets.table.py, line 148:\n    self._checkboxes[ row ][ "checked" ] = row in self._selectedRows';
		$m.__track_lines__[150] = 'widgets.table.py, line 150:\n    def onMouseDown(self, event):';
		$m.__track_lines__[151] = 'widgets.table.py, line 151:\n    tr = self._rowForEvent( event )';
		$m.__track_lines__[152] = 'widgets.table.py, line 152:\n    if tr is None:';
		$m.__track_lines__[153] = 'widgets.table.py, line 153:\n    return';
		$m.__track_lines__[155] = 'widgets.table.py, line 155:\n    row = self.getIndexByTr( tr )';
		$m.__track_lines__[157] = 'widgets.table.py, line 157:\n    if self._isCtlPressed:';
		$m.__track_lines__[158] = 'widgets.table.py, line 158:\n    if row in self._selectedRows:';
		$m.__track_lines__[159] = 'widgets.table.py, line 159:\n    self.removeSelectedRow( row )';
		$m.__track_lines__[161] = 'widgets.table.py, line 161:\n    self.addSelectedRow( row )';
		$m.__track_lines__[167] = 'widgets.table.py, line 167:\n    if row in self._selectedRows:';
		$m.__track_lines__[168] = 'widgets.table.py, line 168:\n    self.removeSelectedRow( row )';
		$m.__track_lines__[170] = 'widgets.table.py, line 170:\n    self.addSelectedRow( row )';
		$m.__track_lines__[173] = 'widgets.table.py, line 173:\n    self._isMouseDown = True';
		$m.__track_lines__[175] = 'widgets.table.py, line 175:\n    if self.checkboxes:';
		$m.__track_lines__[176] = 'widgets.table.py, line 176:\n    if row in self._selectedRows:';
		$m.__track_lines__[177] = 'widgets.table.py, line 177:\n    self.removeSelectedRow( row )';
		$m.__track_lines__[179] = 'widgets.table.py, line 179:\n    self.addSelectedRow( row )';
		$m.__track_lines__[181] = 'widgets.table.py, line 181:\n    self.setCursorRow(self.getIndexByTr(tr), removeExistingSelection=not self.checkboxes)';
		$m.__track_lines__[183] = 'widgets.table.py, line 183:\n    event.preventDefault()';
		$m.__track_lines__[184] = 'widgets.table.py, line 184:\n    self.focus()';
		$m.__track_lines__[186] = 'widgets.table.py, line 186:\n    def onMouseOut(self, event):';
		$m.__track_lines__[187] = 'widgets.table.py, line 187:\n    self._isMouseDown = False';
		$m.__track_lines__[189] = 'widgets.table.py, line 189:\n    def onMouseUp(self, event):';
		$m.__track_lines__[190] = 'widgets.table.py, line 190:\n    self._isMouseDown = False';
		$m.__track_lines__[192] = 'widgets.table.py, line 192:\n    def onMouseMove(self, event):';
		$m.__track_lines__[193] = 'widgets.table.py, line 193:\n    if self._isMouseDown:';
		$m.__track_lines__[194] = 'widgets.table.py, line 194:\n    tr = self._rowForEvent( event )';
		$m.__track_lines__[195] = 'widgets.table.py, line 195:\n    if tr is None:';
		$m.__track_lines__[196] = 'widgets.table.py, line 196:\n    return';
		$m.__track_lines__[197] = 'widgets.table.py, line 197:\n    self.addSelectedRow( self.getIndexByTr(tr) )';
		$m.__track_lines__[198] = 'widgets.table.py, line 198:\n    event.preventDefault()';
		$m.__track_lines__[200] = 'widgets.table.py, line 200:\n    def onKeyDown(self, event):';
		$m.__track_lines__[201] = 'widgets.table.py, line 201:\n    if isArrowDown(event.keyCode): #Arrow down';
		$m.__track_lines__[203] = 'widgets.table.py, line 203:\n    if self._currentRow is None:';
		$m.__track_lines__[204] = 'widgets.table.py, line 204:\n    self.setCursorRow(0)';
		$m.__track_lines__[207] = 'widgets.table.py, line 207:\n    if self._isCtlPressed:';
		$m.__track_lines__[209] = 'widgets.table.py, line 209:\n    if self._ctlStartRow > self._currentRow:';
		$m.__track_lines__[210] = 'widgets.table.py, line 210:\n    self.removeSelectedRow( self._currentRow )';
		$m.__track_lines__[212] = 'widgets.table.py, line 212:\n    self.addSelectedRow( self._currentRow )';
		$m.__track_lines__[214] = 'widgets.table.py, line 214:\n    if self._currentRow+1 < self.getRowCount():';
		$m.__track_lines__[215] = 'widgets.table.py, line 215:\n    self.addSelectedRow( self._currentRow+1 )';
		$m.__track_lines__[217] = 'widgets.table.py, line 217:\n    if self._currentRow+1 < self.getRowCount():';
		$m.__track_lines__[218] = 'widgets.table.py, line 218:\n    self.setCursorRow(self._currentRow+1,';
		$m.__track_lines__[220] = 'widgets.table.py, line 220:\n    event.preventDefault()';
		$m.__track_lines__[224] = 'widgets.table.py, line 224:\n    if self._currentRow is None:';
		$m.__track_lines__[225] = 'widgets.table.py, line 225:\n    self.setCursorRow(0)';
		$m.__track_lines__[228] = 'widgets.table.py, line 228:\n    if self._isCtlPressed: #Check if we extend a selection';
		$m.__track_lines__[229] = 'widgets.table.py, line 229:\n    if self._ctlStartRow < self._currentRow:';
		$m.__track_lines__[230] = 'widgets.table.py, line 230:\n    self.removeSelectedRow( self._currentRow )';
		$m.__track_lines__[232] = 'widgets.table.py, line 232:\n    self.addSelectedRow( self._currentRow )';
		$m.__track_lines__[234] = 'widgets.table.py, line 234:\n    if self._currentRow>0:';
		$m.__track_lines__[235] = 'widgets.table.py, line 235:\n    self.addSelectedRow( self._currentRow-1 )';
		$m.__track_lines__[237] = 'widgets.table.py, line 237:\n    if self._currentRow>0: #Move the cursor if possible';
		$m.__track_lines__[238] = 'widgets.table.py, line 238:\n    self.setCursorRow(self._currentRow-1,';
		$m.__track_lines__[241] = 'widgets.table.py, line 241:\n    event.preventDefault()';
		$m.__track_lines__[245] = 'widgets.table.py, line 245:\n    if len( self._selectedRows )>0:';
		$m.__track_lines__[246] = 'widgets.table.py, line 246:\n    self.selectionActivatedEvent.fire( self, self._selectedRows )';
		$m.__track_lines__[247] = 'widgets.table.py, line 247:\n    event.preventDefault()';
		$m.__track_lines__[248] = 'widgets.table.py, line 248:\n    return';
		$m.__track_lines__[250] = 'widgets.table.py, line 250:\n    if self._currentRow is not None:';
		$m.__track_lines__[251] = 'widgets.table.py, line 251:\n    self.selectionActivatedEvent.fire( self, [self._currentRow] )';
		$m.__track_lines__[252] = 'widgets.table.py, line 252:\n    event.preventDefault()';
		$m.__track_lines__[253] = 'widgets.table.py, line 253:\n    return';
		$m.__track_lines__[256] = 'widgets.table.py, line 256:\n    self._isCtlPressed = True';
		$m.__track_lines__[257] = 'widgets.table.py, line 257:\n    self._ctlStartRow = self._currentRow or 0';
		$m.__track_lines__[259] = 'widgets.table.py, line 259:\n    if self._currentRow is not None and not self._currentRow in self._selectedRows:';
		$m.__track_lines__[260] = 'widgets.table.py, line 260:\n    self.addSelectedRow( self._currentRow )';
		$m.__track_lines__[261] = 'widgets.table.py, line 261:\n    self.setCursorRow( None, removeExistingSelection=False )';
		$m.__track_lines__[263] = 'widgets.table.py, line 263:\n    def onKeyUp(self, event):';
		$m.__track_lines__[264] = 'widgets.table.py, line 264:\n    if isSingleSelectionKey( event.keyCode ):';
		$m.__track_lines__[265] = 'widgets.table.py, line 265:\n    self._isCtlPressed = False';
		$m.__track_lines__[266] = 'widgets.table.py, line 266:\n    self._ctlStartRow = None';
		$m.__track_lines__[268] = 'widgets.table.py, line 268:\n    def onDblClick(self, event):';
		$m.__track_lines__[269] = 'widgets.table.py, line 269:\n    if self._currentRow is not None:';
		$m.__track_lines__[270] = 'widgets.table.py, line 270:\n    self.selectionActivatedEvent.fire( self, [self._currentRow] )';
		$m.__track_lines__[271] = 'widgets.table.py, line 271:\n    event.preventDefault()';
		$m.__track_lines__[273] = 'widgets.table.py, line 273:\n    def addSelectedRow(self, row):';
		$m.__track_lines__[277] = 'widgets.table.py, line 277:\n    if row in self._selectedRows:';
		$m.__track_lines__[278] = 'widgets.table.py, line 278:\n    return';
		$m.__track_lines__[280] = 'widgets.table.py, line 280:\n    self._selectedRows.append( row )';
		$m.__track_lines__[282] = 'widgets.table.py, line 282:\n    tr = self.getTrByIndex( row )';
		$m.__track_lines__[283] = 'widgets.table.py, line 283:\n    tr["class"].append("is_selected")';
		$m.__track_lines__[285] = 'widgets.table.py, line 285:\n    if self.checkboxes:';
		$m.__track_lines__[286] = 'widgets.table.py, line 286:\n    self._checkboxes[ row ][ "checked" ] = True';
		$m.__track_lines__[288] = 'widgets.table.py, line 288:\n    self.selectionChangedEvent.fire( self, self.getCurrentSelection() )';
		$m.__track_lines__[290] = 'widgets.table.py, line 290:\n    print("Currently selected rows: %s" % str(self._selectedRows))';
		$m.__track_lines__[292] = 'widgets.table.py, line 292:\n    def removeSelectedRow(self, row):';
		$m.__track_lines__[298] = 'widgets.table.py, line 298:\n    if not row in self._selectedRows:';
		$m.__track_lines__[299] = 'widgets.table.py, line 299:\n    return';
		$m.__track_lines__[301] = 'widgets.table.py, line 301:\n    self._selectedRows.remove( row )';
		$m.__track_lines__[303] = 'widgets.table.py, line 303:\n    tr = self.getTrByIndex( row )';
		$m.__track_lines__[304] = 'widgets.table.py, line 304:\n    tr["class"].remove("is_selected")';
		$m.__track_lines__[306] = 'widgets.table.py, line 306:\n    if self.checkboxes:';
		$m.__track_lines__[307] = 'widgets.table.py, line 307:\n    self._checkboxes[ row ][ "checked" ] = False';
		$m.__track_lines__[309] = 'widgets.table.py, line 309:\n    self.selectionChangedEvent.fire( self, self.getCurrentSelection() )';
		$m.__track_lines__[311] = 'widgets.table.py, line 311:\n    def selectRow(self, newRow ):';
		$m.__track_lines__[318] = 'widgets.table.py, line 318:\n    self.setCursorRow( newRow )';
		$m.__track_lines__[320] = 'widgets.table.py, line 320:\n    for row in self._selectedRows[:]:';
		$m.__track_lines__[321] = 'widgets.table.py, line 321:\n    if row!=newRow:';
		$m.__track_lines__[322] = 'widgets.table.py, line 322:\n    self.removeSelectedRow( row )';
		$m.__track_lines__[324] = 'widgets.table.py, line 324:\n    if not newRow in self._selectedRows:';
		$m.__track_lines__[325] = 'widgets.table.py, line 325:\n    self._selectedRows.append( newRow )';
		$m.__track_lines__[326] = 'widgets.table.py, line 326:\n    tr = self.getTrByIndex( newRow )';
		$m.__track_lines__[327] = 'widgets.table.py, line 327:\n    tr["class"].append("is_selected")';
		$m.__track_lines__[329] = 'widgets.table.py, line 329:\n    self.selectionChangedEvent.fire( self, self.getCurrentSelection() )';
		$m.__track_lines__[331] = 'widgets.table.py, line 331:\n    def setCursorRow(self, row, removeExistingSelection=True ):';
		$m.__track_lines__[336] = 'widgets.table.py, line 336:\n    if self._currentRow is not None:';
		$m.__track_lines__[337] = 'widgets.table.py, line 337:\n    self.getTrByIndex(self._currentRow)["class"].remove("is_focused")';
		$m.__track_lines__[339] = 'widgets.table.py, line 339:\n    self._currentRow = row';
		$m.__track_lines__[340] = 'widgets.table.py, line 340:\n    if self._currentRow is not None:';
		$m.__track_lines__[341] = 'widgets.table.py, line 341:\n    self.getTrByIndex(self._currentRow)["class"].append("is_focused")';
		$m.__track_lines__[342] = 'widgets.table.py, line 342:\n    self.cursorMovedEvent.fire( self, row )';
		$m.__track_lines__[344] = 'widgets.table.py, line 344:\n    if removeExistingSelection:';
		$m.__track_lines__[345] = 'widgets.table.py, line 345:\n    for row in self._selectedRows[:]:';
		$m.__track_lines__[346] = 'widgets.table.py, line 346:\n    self.removeSelectedRow( row )';
		$m.__track_lines__[347] = 'widgets.table.py, line 347:\n    self.selectionChangedEvent.fire( self, self.getCurrentSelection() )';
		$m.__track_lines__[349] = 'widgets.table.py, line 349:\n    def getCurrentSelection(self):';
		$m.__track_lines__[354] = 'widgets.table.py, line 354:\n    if self._selectedRows:';
		$m.__track_lines__[355] = 'widgets.table.py, line 355:\n    return self._selectedRows[:]';
		$m.__track_lines__[357] = 'widgets.table.py, line 357:\n    return [self._currentRow]';
		$m.__track_lines__[359] = 'widgets.table.py, line 359:\n    return None';
		$m.__track_lines__[361] = 'widgets.table.py, line 361:\n    def clear(self):';
		$m.__track_lines__[365] = 'widgets.table.py, line 365:\n    super(SelectTable, self).clear()';
		$m.__track_lines__[366] = 'widgets.table.py, line 366:\n    self._currentRow = None';
		$m.__track_lines__[367] = 'widgets.table.py, line 367:\n    self._selectedRows = []';
		$m.__track_lines__[369] = 'widgets.table.py, line 369:\n    self.selectionChangedEvent.fire(self, self.getCurrentSelection())';
		$m.__track_lines__[370] = 'widgets.table.py, line 370:\n    self.tableChangedEvent.fire(self, self.getRowCount())';
		$m.__track_lines__[372] = 'widgets.table.py, line 372:\n    def removeRow(self, row):';
		$m.__track_lines__[376] = 'widgets.table.py, line 376:\n    if row in self._selectedRows:';
		$m.__track_lines__[377] = 'widgets.table.py, line 377:\n    self._selectedRows.remove( row )';
		$m.__track_lines__[378] = 'widgets.table.py, line 378:\n    self.selectionChangedEvent.fire( self )';
		$m.__track_lines__[380] = 'widgets.table.py, line 380:\n    if self._currentRow == row:';
		$m.__track_lines__[381] = 'widgets.table.py, line 381:\n    self._currentRow = None';
		$m.__track_lines__[382] = 'widgets.table.py, line 382:\n    self.cursorMovedEvent.fire( self )';
		$m.__track_lines__[384] = 'widgets.table.py, line 384:\n    self.tableChangedEvent.fire(self, self.getRowCount())';
		$m.__track_lines__[385] = 'widgets.table.py, line 385:\n    super( SelectTable, self ).removeRow( row )';
		$m.__track_lines__[387] = 'widgets.table.py, line 387:\n    def _extraCols(self):';
		$m.__track_lines__[388] = 'widgets.table.py, line 388:\n    return int( self.checkboxes ) + int( self.indexes )';
		$m.__track_lines__[390] = 'widgets.table.py, line 390:\n    def prepareCol(self, row, col):';
		$m.__track_lines__[395] = 'widgets.table.py, line 395:\n    super( SelectTable, self ).prepareCol( row, col + self._extraCols() - 1 )';
		$m.__track_lines__[397] = 'widgets.table.py, line 397:\n    if self.checkboxes:';
		$m.__track_lines__[398] = 'widgets.table.py, line 398:\n    checkbox = html5.Input()';
		$m.__track_lines__[399] = 'widgets.table.py, line 399:\n    checkbox[ "type" ] = "checkbox"';
		$m.__track_lines__[400] = 'widgets.table.py, line 400:\n    checkbox[ "class" ].append( "check" )';
		$m.__track_lines__[401] = 'widgets.table.py, line 401:\n    checkbox[ "checked" ] = False';
		$m.__track_lines__[403] = 'widgets.table.py, line 403:\n    self["cell"][ row ][ self.checkboxes_col ] = checkbox';
		$m.__track_lines__[404] = 'widgets.table.py, line 404:\n    self._checkboxes[ row ] = checkbox';
		$m.__track_lines__[406] = 'widgets.table.py, line 406:\n    if self.indexes:';
		$m.__track_lines__[407] = 'widgets.table.py, line 407:\n    lbl = html5.Label( str( row + 1 ) )';
		$m.__track_lines__[408] = 'widgets.table.py, line 408:\n    lbl[ "class" ].append( "index" )';
		$m.__track_lines__[409] = 'widgets.table.py, line 409:\n    self["cell"][ row ][ self.indexes_col ] = lbl';
		$m.__track_lines__[411] = 'widgets.table.py, line 411:\n    self.tableChangedEvent.fire(self, self.getRowCount())';
		$m.__track_lines__[413] = 'widgets.table.py, line 413:\n    def setCell(self, row, col, val):';
		$m.__track_lines__[418] = 'widgets.table.py, line 418:\n    self[ "cell" ][ row ][ col + self._extraCols() ] = val';
		$m.__track_lines__[420] = 'widgets.table.py, line 420:\n    def selectAll(self):';
		$m.__track_lines__[424] = 'widgets.table.py, line 424:\n    self._selectedRows = range(0, self.getRowCount() )';
		$m.__track_lines__[426] = 'widgets.table.py, line 426:\n    for row in self._selectedRows:';
		$m.__track_lines__[427] = 'widgets.table.py, line 427:\n    tr = self.getTrByIndex( row )';
		$m.__track_lines__[429] = 'widgets.table.py, line 429:\n    if not "is_selected" in tr["class"]:';
		$m.__track_lines__[430] = 'widgets.table.py, line 430:\n    tr["class"].append("is_selected")';
		$m.__track_lines__[432] = 'widgets.table.py, line 432:\n    if self.checkboxes:';
		$m.__track_lines__[433] = 'widgets.table.py, line 433:\n    self._checkboxes[ row ][ "checked" ] = True';
		$m.__track_lines__[435] = 'widgets.table.py, line 435:\n    self.selectionChangedEvent.fire( self, self.getCurrentSelection() )';
		$m.__track_lines__[436] = 'widgets.table.py, line 436:\n    return len(self._selectedRows)';
		$m.__track_lines__[438] = 'widgets.table.py, line 438:\n    def unSelectAll(self):';
		$m.__track_lines__[442] = 'widgets.table.py, line 442:\n    unsel = len(self._selectedRows)';
		$m.__track_lines__[444] = 'widgets.table.py, line 444:\n    for row in self._selectedRows:';
		$m.__track_lines__[445] = 'widgets.table.py, line 445:\n    tr = self.getTrByIndex( row )';
		$m.__track_lines__[446] = 'widgets.table.py, line 446:\n    tr["class"].remove("is_selected")';
		$m.__track_lines__[448] = 'widgets.table.py, line 448:\n    if self.checkboxes:';
		$m.__track_lines__[449] = 'widgets.table.py, line 449:\n    self._checkboxes[ row ][ "checked" ] = False';
		$m.__track_lines__[451] = 'widgets.table.py, line 451:\n    self._selectedRows = []';
		$m.__track_lines__[452] = 'widgets.table.py, line 452:\n    self.selectionChangedEvent.fire( self, self.getCurrentSelection() )';
		$m.__track_lines__[453] = 'widgets.table.py, line 453:\n    return unsel';
		$m.__track_lines__[455] = 'widgets.table.py, line 455:\n    def invertSelection(self):';
		$m.__track_lines__[459] = 'widgets.table.py, line 459:\n    current = self._selectedRows[:]';
		$m.__track_lines__[460] = 'widgets.table.py, line 460:\n    self._selectedRows = []';
		$m.__track_lines__[462] = 'widgets.table.py, line 462:\n    for row in range(0, self.getRowCount() ):';
		$m.__track_lines__[463] = 'widgets.table.py, line 463:\n    tr = self.getTrByIndex( row )';
		$m.__track_lines__[465] = 'widgets.table.py, line 465:\n    if row in current:';
		$m.__track_lines__[466] = 'widgets.table.py, line 466:\n    tr["class"].remove("is_selected")';
		$m.__track_lines__[468] = 'widgets.table.py, line 468:\n    self._selectedRows.append(row)';
		$m.__track_lines__[469] = 'widgets.table.py, line 469:\n    tr["class"].append("is_selected")';
		$m.__track_lines__[471] = 'widgets.table.py, line 471:\n    if self.checkboxes:';
		$m.__track_lines__[472] = 'widgets.table.py, line 472:\n    self._checkboxes[ row ][ "checked" ] = row in self._selectedRows';
		$m.__track_lines__[474] = 'widgets.table.py, line 474:\n    self.selectionChangedEvent.fire( self, self.getCurrentSelection() )';
		$m.__track_lines__[475] = 'widgets.table.py, line 475:\n    return len(self._selectedRows), len(current)';
		$m.__track_lines__[479] = 'widgets.table.py, line 479:\n    class DataTable( html5.Div ):';
		$m.__track_lines__[484] = 'widgets.table.py, line 484:\n    def __init__( self, _loadOnDisplay = False, *args, **kwargs ):';
		$m.__track_lines__[485] = 'widgets.table.py, line 485:\n    super( DataTable, self ).__init__( )';
		$m.__track_lines__[486] = 'widgets.table.py, line 486:\n    self.table = SelectTable( *args, **kwargs )';
		$m.__track_lines__[487] = 'widgets.table.py, line 487:\n    self.appendChild(self.table)';
		$m.__track_lines__[489] = 'widgets.table.py, line 489:\n    self._loadOnDisplay = _loadOnDisplay # Load all data content continuously when displaying';
		$m.__track_lines__[491] = 'widgets.table.py, line 491:\n    self._model = [] # List of values we are displaying right now';
		$m.__track_lines__[492] = 'widgets.table.py, line 492:\n    self._shownFields = [] # List of keys we display from the model';
		$m.__track_lines__[493] = 'widgets.table.py, line 493:\n    self._modelIdx = 0 # Internal counter to distinguish between 2 rows with identical data';
		$m.__track_lines__[494] = 'widgets.table.py, line 494:\n    self._isAjaxLoading = False # Determines if we already requested the next batch of rows';
		$m.__track_lines__[495] = 'widgets.table.py, line 495:\n    self._dataProvider = None # Which object to call if we need more data';
		$m.__track_lines__[496] = 'widgets.table.py, line 496:\n    self._cellRender = {} # Map of renders for a given field';
		$m.__track_lines__[499] = 'widgets.table.py, line 499:\n    self.selectionChangedEvent = EventDispatcher("selectionChanged")';
		$m.__track_lines__[500] = 'widgets.table.py, line 500:\n    self.selectionActivatedEvent = EventDispatcher("selectionActivated")';
		$m.__track_lines__[501] = 'widgets.table.py, line 501:\n    self.tableChangedEvent = EventDispatcher("tableChanged")';
		$m.__track_lines__[503] = 'widgets.table.py, line 503:\n    self.table.selectionChangedEvent.register( self )';
		$m.__track_lines__[504] = 'widgets.table.py, line 504:\n    self.table.selectionActivatedEvent.register( self )';
		$m.__track_lines__[505] = 'widgets.table.py, line 505:\n    self.table.tableChangedEvent.register( self )';
		$m.__track_lines__[508] = 'widgets.table.py, line 508:\n    for f in ["cursorMovedEvent","setHeader"]:';
		$m.__track_lines__[509] = 'widgets.table.py, line 509:\n    setattr( self, f, getattr(self.table,f))';
		$m.__track_lines__[511] = 'widgets.table.py, line 511:\n    self.cursorMovedEvent.register( self )';
		$m.__track_lines__[512] = 'widgets.table.py, line 512:\n    self["style"]["overflow"] = "scroll"';
		$m.__track_lines__[513] = 'widgets.table.py, line 513:\n    self.recalcHeight()';
		$m.__track_lines__[514] = 'widgets.table.py, line 514:\n    self.sinkEvent("onScroll")';
		$m.__track_lines__[517] = 'widgets.table.py, line 517:\n    def recalcHeight(self, *args, **kwargs):';
		$m.__track_lines__[518] = 'widgets.table.py, line 518:\n    self["style"]["max-height"] = "%spx" % (int(eval("window.top.innerHeight"))-280)';
		$m.__track_lines__[520] = 'widgets.table.py, line 520:\n    def setDataProvider(self,obj):';
		$m.__track_lines__[529] = 'widgets.table.py, line 529:\n    assert obj==None or "onNextBatchNeeded" in dir(obj),\\';
		$m.__track_lines__[532] = 'widgets.table.py, line 532:\n    self._dataProvider = obj';
		$m.__track_lines__[533] = 'widgets.table.py, line 533:\n    self._isAjaxLoading = False';
		$m.__track_lines__[535] = 'widgets.table.py, line 535:\n    if "is_loading" in self.table["class"]:';
		$m.__track_lines__[536] = 'widgets.table.py, line 536:\n    self.table["class"].remove("is_loading")';
		$m.__track_lines__[538] = 'widgets.table.py, line 538:\n    def onCursorMoved(self, table, row):';
		$m.__track_lines__[542] = 'widgets.table.py, line 542:\n    tr = table.getTrByIndex( row )';
		$m.__track_lines__[543] = 'widgets.table.py, line 543:\n    if tr is None:';
		$m.__track_lines__[544] = 'widgets.table.py, line 544:\n    return';
		$m.__track_lines__[545] = 'widgets.table.py, line 545:\n    return #FIXME';
		$m.__track_lines__[546] = 'widgets.table.py, line 546:\n    if self.element.scrollTop>tr.offsetTop:';
		$m.__track_lines__[547] = 'widgets.table.py, line 547:\n    self.element.scrollTop = tr.offsetTop';
		$m.__track_lines__[549] = 'widgets.table.py, line 549:\n    self.element.scrollTop = tr.offsetTop+tr.clientHeight-self.element.clientHeight';
		$m.__track_lines__[551] = 'widgets.table.py, line 551:\n    def getRowCount(self):';
		$m.__track_lines__[556] = 'widgets.table.py, line 556:\n    return( len( self._model ))';
		$m.__track_lines__[559] = 'widgets.table.py, line 559:\n    def add(self, obj):';
		$m.__track_lines__[565] = 'widgets.table.py, line 565:\n    obj["_uniqeIndex"] = self._modelIdx';
		$m.__track_lines__[566] = 'widgets.table.py, line 566:\n    self._modelIdx += 1';
		$m.__track_lines__[567] = 'widgets.table.py, line 567:\n    self._model.append( obj )';
		$m.__track_lines__[568] = 'widgets.table.py, line 568:\n    self._renderObject( obj )';
		$m.__track_lines__[569] = 'widgets.table.py, line 569:\n    self._isAjaxLoading = False';
		$m.__track_lines__[570] = 'widgets.table.py, line 570:\n    if "is_loading" in self.table["class"]:';
		$m.__track_lines__[571] = 'widgets.table.py, line 571:\n    self.table["class"].remove("is_loading")';
		$m.__track_lines__[572] = 'widgets.table.py, line 572:\n    self.testIfNextBatchNeededImmediately()';
		$m.__track_lines__[574] = 'widgets.table.py, line 574:\n    def extend(self, objList):';
		$m.__track_lines__[579] = 'widgets.table.py, line 579:\n    self.table.prepareGrid( len(objList), len(self._shownFields) )';
		$m.__track_lines__[580] = 'widgets.table.py, line 580:\n    for obj in objList:';
		$m.__track_lines__[581] = 'widgets.table.py, line 581:\n    obj["_uniqeIndex"] = self._modelIdx';
		$m.__track_lines__[582] = 'widgets.table.py, line 582:\n    self._modelIdx += 1';
		$m.__track_lines__[583] = 'widgets.table.py, line 583:\n    self._model.append( obj )';
		$m.__track_lines__[584] = 'widgets.table.py, line 584:\n    self._renderObject( obj, tableIsPrepared=True )';
		$m.__track_lines__[585] = 'widgets.table.py, line 585:\n    self._isAjaxLoading = False';
		$m.__track_lines__[586] = 'widgets.table.py, line 586:\n    if "is_loading" in self.table["class"]:';
		$m.__track_lines__[587] = 'widgets.table.py, line 587:\n    self.table["class"].remove("is_loading")';
		$m.__track_lines__[588] = 'widgets.table.py, line 588:\n    self.testIfNextBatchNeededImmediately()';
		$m.__track_lines__[590] = 'widgets.table.py, line 590:\n    def testIfNextBatchNeededImmediately(self):';
		$m.__track_lines__[596] = 'widgets.table.py, line 596:\n    sumHeight = 0';
		$m.__track_lines__[597] = 'widgets.table.py, line 597:\n    for c in self.table._children:';
		$m.__track_lines__[598] = 'widgets.table.py, line 598:\n    if "clientHeight" in dir(c.element):';
		$m.__track_lines__[599] = 'widgets.table.py, line 599:\n    sumHeight += c.element.clientHeight';
		$m.__track_lines__[601] = 'widgets.table.py, line 601:\n    if (not self._isAjaxLoading';
		$m.__track_lines__[605] = 'widgets.table.py, line 605:\n    if self._dataProvider:';
		$m.__track_lines__[606] = 'widgets.table.py, line 606:\n    self._isAjaxLoading = True';
		$m.__track_lines__[607] = 'widgets.table.py, line 607:\n    if not "is_loading" in self.table["class"]:';
		$m.__track_lines__[608] = 'widgets.table.py, line 608:\n    self.table["class"].append("is_loading")';
		$m.__track_lines__[609] = 'widgets.table.py, line 609:\n    self._dataProvider.onNextBatchNeeded()';
		$m.__track_lines__[611] = 'widgets.table.py, line 611:\n    def remove(self, objOrIndex):';
		$m.__track_lines__[617] = 'widgets.table.py, line 617:\n    if isinstance( objOrIndex, dict ):';
		$m.__track_lines__[618] = 'widgets.table.py, line 618:\n    assert objOrIndex in self._model, "Cannot remove unknown object from Table"';
		$m.__track_lines__[619] = 'widgets.table.py, line 619:\n    objOrIndex = self._model.index( objOrIndex )';
		$m.__track_lines__[620] = 'widgets.table.py, line 620:\n    if isinstance( objOrIndex, int ):';
		$m.__track_lines__[621] = 'widgets.table.py, line 621:\n    assert objOrIndex>0 and objOrIndex<len(self._model), "Modelindex out of range"';
		$m.__track_lines__[622] = 'widgets.table.py, line 622:\n    self._model.remove( self._model[objOrIndex] )';
		$m.__track_lines__[623] = 'widgets.table.py, line 623:\n    self.table.removeRow( objOrIndex )';
		$m.__track_lines__[625] = 'widgets.table.py, line 625:\n    raise TypeError("Expected int or dict, got %s" % str(type(objOrIndex)))';
		$m.__track_lines__[627] = 'widgets.table.py, line 627:\n    def clear(self, keepModel=False):';
		$m.__track_lines__[631] = 'widgets.table.py, line 631:\n    self.table.clear()';
		$m.__track_lines__[632] = 'widgets.table.py, line 632:\n    if not keepModel:';
		$m.__track_lines__[633] = 'widgets.table.py, line 633:\n    self._model = []';
		$m.__track_lines__[635] = 'widgets.table.py, line 635:\n    def _renderObject(self, obj, tableIsPrepared=False):';
		$m.__track_lines__[642] = 'widgets.table.py, line 642:\n    if not self._shownFields:';
		$m.__track_lines__[643] = 'widgets.table.py, line 643:\n    return';
		$m.__track_lines__[645] = 'widgets.table.py, line 645:\n    rowIdx = self._model.index(obj)';
		$m.__track_lines__[646] = 'widgets.table.py, line 646:\n    cellIdx = 0';
		$m.__track_lines__[648] = 'widgets.table.py, line 648:\n    if not tableIsPrepared:';
		$m.__track_lines__[649] = 'widgets.table.py, line 649:\n    self.table.prepareCol( rowIdx, len( self._shownFields ) - 1 )';
		$m.__track_lines__[651] = 'widgets.table.py, line 651:\n    for field in self._shownFields:';
		$m.__track_lines__[652] = 'widgets.table.py, line 652:\n    if field in self._cellRender.keys():';
		$m.__track_lines__[653] = 'widgets.table.py, line 653:\n    lbl = self._cellRender[ field ].render( obj, field )';
		$m.__track_lines__[655] = 'widgets.table.py, line 655:\n    lbl = html5.Label(obj[field])';
		$m.__track_lines__[657] = 'widgets.table.py, line 657:\n    lbl = html5.Label("...")';
		$m.__track_lines__[659] = 'widgets.table.py, line 659:\n    self.table.setCell( rowIdx, cellIdx, lbl )';
		$m.__track_lines__[660] = 'widgets.table.py, line 660:\n    cellIdx += 1';
		$m.__track_lines__[662] = 'widgets.table.py, line 662:\n    def rebuildTable(self):';
		$m.__track_lines__[667] = 'widgets.table.py, line 667:\n    self.clear( keepModel=True )';
		$m.__track_lines__[668] = 'widgets.table.py, line 668:\n    self.table.prepareGrid( len(self._model), len(self._shownFields))';
		$m.__track_lines__[669] = 'widgets.table.py, line 669:\n    for obj in self._model:';
		$m.__track_lines__[670] = 'widgets.table.py, line 670:\n    self._renderObject( obj, tableIsPrepared=True )';
		$m.__track_lines__[672] = 'widgets.table.py, line 672:\n    def setShownFields(self,fields):';
		$m.__track_lines__[680] = 'widgets.table.py, line 680:\n    self._shownFields = fields';
		$m.__track_lines__[681] = 'widgets.table.py, line 681:\n    self.rebuildTable()';
		$m.__track_lines__[683] = 'widgets.table.py, line 683:\n    def onScroll(self, event):';
		$m.__track_lines__[687] = 'widgets.table.py, line 687:\n    if self._loadOnDisplay:';
		$m.__track_lines__[688] = 'widgets.table.py, line 688:\n    return';
		$m.__track_lines__[690] = 'widgets.table.py, line 690:\n    self.recalcHeight()';
		$m.__track_lines__[692] = 'widgets.table.py, line 692:\n    if ( ( self.element.scrollTop + self.element.clientHeight )';
		$m.__track_lines__[696] = 'widgets.table.py, line 696:\n    if self._dataProvider:';
		$m.__track_lines__[698] = 'widgets.table.py, line 698:\n    self._isAjaxLoading = True';
		$m.__track_lines__[699] = 'widgets.table.py, line 699:\n    if not "is_loading" in self.table["class"]:';
		$m.__track_lines__[700] = 'widgets.table.py, line 700:\n    self.table["class"].append("is_loading")';
		$m.__track_lines__[702] = 'widgets.table.py, line 702:\n    self._dataProvider.onNextBatchNeeded()';
		$m.__track_lines__[704] = 'widgets.table.py, line 704:\n    def onSelectionChanged( self, table, rows ):';
		$m.__track_lines__[708] = 'widgets.table.py, line 708:\n    vals = [ self._model[x] for x in (rows or []) ]';
		$m.__track_lines__[709] = 'widgets.table.py, line 709:\n    self.selectionChangedEvent.fire( self, vals )';
		$m.__track_lines__[711] = 'widgets.table.py, line 711:\n    def onSelectionActivated( self, table, rows ):';
		$m.__track_lines__[715] = 'widgets.table.py, line 715:\n    vals = [ self._model[x] for x in rows]';
		$m.__track_lines__[716] = 'widgets.table.py, line 716:\n    self.selectionActivatedEvent.fire( self, vals )';
		$m.__track_lines__[718] = 'widgets.table.py, line 718:\n    def onTableChanged( self, table, rowCount ):';
		$m.__track_lines__[722] = 'widgets.table.py, line 722:\n    self.tableChangedEvent.fire(self, rowCount)';
		$m.__track_lines__[724] = 'widgets.table.py, line 724:\n    def getCurrentSelection(self):';
		$m.__track_lines__[729] = 'widgets.table.py, line 729:\n    rows = self.table.getCurrentSelection()';
		$m.__track_lines__[730] = 'widgets.table.py, line 730:\n    if not self._model or not rows:';
		$m.__track_lines__[731] = 'widgets.table.py, line 731:\n    return( [] )';
		$m.__track_lines__[732] = 'widgets.table.py, line 732:\n    return( [ self._model[x] for x in rows ] )';
		$m.__track_lines__[734] = 'widgets.table.py, line 734:\n    def setCellRender(self, field, render):';
		$m.__track_lines__[740] = 'widgets.table.py, line 740:\n    if render is None:';
		$m.__track_lines__[741] = 'widgets.table.py, line 741:\n    if field in self._cellRender.keys():';
		$m.__track_lines__[742] = 'widgets.table.py, line 742:\n    del self._cellRender[ field ]';
		$m.__track_lines__[744] = 'widgets.table.py, line 744:\n    assert "render" in dir(render), "The render must provide a \'render\' method"';
		$m.__track_lines__[745] = 'widgets.table.py, line 745:\n    self._cellRender[ field ] = render';
		$m.__track_lines__[746] = 'widgets.table.py, line 746:\n    self.rebuildTable()';
		$m.__track_lines__[748] = 'widgets.table.py, line 748:\n    def setCellRenders(self, renders ):';
		$m.__track_lines__[753] = 'widgets.table.py, line 753:\n    assert isinstance( renders, dict )';
		$m.__track_lines__[754] = 'widgets.table.py, line 754:\n    for field, render in renders.items():';
		$m.__track_lines__[755] = 'widgets.table.py, line 755:\n    if render is None:';
		$m.__track_lines__[756] = 'widgets.table.py, line 756:\n    if field in self._cellRender.keys():';
		$m.__track_lines__[757] = 'widgets.table.py, line 757:\n    del self._cellRender[ field ]';
		$m.__track_lines__[759] = 'widgets.table.py, line 759:\n    assert "render" in dir(render), "The render must provide a \'render\' method"';
		$m.__track_lines__[760] = 'widgets.table.py, line 760:\n    self._cellRender[ field ] = render';
		$m.__track_lines__[761] = 'widgets.table.py, line 761:\n    self.rebuildTable()';
		$m.__track_lines__[763] = 'widgets.table.py, line 763:\n    def activateCurrentSelection(self):';
		$m.__track_lines__[768] = 'widgets.table.py, line 768:\n    selection = self.getCurrentSelection()';
		$m.__track_lines__[769] = 'widgets.table.py, line 769:\n    if len( selection )>0:';
		$m.__track_lines__[770] = 'widgets.table.py, line 770:\n    self.selectionActivatedEvent.fire( self, selection )';

		var $constant_int_0 = new $p['int'](0);
		var $constant_int_1 = new $p['int'](1);
		var $constant_int_2 = new $p['int'](2);
		var $constant_int_280 = new $p['int'](280);
		$pyjs['track']['module']='widgets.table';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=1;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['html5'] = $p['___import___']('html5', 'widgets');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['utils'] = $p['___import___']('utils', 'widgets');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=2;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['EventDispatcher'] = $p['___import___']('event.EventDispatcher', 'widgets', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=3;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$p['__import_all__']('html5.keycodes', 'widgets', $m, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=5;
		$m['SelectTable'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'widgets.table';
			$cls_definition['__md5__'] = '31ccbed966e1600b9f2f1f77bbf82ee2';
			$pyjs['track']['lineno']=21;
			$method = $pyjs__bind_method2('__init__', function(checkboxes, indexes) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,2,arguments['length']-1));

					var kwargs = arguments['length'] >= 3 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					checkboxes = arguments[1];
					indexes = arguments[2];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

					var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof indexes != 'undefined') {
						if (indexes !== null && typeof indexes['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = indexes;
							indexes = arguments[3];
						}
					} else 					if (typeof checkboxes != 'undefined') {
						if (checkboxes !== null && typeof checkboxes['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = checkboxes;
							checkboxes = arguments[3];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[3];
						}
					} else {
					}
				}
				if (typeof checkboxes == 'undefined') checkboxes=arguments['callee']['__args__'][3][1];
				if (typeof indexes == 'undefined') indexes=arguments['callee']['__args__'][4][1];
				var $add2,$add1;
				$pyjs['track']={'module':'widgets.table', 'lineno':21};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=21;
				$pyjs['track']['lineno']=22;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectTable'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})();
				$pyjs['track']['lineno']=25;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectionChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('selectionChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})()) : $p['setattr'](self, 'selectionChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('selectionChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})()); 
				$pyjs['track']['lineno']=26;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectionActivatedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('selectionActivated');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})()) : $p['setattr'](self, 'selectionActivatedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('selectionActivated');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})()); 
				$pyjs['track']['lineno']=27;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('cursorMovedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('cursorMoved');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})()) : $p['setattr'](self, 'cursorMovedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('cursorMoved');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})()); 
				$pyjs['track']['lineno']=28;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('tableChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('tableChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})()) : $p['setattr'](self, 'tableChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('tableChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})()); 
				$pyjs['track']['lineno']=30;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['sinkEvent']('onDblClick', 'onMouseMove', 'onMouseDown', 'onMouseUp', 'onKeyDown', 'onKeyUp', 'onMouseOut', 'onChange');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})();
				$pyjs['track']['lineno']=34;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__setitem__']('tabindex', $constant_int_1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})();
				$pyjs['track']['lineno']=36;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_selectedRows', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})()) : $p['setattr'](self, '_selectedRows', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})()); 
				$pyjs['track']['lineno']=37;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_currentRow', null) : $p['setattr'](self, '_currentRow', null); 
				$pyjs['track']['lineno']=38;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isMouseDown', false) : $p['setattr'](self, '_isMouseDown', false); 
				$pyjs['track']['lineno']=39;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isCtlPressed', false) : $p['setattr'](self, '_isCtlPressed', false); 
				$pyjs['track']['lineno']=40;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_ctlStartRow', null) : $p['setattr'](self, '_ctlStartRow', null); 
				$pyjs['track']['lineno']=41;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_selectionChangedListener', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})()) : $p['setattr'](self, '_selectionChangedListener', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})()); 
				$pyjs['track']['lineno']=42;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_selectionActivatedListeners', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})()) : $p['setattr'](self, '_selectionActivatedListeners', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})()); 
				$pyjs['track']['lineno']=43;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_cursorMovedListeners', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})()) : $p['setattr'](self, '_cursorMovedListeners', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})()); 
				$pyjs['track']['lineno']=45;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('indexes', indexes) : $p['setattr'](self, 'indexes', indexes); 
				$pyjs['track']['lineno']=46;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('indexes_col', ($p['bool'](indexes)? ($constant_int_0) : ((typeof ($usub1=$constant_int_1)=='number'?
					-$usub1:
					$p['op_usub']($usub1))))) : $p['setattr'](self, 'indexes_col', ($p['bool'](indexes)? ($constant_int_0) : ((typeof ($usub1=$constant_int_1)=='number'?
					-$usub1:
					$p['op_usub']($usub1))))); 
				$pyjs['track']['lineno']=48;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_checkboxes', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})()) : $p['setattr'](self, '_checkboxes', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})()); 
				$pyjs['track']['lineno']=49;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('checkboxes', checkboxes) : $p['setattr'](self, 'checkboxes', checkboxes); 
				$pyjs['track']['lineno']=50;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('checkboxes_col', ($p['bool'](checkboxes)? ($p['__op_add']($add1=$p['getattr'](self, 'indexes_col'),$add2=$constant_int_1)) : ((typeof ($usub2=$constant_int_1)=='number'?
					-$usub2:
					$p['op_usub']($usub2))))) : $p['setattr'](self, 'checkboxes_col', ($p['bool'](checkboxes)? ($p['__op_add']($add1=$p['getattr'](self, 'indexes_col'),$add2=$constant_int_1)) : ((typeof ($usub2=$constant_int_1)=='number'?
					-$usub2:
					$p['op_usub']($usub2))))); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['checkboxes', false],['indexes', false]]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=52;
			$method = $pyjs__bind_method2('onAttach', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.table', 'lineno':52};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=52;
				$pyjs['track']['lineno']=53;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectTable'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_14_err){if (!$p['isinstance']($pyjs_dbg_14_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_14_err);}throw $pyjs_dbg_14_err;
}})()['onAttach']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})();
				$pyjs['track']['lineno']=54;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['focus']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_16_err){if (!$p['isinstance']($pyjs_dbg_16_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_16_err);}throw $pyjs_dbg_16_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['onAttach'] = $method;
			$pyjs['track']['lineno']=56;
			$method = $pyjs__bind_method2('setHeader', function(headers) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					headers = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var head,$iter1_nextval,$iter1_type,tr,$iter1_iter,$iter1_array,th,$pyjs__trackstack_size_1,$iter1_idx;
				$pyjs['track']={'module':'widgets.table', 'lineno':56};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=56;
				$pyjs['track']['lineno']=63;
				tr = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Tr']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_17_err){if (!$p['isinstance']($pyjs_dbg_17_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_17_err);}throw $pyjs_dbg_17_err;
}})();
				$pyjs['track']['lineno']=66;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'indexes'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})()) {
					$pyjs['track']['lineno']=67;
					th = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Th']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_19_err){if (!$p['isinstance']($pyjs_dbg_19_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_19_err);}throw $pyjs_dbg_19_err;
}})();
					$pyjs['track']['lineno']=68;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return th['__setitem__']('class', 'index');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})();
					$pyjs['track']['lineno']=69;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return tr['appendChild'](th);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})();
				}
				$pyjs['track']['lineno']=72;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'checkboxes'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})()) {
					$pyjs['track']['lineno']=73;
					th = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Th']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})();
					$pyjs['track']['lineno']=74;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return th['__setitem__']('class', 'check');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})();
					$pyjs['track']['lineno']=75;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return tr['appendChild'](th);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})();
				}
				$pyjs['track']['lineno']=78;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter1_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return headers;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_26_err){if (!$p['isinstance']($pyjs_dbg_26_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_26_err);}throw $pyjs_dbg_26_err;
}})();
				$iter1_nextval=$p['__iter_prepare']($iter1_iter,false);
				while (typeof($p['__wrapped_next']($iter1_nextval)['$nextval']) != 'undefined') {
					head = $iter1_nextval['$nextval'];
					$pyjs['track']['lineno']=79;
					th = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Th']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_27_err){if (!$p['isinstance']($pyjs_dbg_27_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_27_err);}throw $pyjs_dbg_27_err;
}})();
					$pyjs['track']['lineno']=80;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return th['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['TextNode'](head);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_28_err){if (!$p['isinstance']($pyjs_dbg_28_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_28_err);}throw $pyjs_dbg_28_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_29_err){if (!$p['isinstance']($pyjs_dbg_29_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_29_err);}throw $pyjs_dbg_29_err;
}})();
					$pyjs['track']['lineno']=81;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return tr['appendChild'](th);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_30_err){if (!$p['isinstance']($pyjs_dbg_30_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_30_err);}throw $pyjs_dbg_30_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=83;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['head']['removeAllChildren']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_31_err){if (!$p['isinstance']($pyjs_dbg_31_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_31_err);}throw $pyjs_dbg_31_err;
}})();
				$pyjs['track']['lineno']=84;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['head']['appendChild'](tr);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_32_err){if (!$p['isinstance']($pyjs_dbg_32_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_32_err);}throw $pyjs_dbg_32_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['headers']]);
			$cls_definition['setHeader'] = $method;
			$pyjs['track']['lineno']=86;
			$method = $pyjs__bind_method2('getTrByIndex', function(idx) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					idx = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var c,$iter2_nextval,$iter2_type,$iter2_iter,$iter2_idx,$pyjs__trackstack_size_1,$sub2,$sub1,$iter2_array;
				$pyjs['track']={'module':'widgets.table', 'lineno':86};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=86;
				$pyjs['track']['lineno']=93;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter2_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr']($p['getattr'](self, 'body'), '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_33_err){if (!$p['isinstance']($pyjs_dbg_33_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_33_err);}throw $pyjs_dbg_33_err;
}})();
				$iter2_nextval=$p['__iter_prepare']($iter2_iter,false);
				while (typeof($p['__wrapped_next']($iter2_nextval)['$nextval']) != 'undefined') {
					c = $iter2_nextval['$nextval'];
					$pyjs['track']['lineno']=94;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['cmp'](idx, $constant_int_0) < 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_34_err){if (!$p['isinstance']($pyjs_dbg_34_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_34_err);}throw $pyjs_dbg_34_err;
}})()) {
						$pyjs['track']['lineno']=95;
						$pyjs['track']['lineno']=95;
						var $pyjs__ret = c;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
					$pyjs['track']['lineno']=96;
					idx = $p['__op_sub']($sub1=idx,$sub2=c['__getitem__']('rowspan'));
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=98;
				$pyjs['track']['lineno']=98;
				var $pyjs__ret = null;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['idx']]);
			$cls_definition['getTrByIndex'] = $method;
			$pyjs['track']['lineno']=100;
			$method = $pyjs__bind_method2('getIndexByTr', function(tr) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					tr = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var c,$iter3_idx,idx,$iter3_array,$add3,$iter3_iter,$add4,$iter3_type,$pyjs__trackstack_size_1,$iter3_nextval;
				$pyjs['track']={'module':'widgets.table', 'lineno':100};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=100;
				$pyjs['track']['lineno']=108;
				idx = $constant_int_0;
				$pyjs['track']['lineno']=109;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter3_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr']($p['getattr'](self, 'body'), '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})();
				$iter3_nextval=$p['__iter_prepare']($iter3_iter,false);
				while (typeof($p['__wrapped_next']($iter3_nextval)['$nextval']) != 'undefined') {
					c = $iter3_nextval['$nextval'];
					$pyjs['track']['lineno']=110;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_eq']($p['getattr'](c, 'element'), tr));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_36_err){if (!$p['isinstance']($pyjs_dbg_36_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_36_err);}throw $pyjs_dbg_36_err;
}})()) {
						$pyjs['track']['lineno']=111;
						$pyjs['track']['lineno']=111;
						var $pyjs__ret = idx;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
					$pyjs['track']['lineno']=112;
					idx = $p['__op_add']($add3=idx,$add4=c['__getitem__']('rowspan'));
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=113;
				$pyjs['track']['lineno']=113;
				var $pyjs__ret = idx;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['tr']]);
			$cls_definition['getIndexByTr'] = $method;
			$pyjs['track']['lineno']=115;
			$method = $pyjs__bind_method2('_rowForEvent', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $pyjs_try_err,tr,tc;
				$pyjs['track']={'module':'widgets.table', 'lineno':115};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=115;
				$pyjs['track']['lineno']=119;
				var $pyjs__trackstack_size_1 = $pyjs['trackstack']['length'];
				try {
					try {
						$pyjs['in_try_except'] += 1;
						$pyjs['track']['lineno']=121;
						tc = $p['getattr'](event, 'srcElement');
					} finally { $pyjs['in_try_except'] -= 1; }
				} catch($pyjs_try_err) {
					$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_1 - 1);
					$pyjs['__active_exception_stack__'] = null;
					$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
					var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
					$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='widgets.table';
					if (true) {
						$pyjs['track']['lineno']=124;
						tc = $p['getattr'](event, 'target');
					}
				}
				$pyjs['track']['lineno']=126;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](tc, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_37_err){if (!$p['isinstance']($pyjs_dbg_37_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_37_err);}throw $pyjs_dbg_37_err;
}})()) {
					$pyjs['track']['lineno']=127;
					$pyjs['track']['lineno']=127;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=129;
				tr = $p['getattr'](tc, 'parentElement');
				$pyjs['track']['lineno']=131;
				while ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['op_is']($p['getattr'](tr, 'parentElement'), null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_38_err){if (!$p['isinstance']($pyjs_dbg_38_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_38_err);}throw $pyjs_dbg_38_err;
}})()) {
					$pyjs['track']['lineno']=132;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_eq']($p['getattr'](tr, 'parentElement'), $p['getattr']($p['getattr'](self, 'body'), 'element')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_39_err){if (!$p['isinstance']($pyjs_dbg_39_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_39_err);}throw $pyjs_dbg_39_err;
}})()) {
						$pyjs['track']['lineno']=133;
						$pyjs['track']['lineno']=133;
						var $pyjs__ret = tr;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
					$pyjs['track']['lineno']=134;
					tr = $p['getattr'](tr, 'parentElement');
				}
				$pyjs['track']['lineno']=136;
				$pyjs['track']['lineno']=136;
				var $pyjs__ret = null;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['_rowForEvent'] = $method;
			$pyjs['track']['lineno']=138;
			$method = $pyjs__bind_method2('onChange', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and1,tr,$and2,row;
				$pyjs['track']={'module':'widgets.table', 'lineno':138};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=138;
				$pyjs['track']['lineno']=139;
				tr = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_rowForEvent'](event);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_40_err){if (!$p['isinstance']($pyjs_dbg_40_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_40_err);}throw $pyjs_dbg_40_err;
}})();
				$pyjs['track']['lineno']=140;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](tr, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_41_err){if (!$p['isinstance']($pyjs_dbg_41_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_41_err);}throw $pyjs_dbg_41_err;
}})()) {
					$pyjs['track']['lineno']=141;
					$pyjs['track']['lineno']=141;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=143;
				row = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getIndexByTr'](tr);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_42_err){if (!$p['isinstance']($pyjs_dbg_42_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_42_err);}throw $pyjs_dbg_42_err;
}})();
				$pyjs['track']['lineno']=145;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and1=$p['getattr'](self, 'checkboxes'))?(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['utils']['doesEventHitWidgetOrChildren'](event, $p['getattr'](self, '_checkboxes')['__getitem__'](row));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_43_err){if (!$p['isinstance']($pyjs_dbg_43_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_43_err);}throw $pyjs_dbg_43_err;
}})():$and1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_44_err){if (!$p['isinstance']($pyjs_dbg_44_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_44_err);}throw $pyjs_dbg_44_err;
}})()) {
					$pyjs['track']['lineno']=148;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, '_checkboxes')['__getitem__'](row)['__setitem__']('checked', $p['getattr'](self, '_selectedRows')['__contains__'](row));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_45_err){if (!$p['isinstance']($pyjs_dbg_45_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_45_err);}throw $pyjs_dbg_45_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onChange'] = $method;
			$pyjs['track']['lineno']=150;
			$method = $pyjs__bind_method2('onMouseDown', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var tr,$and3,$and4,row;
				$pyjs['track']={'module':'widgets.table', 'lineno':150};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=150;
				$pyjs['track']['lineno']=151;
				tr = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_rowForEvent'](event);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_46_err){if (!$p['isinstance']($pyjs_dbg_46_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_46_err);}throw $pyjs_dbg_46_err;
}})();
				$pyjs['track']['lineno']=152;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](tr, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})()) {
					$pyjs['track']['lineno']=153;
					$pyjs['track']['lineno']=153;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=155;
				row = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getIndexByTr'](tr);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_48_err){if (!$p['isinstance']($pyjs_dbg_48_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_48_err);}throw $pyjs_dbg_48_err;
}})();
				$pyjs['track']['lineno']=157;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, '_isCtlPressed'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_49_err){if (!$p['isinstance']($pyjs_dbg_49_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_49_err);}throw $pyjs_dbg_49_err;
}})()) {
					$pyjs['track']['lineno']=158;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, '_selectedRows')['__contains__'](row));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_50_err){if (!$p['isinstance']($pyjs_dbg_50_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_50_err);}throw $pyjs_dbg_50_err;
}})()) {
						$pyjs['track']['lineno']=159;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['removeSelectedRow'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_51_err){if (!$p['isinstance']($pyjs_dbg_51_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_51_err);}throw $pyjs_dbg_51_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=161;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['addSelectedRow'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_52_err){if (!$p['isinstance']($pyjs_dbg_52_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_52_err);}throw $pyjs_dbg_52_err;
}})();
					}
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and3=$p['getattr'](self, 'checkboxes'))?(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['utils']['doesEventHitWidgetOrChildren'](event, $p['getattr'](self, '_checkboxes')['__getitem__'](row));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_53_err){if (!$p['isinstance']($pyjs_dbg_53_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_53_err);}throw $pyjs_dbg_53_err;
}})():$and3));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_54_err){if (!$p['isinstance']($pyjs_dbg_54_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_54_err);}throw $pyjs_dbg_54_err;
}})()) {
					$pyjs['track']['lineno']=167;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, '_selectedRows')['__contains__'](row));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_55_err){if (!$p['isinstance']($pyjs_dbg_55_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_55_err);}throw $pyjs_dbg_55_err;
}})()) {
						$pyjs['track']['lineno']=168;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['removeSelectedRow'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_56_err){if (!$p['isinstance']($pyjs_dbg_56_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_56_err);}throw $pyjs_dbg_56_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=170;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['addSelectedRow'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_57_err){if (!$p['isinstance']($pyjs_dbg_57_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_57_err);}throw $pyjs_dbg_57_err;
}})();
					}
				}
				else {
					$pyjs['track']['lineno']=173;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isMouseDown', true) : $p['setattr'](self, '_isMouseDown', true); 
					$pyjs['track']['lineno']=175;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'checkboxes'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_58_err){if (!$p['isinstance']($pyjs_dbg_58_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_58_err);}throw $pyjs_dbg_58_err;
}})()) {
						$pyjs['track']['lineno']=176;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']($p['getattr'](self, '_selectedRows')['__contains__'](row));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_59_err){if (!$p['isinstance']($pyjs_dbg_59_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_59_err);}throw $pyjs_dbg_59_err;
}})()) {
							$pyjs['track']['lineno']=177;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return self['removeSelectedRow'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_60_err){if (!$p['isinstance']($pyjs_dbg_60_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_60_err);}throw $pyjs_dbg_60_err;
}})();
						}
						else {
							$pyjs['track']['lineno']=179;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return self['addSelectedRow'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_61_err){if (!$p['isinstance']($pyjs_dbg_61_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_61_err);}throw $pyjs_dbg_61_err;
}})();
						}
					}
					$pyjs['track']['lineno']=181;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call(self, 'setCursorRow', null, null, [{'removeExistingSelection':!$p['bool']($p['getattr'](self, 'checkboxes'))}, (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['getIndexByTr'](tr);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_62_err){if (!$p['isinstance']($pyjs_dbg_62_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_62_err);}throw $pyjs_dbg_62_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_63_err){if (!$p['isinstance']($pyjs_dbg_63_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_63_err);}throw $pyjs_dbg_63_err;
}})();
				}
				$pyjs['track']['lineno']=183;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return event['preventDefault']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})();
				$pyjs['track']['lineno']=184;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['focus']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_65_err){if (!$p['isinstance']($pyjs_dbg_65_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_65_err);}throw $pyjs_dbg_65_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onMouseDown'] = $method;
			$pyjs['track']['lineno']=186;
			$method = $pyjs__bind_method2('onMouseOut', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.table', 'lineno':186};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=186;
				$pyjs['track']['lineno']=187;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isMouseDown', false) : $p['setattr'](self, '_isMouseDown', false); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onMouseOut'] = $method;
			$pyjs['track']['lineno']=189;
			$method = $pyjs__bind_method2('onMouseUp', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.table', 'lineno':189};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=189;
				$pyjs['track']['lineno']=190;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isMouseDown', false) : $p['setattr'](self, '_isMouseDown', false); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onMouseUp'] = $method;
			$pyjs['track']['lineno']=192;
			$method = $pyjs__bind_method2('onMouseMove', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var tr;
				$pyjs['track']={'module':'widgets.table', 'lineno':192};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=192;
				$pyjs['track']['lineno']=193;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, '_isMouseDown'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_66_err){if (!$p['isinstance']($pyjs_dbg_66_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_66_err);}throw $pyjs_dbg_66_err;
}})()) {
					$pyjs['track']['lineno']=194;
					tr = (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['_rowForEvent'](event);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})();
					$pyjs['track']['lineno']=195;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_is'](tr, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_68_err){if (!$p['isinstance']($pyjs_dbg_68_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_68_err);}throw $pyjs_dbg_68_err;
}})()) {
						$pyjs['track']['lineno']=196;
						$pyjs['track']['lineno']=196;
						var $pyjs__ret = null;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
					$pyjs['track']['lineno']=197;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['addSelectedRow']((function(){try{try{$pyjs['in_try_except'] += 1;
					return self['getIndexByTr'](tr);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_69_err){if (!$p['isinstance']($pyjs_dbg_69_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_69_err);}throw $pyjs_dbg_69_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_70_err){if (!$p['isinstance']($pyjs_dbg_70_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_70_err);}throw $pyjs_dbg_70_err;
}})();
					$pyjs['track']['lineno']=198;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return event['preventDefault']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_71_err){if (!$p['isinstance']($pyjs_dbg_71_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_71_err);}throw $pyjs_dbg_71_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onMouseMove'] = $method;
			$pyjs['track']['lineno']=200;
			$method = $pyjs__bind_method2('onKeyDown', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $or1,$sub4,$or2,$and5,$and6,$sub6,$add6,$add7,$add12,$add5,$sub3,$add10,$add8,$add9,$sub5,$add11;
				$pyjs['track']={'module':'widgets.table', 'lineno':200};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=200;
				$pyjs['track']['lineno']=201;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof isArrowDown == "undefined"?$m['isArrowDown']:isArrowDown)($p['getattr'](event, 'keyCode'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_72_err){if (!$p['isinstance']($pyjs_dbg_72_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_72_err);}throw $pyjs_dbg_72_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_73_err){if (!$p['isinstance']($pyjs_dbg_73_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_73_err);}throw $pyjs_dbg_73_err;
}})()) {
					$pyjs['track']['lineno']=203;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_is']($p['getattr'](self, '_currentRow'), null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_74_err){if (!$p['isinstance']($pyjs_dbg_74_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_74_err);}throw $pyjs_dbg_74_err;
}})()) {
						$pyjs['track']['lineno']=204;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['setCursorRow']($constant_int_0);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=207;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']($p['getattr'](self, '_isCtlPressed'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})()) {
							$pyjs['track']['lineno']=209;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool'](($p['cmp']($p['getattr'](self, '_ctlStartRow'), $p['getattr'](self, '_currentRow')) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_77_err){if (!$p['isinstance']($pyjs_dbg_77_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_77_err);}throw $pyjs_dbg_77_err;
}})()) {
								$pyjs['track']['lineno']=210;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return self['removeSelectedRow']($p['getattr'](self, '_currentRow'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_78_err){if (!$p['isinstance']($pyjs_dbg_78_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_78_err);}throw $pyjs_dbg_78_err;
}})();
							}
							else {
								$pyjs['track']['lineno']=212;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return self['addSelectedRow']($p['getattr'](self, '_currentRow'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_79_err){if (!$p['isinstance']($pyjs_dbg_79_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_79_err);}throw $pyjs_dbg_79_err;
}})();
								$pyjs['track']['lineno']=214;
								if ((function(){try{try{$pyjs['in_try_except'] += 1;
									return $p['bool'](($p['cmp']($p['__op_add']($add5=$p['getattr'](self, '_currentRow'),$add6=$constant_int_1), (function(){try{try{$pyjs['in_try_except'] += 1;
								return self['getRowCount']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_80_err){if (!$p['isinstance']($pyjs_dbg_80_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_80_err);}throw $pyjs_dbg_80_err;
}})()) == -1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_81_err){if (!$p['isinstance']($pyjs_dbg_81_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_81_err);}throw $pyjs_dbg_81_err;
}})()) {
									$pyjs['track']['lineno']=215;
									(function(){try{try{$pyjs['in_try_except'] += 1;
									return self['addSelectedRow']($p['__op_add']($add7=$p['getattr'](self, '_currentRow'),$add8=$constant_int_1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_82_err){if (!$p['isinstance']($pyjs_dbg_82_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_82_err);}throw $pyjs_dbg_82_err;
}})();
								}
							}
						}
						$pyjs['track']['lineno']=217;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](($p['cmp']($p['__op_add']($add9=$p['getattr'](self, '_currentRow'),$add10=$constant_int_1), (function(){try{try{$pyjs['in_try_except'] += 1;
						return self['getRowCount']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_83_err){if (!$p['isinstance']($pyjs_dbg_83_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_83_err);}throw $pyjs_dbg_83_err;
}})()) == -1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_84_err){if (!$p['isinstance']($pyjs_dbg_84_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_84_err);}throw $pyjs_dbg_84_err;
}})()) {
							$pyjs['track']['lineno']=218;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $pyjs_kwargs_call(self, 'setCursorRow', null, null, [{'removeExistingSelection':!$p['bool']($p['getattr'](self, '_isCtlPressed'))}, $p['__op_add']($add11=$p['getattr'](self, '_currentRow'),$add12=$constant_int_1)]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})();
						}
					}
					$pyjs['track']['lineno']=220;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return event['preventDefault']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof isArrowUp == "undefined"?$m['isArrowUp']:isArrowUp)($p['getattr'](event, 'keyCode'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})()) {
					$pyjs['track']['lineno']=224;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_is']($p['getattr'](self, '_currentRow'), null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_89_err){if (!$p['isinstance']($pyjs_dbg_89_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_89_err);}throw $pyjs_dbg_89_err;
}})()) {
						$pyjs['track']['lineno']=225;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['setCursorRow']($constant_int_0);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=228;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']($p['getattr'](self, '_isCtlPressed'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_91_err){if (!$p['isinstance']($pyjs_dbg_91_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_91_err);}throw $pyjs_dbg_91_err;
}})()) {
							$pyjs['track']['lineno']=229;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool'](($p['cmp']($p['getattr'](self, '_ctlStartRow'), $p['getattr'](self, '_currentRow')) == -1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_92_err){if (!$p['isinstance']($pyjs_dbg_92_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_92_err);}throw $pyjs_dbg_92_err;
}})()) {
								$pyjs['track']['lineno']=230;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return self['removeSelectedRow']($p['getattr'](self, '_currentRow'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_93_err){if (!$p['isinstance']($pyjs_dbg_93_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_93_err);}throw $pyjs_dbg_93_err;
}})();
							}
							else {
								$pyjs['track']['lineno']=232;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return self['addSelectedRow']($p['getattr'](self, '_currentRow'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_94_err){if (!$p['isinstance']($pyjs_dbg_94_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_94_err);}throw $pyjs_dbg_94_err;
}})();
								$pyjs['track']['lineno']=234;
								if ((function(){try{try{$pyjs['in_try_except'] += 1;
									return $p['bool'](($p['cmp']($p['getattr'](self, '_currentRow'), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_95_err){if (!$p['isinstance']($pyjs_dbg_95_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_95_err);}throw $pyjs_dbg_95_err;
}})()) {
									$pyjs['track']['lineno']=235;
									(function(){try{try{$pyjs['in_try_except'] += 1;
									return self['addSelectedRow']($p['__op_sub']($sub3=$p['getattr'](self, '_currentRow'),$sub4=$constant_int_1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_96_err){if (!$p['isinstance']($pyjs_dbg_96_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_96_err);}throw $pyjs_dbg_96_err;
}})();
								}
							}
						}
						$pyjs['track']['lineno']=237;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](($p['cmp']($p['getattr'](self, '_currentRow'), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_97_err){if (!$p['isinstance']($pyjs_dbg_97_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_97_err);}throw $pyjs_dbg_97_err;
}})()) {
							$pyjs['track']['lineno']=238;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $pyjs_kwargs_call(self, 'setCursorRow', null, null, [{'removeExistingSelection':!$p['bool']($p['getattr'](self, '_isCtlPressed'))}, $p['__op_sub']($sub5=$p['getattr'](self, '_currentRow'),$sub6=$constant_int_1)]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_98_err){if (!$p['isinstance']($pyjs_dbg_98_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_98_err);}throw $pyjs_dbg_98_err;
}})();
						}
					}
					$pyjs['track']['lineno']=241;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return event['preventDefault']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_99_err){if (!$p['isinstance']($pyjs_dbg_99_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_99_err);}throw $pyjs_dbg_99_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof isReturn == "undefined"?$m['isReturn']:isReturn)($p['getattr'](event, 'keyCode'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_100_err){if (!$p['isinstance']($pyjs_dbg_100_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_100_err);}throw $pyjs_dbg_100_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_101_err){if (!$p['isinstance']($pyjs_dbg_101_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_101_err);}throw $pyjs_dbg_101_err;
}})()) {
					$pyjs['track']['lineno']=245;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['len']($p['getattr'](self, '_selectedRows'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_102_err){if (!$p['isinstance']($pyjs_dbg_102_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_102_err);}throw $pyjs_dbg_102_err;
}})(), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_103_err){if (!$p['isinstance']($pyjs_dbg_103_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_103_err);}throw $pyjs_dbg_103_err;
}})()) {
						$pyjs['track']['lineno']=246;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['selectionActivatedEvent']['fire'](self, $p['getattr'](self, '_selectedRows'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_104_err){if (!$p['isinstance']($pyjs_dbg_104_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_104_err);}throw $pyjs_dbg_104_err;
}})();
						$pyjs['track']['lineno']=247;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return event['preventDefault']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_105_err){if (!$p['isinstance']($pyjs_dbg_105_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_105_err);}throw $pyjs_dbg_105_err;
}})();
						$pyjs['track']['lineno']=248;
						$pyjs['track']['lineno']=248;
						var $pyjs__ret = null;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
					$pyjs['track']['lineno']=250;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['op_is']($p['getattr'](self, '_currentRow'), null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_106_err){if (!$p['isinstance']($pyjs_dbg_106_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_106_err);}throw $pyjs_dbg_106_err;
}})()) {
						$pyjs['track']['lineno']=251;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['selectionActivatedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['list']([$p['getattr'](self, '_currentRow')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_107_err){if (!$p['isinstance']($pyjs_dbg_107_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_107_err);}throw $pyjs_dbg_107_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_108_err){if (!$p['isinstance']($pyjs_dbg_108_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_108_err);}throw $pyjs_dbg_108_err;
}})();
						$pyjs['track']['lineno']=252;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return event['preventDefault']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_109_err){if (!$p['isinstance']($pyjs_dbg_109_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_109_err);}throw $pyjs_dbg_109_err;
}})();
						$pyjs['track']['lineno']=253;
						$pyjs['track']['lineno']=253;
						var $pyjs__ret = null;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof isSingleSelectionKey == "undefined"?$m['isSingleSelectionKey']:isSingleSelectionKey)($p['getattr'](event, 'keyCode'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_110_err){if (!$p['isinstance']($pyjs_dbg_110_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_110_err);}throw $pyjs_dbg_110_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_111_err){if (!$p['isinstance']($pyjs_dbg_111_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_111_err);}throw $pyjs_dbg_111_err;
}})()) {
					$pyjs['track']['lineno']=256;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isCtlPressed', true) : $p['setattr'](self, '_isCtlPressed', true); 
					$pyjs['track']['lineno']=257;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_ctlStartRow', ($p['bool']($or1=$p['getattr'](self, '_currentRow'))?$or1:$constant_int_0)) : $p['setattr'](self, '_ctlStartRow', ($p['bool']($or1=$p['getattr'](self, '_currentRow'))?$or1:$constant_int_0)); 
					$pyjs['track']['lineno']=259;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and5=!$p['op_is']($p['getattr'](self, '_currentRow'), null))?!$p['bool']($p['getattr'](self, '_selectedRows')['__contains__']($p['getattr'](self, '_currentRow'))):$and5));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_112_err){if (!$p['isinstance']($pyjs_dbg_112_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_112_err);}throw $pyjs_dbg_112_err;
}})()) {
						$pyjs['track']['lineno']=260;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['addSelectedRow']($p['getattr'](self, '_currentRow'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_113_err){if (!$p['isinstance']($pyjs_dbg_113_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_113_err);}throw $pyjs_dbg_113_err;
}})();
						$pyjs['track']['lineno']=261;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call(self, 'setCursorRow', null, null, [{'removeExistingSelection':false}, null]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_114_err){if (!$p['isinstance']($pyjs_dbg_114_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_114_err);}throw $pyjs_dbg_114_err;
}})();
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onKeyDown'] = $method;
			$pyjs['track']['lineno']=263;
			$method = $pyjs__bind_method2('onKeyUp', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.table', 'lineno':263};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=263;
				$pyjs['track']['lineno']=264;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof isSingleSelectionKey == "undefined"?$m['isSingleSelectionKey']:isSingleSelectionKey)($p['getattr'](event, 'keyCode'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_115_err){if (!$p['isinstance']($pyjs_dbg_115_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_115_err);}throw $pyjs_dbg_115_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_116_err){if (!$p['isinstance']($pyjs_dbg_116_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_116_err);}throw $pyjs_dbg_116_err;
}})()) {
					$pyjs['track']['lineno']=265;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isCtlPressed', false) : $p['setattr'](self, '_isCtlPressed', false); 
					$pyjs['track']['lineno']=266;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_ctlStartRow', null) : $p['setattr'](self, '_ctlStartRow', null); 
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onKeyUp'] = $method;
			$pyjs['track']['lineno']=268;
			$method = $pyjs__bind_method2('onDblClick', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.table', 'lineno':268};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=268;
				$pyjs['track']['lineno']=269;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['op_is']($p['getattr'](self, '_currentRow'), null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_117_err){if (!$p['isinstance']($pyjs_dbg_117_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_117_err);}throw $pyjs_dbg_117_err;
}})()) {
					$pyjs['track']['lineno']=270;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['selectionActivatedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([$p['getattr'](self, '_currentRow')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_118_err){if (!$p['isinstance']($pyjs_dbg_118_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_118_err);}throw $pyjs_dbg_118_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_119_err){if (!$p['isinstance']($pyjs_dbg_119_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_119_err);}throw $pyjs_dbg_119_err;
}})();
				}
				$pyjs['track']['lineno']=271;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return event['preventDefault']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_120_err){if (!$p['isinstance']($pyjs_dbg_120_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_120_err);}throw $pyjs_dbg_120_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onDblClick'] = $method;
			$pyjs['track']['lineno']=273;
			$method = $pyjs__bind_method2('addSelectedRow', function(row) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					row = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var tr;
				$pyjs['track']={'module':'widgets.table', 'lineno':273};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=273;
				$pyjs['track']['lineno']=277;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, '_selectedRows')['__contains__'](row));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_121_err){if (!$p['isinstance']($pyjs_dbg_121_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_121_err);}throw $pyjs_dbg_121_err;
}})()) {
					$pyjs['track']['lineno']=278;
					$pyjs['track']['lineno']=278;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=280;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_selectedRows']['append'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_122_err){if (!$p['isinstance']($pyjs_dbg_122_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_122_err);}throw $pyjs_dbg_122_err;
}})();
				$pyjs['track']['lineno']=282;
				tr = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getTrByIndex'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_123_err){if (!$p['isinstance']($pyjs_dbg_123_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_123_err);}throw $pyjs_dbg_123_err;
}})();
				$pyjs['track']['lineno']=283;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return tr['__getitem__']('class')['append']('is_selected');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_124_err){if (!$p['isinstance']($pyjs_dbg_124_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_124_err);}throw $pyjs_dbg_124_err;
}})();
				$pyjs['track']['lineno']=285;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'checkboxes'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_125_err){if (!$p['isinstance']($pyjs_dbg_125_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_125_err);}throw $pyjs_dbg_125_err;
}})()) {
					$pyjs['track']['lineno']=286;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, '_checkboxes')['__getitem__'](row)['__setitem__']('checked', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_126_err){if (!$p['isinstance']($pyjs_dbg_126_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_126_err);}throw $pyjs_dbg_126_err;
}})();
				}
				$pyjs['track']['lineno']=288;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionChangedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_127_err){if (!$p['isinstance']($pyjs_dbg_127_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_127_err);}throw $pyjs_dbg_127_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_128_err){if (!$p['isinstance']($pyjs_dbg_128_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_128_err);}throw $pyjs_dbg_128_err;
}})();
				$pyjs['track']['lineno']=290;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['printFunc']([(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('Currently selected rows: %s', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['str']($p['getattr'](self, '_selectedRows'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_129_err){if (!$p['isinstance']($pyjs_dbg_129_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_129_err);}throw $pyjs_dbg_129_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_130_err){if (!$p['isinstance']($pyjs_dbg_130_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_130_err);}throw $pyjs_dbg_130_err;
}})()], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_131_err){if (!$p['isinstance']($pyjs_dbg_131_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_131_err);}throw $pyjs_dbg_131_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['row']]);
			$cls_definition['addSelectedRow'] = $method;
			$pyjs['track']['lineno']=292;
			$method = $pyjs__bind_method2('removeSelectedRow', function(row) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					row = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var tr;
				$pyjs['track']={'module':'widgets.table', 'lineno':292};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=292;
				$pyjs['track']['lineno']=298;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, '_selectedRows')['__contains__'](row)));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_132_err){if (!$p['isinstance']($pyjs_dbg_132_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_132_err);}throw $pyjs_dbg_132_err;
}})()) {
					$pyjs['track']['lineno']=299;
					$pyjs['track']['lineno']=299;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=301;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_selectedRows']['remove'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_133_err){if (!$p['isinstance']($pyjs_dbg_133_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_133_err);}throw $pyjs_dbg_133_err;
}})();
				$pyjs['track']['lineno']=303;
				tr = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getTrByIndex'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_134_err){if (!$p['isinstance']($pyjs_dbg_134_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_134_err);}throw $pyjs_dbg_134_err;
}})();
				$pyjs['track']['lineno']=304;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return tr['__getitem__']('class')['remove']('is_selected');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_135_err){if (!$p['isinstance']($pyjs_dbg_135_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_135_err);}throw $pyjs_dbg_135_err;
}})();
				$pyjs['track']['lineno']=306;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'checkboxes'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_136_err){if (!$p['isinstance']($pyjs_dbg_136_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_136_err);}throw $pyjs_dbg_136_err;
}})()) {
					$pyjs['track']['lineno']=307;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, '_checkboxes')['__getitem__'](row)['__setitem__']('checked', false);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_137_err){if (!$p['isinstance']($pyjs_dbg_137_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_137_err);}throw $pyjs_dbg_137_err;
}})();
				}
				$pyjs['track']['lineno']=309;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionChangedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_138_err){if (!$p['isinstance']($pyjs_dbg_138_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_138_err);}throw $pyjs_dbg_138_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_139_err){if (!$p['isinstance']($pyjs_dbg_139_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_139_err);}throw $pyjs_dbg_139_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['row']]);
			$cls_definition['removeSelectedRow'] = $method;
			$pyjs['track']['lineno']=311;
			$method = $pyjs__bind_method2('selectRow', function(newRow) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					newRow = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter4_nextval,tr,$iter4_idx,$iter4_type,$pyjs__trackstack_size_1,$iter4_array,$iter4_iter,row;
				$pyjs['track']={'module':'widgets.table', 'lineno':311};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=311;
				$pyjs['track']['lineno']=318;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['setCursorRow'](newRow);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_140_err){if (!$p['isinstance']($pyjs_dbg_140_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_140_err);}throw $pyjs_dbg_140_err;
}})();
				$pyjs['track']['lineno']=320;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter4_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__getslice']($p['getattr'](self, '_selectedRows'), 0, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_141_err){if (!$p['isinstance']($pyjs_dbg_141_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_141_err);}throw $pyjs_dbg_141_err;
}})();
				$iter4_nextval=$p['__iter_prepare']($iter4_iter,false);
				while (typeof($p['__wrapped_next']($iter4_nextval)['$nextval']) != 'undefined') {
					row = $iter4_nextval['$nextval'];
					$pyjs['track']['lineno']=321;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['op_eq'](row, newRow));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_142_err){if (!$p['isinstance']($pyjs_dbg_142_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_142_err);}throw $pyjs_dbg_142_err;
}})()) {
						$pyjs['track']['lineno']=322;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['removeSelectedRow'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_143_err){if (!$p['isinstance']($pyjs_dbg_143_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_143_err);}throw $pyjs_dbg_143_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=324;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, '_selectedRows')['__contains__'](newRow)));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_144_err){if (!$p['isinstance']($pyjs_dbg_144_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_144_err);}throw $pyjs_dbg_144_err;
}})()) {
					$pyjs['track']['lineno']=325;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['_selectedRows']['append'](newRow);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_145_err){if (!$p['isinstance']($pyjs_dbg_145_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_145_err);}throw $pyjs_dbg_145_err;
}})();
					$pyjs['track']['lineno']=326;
					tr = (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['getTrByIndex'](newRow);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_146_err){if (!$p['isinstance']($pyjs_dbg_146_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_146_err);}throw $pyjs_dbg_146_err;
}})();
					$pyjs['track']['lineno']=327;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return tr['__getitem__']('class')['append']('is_selected');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_147_err){if (!$p['isinstance']($pyjs_dbg_147_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_147_err);}throw $pyjs_dbg_147_err;
}})();
				}
				$pyjs['track']['lineno']=329;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionChangedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_148_err){if (!$p['isinstance']($pyjs_dbg_148_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_148_err);}throw $pyjs_dbg_148_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_149_err){if (!$p['isinstance']($pyjs_dbg_149_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_149_err);}throw $pyjs_dbg_149_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['newRow']]);
			$cls_definition['selectRow'] = $method;
			$pyjs['track']['lineno']=331;
			$method = $pyjs__bind_method2('setCursorRow', function(row, removeExistingSelection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					row = arguments[1];
					removeExistingSelection = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 2 || arguments['length'] > 3)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof removeExistingSelection == 'undefined') removeExistingSelection=arguments['callee']['__args__'][4][1];
				var $iter5_nextval,$iter5_array,$pyjs__trackstack_size_1,$iter5_iter,$iter5_idx,$iter5_type;
				$pyjs['track']={'module':'widgets.table', 'lineno':331};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=331;
				$pyjs['track']['lineno']=336;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['op_is']($p['getattr'](self, '_currentRow'), null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_150_err){if (!$p['isinstance']($pyjs_dbg_150_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_150_err);}throw $pyjs_dbg_150_err;
}})()) {
					$pyjs['track']['lineno']=337;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['getTrByIndex']($p['getattr'](self, '_currentRow'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_151_err){if (!$p['isinstance']($pyjs_dbg_151_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_151_err);}throw $pyjs_dbg_151_err;
}})()['__getitem__']('class')['remove']('is_focused');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_152_err){if (!$p['isinstance']($pyjs_dbg_152_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_152_err);}throw $pyjs_dbg_152_err;
}})();
				}
				$pyjs['track']['lineno']=339;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_currentRow', row) : $p['setattr'](self, '_currentRow', row); 
				$pyjs['track']['lineno']=340;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['op_is']($p['getattr'](self, '_currentRow'), null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_153_err){if (!$p['isinstance']($pyjs_dbg_153_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_153_err);}throw $pyjs_dbg_153_err;
}})()) {
					$pyjs['track']['lineno']=341;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['getTrByIndex']($p['getattr'](self, '_currentRow'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_154_err){if (!$p['isinstance']($pyjs_dbg_154_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_154_err);}throw $pyjs_dbg_154_err;
}})()['__getitem__']('class')['append']('is_focused');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_155_err){if (!$p['isinstance']($pyjs_dbg_155_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_155_err);}throw $pyjs_dbg_155_err;
}})();
					$pyjs['track']['lineno']=342;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['cursorMovedEvent']['fire'](self, row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_156_err){if (!$p['isinstance']($pyjs_dbg_156_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_156_err);}throw $pyjs_dbg_156_err;
}})();
				}
				$pyjs['track']['lineno']=344;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](removeExistingSelection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_157_err){if (!$p['isinstance']($pyjs_dbg_157_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_157_err);}throw $pyjs_dbg_157_err;
}})()) {
					$pyjs['track']['lineno']=345;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter5_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['__getslice']($p['getattr'](self, '_selectedRows'), 0, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_158_err){if (!$p['isinstance']($pyjs_dbg_158_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_158_err);}throw $pyjs_dbg_158_err;
}})();
					$iter5_nextval=$p['__iter_prepare']($iter5_iter,false);
					while (typeof($p['__wrapped_next']($iter5_nextval)['$nextval']) != 'undefined') {
						row = $iter5_nextval['$nextval'];
						$pyjs['track']['lineno']=346;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['removeSelectedRow'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_159_err){if (!$p['isinstance']($pyjs_dbg_159_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_159_err);}throw $pyjs_dbg_159_err;
}})();
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='widgets.table';
					$pyjs['track']['lineno']=347;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['selectionChangedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['getCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_160_err){if (!$p['isinstance']($pyjs_dbg_160_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_160_err);}throw $pyjs_dbg_160_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_161_err){if (!$p['isinstance']($pyjs_dbg_161_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_161_err);}throw $pyjs_dbg_161_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['row'],['removeExistingSelection', true]]);
			$cls_definition['setCursorRow'] = $method;
			$pyjs['track']['lineno']=349;
			$method = $pyjs__bind_method2('getCurrentSelection', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.table', 'lineno':349};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=349;
				$pyjs['track']['lineno']=354;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, '_selectedRows'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_162_err){if (!$p['isinstance']($pyjs_dbg_162_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_162_err);}throw $pyjs_dbg_162_err;
}})()) {
					$pyjs['track']['lineno']=355;
					$pyjs['track']['lineno']=355;
					var $pyjs__ret = $p['__getslice']($p['getattr'](self, '_selectedRows'), 0, null);
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['op_is']($p['getattr'](self, '_currentRow'), null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_163_err){if (!$p['isinstance']($pyjs_dbg_163_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_163_err);}throw $pyjs_dbg_163_err;
}})()) {
					$pyjs['track']['lineno']=357;
					$pyjs['track']['lineno']=357;
					var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([$p['getattr'](self, '_currentRow')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_164_err){if (!$p['isinstance']($pyjs_dbg_164_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_164_err);}throw $pyjs_dbg_164_err;
}})();
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=359;
				$pyjs['track']['lineno']=359;
				var $pyjs__ret = null;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['getCurrentSelection'] = $method;
			$pyjs['track']['lineno']=361;
			$method = $pyjs__bind_method2('clear', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.table', 'lineno':361};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=361;
				$pyjs['track']['lineno']=365;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectTable'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_165_err){if (!$p['isinstance']($pyjs_dbg_165_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_165_err);}throw $pyjs_dbg_165_err;
}})()['clear']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_166_err){if (!$p['isinstance']($pyjs_dbg_166_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_166_err);}throw $pyjs_dbg_166_err;
}})();
				$pyjs['track']['lineno']=366;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_currentRow', null) : $p['setattr'](self, '_currentRow', null); 
				$pyjs['track']['lineno']=367;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_selectedRows', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_167_err){if (!$p['isinstance']($pyjs_dbg_167_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_167_err);}throw $pyjs_dbg_167_err;
}})()) : $p['setattr'](self, '_selectedRows', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_167_err){if (!$p['isinstance']($pyjs_dbg_167_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_167_err);}throw $pyjs_dbg_167_err;
}})()); 
				$pyjs['track']['lineno']=369;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionChangedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_168_err){if (!$p['isinstance']($pyjs_dbg_168_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_168_err);}throw $pyjs_dbg_168_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_169_err){if (!$p['isinstance']($pyjs_dbg_169_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_169_err);}throw $pyjs_dbg_169_err;
}})();
				$pyjs['track']['lineno']=370;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['tableChangedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getRowCount']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_170_err){if (!$p['isinstance']($pyjs_dbg_170_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_170_err);}throw $pyjs_dbg_170_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_171_err){if (!$p['isinstance']($pyjs_dbg_171_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_171_err);}throw $pyjs_dbg_171_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['clear'] = $method;
			$pyjs['track']['lineno']=372;
			$method = $pyjs__bind_method2('removeRow', function(row) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					row = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.table', 'lineno':372};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=372;
				$pyjs['track']['lineno']=376;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, '_selectedRows')['__contains__'](row));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_172_err){if (!$p['isinstance']($pyjs_dbg_172_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_172_err);}throw $pyjs_dbg_172_err;
}})()) {
					$pyjs['track']['lineno']=377;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['_selectedRows']['remove'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_173_err){if (!$p['isinstance']($pyjs_dbg_173_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_173_err);}throw $pyjs_dbg_173_err;
}})();
					$pyjs['track']['lineno']=378;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['selectionChangedEvent']['fire'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_174_err){if (!$p['isinstance']($pyjs_dbg_174_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_174_err);}throw $pyjs_dbg_174_err;
}})();
				}
				$pyjs['track']['lineno']=380;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](self, '_currentRow'), row));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_175_err){if (!$p['isinstance']($pyjs_dbg_175_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_175_err);}throw $pyjs_dbg_175_err;
}})()) {
					$pyjs['track']['lineno']=381;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_currentRow', null) : $p['setattr'](self, '_currentRow', null); 
					$pyjs['track']['lineno']=382;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['cursorMovedEvent']['fire'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_176_err){if (!$p['isinstance']($pyjs_dbg_176_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_176_err);}throw $pyjs_dbg_176_err;
}})();
				}
				$pyjs['track']['lineno']=384;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['tableChangedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getRowCount']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_177_err){if (!$p['isinstance']($pyjs_dbg_177_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_177_err);}throw $pyjs_dbg_177_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_178_err){if (!$p['isinstance']($pyjs_dbg_178_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_178_err);}throw $pyjs_dbg_178_err;
}})();
				$pyjs['track']['lineno']=385;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectTable'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_179_err){if (!$p['isinstance']($pyjs_dbg_179_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_179_err);}throw $pyjs_dbg_179_err;
}})()['removeRow'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_180_err){if (!$p['isinstance']($pyjs_dbg_180_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_180_err);}throw $pyjs_dbg_180_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['row']]);
			$cls_definition['removeRow'] = $method;
			$pyjs['track']['lineno']=387;
			$method = $pyjs__bind_method2('_extraCols', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $add14,$add13;
				$pyjs['track']={'module':'widgets.table', 'lineno':387};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=387;
				$pyjs['track']['lineno']=388;
				$pyjs['track']['lineno']=388;
				var $pyjs__ret = $p['__op_add']($add13=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['int']($p['getattr'](self, 'checkboxes'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_181_err){if (!$p['isinstance']($pyjs_dbg_181_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_181_err);}throw $pyjs_dbg_181_err;
}})(),$add14=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['int']($p['getattr'](self, 'indexes'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_182_err){if (!$p['isinstance']($pyjs_dbg_182_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_182_err);}throw $pyjs_dbg_182_err;
}})());
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['_extraCols'] = $method;
			$pyjs['track']['lineno']=390;
			$method = $pyjs__bind_method2('prepareCol', function(row, col) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					row = arguments[1];
					col = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var checkbox,$add18,lbl,$add15,$add16,$sub8,$sub7,$add17;
				$pyjs['track']={'module':'widgets.table', 'lineno':390};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=390;
				$pyjs['track']['lineno']=395;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['SelectTable'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_183_err){if (!$p['isinstance']($pyjs_dbg_183_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_183_err);}throw $pyjs_dbg_183_err;
}})()['prepareCol'](row, $p['__op_sub']($sub7=$p['__op_add']($add15=col,$add16=(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_extraCols']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_184_err){if (!$p['isinstance']($pyjs_dbg_184_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_184_err);}throw $pyjs_dbg_184_err;
}})()),$sub8=$constant_int_1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_185_err){if (!$p['isinstance']($pyjs_dbg_185_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_185_err);}throw $pyjs_dbg_185_err;
}})();
				$pyjs['track']['lineno']=397;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'checkboxes'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_186_err){if (!$p['isinstance']($pyjs_dbg_186_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_186_err);}throw $pyjs_dbg_186_err;
}})()) {
					$pyjs['track']['lineno']=398;
					checkbox = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_187_err){if (!$p['isinstance']($pyjs_dbg_187_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_187_err);}throw $pyjs_dbg_187_err;
}})();
					$pyjs['track']['lineno']=399;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return checkbox['__setitem__']('type', 'checkbox');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_188_err){if (!$p['isinstance']($pyjs_dbg_188_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_188_err);}throw $pyjs_dbg_188_err;
}})();
					$pyjs['track']['lineno']=400;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return checkbox['__getitem__']('class')['append']('check');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_189_err){if (!$p['isinstance']($pyjs_dbg_189_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_189_err);}throw $pyjs_dbg_189_err;
}})();
					$pyjs['track']['lineno']=401;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return checkbox['__setitem__']('checked', false);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_190_err){if (!$p['isinstance']($pyjs_dbg_190_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_190_err);}throw $pyjs_dbg_190_err;
}})();
					$pyjs['track']['lineno']=403;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__getitem__']('cell')['__getitem__'](row)['__setitem__']($p['getattr'](self, 'checkboxes_col'), checkbox);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_191_err){if (!$p['isinstance']($pyjs_dbg_191_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_191_err);}throw $pyjs_dbg_191_err;
}})();
					$pyjs['track']['lineno']=404;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, '_checkboxes')['__setitem__'](row, checkbox);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_192_err){if (!$p['isinstance']($pyjs_dbg_192_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_192_err);}throw $pyjs_dbg_192_err;
}})();
				}
				$pyjs['track']['lineno']=406;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'indexes'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_193_err){if (!$p['isinstance']($pyjs_dbg_193_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_193_err);}throw $pyjs_dbg_193_err;
}})()) {
					$pyjs['track']['lineno']=407;
					lbl = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Label']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['str']($p['__op_add']($add17=row,$add18=$constant_int_1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_194_err){if (!$p['isinstance']($pyjs_dbg_194_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_194_err);}throw $pyjs_dbg_194_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_195_err){if (!$p['isinstance']($pyjs_dbg_195_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_195_err);}throw $pyjs_dbg_195_err;
}})();
					$pyjs['track']['lineno']=408;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return lbl['__getitem__']('class')['append']('index');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_196_err){if (!$p['isinstance']($pyjs_dbg_196_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_196_err);}throw $pyjs_dbg_196_err;
}})();
					$pyjs['track']['lineno']=409;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__getitem__']('cell')['__getitem__'](row)['__setitem__']($p['getattr'](self, 'indexes_col'), lbl);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_197_err){if (!$p['isinstance']($pyjs_dbg_197_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_197_err);}throw $pyjs_dbg_197_err;
}})();
				}
				$pyjs['track']['lineno']=411;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['tableChangedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getRowCount']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_198_err){if (!$p['isinstance']($pyjs_dbg_198_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_198_err);}throw $pyjs_dbg_198_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_199_err){if (!$p['isinstance']($pyjs_dbg_199_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_199_err);}throw $pyjs_dbg_199_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['row'],['col']]);
			$cls_definition['prepareCol'] = $method;
			$pyjs['track']['lineno']=413;
			$method = $pyjs__bind_method2('setCell', function(row, col, val) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 4, arguments['length']+1);
				} else {
					var self = arguments[0];
					row = arguments[1];
					col = arguments[2];
					val = arguments[3];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 4, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $add20,$add19;
				$pyjs['track']={'module':'widgets.table', 'lineno':413};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=413;
				$pyjs['track']['lineno']=418;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__getitem__']('cell')['__getitem__'](row)['__setitem__']($p['__op_add']($add19=col,$add20=(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_extraCols']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_200_err){if (!$p['isinstance']($pyjs_dbg_200_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_200_err);}throw $pyjs_dbg_200_err;
}})()), val);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_201_err){if (!$p['isinstance']($pyjs_dbg_201_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_201_err);}throw $pyjs_dbg_201_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['row'],['col'],['val']]);
			$cls_definition['setCell'] = $method;
			$pyjs['track']['lineno']=420;
			$method = $pyjs__bind_method2('selectAll', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter6_idx,$iter6_type,tr,$iter6_array,$pyjs__trackstack_size_1,$iter6_iter,row,$iter6_nextval;
				$pyjs['track']={'module':'widgets.table', 'lineno':420};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=420;
				$pyjs['track']['lineno']=424;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_selectedRows', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['range']($constant_int_0, (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getRowCount']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_202_err){if (!$p['isinstance']($pyjs_dbg_202_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_202_err);}throw $pyjs_dbg_202_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_203_err){if (!$p['isinstance']($pyjs_dbg_203_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_203_err);}throw $pyjs_dbg_203_err;
}})()) : $p['setattr'](self, '_selectedRows', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['range']($constant_int_0, (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getRowCount']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_202_err){if (!$p['isinstance']($pyjs_dbg_202_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_202_err);}throw $pyjs_dbg_202_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_203_err){if (!$p['isinstance']($pyjs_dbg_203_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_203_err);}throw $pyjs_dbg_203_err;
}})()); 
				$pyjs['track']['lineno']=426;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter6_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, '_selectedRows');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_204_err){if (!$p['isinstance']($pyjs_dbg_204_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_204_err);}throw $pyjs_dbg_204_err;
}})();
				$iter6_nextval=$p['__iter_prepare']($iter6_iter,false);
				while (typeof($p['__wrapped_next']($iter6_nextval)['$nextval']) != 'undefined') {
					row = $iter6_nextval['$nextval'];
					$pyjs['track']['lineno']=427;
					tr = (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['getTrByIndex'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_205_err){if (!$p['isinstance']($pyjs_dbg_205_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_205_err);}throw $pyjs_dbg_205_err;
}})();
					$pyjs['track']['lineno']=429;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool'](tr['__getitem__']('class')['__contains__']('is_selected')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_206_err){if (!$p['isinstance']($pyjs_dbg_206_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_206_err);}throw $pyjs_dbg_206_err;
}})()) {
						$pyjs['track']['lineno']=430;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return tr['__getitem__']('class')['append']('is_selected');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_207_err){if (!$p['isinstance']($pyjs_dbg_207_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_207_err);}throw $pyjs_dbg_207_err;
}})();
					}
					$pyjs['track']['lineno']=432;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'checkboxes'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_208_err){if (!$p['isinstance']($pyjs_dbg_208_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_208_err);}throw $pyjs_dbg_208_err;
}})()) {
						$pyjs['track']['lineno']=433;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, '_checkboxes')['__getitem__'](row)['__setitem__']('checked', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_209_err){if (!$p['isinstance']($pyjs_dbg_209_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_209_err);}throw $pyjs_dbg_209_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=435;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionChangedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_210_err){if (!$p['isinstance']($pyjs_dbg_210_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_210_err);}throw $pyjs_dbg_210_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_211_err){if (!$p['isinstance']($pyjs_dbg_211_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_211_err);}throw $pyjs_dbg_211_err;
}})();
				$pyjs['track']['lineno']=436;
				$pyjs['track']['lineno']=436;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len']($p['getattr'](self, '_selectedRows'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_212_err){if (!$p['isinstance']($pyjs_dbg_212_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_212_err);}throw $pyjs_dbg_212_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['selectAll'] = $method;
			$pyjs['track']['lineno']=438;
			$method = $pyjs__bind_method2('unSelectAll', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var unsel,$iter7_nextval,$iter7_iter,$iter7_array,tr,$iter7_idx,$iter7_type,$pyjs__trackstack_size_1,row;
				$pyjs['track']={'module':'widgets.table', 'lineno':438};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=438;
				$pyjs['track']['lineno']=442;
				unsel = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len']($p['getattr'](self, '_selectedRows'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_213_err){if (!$p['isinstance']($pyjs_dbg_213_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_213_err);}throw $pyjs_dbg_213_err;
}})();
				$pyjs['track']['lineno']=444;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter7_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, '_selectedRows');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_214_err){if (!$p['isinstance']($pyjs_dbg_214_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_214_err);}throw $pyjs_dbg_214_err;
}})();
				$iter7_nextval=$p['__iter_prepare']($iter7_iter,false);
				while (typeof($p['__wrapped_next']($iter7_nextval)['$nextval']) != 'undefined') {
					row = $iter7_nextval['$nextval'];
					$pyjs['track']['lineno']=445;
					tr = (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['getTrByIndex'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_215_err){if (!$p['isinstance']($pyjs_dbg_215_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_215_err);}throw $pyjs_dbg_215_err;
}})();
					$pyjs['track']['lineno']=446;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return tr['__getitem__']('class')['remove']('is_selected');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_216_err){if (!$p['isinstance']($pyjs_dbg_216_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_216_err);}throw $pyjs_dbg_216_err;
}})();
					$pyjs['track']['lineno']=448;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'checkboxes'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_217_err){if (!$p['isinstance']($pyjs_dbg_217_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_217_err);}throw $pyjs_dbg_217_err;
}})()) {
						$pyjs['track']['lineno']=449;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, '_checkboxes')['__getitem__'](row)['__setitem__']('checked', false);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_218_err){if (!$p['isinstance']($pyjs_dbg_218_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_218_err);}throw $pyjs_dbg_218_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=451;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_selectedRows', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_219_err){if (!$p['isinstance']($pyjs_dbg_219_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_219_err);}throw $pyjs_dbg_219_err;
}})()) : $p['setattr'](self, '_selectedRows', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_219_err){if (!$p['isinstance']($pyjs_dbg_219_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_219_err);}throw $pyjs_dbg_219_err;
}})()); 
				$pyjs['track']['lineno']=452;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionChangedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_220_err){if (!$p['isinstance']($pyjs_dbg_220_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_220_err);}throw $pyjs_dbg_220_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_221_err){if (!$p['isinstance']($pyjs_dbg_221_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_221_err);}throw $pyjs_dbg_221_err;
}})();
				$pyjs['track']['lineno']=453;
				$pyjs['track']['lineno']=453;
				var $pyjs__ret = unsel;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['unSelectAll'] = $method;
			$pyjs['track']['lineno']=455;
			$method = $pyjs__bind_method2('invertSelection', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '31ccbed966e1600b9f2f1f77bbf82ee2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter8_iter,$iter8_idx,tr,$iter8_array,current,$iter8_nextval,$pyjs__trackstack_size_1,$iter8_type,row;
				$pyjs['track']={'module':'widgets.table', 'lineno':455};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=455;
				$pyjs['track']['lineno']=459;
				current = $p['__getslice']($p['getattr'](self, '_selectedRows'), 0, null);
				$pyjs['track']['lineno']=460;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_selectedRows', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_222_err){if (!$p['isinstance']($pyjs_dbg_222_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_222_err);}throw $pyjs_dbg_222_err;
}})()) : $p['setattr'](self, '_selectedRows', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_222_err){if (!$p['isinstance']($pyjs_dbg_222_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_222_err);}throw $pyjs_dbg_222_err;
}})()); 
				$pyjs['track']['lineno']=462;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter8_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['range']($constant_int_0, (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getRowCount']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_223_err){if (!$p['isinstance']($pyjs_dbg_223_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_223_err);}throw $pyjs_dbg_223_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_224_err){if (!$p['isinstance']($pyjs_dbg_224_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_224_err);}throw $pyjs_dbg_224_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_225_err){if (!$p['isinstance']($pyjs_dbg_225_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_225_err);}throw $pyjs_dbg_225_err;
}})();
				$iter8_nextval=$p['__iter_prepare']($iter8_iter,false);
				while (typeof($p['__wrapped_next']($iter8_nextval)['$nextval']) != 'undefined') {
					row = $iter8_nextval['$nextval'];
					$pyjs['track']['lineno']=463;
					tr = (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['getTrByIndex'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_226_err){if (!$p['isinstance']($pyjs_dbg_226_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_226_err);}throw $pyjs_dbg_226_err;
}})();
					$pyjs['track']['lineno']=465;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](current['__contains__'](row));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_227_err){if (!$p['isinstance']($pyjs_dbg_227_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_227_err);}throw $pyjs_dbg_227_err;
}})()) {
						$pyjs['track']['lineno']=466;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return tr['__getitem__']('class')['remove']('is_selected');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_228_err){if (!$p['isinstance']($pyjs_dbg_228_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_228_err);}throw $pyjs_dbg_228_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=468;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['_selectedRows']['append'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_229_err){if (!$p['isinstance']($pyjs_dbg_229_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_229_err);}throw $pyjs_dbg_229_err;
}})();
						$pyjs['track']['lineno']=469;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return tr['__getitem__']('class')['append']('is_selected');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_230_err){if (!$p['isinstance']($pyjs_dbg_230_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_230_err);}throw $pyjs_dbg_230_err;
}})();
					}
					$pyjs['track']['lineno']=471;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'checkboxes'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_231_err){if (!$p['isinstance']($pyjs_dbg_231_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_231_err);}throw $pyjs_dbg_231_err;
}})()) {
						$pyjs['track']['lineno']=472;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, '_checkboxes')['__getitem__'](row)['__setitem__']('checked', $p['getattr'](self, '_selectedRows')['__contains__'](row));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_232_err){if (!$p['isinstance']($pyjs_dbg_232_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_232_err);}throw $pyjs_dbg_232_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=474;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionChangedEvent']['fire'](self, (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_233_err){if (!$p['isinstance']($pyjs_dbg_233_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_233_err);}throw $pyjs_dbg_233_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_234_err){if (!$p['isinstance']($pyjs_dbg_234_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_234_err);}throw $pyjs_dbg_234_err;
}})();
				$pyjs['track']['lineno']=475;
				$pyjs['track']['lineno']=475;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['tuple']([(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len']($p['getattr'](self, '_selectedRows'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_235_err){if (!$p['isinstance']($pyjs_dbg_235_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_235_err);}throw $pyjs_dbg_235_err;
}})(), (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](current);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_236_err){if (!$p['isinstance']($pyjs_dbg_236_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_236_err);}throw $pyjs_dbg_236_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_237_err){if (!$p['isinstance']($pyjs_dbg_237_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_237_err);}throw $pyjs_dbg_237_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['invertSelection'] = $method;
			$pyjs['track']['lineno']=5;
			var $bases = new Array($p['getattr']($m['html5'], 'Table'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('SelectTable', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=479;
		$m['DataTable'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'widgets.table';
			$cls_definition['__md5__'] = '50dd62acd197ce3eed7df6c8fd49babb';
			$pyjs['track']['lineno']=484;
			$method = $pyjs__bind_method2('__init__', function(_loadOnDisplay) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					_loadOnDisplay = arguments[1];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,2,arguments['length']-1));

					var kwargs = arguments['length'] >= 3 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof _loadOnDisplay != 'undefined') {
						if (_loadOnDisplay !== null && typeof _loadOnDisplay['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = _loadOnDisplay;
							_loadOnDisplay = arguments[2];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[2];
						}
					} else {
					}
				}
				if (typeof _loadOnDisplay == 'undefined') _loadOnDisplay=arguments['callee']['__args__'][3][1];
				var f,$iter9_iter,$iter9_nextval,$iter9_idx,$iter9_array,$pyjs__trackstack_size_1,$iter9_type;
				$pyjs['track']={'module':'widgets.table', 'lineno':484};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=484;
				$pyjs['track']['lineno']=485;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['DataTable'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_238_err){if (!$p['isinstance']($pyjs_dbg_238_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_238_err);}throw $pyjs_dbg_238_err;
}})()['__init__']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_239_err){if (!$p['isinstance']($pyjs_dbg_239_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_239_err);}throw $pyjs_dbg_239_err;
}})();
				$pyjs['track']['lineno']=486;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('table', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['SelectTable'], args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_240_err){if (!$p['isinstance']($pyjs_dbg_240_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_240_err);}throw $pyjs_dbg_240_err;
}})()) : $p['setattr'](self, 'table', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['SelectTable'], args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_240_err){if (!$p['isinstance']($pyjs_dbg_240_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_240_err);}throw $pyjs_dbg_240_err;
}})()); 
				$pyjs['track']['lineno']=487;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'table'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_241_err){if (!$p['isinstance']($pyjs_dbg_241_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_241_err);}throw $pyjs_dbg_241_err;
}})();
				$pyjs['track']['lineno']=489;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_loadOnDisplay', _loadOnDisplay) : $p['setattr'](self, '_loadOnDisplay', _loadOnDisplay); 
				$pyjs['track']['lineno']=491;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_model', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_242_err){if (!$p['isinstance']($pyjs_dbg_242_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_242_err);}throw $pyjs_dbg_242_err;
}})()) : $p['setattr'](self, '_model', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_242_err){if (!$p['isinstance']($pyjs_dbg_242_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_242_err);}throw $pyjs_dbg_242_err;
}})()); 
				$pyjs['track']['lineno']=492;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_shownFields', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_243_err){if (!$p['isinstance']($pyjs_dbg_243_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_243_err);}throw $pyjs_dbg_243_err;
}})()) : $p['setattr'](self, '_shownFields', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_243_err){if (!$p['isinstance']($pyjs_dbg_243_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_243_err);}throw $pyjs_dbg_243_err;
}})()); 
				$pyjs['track']['lineno']=493;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_modelIdx', $constant_int_0) : $p['setattr'](self, '_modelIdx', $constant_int_0); 
				$pyjs['track']['lineno']=494;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isAjaxLoading', false) : $p['setattr'](self, '_isAjaxLoading', false); 
				$pyjs['track']['lineno']=495;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_dataProvider', null) : $p['setattr'](self, '_dataProvider', null); 
				$pyjs['track']['lineno']=496;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_cellRender', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_244_err){if (!$p['isinstance']($pyjs_dbg_244_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_244_err);}throw $pyjs_dbg_244_err;
}})()) : $p['setattr'](self, '_cellRender', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_244_err){if (!$p['isinstance']($pyjs_dbg_244_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_244_err);}throw $pyjs_dbg_244_err;
}})()); 
				$pyjs['track']['lineno']=499;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectionChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('selectionChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_245_err){if (!$p['isinstance']($pyjs_dbg_245_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_245_err);}throw $pyjs_dbg_245_err;
}})()) : $p['setattr'](self, 'selectionChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('selectionChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_245_err){if (!$p['isinstance']($pyjs_dbg_245_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_245_err);}throw $pyjs_dbg_245_err;
}})()); 
				$pyjs['track']['lineno']=500;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectionActivatedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('selectionActivated');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_246_err){if (!$p['isinstance']($pyjs_dbg_246_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_246_err);}throw $pyjs_dbg_246_err;
}})()) : $p['setattr'](self, 'selectionActivatedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('selectionActivated');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_246_err){if (!$p['isinstance']($pyjs_dbg_246_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_246_err);}throw $pyjs_dbg_246_err;
}})()); 
				$pyjs['track']['lineno']=501;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('tableChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('tableChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_247_err){if (!$p['isinstance']($pyjs_dbg_247_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_247_err);}throw $pyjs_dbg_247_err;
}})()) : $p['setattr'](self, 'tableChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('tableChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_247_err){if (!$p['isinstance']($pyjs_dbg_247_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_247_err);}throw $pyjs_dbg_247_err;
}})()); 
				$pyjs['track']['lineno']=503;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['table']['selectionChangedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_248_err){if (!$p['isinstance']($pyjs_dbg_248_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_248_err);}throw $pyjs_dbg_248_err;
}})();
				$pyjs['track']['lineno']=504;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['table']['selectionActivatedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_249_err){if (!$p['isinstance']($pyjs_dbg_249_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_249_err);}throw $pyjs_dbg_249_err;
}})();
				$pyjs['track']['lineno']=505;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['table']['tableChangedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_250_err){if (!$p['isinstance']($pyjs_dbg_250_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_250_err);}throw $pyjs_dbg_250_err;
}})();
				$pyjs['track']['lineno']=508;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter9_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list'](['cursorMovedEvent', 'setHeader']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_251_err){if (!$p['isinstance']($pyjs_dbg_251_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_251_err);}throw $pyjs_dbg_251_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_252_err){if (!$p['isinstance']($pyjs_dbg_252_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_252_err);}throw $pyjs_dbg_252_err;
}})();
				$iter9_nextval=$p['__iter_prepare']($iter9_iter,false);
				while (typeof($p['__wrapped_next']($iter9_nextval)['$nextval']) != 'undefined') {
					f = $iter9_nextval['$nextval'];
					$pyjs['track']['lineno']=509;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['setattr'](self, f, (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr']($p['getattr'](self, 'table'), f);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_253_err){if (!$p['isinstance']($pyjs_dbg_253_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_253_err);}throw $pyjs_dbg_253_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_254_err){if (!$p['isinstance']($pyjs_dbg_254_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_254_err);}throw $pyjs_dbg_254_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=511;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['cursorMovedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_255_err){if (!$p['isinstance']($pyjs_dbg_255_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_255_err);}throw $pyjs_dbg_255_err;
}})();
				$pyjs['track']['lineno']=512;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__getitem__']('style')['__setitem__']('overflow', 'scroll');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_256_err){if (!$p['isinstance']($pyjs_dbg_256_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_256_err);}throw $pyjs_dbg_256_err;
}})();
				$pyjs['track']['lineno']=513;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['recalcHeight']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_257_err){if (!$p['isinstance']($pyjs_dbg_257_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_257_err);}throw $pyjs_dbg_257_err;
}})();
				$pyjs['track']['lineno']=514;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['sinkEvent']('onScroll');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_258_err){if (!$p['isinstance']($pyjs_dbg_258_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_258_err);}throw $pyjs_dbg_258_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['_loadOnDisplay', false]]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=517;
			$method = $pyjs__bind_method2('recalcHeight', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var $sub9,$sub10;
				$pyjs['track']={'module':'widgets.table', 'lineno':517};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=517;
				$pyjs['track']['lineno']=518;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__getitem__']('style')['__setitem__']('max-height', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('%spx', $p['__op_sub']($sub9=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['int']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof eval == "undefined"?$m['eval']:eval)('window.top.innerHeight');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_259_err){if (!$p['isinstance']($pyjs_dbg_259_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_259_err);}throw $pyjs_dbg_259_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_260_err){if (!$p['isinstance']($pyjs_dbg_260_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_260_err);}throw $pyjs_dbg_260_err;
}})(),$sub10=$constant_int_280));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_261_err){if (!$p['isinstance']($pyjs_dbg_261_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_261_err);}throw $pyjs_dbg_261_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_262_err){if (!$p['isinstance']($pyjs_dbg_262_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_262_err);}throw $pyjs_dbg_262_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['recalcHeight'] = $method;
			$pyjs['track']['lineno']=520;
			$method = $pyjs__bind_method2('setDataProvider', function(obj) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					obj = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $or4,$or3;
				$pyjs['track']={'module':'widgets.table', 'lineno':520};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=520;
				$pyjs['track']['lineno']=529;
				if (!( ($p['bool']($or3=$p['op_eq'](obj, null))?$or3:(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dir'](obj);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_263_err){if (!$p['isinstance']($pyjs_dbg_263_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_263_err);}throw $pyjs_dbg_263_err;
}})()['__contains__']('onNextBatchNeeded')) )) {
				   throw $p['AssertionError']("The dataProvider must provide a 'onNextBatchNeeded' function");
				 }
				$pyjs['track']['lineno']=532;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_dataProvider', obj) : $p['setattr'](self, '_dataProvider', obj); 
				$pyjs['track']['lineno']=533;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isAjaxLoading', false) : $p['setattr'](self, '_isAjaxLoading', false); 
				$pyjs['track']['lineno']=535;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'table')['__getitem__']('class')['__contains__']('is_loading'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_264_err){if (!$p['isinstance']($pyjs_dbg_264_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_264_err);}throw $pyjs_dbg_264_err;
}})()) {
					$pyjs['track']['lineno']=536;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'table')['__getitem__']('class')['remove']('is_loading');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_265_err){if (!$p['isinstance']($pyjs_dbg_265_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_265_err);}throw $pyjs_dbg_265_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['obj']]);
			$cls_definition['setDataProvider'] = $method;
			$pyjs['track']['lineno']=538;
			$method = $pyjs__bind_method2('onCursorMoved', function(table, row) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					row = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $add21,$sub12,$add22,tr,$add24,$sub11,$add23;
				$pyjs['track']={'module':'widgets.table', 'lineno':538};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=538;
				$pyjs['track']['lineno']=542;
				tr = (function(){try{try{$pyjs['in_try_except'] += 1;
				return table['getTrByIndex'](row);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_266_err){if (!$p['isinstance']($pyjs_dbg_266_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_266_err);}throw $pyjs_dbg_266_err;
}})();
				$pyjs['track']['lineno']=543;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](tr, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_267_err){if (!$p['isinstance']($pyjs_dbg_267_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_267_err);}throw $pyjs_dbg_267_err;
}})()) {
					$pyjs['track']['lineno']=544;
					$pyjs['track']['lineno']=544;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=545;
				$pyjs['track']['lineno']=545;
				var $pyjs__ret = null;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
				$pyjs['track']['lineno']=546;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp']($p['getattr']($p['getattr'](self, 'element'), 'scrollTop'), $p['getattr'](tr, 'offsetTop')) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_268_err){if (!$p['isinstance']($pyjs_dbg_268_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_268_err);}throw $pyjs_dbg_268_err;
}})()) {
					$pyjs['track']['lineno']=547;
					$p['getattr'](self, 'element')['__is_instance__'] && typeof $p['getattr'](self, 'element')['__setattr__'] == 'function' ? $p['getattr'](self, 'element')['__setattr__']('scrollTop', $p['getattr'](tr, 'offsetTop')) : $p['setattr']($p['getattr'](self, 'element'), 'scrollTop', $p['getattr'](tr, 'offsetTop')); 
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp']($p['__op_add']($add21=$p['getattr']($p['getattr'](self, 'element'), 'scrollTop'),$add22=$p['getattr']($p['getattr'](self, 'element'), 'clientHeight')), $p['getattr'](tr, 'offsetTop')) == -1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_269_err){if (!$p['isinstance']($pyjs_dbg_269_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_269_err);}throw $pyjs_dbg_269_err;
}})()) {
					$pyjs['track']['lineno']=549;
					$p['getattr'](self, 'element')['__is_instance__'] && typeof $p['getattr'](self, 'element')['__setattr__'] == 'function' ? $p['getattr'](self, 'element')['__setattr__']('scrollTop', $p['__op_sub']($sub11=$p['__op_add']($add23=$p['getattr'](tr, 'offsetTop'),$add24=$p['getattr'](tr, 'clientHeight')),$sub12=$p['getattr']($p['getattr'](self, 'element'), 'clientHeight'))) : $p['setattr']($p['getattr'](self, 'element'), 'scrollTop', $p['__op_sub']($sub11=$p['__op_add']($add23=$p['getattr'](tr, 'offsetTop'),$add24=$p['getattr'](tr, 'clientHeight')),$sub12=$p['getattr']($p['getattr'](self, 'element'), 'clientHeight'))); 
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['row']]);
			$cls_definition['onCursorMoved'] = $method;
			$pyjs['track']['lineno']=551;
			$method = $pyjs__bind_method2('getRowCount', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.table', 'lineno':551};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=551;
				$pyjs['track']['lineno']=556;
				$pyjs['track']['lineno']=556;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len']($p['getattr'](self, '_model'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_270_err){if (!$p['isinstance']($pyjs_dbg_270_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_270_err);}throw $pyjs_dbg_270_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['getRowCount'] = $method;
			$pyjs['track']['lineno']=559;
			$method = $pyjs__bind_method2('add', function(obj) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					obj = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $add25,$add26;
				$pyjs['track']={'module':'widgets.table', 'lineno':559};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=559;
				$pyjs['track']['lineno']=565;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return obj['__setitem__']('_uniqeIndex', $p['getattr'](self, '_modelIdx'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_271_err){if (!$p['isinstance']($pyjs_dbg_271_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_271_err);}throw $pyjs_dbg_271_err;
}})();
				$pyjs['track']['lineno']=566;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_modelIdx', $p['__op_add']($add25=$p['getattr'](self, '_modelIdx'),$add26=$constant_int_1)) : $p['setattr'](self, '_modelIdx', $p['__op_add']($add25=$p['getattr'](self, '_modelIdx'),$add26=$constant_int_1)); 
				$pyjs['track']['lineno']=567;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_model']['append'](obj);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_272_err){if (!$p['isinstance']($pyjs_dbg_272_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_272_err);}throw $pyjs_dbg_272_err;
}})();
				$pyjs['track']['lineno']=568;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_renderObject'](obj);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_273_err){if (!$p['isinstance']($pyjs_dbg_273_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_273_err);}throw $pyjs_dbg_273_err;
}})();
				$pyjs['track']['lineno']=569;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isAjaxLoading', false) : $p['setattr'](self, '_isAjaxLoading', false); 
				$pyjs['track']['lineno']=570;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'table')['__getitem__']('class')['__contains__']('is_loading'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_274_err){if (!$p['isinstance']($pyjs_dbg_274_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_274_err);}throw $pyjs_dbg_274_err;
}})()) {
					$pyjs['track']['lineno']=571;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'table')['__getitem__']('class')['remove']('is_loading');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_275_err){if (!$p['isinstance']($pyjs_dbg_275_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_275_err);}throw $pyjs_dbg_275_err;
}})();
				}
				$pyjs['track']['lineno']=572;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['testIfNextBatchNeededImmediately']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_276_err){if (!$p['isinstance']($pyjs_dbg_276_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_276_err);}throw $pyjs_dbg_276_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['obj']]);
			$cls_definition['add'] = $method;
			$pyjs['track']['lineno']=574;
			$method = $pyjs__bind_method2('extend', function(objList) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					objList = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $add28,$iter10_nextval,obj,$iter10_array,$add27,$pyjs__trackstack_size_1,$iter10_type,$iter10_iter,$iter10_idx;
				$pyjs['track']={'module':'widgets.table', 'lineno':574};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=574;
				$pyjs['track']['lineno']=579;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['table']['prepareGrid']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](objList);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_277_err){if (!$p['isinstance']($pyjs_dbg_277_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_277_err);}throw $pyjs_dbg_277_err;
}})(), (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len']($p['getattr'](self, '_shownFields'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_278_err){if (!$p['isinstance']($pyjs_dbg_278_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_278_err);}throw $pyjs_dbg_278_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_279_err){if (!$p['isinstance']($pyjs_dbg_279_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_279_err);}throw $pyjs_dbg_279_err;
}})();
				$pyjs['track']['lineno']=580;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter10_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return objList;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_280_err){if (!$p['isinstance']($pyjs_dbg_280_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_280_err);}throw $pyjs_dbg_280_err;
}})();
				$iter10_nextval=$p['__iter_prepare']($iter10_iter,false);
				while (typeof($p['__wrapped_next']($iter10_nextval)['$nextval']) != 'undefined') {
					obj = $iter10_nextval['$nextval'];
					$pyjs['track']['lineno']=581;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return obj['__setitem__']('_uniqeIndex', $p['getattr'](self, '_modelIdx'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_281_err){if (!$p['isinstance']($pyjs_dbg_281_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_281_err);}throw $pyjs_dbg_281_err;
}})();
					$pyjs['track']['lineno']=582;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_modelIdx', $p['__op_add']($add27=$p['getattr'](self, '_modelIdx'),$add28=$constant_int_1)) : $p['setattr'](self, '_modelIdx', $p['__op_add']($add27=$p['getattr'](self, '_modelIdx'),$add28=$constant_int_1)); 
					$pyjs['track']['lineno']=583;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['_model']['append'](obj);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_282_err){if (!$p['isinstance']($pyjs_dbg_282_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_282_err);}throw $pyjs_dbg_282_err;
}})();
					$pyjs['track']['lineno']=584;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call(self, '_renderObject', null, null, [{'tableIsPrepared':true}, obj]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_283_err){if (!$p['isinstance']($pyjs_dbg_283_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_283_err);}throw $pyjs_dbg_283_err;
}})();
					$pyjs['track']['lineno']=585;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isAjaxLoading', false) : $p['setattr'](self, '_isAjaxLoading', false); 
					$pyjs['track']['lineno']=586;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'table')['__getitem__']('class')['__contains__']('is_loading'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_284_err){if (!$p['isinstance']($pyjs_dbg_284_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_284_err);}throw $pyjs_dbg_284_err;
}})()) {
						$pyjs['track']['lineno']=587;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'table')['__getitem__']('class')['remove']('is_loading');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_285_err){if (!$p['isinstance']($pyjs_dbg_285_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_285_err);}throw $pyjs_dbg_285_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=588;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['testIfNextBatchNeededImmediately']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_286_err){if (!$p['isinstance']($pyjs_dbg_286_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_286_err);}throw $pyjs_dbg_286_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['objList']]);
			$cls_definition['extend'] = $method;
			$pyjs['track']['lineno']=590;
			$method = $pyjs__bind_method2('testIfNextBatchNeededImmediately', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $add29,$and8,c,$or6,$or5,$iter11_iter,$iter11_type,$and7,sumHeight,$iter11_array,$iter11_nextval,$iter11_idx,$pyjs__trackstack_size_1,$add30;
				$pyjs['track']={'module':'widgets.table', 'lineno':590};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=590;
				$pyjs['track']['lineno']=596;
				sumHeight = $constant_int_0;
				$pyjs['track']['lineno']=597;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter11_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr']($p['getattr'](self, 'table'), '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_287_err){if (!$p['isinstance']($pyjs_dbg_287_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_287_err);}throw $pyjs_dbg_287_err;
}})();
				$iter11_nextval=$p['__iter_prepare']($iter11_iter,false);
				while (typeof($p['__wrapped_next']($iter11_nextval)['$nextval']) != 'undefined') {
					c = $iter11_nextval['$nextval'];
					$pyjs['track']['lineno']=598;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dir']($p['getattr'](c, 'element'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_288_err){if (!$p['isinstance']($pyjs_dbg_288_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_288_err);}throw $pyjs_dbg_288_err;
}})()['__contains__']('clientHeight'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_289_err){if (!$p['isinstance']($pyjs_dbg_289_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_289_err);}throw $pyjs_dbg_289_err;
}})()) {
						$pyjs['track']['lineno']=599;
						sumHeight = $p['__op_add']($add29=sumHeight,$add30=$p['getattr']($p['getattr'](c, 'element'), 'clientHeight'));
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=601;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and7=!$p['bool']($p['getattr'](self, '_isAjaxLoading')))?($p['bool']($or5=$p['getattr'](self, '_loadOnDisplay'))?$or5:!$p['bool'](($p['cmp'](sumHeight, (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['int']($p['__getslice'](self['__getitem__']('style')['__getitem__']('max-height'), 0, (typeof ($usub3=$constant_int_2)=='number'?
					-$usub3:
					$p['op_usub']($usub3))));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_290_err){if (!$p['isinstance']($pyjs_dbg_290_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_290_err);}throw $pyjs_dbg_290_err;
}})()) == 1))):$and7));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_291_err){if (!$p['isinstance']($pyjs_dbg_291_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_291_err);}throw $pyjs_dbg_291_err;
}})()) {
					$pyjs['track']['lineno']=605;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, '_dataProvider'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_292_err){if (!$p['isinstance']($pyjs_dbg_292_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_292_err);}throw $pyjs_dbg_292_err;
}})()) {
						$pyjs['track']['lineno']=606;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isAjaxLoading', true) : $p['setattr'](self, '_isAjaxLoading', true); 
						$pyjs['track']['lineno']=607;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['bool']($p['getattr'](self, 'table')['__getitem__']('class')['__contains__']('is_loading')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_293_err){if (!$p['isinstance']($pyjs_dbg_293_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_293_err);}throw $pyjs_dbg_293_err;
}})()) {
							$pyjs['track']['lineno']=608;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['getattr'](self, 'table')['__getitem__']('class')['append']('is_loading');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_294_err){if (!$p['isinstance']($pyjs_dbg_294_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_294_err);}throw $pyjs_dbg_294_err;
}})();
						}
						$pyjs['track']['lineno']=609;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['_dataProvider']['onNextBatchNeeded']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_295_err){if (!$p['isinstance']($pyjs_dbg_295_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_295_err);}throw $pyjs_dbg_295_err;
}})();
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['testIfNextBatchNeededImmediately'] = $method;
			$pyjs['track']['lineno']=611;
			$method = $pyjs__bind_method2('remove', function(objOrIndex) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					objOrIndex = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and10,$and9;
				$pyjs['track']={'module':'widgets.table', 'lineno':611};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=611;
				$pyjs['track']['lineno']=617;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](objOrIndex, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_296_err){if (!$p['isinstance']($pyjs_dbg_296_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_296_err);}throw $pyjs_dbg_296_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_297_err){if (!$p['isinstance']($pyjs_dbg_297_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_297_err);}throw $pyjs_dbg_297_err;
}})()) {
					$pyjs['track']['lineno']=618;
					if (!( $p['getattr'](self, '_model')['__contains__'](objOrIndex) )) {
					   throw $p['AssertionError']('Cannot remove unknown object from Table');
					 }
					$pyjs['track']['lineno']=619;
					objOrIndex = (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['_model']['index'](objOrIndex);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_298_err){if (!$p['isinstance']($pyjs_dbg_298_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_298_err);}throw $pyjs_dbg_298_err;
}})();
				}
				$pyjs['track']['lineno']=620;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](objOrIndex, $p['int']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_299_err){if (!$p['isinstance']($pyjs_dbg_299_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_299_err);}throw $pyjs_dbg_299_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_300_err){if (!$p['isinstance']($pyjs_dbg_300_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_300_err);}throw $pyjs_dbg_300_err;
}})()) {
					$pyjs['track']['lineno']=621;
					if (!( ($p['bool']($and9=($p['cmp'](objOrIndex, $constant_int_0) == 1))?($p['cmp'](objOrIndex, (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['len']($p['getattr'](self, '_model'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_301_err){if (!$p['isinstance']($pyjs_dbg_301_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_301_err);}throw $pyjs_dbg_301_err;
}})()) == -1):$and9) )) {
					   throw $p['AssertionError']('Modelindex out of range');
					 }
					$pyjs['track']['lineno']=622;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['_model']['remove']($p['getattr'](self, '_model')['__getitem__'](objOrIndex));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_302_err){if (!$p['isinstance']($pyjs_dbg_302_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_302_err);}throw $pyjs_dbg_302_err;
}})();
					$pyjs['track']['lineno']=623;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['table']['removeRow'](objOrIndex);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_303_err){if (!$p['isinstance']($pyjs_dbg_303_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_303_err);}throw $pyjs_dbg_303_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=625;
					$pyjs['__active_exception_stack__'] = null;
					throw ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['TypeError']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('Expected int or dict, got %s', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['str']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['type'](objOrIndex);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_304_err){if (!$p['isinstance']($pyjs_dbg_304_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_304_err);}throw $pyjs_dbg_304_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_305_err){if (!$p['isinstance']($pyjs_dbg_305_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_305_err);}throw $pyjs_dbg_305_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_306_err){if (!$p['isinstance']($pyjs_dbg_306_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_306_err);}throw $pyjs_dbg_306_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_307_err){if (!$p['isinstance']($pyjs_dbg_307_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_307_err);}throw $pyjs_dbg_307_err;
}})());
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['objOrIndex']]);
			$cls_definition['remove'] = $method;
			$pyjs['track']['lineno']=627;
			$method = $pyjs__bind_method2('clear', function(keepModel) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					keepModel = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof keepModel == 'undefined') keepModel=arguments['callee']['__args__'][3][1];

				$pyjs['track']={'module':'widgets.table', 'lineno':627};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=627;
				$pyjs['track']['lineno']=631;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['table']['clear']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_308_err){if (!$p['isinstance']($pyjs_dbg_308_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_308_err);}throw $pyjs_dbg_308_err;
}})();
				$pyjs['track']['lineno']=632;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool'](keepModel));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_309_err){if (!$p['isinstance']($pyjs_dbg_309_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_309_err);}throw $pyjs_dbg_309_err;
}})()) {
					$pyjs['track']['lineno']=633;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_model', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_310_err){if (!$p['isinstance']($pyjs_dbg_310_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_310_err);}throw $pyjs_dbg_310_err;
}})()) : $p['setattr'](self, '_model', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_310_err){if (!$p['isinstance']($pyjs_dbg_310_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_310_err);}throw $pyjs_dbg_310_err;
}})()); 
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['keepModel', false]]);
			$cls_definition['clear'] = $method;
			$pyjs['track']['lineno']=635;
			$method = $pyjs__bind_method2('_renderObject', function(obj, tableIsPrepared) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					obj = arguments[1];
					tableIsPrepared = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 2 || arguments['length'] > 3)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof tableIsPrepared == 'undefined') tableIsPrepared=arguments['callee']['__args__'][4][1];
				var $iter12_type,lbl,rowIdx,$sub13,$iter12_idx,$add31,$iter12_array,cellIdx,$sub14,$iter12_iter,field,$add32,$pyjs__trackstack_size_1,$iter12_nextval;
				$pyjs['track']={'module':'widgets.table', 'lineno':635};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=635;
				$pyjs['track']['lineno']=642;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, '_shownFields')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_311_err){if (!$p['isinstance']($pyjs_dbg_311_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_311_err);}throw $pyjs_dbg_311_err;
}})()) {
					$pyjs['track']['lineno']=643;
					$pyjs['track']['lineno']=643;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=645;
				rowIdx = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_model']['index'](obj);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_312_err){if (!$p['isinstance']($pyjs_dbg_312_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_312_err);}throw $pyjs_dbg_312_err;
}})();
				$pyjs['track']['lineno']=646;
				cellIdx = $constant_int_0;
				$pyjs['track']['lineno']=648;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool'](tableIsPrepared));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_313_err){if (!$p['isinstance']($pyjs_dbg_313_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_313_err);}throw $pyjs_dbg_313_err;
}})()) {
					$pyjs['track']['lineno']=649;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['table']['prepareCol'](rowIdx, $p['__op_sub']($sub13=(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['len']($p['getattr'](self, '_shownFields'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_314_err){if (!$p['isinstance']($pyjs_dbg_314_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_314_err);}throw $pyjs_dbg_314_err;
}})(),$sub14=$constant_int_1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_315_err){if (!$p['isinstance']($pyjs_dbg_315_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_315_err);}throw $pyjs_dbg_315_err;
}})();
				}
				$pyjs['track']['lineno']=651;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter12_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, '_shownFields');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_316_err){if (!$p['isinstance']($pyjs_dbg_316_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_316_err);}throw $pyjs_dbg_316_err;
}})();
				$iter12_nextval=$p['__iter_prepare']($iter12_iter,false);
				while (typeof($p['__wrapped_next']($iter12_nextval)['$nextval']) != 'undefined') {
					field = $iter12_nextval['$nextval'];
					$pyjs['track']['lineno']=652;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return self['_cellRender']['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_317_err){if (!$p['isinstance']($pyjs_dbg_317_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_317_err);}throw $pyjs_dbg_317_err;
}})()['__contains__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_318_err){if (!$p['isinstance']($pyjs_dbg_318_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_318_err);}throw $pyjs_dbg_318_err;
}})()) {
						$pyjs['track']['lineno']=653;
						lbl = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, '_cellRender')['__getitem__'](field)['render'](obj, field);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_319_err){if (!$p['isinstance']($pyjs_dbg_319_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_319_err);}throw $pyjs_dbg_319_err;
}})();
					}
					else if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return obj['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_320_err){if (!$p['isinstance']($pyjs_dbg_320_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_320_err);}throw $pyjs_dbg_320_err;
}})()['__contains__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_321_err){if (!$p['isinstance']($pyjs_dbg_321_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_321_err);}throw $pyjs_dbg_321_err;
}})()) {
						$pyjs['track']['lineno']=655;
						lbl = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Label'](obj['__getitem__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_322_err){if (!$p['isinstance']($pyjs_dbg_322_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_322_err);}throw $pyjs_dbg_322_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=657;
						lbl = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Label']('...');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_323_err){if (!$p['isinstance']($pyjs_dbg_323_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_323_err);}throw $pyjs_dbg_323_err;
}})();
					}
					$pyjs['track']['lineno']=659;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['table']['setCell'](rowIdx, cellIdx, lbl);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_324_err){if (!$p['isinstance']($pyjs_dbg_324_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_324_err);}throw $pyjs_dbg_324_err;
}})();
					$pyjs['track']['lineno']=660;
					cellIdx = $p['__op_add']($add31=cellIdx,$add32=$constant_int_1);
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['obj'],['tableIsPrepared', false]]);
			$cls_definition['_renderObject'] = $method;
			$pyjs['track']['lineno']=662;
			$method = $pyjs__bind_method2('rebuildTable', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter13_nextval,$iter13_iter,$iter13_type,$iter13_idx,$pyjs__trackstack_size_1,obj,$iter13_array;
				$pyjs['track']={'module':'widgets.table', 'lineno':662};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=662;
				$pyjs['track']['lineno']=667;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(self, 'clear', null, null, [{'keepModel':true}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_325_err){if (!$p['isinstance']($pyjs_dbg_325_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_325_err);}throw $pyjs_dbg_325_err;
}})();
				$pyjs['track']['lineno']=668;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['table']['prepareGrid']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len']($p['getattr'](self, '_model'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_326_err){if (!$p['isinstance']($pyjs_dbg_326_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_326_err);}throw $pyjs_dbg_326_err;
}})(), (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len']($p['getattr'](self, '_shownFields'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_327_err){if (!$p['isinstance']($pyjs_dbg_327_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_327_err);}throw $pyjs_dbg_327_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_328_err){if (!$p['isinstance']($pyjs_dbg_328_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_328_err);}throw $pyjs_dbg_328_err;
}})();
				$pyjs['track']['lineno']=669;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter13_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, '_model');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_329_err){if (!$p['isinstance']($pyjs_dbg_329_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_329_err);}throw $pyjs_dbg_329_err;
}})();
				$iter13_nextval=$p['__iter_prepare']($iter13_iter,false);
				while (typeof($p['__wrapped_next']($iter13_nextval)['$nextval']) != 'undefined') {
					obj = $iter13_nextval['$nextval'];
					$pyjs['track']['lineno']=670;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call(self, '_renderObject', null, null, [{'tableIsPrepared':true}, obj]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_330_err){if (!$p['isinstance']($pyjs_dbg_330_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_330_err);}throw $pyjs_dbg_330_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['rebuildTable'] = $method;
			$pyjs['track']['lineno']=672;
			$method = $pyjs__bind_method2('setShownFields', function(fields) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					fields = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.table', 'lineno':672};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=672;
				$pyjs['track']['lineno']=680;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_shownFields', fields) : $p['setattr'](self, '_shownFields', fields); 
				$pyjs['track']['lineno']=681;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['rebuildTable']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_331_err){if (!$p['isinstance']($pyjs_dbg_331_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_331_err);}throw $pyjs_dbg_331_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['fields']]);
			$cls_definition['setShownFields'] = $method;
			$pyjs['track']['lineno']=683;
			$method = $pyjs__bind_method2('onScroll', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $add33,$add34,$and12,$and11;
				$pyjs['track']={'module':'widgets.table', 'lineno':683};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=683;
				$pyjs['track']['lineno']=687;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, '_loadOnDisplay'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_332_err){if (!$p['isinstance']($pyjs_dbg_332_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_332_err);}throw $pyjs_dbg_332_err;
}})()) {
					$pyjs['track']['lineno']=688;
					$pyjs['track']['lineno']=688;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=690;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['recalcHeight']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_333_err){if (!$p['isinstance']($pyjs_dbg_333_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_333_err);}throw $pyjs_dbg_333_err;
}})();
				$pyjs['track']['lineno']=692;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and11=((($p['cmp']($p['__op_add']($add33=$p['getattr']($p['getattr'](self, 'element'), 'scrollTop'),$add34=$p['getattr']($p['getattr'](self, 'element'), 'clientHeight')), $p['getattr']($p['getattr'](self, 'element'), 'scrollHeight')))|1) == 1))?!$p['bool']($p['getattr'](self, '_isAjaxLoading')):$and11));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_334_err){if (!$p['isinstance']($pyjs_dbg_334_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_334_err);}throw $pyjs_dbg_334_err;
}})()) {
					$pyjs['track']['lineno']=696;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, '_dataProvider'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_335_err){if (!$p['isinstance']($pyjs_dbg_335_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_335_err);}throw $pyjs_dbg_335_err;
}})()) {
						$pyjs['track']['lineno']=698;
						self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_isAjaxLoading', true) : $p['setattr'](self, '_isAjaxLoading', true); 
						$pyjs['track']['lineno']=699;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['bool']($p['getattr'](self, 'table')['__getitem__']('class')['__contains__']('is_loading')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_336_err){if (!$p['isinstance']($pyjs_dbg_336_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_336_err);}throw $pyjs_dbg_336_err;
}})()) {
							$pyjs['track']['lineno']=700;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['getattr'](self, 'table')['__getitem__']('class')['append']('is_loading');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_337_err){if (!$p['isinstance']($pyjs_dbg_337_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_337_err);}throw $pyjs_dbg_337_err;
}})();
						}
						$pyjs['track']['lineno']=702;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['_dataProvider']['onNextBatchNeeded']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_338_err){if (!$p['isinstance']($pyjs_dbg_338_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_338_err);}throw $pyjs_dbg_338_err;
}})();
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onScroll'] = $method;
			$pyjs['track']['lineno']=704;
			$method = $pyjs__bind_method2('onSelectionChanged', function(table, rows) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					rows = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var vals;
				$pyjs['track']={'module':'widgets.table', 'lineno':704};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=704;
				$pyjs['track']['lineno']=708;
				vals = function(){
					var $iter14_array,$iter14_type,$or7,$collcomp1,$or8,$iter14_iter,$pyjs__trackstack_size_1,x,$iter14_idx,$iter14_nextval;
	$collcomp1 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter14_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return ($p['bool']($or7=rows)?$or7:(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_339_err){if (!$p['isinstance']($pyjs_dbg_339_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_339_err);}throw $pyjs_dbg_339_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_340_err){if (!$p['isinstance']($pyjs_dbg_340_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_340_err);}throw $pyjs_dbg_340_err;
}})();
				$iter14_nextval=$p['__iter_prepare']($iter14_iter,false);
				while (typeof($p['__wrapped_next']($iter14_nextval)['$nextval']) != 'undefined') {
					x = $iter14_nextval['$nextval'];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp1['append']($p['getattr'](self, '_model')['__getitem__'](x));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_341_err){if (!$p['isinstance']($pyjs_dbg_341_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_341_err);}throw $pyjs_dbg_341_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';

	return $collcomp1;}();
				$pyjs['track']['lineno']=709;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionChangedEvent']['fire'](self, vals);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_342_err){if (!$p['isinstance']($pyjs_dbg_342_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_342_err);}throw $pyjs_dbg_342_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['rows']]);
			$cls_definition['onSelectionChanged'] = $method;
			$pyjs['track']['lineno']=711;
			$method = $pyjs__bind_method2('onSelectionActivated', function(table, rows) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					rows = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var vals;
				$pyjs['track']={'module':'widgets.table', 'lineno':711};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=711;
				$pyjs['track']['lineno']=715;
				vals = function(){
					var $iter15_iter,$iter15_array,$collcomp2,$iter15_idx,$iter15_nextval,$iter15_type,x,$pyjs__trackstack_size_1;
	$collcomp2 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter15_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return rows;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_343_err){if (!$p['isinstance']($pyjs_dbg_343_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_343_err);}throw $pyjs_dbg_343_err;
}})();
				$iter15_nextval=$p['__iter_prepare']($iter15_iter,false);
				while (typeof($p['__wrapped_next']($iter15_nextval)['$nextval']) != 'undefined') {
					x = $iter15_nextval['$nextval'];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp2['append']($p['getattr'](self, '_model')['__getitem__'](x));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_344_err){if (!$p['isinstance']($pyjs_dbg_344_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_344_err);}throw $pyjs_dbg_344_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';

	return $collcomp2;}();
				$pyjs['track']['lineno']=716;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionActivatedEvent']['fire'](self, vals);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_345_err){if (!$p['isinstance']($pyjs_dbg_345_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_345_err);}throw $pyjs_dbg_345_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['rows']]);
			$cls_definition['onSelectionActivated'] = $method;
			$pyjs['track']['lineno']=718;
			$method = $pyjs__bind_method2('onTableChanged', function(table, rowCount) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					rowCount = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.table', 'lineno':718};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=718;
				$pyjs['track']['lineno']=722;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['tableChangedEvent']['fire'](self, rowCount);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_346_err){if (!$p['isinstance']($pyjs_dbg_346_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_346_err);}throw $pyjs_dbg_346_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['rowCount']]);
			$cls_definition['onTableChanged'] = $method;
			$pyjs['track']['lineno']=724;
			$method = $pyjs__bind_method2('getCurrentSelection', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var rows,$or9,$or10;
				$pyjs['track']={'module':'widgets.table', 'lineno':724};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=724;
				$pyjs['track']['lineno']=729;
				rows = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['table']['getCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_347_err){if (!$p['isinstance']($pyjs_dbg_347_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_347_err);}throw $pyjs_dbg_347_err;
}})();
				$pyjs['track']['lineno']=730;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($or9=!$p['bool']($p['getattr'](self, '_model')))?$or9:!$p['bool'](rows)));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_348_err){if (!$p['isinstance']($pyjs_dbg_348_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_348_err);}throw $pyjs_dbg_348_err;
}})()) {
					$pyjs['track']['lineno']=731;
					$pyjs['track']['lineno']=731;
					var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_349_err){if (!$p['isinstance']($pyjs_dbg_349_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_349_err);}throw $pyjs_dbg_349_err;
}})();
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=732;
				$pyjs['track']['lineno']=732;
				var $pyjs__ret = function(){
					var $iter16_array,$iter16_type,$pyjs__trackstack_size_1,$collcomp3,$iter16_idx,x,$iter16_nextval,$iter16_iter;
	$collcomp3 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter16_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return rows;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_350_err){if (!$p['isinstance']($pyjs_dbg_350_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_350_err);}throw $pyjs_dbg_350_err;
}})();
				$iter16_nextval=$p['__iter_prepare']($iter16_iter,false);
				while (typeof($p['__wrapped_next']($iter16_nextval)['$nextval']) != 'undefined') {
					x = $iter16_nextval['$nextval'];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp3['append']($p['getattr'](self, '_model')['__getitem__'](x));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_351_err){if (!$p['isinstance']($pyjs_dbg_351_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_351_err);}throw $pyjs_dbg_351_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';

	return $collcomp3;}();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['getCurrentSelection'] = $method;
			$pyjs['track']['lineno']=734;
			$method = $pyjs__bind_method2('setCellRender', function(field, render) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					field = arguments[1];
					render = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'widgets.table', 'lineno':734};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=734;
				$pyjs['track']['lineno']=740;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](render, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_352_err){if (!$p['isinstance']($pyjs_dbg_352_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_352_err);}throw $pyjs_dbg_352_err;
}})()) {
					$pyjs['track']['lineno']=741;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return self['_cellRender']['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_353_err){if (!$p['isinstance']($pyjs_dbg_353_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_353_err);}throw $pyjs_dbg_353_err;
}})()['__contains__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_354_err){if (!$p['isinstance']($pyjs_dbg_354_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_354_err);}throw $pyjs_dbg_354_err;
}})()) {
						$pyjs['track']['lineno']=742;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, '_cellRender')['__delitem__'](field);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_355_err){if (!$p['isinstance']($pyjs_dbg_355_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_355_err);}throw $pyjs_dbg_355_err;
}})();
					}
				}
				else {
					$pyjs['track']['lineno']=744;
					if (!( (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dir'](render);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_356_err){if (!$p['isinstance']($pyjs_dbg_356_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_356_err);}throw $pyjs_dbg_356_err;
}})()['__contains__']('render') )) {
					   throw $p['AssertionError']("The render must provide a 'render' method");
					 }
					$pyjs['track']['lineno']=745;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, '_cellRender')['__setitem__'](field, render);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_357_err){if (!$p['isinstance']($pyjs_dbg_357_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_357_err);}throw $pyjs_dbg_357_err;
}})();
				}
				$pyjs['track']['lineno']=746;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['rebuildTable']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_358_err){if (!$p['isinstance']($pyjs_dbg_358_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_358_err);}throw $pyjs_dbg_358_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['field'],['render']]);
			$cls_definition['setCellRender'] = $method;
			$pyjs['track']['lineno']=748;
			$method = $pyjs__bind_method2('setCellRenders', function(renders) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					renders = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter17_nextval,$iter17_iter,render,field,$pyjs__trackstack_size_1,$iter17_array,$iter17_idx,$iter17_type;
				$pyjs['track']={'module':'widgets.table', 'lineno':748};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=748;
				$pyjs['track']['lineno']=753;
				if (!( (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](renders, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_359_err){if (!$p['isinstance']($pyjs_dbg_359_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_359_err);}throw $pyjs_dbg_359_err;
}})() )) {
				   throw $p['AssertionError']();
				 }
				$pyjs['track']['lineno']=754;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter17_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return renders['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_361_err){if (!$p['isinstance']($pyjs_dbg_361_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_361_err);}throw $pyjs_dbg_361_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_362_err){if (!$p['isinstance']($pyjs_dbg_362_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_362_err);}throw $pyjs_dbg_362_err;
}})();
				$iter17_nextval=$p['__iter_prepare']($iter17_iter,false);
				while (typeof($p['__wrapped_next']($iter17_nextval)['$nextval']) != 'undefined') {
					var $tupleassign1 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter17_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_360_err){if (!$p['isinstance']($pyjs_dbg_360_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_360_err);}throw $pyjs_dbg_360_err;
}})();
					field = $tupleassign1[0];
					render = $tupleassign1[1];
					$pyjs['track']['lineno']=755;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_is'](render, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_363_err){if (!$p['isinstance']($pyjs_dbg_363_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_363_err);}throw $pyjs_dbg_363_err;
}})()) {
						$pyjs['track']['lineno']=756;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return self['_cellRender']['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_364_err){if (!$p['isinstance']($pyjs_dbg_364_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_364_err);}throw $pyjs_dbg_364_err;
}})()['__contains__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_365_err){if (!$p['isinstance']($pyjs_dbg_365_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_365_err);}throw $pyjs_dbg_365_err;
}})()) {
							$pyjs['track']['lineno']=757;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['getattr'](self, '_cellRender')['__delitem__'](field);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_366_err){if (!$p['isinstance']($pyjs_dbg_366_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_366_err);}throw $pyjs_dbg_366_err;
}})();
						}
					}
					else {
						$pyjs['track']['lineno']=759;
						if (!( (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['dir'](render);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_367_err){if (!$p['isinstance']($pyjs_dbg_367_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_367_err);}throw $pyjs_dbg_367_err;
}})()['__contains__']('render') )) {
						   throw $p['AssertionError']("The render must provide a 'render' method");
						 }
						$pyjs['track']['lineno']=760;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, '_cellRender')['__setitem__'](field, render);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_368_err){if (!$p['isinstance']($pyjs_dbg_368_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_368_err);}throw $pyjs_dbg_368_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=761;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['rebuildTable']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_369_err){if (!$p['isinstance']($pyjs_dbg_369_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_369_err);}throw $pyjs_dbg_369_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['renders']]);
			$cls_definition['setCellRenders'] = $method;
			$pyjs['track']['lineno']=763;
			$method = $pyjs__bind_method2('activateCurrentSelection', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '50dd62acd197ce3eed7df6c8fd49babb') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var selection;
				$pyjs['track']={'module':'widgets.table', 'lineno':763};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='widgets.table';
				$pyjs['track']['lineno']=763;
				$pyjs['track']['lineno']=768;
				selection = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getCurrentSelection']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_370_err){if (!$p['isinstance']($pyjs_dbg_370_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_370_err);}throw $pyjs_dbg_370_err;
}})();
				$pyjs['track']['lineno']=769;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len'](selection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_371_err){if (!$p['isinstance']($pyjs_dbg_371_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_371_err);}throw $pyjs_dbg_371_err;
}})(), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_372_err){if (!$p['isinstance']($pyjs_dbg_372_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_372_err);}throw $pyjs_dbg_372_err;
}})()) {
					$pyjs['track']['lineno']=770;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['selectionActivatedEvent']['fire'](self, selection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_373_err){if (!$p['isinstance']($pyjs_dbg_373_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_373_err);}throw $pyjs_dbg_373_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['activateCurrentSelection'] = $method;
			$pyjs['track']['lineno']=479;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('DataTable', $p['tuple']($bases), $data);
		})();
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end widgets.table */


/* end module: widgets.table */


/*
PYJS_DEPS: ['html5', 'utils', 'event.EventDispatcher', 'event', 'html5.keycodes']
*/
