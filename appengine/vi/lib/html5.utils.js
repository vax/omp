/* start module: html5.utils */
$pyjs['loaded_modules']['html5.utils'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['html5.utils']['__was_initialized__']) return $pyjs['loaded_modules']['html5.utils'];
	if(typeof $pyjs['loaded_modules']['html5'] == 'undefined' || !$pyjs['loaded_modules']['html5']['__was_initialized__']) $p['___import___']('html5', null);
	var $m = $pyjs['loaded_modules']['html5.utils'];
	$m['__repr__'] = function() { return '<module: html5.utils>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'html5.utils';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	$pyjs['loaded_modules']['html5']['utils'] = $pyjs['loaded_modules']['html5.utils'];
	try {
		$m.__track_lines__[1] = 'html5.utils.py, line 1:\n    # -*- coding: utf-8 -*-';
		$m.__track_lines__[3] = 'html5.utils.py, line 3:\n    def unescape(val, maxLength = 0):';
		$m.__track_lines__[17] = 'html5.utils.py, line 17:\n    try:';
		$m.__track_lines__[18] = 'html5.utils.py, line 18:\n    val = val \\';
		$m.__track_lines__[24] = 'html5.utils.py, line 24:\n    pass';
		$m.__track_lines__[26] = 'html5.utils.py, line 26:\n    if maxLength > 0:';
		$m.__track_lines__[27] = 'html5.utils.py, line 27:\n    return val[0:maxLength]';
		$m.__track_lines__[29] = 'html5.utils.py, line 29:\n    return val';

		var $constant_int_0 = new $p['int'](0);
		$pyjs['track']['module']='html5.utils';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=3;
		$m['unescape'] = function(val, maxLength) {
			if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
			if (typeof maxLength == 'undefined') maxLength=arguments['callee']['__args__'][3][1];
			var $pyjs_try_err;
			$pyjs['track']={'module':'html5.utils','lineno':3};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='html5.utils';
			$pyjs['track']['lineno']=3;
			$pyjs['track']['lineno']=17;
			var $pyjs__trackstack_size_1 = $pyjs['trackstack']['length'];
			try {
				try {
					$pyjs['in_try_except'] += 1;
					$pyjs['track']['lineno']=18;
					val = (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return val['$$replace']('&lt;', '<');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})()['$$replace']('&gt;', '>');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})()['$$replace']('&quot;', '"');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})()['$$replace']('&#39;', "'");
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})();
				} finally { $pyjs['in_try_except'] -= 1; }
			} catch($pyjs_try_err) {
				$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_1 - 1);
				$pyjs['__active_exception_stack__'] = null;
				$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
				var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
				$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='html5.utils';
				if (($pyjs_try_err_name == $p['AttributeError']['__name__'])||$p['_isinstance']($pyjs_try_err,$p['AttributeError'])) {
					$pyjs['track']['lineno']=24;
				} else { $pyjs['__active_exception_stack__'] = $pyjs['__last_exception_stack__']; $pyjs['__last_exception_stack__'] = null; throw $pyjs_try_err; }
			}
			$pyjs['track']['lineno']=26;
			if ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['bool'](($p['cmp'](maxLength, $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})()) {
				$pyjs['track']['lineno']=27;
				$pyjs['track']['lineno']=27;
				var $pyjs__ret = $p['__getslice'](val, $constant_int_0, maxLength);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
			$pyjs['track']['lineno']=29;
			$pyjs['track']['lineno']=29;
			var $pyjs__ret = val;
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['unescape']['__name__'] = 'unescape';

		$m['unescape']['__bind_type__'] = 0;
		$m['unescape']['__args__'] = [null,null,['val'],['maxLength', $constant_int_0]];
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end html5.utils */


/* end module: html5.utils */


