/* start module: bones.extendenrelational */
$pyjs['loaded_modules']['bones.extendenrelational'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['bones.extendenrelational']['__was_initialized__']) return $pyjs['loaded_modules']['bones.extendenrelational'];
	if(typeof $pyjs['loaded_modules']['bones'] == 'undefined' || !$pyjs['loaded_modules']['bones']['__was_initialized__']) $p['___import___']('bones', null);
	var $m = $pyjs['loaded_modules']['bones.extendenrelational'];
	$m['__repr__'] = function() { return '<module: bones.extendenrelational>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'bones.extendenrelational';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	$pyjs['loaded_modules']['bones']['extendenrelational'] = $pyjs['loaded_modules']['bones.extendenrelational'];
	try {
		$m.__track_lines__[1] = 'bones.extendenrelational.py, line 1:\n    # -*- coding: utf-8 -*-';
		$m.__track_lines__[3] = 'bones.extendenrelational.py, line 3:\n    import html5';
		$m.__track_lines__[4] = 'bones.extendenrelational.py, line 4:\n    from priorityqueue import editBoneSelector, viewDelegateSelector, extendedSearchWidgetSelector, extractorDelegateSelector';
		$m.__track_lines__[5] = 'bones.extendenrelational.py, line 5:\n    from event import EventDispatcher';
		$m.__track_lines__[6] = 'bones.extendenrelational.py, line 6:\n    from utils import formatString';
		$m.__track_lines__[7] = 'bones.extendenrelational.py, line 7:\n    from widgets.list import ListWidget';
		$m.__track_lines__[8] = 'bones.extendenrelational.py, line 8:\n    from widgets.edit import InternalEdit';
		$m.__track_lines__[9] = 'bones.extendenrelational.py, line 9:\n    from config import conf';
		$m.__track_lines__[10] = 'bones.extendenrelational.py, line 10:\n    from i18n import translate';
		$m.__track_lines__[13] = 'bones.extendenrelational.py, line 13:\n    class ExtendedRelationalBoneExtractor( object ):';
		$m.__track_lines__[14] = 'bones.extendenrelational.py, line 14:\n    cantSort = True';
		$m.__track_lines__[15] = 'bones.extendenrelational.py, line 15:\n    def __init__(self, modul, boneName, structure):';
		$m.__track_lines__[16] = 'bones.extendenrelational.py, line 16:\n    super(ExtendedRelationalBoneExtractor, self).__init__()';
		$m.__track_lines__[17] = 'bones.extendenrelational.py, line 17:\n    self.format = "$(dest.name)"';
		$m.__track_lines__[18] = 'bones.extendenrelational.py, line 18:\n    if "format" in structure[boneName].keys():';
		$m.__track_lines__[19] = 'bones.extendenrelational.py, line 19:\n    self.format = structure[boneName]["format"]';
		$m.__track_lines__[20] = 'bones.extendenrelational.py, line 20:\n    self.modul = modul';
		$m.__track_lines__[21] = 'bones.extendenrelational.py, line 21:\n    self.structure = structure';
		$m.__track_lines__[22] = 'bones.extendenrelational.py, line 22:\n    self.boneName = boneName';
		$m.__track_lines__[24] = 'bones.extendenrelational.py, line 24:\n    def render(self, data, field ):';
		$m.__track_lines__[25] = 'bones.extendenrelational.py, line 25:\n    assert field == self.boneName, "render() was called with field %s, expected %s" % (field,self.boneName)';
		$m.__track_lines__[26] = 'bones.extendenrelational.py, line 26:\n    if field in data.keys():';
		$m.__track_lines__[27] = 'bones.extendenrelational.py, line 27:\n    val = data[field]';
		$m.__track_lines__[29] = 'bones.extendenrelational.py, line 29:\n    val = ""';
		$m.__track_lines__[30] = 'bones.extendenrelational.py, line 30:\n    relStructList = self.structure[self.boneName]["using"]';
		$m.__track_lines__[31] = 'bones.extendenrelational.py, line 31:\n    relStructDict = { k:v for k,v in relStructList }';
		$m.__track_lines__[32] = 'bones.extendenrelational.py, line 32:\n    try:';
		$m.__track_lines__[33] = 'bones.extendenrelational.py, line 33:\n    if isinstance(val,list):';
		$m.__track_lines__[34] = 'bones.extendenrelational.py, line 34:\n    val = ", ".join( [ (formatString(formatString(self.format, self.structure, x["dest"], prefix=["dest"]), relStructDict, x["rel"], prefix=["rel"] ) or x["id"]) for x in val] )';
		$m.__track_lines__[36] = 'bones.extendenrelational.py, line 36:\n    val = formatString(formatString(self.format,self.structure, val["dest"], prefix=["dest"]), relStructDict, val["rel"], prefix=["rel"] ) or val["id"]';
		$m.__track_lines__[39] = 'bones.extendenrelational.py, line 39:\n    val = ""';
		$m.__track_lines__[40] = 'bones.extendenrelational.py, line 40:\n    return val';
		$m.__track_lines__[43] = 'bones.extendenrelational.py, line 43:\n    class ExtendedRelationalViewBoneDelegate( object ):';
		$m.__track_lines__[44] = 'bones.extendenrelational.py, line 44:\n    cantSort = True';
		$m.__track_lines__[45] = 'bones.extendenrelational.py, line 45:\n    def __init__(self, modul, boneName, structure):';
		$m.__track_lines__[46] = 'bones.extendenrelational.py, line 46:\n    super(ExtendedRelationalViewBoneDelegate, self).__init__()';
		$m.__track_lines__[47] = 'bones.extendenrelational.py, line 47:\n    self.format = "$(dest.name)"';
		$m.__track_lines__[48] = 'bones.extendenrelational.py, line 48:\n    if "format" in structure[boneName].keys():';
		$m.__track_lines__[49] = 'bones.extendenrelational.py, line 49:\n    self.format = structure[boneName]["format"]';
		$m.__track_lines__[50] = 'bones.extendenrelational.py, line 50:\n    self.modul = modul';
		$m.__track_lines__[51] = 'bones.extendenrelational.py, line 51:\n    self.structure = structure';
		$m.__track_lines__[52] = 'bones.extendenrelational.py, line 52:\n    self.boneName = boneName';
		$m.__track_lines__[54] = 'bones.extendenrelational.py, line 54:\n    def render(self, data, field ):';
		$m.__track_lines__[55] = 'bones.extendenrelational.py, line 55:\n    assert field == self.boneName, "render() was called with field %s, expected %s" % (field,self.boneName)';
		$m.__track_lines__[56] = 'bones.extendenrelational.py, line 56:\n    if field in data.keys():';
		$m.__track_lines__[57] = 'bones.extendenrelational.py, line 57:\n    val = data[field]';
		$m.__track_lines__[59] = 'bones.extendenrelational.py, line 59:\n    val = ""';
		$m.__track_lines__[60] = 'bones.extendenrelational.py, line 60:\n    relStructList = self.structure[self.boneName]["using"]';
		$m.__track_lines__[61] = 'bones.extendenrelational.py, line 61:\n    relStructDict = { k:v for k,v in relStructList }';
		$m.__track_lines__[62] = 'bones.extendenrelational.py, line 62:\n    try:';
		$m.__track_lines__[63] = 'bones.extendenrelational.py, line 63:\n    if isinstance(val,list):';
		$m.__track_lines__[64] = 'bones.extendenrelational.py, line 64:\n    if len(val)<5:';
		$m.__track_lines__[65] = 'bones.extendenrelational.py, line 65:\n    res = ", ".join( [ (formatString(formatString(self.format, self.structure, x["dest"], prefix=["dest"]), relStructDict, x["rel"], prefix=["rel"] ) or x["id"]) for x in val] )';
		$m.__track_lines__[67] = 'bones.extendenrelational.py, line 67:\n    res = ", ".join( [ (formatString(formatString(self.format, self.structure, x["dest"], prefix=["dest"]), relStructDict, x["rel"], prefix=["rel"] ) or x["id"]) for x in val[:4]] )';
		$m.__track_lines__[68] = 'bones.extendenrelational.py, line 68:\n    res += " "+translate("and {count} more",count=len(val)-4)';
		$m.__track_lines__[70] = 'bones.extendenrelational.py, line 70:\n    res = formatString(formatString(self.format,self.structure, val["dest"], prefix=["dest"]), relStructDict, val["rel"], prefix=["rel"] ) or val["id"]';
		$m.__track_lines__[73] = 'bones.extendenrelational.py, line 73:\n    res = ""';
		$m.__track_lines__[74] = 'bones.extendenrelational.py, line 74:\n    return( html5.Label( res ) )';
		$m.__track_lines__[76] = 'bones.extendenrelational.py, line 76:\n    class ExtendedRelationalSelectionBoneEntry( html5.Div ):';
		$m.__track_lines__[82] = 'bones.extendenrelational.py, line 82:\n    def __init__(self, parent, modul, data, using, errorInfo, *args, **kwargs ):';
		$m.__track_lines__[91] = 'bones.extendenrelational.py, line 91:\n    super( ExtendedRelationalSelectionBoneEntry, self ).__init__( *args, **kwargs )';
		$m.__track_lines__[92] = 'bones.extendenrelational.py, line 92:\n    self.parent = parent';
		$m.__track_lines__[93] = 'bones.extendenrelational.py, line 93:\n    self.modul = modul';
		$m.__track_lines__[94] = 'bones.extendenrelational.py, line 94:\n    self.data = data';
		$m.__track_lines__[95] = 'bones.extendenrelational.py, line 95:\n    if using and "dest" in data.keys():';
		$m.__track_lines__[96] = 'bones.extendenrelational.py, line 96:\n    if "name" in data["dest"].keys():';
		$m.__track_lines__[97] = 'bones.extendenrelational.py, line 97:\n    txtLbl = html5.Label( data["dest"]["name"])';
		$m.__track_lines__[99] = 'bones.extendenrelational.py, line 99:\n    txtLbl = html5.Label( data["dest"]["id"])';
		$m.__track_lines__[101] = 'bones.extendenrelational.py, line 101:\n    if "name" in data.keys():';
		$m.__track_lines__[102] = 'bones.extendenrelational.py, line 102:\n    txtLbl = html5.Label( data["name"])';
		$m.__track_lines__[104] = 'bones.extendenrelational.py, line 104:\n    txtLbl = html5.Label( data["id"])';
		$m.__track_lines__[105] = 'bones.extendenrelational.py, line 105:\n    wrapperDiv = html5.Div()';
		$m.__track_lines__[106] = 'bones.extendenrelational.py, line 106:\n    wrapperDiv.appendChild( txtLbl )';
		$m.__track_lines__[107] = 'bones.extendenrelational.py, line 107:\n    wrapperDiv["class"].append("labelwrapper")';
		$m.__track_lines__[109] = 'bones.extendenrelational.py, line 109:\n    if not parent.readOnly:';
		$m.__track_lines__[110] = 'bones.extendenrelational.py, line 110:\n    remBtn = html5.ext.Button(translate("Remove"), self.onRemove )';
		$m.__track_lines__[111] = 'bones.extendenrelational.py, line 111:\n    remBtn["class"].append("icon")';
		$m.__track_lines__[112] = 'bones.extendenrelational.py, line 112:\n    remBtn["class"].append("cancel")';
		$m.__track_lines__[113] = 'bones.extendenrelational.py, line 113:\n    wrapperDiv.appendChild( remBtn )';
		$m.__track_lines__[115] = 'bones.extendenrelational.py, line 115:\n    self.appendChild( wrapperDiv )';
		$m.__track_lines__[117] = 'bones.extendenrelational.py, line 117:\n    if using:';
		$m.__track_lines__[118] = 'bones.extendenrelational.py, line 118:\n    self.ie = InternalEdit( using, data["rel"], errorInfo, readOnly = parent.readOnly )';
		$m.__track_lines__[119] = 'bones.extendenrelational.py, line 119:\n    self.appendChild( self.ie )';
		$m.__track_lines__[121] = 'bones.extendenrelational.py, line 121:\n    self.ie = None';
		$m.__track_lines__[124] = 'bones.extendenrelational.py, line 124:\n    def onRemove(self, *args, **kwargs):';
		$m.__track_lines__[125] = 'bones.extendenrelational.py, line 125:\n    self.parent.removeEntry( self )';
		$m.__track_lines__[127] = 'bones.extendenrelational.py, line 127:\n    def serialize(self):';
		$m.__track_lines__[128] = 'bones.extendenrelational.py, line 128:\n    if self.ie:';
		$m.__track_lines__[129] = 'bones.extendenrelational.py, line 129:\n    res = {}';
		$m.__track_lines__[130] = 'bones.extendenrelational.py, line 130:\n    res.update( self.ie.doSave() )';
		$m.__track_lines__[131] = 'bones.extendenrelational.py, line 131:\n    res["id"] = self.data["dest"]["id"]';
		$m.__track_lines__[132] = 'bones.extendenrelational.py, line 132:\n    return( res )';
		$m.__track_lines__[134] = 'bones.extendenrelational.py, line 134:\n    return( self.data["id"] )';
		$m.__track_lines__[137] = 'bones.extendenrelational.py, line 137:\n    class ExtendedRelationalSelectionBone( html5.Div ):';
		$m.__track_lines__[142] = 'bones.extendenrelational.py, line 142:\n    def __init__(self, srcModul, boneName, readOnly, destModul, format="$(name)", using=None, *args, **kwargs ):';
		$m.__track_lines__[155] = 'bones.extendenrelational.py, line 155:\n    print("---------- EXTENDED RELATIONAL SELECTION UP---------------")';
		$m.__track_lines__[156] = 'bones.extendenrelational.py, line 156:\n    super( ExtendedRelationalSelectionBone,  self ).__init__( *args, **kwargs )';
		$m.__track_lines__[159] = 'bones.extendenrelational.py, line 159:\n    self.srcModul = srcModul';
		$m.__track_lines__[160] = 'bones.extendenrelational.py, line 160:\n    self.boneName = boneName';
		$m.__track_lines__[161] = 'bones.extendenrelational.py, line 161:\n    self.readOnly = readOnly';
		$m.__track_lines__[162] = 'bones.extendenrelational.py, line 162:\n    self.destModul = destModul';
		$m.__track_lines__[163] = 'bones.extendenrelational.py, line 163:\n    self.format = format';
		$m.__track_lines__[164] = 'bones.extendenrelational.py, line 164:\n    self.using = using';
		$m.__track_lines__[165] = 'bones.extendenrelational.py, line 165:\n    self.entries = []';
		$m.__track_lines__[166] = 'bones.extendenrelational.py, line 166:\n    self.extendedErrorInformation = {}';
		$m.__track_lines__[167] = 'bones.extendenrelational.py, line 167:\n    self.selectionDiv = html5.Div()';
		$m.__track_lines__[168] = 'bones.extendenrelational.py, line 168:\n    self.selectionDiv["class"].append("selectioncontainer")';
		$m.__track_lines__[169] = 'bones.extendenrelational.py, line 169:\n    self.appendChild( self.selectionDiv )';
		$m.__track_lines__[171] = 'bones.extendenrelational.py, line 171:\n    if ( "root" in conf[ "currentUser" ][ "access" ]';
		$m.__track_lines__[173] = 'bones.extendenrelational.py, line 173:\n    self.selectBtn = html5.ext.Button("Select", self.onShowSelector)';
		$m.__track_lines__[174] = 'bones.extendenrelational.py, line 174:\n    self.selectBtn["class"].append("icon")';
		$m.__track_lines__[175] = 'bones.extendenrelational.py, line 175:\n    self.selectBtn["class"].append("select")';
		$m.__track_lines__[176] = 'bones.extendenrelational.py, line 176:\n    self.appendChild( self.selectBtn )';
		$m.__track_lines__[178] = 'bones.extendenrelational.py, line 178:\n    self.selectBtn = None';
		$m.__track_lines__[180] = 'bones.extendenrelational.py, line 180:\n    if self.readOnly:';
		$m.__track_lines__[181] = 'bones.extendenrelational.py, line 181:\n    self["disabled"] = True';
		$m.__track_lines__[183] = 'bones.extendenrelational.py, line 183:\n    def _setDisabled(self, disable):';
		$m.__track_lines__[187] = 'bones.extendenrelational.py, line 187:\n    super(ExtendedRelationalSelectionBone, self)._setDisabled( disable )';
		$m.__track_lines__[188] = 'bones.extendenrelational.py, line 188:\n    if not disable and not self._disabledState and "is_active" in self.parent()["class"]:';
		$m.__track_lines__[189] = 'bones.extendenrelational.py, line 189:\n    self.parent()["class"].remove("is_active")';
		$m.__track_lines__[193] = 'bones.extendenrelational.py, line 192:\n    @classmethod ... def fromSkelStructure( cls, modulName, boneName, skelStructure ):';
		$m.__track_lines__[203] = 'bones.extendenrelational.py, line 203:\n    readOnly = "readonly" in skelStructure[ boneName ].keys() and skelStructure[ boneName ]["readonly"]';
		$m.__track_lines__[204] = 'bones.extendenrelational.py, line 204:\n    multiple = skelStructure[boneName]["multiple"]';
		$m.__track_lines__[205] = 'bones.extendenrelational.py, line 205:\n    if "modul" in skelStructure[ boneName ].keys():';
		$m.__track_lines__[206] = 'bones.extendenrelational.py, line 206:\n    destModul = skelStructure[ boneName ][ "modul" ]';
		$m.__track_lines__[208] = 'bones.extendenrelational.py, line 208:\n    destModul = skelStructure[ boneName ]["type"].split(".")[1]';
		$m.__track_lines__[209] = 'bones.extendenrelational.py, line 209:\n    format= "$(name)"';
		$m.__track_lines__[210] = 'bones.extendenrelational.py, line 210:\n    if "format" in skelStructure[ boneName ].keys():';
		$m.__track_lines__[211] = 'bones.extendenrelational.py, line 211:\n    format = skelStructure[ boneName ]["format"]';
		$m.__track_lines__[212] = 'bones.extendenrelational.py, line 212:\n    using= None';
		$m.__track_lines__[213] = 'bones.extendenrelational.py, line 213:\n    if "using" in skelStructure[ boneName ].keys():';
		$m.__track_lines__[214] = 'bones.extendenrelational.py, line 214:\n    using = skelStructure[ boneName ]["using"]';
		$m.__track_lines__[215] = 'bones.extendenrelational.py, line 215:\n    return( cls( modulName, boneName, readOnly, destModul=destModul, format=format, using=using ) )';
		$m.__track_lines__[217] = 'bones.extendenrelational.py, line 217:\n    def unserialize(self, data):';
		$m.__track_lines__[223] = 'bones.extendenrelational.py, line 223:\n    if self.boneName in data.keys():';
		$m.__track_lines__[224] = 'bones.extendenrelational.py, line 224:\n    print("USERIALIZING", data[ self.boneName ])';
		$m.__track_lines__[225] = 'bones.extendenrelational.py, line 225:\n    val = data[ self.boneName ]';
		$m.__track_lines__[226] = 'bones.extendenrelational.py, line 226:\n    if isinstance( val, dict ):';
		$m.__track_lines__[227] = 'bones.extendenrelational.py, line 227:\n    val = [ val ]';
		$m.__track_lines__[228] = 'bones.extendenrelational.py, line 228:\n    self.setSelection( val )';
		$m.__track_lines__[232] = 'bones.extendenrelational.py, line 232:\n    def serializeForPost(self):';
		$m.__track_lines__[238] = 'bones.extendenrelational.py, line 238:\n    res = {}';
		$m.__track_lines__[239] = 'bones.extendenrelational.py, line 239:\n    idx = 0';
		$m.__track_lines__[240] = 'bones.extendenrelational.py, line 240:\n    for entry in self.entries:';
		$m.__track_lines__[241] = 'bones.extendenrelational.py, line 241:\n    currRes = entry.serialize()';
		$m.__track_lines__[242] = 'bones.extendenrelational.py, line 242:\n    if isinstance( currRes, dict ):';
		$m.__track_lines__[243] = 'bones.extendenrelational.py, line 243:\n    for k,v in currRes.items():';
		$m.__track_lines__[244] = 'bones.extendenrelational.py, line 244:\n    res["%s%s.%s" % (self.boneName,idx,k) ] = v';
		$m.__track_lines__[246] = 'bones.extendenrelational.py, line 246:\n    res["%s%s.id" % (self.boneName,idx) ] = currRes';
		$m.__track_lines__[247] = 'bones.extendenrelational.py, line 247:\n    idx += 1';
		$m.__track_lines__[248] = 'bones.extendenrelational.py, line 248:\n    return( res )';
		$m.__track_lines__[251] = 'bones.extendenrelational.py, line 251:\n    def serializeForDocument(self):';
		$m.__track_lines__[252] = 'bones.extendenrelational.py, line 252:\n    return( self.serialize( ) )';
		$m.__track_lines__[254] = 'bones.extendenrelational.py, line 254:\n    def onShowSelector(self, *args, **kwargs):';
		$m.__track_lines__[258] = 'bones.extendenrelational.py, line 258:\n    currentSelector = ListWidget( self.destModul, isSelector=True )';
		$m.__track_lines__[259] = 'bones.extendenrelational.py, line 259:\n    currentSelector.selectionActivatedEvent.register( self )';
		$m.__track_lines__[260] = 'bones.extendenrelational.py, line 260:\n    conf["mainWindow"].stackWidget( currentSelector )';
		$m.__track_lines__[261] = 'bones.extendenrelational.py, line 261:\n    self.parent()["class"].append("is_active")';
		$m.__track_lines__[263] = 'bones.extendenrelational.py, line 263:\n    def onSelectionActivated(self, table, selection ):';
		$m.__track_lines__[267] = 'bones.extendenrelational.py, line 267:\n    if self.using:';
		$m.__track_lines__[268] = 'bones.extendenrelational.py, line 268:\n    selection = [{"dest": data,"rel":{}} for data in selection]';
		$m.__track_lines__[269] = 'bones.extendenrelational.py, line 269:\n    self.setSelection( selection )';
		$m.__track_lines__[271] = 'bones.extendenrelational.py, line 271:\n    def setSelection(self, selection):';
		$m.__track_lines__[277] = 'bones.extendenrelational.py, line 277:\n    if selection is None:';
		$m.__track_lines__[278] = 'bones.extendenrelational.py, line 278:\n    return';
		$m.__track_lines__[279] = 'bones.extendenrelational.py, line 279:\n    for data in selection:';
		$m.__track_lines__[280] = 'bones.extendenrelational.py, line 280:\n    errIdx = len( self. entries )';
		$m.__track_lines__[281] = 'bones.extendenrelational.py, line 281:\n    errDict = {}';
		$m.__track_lines__[282] = 'bones.extendenrelational.py, line 282:\n    if self.extendedErrorInformation:';
		$m.__track_lines__[283] = 'bones.extendenrelational.py, line 283:\n    for k,v in self.extendedErrorInformation.items():';
		$m.__track_lines__[284] = 'bones.extendenrelational.py, line 284:\n    k = k.replace("%s." % self.boneName, "")';
		$m.__track_lines__[285] = 'bones.extendenrelational.py, line 285:\n    if 1:';
		$m.__track_lines__[286] = 'bones.extendenrelational.py, line 286:\n    idx, errKey = k.split(".")';
		$m.__track_lines__[287] = 'bones.extendenrelational.py, line 287:\n    idx = int( idx )';
		$m.__track_lines__[289] = 'bones.extendenrelational.py, line 289:\n    continue';
		$m.__track_lines__[290] = 'bones.extendenrelational.py, line 290:\n    if idx == errIdx:';
		$m.__track_lines__[291] = 'bones.extendenrelational.py, line 291:\n    errDict[ errKey ] = v';
		$m.__track_lines__[292] = 'bones.extendenrelational.py, line 292:\n    entry = ExtendedRelationalSelectionBoneEntry( self, self.destModul, data, self.using, errDict )';
		$m.__track_lines__[293] = 'bones.extendenrelational.py, line 293:\n    self.addEntry( entry )';
		$m.__track_lines__[295] = 'bones.extendenrelational.py, line 295:\n    def addEntry(self, entry):';
		$m.__track_lines__[300] = 'bones.extendenrelational.py, line 300:\n    print("--adding entry--")';
		$m.__track_lines__[301] = 'bones.extendenrelational.py, line 301:\n    self.entries.append( entry )';
		$m.__track_lines__[302] = 'bones.extendenrelational.py, line 302:\n    self.selectionDiv.appendChild( entry )';
		$m.__track_lines__[304] = 'bones.extendenrelational.py, line 304:\n    def removeEntry(self, entry ):';
		$m.__track_lines__[309] = 'bones.extendenrelational.py, line 309:\n    assert entry in self.entries, "Cannot remove unknown entry %s from realtionalBone" % str(entry)';
		$m.__track_lines__[310] = 'bones.extendenrelational.py, line 310:\n    self.selectionDiv.removeChild( entry )';
		$m.__track_lines__[311] = 'bones.extendenrelational.py, line 311:\n    self.entries.remove( entry )';
		$m.__track_lines__[313] = 'bones.extendenrelational.py, line 313:\n    def setExtendedErrorInformation(self, errorInfo ):';
		$m.__track_lines__[314] = 'bones.extendenrelational.py, line 314:\n    print("------- EXTENDEND ERROR INFO --------")';
		$m.__track_lines__[315] = 'bones.extendenrelational.py, line 315:\n    print( errorInfo )';
		$m.__track_lines__[316] = 'bones.extendenrelational.py, line 316:\n    self.extendedErrorInformation = errorInfo';
		$m.__track_lines__[317] = 'bones.extendenrelational.py, line 317:\n    for k,v in errorInfo.items():';
		$m.__track_lines__[318] = 'bones.extendenrelational.py, line 318:\n    k = k.replace("%s." % self.boneName, "")';
		$m.__track_lines__[319] = 'bones.extendenrelational.py, line 319:\n    if 1:';
		$m.__track_lines__[320] = 'bones.extendenrelational.py, line 320:\n    idx, err = k.split(".")';
		$m.__track_lines__[321] = 'bones.extendenrelational.py, line 321:\n    idx = int( idx )';
		$m.__track_lines__[323] = 'bones.extendenrelational.py, line 323:\n    continue';
		$m.__track_lines__[324] = 'bones.extendenrelational.py, line 324:\n    print("k: %s, v: %s" % (k,v))';
		$m.__track_lines__[325] = 'bones.extendenrelational.py, line 325:\n    print("idx: %s" % idx )';
		$m.__track_lines__[326] = 'bones.extendenrelational.py, line 326:\n    print( len(self.entries))';
		$m.__track_lines__[327] = 'bones.extendenrelational.py, line 327:\n    if idx>=0 and idx < len(self.entries):';
		$m.__track_lines__[328] = 'bones.extendenrelational.py, line 328:\n    self.entries[ idx ].setError( err )';
		$m.__track_lines__[329] = 'bones.extendenrelational.py, line 329:\n    pass';
		$m.__track_lines__[332] = 'bones.extendenrelational.py, line 332:\n    class ExtendedRelationalSearch( html5.Div ):';
		$m.__track_lines__[333] = 'bones.extendenrelational.py, line 333:\n    def __init__(self, extension, view, modul, *args, **kwargs ):';
		$m.__track_lines__[334] = 'bones.extendenrelational.py, line 334:\n    super( ExtendedRelationalSearch, self ).__init__( *args, **kwargs )';
		$m.__track_lines__[335] = 'bones.extendenrelational.py, line 335:\n    self.view = view';
		$m.__track_lines__[336] = 'bones.extendenrelational.py, line 336:\n    self.extension = extension';
		$m.__track_lines__[337] = 'bones.extendenrelational.py, line 337:\n    self.modul = modul';
		$m.__track_lines__[338] = 'bones.extendenrelational.py, line 338:\n    self.currentSelection = None';
		$m.__track_lines__[339] = 'bones.extendenrelational.py, line 339:\n    self.filterChangedEvent = EventDispatcher("filterChanged")';
		$m.__track_lines__[340] = 'bones.extendenrelational.py, line 340:\n    self.appendChild( html5.TextNode("RELATIONAL SEARCH"))';
		$m.__track_lines__[341] = 'bones.extendenrelational.py, line 341:\n    self.appendChild( html5.TextNode(extension["name"]))';
		$m.__track_lines__[342] = 'bones.extendenrelational.py, line 342:\n    self.currentEntry = html5.Span()';
		$m.__track_lines__[343] = 'bones.extendenrelational.py, line 343:\n    self.appendChild(self.currentEntry)';
		$m.__track_lines__[344] = 'bones.extendenrelational.py, line 344:\n    btn = html5.ext.Button("Select", self.openSelector)';
		$m.__track_lines__[345] = 'bones.extendenrelational.py, line 345:\n    self.appendChild( btn )';
		$m.__track_lines__[346] = 'bones.extendenrelational.py, line 346:\n    btn = html5.ext.Button("Clear", self.clearSelection)';
		$m.__track_lines__[347] = 'bones.extendenrelational.py, line 347:\n    self.appendChild( btn )';
		$m.__track_lines__[349] = 'bones.extendenrelational.py, line 349:\n    def clearSelection(self, *args, **kwargs):';
		$m.__track_lines__[350] = 'bones.extendenrelational.py, line 350:\n    self.currentSelection = None';
		$m.__track_lines__[351] = 'bones.extendenrelational.py, line 351:\n    self.filterChangedEvent.fire()';
		$m.__track_lines__[353] = 'bones.extendenrelational.py, line 353:\n    def openSelector(self, *args, **kwargs):';
		$m.__track_lines__[354] = 'bones.extendenrelational.py, line 354:\n    currentSelector = ListWidget( self.extension["modul"], isSelector=True )';
		$m.__track_lines__[355] = 'bones.extendenrelational.py, line 355:\n    currentSelector.selectionActivatedEvent.register( self )';
		$m.__track_lines__[356] = 'bones.extendenrelational.py, line 356:\n    conf["mainWindow"].stackWidget( currentSelector )';
		$m.__track_lines__[358] = 'bones.extendenrelational.py, line 358:\n    def onSelectionActivated(self, table,selection):';
		$m.__track_lines__[359] = 'bones.extendenrelational.py, line 359:\n    self.currentSelection = selection';
		$m.__track_lines__[360] = 'bones.extendenrelational.py, line 360:\n    self.filterChangedEvent.fire()';
		$m.__track_lines__[363] = 'bones.extendenrelational.py, line 363:\n    def updateFilter(self, filter):';
		$m.__track_lines__[364] = 'bones.extendenrelational.py, line 364:\n    if self.currentSelection:';
		$m.__track_lines__[365] = 'bones.extendenrelational.py, line 365:\n    self.currentEntry.element.innerHTML = self.currentSelection[0]["name"]';
		$m.__track_lines__[366] = 'bones.extendenrelational.py, line 366:\n    newId = self.currentSelection[0]["id"]';
		$m.__track_lines__[367] = 'bones.extendenrelational.py, line 367:\n    filter[ self.extension["target"]+".id" ] = newId';
		$m.__track_lines__[369] = 'bones.extendenrelational.py, line 369:\n    self.currentEntry.element.innerHTML = ""';
		$m.__track_lines__[370] = 'bones.extendenrelational.py, line 370:\n    return( filter )';
		$m.__track_lines__[373] = 'bones.extendenrelational.py, line 372:\n    @staticmethod ... def canHandleExtension( extension, view, modul ):';
		$m.__track_lines__[374] = 'bones.extendenrelational.py, line 374:\n    return( isinstance( extension, dict) and "type" in extension.keys() and (extension["type"]=="relational" or extension["type"].startswith("relational.") ) )';
		$m.__track_lines__[377] = 'bones.extendenrelational.py, line 377:\n    def CheckForExtendedRelationalBoneSelection( modulName, boneName, skelStructure, *args, **kwargs ):';
		$m.__track_lines__[378] = 'bones.extendenrelational.py, line 378:\n    isMultiple = "multiple" in skelStructure[boneName].keys() and skelStructure[boneName]["multiple"]';
		$m.__track_lines__[379] = 'bones.extendenrelational.py, line 379:\n    return( skelStructure[boneName]["type"].startswith("extendedrelational.") and isMultiple )';
		$m.__track_lines__[384] = 'bones.extendenrelational.py, line 384:\n    editBoneSelector.insert( 5, CheckForExtendedRelationalBoneSelection, ExtendedRelationalSelectionBone)';
		$m.__track_lines__[385] = 'bones.extendenrelational.py, line 385:\n    viewDelegateSelector.insert( 5, CheckForExtendedRelationalBoneSelection, ExtendedRelationalViewBoneDelegate)';
		$m.__track_lines__[386] = 'bones.extendenrelational.py, line 386:\n    extendedSearchWidgetSelector.insert( 1, ExtendedRelationalSearch.canHandleExtension, ExtendedRelationalSearch )';
		$m.__track_lines__[387] = 'bones.extendenrelational.py, line 387:\n    extractorDelegateSelector.insert(4, CheckForExtendedRelationalBoneSelection, ExtendedRelationalBoneExtractor)';

		var $constant_int_0 = new $p['int'](0);
		var $constant_int_1 = new $p['int'](1);
		var $constant_int_4 = new $p['int'](4);
		var $constant_int_5 = new $p['int'](5);
		$pyjs['track']['module']='bones.extendenrelational';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=3;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['html5'] = $p['___import___']('html5', 'bones');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=4;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['editBoneSelector'] = $p['___import___']('priorityqueue.editBoneSelector', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['viewDelegateSelector'] = $p['___import___']('priorityqueue.viewDelegateSelector', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['extendedSearchWidgetSelector'] = $p['___import___']('priorityqueue.extendedSearchWidgetSelector', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['extractorDelegateSelector'] = $p['___import___']('priorityqueue.extractorDelegateSelector', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=5;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['EventDispatcher'] = $p['___import___']('event.EventDispatcher', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=6;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['formatString'] = $p['___import___']('utils.formatString', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=7;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['ListWidget'] = $p['___import___']('widgets.list.ListWidget', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=8;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['InternalEdit'] = $p['___import___']('widgets.edit.InternalEdit', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=9;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['conf'] = $p['___import___']('config.conf', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=10;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['translate'] = $p['___import___']('i18n.translate', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=13;
		$m['ExtendedRelationalBoneExtractor'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.extendenrelational';
			$cls_definition['__md5__'] = 'cbc75b2a6dd6f614067652e617d83ea8';
			$pyjs['track']['lineno']=14;
			$cls_definition['cantSort'] = true;
			$pyjs['track']['lineno']=15;
			$method = $pyjs__bind_method2('__init__', function(modul, boneName, structure) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 4, arguments['length']+1);
				} else {
					var self = arguments[0];
					modul = arguments[1];
					boneName = arguments[2];
					structure = arguments[3];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 4, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'cbc75b2a6dd6f614067652e617d83ea8') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':15};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=15;
				$pyjs['track']['lineno']=16;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ExtendedRelationalBoneExtractor'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})()['__init__']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})();
				$pyjs['track']['lineno']=17;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('format', '$(dest.name)') : $p['setattr'](self, 'format', '$(dest.name)'); 
				$pyjs['track']['lineno']=18;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return structure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})()['__contains__']('format'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})()) {
					$pyjs['track']['lineno']=19;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('format', structure['__getitem__'](boneName)['__getitem__']('format')) : $p['setattr'](self, 'format', structure['__getitem__'](boneName)['__getitem__']('format')); 
				}
				$pyjs['track']['lineno']=20;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=21;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('structure', structure) : $p['setattr'](self, 'structure', structure); 
				$pyjs['track']['lineno']=22;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('boneName', boneName) : $p['setattr'](self, 'boneName', boneName); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['modul'],['boneName'],['structure']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=24;
			$method = $pyjs__bind_method2('render', function(data, field) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					field = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'cbc75b2a6dd6f614067652e617d83ea8') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $or4,relStructList,$or3,val,relStructDict,$pyjs_try_err;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':24};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=24;
				$pyjs['track']['lineno']=25;
				if (!( $p['op_eq'](field, $p['getattr'](self, 'boneName')) )) {
				   throw $p['AssertionError']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('render() was called with field %s, expected %s', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['tuple']([field, $p['getattr'](self, 'boneName')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})());
				 }
				$pyjs['track']['lineno']=26;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})()['__contains__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})()) {
					$pyjs['track']['lineno']=27;
					val = data['__getitem__'](field);
				}
				else {
					$pyjs['track']['lineno']=29;
					val = '';
				}
				$pyjs['track']['lineno']=30;
				relStructList = $p['getattr'](self, 'structure')['__getitem__']($p['getattr'](self, 'boneName'))['__getitem__']('using');
				$pyjs['track']['lineno']=31;
				relStructDict = function(){
					var $iter1_nextval,$iter1_type,k,$pyjs__trackstack_size_1,$collcomp1,$iter1_iter,$iter1_idx,v,$iter1_array;
	$collcomp1 = $p['dict']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter1_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return relStructList;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})();
				$iter1_nextval=$p['__iter_prepare']($iter1_iter,false);
				while (typeof($p['__wrapped_next']($iter1_nextval)['$nextval']) != 'undefined') {
					var $tupleassign1 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter1_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})();
					k = $tupleassign1[0];
					v = $tupleassign1[1];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp1['__setitem__'](k, v);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='bones.extendenrelational';

	return $collcomp1;}();
				$pyjs['track']['lineno']=32;
				var $pyjs__trackstack_size_1 = $pyjs['trackstack']['length'];
				try {
					try {
						$pyjs['in_try_except'] += 1;
						$pyjs['track']['lineno']=33;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['isinstance'](val, $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})()) {
							$pyjs['track']['lineno']=34;
							val = (function(){try{try{$pyjs['in_try_except'] += 1;
							return ', '['join'](function(){
								var $iter2_nextval,$iter2_type,$iter2_iter,$or2,$collcomp2,$iter2_idx,$pyjs__trackstack_size_2,x,$or1,$iter2_array;
	$collcomp2 = $p['list']();
							$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
							$iter2_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
							return val;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_14_err){if (!$p['isinstance']($pyjs_dbg_14_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_14_err);}throw $pyjs_dbg_14_err;
}})();
							$iter2_nextval=$p['__iter_prepare']($iter2_iter,false);
							while (typeof($p['__wrapped_next']($iter2_nextval)['$nextval']) != 'undefined') {
								x = $iter2_nextval['$nextval'];
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return $collcomp2['append'](($p['bool']($or1=(function(){try{try{$pyjs['in_try_except'] += 1;
								return $pyjs_kwargs_call(null, $m['formatString'], null, null, [{'prefix':(function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['list'](['rel']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_17_err){if (!$p['isinstance']($pyjs_dbg_17_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_17_err);}throw $pyjs_dbg_17_err;
}})()}, (function(){try{try{$pyjs['in_try_except'] += 1;
								return $pyjs_kwargs_call(null, $m['formatString'], null, null, [{'prefix':(function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['list'](['dest']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})()}, $p['getattr'](self, 'format'), $p['getattr'](self, 'structure'), x['__getitem__']('dest')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_16_err){if (!$p['isinstance']($pyjs_dbg_16_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_16_err);}throw $pyjs_dbg_16_err;
}})(), relStructDict, x['__getitem__']('rel')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})())?$or1:x['__getitem__']('id')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_19_err){if (!$p['isinstance']($pyjs_dbg_19_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_19_err);}throw $pyjs_dbg_19_err;
}})();
							}
							if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
								$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
								$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
							}
							$pyjs['track']['module']='bones.extendenrelational';

	return $collcomp2;}());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})();
						}
						else if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['isinstance'](val, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})()) {
							$pyjs['track']['lineno']=36;
							val = ($p['bool']($or3=(function(){try{try{$pyjs['in_try_except'] += 1;
							return $pyjs_kwargs_call(null, $m['formatString'], null, null, [{'prefix':(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['list'](['rel']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})()}, (function(){try{try{$pyjs['in_try_except'] += 1;
							return $pyjs_kwargs_call(null, $m['formatString'], null, null, [{'prefix':(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['list'](['dest']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})()}, $p['getattr'](self, 'format'), $p['getattr'](self, 'structure'), val['__getitem__']('dest')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})(), relStructDict, val['__getitem__']('rel')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_26_err){if (!$p['isinstance']($pyjs_dbg_26_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_26_err);}throw $pyjs_dbg_26_err;
}})())?$or3:val['__getitem__']('id'));
						}
					} finally { $pyjs['in_try_except'] -= 1; }
				} catch($pyjs_try_err) {
					$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_1 - 1);
					$pyjs['__active_exception_stack__'] = null;
					$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
					var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
					$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.extendenrelational';
					if (true) {
						$pyjs['track']['lineno']=39;
						val = '';
					}
				}
				$pyjs['track']['lineno']=40;
				$pyjs['track']['lineno']=40;
				var $pyjs__ret = val;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['data'],['field']]);
			$cls_definition['render'] = $method;
			$pyjs['track']['lineno']=13;
			var $bases = new Array($p['object']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('ExtendedRelationalBoneExtractor', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=43;
		$m['ExtendedRelationalViewBoneDelegate'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.extendenrelational';
			$cls_definition['__md5__'] = '4eb6813d9290fd45eb4ae74d89ee2dac';
			$pyjs['track']['lineno']=44;
			$cls_definition['cantSort'] = true;
			$pyjs['track']['lineno']=45;
			$method = $pyjs__bind_method2('__init__', function(modul, boneName, structure) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 4, arguments['length']+1);
				} else {
					var self = arguments[0];
					modul = arguments[1];
					boneName = arguments[2];
					structure = arguments[3];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 4, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '4eb6813d9290fd45eb4ae74d89ee2dac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':45};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=45;
				$pyjs['track']['lineno']=46;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ExtendedRelationalViewBoneDelegate'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_27_err){if (!$p['isinstance']($pyjs_dbg_27_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_27_err);}throw $pyjs_dbg_27_err;
}})()['__init__']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_28_err){if (!$p['isinstance']($pyjs_dbg_28_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_28_err);}throw $pyjs_dbg_28_err;
}})();
				$pyjs['track']['lineno']=47;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('format', '$(dest.name)') : $p['setattr'](self, 'format', '$(dest.name)'); 
				$pyjs['track']['lineno']=48;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return structure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_29_err){if (!$p['isinstance']($pyjs_dbg_29_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_29_err);}throw $pyjs_dbg_29_err;
}})()['__contains__']('format'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_30_err){if (!$p['isinstance']($pyjs_dbg_30_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_30_err);}throw $pyjs_dbg_30_err;
}})()) {
					$pyjs['track']['lineno']=49;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('format', structure['__getitem__'](boneName)['__getitem__']('format')) : $p['setattr'](self, 'format', structure['__getitem__'](boneName)['__getitem__']('format')); 
				}
				$pyjs['track']['lineno']=50;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=51;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('structure', structure) : $p['setattr'](self, 'structure', structure); 
				$pyjs['track']['lineno']=52;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('boneName', boneName) : $p['setattr'](self, 'boneName', boneName); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['modul'],['boneName'],['structure']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=54;
			$method = $pyjs__bind_method2('render', function(data, field) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					field = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '4eb6813d9290fd45eb4ae74d89ee2dac') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var relStructList,val,$add3,res,relStructDict,$or9,$pyjs_try_err,$add2,$add1,$or10,$add4,$sub2,$sub1;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':54};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=54;
				$pyjs['track']['lineno']=55;
				if (!( $p['op_eq'](field, $p['getattr'](self, 'boneName')) )) {
				   throw $p['AssertionError']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('render() was called with field %s, expected %s', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['tuple']([field, $p['getattr'](self, 'boneName')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_31_err){if (!$p['isinstance']($pyjs_dbg_31_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_31_err);}throw $pyjs_dbg_31_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_32_err){if (!$p['isinstance']($pyjs_dbg_32_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_32_err);}throw $pyjs_dbg_32_err;
}})());
				 }
				$pyjs['track']['lineno']=56;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_33_err){if (!$p['isinstance']($pyjs_dbg_33_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_33_err);}throw $pyjs_dbg_33_err;
}})()['__contains__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_34_err){if (!$p['isinstance']($pyjs_dbg_34_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_34_err);}throw $pyjs_dbg_34_err;
}})()) {
					$pyjs['track']['lineno']=57;
					val = data['__getitem__'](field);
				}
				else {
					$pyjs['track']['lineno']=59;
					val = '';
				}
				$pyjs['track']['lineno']=60;
				relStructList = $p['getattr'](self, 'structure')['__getitem__']($p['getattr'](self, 'boneName'))['__getitem__']('using');
				$pyjs['track']['lineno']=61;
				relStructDict = function(){
					var $iter3_idx,v,$iter3_nextval,$iter3_type,$collcomp3,$iter3_iter,$iter3_array,$pyjs__trackstack_size_1,k;
	$collcomp3 = $p['dict']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter3_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return relStructList;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_36_err){if (!$p['isinstance']($pyjs_dbg_36_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_36_err);}throw $pyjs_dbg_36_err;
}})();
				$iter3_nextval=$p['__iter_prepare']($iter3_iter,false);
				while (typeof($p['__wrapped_next']($iter3_nextval)['$nextval']) != 'undefined') {
					var $tupleassign2 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter3_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})();
					k = $tupleassign2[0];
					v = $tupleassign2[1];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp3['__setitem__'](k, v);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_37_err){if (!$p['isinstance']($pyjs_dbg_37_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_37_err);}throw $pyjs_dbg_37_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='bones.extendenrelational';

	return $collcomp3;}();
				$pyjs['track']['lineno']=62;
				var $pyjs__trackstack_size_1 = $pyjs['trackstack']['length'];
				try {
					try {
						$pyjs['in_try_except'] += 1;
						$pyjs['track']['lineno']=63;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['isinstance'](val, $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_38_err){if (!$p['isinstance']($pyjs_dbg_38_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_38_err);}throw $pyjs_dbg_38_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_39_err){if (!$p['isinstance']($pyjs_dbg_39_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_39_err);}throw $pyjs_dbg_39_err;
}})()) {
							$pyjs['track']['lineno']=64;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['len'](val);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_40_err){if (!$p['isinstance']($pyjs_dbg_40_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_40_err);}throw $pyjs_dbg_40_err;
}})(), $constant_int_5) == -1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_41_err){if (!$p['isinstance']($pyjs_dbg_41_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_41_err);}throw $pyjs_dbg_41_err;
}})()) {
								$pyjs['track']['lineno']=65;
								res = (function(){try{try{$pyjs['in_try_except'] += 1;
								return ', '['join'](function(){
									var $or5,$or6,$iter4_nextval,$collcomp4,$iter4_idx,$pyjs__trackstack_size_2,$iter4_type,$iter4_array,x,$iter4_iter;
	$collcomp4 = $p['list']();
								$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
								$iter4_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
								return val;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_42_err){if (!$p['isinstance']($pyjs_dbg_42_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_42_err);}throw $pyjs_dbg_42_err;
}})();
								$iter4_nextval=$p['__iter_prepare']($iter4_iter,false);
								while (typeof($p['__wrapped_next']($iter4_nextval)['$nextval']) != 'undefined') {
									x = $iter4_nextval['$nextval'];
									(function(){try{try{$pyjs['in_try_except'] += 1;
									return $collcomp4['append'](($p['bool']($or5=(function(){try{try{$pyjs['in_try_except'] += 1;
									return $pyjs_kwargs_call(null, $m['formatString'], null, null, [{'prefix':(function(){try{try{$pyjs['in_try_except'] += 1;
									return $p['list'](['rel']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_45_err){if (!$p['isinstance']($pyjs_dbg_45_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_45_err);}throw $pyjs_dbg_45_err;
}})()}, (function(){try{try{$pyjs['in_try_except'] += 1;
									return $pyjs_kwargs_call(null, $m['formatString'], null, null, [{'prefix':(function(){try{try{$pyjs['in_try_except'] += 1;
									return $p['list'](['dest']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_43_err){if (!$p['isinstance']($pyjs_dbg_43_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_43_err);}throw $pyjs_dbg_43_err;
}})()}, $p['getattr'](self, 'format'), $p['getattr'](self, 'structure'), x['__getitem__']('dest')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_44_err){if (!$p['isinstance']($pyjs_dbg_44_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_44_err);}throw $pyjs_dbg_44_err;
}})(), relStructDict, x['__getitem__']('rel')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_46_err){if (!$p['isinstance']($pyjs_dbg_46_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_46_err);}throw $pyjs_dbg_46_err;
}})())?$or5:x['__getitem__']('id')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})();
								}
								if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
									$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
									$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
								}
								$pyjs['track']['module']='bones.extendenrelational';

	return $collcomp4;}());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_48_err){if (!$p['isinstance']($pyjs_dbg_48_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_48_err);}throw $pyjs_dbg_48_err;
}})();
							}
							else {
								$pyjs['track']['lineno']=67;
								res = (function(){try{try{$pyjs['in_try_except'] += 1;
								return ', '['join'](function(){
									var $iter5_nextval,x,$or7,$iter5_idx,$collcomp5,$or8,$iter5_iter,$pyjs__trackstack_size_2,$iter5_array,$iter5_type;
	$collcomp5 = $p['list']();
								$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
								$iter5_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['__getslice'](val, 0, $constant_int_4);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_49_err){if (!$p['isinstance']($pyjs_dbg_49_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_49_err);}throw $pyjs_dbg_49_err;
}})();
								$iter5_nextval=$p['__iter_prepare']($iter5_iter,false);
								while (typeof($p['__wrapped_next']($iter5_nextval)['$nextval']) != 'undefined') {
									x = $iter5_nextval['$nextval'];
									(function(){try{try{$pyjs['in_try_except'] += 1;
									return $collcomp5['append'](($p['bool']($or7=(function(){try{try{$pyjs['in_try_except'] += 1;
									return $pyjs_kwargs_call(null, $m['formatString'], null, null, [{'prefix':(function(){try{try{$pyjs['in_try_except'] += 1;
									return $p['list'](['rel']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_52_err){if (!$p['isinstance']($pyjs_dbg_52_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_52_err);}throw $pyjs_dbg_52_err;
}})()}, (function(){try{try{$pyjs['in_try_except'] += 1;
									return $pyjs_kwargs_call(null, $m['formatString'], null, null, [{'prefix':(function(){try{try{$pyjs['in_try_except'] += 1;
									return $p['list'](['dest']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_50_err){if (!$p['isinstance']($pyjs_dbg_50_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_50_err);}throw $pyjs_dbg_50_err;
}})()}, $p['getattr'](self, 'format'), $p['getattr'](self, 'structure'), x['__getitem__']('dest')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_51_err){if (!$p['isinstance']($pyjs_dbg_51_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_51_err);}throw $pyjs_dbg_51_err;
}})(), relStructDict, x['__getitem__']('rel')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_53_err){if (!$p['isinstance']($pyjs_dbg_53_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_53_err);}throw $pyjs_dbg_53_err;
}})())?$or7:x['__getitem__']('id')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_54_err){if (!$p['isinstance']($pyjs_dbg_54_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_54_err);}throw $pyjs_dbg_54_err;
}})();
								}
								if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
									$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
									$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
								}
								$pyjs['track']['module']='bones.extendenrelational';

	return $collcomp5;}());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_55_err){if (!$p['isinstance']($pyjs_dbg_55_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_55_err);}throw $pyjs_dbg_55_err;
}})();
								$pyjs['track']['lineno']=68;
								res = $p['__op_add']($add3=res,$add4=$p['__op_add']($add1=' ',$add2=(function(){try{try{$pyjs['in_try_except'] += 1;
								return $pyjs_kwargs_call(null, $m['translate'], null, null, [{'count':$p['__op_sub']($sub1=(function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['len'](val);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_56_err){if (!$p['isinstance']($pyjs_dbg_56_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_56_err);}throw $pyjs_dbg_56_err;
}})(),$sub2=$constant_int_4)}, 'and {count} more']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_57_err){if (!$p['isinstance']($pyjs_dbg_57_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_57_err);}throw $pyjs_dbg_57_err;
}})()));
							}
						}
						else if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['isinstance'](val, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_58_err){if (!$p['isinstance']($pyjs_dbg_58_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_58_err);}throw $pyjs_dbg_58_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_59_err){if (!$p['isinstance']($pyjs_dbg_59_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_59_err);}throw $pyjs_dbg_59_err;
}})()) {
							$pyjs['track']['lineno']=70;
							res = ($p['bool']($or9=(function(){try{try{$pyjs['in_try_except'] += 1;
							return $pyjs_kwargs_call(null, $m['formatString'], null, null, [{'prefix':(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['list'](['rel']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_62_err){if (!$p['isinstance']($pyjs_dbg_62_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_62_err);}throw $pyjs_dbg_62_err;
}})()}, (function(){try{try{$pyjs['in_try_except'] += 1;
							return $pyjs_kwargs_call(null, $m['formatString'], null, null, [{'prefix':(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['list'](['dest']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_60_err){if (!$p['isinstance']($pyjs_dbg_60_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_60_err);}throw $pyjs_dbg_60_err;
}})()}, $p['getattr'](self, 'format'), $p['getattr'](self, 'structure'), val['__getitem__']('dest')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_61_err){if (!$p['isinstance']($pyjs_dbg_61_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_61_err);}throw $pyjs_dbg_61_err;
}})(), relStructDict, val['__getitem__']('rel')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_63_err){if (!$p['isinstance']($pyjs_dbg_63_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_63_err);}throw $pyjs_dbg_63_err;
}})())?$or9:val['__getitem__']('id'));
						}
					} finally { $pyjs['in_try_except'] -= 1; }
				} catch($pyjs_try_err) {
					$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_1 - 1);
					$pyjs['__active_exception_stack__'] = null;
					$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
					var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
					$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.extendenrelational';
					if (true) {
						$pyjs['track']['lineno']=73;
						res = '';
					}
				}
				$pyjs['track']['lineno']=74;
				$pyjs['track']['lineno']=74;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Label'](res);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['data'],['field']]);
			$cls_definition['render'] = $method;
			$pyjs['track']['lineno']=43;
			var $bases = new Array($p['object']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('ExtendedRelationalViewBoneDelegate', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=76;
		$m['ExtendedRelationalSelectionBoneEntry'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.extendenrelational';
			$cls_definition['__md5__'] = '883299a16528f6c2b67613ae55cd192c';
			$pyjs['track']['lineno']=82;
			$method = $pyjs__bind_method2('__init__', function(parent, modul, data, using, errorInfo) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,5,arguments['length']-1));

					var kwargs = arguments['length'] >= 6 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 5) $pyjs__exception_func_param(arguments['callee']['__name__'], 6, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					parent = arguments[1];
					modul = arguments[2];
					data = arguments[3];
					using = arguments[4];
					errorInfo = arguments[5];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,6,arguments['length']-1));

					var kwargs = arguments['length'] >= 7 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 6) $pyjs__exception_func_param(arguments['callee']['__name__'], 6, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '883299a16528f6c2b67613ae55cd192c') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof errorInfo != 'undefined') {
						if (errorInfo !== null && typeof errorInfo['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = errorInfo;
							errorInfo = arguments[6];
						}
					} else 					if (typeof using != 'undefined') {
						if (using !== null && typeof using['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = using;
							using = arguments[6];
						}
					} else 					if (typeof data != 'undefined') {
						if (data !== null && typeof data['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = data;
							data = arguments[6];
						}
					} else 					if (typeof modul != 'undefined') {
						if (modul !== null && typeof modul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modul;
							modul = arguments[6];
						}
					} else 					if (typeof parent != 'undefined') {
						if (parent !== null && typeof parent['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = parent;
							parent = arguments[6];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[6];
						}
					} else {
					}
				}
				var txtLbl,wrapperDiv,remBtn,$and1,$and2;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':82};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=82;
				$pyjs['track']['lineno']=91;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ExtendedRelationalSelectionBoneEntry'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_65_err){if (!$p['isinstance']($pyjs_dbg_65_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_65_err);}throw $pyjs_dbg_65_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_66_err){if (!$p['isinstance']($pyjs_dbg_66_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_66_err);}throw $pyjs_dbg_66_err;
}})();
				$pyjs['track']['lineno']=92;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('parent', parent) : $p['setattr'](self, 'parent', parent); 
				$pyjs['track']['lineno']=93;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=94;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('data', data) : $p['setattr'](self, 'data', data); 
				$pyjs['track']['lineno']=95;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and1=using)?(function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})()['__contains__']('dest'):$and1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_68_err){if (!$p['isinstance']($pyjs_dbg_68_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_68_err);}throw $pyjs_dbg_68_err;
}})()) {
					$pyjs['track']['lineno']=96;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return data['__getitem__']('dest')['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_69_err){if (!$p['isinstance']($pyjs_dbg_69_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_69_err);}throw $pyjs_dbg_69_err;
}})()['__contains__']('name'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_70_err){if (!$p['isinstance']($pyjs_dbg_70_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_70_err);}throw $pyjs_dbg_70_err;
}})()) {
						$pyjs['track']['lineno']=97;
						txtLbl = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Label'](data['__getitem__']('dest')['__getitem__']('name'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_71_err){if (!$p['isinstance']($pyjs_dbg_71_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_71_err);}throw $pyjs_dbg_71_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=99;
						txtLbl = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Label'](data['__getitem__']('dest')['__getitem__']('id'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_72_err){if (!$p['isinstance']($pyjs_dbg_72_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_72_err);}throw $pyjs_dbg_72_err;
}})();
					}
				}
				else {
					$pyjs['track']['lineno']=101;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_73_err){if (!$p['isinstance']($pyjs_dbg_73_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_73_err);}throw $pyjs_dbg_73_err;
}})()['__contains__']('name'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_74_err){if (!$p['isinstance']($pyjs_dbg_74_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_74_err);}throw $pyjs_dbg_74_err;
}})()) {
						$pyjs['track']['lineno']=102;
						txtLbl = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Label'](data['__getitem__']('name'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=104;
						txtLbl = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Label'](data['__getitem__']('id'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})();
					}
				}
				$pyjs['track']['lineno']=105;
				wrapperDiv = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_77_err){if (!$p['isinstance']($pyjs_dbg_77_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_77_err);}throw $pyjs_dbg_77_err;
}})();
				$pyjs['track']['lineno']=106;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return wrapperDiv['appendChild'](txtLbl);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_78_err){if (!$p['isinstance']($pyjs_dbg_78_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_78_err);}throw $pyjs_dbg_78_err;
}})();
				$pyjs['track']['lineno']=107;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return wrapperDiv['__getitem__']('class')['append']('labelwrapper');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_79_err){if (!$p['isinstance']($pyjs_dbg_79_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_79_err);}throw $pyjs_dbg_79_err;
}})();
				$pyjs['track']['lineno']=109;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](parent, 'readOnly')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_80_err){if (!$p['isinstance']($pyjs_dbg_80_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_80_err);}throw $pyjs_dbg_80_err;
}})()) {
					$pyjs['track']['lineno']=110;
					remBtn = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['ext']['Button']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Remove');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_81_err){if (!$p['isinstance']($pyjs_dbg_81_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_81_err);}throw $pyjs_dbg_81_err;
}})(), $p['getattr'](self, 'onRemove'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_82_err){if (!$p['isinstance']($pyjs_dbg_82_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_82_err);}throw $pyjs_dbg_82_err;
}})();
					$pyjs['track']['lineno']=111;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return remBtn['__getitem__']('class')['append']('icon');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_83_err){if (!$p['isinstance']($pyjs_dbg_83_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_83_err);}throw $pyjs_dbg_83_err;
}})();
					$pyjs['track']['lineno']=112;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return remBtn['__getitem__']('class')['append']('cancel');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_84_err){if (!$p['isinstance']($pyjs_dbg_84_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_84_err);}throw $pyjs_dbg_84_err;
}})();
					$pyjs['track']['lineno']=113;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return wrapperDiv['appendChild'](remBtn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})();
				}
				$pyjs['track']['lineno']=115;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](wrapperDiv);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})();
				$pyjs['track']['lineno']=117;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](using);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})()) {
					$pyjs['track']['lineno']=118;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('ie', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call(null, $m['InternalEdit'], null, null, [{'readOnly':$p['getattr'](parent, 'readOnly')}, using, data['__getitem__']('rel'), errorInfo]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})()) : $p['setattr'](self, 'ie', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call(null, $m['InternalEdit'], null, null, [{'readOnly':$p['getattr'](parent, 'readOnly')}, using, data['__getitem__']('rel'), errorInfo]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})()); 
					$pyjs['track']['lineno']=119;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'ie'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_89_err){if (!$p['isinstance']($pyjs_dbg_89_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_89_err);}throw $pyjs_dbg_89_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=121;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('ie', null) : $p['setattr'](self, 'ie', null); 
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['parent'],['modul'],['data'],['using'],['errorInfo']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=124;
			$method = $pyjs__bind_method2('onRemove', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '883299a16528f6c2b67613ae55cd192c') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':124};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=124;
				$pyjs['track']['lineno']=125;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']['removeEntry'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['onRemove'] = $method;
			$pyjs['track']['lineno']=127;
			$method = $pyjs__bind_method2('serialize', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '883299a16528f6c2b67613ae55cd192c') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var res;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':127};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=127;
				$pyjs['track']['lineno']=128;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'ie'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_91_err){if (!$p['isinstance']($pyjs_dbg_91_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_91_err);}throw $pyjs_dbg_91_err;
}})()) {
					$pyjs['track']['lineno']=129;
					res = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_92_err){if (!$p['isinstance']($pyjs_dbg_92_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_92_err);}throw $pyjs_dbg_92_err;
}})();
					$pyjs['track']['lineno']=130;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return res['update']((function(){try{try{$pyjs['in_try_except'] += 1;
					return self['ie']['doSave']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_93_err){if (!$p['isinstance']($pyjs_dbg_93_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_93_err);}throw $pyjs_dbg_93_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_94_err){if (!$p['isinstance']($pyjs_dbg_94_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_94_err);}throw $pyjs_dbg_94_err;
}})();
					$pyjs['track']['lineno']=131;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return res['__setitem__']('id', $p['getattr'](self, 'data')['__getitem__']('dest')['__getitem__']('id'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_95_err){if (!$p['isinstance']($pyjs_dbg_95_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_95_err);}throw $pyjs_dbg_95_err;
}})();
					$pyjs['track']['lineno']=132;
					$pyjs['track']['lineno']=132;
					var $pyjs__ret = res;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				else {
					$pyjs['track']['lineno']=134;
					$pyjs['track']['lineno']=134;
					var $pyjs__ret = $p['getattr'](self, 'data')['__getitem__']('id');
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['serialize'] = $method;
			$pyjs['track']['lineno']=76;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('ExtendedRelationalSelectionBoneEntry', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=137;
		$m['ExtendedRelationalSelectionBone'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.extendenrelational';
			$cls_definition['__md5__'] = '047f802cbdae9e21bd0dab93b6d1f7e0';
			$pyjs['track']['lineno']=142;
			$method = $pyjs__bind_method2('__init__', function(srcModul, boneName, readOnly, destModul, format, using) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,6,arguments['length']-1));

					var kwargs = arguments['length'] >= 7 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 5, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					srcModul = arguments[1];
					boneName = arguments[2];
					readOnly = arguments[3];
					destModul = arguments[4];
					format = arguments[5];
					using = arguments[6];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,7,arguments['length']-1));

					var kwargs = arguments['length'] >= 8 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 5) $pyjs__exception_func_param(arguments['callee']['__name__'], 5, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '047f802cbdae9e21bd0dab93b6d1f7e0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof using != 'undefined') {
						if (using !== null && typeof using['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = using;
							using = arguments[7];
						}
					} else 					if (typeof format != 'undefined') {
						if (format !== null && typeof format['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = format;
							format = arguments[7];
						}
					} else 					if (typeof destModul != 'undefined') {
						if (destModul !== null && typeof destModul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = destModul;
							destModul = arguments[7];
						}
					} else 					if (typeof readOnly != 'undefined') {
						if (readOnly !== null && typeof readOnly['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = readOnly;
							readOnly = arguments[7];
						}
					} else 					if (typeof boneName != 'undefined') {
						if (boneName !== null && typeof boneName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = boneName;
							boneName = arguments[7];
						}
					} else 					if (typeof srcModul != 'undefined') {
						if (srcModul !== null && typeof srcModul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = srcModul;
							srcModul = arguments[7];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[7];
						}
					} else {
					}
				}
				if (typeof format == 'undefined') format=arguments['callee']['__args__'][7][1];
				if (typeof using == 'undefined') using=arguments['callee']['__args__'][8][1];
				var $add6,$add5,$or12,$or11;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':142};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=142;
				$pyjs['track']['lineno']=155;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['printFunc'](['---------- EXTENDED RELATIONAL SELECTION UP---------------'], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_96_err){if (!$p['isinstance']($pyjs_dbg_96_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_96_err);}throw $pyjs_dbg_96_err;
}})();
				$pyjs['track']['lineno']=156;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ExtendedRelationalSelectionBone'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_97_err){if (!$p['isinstance']($pyjs_dbg_97_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_97_err);}throw $pyjs_dbg_97_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_98_err){if (!$p['isinstance']($pyjs_dbg_98_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_98_err);}throw $pyjs_dbg_98_err;
}})();
				$pyjs['track']['lineno']=159;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('srcModul', srcModul) : $p['setattr'](self, 'srcModul', srcModul); 
				$pyjs['track']['lineno']=160;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('boneName', boneName) : $p['setattr'](self, 'boneName', boneName); 
				$pyjs['track']['lineno']=161;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('readOnly', readOnly) : $p['setattr'](self, 'readOnly', readOnly); 
				$pyjs['track']['lineno']=162;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('destModul', destModul) : $p['setattr'](self, 'destModul', destModul); 
				$pyjs['track']['lineno']=163;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('format', format) : $p['setattr'](self, 'format', format); 
				$pyjs['track']['lineno']=164;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('using', using) : $p['setattr'](self, 'using', using); 
				$pyjs['track']['lineno']=165;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('entries', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_99_err){if (!$p['isinstance']($pyjs_dbg_99_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_99_err);}throw $pyjs_dbg_99_err;
}})()) : $p['setattr'](self, 'entries', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_99_err){if (!$p['isinstance']($pyjs_dbg_99_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_99_err);}throw $pyjs_dbg_99_err;
}})()); 
				$pyjs['track']['lineno']=166;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('extendedErrorInformation', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_100_err){if (!$p['isinstance']($pyjs_dbg_100_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_100_err);}throw $pyjs_dbg_100_err;
}})()) : $p['setattr'](self, 'extendedErrorInformation', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_100_err){if (!$p['isinstance']($pyjs_dbg_100_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_100_err);}throw $pyjs_dbg_100_err;
}})()); 
				$pyjs['track']['lineno']=167;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectionDiv', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_101_err){if (!$p['isinstance']($pyjs_dbg_101_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_101_err);}throw $pyjs_dbg_101_err;
}})()) : $p['setattr'](self, 'selectionDiv', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_101_err){if (!$p['isinstance']($pyjs_dbg_101_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_101_err);}throw $pyjs_dbg_101_err;
}})()); 
				$pyjs['track']['lineno']=168;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'selectionDiv')['__getitem__']('class')['append']('selectioncontainer');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_102_err){if (!$p['isinstance']($pyjs_dbg_102_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_102_err);}throw $pyjs_dbg_102_err;
}})();
				$pyjs['track']['lineno']=169;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'selectionDiv'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_103_err){if (!$p['isinstance']($pyjs_dbg_103_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_103_err);}throw $pyjs_dbg_103_err;
}})();
				$pyjs['track']['lineno']=171;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($or11=$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']('root'))?$or11:$m['conf']['__getitem__']('currentUser')['__getitem__']('access')['__contains__']($p['__op_add']($add5=destModul,$add6='-view'))));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_104_err){if (!$p['isinstance']($pyjs_dbg_104_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_104_err);}throw $pyjs_dbg_104_err;
}})()) {
					$pyjs['track']['lineno']=173;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['ext']['Button']('Select', $p['getattr'](self, 'onShowSelector'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_105_err){if (!$p['isinstance']($pyjs_dbg_105_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_105_err);}throw $pyjs_dbg_105_err;
}})()) : $p['setattr'](self, 'selectBtn', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['ext']['Button']('Select', $p['getattr'](self, 'onShowSelector'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_105_err){if (!$p['isinstance']($pyjs_dbg_105_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_105_err);}throw $pyjs_dbg_105_err;
}})()); 
					$pyjs['track']['lineno']=174;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'selectBtn')['__getitem__']('class')['append']('icon');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_106_err){if (!$p['isinstance']($pyjs_dbg_106_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_106_err);}throw $pyjs_dbg_106_err;
}})();
					$pyjs['track']['lineno']=175;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'selectBtn')['__getitem__']('class')['append']('select');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_107_err){if (!$p['isinstance']($pyjs_dbg_107_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_107_err);}throw $pyjs_dbg_107_err;
}})();
					$pyjs['track']['lineno']=176;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'selectBtn'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_108_err){if (!$p['isinstance']($pyjs_dbg_108_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_108_err);}throw $pyjs_dbg_108_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=178;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('selectBtn', null) : $p['setattr'](self, 'selectBtn', null); 
				}
				$pyjs['track']['lineno']=180;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'readOnly'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_109_err){if (!$p['isinstance']($pyjs_dbg_109_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_109_err);}throw $pyjs_dbg_109_err;
}})()) {
					$pyjs['track']['lineno']=181;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__setitem__']('disabled', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_110_err){if (!$p['isinstance']($pyjs_dbg_110_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_110_err);}throw $pyjs_dbg_110_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['srcModul'],['boneName'],['readOnly'],['destModul'],['format', '$(name)'],['using', null]]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=183;
			$method = $pyjs__bind_method2('_setDisabled', function(disable) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					disable = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '047f802cbdae9e21bd0dab93b6d1f7e0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and4,$and5,$and3;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':183};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=183;
				$pyjs['track']['lineno']=187;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ExtendedRelationalSelectionBone'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_111_err){if (!$p['isinstance']($pyjs_dbg_111_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_111_err);}throw $pyjs_dbg_111_err;
}})()['_setDisabled'](disable);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_112_err){if (!$p['isinstance']($pyjs_dbg_112_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_112_err);}throw $pyjs_dbg_112_err;
}})();
				$pyjs['track']['lineno']=188;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and3=!$p['bool'](disable))?($p['bool']($and4=!$p['bool']($p['getattr'](self, '_disabledState')))?(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_113_err){if (!$p['isinstance']($pyjs_dbg_113_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_113_err);}throw $pyjs_dbg_113_err;
}})()['__getitem__']('class')['__contains__']('is_active'):$and4):$and3));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_114_err){if (!$p['isinstance']($pyjs_dbg_114_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_114_err);}throw $pyjs_dbg_114_err;
}})()) {
					$pyjs['track']['lineno']=189;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_115_err){if (!$p['isinstance']($pyjs_dbg_115_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_115_err);}throw $pyjs_dbg_115_err;
}})()['__getitem__']('class')['remove']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_116_err){if (!$p['isinstance']($pyjs_dbg_116_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_116_err);}throw $pyjs_dbg_116_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['disable']]);
			$cls_definition['_setDisabled'] = $method;
			$pyjs['track']['lineno']=193;
			$method = $pyjs__bind_method2('fromSkelStructure', function(modulName, boneName, skelStructure) {
    if ($pyjs['options']['arg_is_instance'] && this['__is_instance__'] !== true && this['__is_instance__'] !== false) $pyjs__exception_func_class_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__']);
    if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 3, arguments['length']);
    var cls = this['prototype'];
				var multiple,format,destModul,$and6,$and7,readOnly,using;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':193};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=193;
				$pyjs['track']['lineno']=203;
				readOnly = ($p['bool']($and6=(function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_117_err){if (!$p['isinstance']($pyjs_dbg_117_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_117_err);}throw $pyjs_dbg_117_err;
}})()['__contains__']('readonly'))?skelStructure['__getitem__'](boneName)['__getitem__']('readonly'):$and6);
				$pyjs['track']['lineno']=204;
				multiple = skelStructure['__getitem__'](boneName)['__getitem__']('multiple');
				$pyjs['track']['lineno']=205;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_118_err){if (!$p['isinstance']($pyjs_dbg_118_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_118_err);}throw $pyjs_dbg_118_err;
}})()['__contains__']('modul'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_119_err){if (!$p['isinstance']($pyjs_dbg_119_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_119_err);}throw $pyjs_dbg_119_err;
}})()) {
					$pyjs['track']['lineno']=206;
					destModul = skelStructure['__getitem__'](boneName)['__getitem__']('modul');
				}
				else {
					$pyjs['track']['lineno']=208;
					destModul = (function(){try{try{$pyjs['in_try_except'] += 1;
					return skelStructure['__getitem__'](boneName)['__getitem__']('type')['$$split']('.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_120_err){if (!$p['isinstance']($pyjs_dbg_120_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_120_err);}throw $pyjs_dbg_120_err;
}})()['__getitem__']($constant_int_1);
				}
				$pyjs['track']['lineno']=209;
				format = '$(name)';
				$pyjs['track']['lineno']=210;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_121_err){if (!$p['isinstance']($pyjs_dbg_121_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_121_err);}throw $pyjs_dbg_121_err;
}})()['__contains__']('format'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_122_err){if (!$p['isinstance']($pyjs_dbg_122_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_122_err);}throw $pyjs_dbg_122_err;
}})()) {
					$pyjs['track']['lineno']=211;
					format = skelStructure['__getitem__'](boneName)['__getitem__']('format');
				}
				$pyjs['track']['lineno']=212;
				using = null;
				$pyjs['track']['lineno']=213;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_123_err){if (!$p['isinstance']($pyjs_dbg_123_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_123_err);}throw $pyjs_dbg_123_err;
}})()['__contains__']('using'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_124_err){if (!$p['isinstance']($pyjs_dbg_124_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_124_err);}throw $pyjs_dbg_124_err;
}})()) {
					$pyjs['track']['lineno']=214;
					using = skelStructure['__getitem__'](boneName)['__getitem__']('using');
				}
				$pyjs['track']['lineno']=215;
				$pyjs['track']['lineno']=215;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, cls, null, null, [{'destModul':destModul, 'format':format, 'using':using}, modulName, boneName, readOnly]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_125_err){if (!$p['isinstance']($pyjs_dbg_125_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_125_err);}throw $pyjs_dbg_125_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 2, [null,null,['cls'],['modulName'],['boneName'],['skelStructure']]);
			$cls_definition['fromSkelStructure'] = $method;
			$pyjs['track']['lineno']=217;
			$method = $pyjs__bind_method2('unserialize', function(data) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '047f802cbdae9e21bd0dab93b6d1f7e0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var val;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':217};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=217;
				$pyjs['track']['lineno']=223;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_126_err){if (!$p['isinstance']($pyjs_dbg_126_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_126_err);}throw $pyjs_dbg_126_err;
}})()['__contains__']($p['getattr'](self, 'boneName')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_127_err){if (!$p['isinstance']($pyjs_dbg_127_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_127_err);}throw $pyjs_dbg_127_err;
}})()) {
					$pyjs['track']['lineno']=224;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['printFunc']([(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['tuple'](['USERIALIZING', data['__getitem__']($p['getattr'](self, 'boneName'))]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_128_err){if (!$p['isinstance']($pyjs_dbg_128_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_128_err);}throw $pyjs_dbg_128_err;
}})()], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_129_err){if (!$p['isinstance']($pyjs_dbg_129_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_129_err);}throw $pyjs_dbg_129_err;
}})();
					$pyjs['track']['lineno']=225;
					val = data['__getitem__']($p['getattr'](self, 'boneName'));
					$pyjs['track']['lineno']=226;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](val, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_130_err){if (!$p['isinstance']($pyjs_dbg_130_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_130_err);}throw $pyjs_dbg_130_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_131_err){if (!$p['isinstance']($pyjs_dbg_131_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_131_err);}throw $pyjs_dbg_131_err;
}})()) {
						$pyjs['track']['lineno']=227;
						val = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['list']([val]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_132_err){if (!$p['isinstance']($pyjs_dbg_132_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_132_err);}throw $pyjs_dbg_132_err;
}})();
					}
					$pyjs['track']['lineno']=228;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['setSelection'](val);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_133_err){if (!$p['isinstance']($pyjs_dbg_133_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_133_err);}throw $pyjs_dbg_133_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['data']]);
			$cls_definition['unserialize'] = $method;
			$pyjs['track']['lineno']=232;
			$method = $pyjs__bind_method2('serializeForPost', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '047f802cbdae9e21bd0dab93b6d1f7e0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var v,$iter6_idx,idx,$iter6_type,res,$iter7_array,currRes,$add8,$iter6_array,k,$pyjs__trackstack_size_2,$add7,$iter7_idx,$pyjs__trackstack_size_1,entry,$iter6_iter,$iter7_type,$iter7_nextval,$iter7_iter,$iter6_nextval;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':232};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=232;
				$pyjs['track']['lineno']=238;
				res = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_134_err){if (!$p['isinstance']($pyjs_dbg_134_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_134_err);}throw $pyjs_dbg_134_err;
}})();
				$pyjs['track']['lineno']=239;
				idx = $constant_int_0;
				$pyjs['track']['lineno']=240;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter6_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'entries');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_135_err){if (!$p['isinstance']($pyjs_dbg_135_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_135_err);}throw $pyjs_dbg_135_err;
}})();
				$iter6_nextval=$p['__iter_prepare']($iter6_iter,false);
				while (typeof($p['__wrapped_next']($iter6_nextval)['$nextval']) != 'undefined') {
					entry = $iter6_nextval['$nextval'];
					$pyjs['track']['lineno']=241;
					currRes = (function(){try{try{$pyjs['in_try_except'] += 1;
					return entry['serialize']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_136_err){if (!$p['isinstance']($pyjs_dbg_136_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_136_err);}throw $pyjs_dbg_136_err;
}})();
					$pyjs['track']['lineno']=242;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](currRes, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_137_err){if (!$p['isinstance']($pyjs_dbg_137_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_137_err);}throw $pyjs_dbg_137_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_138_err){if (!$p['isinstance']($pyjs_dbg_138_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_138_err);}throw $pyjs_dbg_138_err;
}})()) {
						$pyjs['track']['lineno']=243;
						$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
						$iter7_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return currRes['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_140_err){if (!$p['isinstance']($pyjs_dbg_140_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_140_err);}throw $pyjs_dbg_140_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_141_err){if (!$p['isinstance']($pyjs_dbg_141_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_141_err);}throw $pyjs_dbg_141_err;
}})();
						$iter7_nextval=$p['__iter_prepare']($iter7_iter,false);
						while (typeof($p['__wrapped_next']($iter7_nextval)['$nextval']) != 'undefined') {
							var $tupleassign3 = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['__ass_unpack']($iter7_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_139_err){if (!$p['isinstance']($pyjs_dbg_139_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_139_err);}throw $pyjs_dbg_139_err;
}})();
							k = $tupleassign3[0];
							v = $tupleassign3[1];
							$pyjs['track']['lineno']=244;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return res['__setitem__']((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['sprintf']('%s%s.%s', (function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['tuple']([$p['getattr'](self, 'boneName'), idx, k]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_142_err){if (!$p['isinstance']($pyjs_dbg_142_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_142_err);}throw $pyjs_dbg_142_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_143_err){if (!$p['isinstance']($pyjs_dbg_143_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_143_err);}throw $pyjs_dbg_143_err;
}})(), v);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_144_err){if (!$p['isinstance']($pyjs_dbg_144_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_144_err);}throw $pyjs_dbg_144_err;
}})();
						}
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='bones.extendenrelational';
					}
					else {
						$pyjs['track']['lineno']=246;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return res['__setitem__']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('%s%s.id', (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['tuple']([$p['getattr'](self, 'boneName'), idx]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_145_err){if (!$p['isinstance']($pyjs_dbg_145_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_145_err);}throw $pyjs_dbg_145_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_146_err){if (!$p['isinstance']($pyjs_dbg_146_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_146_err);}throw $pyjs_dbg_146_err;
}})(), currRes);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_147_err){if (!$p['isinstance']($pyjs_dbg_147_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_147_err);}throw $pyjs_dbg_147_err;
}})();
					}
					$pyjs['track']['lineno']=247;
					idx = $p['__op_add']($add7=idx,$add8=$constant_int_1);
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=248;
				$pyjs['track']['lineno']=248;
				var $pyjs__ret = res;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['serializeForPost'] = $method;
			$pyjs['track']['lineno']=251;
			$method = $pyjs__bind_method2('serializeForDocument', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '047f802cbdae9e21bd0dab93b6d1f7e0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':251};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=251;
				$pyjs['track']['lineno']=252;
				$pyjs['track']['lineno']=252;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['serialize']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_148_err){if (!$p['isinstance']($pyjs_dbg_148_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_148_err);}throw $pyjs_dbg_148_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['serializeForDocument'] = $method;
			$pyjs['track']['lineno']=254;
			$method = $pyjs__bind_method2('onShowSelector', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '047f802cbdae9e21bd0dab93b6d1f7e0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var currentSelector;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':254};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=254;
				$pyjs['track']['lineno']=258;
				currentSelector = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['ListWidget'], null, null, [{'isSelector':true}, $p['getattr'](self, 'destModul')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_149_err){if (!$p['isinstance']($pyjs_dbg_149_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_149_err);}throw $pyjs_dbg_149_err;
}})();
				$pyjs['track']['lineno']=259;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return currentSelector['selectionActivatedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_150_err){if (!$p['isinstance']($pyjs_dbg_150_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_150_err);}throw $pyjs_dbg_150_err;
}})();
				$pyjs['track']['lineno']=260;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['stackWidget'](currentSelector);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_151_err){if (!$p['isinstance']($pyjs_dbg_151_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_151_err);}throw $pyjs_dbg_151_err;
}})();
				$pyjs['track']['lineno']=261;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_152_err){if (!$p['isinstance']($pyjs_dbg_152_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_152_err);}throw $pyjs_dbg_152_err;
}})()['__getitem__']('class')['append']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_153_err){if (!$p['isinstance']($pyjs_dbg_153_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_153_err);}throw $pyjs_dbg_153_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['onShowSelector'] = $method;
			$pyjs['track']['lineno']=263;
			$method = $pyjs__bind_method2('onSelectionActivated', function(table, selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					selection = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '047f802cbdae9e21bd0dab93b6d1f7e0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':263};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=263;
				$pyjs['track']['lineno']=267;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'using'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_154_err){if (!$p['isinstance']($pyjs_dbg_154_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_154_err);}throw $pyjs_dbg_154_err;
}})()) {
					$pyjs['track']['lineno']=268;
					selection = function(){
						var $iter8_idx,$collcomp6,$iter8_type,$iter8_array,$iter8_iter,$iter8_nextval,$pyjs__trackstack_size_1,data;
	$collcomp6 = $p['list']();
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter8_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return selection;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_155_err){if (!$p['isinstance']($pyjs_dbg_155_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_155_err);}throw $pyjs_dbg_155_err;
}})();
					$iter8_nextval=$p['__iter_prepare']($iter8_iter,false);
					while (typeof($p['__wrapped_next']($iter8_nextval)['$nextval']) != 'undefined') {
						data = $iter8_nextval['$nextval'];
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $collcomp6['append']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['dict']([['dest', data], ['rel', (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_156_err){if (!$p['isinstance']($pyjs_dbg_156_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_156_err);}throw $pyjs_dbg_156_err;
}})()]]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_157_err){if (!$p['isinstance']($pyjs_dbg_157_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_157_err);}throw $pyjs_dbg_157_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_158_err){if (!$p['isinstance']($pyjs_dbg_158_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_158_err);}throw $pyjs_dbg_158_err;
}})();
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.extendenrelational';

	return $collcomp6;}();
				}
				$pyjs['track']['lineno']=269;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['setSelection'](selection);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_159_err){if (!$p['isinstance']($pyjs_dbg_159_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_159_err);}throw $pyjs_dbg_159_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['selection']]);
			$cls_definition['onSelectionActivated'] = $method;
			$pyjs['track']['lineno']=271;
			$method = $pyjs__bind_method2('setSelection', function(selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					selection = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '047f802cbdae9e21bd0dab93b6d1f7e0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter10_nextval,$iter10_iter,errKey,$iter9_iter,$iter9_nextval,$iter9_idx,errIdx,$iter9_type,$pyjs__trackstack_size_1,data,errDict,idx,k,$iter10_array,$iter9_array,$pyjs__trackstack_size_2,v,$iter10_type,entry,$iter10_idx;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':271};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=271;
				$pyjs['track']['lineno']=277;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_is'](selection, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_160_err){if (!$p['isinstance']($pyjs_dbg_160_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_160_err);}throw $pyjs_dbg_160_err;
}})()) {
					$pyjs['track']['lineno']=278;
					$pyjs['track']['lineno']=278;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=279;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter9_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return selection;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_161_err){if (!$p['isinstance']($pyjs_dbg_161_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_161_err);}throw $pyjs_dbg_161_err;
}})();
				$iter9_nextval=$p['__iter_prepare']($iter9_iter,false);
				while (typeof($p['__wrapped_next']($iter9_nextval)['$nextval']) != 'undefined') {
					data = $iter9_nextval['$nextval'];
					$pyjs['track']['lineno']=280;
					errIdx = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['len']($p['getattr'](self, 'entries'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_162_err){if (!$p['isinstance']($pyjs_dbg_162_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_162_err);}throw $pyjs_dbg_162_err;
}})();
					$pyjs['track']['lineno']=281;
					errDict = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_163_err){if (!$p['isinstance']($pyjs_dbg_163_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_163_err);}throw $pyjs_dbg_163_err;
}})();
					$pyjs['track']['lineno']=282;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'extendedErrorInformation'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_164_err){if (!$p['isinstance']($pyjs_dbg_164_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_164_err);}throw $pyjs_dbg_164_err;
}})()) {
						$pyjs['track']['lineno']=283;
						$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
						$iter10_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return self['extendedErrorInformation']['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_166_err){if (!$p['isinstance']($pyjs_dbg_166_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_166_err);}throw $pyjs_dbg_166_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_167_err){if (!$p['isinstance']($pyjs_dbg_167_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_167_err);}throw $pyjs_dbg_167_err;
}})();
						$iter10_nextval=$p['__iter_prepare']($iter10_iter,false);
						while (typeof($p['__wrapped_next']($iter10_nextval)['$nextval']) != 'undefined') {
							var $tupleassign4 = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['__ass_unpack']($iter10_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_165_err){if (!$p['isinstance']($pyjs_dbg_165_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_165_err);}throw $pyjs_dbg_165_err;
}})();
							k = $tupleassign4[0];
							v = $tupleassign4[1];
							$pyjs['track']['lineno']=284;
							k = (function(){try{try{$pyjs['in_try_except'] += 1;
							return k['$$replace']((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['sprintf']('%s.', $p['getattr'](self, 'boneName'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_168_err){if (!$p['isinstance']($pyjs_dbg_168_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_168_err);}throw $pyjs_dbg_168_err;
}})(), '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_169_err){if (!$p['isinstance']($pyjs_dbg_169_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_169_err);}throw $pyjs_dbg_169_err;
}})();
							$pyjs['track']['lineno']=285;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool']($constant_int_1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_170_err){if (!$p['isinstance']($pyjs_dbg_170_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_170_err);}throw $pyjs_dbg_170_err;
}})()) {
								$pyjs['track']['lineno']=286;
								var $tupleassign5 = (function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['__ass_unpack']((function(){try{try{$pyjs['in_try_except'] += 1;
								return k['$$split']('.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_171_err){if (!$p['isinstance']($pyjs_dbg_171_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_171_err);}throw $pyjs_dbg_171_err;
}})(), 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_172_err){if (!$p['isinstance']($pyjs_dbg_172_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_172_err);}throw $pyjs_dbg_172_err;
}})();
								idx = $tupleassign5[0];
								errKey = $tupleassign5[1];
								$pyjs['track']['lineno']=287;
								idx = (function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['int'](idx);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_173_err){if (!$p['isinstance']($pyjs_dbg_173_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_173_err);}throw $pyjs_dbg_173_err;
}})();
							}
							else {
								$pyjs['track']['lineno']=289;
								continue;
							}
							$pyjs['track']['lineno']=290;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool']($p['op_eq'](idx, errIdx));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_174_err){if (!$p['isinstance']($pyjs_dbg_174_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_174_err);}throw $pyjs_dbg_174_err;
}})()) {
								$pyjs['track']['lineno']=291;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return errDict['__setitem__'](errKey, v);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_175_err){if (!$p['isinstance']($pyjs_dbg_175_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_175_err);}throw $pyjs_dbg_175_err;
}})();
							}
						}
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='bones.extendenrelational';
					}
					$pyjs['track']['lineno']=292;
					entry = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['ExtendedRelationalSelectionBoneEntry'](self, $p['getattr'](self, 'destModul'), data, $p['getattr'](self, 'using'), errDict);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_176_err){if (!$p['isinstance']($pyjs_dbg_176_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_176_err);}throw $pyjs_dbg_176_err;
}})();
					$pyjs['track']['lineno']=293;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['addEntry'](entry);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_177_err){if (!$p['isinstance']($pyjs_dbg_177_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_177_err);}throw $pyjs_dbg_177_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['selection']]);
			$cls_definition['setSelection'] = $method;
			$pyjs['track']['lineno']=295;
			$method = $pyjs__bind_method2('addEntry', function(entry) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					entry = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '047f802cbdae9e21bd0dab93b6d1f7e0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':295};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=295;
				$pyjs['track']['lineno']=300;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['printFunc'](['--adding entry--'], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_178_err){if (!$p['isinstance']($pyjs_dbg_178_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_178_err);}throw $pyjs_dbg_178_err;
}})();
				$pyjs['track']['lineno']=301;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['entries']['append'](entry);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_179_err){if (!$p['isinstance']($pyjs_dbg_179_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_179_err);}throw $pyjs_dbg_179_err;
}})();
				$pyjs['track']['lineno']=302;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionDiv']['appendChild'](entry);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_180_err){if (!$p['isinstance']($pyjs_dbg_180_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_180_err);}throw $pyjs_dbg_180_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['entry']]);
			$cls_definition['addEntry'] = $method;
			$pyjs['track']['lineno']=304;
			$method = $pyjs__bind_method2('removeEntry', function(entry) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					entry = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '047f802cbdae9e21bd0dab93b6d1f7e0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':304};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=304;
				$pyjs['track']['lineno']=309;
				if (!( $p['getattr'](self, 'entries')['__contains__'](entry) )) {
				   throw $p['AssertionError']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('Cannot remove unknown entry %s from realtionalBone', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['str'](entry);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_181_err){if (!$p['isinstance']($pyjs_dbg_181_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_181_err);}throw $pyjs_dbg_181_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_182_err){if (!$p['isinstance']($pyjs_dbg_182_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_182_err);}throw $pyjs_dbg_182_err;
}})());
				 }
				$pyjs['track']['lineno']=310;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['selectionDiv']['removeChild'](entry);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_183_err){if (!$p['isinstance']($pyjs_dbg_183_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_183_err);}throw $pyjs_dbg_183_err;
}})();
				$pyjs['track']['lineno']=311;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['entries']['remove'](entry);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_184_err){if (!$p['isinstance']($pyjs_dbg_184_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_184_err);}throw $pyjs_dbg_184_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['entry']]);
			$cls_definition['removeEntry'] = $method;
			$pyjs['track']['lineno']=313;
			$method = $pyjs__bind_method2('setExtendedErrorInformation', function(errorInfo) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					errorInfo = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '047f802cbdae9e21bd0dab93b6d1f7e0') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and8,$and9,$iter11_idx,err,idx,$iter11_iter,$iter11_type,v,$iter11_array,$iter11_nextval,$pyjs__trackstack_size_1,k;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':313};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=313;
				$pyjs['track']['lineno']=314;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['printFunc'](['------- EXTENDEND ERROR INFO --------'], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_185_err){if (!$p['isinstance']($pyjs_dbg_185_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_185_err);}throw $pyjs_dbg_185_err;
}})();
				$pyjs['track']['lineno']=315;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['printFunc']([errorInfo], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_186_err){if (!$p['isinstance']($pyjs_dbg_186_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_186_err);}throw $pyjs_dbg_186_err;
}})();
				$pyjs['track']['lineno']=316;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('extendedErrorInformation', errorInfo) : $p['setattr'](self, 'extendedErrorInformation', errorInfo); 
				$pyjs['track']['lineno']=317;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter11_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return errorInfo['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_188_err){if (!$p['isinstance']($pyjs_dbg_188_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_188_err);}throw $pyjs_dbg_188_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_189_err){if (!$p['isinstance']($pyjs_dbg_189_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_189_err);}throw $pyjs_dbg_189_err;
}})();
				$iter11_nextval=$p['__iter_prepare']($iter11_iter,false);
				while (typeof($p['__wrapped_next']($iter11_nextval)['$nextval']) != 'undefined') {
					var $tupleassign6 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter11_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_187_err){if (!$p['isinstance']($pyjs_dbg_187_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_187_err);}throw $pyjs_dbg_187_err;
}})();
					k = $tupleassign6[0];
					v = $tupleassign6[1];
					$pyjs['track']['lineno']=318;
					k = (function(){try{try{$pyjs['in_try_except'] += 1;
					return k['$$replace']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('%s.', $p['getattr'](self, 'boneName'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_190_err){if (!$p['isinstance']($pyjs_dbg_190_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_190_err);}throw $pyjs_dbg_190_err;
}})(), '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_191_err){if (!$p['isinstance']($pyjs_dbg_191_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_191_err);}throw $pyjs_dbg_191_err;
}})();
					$pyjs['track']['lineno']=319;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($constant_int_1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_192_err){if (!$p['isinstance']($pyjs_dbg_192_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_192_err);}throw $pyjs_dbg_192_err;
}})()) {
						$pyjs['track']['lineno']=320;
						var $tupleassign7 = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['__ass_unpack']((function(){try{try{$pyjs['in_try_except'] += 1;
						return k['$$split']('.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_193_err){if (!$p['isinstance']($pyjs_dbg_193_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_193_err);}throw $pyjs_dbg_193_err;
}})(), 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_194_err){if (!$p['isinstance']($pyjs_dbg_194_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_194_err);}throw $pyjs_dbg_194_err;
}})();
						idx = $tupleassign7[0];
						err = $tupleassign7[1];
						$pyjs['track']['lineno']=321;
						idx = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['int'](idx);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_195_err){if (!$p['isinstance']($pyjs_dbg_195_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_195_err);}throw $pyjs_dbg_195_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=323;
						continue;
					}
					$pyjs['track']['lineno']=324;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['printFunc']([(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('k: %s, v: %s', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['tuple']([k, v]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_196_err){if (!$p['isinstance']($pyjs_dbg_196_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_196_err);}throw $pyjs_dbg_196_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_197_err){if (!$p['isinstance']($pyjs_dbg_197_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_197_err);}throw $pyjs_dbg_197_err;
}})()], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_198_err){if (!$p['isinstance']($pyjs_dbg_198_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_198_err);}throw $pyjs_dbg_198_err;
}})();
					$pyjs['track']['lineno']=325;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['printFunc']([(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('idx: %s', idx);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_199_err){if (!$p['isinstance']($pyjs_dbg_199_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_199_err);}throw $pyjs_dbg_199_err;
}})()], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_200_err){if (!$p['isinstance']($pyjs_dbg_200_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_200_err);}throw $pyjs_dbg_200_err;
}})();
					$pyjs['track']['lineno']=326;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['printFunc']([(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['len']($p['getattr'](self, 'entries'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_201_err){if (!$p['isinstance']($pyjs_dbg_201_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_201_err);}throw $pyjs_dbg_201_err;
}})()], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_202_err){if (!$p['isinstance']($pyjs_dbg_202_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_202_err);}throw $pyjs_dbg_202_err;
}})();
					$pyjs['track']['lineno']=327;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and8=((($p['cmp'](idx, $constant_int_0))|1) == 1))?($p['cmp'](idx, (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['len']($p['getattr'](self, 'entries'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_203_err){if (!$p['isinstance']($pyjs_dbg_203_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_203_err);}throw $pyjs_dbg_203_err;
}})()) == -1):$and8));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_204_err){if (!$p['isinstance']($pyjs_dbg_204_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_204_err);}throw $pyjs_dbg_204_err;
}})()) {
						$pyjs['track']['lineno']=328;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'entries')['__getitem__'](idx)['setError'](err);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_205_err){if (!$p['isinstance']($pyjs_dbg_205_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_205_err);}throw $pyjs_dbg_205_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=329;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['errorInfo']]);
			$cls_definition['setExtendedErrorInformation'] = $method;
			$pyjs['track']['lineno']=137;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('ExtendedRelationalSelectionBone', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=332;
		$m['ExtendedRelationalSearch'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.extendenrelational';
			$cls_definition['__md5__'] = '58472c1c00323008a351235574aa1092';
			$pyjs['track']['lineno']=333;
			$method = $pyjs__bind_method2('__init__', function(extension, view, modul) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

					var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					extension = arguments[1];
					view = arguments[2];
					modul = arguments[3];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,4,arguments['length']-1));

					var kwargs = arguments['length'] >= 5 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '58472c1c00323008a351235574aa1092') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof modul != 'undefined') {
						if (modul !== null && typeof modul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modul;
							modul = arguments[4];
						}
					} else 					if (typeof view != 'undefined') {
						if (view !== null && typeof view['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = view;
							view = arguments[4];
						}
					} else 					if (typeof extension != 'undefined') {
						if (extension !== null && typeof extension['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = extension;
							extension = arguments[4];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[4];
						}
					} else {
					}
				}
				var btn;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':333};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=333;
				$pyjs['track']['lineno']=334;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ExtendedRelationalSearch'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_206_err){if (!$p['isinstance']($pyjs_dbg_206_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_206_err);}throw $pyjs_dbg_206_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_207_err){if (!$p['isinstance']($pyjs_dbg_207_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_207_err);}throw $pyjs_dbg_207_err;
}})();
				$pyjs['track']['lineno']=335;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('view', view) : $p['setattr'](self, 'view', view); 
				$pyjs['track']['lineno']=336;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('extension', extension) : $p['setattr'](self, 'extension', extension); 
				$pyjs['track']['lineno']=337;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=338;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentSelection', null) : $p['setattr'](self, 'currentSelection', null); 
				$pyjs['track']['lineno']=339;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('filterChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('filterChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_208_err){if (!$p['isinstance']($pyjs_dbg_208_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_208_err);}throw $pyjs_dbg_208_err;
}})()) : $p['setattr'](self, 'filterChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('filterChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_208_err){if (!$p['isinstance']($pyjs_dbg_208_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_208_err);}throw $pyjs_dbg_208_err;
}})()); 
				$pyjs['track']['lineno']=340;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['TextNode']('RELATIONAL SEARCH');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_209_err){if (!$p['isinstance']($pyjs_dbg_209_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_209_err);}throw $pyjs_dbg_209_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_210_err){if (!$p['isinstance']($pyjs_dbg_210_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_210_err);}throw $pyjs_dbg_210_err;
}})();
				$pyjs['track']['lineno']=341;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['TextNode'](extension['__getitem__']('name'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_211_err){if (!$p['isinstance']($pyjs_dbg_211_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_211_err);}throw $pyjs_dbg_211_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_212_err){if (!$p['isinstance']($pyjs_dbg_212_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_212_err);}throw $pyjs_dbg_212_err;
}})();
				$pyjs['track']['lineno']=342;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentEntry', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_213_err){if (!$p['isinstance']($pyjs_dbg_213_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_213_err);}throw $pyjs_dbg_213_err;
}})()) : $p['setattr'](self, 'currentEntry', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_213_err){if (!$p['isinstance']($pyjs_dbg_213_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_213_err);}throw $pyjs_dbg_213_err;
}})()); 
				$pyjs['track']['lineno']=343;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'currentEntry'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_214_err){if (!$p['isinstance']($pyjs_dbg_214_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_214_err);}throw $pyjs_dbg_214_err;
}})();
				$pyjs['track']['lineno']=344;
				btn = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['ext']['Button']('Select', $p['getattr'](self, 'openSelector'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_215_err){if (!$p['isinstance']($pyjs_dbg_215_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_215_err);}throw $pyjs_dbg_215_err;
}})();
				$pyjs['track']['lineno']=345;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](btn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_216_err){if (!$p['isinstance']($pyjs_dbg_216_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_216_err);}throw $pyjs_dbg_216_err;
}})();
				$pyjs['track']['lineno']=346;
				btn = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['ext']['Button']('Clear', $p['getattr'](self, 'clearSelection'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_217_err){if (!$p['isinstance']($pyjs_dbg_217_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_217_err);}throw $pyjs_dbg_217_err;
}})();
				$pyjs['track']['lineno']=347;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild'](btn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_218_err){if (!$p['isinstance']($pyjs_dbg_218_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_218_err);}throw $pyjs_dbg_218_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['extension'],['view'],['modul']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=349;
			$method = $pyjs__bind_method2('clearSelection', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '58472c1c00323008a351235574aa1092') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':349};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=349;
				$pyjs['track']['lineno']=350;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentSelection', null) : $p['setattr'](self, 'currentSelection', null); 
				$pyjs['track']['lineno']=351;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['filterChangedEvent']['fire']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_219_err){if (!$p['isinstance']($pyjs_dbg_219_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_219_err);}throw $pyjs_dbg_219_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['clearSelection'] = $method;
			$pyjs['track']['lineno']=353;
			$method = $pyjs__bind_method2('openSelector', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '58472c1c00323008a351235574aa1092') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var currentSelector;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':353};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=353;
				$pyjs['track']['lineno']=354;
				currentSelector = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['ListWidget'], null, null, [{'isSelector':true}, $p['getattr'](self, 'extension')['__getitem__']('modul')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_220_err){if (!$p['isinstance']($pyjs_dbg_220_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_220_err);}throw $pyjs_dbg_220_err;
}})();
				$pyjs['track']['lineno']=355;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return currentSelector['selectionActivatedEvent']['register'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_221_err){if (!$p['isinstance']($pyjs_dbg_221_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_221_err);}throw $pyjs_dbg_221_err;
}})();
				$pyjs['track']['lineno']=356;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['conf']['__getitem__']('mainWindow')['stackWidget'](currentSelector);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_222_err){if (!$p['isinstance']($pyjs_dbg_222_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_222_err);}throw $pyjs_dbg_222_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['openSelector'] = $method;
			$pyjs['track']['lineno']=358;
			$method = $pyjs__bind_method2('onSelectionActivated', function(table, selection) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					table = arguments[1];
					selection = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '58472c1c00323008a351235574aa1092') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':358};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=358;
				$pyjs['track']['lineno']=359;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentSelection', selection) : $p['setattr'](self, 'currentSelection', selection); 
				$pyjs['track']['lineno']=360;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['filterChangedEvent']['fire']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_223_err){if (!$p['isinstance']($pyjs_dbg_223_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_223_err);}throw $pyjs_dbg_223_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['table'],['selection']]);
			$cls_definition['onSelectionActivated'] = $method;
			$pyjs['track']['lineno']=363;
			$method = $pyjs__bind_method2('updateFilter', function(filter) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					filter = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '58472c1c00323008a351235574aa1092') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $add10,$add9,newId;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':363};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=363;
				$pyjs['track']['lineno']=364;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'currentSelection'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_224_err){if (!$p['isinstance']($pyjs_dbg_224_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_224_err);}throw $pyjs_dbg_224_err;
}})()) {
					$pyjs['track']['lineno']=365;
					$p['getattr']($p['getattr'](self, 'currentEntry'), 'element')['__is_instance__'] && typeof $p['getattr']($p['getattr'](self, 'currentEntry'), 'element')['__setattr__'] == 'function' ? $p['getattr']($p['getattr'](self, 'currentEntry'), 'element')['__setattr__']('innerHTML', $p['getattr'](self, 'currentSelection')['__getitem__']($constant_int_0)['__getitem__']('name')) : $p['setattr']($p['getattr']($p['getattr'](self, 'currentEntry'), 'element'), 'innerHTML', $p['getattr'](self, 'currentSelection')['__getitem__']($constant_int_0)['__getitem__']('name')); 
					$pyjs['track']['lineno']=366;
					newId = $p['getattr'](self, 'currentSelection')['__getitem__']($constant_int_0)['__getitem__']('id');
					$pyjs['track']['lineno']=367;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return filter['__setitem__']($p['__op_add']($add9=$p['getattr'](self, 'extension')['__getitem__']('target'),$add10='.id'), newId);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_225_err){if (!$p['isinstance']($pyjs_dbg_225_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_225_err);}throw $pyjs_dbg_225_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=369;
					$p['getattr']($p['getattr'](self, 'currentEntry'), 'element')['__is_instance__'] && typeof $p['getattr']($p['getattr'](self, 'currentEntry'), 'element')['__setattr__'] == 'function' ? $p['getattr']($p['getattr'](self, 'currentEntry'), 'element')['__setattr__']('innerHTML', '') : $p['setattr']($p['getattr']($p['getattr'](self, 'currentEntry'), 'element'), 'innerHTML', ''); 
				}
				$pyjs['track']['lineno']=370;
				$pyjs['track']['lineno']=370;
				var $pyjs__ret = filter;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['filter']]);
			$cls_definition['updateFilter'] = $method;
			$pyjs['track']['lineno']=373;
			$method = $pyjs__bind_method2('canHandleExtension', function(extension, view, modul) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var $and12,$or14,$and10,$and11,$or13;
				$pyjs['track']={'module':'bones.extendenrelational', 'lineno':373};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.extendenrelational';
				$pyjs['track']['lineno']=373;
				$pyjs['track']['lineno']=374;
				$pyjs['track']['lineno']=374;
				var $pyjs__ret = ($p['bool']($and10=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](extension, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_226_err){if (!$p['isinstance']($pyjs_dbg_226_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_226_err);}throw $pyjs_dbg_226_err;
}})())?($p['bool']($and11=(function(){try{try{$pyjs['in_try_except'] += 1;
				return extension['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_227_err){if (!$p['isinstance']($pyjs_dbg_227_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_227_err);}throw $pyjs_dbg_227_err;
}})()['__contains__']('type'))?($p['bool']($or13=$p['op_eq'](extension['__getitem__']('type'), 'relational'))?$or13:(function(){try{try{$pyjs['in_try_except'] += 1;
				return extension['__getitem__']('type')['startswith']('relational.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_228_err){if (!$p['isinstance']($pyjs_dbg_228_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_228_err);}throw $pyjs_dbg_228_err;
}})()):$and11):$and10);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['extension'],['view'],['modul']]);
			$cls_definition['canHandleExtension'] = $method;
			$pyjs['track']['lineno']=332;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('ExtendedRelationalSearch', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=377;
		$m['CheckForExtendedRelationalBoneSelection'] = function(modulName, boneName, skelStructure) {
			if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, null, arguments['length']);
			var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

			var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
			if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
				if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
				kwargs = arguments[arguments['length']+1];
			} else {
				delete kwargs['$pyjs_is_kwarg'];
			}
			if (typeof kwargs == 'undefined') {
				kwargs = $p['__empty_dict']();
				if (typeof skelStructure != 'undefined') {
					if (skelStructure !== null && typeof skelStructure['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = skelStructure;
						skelStructure = arguments[3];
					}
				} else 				if (typeof boneName != 'undefined') {
					if (boneName !== null && typeof boneName['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = boneName;
						boneName = arguments[3];
					}
				} else 				if (typeof modulName != 'undefined') {
					if (modulName !== null && typeof modulName['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = modulName;
						modulName = arguments[3];
					}
				} else {
				}
			}
			var $and15,$and13,$and16,$and14,isMultiple;
			$pyjs['track']={'module':'bones.extendenrelational','lineno':377};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='bones.extendenrelational';
			$pyjs['track']['lineno']=377;
			$pyjs['track']['lineno']=378;
			isMultiple = ($p['bool']($and13=(function(){try{try{$pyjs['in_try_except'] += 1;
			return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_229_err){if (!$p['isinstance']($pyjs_dbg_229_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_229_err);}throw $pyjs_dbg_229_err;
}})()['__contains__']('multiple'))?skelStructure['__getitem__'](boneName)['__getitem__']('multiple'):$and13);
			$pyjs['track']['lineno']=379;
			$pyjs['track']['lineno']=379;
			var $pyjs__ret = ($p['bool']($and15=(function(){try{try{$pyjs['in_try_except'] += 1;
			return skelStructure['__getitem__'](boneName)['__getitem__']('type')['startswith']('extendedrelational.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_230_err){if (!$p['isinstance']($pyjs_dbg_230_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_230_err);}throw $pyjs_dbg_230_err;
}})())?isMultiple:$and15);
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['CheckForExtendedRelationalBoneSelection']['__name__'] = 'CheckForExtendedRelationalBoneSelection';

		$m['CheckForExtendedRelationalBoneSelection']['__bind_type__'] = 0;
		$m['CheckForExtendedRelationalBoneSelection']['__args__'] = ['args',['kwargs'],['modulName'],['boneName'],['skelStructure']];
		$pyjs['track']['lineno']=384;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['editBoneSelector']['insert']($constant_int_5, $m['CheckForExtendedRelationalBoneSelection'], $m['ExtendedRelationalSelectionBone']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_231_err){if (!$p['isinstance']($pyjs_dbg_231_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_231_err);}throw $pyjs_dbg_231_err;
}})();
		$pyjs['track']['lineno']=385;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['viewDelegateSelector']['insert']($constant_int_5, $m['CheckForExtendedRelationalBoneSelection'], $m['ExtendedRelationalViewBoneDelegate']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_232_err){if (!$p['isinstance']($pyjs_dbg_232_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_232_err);}throw $pyjs_dbg_232_err;
}})();
		$pyjs['track']['lineno']=386;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['extendedSearchWidgetSelector']['insert']($constant_int_1, $p['getattr']($m['ExtendedRelationalSearch'], 'canHandleExtension'), $m['ExtendedRelationalSearch']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_233_err){if (!$p['isinstance']($pyjs_dbg_233_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_233_err);}throw $pyjs_dbg_233_err;
}})();
		$pyjs['track']['lineno']=387;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['extractorDelegateSelector']['insert']($constant_int_4, $m['CheckForExtendedRelationalBoneSelection'], $m['ExtendedRelationalBoneExtractor']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_234_err){if (!$p['isinstance']($pyjs_dbg_234_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_234_err);}throw $pyjs_dbg_234_err;
}})();
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end bones.extendenrelational */


/* end module: bones.extendenrelational */


/*
PYJS_DEPS: ['html5', 'priorityqueue.editBoneSelector', 'priorityqueue', 'priorityqueue.viewDelegateSelector', 'priorityqueue.extendedSearchWidgetSelector', 'priorityqueue.extractorDelegateSelector', 'event.EventDispatcher', 'event', 'utils.formatString', 'utils', 'widgets.list.ListWidget', 'widgets', 'widgets.list', 'widgets.edit.InternalEdit', 'widgets.edit', 'config.conf', 'config', 'i18n.translate', 'i18n']
*/
