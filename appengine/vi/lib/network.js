/* start module: network */
$pyjs['loaded_modules']['network'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['network']['__was_initialized__']) return $pyjs['loaded_modules']['network'];
	var $m = $pyjs['loaded_modules']['network'];
	$m['__repr__'] = function() { return '<module: network>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'network';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	try {
		$m.__track_lines__[1] = 'network.py, line 1:\n    #!/usr/bin/env python3';
		$m.__track_lines__[4] = 'network.py, line 4:\n    import sys, os';
		$m.__track_lines__[5] = 'network.py, line 5:\n    import json';
		$m.__track_lines__[6] = 'network.py, line 6:\n    import string, random, time';
		$m.__track_lines__[8] = 'network.py, line 8:\n    class DeferredCall( object ):';
		$m.__track_lines__[15] = 'network.py, line 15:\n    def __init__( self, func, *args, **kwargs ):';
		$m.__track_lines__[20] = 'network.py, line 20:\n    super( DeferredCall, self ).__init__()';
		$m.__track_lines__[21] = 'network.py, line 21:\n    delay = 25';
		$m.__track_lines__[22] = 'network.py, line 22:\n    if "_delay" in kwargs.keys():';
		$m.__track_lines__[23] = 'network.py, line 23:\n    delay = kwargs["_delay"]';
		$m.__track_lines__[24] = 'network.py, line 24:\n    del kwargs["_delay"]';
		$m.__track_lines__[25] = 'network.py, line 25:\n    self._tFunc = func';
		$m.__track_lines__[26] = 'network.py, line 26:\n    self._tArgs = args';
		$m.__track_lines__[27] = 'network.py, line 27:\n    self._tKwArgs = kwargs';
		$m.__track_lines__[28] = 'network.py, line 28:\n    w = eval("window")';
		$m.__track_lines__[29] = 'network.py, line 29:\n    w.setTimeout( self.run, delay )';
		$m.__track_lines__[31] = 'network.py, line 31:\n    def run(self):';
		$m.__track_lines__[35] = 'network.py, line 35:\n    self._tFunc( *self._tArgs, **self._tKwArgs )';
		$m.__track_lines__[37] = 'network.py, line 37:\n    class HTTPRequest(object):';
		$m.__track_lines__[41] = 'network.py, line 41:\n    def __init__(self, *args, **kwargs ):';
		$m.__track_lines__[42] = 'network.py, line 42:\n    super( HTTPRequest, self ).__init__( *args, **kwargs )';
		$m.__track_lines__[43] = 'network.py, line 43:\n    self.req = eval("new XMLHttpRequest()")';
		$m.__track_lines__[44] = 'network.py, line 44:\n    self.req.onreadystatechange = self.onReadyStateChange';
		$m.__track_lines__[45] = 'network.py, line 45:\n    self.cb = None';
		$m.__track_lines__[46] = 'network.py, line 46:\n    self.hasBeenSent = False';
		$m.__track_lines__[48] = 'network.py, line 48:\n    def asyncGet(self, url, cb):';
		$m.__track_lines__[56] = 'network.py, line 56:\n    self.cb = cb';
		$m.__track_lines__[57] = 'network.py, line 57:\n    self.type = "GET"';
		$m.__track_lines__[58] = 'network.py, line 58:\n    self.payload = None';
		$m.__track_lines__[59] = 'network.py, line 59:\n    self.content_type = None';
		$m.__track_lines__[60] = 'network.py, line 60:\n    self.req.open("GET",url,True)';
		$m.__track_lines__[62] = 'network.py, line 62:\n    def asyncPost(self, url, payload, cb, content_type=None ):';
		$m.__track_lines__[70] = 'network.py, line 70:\n    self.cb = cb';
		$m.__track_lines__[71] = 'network.py, line 71:\n    self.type = "POST"';
		$m.__track_lines__[72] = 'network.py, line 72:\n    self.payload = payload';
		$m.__track_lines__[73] = 'network.py, line 73:\n    self.content_type = content_type';
		$m.__track_lines__[74] = 'network.py, line 74:\n    self.req.open("POST",url,True)';
		$m.__track_lines__[76] = 'network.py, line 76:\n    def onReadyStateChange(self, *args, **kwargs):';
		$m.__track_lines__[80] = 'network.py, line 80:\n    if self.req.readyState == 1 and not self.hasBeenSent:';
		$m.__track_lines__[81] = 'network.py, line 81:\n    self.hasBeenSent = True # Internet Explorer calls this function twice!';
		$m.__track_lines__[83] = 'network.py, line 83:\n    if self.type=="POST" and self.content_type is not None:';
		$m.__track_lines__[84] = "network.py, line 84:\n    self.req.setRequestHeader('Content-Type', self.content_type)";
		$m.__track_lines__[86] = 'network.py, line 86:\n    self.req.send( self.payload )';
		$m.__track_lines__[88] = 'network.py, line 88:\n    if self.req.readyState == 4:';
		$m.__track_lines__[89] = 'network.py, line 89:\n    if self.req.status >= 200 and self.req.status < 300:';
		$m.__track_lines__[90] = 'network.py, line 90:\n    self.cb.onCompletion( self.req.responseText )';
		$m.__track_lines__[92] = 'network.py, line 92:\n    self.cb.onError( self.req.responseText, self.req.status )';
		$m.__track_lines__[94] = 'network.py, line 94:\n    class NetworkService( object ):';
		$m.__track_lines__[101] = 'network.py, line 101:\n    changeListeners = [] # All currently active widgets which will be informed of changes made';
		$m.__track_lines__[102] = 'network.py, line 102:\n    _cache = {} # Modul->Cache index map (for requests that can be cached)';
		$m.__track_lines__[105] = 'network.py, line 104:\n    @staticmethod ... def notifyChange( modul ):';
		$m.__track_lines__[113] = 'network.py, line 113:\n    if modul in NetworkService._cache.keys():';
		$m.__track_lines__[114] = 'network.py, line 114:\n    NetworkService._cache[ modul ] += 1';
		$m.__track_lines__[115] = 'network.py, line 115:\n    for c in NetworkService.changeListeners:';
		$m.__track_lines__[116] = 'network.py, line 116:\n    c.onDataChanged( modul )';
		$m.__track_lines__[119] = 'network.py, line 118:\n    @staticmethod ... def registerChangeListener( listener ):';
		$m.__track_lines__[128] = 'network.py, line 128:\n    if listener in NetworkService.changeListeners:';
		$m.__track_lines__[129] = 'network.py, line 129:\n    return';
		$m.__track_lines__[130] = 'network.py, line 130:\n    NetworkService.changeListeners.append( listener )';
		$m.__track_lines__[133] = 'network.py, line 132:\n    @staticmethod ... def removeChangeListener( listener ):';
		$m.__track_lines__[139] = 'network.py, line 139:\n    assert listener in NetworkService.changeListeners, "Attempt to remove unregistered listener %s" % str( listener )';
		$m.__track_lines__[140] = 'network.py, line 140:\n    NetworkService.changeListeners.remove( listener )';
		$m.__track_lines__[143] = 'network.py, line 142:\n    @staticmethod ... def genReqStr( params ):';
		$m.__track_lines__[151] = 'network.py, line 151:\n    boundary_str = "---"+\'\'.join( [ random.choice(string.ascii_lowercase+string.ascii_uppercase + string.digits) for x in range(13) ] )';
		$m.__track_lines__[152] = 'network.py, line 152:\n    boundary = boundary_str';
		$m.__track_lines__[153] = 'network.py, line 153:\n    res = b\'Content-Type: multipart/mixed; boundary="\'+boundary+b\'"\\r\\nMIME-Version: 1.0\\r\\n\'';
		$m.__track_lines__[154] = "network.py, line 154:\n    res += b'\\r\\n--'+boundary";
		$m.__track_lines__[155] = 'network.py, line 155:\n    for(key, value) in list(params.items()):';
		$m.__track_lines__[156] = 'network.py, line 156:\n    if all( [x in dir( value ) for x in ["name", "read"] ] ): #File';
		$m.__track_lines__[157] = 'network.py, line 157:\n    try:';
		$m.__track_lines__[158] = 'network.py, line 158:\n    (type, encoding) = mimetypes.guess_type( value.name.decode( sys.getfilesystemencoding() ), strict=False )';
		$m.__track_lines__[159] = 'network.py, line 159:\n    type = type or "application/octet-stream"';
		$m.__track_lines__[161] = 'network.py, line 161:\n    type = "application/octet-stream"';
		$m.__track_lines__[162] = 'network.py, line 162:\n    res += b\'\\r\\nContent-Type: \'+type+b\'\\r\\nMIME-Version: 1.0\\r\\nContent-Disposition: form-data; name="\'+key+b\'"; filename="\'+os.path.basename(value.name).decode(sys.getfilesystemencoding())+b\'"\\r\\n\\r\\n\'';
		$m.__track_lines__[163] = 'network.py, line 163:\n    res += str(value.read())';
		$m.__track_lines__[164] = "network.py, line 164:\n    res += b'\\r\\n--'+boundary";
		$m.__track_lines__[166] = 'network.py, line 166:\n    for val in value:';
		$m.__track_lines__[167] = 'network.py, line 167:\n    res += b\'\\r\\nContent-Type: application/octet-stream\\r\\nMIME-Version: 1.0\\r\\nContent-Disposition: form-data; name="\'+key+b\'"\\r\\n\\r\\n\'';
		$m.__track_lines__[168] = 'network.py, line 168:\n    res += str(val)';
		$m.__track_lines__[169] = "network.py, line 169:\n    res += b'\\r\\n--'+boundary";
		$m.__track_lines__[171] = 'network.py, line 171:\n    for k,v in value.items():';
		$m.__track_lines__[172] = 'network.py, line 172:\n    res += b\'\\r\\nContent-Type: application/octet-stream\\r\\nMIME-Version: 1.0\\r\\nContent-Disposition: form-data; name="\'+key+b"."+k+b\'"\\r\\n\\r\\n\'';
		$m.__track_lines__[173] = 'network.py, line 173:\n    res += str(v)';
		$m.__track_lines__[174] = "network.py, line 174:\n    res += b'\\r\\n--'+boundary";
		$m.__track_lines__[176] = 'network.py, line 176:\n    res += b\'\\r\\nContent-Type: application/octet-stream\\r\\nMIME-Version: 1.0\\r\\nContent-Disposition: form-data; name="\'+key+b\'"\\r\\n\\r\\n\'';
		$m.__track_lines__[177] = 'network.py, line 177:\n    res += str(value)';
		$m.__track_lines__[178] = "network.py, line 178:\n    res += b'\\r\\n--'+boundary";
		$m.__track_lines__[179] = "network.py, line 179:\n    res += b'--\\r\\n'";
		$m.__track_lines__[180] = 'network.py, line 180:\n    return( res, boundary )';
		$m.__track_lines__[184] = 'network.py, line 183:\n    @staticmethod ... def decode( req ):';
		$m.__track_lines__[190] = 'network.py, line 190:\n    return( json.loads( req.result ) )';
		$m.__track_lines__[195] = 'network.py, line 194:\n    @staticmethod ... def urlForArgs( modul, path, cacheable ):';
		$m.__track_lines__[209] = 'network.py, line 209:\n    cacheKey = time.time()';
		$m.__track_lines__[210] = 'network.py, line 210:\n    if cacheable and modul:';
		$m.__track_lines__[211] = 'network.py, line 211:\n    if not modul in NetworkService._cache.keys():';
		$m.__track_lines__[212] = 'network.py, line 212:\n    NetworkService._cache[ modul ] = 1';
		$m.__track_lines__[213] = 'network.py, line 213:\n    cacheKey = "c%s" % NetworkService._cache[ modul ]';
		$m.__track_lines__[214] = 'network.py, line 214:\n    if modul:';
		$m.__track_lines__[215] = 'network.py, line 215:\n    return( "/admin/%s/%s?_unused_time_stamp=%s" % (modul, path, cacheKey))';
		$m.__track_lines__[217] = 'network.py, line 217:\n    if "?" in path:';
		$m.__track_lines__[218] = 'network.py, line 218:\n    return( path+"&_unused_time_stamp=%s" % cacheKey)';
		$m.__track_lines__[220] = 'network.py, line 220:\n    return( path+"?_unused_time_stamp=%s" % cacheKey)';
		$m.__track_lines__[222] = 'network.py, line 222:\n    def __init__(self, modul, url, params, successHandler, failureHandler, finishedHandler, modifies, cacheable, secure ):';
		$m.__track_lines__[227] = 'network.py, line 227:\n    super( NetworkService, self ).__init__()';
		$m.__track_lines__[228] = 'network.py, line 228:\n    self.result = None';
		$m.__track_lines__[229] = 'network.py, line 229:\n    self.status = "running"';
		$m.__track_lines__[230] = 'network.py, line 230:\n    self.waitingForSkey = False';
		$m.__track_lines__[231] = 'network.py, line 231:\n    self.modul = modul';
		$m.__track_lines__[232] = 'network.py, line 232:\n    self.url = url';
		$m.__track_lines__[233] = 'network.py, line 233:\n    self.params = params';
		$m.__track_lines__[234] = 'network.py, line 234:\n    self.successHandler = [successHandler] if successHandler else []';
		$m.__track_lines__[235] = 'network.py, line 235:\n    self.failureHandler = [failureHandler] if failureHandler else []';
		$m.__track_lines__[236] = 'network.py, line 236:\n    self.finishedHandler = [finishedHandler] if finishedHandler else []';
		$m.__track_lines__[237] = 'network.py, line 237:\n    self.modifies = modifies';
		$m.__track_lines__[238] = 'network.py, line 238:\n    self.cacheable = cacheable';
		$m.__track_lines__[239] = 'network.py, line 239:\n    self.secure = secure';
		$m.__track_lines__[240] = 'network.py, line 240:\n    if secure:';
		$m.__track_lines__[241] = 'network.py, line 241:\n    self.waitingForSkey = True';
		$m.__track_lines__[242] = 'network.py, line 242:\n    self.doFetch("/admin/skey",None,None)';
		$m.__track_lines__[244] = 'network.py, line 244:\n    self.doFetch(NetworkService.urlForArgs(modul,url,cacheable),params, None)';
		$m.__track_lines__[248] = 'network.py, line 247:\n    @staticmethod ... def request( modul, url, params=None, successHandler=None, failureHandler=None,';
		$m.__track_lines__[272] = 'network.py, line 272:\n    print("NS REQUEST", modul, url, params )';
		$m.__track_lines__[273] = 'network.py, line 273:\n    assert not( cacheable and modifies ), "Cannot cache a request modifying data!"';
		$m.__track_lines__[275] = 'network.py, line 275:\n    return( NetworkService(modul, url, params, successHandler, failureHandler, finishedHandler,';
		$m.__track_lines__[279] = 'network.py, line 279:\n    def doFetch(self, url, params, skey ):';
		$m.__track_lines__[283] = 'network.py, line 283:\n    if params:';
		$m.__track_lines__[284] = 'network.py, line 284:\n    if skey:';
		$m.__track_lines__[285] = 'network.py, line 285:\n    params["skey"] = skey';
		$m.__track_lines__[286] = 'network.py, line 286:\n    contentType = None';
		$m.__track_lines__[287] = 'network.py, line 287:\n    if isinstance( params, dict):';
		$m.__track_lines__[288] = 'network.py, line 288:\n    multipart, boundary = NetworkService.genReqStr( params )';
		$m.__track_lines__[289] = "network.py, line 289:\n    contentType = b'multipart/form-data; boundary='+boundary+b'; charset=utf-8'";
		$m.__track_lines__[291] = "network.py, line 291:\n    contentType =  b'application/x-www-form-urlencoded'";
		$m.__track_lines__[292] = 'network.py, line 292:\n    multipart = params';
		$m.__track_lines__[294] = 'network.py, line 294:\n    print( params )';
		$m.__track_lines__[295] = 'network.py, line 295:\n    print( type( params ) )';
		$m.__track_lines__[296] = 'network.py, line 296:\n    HTTPRequest().asyncPost(url, multipart, self, content_type=contentType )';
		$m.__track_lines__[298] = 'network.py, line 298:\n    if skey:';
		$m.__track_lines__[299] = 'network.py, line 299:\n    if "?" in url:';
		$m.__track_lines__[300] = 'network.py, line 300:\n    url += "&skey=%s" % skey';
		$m.__track_lines__[302] = 'network.py, line 302:\n    url += "?skey=%s" % skey';
		$m.__track_lines__[303] = 'network.py, line 303:\n    HTTPRequest().asyncGet(url, self)';
		$m.__track_lines__[305] = 'network.py, line 305:\n    def onCompletion(self, text):';
		$m.__track_lines__[309] = 'network.py, line 309:\n    if self.waitingForSkey:';
		$m.__track_lines__[310] = 'network.py, line 310:\n    self.waitingForSkey = False';
		$m.__track_lines__[311] = 'network.py, line 311:\n    self.doFetch( NetworkService.urlForArgs(self.modul,self.url,self.cacheable), self.params, json.loads(text) )';
		$m.__track_lines__[314] = 'network.py, line 314:\n    self.result = text';
		$m.__track_lines__[315] = 'network.py, line 315:\n    self.status = "succeeded"';
		$m.__track_lines__[316] = 'network.py, line 316:\n    try:';
		$m.__track_lines__[317] = 'network.py, line 317:\n    for s in self.successHandler:';
		$m.__track_lines__[318] = 'network.py, line 318:\n    s( self )';
		$m.__track_lines__[319] = 'network.py, line 319:\n    for s in self.finishedHandler:';
		$m.__track_lines__[320] = 'network.py, line 320:\n    s( self )';
		$m.__track_lines__[322] = 'network.py, line 322:\n    if self.modifies:';
		$m.__track_lines__[323] = 'network.py, line 323:\n    DeferredCall(NetworkService.notifyChange, self.modul, _delay=2500)';
		$m.__track_lines__[325] = 'network.py, line 325:\n    raise';
		$m.__track_lines__[327] = 'network.py, line 327:\n    self.successHandler = []';
		$m.__track_lines__[328] = 'network.py, line 328:\n    self.finishedHandler = []';
		$m.__track_lines__[329] = 'network.py, line 329:\n    self.failureHandler = []';
		$m.__track_lines__[330] = 'network.py, line 330:\n    self.params = None';
		$m.__track_lines__[331] = 'network.py, line 331:\n    if self.modifies:';
		$m.__track_lines__[332] = 'network.py, line 332:\n    DeferredCall(NetworkService.notifyChange, self.modul, _delay=2500)';
		$m.__track_lines__[334] = 'network.py, line 334:\n    def onError(self, text, code):';
		$m.__track_lines__[338] = 'network.py, line 338:\n    self.status = "failed"';
		$m.__track_lines__[339] = 'network.py, line 339:\n    self.result = text';
		$m.__track_lines__[340] = 'network.py, line 340:\n    for s in self.failureHandler:';
		$m.__track_lines__[341] = 'network.py, line 341:\n    s( self, code )';
		$m.__track_lines__[342] = 'network.py, line 342:\n    for s in self.finishedHandler:';
		$m.__track_lines__[343] = 'network.py, line 343:\n    s( self )';
		$m.__track_lines__[344] = 'network.py, line 344:\n    self.successHandler = []';
		$m.__track_lines__[345] = 'network.py, line 345:\n    self.finishedHandler = []';
		$m.__track_lines__[346] = 'network.py, line 346:\n    self.failureHandler = []';
		$m.__track_lines__[347] = 'network.py, line 347:\n    self.params = None';
		$m.__track_lines__[349] = 'network.py, line 349:\n    def onTimeout(self, text):';
		$m.__track_lines__[353] = 'network.py, line 353:\n    self.onError( text, -1 )';

		var $constant_int_1 = new $p['int'](1);
		var $constant_int_4 = new $p['int'](4);
		var $constant_int_200 = new $p['int'](200);
		var $constant_int_300 = new $p['int'](300);
		var $constant_int_13 = new $p['int'](13);
		var $constant_int_25 = new $p['int'](25);
		var $constant_int_2500 = new $p['int'](2500);
		$pyjs['track']['module']='network';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=4;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['sys'] = $p['___import___']('sys', null);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['os'] = $p['___import___']('os', null);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=5;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['json'] = $p['___import___']('json', null);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=6;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['string'] = $p['___import___']('string', null);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['random'] = $p['___import___']('random', null);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['time'] = $p['___import___']('time', null);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=8;
		$m['DeferredCall'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'network';
			$cls_definition['__md5__'] = '273880ec89d8c37c64001d8f9e9fbf13';
			$pyjs['track']['lineno']=15;
			$method = $pyjs__bind_method2('__init__', function(func) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					func = arguments[1];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,2,arguments['length']-1));

					var kwargs = arguments['length'] >= 3 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '273880ec89d8c37c64001d8f9e9fbf13') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof func != 'undefined') {
						if (func !== null && typeof func['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = func;
							func = arguments[2];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[2];
						}
					} else {
					}
				}
				var delay,w;
				$pyjs['track']={'module':'network', 'lineno':15};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=15;
				$pyjs['track']['lineno']=20;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['DeferredCall'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})()['__init__']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})();
				$pyjs['track']['lineno']=21;
				delay = $constant_int_25;
				$pyjs['track']['lineno']=22;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return kwargs['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})()['__contains__']('_delay'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})()) {
					$pyjs['track']['lineno']=23;
					delay = kwargs['__getitem__']('_delay');
					$pyjs['track']['lineno']=24;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return kwargs['__delitem__']('_delay');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})();
				}
				$pyjs['track']['lineno']=25;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_tFunc', func) : $p['setattr'](self, '_tFunc', func); 
				$pyjs['track']['lineno']=26;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_tArgs', args) : $p['setattr'](self, '_tArgs', args); 
				$pyjs['track']['lineno']=27;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('_tKwArgs', kwargs) : $p['setattr'](self, '_tKwArgs', kwargs); 
				$pyjs['track']['lineno']=28;
				w = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof eval == "undefined"?$m['eval']:eval)('window');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})();
				$pyjs['track']['lineno']=29;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return w['setTimeout']($p['getattr'](self, 'run'), delay);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['func']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=31;
			$method = $pyjs__bind_method2('run', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '273880ec89d8c37c64001d8f9e9fbf13') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'network', 'lineno':31};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=31;
				$pyjs['track']['lineno']=35;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(self, '_tFunc', $p['getattr'](self, '_tArgs'), $p['getattr'](self, '_tKwArgs'), [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['run'] = $method;
			$pyjs['track']['lineno']=8;
			var $bases = new Array($p['object']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('DeferredCall', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=37;
		$m['HTTPRequest'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'network';
			$cls_definition['__md5__'] = '1a4497aa35066d977157cc4bf9d81311';
			$pyjs['track']['lineno']=41;
			$method = $pyjs__bind_method2('__init__', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '1a4497aa35066d977157cc4bf9d81311') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'network', 'lineno':41};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=41;
				$pyjs['track']['lineno']=42;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['HTTPRequest'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})();
				$pyjs['track']['lineno']=43;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('req', (function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof eval == "undefined"?$m['eval']:eval)('new XMLHttpRequest()');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})()) : $p['setattr'](self, 'req', (function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof eval == "undefined"?$m['eval']:eval)('new XMLHttpRequest()');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})()); 
				$pyjs['track']['lineno']=44;
				$p['getattr'](self, 'req')['__is_instance__'] && typeof $p['getattr'](self, 'req')['__setattr__'] == 'function' ? $p['getattr'](self, 'req')['__setattr__']('onreadystatechange', $p['getattr'](self, 'onReadyStateChange')) : $p['setattr']($p['getattr'](self, 'req'), 'onreadystatechange', $p['getattr'](self, 'onReadyStateChange')); 
				$pyjs['track']['lineno']=45;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('cb', null) : $p['setattr'](self, 'cb', null); 
				$pyjs['track']['lineno']=46;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('hasBeenSent', false) : $p['setattr'](self, 'hasBeenSent', false); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=48;
			$method = $pyjs__bind_method2('asyncGet', function(url, cb) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					url = arguments[1];
					cb = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '1a4497aa35066d977157cc4bf9d81311') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'network', 'lineno':48};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=48;
				$pyjs['track']['lineno']=56;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('cb', cb) : $p['setattr'](self, 'cb', cb); 
				$pyjs['track']['lineno']=57;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('type', 'GET') : $p['setattr'](self, 'type', 'GET'); 
				$pyjs['track']['lineno']=58;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('payload', null) : $p['setattr'](self, 'payload', null); 
				$pyjs['track']['lineno']=59;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('content_type', null) : $p['setattr'](self, 'content_type', null); 
				$pyjs['track']['lineno']=60;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['req']['open']('GET', url, true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['url'],['cb']]);
			$cls_definition['asyncGet'] = $method;
			$pyjs['track']['lineno']=62;
			$method = $pyjs__bind_method2('asyncPost', function(url, payload, cb, content_type) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 3 || arguments['length'] > 4)) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 5, arguments['length']+1);
				} else {
					var self = arguments[0];
					url = arguments[1];
					payload = arguments[2];
					cb = arguments[3];
					content_type = arguments[4];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 4 || arguments['length'] > 5)) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 5, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '1a4497aa35066d977157cc4bf9d81311') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof content_type == 'undefined') content_type=arguments['callee']['__args__'][6][1];

				$pyjs['track']={'module':'network', 'lineno':62};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=62;
				$pyjs['track']['lineno']=70;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('cb', cb) : $p['setattr'](self, 'cb', cb); 
				$pyjs['track']['lineno']=71;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('type', 'POST') : $p['setattr'](self, 'type', 'POST'); 
				$pyjs['track']['lineno']=72;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('payload', payload) : $p['setattr'](self, 'payload', payload); 
				$pyjs['track']['lineno']=73;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('content_type', content_type) : $p['setattr'](self, 'content_type', content_type); 
				$pyjs['track']['lineno']=74;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['req']['open']('POST', url, true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['url'],['payload'],['cb'],['content_type', null]]);
			$cls_definition['asyncPost'] = $method;
			$pyjs['track']['lineno']=76;
			$method = $pyjs__bind_method2('onReadyStateChange', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '1a4497aa35066d977157cc4bf9d81311') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}
				var $and1,$and3,$and4,$and5,$and6,$and2;
				$pyjs['track']={'module':'network', 'lineno':76};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=76;
				$pyjs['track']['lineno']=80;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and1=$p['op_eq']($p['getattr']($p['getattr'](self, 'req'), 'readyState'), $constant_int_1))?!$p['bool']($p['getattr'](self, 'hasBeenSent')):$and1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_14_err){if (!$p['isinstance']($pyjs_dbg_14_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_14_err);}throw $pyjs_dbg_14_err;
}})()) {
					$pyjs['track']['lineno']=81;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('hasBeenSent', true) : $p['setattr'](self, 'hasBeenSent', true); 
					$pyjs['track']['lineno']=83;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and3=$p['op_eq']($p['getattr'](self, 'type'), 'POST'))?!$p['op_is']($p['getattr'](self, 'content_type'), null):$and3));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})()) {
						$pyjs['track']['lineno']=84;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['req']['setRequestHeader']('Content-Type', $p['getattr'](self, 'content_type'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_16_err){if (!$p['isinstance']($pyjs_dbg_16_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_16_err);}throw $pyjs_dbg_16_err;
}})();
					}
					$pyjs['track']['lineno']=86;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['req']['send']($p['getattr'](self, 'payload'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_17_err){if (!$p['isinstance']($pyjs_dbg_17_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_17_err);}throw $pyjs_dbg_17_err;
}})();
				}
				$pyjs['track']['lineno']=88;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr']($p['getattr'](self, 'req'), 'readyState'), $constant_int_4));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})()) {
					$pyjs['track']['lineno']=89;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](($p['bool']($and5=((($p['cmp']($p['getattr']($p['getattr'](self, 'req'), 'status'), $constant_int_200))|1) == 1))?($p['cmp']($p['getattr']($p['getattr'](self, 'req'), 'status'), $constant_int_300) == -1):$and5));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_19_err){if (!$p['isinstance']($pyjs_dbg_19_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_19_err);}throw $pyjs_dbg_19_err;
}})()) {
						$pyjs['track']['lineno']=90;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['cb']['onCompletion']($p['getattr']($p['getattr'](self, 'req'), 'responseText'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})();
					}
					else {
						$pyjs['track']['lineno']=92;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['cb']['onError']($p['getattr']($p['getattr'](self, 'req'), 'responseText'), $p['getattr']($p['getattr'](self, 'req'), 'status'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})();
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['onReadyStateChange'] = $method;
			$pyjs['track']['lineno']=37;
			var $bases = new Array($p['object']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('HTTPRequest', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=94;
		$m['NetworkService'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'network';
			$cls_definition['__md5__'] = '0a688dabaf38b882f9f19e7c6cbf454b';
			$pyjs['track']['lineno']=101;
			$cls_definition['changeListeners'] = (function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})();
			$pyjs['track']['lineno']=102;
			$cls_definition['_cache'] = (function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})();
			$pyjs['track']['lineno']=105;
			$method = $pyjs__bind_method2('notifyChange', function(modul) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				var $augexpr1,c,$iter1_nextval,$iter1_type,$augsub1,$iter1_iter,$add2,$add1,$iter1_array,$pyjs__trackstack_size_1,$iter1_idx;
				$pyjs['track']={'module':'network', 'lineno':105};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=105;
				$pyjs['track']['lineno']=113;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['_cache']['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})()['__contains__'](modul));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})()) {
					$pyjs['track']['lineno']=114;
					var $augsub1 = modul;
					var $augexpr1 = $p['getattr']($m['NetworkService'], '_cache');
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $augexpr1['__setitem__']($augsub1, $p['__op_add']($add1=$augexpr1['__getitem__']($augsub1),$add2=$constant_int_1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_26_err){if (!$p['isinstance']($pyjs_dbg_26_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_26_err);}throw $pyjs_dbg_26_err;
}})();
				}
				$pyjs['track']['lineno']=115;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter1_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr']($m['NetworkService'], 'changeListeners');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_27_err){if (!$p['isinstance']($pyjs_dbg_27_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_27_err);}throw $pyjs_dbg_27_err;
}})();
				$iter1_nextval=$p['__iter_prepare']($iter1_iter,false);
				while (typeof($p['__wrapped_next']($iter1_nextval)['$nextval']) != 'undefined') {
					c = $iter1_nextval['$nextval'];
					$pyjs['track']['lineno']=116;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return c['onDataChanged'](modul);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_28_err){if (!$p['isinstance']($pyjs_dbg_28_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_28_err);}throw $pyjs_dbg_28_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='network';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 3, [null,null,['modul']]);
			$cls_definition['notifyChange'] = $method;
			$pyjs['track']['lineno']=119;
			$method = $pyjs__bind_method2('registerChangeListener', function(listener) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);

				$pyjs['track']={'module':'network', 'lineno':119};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=119;
				$pyjs['track']['lineno']=128;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr']($m['NetworkService'], 'changeListeners')['__contains__'](listener));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_29_err){if (!$p['isinstance']($pyjs_dbg_29_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_29_err);}throw $pyjs_dbg_29_err;
}})()) {
					$pyjs['track']['lineno']=129;
					$pyjs['track']['lineno']=129;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=130;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['changeListeners']['append'](listener);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_30_err){if (!$p['isinstance']($pyjs_dbg_30_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_30_err);}throw $pyjs_dbg_30_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 3, [null,null,['listener']]);
			$cls_definition['registerChangeListener'] = $method;
			$pyjs['track']['lineno']=133;
			$method = $pyjs__bind_method2('removeChangeListener', function(listener) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);

				$pyjs['track']={'module':'network', 'lineno':133};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=133;
				$pyjs['track']['lineno']=139;
				if (!( $p['getattr']($m['NetworkService'], 'changeListeners')['__contains__'](listener) )) {
				   throw $p['AssertionError']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['sprintf']('Attempt to remove unregistered listener %s', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['str'](listener);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_31_err){if (!$p['isinstance']($pyjs_dbg_31_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_31_err);}throw $pyjs_dbg_31_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_32_err){if (!$p['isinstance']($pyjs_dbg_32_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_32_err);}throw $pyjs_dbg_32_err;
}})());
				 }
				$pyjs['track']['lineno']=140;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService']['changeListeners']['remove'](listener);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_33_err){if (!$p['isinstance']($pyjs_dbg_33_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_33_err);}throw $pyjs_dbg_33_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 3, [null,null,['listener']]);
			$cls_definition['removeChangeListener'] = $method;
			$pyjs['track']['lineno']=143;
			$method = $pyjs__bind_method2('genReqStr', function(params) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				var $iter5_idx,$iter3_iter,$iter6_array,$add76,$add77,$add74,$add75,$add72,$add73,$add70,$add71,$add78,$pyjs__trackstack_size_2,$pyjs__trackstack_size_1,$iter5_nextval,$add49,$iter5_array,value,$iter6_iter,$add65,$add64,$add67,$add66,$add61,$add60,$add63,$add62,res,$add69,$add68,boundary,$iter6_idx,key,$add50,$add51,$add52,$add53,$add54,$add55,$add56,$add57,$add58,$add59,k,$add45,encoding,$iter6_type,$iter3_array,$iter5_type,boundary_str,val,$add48,$add47,$add46,$iter3_nextval,$add44,$add43,$add42,$add41,$add40,type,$add38,$add39,$add32,$add33,$add30,$add31,$add36,$add37,$add34,$add35,$add7,$add13,v,$add8,$add9,$iter5_iter,$iter6_nextval,$add29,$add28,$iter3_idx,$add21,$add20,$add23,$add22,$add25,$add24,$add27,$add26,$pyjs_try_err,$or1,$or2,$add14,$add15,$add16,$add17,$add10,$add11,$add12,$iter3_type,$add18,$add19;
				$pyjs['track']={'module':'network', 'lineno':143};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=143;
				$pyjs['track']['lineno']=151;
				boundary_str = $p['__op_add']($add7='---',$add8=(function(){try{try{$pyjs['in_try_except'] += 1;
				return ''['join'](function(){
					var $iter2_nextval,$iter2_type,$iter2_iter,$add5,$collcomp1,$add3,$iter2_idx,$add6,$add4,$pyjs__trackstack_size_1,x,$iter2_array;
	$collcomp1 = $p['list']();
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter2_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['range']($constant_int_13);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_34_err){if (!$p['isinstance']($pyjs_dbg_34_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_34_err);}throw $pyjs_dbg_34_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})();
				$iter2_nextval=$p['__iter_prepare']($iter2_iter,false);
				while (typeof($p['__wrapped_next']($iter2_nextval)['$nextval']) != 'undefined') {
					x = $iter2_nextval['$nextval'];
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $collcomp1['append']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['random']['choice']($p['__op_add']($add5=$p['__op_add']($add3=$p['getattr']($m['string'], 'ascii_lowercase'),$add4=$p['getattr']($m['string'], 'ascii_uppercase')),$add6=$p['getattr']($m['string'], 'digits')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_36_err){if (!$p['isinstance']($pyjs_dbg_36_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_36_err);}throw $pyjs_dbg_36_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_37_err){if (!$p['isinstance']($pyjs_dbg_37_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_37_err);}throw $pyjs_dbg_37_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='network';

	return $collcomp1;}());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_38_err){if (!$p['isinstance']($pyjs_dbg_38_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_38_err);}throw $pyjs_dbg_38_err;
}})());
				$pyjs['track']['lineno']=152;
				boundary = boundary_str;
				$pyjs['track']['lineno']=153;
				res = $p['__op_add']($add11=$p['__op_add']($add9='Content-Type: multipart/mixed; boundary="',$add10=boundary),$add12='"\r\nMIME-Version: 1.0\r\n');
				$pyjs['track']['lineno']=154;
				res = $p['__op_add']($add15=res,$add16=$p['__op_add']($add13='\r\n--',$add14=boundary));
				$pyjs['track']['lineno']=155;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter3_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']((function(){try{try{$pyjs['in_try_except'] += 1;
				return params['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_40_err){if (!$p['isinstance']($pyjs_dbg_40_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_40_err);}throw $pyjs_dbg_40_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_41_err){if (!$p['isinstance']($pyjs_dbg_41_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_41_err);}throw $pyjs_dbg_41_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_42_err){if (!$p['isinstance']($pyjs_dbg_42_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_42_err);}throw $pyjs_dbg_42_err;
}})();
				$iter3_nextval=$p['__iter_prepare']($iter3_iter,false);
				while (typeof($p['__wrapped_next']($iter3_nextval)['$nextval']) != 'undefined') {
					var $tupleassign1 = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['__ass_unpack']($iter3_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_39_err){if (!$p['isinstance']($pyjs_dbg_39_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_39_err);}throw $pyjs_dbg_39_err;
}})();
					key = $tupleassign1[0];
					value = $tupleassign1[1];
					$pyjs['track']['lineno']=156;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['all'](function(){
						var $iter4_nextval,$collcomp2,$iter4_idx,$pyjs__trackstack_size_2,$iter4_type,$iter4_array,x,$iter4_iter;
	$collcomp2 = $p['list']();
					$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
					$iter4_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list'](['name', 'read']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_43_err){if (!$p['isinstance']($pyjs_dbg_43_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_43_err);}throw $pyjs_dbg_43_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_44_err){if (!$p['isinstance']($pyjs_dbg_44_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_44_err);}throw $pyjs_dbg_44_err;
}})();
					$iter4_nextval=$p['__iter_prepare']($iter4_iter,false);
					while (typeof($p['__wrapped_next']($iter4_nextval)['$nextval']) != 'undefined') {
						x = $iter4_nextval['$nextval'];
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $collcomp2['append']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['dir'](value);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_45_err){if (!$p['isinstance']($pyjs_dbg_45_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_45_err);}throw $pyjs_dbg_45_err;
}})()['__contains__'](x));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_46_err){if (!$p['isinstance']($pyjs_dbg_46_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_46_err);}throw $pyjs_dbg_46_err;
}})();
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='network';

	return $collcomp2;}());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_48_err){if (!$p['isinstance']($pyjs_dbg_48_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_48_err);}throw $pyjs_dbg_48_err;
}})()) {
						$pyjs['track']['lineno']=157;
						var $pyjs__trackstack_size_2 = $pyjs['trackstack']['length'];
						try {
							try {
								$pyjs['in_try_except'] += 1;
								$pyjs['track']['lineno']=158;
								var $tupleassign2 = (function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['__ass_unpack']((function(){try{try{$pyjs['in_try_except'] += 1;
								return $pyjs_kwargs_call($m['mimetypes'], 'guess_type', null, null, [{'strict':false}, (function(){try{try{$pyjs['in_try_except'] += 1;
								return value['$$name']['decode']((function(){try{try{$pyjs['in_try_except'] += 1;
								return $m['sys']['getfilesystemencoding']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_49_err){if (!$p['isinstance']($pyjs_dbg_49_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_49_err);}throw $pyjs_dbg_49_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_50_err){if (!$p['isinstance']($pyjs_dbg_50_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_50_err);}throw $pyjs_dbg_50_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_51_err){if (!$p['isinstance']($pyjs_dbg_51_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_51_err);}throw $pyjs_dbg_51_err;
}})(), 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_52_err){if (!$p['isinstance']($pyjs_dbg_52_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_52_err);}throw $pyjs_dbg_52_err;
}})();
								type = $tupleassign2[0];
								encoding = $tupleassign2[1];
								$pyjs['track']['lineno']=159;
								type = ($p['bool']($or1=type)?$or1:'application/octet-stream');
							} finally { $pyjs['in_try_except'] -= 1; }
						} catch($pyjs_try_err) {
							$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_2 - 1);
							$pyjs['__active_exception_stack__'] = null;
							$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
							var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
							$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
							if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
								$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
								$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
							}
							$pyjs['track']['module']='network';
							if (true) {
								$pyjs['track']['lineno']=161;
								type = 'application/octet-stream';
							}
						}
						$pyjs['track']['lineno']=162;
						res = $p['__op_add']($add29=res,$add30=$p['__op_add']($add27=$p['__op_add']($add25=$p['__op_add']($add23=$p['__op_add']($add21=$p['__op_add']($add19=$p['__op_add']($add17='\r\nContent-Type: ',$add18=type),$add20='\r\nMIME-Version: 1.0\r\nContent-Disposition: form-data; name="'),$add22=key),$add24='"; filename="'),$add26=(function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['os']['path']['basename']($p['getattr'](value, '$$name'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_53_err){if (!$p['isinstance']($pyjs_dbg_53_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_53_err);}throw $pyjs_dbg_53_err;
}})()['decode']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['sys']['getfilesystemencoding']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_54_err){if (!$p['isinstance']($pyjs_dbg_54_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_54_err);}throw $pyjs_dbg_54_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_55_err){if (!$p['isinstance']($pyjs_dbg_55_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_55_err);}throw $pyjs_dbg_55_err;
}})()),$add28='"\r\n\r\n'));
						$pyjs['track']['lineno']=163;
						res = $p['__op_add']($add31=res,$add32=(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['str']((function(){try{try{$pyjs['in_try_except'] += 1;
						return value['read']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_56_err){if (!$p['isinstance']($pyjs_dbg_56_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_56_err);}throw $pyjs_dbg_56_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_57_err){if (!$p['isinstance']($pyjs_dbg_57_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_57_err);}throw $pyjs_dbg_57_err;
}})());
						$pyjs['track']['lineno']=164;
						res = $p['__op_add']($add35=res,$add36=$p['__op_add']($add33='\r\n--',$add34=boundary));
					}
					else if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](value, $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_58_err){if (!$p['isinstance']($pyjs_dbg_58_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_58_err);}throw $pyjs_dbg_58_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_59_err){if (!$p['isinstance']($pyjs_dbg_59_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_59_err);}throw $pyjs_dbg_59_err;
}})()) {
						$pyjs['track']['lineno']=166;
						$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
						$iter5_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
						return value;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_60_err){if (!$p['isinstance']($pyjs_dbg_60_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_60_err);}throw $pyjs_dbg_60_err;
}})();
						$iter5_nextval=$p['__iter_prepare']($iter5_iter,false);
						while (typeof($p['__wrapped_next']($iter5_nextval)['$nextval']) != 'undefined') {
							val = $iter5_nextval['$nextval'];
							$pyjs['track']['lineno']=167;
							res = $p['__op_add']($add41=res,$add42=$p['__op_add']($add39=$p['__op_add']($add37='\r\nContent-Type: application/octet-stream\r\nMIME-Version: 1.0\r\nContent-Disposition: form-data; name="',$add38=key),$add40='"\r\n\r\n'));
							$pyjs['track']['lineno']=168;
							res = $p['__op_add']($add43=res,$add44=(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['str'](val);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_61_err){if (!$p['isinstance']($pyjs_dbg_61_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_61_err);}throw $pyjs_dbg_61_err;
}})());
							$pyjs['track']['lineno']=169;
							res = $p['__op_add']($add47=res,$add48=$p['__op_add']($add45='\r\n--',$add46=boundary));
						}
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='network';
					}
					else if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](value, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_62_err){if (!$p['isinstance']($pyjs_dbg_62_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_62_err);}throw $pyjs_dbg_62_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_63_err){if (!$p['isinstance']($pyjs_dbg_63_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_63_err);}throw $pyjs_dbg_63_err;
}})()) {
						$pyjs['track']['lineno']=171;
						$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
						$iter6_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return value['items']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_65_err){if (!$p['isinstance']($pyjs_dbg_65_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_65_err);}throw $pyjs_dbg_65_err;
}})();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_66_err){if (!$p['isinstance']($pyjs_dbg_66_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_66_err);}throw $pyjs_dbg_66_err;
}})();
						$iter6_nextval=$p['__iter_prepare']($iter6_iter,false);
						while (typeof($p['__wrapped_next']($iter6_nextval)['$nextval']) != 'undefined') {
							var $tupleassign3 = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['__ass_unpack']($iter6_nextval['$nextval'], 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})();
							k = $tupleassign3[0];
							v = $tupleassign3[1];
							$pyjs['track']['lineno']=172;
							res = $p['__op_add']($add57=res,$add58=$p['__op_add']($add55=$p['__op_add']($add53=$p['__op_add']($add51=$p['__op_add']($add49='\r\nContent-Type: application/octet-stream\r\nMIME-Version: 1.0\r\nContent-Disposition: form-data; name="',$add50=key),$add52='.'),$add54=k),$add56='"\r\n\r\n'));
							$pyjs['track']['lineno']=173;
							res = $p['__op_add']($add59=res,$add60=(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['str'](v);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})());
							$pyjs['track']['lineno']=174;
							res = $p['__op_add']($add63=res,$add64=$p['__op_add']($add61='\r\n--',$add62=boundary));
						}
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='network';
					}
					else {
						$pyjs['track']['lineno']=176;
						res = $p['__op_add']($add69=res,$add70=$p['__op_add']($add67=$p['__op_add']($add65='\r\nContent-Type: application/octet-stream\r\nMIME-Version: 1.0\r\nContent-Disposition: form-data; name="',$add66=key),$add68='"\r\n\r\n'));
						$pyjs['track']['lineno']=177;
						res = $p['__op_add']($add71=res,$add72=(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['str'](value);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_68_err){if (!$p['isinstance']($pyjs_dbg_68_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_68_err);}throw $pyjs_dbg_68_err;
}})());
						$pyjs['track']['lineno']=178;
						res = $p['__op_add']($add75=res,$add76=$p['__op_add']($add73='\r\n--',$add74=boundary));
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=179;
				res = $p['__op_add']($add77=res,$add78='--\r\n');
				$pyjs['track']['lineno']=180;
				$pyjs['track']['lineno']=180;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['tuple']([res, boundary]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_69_err){if (!$p['isinstance']($pyjs_dbg_69_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_69_err);}throw $pyjs_dbg_69_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['params']]);
			$cls_definition['genReqStr'] = $method;
			$pyjs['track']['lineno']=184;
			$method = $pyjs__bind_method2('decode', function(req) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);

				$pyjs['track']={'module':'network', 'lineno':184};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=184;
				$pyjs['track']['lineno']=190;
				$pyjs['track']['lineno']=190;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['json']['loads']($p['getattr'](req, 'result'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_70_err){if (!$p['isinstance']($pyjs_dbg_70_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_70_err);}throw $pyjs_dbg_70_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['req']]);
			$cls_definition['decode'] = $method;
			$pyjs['track']['lineno']=195;
			$method = $pyjs__bind_method2('urlForArgs', function(modul, path, cacheable) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var $and8,$add82,$add80,$and7,cacheKey,$add79,$add81;
				$pyjs['track']={'module':'network', 'lineno':195};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=195;
				$pyjs['track']['lineno']=209;
				cacheKey = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['time']['time']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_71_err){if (!$p['isinstance']($pyjs_dbg_71_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_71_err);}throw $pyjs_dbg_71_err;
}})();
				$pyjs['track']['lineno']=210;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and7=cacheable)?modul:$and7));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_72_err){if (!$p['isinstance']($pyjs_dbg_72_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_72_err);}throw $pyjs_dbg_72_err;
}})()) {
					$pyjs['track']['lineno']=211;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['NetworkService']['_cache']['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_73_err){if (!$p['isinstance']($pyjs_dbg_73_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_73_err);}throw $pyjs_dbg_73_err;
}})()['__contains__'](modul)));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_74_err){if (!$p['isinstance']($pyjs_dbg_74_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_74_err);}throw $pyjs_dbg_74_err;
}})()) {
						$pyjs['track']['lineno']=212;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr']($m['NetworkService'], '_cache')['__setitem__'](modul, $constant_int_1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})();
					}
					$pyjs['track']['lineno']=213;
					cacheKey = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('c%s', $p['getattr']($m['NetworkService'], '_cache')['__getitem__'](modul));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})();
				}
				$pyjs['track']['lineno']=214;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](modul);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_77_err){if (!$p['isinstance']($pyjs_dbg_77_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_77_err);}throw $pyjs_dbg_77_err;
}})()) {
					$pyjs['track']['lineno']=215;
					$pyjs['track']['lineno']=215;
					var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['sprintf']('/admin/%s/%s?_unused_time_stamp=%s', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['tuple']([modul, path, cacheKey]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_78_err){if (!$p['isinstance']($pyjs_dbg_78_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_78_err);}throw $pyjs_dbg_78_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_79_err){if (!$p['isinstance']($pyjs_dbg_79_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_79_err);}throw $pyjs_dbg_79_err;
}})();
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				else {
					$pyjs['track']['lineno']=217;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](path['__contains__']('?'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_80_err){if (!$p['isinstance']($pyjs_dbg_80_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_80_err);}throw $pyjs_dbg_80_err;
}})()) {
						$pyjs['track']['lineno']=218;
						$pyjs['track']['lineno']=218;
						var $pyjs__ret = $p['__op_add']($add79=path,$add80=(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('&_unused_time_stamp=%s', cacheKey);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_81_err){if (!$p['isinstance']($pyjs_dbg_81_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_81_err);}throw $pyjs_dbg_81_err;
}})());
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
					else {
						$pyjs['track']['lineno']=220;
						$pyjs['track']['lineno']=220;
						var $pyjs__ret = $p['__op_add']($add81=path,$add82=(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('?_unused_time_stamp=%s', cacheKey);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_82_err){if (!$p['isinstance']($pyjs_dbg_82_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_82_err);}throw $pyjs_dbg_82_err;
}})());
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 3, [null,null,['modul'],['path'],['cacheable']]);
			$cls_definition['urlForArgs'] = $method;
			$pyjs['track']['lineno']=222;
			$method = $pyjs__bind_method2('__init__', function(modul, url, params, successHandler, failureHandler, finishedHandler, modifies, cacheable, secure) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 9) $pyjs__exception_func_param(arguments['callee']['__name__'], 10, 10, arguments['length']+1);
				} else {
					var self = arguments[0];
					modul = arguments[1];
					url = arguments[2];
					params = arguments[3];
					successHandler = arguments[4];
					failureHandler = arguments[5];
					finishedHandler = arguments[6];
					modifies = arguments[7];
					cacheable = arguments[8];
					secure = arguments[9];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 10) $pyjs__exception_func_param(arguments['callee']['__name__'], 10, 10, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0a688dabaf38b882f9f19e7c6cbf454b') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'network', 'lineno':222};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=222;
				$pyjs['track']['lineno']=227;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['NetworkService'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_83_err){if (!$p['isinstance']($pyjs_dbg_83_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_83_err);}throw $pyjs_dbg_83_err;
}})()['__init__']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_84_err){if (!$p['isinstance']($pyjs_dbg_84_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_84_err);}throw $pyjs_dbg_84_err;
}})();
				$pyjs['track']['lineno']=228;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('result', null) : $p['setattr'](self, 'result', null); 
				$pyjs['track']['lineno']=229;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('status', 'running') : $p['setattr'](self, 'status', 'running'); 
				$pyjs['track']['lineno']=230;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('waitingForSkey', false) : $p['setattr'](self, 'waitingForSkey', false); 
				$pyjs['track']['lineno']=231;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=232;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('url', url) : $p['setattr'](self, 'url', url); 
				$pyjs['track']['lineno']=233;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('params', params) : $p['setattr'](self, 'params', params); 
				$pyjs['track']['lineno']=234;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('successHandler', ($p['bool'](successHandler)? ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([successHandler]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})()) : ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})()))) : $p['setattr'](self, 'successHandler', ($p['bool'](successHandler)? ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([successHandler]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})()) : ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})()))); 
				$pyjs['track']['lineno']=235;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('failureHandler', ($p['bool'](failureHandler)? ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([failureHandler]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})()) : ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})()))) : $p['setattr'](self, 'failureHandler', ($p['bool'](failureHandler)? ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([failureHandler]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})()) : ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})()))); 
				$pyjs['track']['lineno']=236;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('finishedHandler', ($p['bool'](finishedHandler)? ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([finishedHandler]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_89_err){if (!$p['isinstance']($pyjs_dbg_89_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_89_err);}throw $pyjs_dbg_89_err;
}})()) : ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})()))) : $p['setattr'](self, 'finishedHandler', ($p['bool'](finishedHandler)? ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([finishedHandler]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_89_err){if (!$p['isinstance']($pyjs_dbg_89_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_89_err);}throw $pyjs_dbg_89_err;
}})()) : ((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})()))); 
				$pyjs['track']['lineno']=237;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modifies', modifies) : $p['setattr'](self, 'modifies', modifies); 
				$pyjs['track']['lineno']=238;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('cacheable', cacheable) : $p['setattr'](self, 'cacheable', cacheable); 
				$pyjs['track']['lineno']=239;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('secure', secure) : $p['setattr'](self, 'secure', secure); 
				$pyjs['track']['lineno']=240;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](secure);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_91_err){if (!$p['isinstance']($pyjs_dbg_91_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_91_err);}throw $pyjs_dbg_91_err;
}})()) {
					$pyjs['track']['lineno']=241;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('waitingForSkey', true) : $p['setattr'](self, 'waitingForSkey', true); 
					$pyjs['track']['lineno']=242;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['doFetch']('/admin/skey', null, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_92_err){if (!$p['isinstance']($pyjs_dbg_92_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_92_err);}throw $pyjs_dbg_92_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=244;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['doFetch']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['NetworkService']['urlForArgs'](modul, url, cacheable);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_93_err){if (!$p['isinstance']($pyjs_dbg_93_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_93_err);}throw $pyjs_dbg_93_err;
}})(), params, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_94_err){if (!$p['isinstance']($pyjs_dbg_94_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_94_err);}throw $pyjs_dbg_94_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['modul'],['url'],['params'],['successHandler'],['failureHandler'],['finishedHandler'],['modifies'],['cacheable'],['secure']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=248;
			$method = $pyjs__bind_method2('request', function(modul, url, params, successHandler, failureHandler, finishedHandler, modifies, cacheable, secure) {
				if ($pyjs['options']['arg_count'] && (arguments['length'] < 2 || arguments['length'] > 9)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 9, arguments['length']);
				if (typeof params == 'undefined') params=arguments['callee']['__args__'][4][1];
				if (typeof successHandler == 'undefined') successHandler=arguments['callee']['__args__'][5][1];
				if (typeof failureHandler == 'undefined') failureHandler=arguments['callee']['__args__'][6][1];
				if (typeof finishedHandler == 'undefined') finishedHandler=arguments['callee']['__args__'][7][1];
				if (typeof modifies == 'undefined') modifies=arguments['callee']['__args__'][8][1];
				if (typeof cacheable == 'undefined') cacheable=arguments['callee']['__args__'][9][1];
				if (typeof secure == 'undefined') secure=arguments['callee']['__args__'][10][1];
				var $and9,$and10;
				$pyjs['track']={'module':'network', 'lineno':248};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=248;
				$pyjs['track']['lineno']=272;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['printFunc']([(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['tuple'](['NS REQUEST', modul, url, params]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_95_err){if (!$p['isinstance']($pyjs_dbg_95_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_95_err);}throw $pyjs_dbg_95_err;
}})()], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_96_err){if (!$p['isinstance']($pyjs_dbg_96_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_96_err);}throw $pyjs_dbg_96_err;
}})();
				$pyjs['track']['lineno']=273;
				if (!( !$p['bool'](($p['bool']($and9=cacheable)?modifies:$and9)) )) {
				   throw $p['AssertionError']('Cannot cache a request modifying data!');
				 }
				$pyjs['track']['lineno']=275;
				$pyjs['track']['lineno']=275;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['NetworkService'](modul, url, params, successHandler, failureHandler, finishedHandler, modifies, cacheable, secure);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_97_err){if (!$p['isinstance']($pyjs_dbg_97_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_97_err);}throw $pyjs_dbg_97_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modul'],['url'],['params', null],['successHandler', null],['failureHandler', null],['finishedHandler', null],['modifies', false],['cacheable', false],['secure', false]]);
			$cls_definition['request'] = $method;
			$pyjs['track']['lineno']=279;
			$method = $pyjs__bind_method2('doFetch', function(url, params, skey) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 4, arguments['length']+1);
				} else {
					var self = arguments[0];
					url = arguments[1];
					params = arguments[2];
					skey = arguments[3];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, 4, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0a688dabaf38b882f9f19e7c6cbf454b') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $add89,$add88,$add90,$add87,$add86,$add85,$add84,$add83,multipart,contentType,boundary;
				$pyjs['track']={'module':'network', 'lineno':279};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=279;
				$pyjs['track']['lineno']=283;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](params);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_98_err){if (!$p['isinstance']($pyjs_dbg_98_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_98_err);}throw $pyjs_dbg_98_err;
}})()) {
					$pyjs['track']['lineno']=284;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](skey);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_99_err){if (!$p['isinstance']($pyjs_dbg_99_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_99_err);}throw $pyjs_dbg_99_err;
}})()) {
						$pyjs['track']['lineno']=285;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return params['__setitem__']('skey', skey);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_100_err){if (!$p['isinstance']($pyjs_dbg_100_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_100_err);}throw $pyjs_dbg_100_err;
}})();
					}
					$pyjs['track']['lineno']=286;
					contentType = null;
					$pyjs['track']['lineno']=287;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](params, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_101_err){if (!$p['isinstance']($pyjs_dbg_101_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_101_err);}throw $pyjs_dbg_101_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_102_err){if (!$p['isinstance']($pyjs_dbg_102_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_102_err);}throw $pyjs_dbg_102_err;
}})()) {
						$pyjs['track']['lineno']=288;
						var $tupleassign4 = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['__ass_unpack']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['NetworkService']['genReqStr'](params);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_103_err){if (!$p['isinstance']($pyjs_dbg_103_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_103_err);}throw $pyjs_dbg_103_err;
}})(), 2, null);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_104_err){if (!$p['isinstance']($pyjs_dbg_104_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_104_err);}throw $pyjs_dbg_104_err;
}})();
						multipart = $tupleassign4[0];
						boundary = $tupleassign4[1];
						$pyjs['track']['lineno']=289;
						contentType = $p['__op_add']($add85=$p['__op_add']($add83='multipart/form-data; boundary=',$add84=boundary),$add86='; charset=utf-8');
					}
					else if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](params, (typeof bytes == "undefined"?$m['bytes']:bytes));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_105_err){if (!$p['isinstance']($pyjs_dbg_105_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_105_err);}throw $pyjs_dbg_105_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_106_err){if (!$p['isinstance']($pyjs_dbg_106_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_106_err);}throw $pyjs_dbg_106_err;
}})()) {
						$pyjs['track']['lineno']=291;
						contentType = 'application/x-www-form-urlencoded';
						$pyjs['track']['lineno']=292;
						multipart = params;
					}
					else {
						$pyjs['track']['lineno']=294;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['printFunc']([params], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_107_err){if (!$p['isinstance']($pyjs_dbg_107_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_107_err);}throw $pyjs_dbg_107_err;
}})();
						$pyjs['track']['lineno']=295;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['printFunc']([(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['type'](params);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_108_err){if (!$p['isinstance']($pyjs_dbg_108_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_108_err);}throw $pyjs_dbg_108_err;
}})()], 1);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_109_err){if (!$p['isinstance']($pyjs_dbg_109_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_109_err);}throw $pyjs_dbg_109_err;
}})();
					}
					$pyjs['track']['lineno']=296;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['HTTPRequest']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_110_err){if (!$p['isinstance']($pyjs_dbg_110_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_110_err);}throw $pyjs_dbg_110_err;
}})(), 'asyncPost', null, null, [{'content_type':contentType}, url, multipart, self]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_111_err){if (!$p['isinstance']($pyjs_dbg_111_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_111_err);}throw $pyjs_dbg_111_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=298;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](skey);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_112_err){if (!$p['isinstance']($pyjs_dbg_112_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_112_err);}throw $pyjs_dbg_112_err;
}})()) {
						$pyjs['track']['lineno']=299;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](url['__contains__']('?'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_113_err){if (!$p['isinstance']($pyjs_dbg_113_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_113_err);}throw $pyjs_dbg_113_err;
}})()) {
							$pyjs['track']['lineno']=300;
							url = $p['__op_add']($add87=url,$add88=(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['sprintf']('&skey=%s', skey);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_114_err){if (!$p['isinstance']($pyjs_dbg_114_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_114_err);}throw $pyjs_dbg_114_err;
}})());
						}
						else {
							$pyjs['track']['lineno']=302;
							url = $p['__op_add']($add89=url,$add90=(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['sprintf']('?skey=%s', skey);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_115_err){if (!$p['isinstance']($pyjs_dbg_115_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_115_err);}throw $pyjs_dbg_115_err;
}})());
						}
					}
					$pyjs['track']['lineno']=303;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['HTTPRequest']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_116_err){if (!$p['isinstance']($pyjs_dbg_116_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_116_err);}throw $pyjs_dbg_116_err;
}})()['asyncGet'](url, self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_117_err){if (!$p['isinstance']($pyjs_dbg_117_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_117_err);}throw $pyjs_dbg_117_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['url'],['params'],['skey']]);
			$cls_definition['doFetch'] = $method;
			$pyjs['track']['lineno']=305;
			$method = $pyjs__bind_method2('onCompletion', function(text) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					text = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0a688dabaf38b882f9f19e7c6cbf454b') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter8_idx,$iter7_nextval,$iter7_array,$iter8_array,$iter8_iter,s,$pyjs_try_err,$pyjs__trackstack_size_2,$iter8_nextval,$iter7_idx,$iter7_iter,$iter7_type,$iter8_type;
				$pyjs['track']={'module':'network', 'lineno':305};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=305;
				$pyjs['track']['lineno']=309;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'waitingForSkey'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_118_err){if (!$p['isinstance']($pyjs_dbg_118_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_118_err);}throw $pyjs_dbg_118_err;
}})()) {
					$pyjs['track']['lineno']=310;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('waitingForSkey', false) : $p['setattr'](self, 'waitingForSkey', false); 
					$pyjs['track']['lineno']=311;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['doFetch']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['NetworkService']['urlForArgs']($p['getattr'](self, 'modul'), $p['getattr'](self, 'url'), $p['getattr'](self, 'cacheable'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_119_err){if (!$p['isinstance']($pyjs_dbg_119_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_119_err);}throw $pyjs_dbg_119_err;
}})(), $p['getattr'](self, 'params'), (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['json']['loads'](text);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_120_err){if (!$p['isinstance']($pyjs_dbg_120_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_120_err);}throw $pyjs_dbg_120_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_121_err){if (!$p['isinstance']($pyjs_dbg_121_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_121_err);}throw $pyjs_dbg_121_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=314;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('result', text) : $p['setattr'](self, 'result', text); 
					$pyjs['track']['lineno']=315;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('status', 'succeeded') : $p['setattr'](self, 'status', 'succeeded'); 
					$pyjs['track']['lineno']=316;
					var $pyjs__trackstack_size_1 = $pyjs['trackstack']['length'];
					try {
						try {
							$pyjs['in_try_except'] += 1;
							$pyjs['track']['lineno']=317;
							$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
							$iter7_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['getattr'](self, 'successHandler');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_122_err){if (!$p['isinstance']($pyjs_dbg_122_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_122_err);}throw $pyjs_dbg_122_err;
}})();
							$iter7_nextval=$p['__iter_prepare']($iter7_iter,false);
							while (typeof($p['__wrapped_next']($iter7_nextval)['$nextval']) != 'undefined') {
								s = $iter7_nextval['$nextval'];
								$pyjs['track']['lineno']=318;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return s(self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_123_err){if (!$p['isinstance']($pyjs_dbg_123_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_123_err);}throw $pyjs_dbg_123_err;
}})();
							}
							if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
								$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
								$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
							}
							$pyjs['track']['module']='network';
							$pyjs['track']['lineno']=319;
							$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
							$iter8_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['getattr'](self, 'finishedHandler');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_124_err){if (!$p['isinstance']($pyjs_dbg_124_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_124_err);}throw $pyjs_dbg_124_err;
}})();
							$iter8_nextval=$p['__iter_prepare']($iter8_iter,false);
							while (typeof($p['__wrapped_next']($iter8_nextval)['$nextval']) != 'undefined') {
								s = $iter8_nextval['$nextval'];
								$pyjs['track']['lineno']=320;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return s(self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_125_err){if (!$p['isinstance']($pyjs_dbg_125_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_125_err);}throw $pyjs_dbg_125_err;
}})();
							}
							if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
								$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
								$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
							}
							$pyjs['track']['module']='network';
						} finally { $pyjs['in_try_except'] -= 1; }
					} catch($pyjs_try_err) {
						$pyjs['__last_exception_stack__'] = sys['save_exception_stack']($pyjs__trackstack_size_1 - 1);
						$pyjs['__active_exception_stack__'] = null;
						$pyjs_try_err = $p['_errorMapping']($pyjs_try_err);
						var $pyjs_try_err_name = (typeof $pyjs_try_err['__name__'] == 'undefined' ? $pyjs_try_err['name'] : $pyjs_try_err['__name__'] );
						$pyjs['__last_exception__'] = {'error': $pyjs_try_err, 'module': $m};
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='network';
						if (true) {
							$pyjs['track']['lineno']=322;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool']($p['getattr'](self, 'modifies'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_126_err){if (!$p['isinstance']($pyjs_dbg_126_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_126_err);}throw $pyjs_dbg_126_err;
}})()) {
								$pyjs['track']['lineno']=323;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return $pyjs_kwargs_call(null, $m['DeferredCall'], null, null, [{'_delay':$constant_int_2500}, $p['getattr']($m['NetworkService'], 'notifyChange'), $p['getattr'](self, 'modul')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_127_err){if (!$p['isinstance']($pyjs_dbg_127_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_127_err);}throw $pyjs_dbg_127_err;
}})();
							}
							$pyjs['track']['lineno']=325;
							$pyjs['__active_exception_stack__'] = $pyjs['__last_exception_stack__'];
							$pyjs['__last_exception_stack__'] = null;
							throw ($pyjs['__last_exception__']?
								$pyjs['__last_exception__']['error']:
								$p['TypeError']('exceptions must be classes, instances, or strings (deprecated), not NoneType'));
						}
					}
					$pyjs['track']['lineno']=327;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('successHandler', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_128_err){if (!$p['isinstance']($pyjs_dbg_128_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_128_err);}throw $pyjs_dbg_128_err;
}})()) : $p['setattr'](self, 'successHandler', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_128_err){if (!$p['isinstance']($pyjs_dbg_128_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_128_err);}throw $pyjs_dbg_128_err;
}})()); 
					$pyjs['track']['lineno']=328;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('finishedHandler', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_129_err){if (!$p['isinstance']($pyjs_dbg_129_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_129_err);}throw $pyjs_dbg_129_err;
}})()) : $p['setattr'](self, 'finishedHandler', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_129_err){if (!$p['isinstance']($pyjs_dbg_129_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_129_err);}throw $pyjs_dbg_129_err;
}})()); 
					$pyjs['track']['lineno']=329;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('failureHandler', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_130_err){if (!$p['isinstance']($pyjs_dbg_130_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_130_err);}throw $pyjs_dbg_130_err;
}})()) : $p['setattr'](self, 'failureHandler', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_130_err){if (!$p['isinstance']($pyjs_dbg_130_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_130_err);}throw $pyjs_dbg_130_err;
}})()); 
					$pyjs['track']['lineno']=330;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('params', null) : $p['setattr'](self, 'params', null); 
					$pyjs['track']['lineno']=331;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'modifies'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_131_err){if (!$p['isinstance']($pyjs_dbg_131_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_131_err);}throw $pyjs_dbg_131_err;
}})()) {
						$pyjs['track']['lineno']=332;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call(null, $m['DeferredCall'], null, null, [{'_delay':$constant_int_2500}, $p['getattr']($m['NetworkService'], 'notifyChange'), $p['getattr'](self, 'modul')]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_132_err){if (!$p['isinstance']($pyjs_dbg_132_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_132_err);}throw $pyjs_dbg_132_err;
}})();
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['text']]);
			$cls_definition['onCompletion'] = $method;
			$pyjs['track']['lineno']=334;
			$method = $pyjs__bind_method2('onError', function(text, code) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					text = arguments[1];
					code = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0a688dabaf38b882f9f19e7c6cbf454b') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $iter9_iter,$iter10_idx,$iter9_nextval,$iter9_idx,$iter10_array,$iter9_array,s,$iter10_nextval,$pyjs__trackstack_size_1,$iter10_type,$iter10_iter,$iter9_type;
				$pyjs['track']={'module':'network', 'lineno':334};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=334;
				$pyjs['track']['lineno']=338;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('status', 'failed') : $p['setattr'](self, 'status', 'failed'); 
				$pyjs['track']['lineno']=339;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('result', text) : $p['setattr'](self, 'result', text); 
				$pyjs['track']['lineno']=340;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter9_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'failureHandler');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_133_err){if (!$p['isinstance']($pyjs_dbg_133_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_133_err);}throw $pyjs_dbg_133_err;
}})();
				$iter9_nextval=$p['__iter_prepare']($iter9_iter,false);
				while (typeof($p['__wrapped_next']($iter9_nextval)['$nextval']) != 'undefined') {
					s = $iter9_nextval['$nextval'];
					$pyjs['track']['lineno']=341;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return s(self, code);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_134_err){if (!$p['isinstance']($pyjs_dbg_134_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_134_err);}throw $pyjs_dbg_134_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=342;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter10_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'finishedHandler');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_135_err){if (!$p['isinstance']($pyjs_dbg_135_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_135_err);}throw $pyjs_dbg_135_err;
}})();
				$iter10_nextval=$p['__iter_prepare']($iter10_iter,false);
				while (typeof($p['__wrapped_next']($iter10_nextval)['$nextval']) != 'undefined') {
					s = $iter10_nextval['$nextval'];
					$pyjs['track']['lineno']=343;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return s(self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_136_err){if (!$p['isinstance']($pyjs_dbg_136_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_136_err);}throw $pyjs_dbg_136_err;
}})();
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=344;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('successHandler', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_137_err){if (!$p['isinstance']($pyjs_dbg_137_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_137_err);}throw $pyjs_dbg_137_err;
}})()) : $p['setattr'](self, 'successHandler', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_137_err){if (!$p['isinstance']($pyjs_dbg_137_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_137_err);}throw $pyjs_dbg_137_err;
}})()); 
				$pyjs['track']['lineno']=345;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('finishedHandler', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_138_err){if (!$p['isinstance']($pyjs_dbg_138_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_138_err);}throw $pyjs_dbg_138_err;
}})()) : $p['setattr'](self, 'finishedHandler', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_138_err){if (!$p['isinstance']($pyjs_dbg_138_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_138_err);}throw $pyjs_dbg_138_err;
}})()); 
				$pyjs['track']['lineno']=346;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('failureHandler', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_139_err){if (!$p['isinstance']($pyjs_dbg_139_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_139_err);}throw $pyjs_dbg_139_err;
}})()) : $p['setattr'](self, 'failureHandler', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_139_err){if (!$p['isinstance']($pyjs_dbg_139_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_139_err);}throw $pyjs_dbg_139_err;
}})()); 
				$pyjs['track']['lineno']=347;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('params', null) : $p['setattr'](self, 'params', null); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['text'],['code']]);
			$cls_definition['onError'] = $method;
			$pyjs['track']['lineno']=349;
			$method = $pyjs__bind_method2('onTimeout', function(text) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					text = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0a688dabaf38b882f9f19e7c6cbf454b') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'network', 'lineno':349};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='network';
				$pyjs['track']['lineno']=349;
				$pyjs['track']['lineno']=353;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['onError'](text, (typeof ($usub1=$constant_int_1)=='number'?
					-$usub1:
					$p['op_usub']($usub1)));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_140_err){if (!$p['isinstance']($pyjs_dbg_140_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_140_err);}throw $pyjs_dbg_140_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['text']]);
			$cls_definition['onTimeout'] = $method;
			$pyjs['track']['lineno']=94;
			var $bases = new Array($p['object']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('NetworkService', $p['tuple']($bases), $data);
		})();
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end network */


/* end module: network */


/*
PYJS_DEPS: ['sys', 'os', 'json', 'string', 'random', 'time']
*/
