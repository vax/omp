/* start module: bones.string */
$pyjs['loaded_modules']['bones.string'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['bones.string']['__was_initialized__']) return $pyjs['loaded_modules']['bones.string'];
	if(typeof $pyjs['loaded_modules']['bones'] == 'undefined' || !$pyjs['loaded_modules']['bones']['__was_initialized__']) $p['___import___']('bones', null);
	var $m = $pyjs['loaded_modules']['bones.string'];
	$m['__repr__'] = function() { return '<module: bones.string>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'bones.string';
	$m['__name__'] = __mod_name__;
	$m.__track_lines__ = new Array();
	$pyjs['loaded_modules']['bones']['string'] = $pyjs['loaded_modules']['bones.string'];
	try {
		$m.__track_lines__[1] = 'bones.string.py, line 1:\n    #!/usr/bin/env python2';
		$m.__track_lines__[3] = 'bones.string.py, line 3:\n    import html5';
		$m.__track_lines__[4] = 'bones.string.py, line 4:\n    from priorityqueue import editBoneSelector, viewDelegateSelector, extendedSearchWidgetSelector, extractorDelegateSelector';
		$m.__track_lines__[5] = 'bones.string.py, line 5:\n    from config import conf';
		$m.__track_lines__[6] = 'bones.string.py, line 6:\n    from event import EventDispatcher';
		$m.__track_lines__[7] = 'bones.string.py, line 7:\n    from html5.keycodes import *';
		$m.__track_lines__[8] = 'bones.string.py, line 8:\n    from i18n import translate';
		$m.__track_lines__[11] = 'bones.string.py, line 11:\n    class StringBoneExtractor(object):';
		$m.__track_lines__[12] = 'bones.string.py, line 12:\n    def __init__(self, modulName, boneName, skelStructure, *args, **kwargs):';
		$m.__track_lines__[13] = 'bones.string.py, line 13:\n    super(StringBoneExtractor, self).__init__()';
		$m.__track_lines__[14] = 'bones.string.py, line 14:\n    self.skelStructure = skelStructure';
		$m.__track_lines__[15] = 'bones.string.py, line 15:\n    self.boneName = boneName';
		$m.__track_lines__[16] = 'bones.string.py, line 16:\n    self.modulName = modulName';
		$m.__track_lines__[18] = 'bones.string.py, line 18:\n    def render(self, data, field):';
		$m.__track_lines__[19] = 'bones.string.py, line 19:\n    if field in data.keys():';
		$m.__track_lines__[21] = 'bones.string.py, line 21:\n    if isinstance(data[field], dict):';
		$m.__track_lines__[22] = 'bones.string.py, line 22:\n    resstr = ""';
		$m.__track_lines__[23] = 'bones.string.py, line 23:\n    if "currentlanguage" in conf.keys():';
		$m.__track_lines__[24] = 'bones.string.py, line 24:\n    if conf["currentlanguage"] in data[field].keys():';
		$m.__track_lines__[25] = 'bones.string.py, line 25:\n    resstr = data[field][conf["currentlanguage"]].replace("&quot;", "\'").replace(";", " ").replace(\'"\', "\'")';
		$m.__track_lines__[27] = 'bones.string.py, line 27:\n    if len(data[field].keys()) > 0:';
		$m.__track_lines__[28] = 'bones.string.py, line 28:\n    resstr = data[field][data[field].keys()[0]].replace("&quot;", "\'").replace(";", " ").replace(\'"\', "\'")';
		$m.__track_lines__[29] = 'bones.string.py, line 29:\n    return \'"%s"\' % resstr';
		$m.__track_lines__[31] = 'bones.string.py, line 31:\n    return ", ".join([item.replace("&quot;", "").replace(";", " ").replace(\'"\', "\'") for item in data[field]])';
		$m.__track_lines__[33] = 'bones.string.py, line 33:\n    return str(\'"%s"\' % data[field].replace("&quot;", "").replace(";", " ").replace(\'"\', "\'"))';
		$m.__track_lines__[34] = 'bones.string.py, line 34:\n    return conf["empty_value"]';
		$m.__track_lines__[37] = 'bones.string.py, line 37:\n    class StringViewBoneDelegate( object ):';
		$m.__track_lines__[38] = 'bones.string.py, line 38:\n    def __init__(self, modulName, boneName, skelStructure, *args, **kwargs ):';
		$m.__track_lines__[39] = 'bones.string.py, line 39:\n    super( StringViewBoneDelegate, self ).__init__()';
		$m.__track_lines__[40] = 'bones.string.py, line 40:\n    self.skelStructure = skelStructure';
		$m.__track_lines__[41] = 'bones.string.py, line 41:\n    self.boneName = boneName';
		$m.__track_lines__[42] = 'bones.string.py, line 42:\n    self.modulName=modulName';
		$m.__track_lines__[44] = 'bones.string.py, line 44:\n    def render( self, data, field ):';
		$m.__track_lines__[45] = 'bones.string.py, line 45:\n    if field in data.keys():';
		$m.__track_lines__[47] = 'bones.string.py, line 47:\n    if isinstance(data[field],dict):';
		$m.__track_lines__[48] = 'bones.string.py, line 48:\n    resstr=""';
		$m.__track_lines__[49] = 'bones.string.py, line 49:\n    if "currentlanguage" in conf.keys():';
		$m.__track_lines__[50] = 'bones.string.py, line 50:\n    if conf["currentlanguage"] in data[field].keys():';
		$m.__track_lines__[51] = 'bones.string.py, line 51:\n    resstr=data[field][conf["currentlanguage"]]';
		$m.__track_lines__[53] = 'bones.string.py, line 53:\n    if len(data[field].keys())>0:';
		$m.__track_lines__[54] = 'bones.string.py, line 54:\n    resstr=data[field][data[field].keys()[0]]';
		$m.__track_lines__[55] = 'bones.string.py, line 55:\n    return (self.getViewElement(resstr,data[field]))';
		$m.__track_lines__[60] = 'bones.string.py, line 60:\n    if isinstance( data[field], list ):';
		$m.__track_lines__[61] = 'bones.string.py, line 61:\n    output = ", ".join( data[field] )';
		$m.__track_lines__[63] = 'bones.string.py, line 63:\n    output=str( data[field] )';
		$m.__track_lines__[65] = 'bones.string.py, line 65:\n    return self.getViewElement( output,False )';
		$m.__track_lines__[67] = 'bones.string.py, line 67:\n    return self.getViewElement( conf[ "empty_value" ], False )';
		$m.__track_lines__[69] = 'bones.string.py, line 69:\n    def getViewElement(self, labelstr, datafield):';
		$m.__track_lines__[70] = 'bones.string.py, line 70:\n    labelstr = html5.utils.unescape(labelstr)';
		$m.__track_lines__[72] = 'bones.string.py, line 72:\n    if not datafield:';
		$m.__track_lines__[73] = 'bones.string.py, line 73:\n    return( html5.Label(labelstr))';
		$m.__track_lines__[75] = 'bones.string.py, line 75:\n    aspan=html5.Span()';
		$m.__track_lines__[76] = 'bones.string.py, line 76:\n    aspan.appendChild(html5.TextNode(labelstr))';
		$m.__track_lines__[77] = 'bones.string.py, line 77:\n    aspan["Title"] = str(datafield)';
		$m.__track_lines__[78] = 'bones.string.py, line 78:\n    return aspan';
		$m.__track_lines__[80] = 'bones.string.py, line 80:\n    class Tag( html5.Span ):';
		$m.__track_lines__[81] = 'bones.string.py, line 81:\n    def __init__(self, tag, isEditMode, readonly=False, *args, **kwargs ):';
		$m.__track_lines__[82] = 'bones.string.py, line 82:\n    super( Tag, self ).__init__( *args, **kwargs )';
		$m.__track_lines__[83] = 'bones.string.py, line 83:\n    self["class"].append("tag")';
		$m.__track_lines__[85] = 'bones.string.py, line 85:\n    self.input = html5.Input()';
		$m.__track_lines__[86] = 'bones.string.py, line 86:\n    self.input["type"] = "text"';
		$m.__track_lines__[87] = 'bones.string.py, line 87:\n    self.input["value"] = tag';
		$m.__track_lines__[88] = 'bones.string.py, line 88:\n    self.appendChild(self.input)';
		$m.__track_lines__[90] = 'bones.string.py, line 90:\n    if readonly:';
		$m.__track_lines__[91] = 'bones.string.py, line 91:\n    self.input[ "readonly" ] = True';
		$m.__track_lines__[93] = 'bones.string.py, line 93:\n    delBtn = html5.ext.Button(translate("Delete"), self.removeMe)';
		$m.__track_lines__[94] = 'bones.string.py, line 94:\n    delBtn["class"].append("icon delete tag")';
		$m.__track_lines__[95] = 'bones.string.py, line 95:\n    self.appendChild(delBtn)';
		$m.__track_lines__[97] = 'bones.string.py, line 97:\n    def removeMe(self, *args, **kwargs):';
		$m.__track_lines__[98] = 'bones.string.py, line 98:\n    self.parent().removeChild( self )';
		$m.__track_lines__[100] = 'bones.string.py, line 100:\n    def focus(self):';
		$m.__track_lines__[101] = 'bones.string.py, line 101:\n    self.input.focus()';
		$m.__track_lines__[103] = 'bones.string.py, line 103:\n    class StringEditBone( html5.Div ):';
		$m.__track_lines__[104] = 'bones.string.py, line 104:\n    def __init__(self, modulName, boneName, readOnly, multiple=False, languages=None, *args, **kwargs ):';
		$m.__track_lines__[105] = 'bones.string.py, line 105:\n    super( StringEditBone,  self ).__init__( *args, **kwargs )';
		$m.__track_lines__[106] = 'bones.string.py, line 106:\n    self.modulName = modulName';
		$m.__track_lines__[107] = 'bones.string.py, line 107:\n    self.boneName = boneName';
		$m.__track_lines__[108] = 'bones.string.py, line 108:\n    self.readOnly = readOnly';
		$m.__track_lines__[109] = 'bones.string.py, line 109:\n    self.multiple = multiple';
		$m.__track_lines__[110] = 'bones.string.py, line 110:\n    self.languages = languages';
		$m.__track_lines__[111] = 'bones.string.py, line 111:\n    self.boneName = boneName';
		$m.__track_lines__[112] = 'bones.string.py, line 112:\n    self.currentLanguage = None';
		$m.__track_lines__[114] = 'bones.string.py, line 114:\n    if self.languages and self.multiple:';
		$m.__track_lines__[115] = 'bones.string.py, line 115:\n    self["class"].append("is_translated")';
		$m.__track_lines__[116] = 'bones.string.py, line 116:\n    self["class"].append("is_multiple")';
		$m.__track_lines__[117] = 'bones.string.py, line 117:\n    self.languagesContainer = html5.Div()';
		$m.__track_lines__[118] = 'bones.string.py, line 118:\n    self.appendChild( self.languagesContainer )';
		$m.__track_lines__[119] = 'bones.string.py, line 119:\n    self.buttonContainer = html5.Div()';
		$m.__track_lines__[120] = 'bones.string.py, line 120:\n    self.buttonContainer["class"] = "languagebuttons"';
		$m.__track_lines__[121] = 'bones.string.py, line 121:\n    self.appendChild( self.buttonContainer )';
		$m.__track_lines__[122] = 'bones.string.py, line 122:\n    self.langEdits = {}';
		$m.__track_lines__[123] = 'bones.string.py, line 123:\n    self.langBtns = {}';
		$m.__track_lines__[125] = 'bones.string.py, line 125:\n    for lang in self.languages:';
		$m.__track_lines__[126] = 'bones.string.py, line 126:\n    tagContainer = html5.Div()';
		$m.__track_lines__[127] = 'bones.string.py, line 127:\n    tagContainer["class"].append("lang_%s" % lang )';
		$m.__track_lines__[128] = 'bones.string.py, line 128:\n    tagContainer["class"].append("tagcontainer")';
		$m.__track_lines__[129] = 'bones.string.py, line 129:\n    tagContainer["style"]["display"] = "none"';
		$m.__track_lines__[131] = 'bones.string.py, line 131:\n    langBtn = html5.ext.Button(lang, callback=self.onLangBtnClicked)';
		$m.__track_lines__[132] = 'bones.string.py, line 132:\n    langBtn.lang = lang';
		$m.__track_lines__[133] = 'bones.string.py, line 133:\n    self.buttonContainer.appendChild(langBtn)';
		$m.__track_lines__[135] = 'bones.string.py, line 135:\n    if not self.readOnly:';
		$m.__track_lines__[136] = 'bones.string.py, line 136:\n    addBtn = html5.ext.Button(translate("New"), callback=self.onBtnGenTag)';
		$m.__track_lines__[137] = 'bones.string.py, line 137:\n    addBtn["class"].append("icon new tag")';
		$m.__track_lines__[138] = 'bones.string.py, line 138:\n    addBtn.lang = lang';
		$m.__track_lines__[139] = 'bones.string.py, line 139:\n    tagContainer.appendChild(addBtn)';
		$m.__track_lines__[141] = 'bones.string.py, line 141:\n    self.languagesContainer.appendChild(tagContainer)';
		$m.__track_lines__[142] = 'bones.string.py, line 142:\n    self.langEdits[lang] = tagContainer';
		$m.__track_lines__[143] = 'bones.string.py, line 143:\n    self.langBtns[lang] = langBtn';
		$m.__track_lines__[145] = 'bones.string.py, line 145:\n    self.setLang(self.languages[0])';
		$m.__track_lines__[148] = 'bones.string.py, line 148:\n    self["class"].append("is_translated")';
		$m.__track_lines__[149] = 'bones.string.py, line 149:\n    self.languagesContainer = html5.Div()';
		$m.__track_lines__[150] = 'bones.string.py, line 150:\n    self.appendChild( self.languagesContainer )';
		$m.__track_lines__[151] = 'bones.string.py, line 151:\n    self.buttonContainer = html5.Div()';
		$m.__track_lines__[152] = 'bones.string.py, line 152:\n    self.buttonContainer["class"] = "languagebuttons"';
		$m.__track_lines__[153] = 'bones.string.py, line 153:\n    self.appendChild( self.buttonContainer )';
		$m.__track_lines__[154] = 'bones.string.py, line 154:\n    self.langEdits = {}';
		$m.__track_lines__[155] = 'bones.string.py, line 155:\n    self.langBtns = {}';
		$m.__track_lines__[157] = 'bones.string.py, line 157:\n    for lang in self.languages:';
		$m.__track_lines__[158] = 'bones.string.py, line 158:\n    langBtn = html5.ext.Button(lang, callback=self.onLangBtnClicked)';
		$m.__track_lines__[159] = 'bones.string.py, line 159:\n    langBtn.lang = lang';
		$m.__track_lines__[160] = 'bones.string.py, line 160:\n    self.buttonContainer.appendChild(langBtn)';
		$m.__track_lines__[162] = 'bones.string.py, line 162:\n    inputField = html5.Input()';
		$m.__track_lines__[163] = 'bones.string.py, line 163:\n    inputField["type"] = "text"';
		$m.__track_lines__[164] = 'bones.string.py, line 164:\n    inputField["style"]["display"] = "none"';
		$m.__track_lines__[165] = 'bones.string.py, line 165:\n    inputField["class"].append("lang_%s" % lang)';
		$m.__track_lines__[167] = 'bones.string.py, line 167:\n    if self.readOnly:';
		$m.__track_lines__[168] = 'bones.string.py, line 168:\n    inputField["readonly"] = True';
		$m.__track_lines__[170] = 'bones.string.py, line 170:\n    self.languagesContainer.appendChild( inputField )';
		$m.__track_lines__[171] = 'bones.string.py, line 171:\n    self.langEdits[lang] = inputField';
		$m.__track_lines__[172] = 'bones.string.py, line 172:\n    self.langBtns[lang] = langBtn';
		$m.__track_lines__[174] = 'bones.string.py, line 174:\n    self.setLang(self.languages[0])';
		$m.__track_lines__[177] = 'bones.string.py, line 177:\n    self["class"].append("is_multiple")';
		$m.__track_lines__[178] = 'bones.string.py, line 178:\n    self.tagContainer = html5.Div()';
		$m.__track_lines__[179] = 'bones.string.py, line 179:\n    self.tagContainer["class"].append("tagcontainer")';
		$m.__track_lines__[180] = 'bones.string.py, line 180:\n    self.appendChild(self.tagContainer)';
		$m.__track_lines__[182] = 'bones.string.py, line 182:\n    if not self.readOnly:';
		$m.__track_lines__[183] = 'bones.string.py, line 183:\n    addBtn = html5.ext.Button(translate("New"), callback=self.onBtnGenTag)';
		$m.__track_lines__[184] = 'bones.string.py, line 184:\n    addBtn.lang = None';
		$m.__track_lines__[185] = 'bones.string.py, line 185:\n    addBtn["class"].append("icon new tag")';
		$m.__track_lines__[187] = 'bones.string.py, line 187:\n    self.tagContainer.appendChild(addBtn)';
		$m.__track_lines__[190] = 'bones.string.py, line 190:\n    self.input = html5.Input()';
		$m.__track_lines__[191] = 'bones.string.py, line 191:\n    self.input["type"] = "text"';
		$m.__track_lines__[192] = 'bones.string.py, line 192:\n    self.appendChild( self.input )';
		$m.__track_lines__[194] = 'bones.string.py, line 194:\n    if self.readOnly:';
		$m.__track_lines__[195] = 'bones.string.py, line 195:\n    self.input["readonly"] = True';
		$m.__track_lines__[198] = 'bones.string.py, line 197:\n    @staticmethod ... def fromSkelStructure( modulName, boneName, skelStructure ):';
		$m.__track_lines__[199] = 'bones.string.py, line 199:\n    readOnly = "readonly" in skelStructure[ boneName ].keys() and skelStructure[ boneName ]["readonly"]';
		$m.__track_lines__[201] = 'bones.string.py, line 201:\n    if boneName in skelStructure.keys():';
		$m.__track_lines__[202] = 'bones.string.py, line 202:\n    if "multiple" in skelStructure[ boneName ].keys():';
		$m.__track_lines__[203] = 'bones.string.py, line 203:\n    multiple = skelStructure[ boneName ]["multiple"]';
		$m.__track_lines__[205] = 'bones.string.py, line 205:\n    multiple = False';
		$m.__track_lines__[206] = 'bones.string.py, line 206:\n    if "languages" in skelStructure[ boneName ].keys():';
		$m.__track_lines__[207] = 'bones.string.py, line 207:\n    languages = skelStructure[ boneName ]["languages"]';
		$m.__track_lines__[209] = 'bones.string.py, line 209:\n    languages = None';
		$m.__track_lines__[210] = 'bones.string.py, line 210:\n    return( StringEditBone( modulName, boneName, readOnly, multiple=multiple, languages=languages ) )';
		$m.__track_lines__[212] = 'bones.string.py, line 212:\n    def onLangBtnClicked(self, btn):';
		$m.__track_lines__[213] = 'bones.string.py, line 213:\n    self.setLang( btn.lang )';
		$m.__track_lines__[215] = 'bones.string.py, line 215:\n    def isFilled(self, lang=None):';
		$m.__track_lines__[216] = 'bones.string.py, line 216:\n    if self.languages:';
		$m.__track_lines__[217] = 'bones.string.py, line 217:\n    if lang is None:';
		$m.__track_lines__[218] = 'bones.string.py, line 218:\n    lang = self.languages[0]';
		$m.__track_lines__[220] = 'bones.string.py, line 220:\n    if self.multiple:';
		$m.__track_lines__[221] = 'bones.string.py, line 221:\n    for item in self.langEdits[lang]._children:';
		$m.__track_lines__[222] = 'bones.string.py, line 222:\n    if isinstance(item, Tag) and item.input["value"]:';
		$m.__track_lines__[223] = 'bones.string.py, line 223:\n    return True';
		$m.__track_lines__[225] = 'bones.string.py, line 225:\n    return False';
		$m.__track_lines__[227] = 'bones.string.py, line 227:\n    return bool(len(self.langEdits[lang]["value"]))';
		$m.__track_lines__[230] = 'bones.string.py, line 230:\n    for item in self.tagContainer._children:';
		$m.__track_lines__[231] = 'bones.string.py, line 231:\n    if isinstance(item, Tag) and item.input["value"]:';
		$m.__track_lines__[232] = 'bones.string.py, line 232:\n    return True';
		$m.__track_lines__[234] = 'bones.string.py, line 234:\n    return False';
		$m.__track_lines__[236] = 'bones.string.py, line 236:\n    return bool(len(self.input["value"]))';
		$m.__track_lines__[238] = 'bones.string.py, line 238:\n    def _updateLanguageButtons(self):';
		$m.__track_lines__[239] = 'bones.string.py, line 239:\n    if not self.languages:';
		$m.__track_lines__[240] = 'bones.string.py, line 240:\n    return';
		$m.__track_lines__[242] = 'bones.string.py, line 242:\n    for lang in self.languages:';
		$m.__track_lines__[243] = 'bones.string.py, line 243:\n    if self.isFilled(lang):';
		$m.__track_lines__[244] = 'bones.string.py, line 244:\n    self.langBtns[lang]["class"].remove("is_unfilled")';
		$m.__track_lines__[245] = 'bones.string.py, line 245:\n    if not "is_filled" in self.langBtns[lang]["class"]:';
		$m.__track_lines__[246] = 'bones.string.py, line 246:\n    self.langBtns[lang]["class"].append("is_filled")';
		$m.__track_lines__[248] = 'bones.string.py, line 248:\n    self.langBtns[lang]["class"].remove("is_filled")';
		$m.__track_lines__[249] = 'bones.string.py, line 249:\n    if not "is_unfilled" in self.langBtns[lang]["class"]:';
		$m.__track_lines__[250] = 'bones.string.py, line 250:\n    self.langBtns[lang]["class"].append("is_unfilled")';
		$m.__track_lines__[252] = 'bones.string.py, line 252:\n    if lang == self.currentLanguage:';
		$m.__track_lines__[256] = 'bones.string.py, line 256:\n    if not "is_active" in self.langBtns[lang]["class"]:';
		$m.__track_lines__[257] = 'bones.string.py, line 257:\n    self.langBtns[lang]["class"].append("is_active")';
		$m.__track_lines__[259] = 'bones.string.py, line 259:\n    self.langBtns[lang]["class"].remove("is_active")';
		$m.__track_lines__[261] = 'bones.string.py, line 261:\n    def setLang(self, lang):';
		$m.__track_lines__[262] = 'bones.string.py, line 262:\n    if self.currentLanguage:';
		$m.__track_lines__[263] = 'bones.string.py, line 263:\n    self.langEdits[ self.currentLanguage ]["style"]["display"] = "none"';
		$m.__track_lines__[265] = 'bones.string.py, line 265:\n    self.currentLanguage = lang';
		$m.__track_lines__[266] = 'bones.string.py, line 266:\n    self.langEdits[ self.currentLanguage ]["style"]["display"] = ""';
		$m.__track_lines__[267] = 'bones.string.py, line 267:\n    self._updateLanguageButtons()';
		$m.__track_lines__[269] = 'bones.string.py, line 269:\n    for btn in self.buttonContainer._children:';
		$m.__track_lines__[270] = 'bones.string.py, line 270:\n    if btn.lang == lang:';
		$m.__track_lines__[271] = 'bones.string.py, line 271:\n    if "is_active" not in btn[ "class" ]:';
		$m.__track_lines__[272] = 'bones.string.py, line 272:\n    btn[ "class" ].append( "is_active" )';
		$m.__track_lines__[274] = 'bones.string.py, line 274:\n    btn[ "class" ].remove( "is_active" )';
		$m.__track_lines__[276] = 'bones.string.py, line 276:\n    def onBtnGenTag(self, btn):';
		$m.__track_lines__[277] = 'bones.string.py, line 277:\n    tag = self.genTag( "", lang=btn.lang )';
		$m.__track_lines__[278] = 'bones.string.py, line 278:\n    tag.focus()';
		$m.__track_lines__[280] = 'bones.string.py, line 280:\n    def unserialize( self, data, extendedErrorInformation=None ):';
		$m.__track_lines__[281] = 'bones.string.py, line 281:\n    if not self.boneName in data.keys():';
		$m.__track_lines__[282] = 'bones.string.py, line 282:\n    return';
		$m.__track_lines__[283] = 'bones.string.py, line 283:\n    data = data[ self.boneName ]';
		$m.__track_lines__[284] = 'bones.string.py, line 284:\n    if not data:';
		$m.__track_lines__[285] = 'bones.string.py, line 285:\n    return';
		$m.__track_lines__[286] = 'bones.string.py, line 286:\n    if self.languages and self.multiple:';
		$m.__track_lines__[287] = 'bones.string.py, line 287:\n    assert isinstance(data,dict)';
		$m.__track_lines__[288] = 'bones.string.py, line 288:\n    for lang in self.languages:';
		$m.__track_lines__[289] = 'bones.string.py, line 289:\n    if lang in data.keys():';
		$m.__track_lines__[290] = 'bones.string.py, line 290:\n    val = data[ lang ]';
		$m.__track_lines__[291] = 'bones.string.py, line 291:\n    if isinstance( val, str ):';
		$m.__track_lines__[292] = 'bones.string.py, line 292:\n    self.genTag( html5.utils.unescape(val), lang=lang )';
		$m.__track_lines__[294] = 'bones.string.py, line 294:\n    for v in val:';
		$m.__track_lines__[295] = 'bones.string.py, line 295:\n    self.genTag( html5.utils.unescape(v), lang=lang )';
		$m.__track_lines__[297] = 'bones.string.py, line 297:\n    assert isinstance(data,dict)';
		$m.__track_lines__[298] = 'bones.string.py, line 298:\n    for lang in self.languages:';
		$m.__track_lines__[299] = 'bones.string.py, line 299:\n    if lang in data.keys() and data[ lang ]:';
		$m.__track_lines__[300] = 'bones.string.py, line 300:\n    self.langEdits[ lang ]["value"] = html5.utils.unescape(str(data[ lang ]))';
		$m.__track_lines__[302] = 'bones.string.py, line 302:\n    self.langEdits[ lang ]["value"] = ""';
		$m.__track_lines__[304] = 'bones.string.py, line 304:\n    if isinstance( data,list ):';
		$m.__track_lines__[305] = 'bones.string.py, line 305:\n    for tagStr in data:';
		$m.__track_lines__[306] = 'bones.string.py, line 306:\n    self.genTag( html5.utils.unescape(tagStr) )';
		$m.__track_lines__[308] = 'bones.string.py, line 308:\n    self.genTag( html5.utils.unescape(data) )';
		$m.__track_lines__[310] = 'bones.string.py, line 310:\n    self.input["value"] = html5.utils.unescape(str(data))';
		$m.__track_lines__[312] = 'bones.string.py, line 312:\n    self._updateLanguageButtons()';
		$m.__track_lines__[314] = 'bones.string.py, line 314:\n    def serializeForPost(self):';
		$m.__track_lines__[315] = 'bones.string.py, line 315:\n    res = {}';
		$m.__track_lines__[316] = 'bones.string.py, line 316:\n    if self.languages and self.multiple:';
		$m.__track_lines__[317] = 'bones.string.py, line 317:\n    for lang in self.languages:';
		$m.__track_lines__[318] = 'bones.string.py, line 318:\n    res[ "%s.%s" % (self.boneName, lang ) ] = []';
		$m.__track_lines__[319] = 'bones.string.py, line 319:\n    for child in self.langEdits[ lang ]._children:';
		$m.__track_lines__[320] = 'bones.string.py, line 320:\n    if isinstance( child, Tag ):';
		$m.__track_lines__[321] = 'bones.string.py, line 321:\n    res[ "%s.%s" % (self.boneName, lang ) ].append( child.input["value"] )';
		$m.__track_lines__[323] = 'bones.string.py, line 323:\n    for lang in self.languages:';
		$m.__track_lines__[324] = 'bones.string.py, line 324:\n    txt = self.langEdits[ lang ]["value"]';
		$m.__track_lines__[325] = 'bones.string.py, line 325:\n    if txt:';
		$m.__track_lines__[326] = 'bones.string.py, line 326:\n    res[ "%s.%s" % (self.boneName, lang) ] = txt';
		$m.__track_lines__[328] = 'bones.string.py, line 328:\n    res[ self.boneName ] = []';
		$m.__track_lines__[329] = 'bones.string.py, line 329:\n    for child in self.tagContainer._children:';
		$m.__track_lines__[330] = 'bones.string.py, line 330:\n    if isinstance( child, Tag ):';
		$m.__track_lines__[331] = 'bones.string.py, line 331:\n    res[ self.boneName ].append( child.input["value"] )';
		$m.__track_lines__[333] = 'bones.string.py, line 333:\n    res[ self.boneName ] = self.input["value"]';
		$m.__track_lines__[334] = 'bones.string.py, line 334:\n    return( res )';
		$m.__track_lines__[336] = 'bones.string.py, line 336:\n    def serializeForDocument(self):';
		$m.__track_lines__[337] = 'bones.string.py, line 337:\n    return( self.serialize( ) )';
		$m.__track_lines__[339] = 'bones.string.py, line 339:\n    def genTag( self, tag, editMode=False, lang=None ):';
		$m.__track_lines__[340] = 'bones.string.py, line 340:\n    tag = Tag( tag, editMode, readonly = self.readOnly )';
		$m.__track_lines__[341] = 'bones.string.py, line 341:\n    if lang is not None:';
		$m.__track_lines__[342] = 'bones.string.py, line 342:\n    self.langEdits[ lang ].appendChild( tag )';
		$m.__track_lines__[344] = 'bones.string.py, line 344:\n    self.tagContainer.appendChild( tag )';
		$m.__track_lines__[346] = 'bones.string.py, line 346:\n    return tag';
		$m.__track_lines__[349] = 'bones.string.py, line 349:\n    def CheckForStringBone(  modulName, boneName, skelStucture, *args, **kwargs ):';
		$m.__track_lines__[350] = 'bones.string.py, line 350:\n    return( str(skelStucture[boneName]["type"]).startswith("str") )';
		$m.__track_lines__[353] = 'bones.string.py, line 353:\n    class ExtendedStringSearch( html5.Div ):';
		$m.__track_lines__[354] = 'bones.string.py, line 354:\n    def __init__(self, extension, view, modul, *args, **kwargs ):';
		$m.__track_lines__[355] = 'bones.string.py, line 355:\n    super( ExtendedStringSearch, self ).__init__( *args, **kwargs )';
		$m.__track_lines__[356] = 'bones.string.py, line 356:\n    self.view = view';
		$m.__track_lines__[357] = 'bones.string.py, line 357:\n    self.extension = extension';
		$m.__track_lines__[358] = 'bones.string.py, line 358:\n    self.modul = modul';
		$m.__track_lines__[359] = 'bones.string.py, line 359:\n    self.opMode = extension["mode"]';
		$m.__track_lines__[360] = 'bones.string.py, line 360:\n    self.filterChangedEvent = EventDispatcher("filterChanged")';
		$m.__track_lines__[361] = 'bones.string.py, line 361:\n    assert self.opMode in ["equals","from", "to", "prefix","range"]';
		$m.__track_lines__[362] = 'bones.string.py, line 362:\n    self.appendChild( html5.TextNode(extension["name"]))';
		$m.__track_lines__[363] = 'bones.string.py, line 363:\n    self.sinkEvent("onKeyDown")';
		$m.__track_lines__[364] = 'bones.string.py, line 364:\n    if self.opMode in ["equals","from", "to", "prefix"]:';
		$m.__track_lines__[365] = 'bones.string.py, line 365:\n    self.input = html5.Input()';
		$m.__track_lines__[366] = 'bones.string.py, line 366:\n    self.input["type"] = "text"';
		$m.__track_lines__[367] = 'bones.string.py, line 367:\n    self.appendChild( self.input )';
		$m.__track_lines__[369] = 'bones.string.py, line 369:\n    self.input1 = html5.Input()';
		$m.__track_lines__[370] = 'bones.string.py, line 370:\n    self.input1["type"] = "text"';
		$m.__track_lines__[371] = 'bones.string.py, line 371:\n    self.appendChild( self.input1 )';
		$m.__track_lines__[372] = 'bones.string.py, line 372:\n    self.appendChild( html5.TextNode("to") )';
		$m.__track_lines__[373] = 'bones.string.py, line 373:\n    self.input2 = html5.Input()';
		$m.__track_lines__[374] = 'bones.string.py, line 374:\n    self.input2["type"] = "text"';
		$m.__track_lines__[375] = 'bones.string.py, line 375:\n    self.appendChild( self.input2 )';
		$m.__track_lines__[377] = 'bones.string.py, line 377:\n    def onKeyDown(self, event):';
		$m.__track_lines__[378] = 'bones.string.py, line 378:\n    if isReturn(event.keyCode):';
		$m.__track_lines__[379] = 'bones.string.py, line 379:\n    self.filterChangedEvent.fire()';
		$m.__track_lines__[381] = 'bones.string.py, line 381:\n    def updateFilter(self, filter):';
		$m.__track_lines__[382] = 'bones.string.py, line 382:\n    if self.opMode=="equals":';
		$m.__track_lines__[383] = 'bones.string.py, line 383:\n    filter[ self.extension["target"] ] = self.input["value"]';
		$m.__track_lines__[385] = 'bones.string.py, line 385:\n    filter[ self.extension["target"]+"$gt" ] = self.input["value"]';
		$m.__track_lines__[387] = 'bones.string.py, line 387:\n    filter[ self.extension["target"]+"$lt" ] = self.input["value"]';
		$m.__track_lines__[389] = 'bones.string.py, line 389:\n    filter[ self.extension["target"]+"$lk" ] = self.input["value"]';
		$m.__track_lines__[391] = 'bones.string.py, line 391:\n    filter[ self.extension["target"]+"$gt" ] = self.input1["value"]';
		$m.__track_lines__[392] = 'bones.string.py, line 392:\n    filter[ self.extension["target"]+"$lt" ] = self.input2["value"]';
		$m.__track_lines__[393] = 'bones.string.py, line 393:\n    return( filter )';
		$m.__track_lines__[396] = 'bones.string.py, line 395:\n    @staticmethod ... def canHandleExtension( extension, view, modul ):';
		$m.__track_lines__[397] = 'bones.string.py, line 397:\n    return( isinstance( extension, dict) and "type" in extension.keys() and (extension["type"]=="string" or extension["type"].startswith("string.") ) )';
		$m.__track_lines__[401] = 'bones.string.py, line 401:\n    editBoneSelector.insert( 3, CheckForStringBone, StringEditBone)';
		$m.__track_lines__[402] = 'bones.string.py, line 402:\n    viewDelegateSelector.insert( 3, CheckForStringBone, StringViewBoneDelegate)';
		$m.__track_lines__[403] = 'bones.string.py, line 403:\n    extendedSearchWidgetSelector.insert( 1, ExtendedStringSearch.canHandleExtension, ExtendedStringSearch )';
		$m.__track_lines__[404] = 'bones.string.py, line 404:\n    extractorDelegateSelector.insert(3, CheckForStringBone, StringBoneExtractor)';

		var $constant_int_0 = new $p['int'](0);
		var $constant_int_1 = new $p['int'](1);
		var $constant_int_3 = new $p['int'](3);
		$pyjs['track']['module']='bones.string';
		$pyjs['track']['lineno']=1;
		$pyjs['track']['lineno']=3;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['html5'] = $p['___import___']('html5', 'bones');
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=4;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['editBoneSelector'] = $p['___import___']('priorityqueue.editBoneSelector', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['viewDelegateSelector'] = $p['___import___']('priorityqueue.viewDelegateSelector', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['extendedSearchWidgetSelector'] = $p['___import___']('priorityqueue.extendedSearchWidgetSelector', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['extractorDelegateSelector'] = $p['___import___']('priorityqueue.extractorDelegateSelector', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=5;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['conf'] = $p['___import___']('config.conf', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=6;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['EventDispatcher'] = $p['___import___']('event.EventDispatcher', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=7;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$p['__import_all__']('html5.keycodes', 'bones', $m, null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=8;
		$pyjs['track']={'module':$pyjs['track']['module'],'lineno':$pyjs['track']['lineno']};$pyjs['trackstack']['push']($pyjs['track']);
		$m['translate'] = $p['___import___']('i18n.translate', 'bones', null, false);
		$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
		$pyjs['track']['lineno']=11;
		$m['StringBoneExtractor'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.string';
			$cls_definition['__md5__'] = 'fca35cf5d278fd7a71a360cd34ea6d49';
			$pyjs['track']['lineno']=12;
			$method = $pyjs__bind_method2('__init__', function(modulName, boneName, skelStructure) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

					var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					modulName = arguments[1];
					boneName = arguments[2];
					skelStructure = arguments[3];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,4,arguments['length']-1));

					var kwargs = arguments['length'] >= 5 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'fca35cf5d278fd7a71a360cd34ea6d49') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof skelStructure != 'undefined') {
						if (skelStructure !== null && typeof skelStructure['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = skelStructure;
							skelStructure = arguments[4];
						}
					} else 					if (typeof boneName != 'undefined') {
						if (boneName !== null && typeof boneName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = boneName;
							boneName = arguments[4];
						}
					} else 					if (typeof modulName != 'undefined') {
						if (modulName !== null && typeof modulName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modulName;
							modulName = arguments[4];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[4];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'bones.string', 'lineno':12};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=12;
				$pyjs['track']['lineno']=13;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['StringBoneExtractor'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_1_err){if (!$p['isinstance']($pyjs_dbg_1_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_1_err);}throw $pyjs_dbg_1_err;
}})()['__init__']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_2_err){if (!$p['isinstance']($pyjs_dbg_2_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_2_err);}throw $pyjs_dbg_2_err;
}})();
				$pyjs['track']['lineno']=14;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('skelStructure', skelStructure) : $p['setattr'](self, 'skelStructure', skelStructure); 
				$pyjs['track']['lineno']=15;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('boneName', boneName) : $p['setattr'](self, 'boneName', boneName); 
				$pyjs['track']['lineno']=16;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modulName', modulName) : $p['setattr'](self, 'modulName', modulName); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['modulName'],['boneName'],['skelStructure']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=18;
			$method = $pyjs__bind_method2('render', function(data, field) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					field = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'fca35cf5d278fd7a71a360cd34ea6d49') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var resstr;
				$pyjs['track']={'module':'bones.string', 'lineno':18};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=18;
				$pyjs['track']['lineno']=19;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_3_err){if (!$p['isinstance']($pyjs_dbg_3_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_3_err);}throw $pyjs_dbg_3_err;
}})()['__contains__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_4_err){if (!$p['isinstance']($pyjs_dbg_4_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_4_err);}throw $pyjs_dbg_4_err;
}})()) {
					$pyjs['track']['lineno']=21;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](data['__getitem__'](field), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_5_err){if (!$p['isinstance']($pyjs_dbg_5_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_5_err);}throw $pyjs_dbg_5_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_6_err){if (!$p['isinstance']($pyjs_dbg_6_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_6_err);}throw $pyjs_dbg_6_err;
}})()) {
						$pyjs['track']['lineno']=22;
						resstr = '';
						$pyjs['track']['lineno']=23;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['conf']['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_7_err){if (!$p['isinstance']($pyjs_dbg_7_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_7_err);}throw $pyjs_dbg_7_err;
}})()['__contains__']('currentlanguage'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_8_err){if (!$p['isinstance']($pyjs_dbg_8_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_8_err);}throw $pyjs_dbg_8_err;
}})()) {
							$pyjs['track']['lineno']=24;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
							return data['__getitem__'](field)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_9_err){if (!$p['isinstance']($pyjs_dbg_9_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_9_err);}throw $pyjs_dbg_9_err;
}})()['__contains__']($m['conf']['__getitem__']('currentlanguage')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_10_err){if (!$p['isinstance']($pyjs_dbg_10_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_10_err);}throw $pyjs_dbg_10_err;
}})()) {
								$pyjs['track']['lineno']=25;
								resstr = (function(){try{try{$pyjs['in_try_except'] += 1;
								return (function(){try{try{$pyjs['in_try_except'] += 1;
								return (function(){try{try{$pyjs['in_try_except'] += 1;
								return data['__getitem__'](field)['__getitem__']($m['conf']['__getitem__']('currentlanguage'))['$$replace']('&quot;', "'");
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_11_err){if (!$p['isinstance']($pyjs_dbg_11_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_11_err);}throw $pyjs_dbg_11_err;
}})()['$$replace'](';', ' ');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_12_err){if (!$p['isinstance']($pyjs_dbg_12_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_12_err);}throw $pyjs_dbg_12_err;
}})()['$$replace']('"', "'");
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_13_err){if (!$p['isinstance']($pyjs_dbg_13_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_13_err);}throw $pyjs_dbg_13_err;
}})();
							}
							else {
								$pyjs['track']['lineno']=27;
								if ((function(){try{try{$pyjs['in_try_except'] += 1;
									return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['len']((function(){try{try{$pyjs['in_try_except'] += 1;
								return data['__getitem__'](field)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_14_err){if (!$p['isinstance']($pyjs_dbg_14_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_14_err);}throw $pyjs_dbg_14_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_15_err){if (!$p['isinstance']($pyjs_dbg_15_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_15_err);}throw $pyjs_dbg_15_err;
}})(), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_16_err){if (!$p['isinstance']($pyjs_dbg_16_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_16_err);}throw $pyjs_dbg_16_err;
}})()) {
									$pyjs['track']['lineno']=28;
									resstr = (function(){try{try{$pyjs['in_try_except'] += 1;
									return (function(){try{try{$pyjs['in_try_except'] += 1;
									return (function(){try{try{$pyjs['in_try_except'] += 1;
									return data['__getitem__'](field)['__getitem__']((function(){try{try{$pyjs['in_try_except'] += 1;
									return data['__getitem__'](field)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_17_err){if (!$p['isinstance']($pyjs_dbg_17_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_17_err);}throw $pyjs_dbg_17_err;
}})()['__getitem__']($constant_int_0))['$$replace']('&quot;', "'");
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_18_err){if (!$p['isinstance']($pyjs_dbg_18_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_18_err);}throw $pyjs_dbg_18_err;
}})()['$$replace'](';', ' ');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_19_err){if (!$p['isinstance']($pyjs_dbg_19_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_19_err);}throw $pyjs_dbg_19_err;
}})()['$$replace']('"', "'");
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_20_err){if (!$p['isinstance']($pyjs_dbg_20_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_20_err);}throw $pyjs_dbg_20_err;
}})();
								}
							}
						}
						$pyjs['track']['lineno']=29;
						$pyjs['track']['lineno']=29;
						var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('"%s"', resstr);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_21_err){if (!$p['isinstance']($pyjs_dbg_21_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_21_err);}throw $pyjs_dbg_21_err;
}})();
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
					else if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](data['__getitem__'](field), $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_22_err){if (!$p['isinstance']($pyjs_dbg_22_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_22_err);}throw $pyjs_dbg_22_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_23_err){if (!$p['isinstance']($pyjs_dbg_23_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_23_err);}throw $pyjs_dbg_23_err;
}})()) {
						$pyjs['track']['lineno']=31;
						$pyjs['track']['lineno']=31;
						var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
						return ', '['join'](function(){
							var $iter1_nextval,$iter1_type,$collcomp1,$iter1_iter,item,$iter1_idx,$pyjs__trackstack_size_1,$iter1_array;
	$collcomp1 = $p['list']();
						$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
						$iter1_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
						return data['__getitem__'](field);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_24_err){if (!$p['isinstance']($pyjs_dbg_24_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_24_err);}throw $pyjs_dbg_24_err;
}})();
						$iter1_nextval=$p['__iter_prepare']($iter1_iter,false);
						while (typeof($p['__wrapped_next']($iter1_nextval)['$nextval']) != 'undefined') {
							item = $iter1_nextval['$nextval'];
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $collcomp1['append']((function(){try{try{$pyjs['in_try_except'] += 1;
							return (function(){try{try{$pyjs['in_try_except'] += 1;
							return (function(){try{try{$pyjs['in_try_except'] += 1;
							return item['$$replace']('&quot;', '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_25_err){if (!$p['isinstance']($pyjs_dbg_25_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_25_err);}throw $pyjs_dbg_25_err;
}})()['$$replace'](';', ' ');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_26_err){if (!$p['isinstance']($pyjs_dbg_26_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_26_err);}throw $pyjs_dbg_26_err;
}})()['$$replace']('"', "'");
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_27_err){if (!$p['isinstance']($pyjs_dbg_27_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_27_err);}throw $pyjs_dbg_27_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_28_err){if (!$p['isinstance']($pyjs_dbg_28_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_28_err);}throw $pyjs_dbg_28_err;
}})();
						}
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='bones.string';

	return $collcomp1;}());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_29_err){if (!$p['isinstance']($pyjs_dbg_29_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_29_err);}throw $pyjs_dbg_29_err;
}})();
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
					else {
						$pyjs['track']['lineno']=33;
						$pyjs['track']['lineno']=33;
						var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['str']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('"%s"', (function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return (function(){try{try{$pyjs['in_try_except'] += 1;
						return data['__getitem__'](field)['$$replace']('&quot;', '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_30_err){if (!$p['isinstance']($pyjs_dbg_30_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_30_err);}throw $pyjs_dbg_30_err;
}})()['$$replace'](';', ' ');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_31_err){if (!$p['isinstance']($pyjs_dbg_31_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_31_err);}throw $pyjs_dbg_31_err;
}})()['$$replace']('"', "'");
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_32_err){if (!$p['isinstance']($pyjs_dbg_32_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_32_err);}throw $pyjs_dbg_32_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_33_err){if (!$p['isinstance']($pyjs_dbg_33_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_33_err);}throw $pyjs_dbg_33_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_34_err){if (!$p['isinstance']($pyjs_dbg_34_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_34_err);}throw $pyjs_dbg_34_err;
}})();
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
				}
				$pyjs['track']['lineno']=34;
				$pyjs['track']['lineno']=34;
				var $pyjs__ret = $m['conf']['__getitem__']('empty_value');
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['data'],['field']]);
			$cls_definition['render'] = $method;
			$pyjs['track']['lineno']=11;
			var $bases = new Array($p['object']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('StringBoneExtractor', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=37;
		$m['StringViewBoneDelegate'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.string';
			$cls_definition['__md5__'] = '8c760d3775edf457d6605d80656b3ca2';
			$pyjs['track']['lineno']=38;
			$method = $pyjs__bind_method2('__init__', function(modulName, boneName, skelStructure) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

					var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					modulName = arguments[1];
					boneName = arguments[2];
					skelStructure = arguments[3];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,4,arguments['length']-1));

					var kwargs = arguments['length'] >= 5 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8c760d3775edf457d6605d80656b3ca2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof skelStructure != 'undefined') {
						if (skelStructure !== null && typeof skelStructure['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = skelStructure;
							skelStructure = arguments[4];
						}
					} else 					if (typeof boneName != 'undefined') {
						if (boneName !== null && typeof boneName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = boneName;
							boneName = arguments[4];
						}
					} else 					if (typeof modulName != 'undefined') {
						if (modulName !== null && typeof modulName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modulName;
							modulName = arguments[4];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[4];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'bones.string', 'lineno':38};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=38;
				$pyjs['track']['lineno']=39;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['StringViewBoneDelegate'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_35_err){if (!$p['isinstance']($pyjs_dbg_35_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_35_err);}throw $pyjs_dbg_35_err;
}})()['__init__']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_36_err){if (!$p['isinstance']($pyjs_dbg_36_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_36_err);}throw $pyjs_dbg_36_err;
}})();
				$pyjs['track']['lineno']=40;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('skelStructure', skelStructure) : $p['setattr'](self, 'skelStructure', skelStructure); 
				$pyjs['track']['lineno']=41;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('boneName', boneName) : $p['setattr'](self, 'boneName', boneName); 
				$pyjs['track']['lineno']=42;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modulName', modulName) : $p['setattr'](self, 'modulName', modulName); 
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['modulName'],['boneName'],['skelStructure']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=44;
			$method = $pyjs__bind_method2('render', function(data, field) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					field = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8c760d3775edf457d6605d80656b3ca2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var resstr,output;
				$pyjs['track']={'module':'bones.string', 'lineno':44};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=44;
				$pyjs['track']['lineno']=45;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_37_err){if (!$p['isinstance']($pyjs_dbg_37_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_37_err);}throw $pyjs_dbg_37_err;
}})()['__contains__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_38_err){if (!$p['isinstance']($pyjs_dbg_38_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_38_err);}throw $pyjs_dbg_38_err;
}})()) {
					$pyjs['track']['lineno']=47;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](data['__getitem__'](field), $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_39_err){if (!$p['isinstance']($pyjs_dbg_39_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_39_err);}throw $pyjs_dbg_39_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_40_err){if (!$p['isinstance']($pyjs_dbg_40_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_40_err);}throw $pyjs_dbg_40_err;
}})()) {
						$pyjs['track']['lineno']=48;
						resstr = '';
						$pyjs['track']['lineno']=49;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['conf']['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_41_err){if (!$p['isinstance']($pyjs_dbg_41_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_41_err);}throw $pyjs_dbg_41_err;
}})()['__contains__']('currentlanguage'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_42_err){if (!$p['isinstance']($pyjs_dbg_42_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_42_err);}throw $pyjs_dbg_42_err;
}})()) {
							$pyjs['track']['lineno']=50;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
							return data['__getitem__'](field)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_43_err){if (!$p['isinstance']($pyjs_dbg_43_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_43_err);}throw $pyjs_dbg_43_err;
}})()['__contains__']($m['conf']['__getitem__']('currentlanguage')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_44_err){if (!$p['isinstance']($pyjs_dbg_44_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_44_err);}throw $pyjs_dbg_44_err;
}})()) {
								$pyjs['track']['lineno']=51;
								resstr = data['__getitem__'](field)['__getitem__']($m['conf']['__getitem__']('currentlanguage'));
							}
							else {
								$pyjs['track']['lineno']=53;
								if ((function(){try{try{$pyjs['in_try_except'] += 1;
									return $p['bool'](($p['cmp']((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['len']((function(){try{try{$pyjs['in_try_except'] += 1;
								return data['__getitem__'](field)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_45_err){if (!$p['isinstance']($pyjs_dbg_45_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_45_err);}throw $pyjs_dbg_45_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_46_err){if (!$p['isinstance']($pyjs_dbg_46_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_46_err);}throw $pyjs_dbg_46_err;
}})(), $constant_int_0) == 1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_47_err){if (!$p['isinstance']($pyjs_dbg_47_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_47_err);}throw $pyjs_dbg_47_err;
}})()) {
									$pyjs['track']['lineno']=54;
									resstr = data['__getitem__'](field)['__getitem__']((function(){try{try{$pyjs['in_try_except'] += 1;
									return data['__getitem__'](field)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_48_err){if (!$p['isinstance']($pyjs_dbg_48_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_48_err);}throw $pyjs_dbg_48_err;
}})()['__getitem__']($constant_int_0));
								}
							}
						}
						$pyjs['track']['lineno']=55;
						$pyjs['track']['lineno']=55;
						var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
						return self['getViewElement'](resstr, data['__getitem__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_49_err){if (!$p['isinstance']($pyjs_dbg_49_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_49_err);}throw $pyjs_dbg_49_err;
}})();
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
					else {
						$pyjs['track']['lineno']=60;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['isinstance'](data['__getitem__'](field), $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_50_err){if (!$p['isinstance']($pyjs_dbg_50_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_50_err);}throw $pyjs_dbg_50_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_51_err){if (!$p['isinstance']($pyjs_dbg_51_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_51_err);}throw $pyjs_dbg_51_err;
}})()) {
							$pyjs['track']['lineno']=61;
							output = (function(){try{try{$pyjs['in_try_except'] += 1;
							return ', '['join'](data['__getitem__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_52_err){if (!$p['isinstance']($pyjs_dbg_52_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_52_err);}throw $pyjs_dbg_52_err;
}})();
						}
						else {
							$pyjs['track']['lineno']=63;
							output = (function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['str'](data['__getitem__'](field));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_53_err){if (!$p['isinstance']($pyjs_dbg_53_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_53_err);}throw $pyjs_dbg_53_err;
}})();
						}
						$pyjs['track']['lineno']=65;
						$pyjs['track']['lineno']=65;
						var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
						return self['getViewElement'](output, false);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_54_err){if (!$p['isinstance']($pyjs_dbg_54_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_54_err);}throw $pyjs_dbg_54_err;
}})();
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
				}
				$pyjs['track']['lineno']=67;
				$pyjs['track']['lineno']=67;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['getViewElement']($m['conf']['__getitem__']('empty_value'), false);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_55_err){if (!$p['isinstance']($pyjs_dbg_55_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_55_err);}throw $pyjs_dbg_55_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['data'],['field']]);
			$cls_definition['render'] = $method;
			$pyjs['track']['lineno']=69;
			$method = $pyjs__bind_method2('getViewElement', function(labelstr, datafield) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					labelstr = arguments[1];
					datafield = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '8c760d3775edf457d6605d80656b3ca2') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var aspan;
				$pyjs['track']={'module':'bones.string', 'lineno':69};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=69;
				$pyjs['track']['lineno']=70;
				labelstr = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['utils']['unescape'](labelstr);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_56_err){if (!$p['isinstance']($pyjs_dbg_56_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_56_err);}throw $pyjs_dbg_56_err;
}})();
				$pyjs['track']['lineno']=72;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool'](datafield));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_57_err){if (!$p['isinstance']($pyjs_dbg_57_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_57_err);}throw $pyjs_dbg_57_err;
}})()) {
					$pyjs['track']['lineno']=73;
					$pyjs['track']['lineno']=73;
					var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Label'](labelstr);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_58_err){if (!$p['isinstance']($pyjs_dbg_58_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_58_err);}throw $pyjs_dbg_58_err;
}})();
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				else {
					$pyjs['track']['lineno']=75;
					aspan = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Span']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_59_err){if (!$p['isinstance']($pyjs_dbg_59_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_59_err);}throw $pyjs_dbg_59_err;
}})();
					$pyjs['track']['lineno']=76;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return aspan['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['TextNode'](labelstr);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_60_err){if (!$p['isinstance']($pyjs_dbg_60_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_60_err);}throw $pyjs_dbg_60_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_61_err){if (!$p['isinstance']($pyjs_dbg_61_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_61_err);}throw $pyjs_dbg_61_err;
}})();
					$pyjs['track']['lineno']=77;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return aspan['__setitem__']('Title', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['str'](datafield);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_62_err){if (!$p['isinstance']($pyjs_dbg_62_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_62_err);}throw $pyjs_dbg_62_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_63_err){if (!$p['isinstance']($pyjs_dbg_63_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_63_err);}throw $pyjs_dbg_63_err;
}})();
					$pyjs['track']['lineno']=78;
					$pyjs['track']['lineno']=78;
					var $pyjs__ret = aspan;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['labelstr'],['datafield']]);
			$cls_definition['getViewElement'] = $method;
			$pyjs['track']['lineno']=37;
			var $bases = new Array($p['object']);
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('StringViewBoneDelegate', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=80;
		$m['Tag'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.string';
			$cls_definition['__md5__'] = '623973c12ba852f4c0217674f4363f99';
			$pyjs['track']['lineno']=81;
			$method = $pyjs__bind_method2('__init__', function(tag, isEditMode, readonly) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

					var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					tag = arguments[1];
					isEditMode = arguments[2];
					readonly = arguments[3];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,4,arguments['length']-1));

					var kwargs = arguments['length'] >= 5 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '623973c12ba852f4c0217674f4363f99') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof readonly != 'undefined') {
						if (readonly !== null && typeof readonly['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = readonly;
							readonly = arguments[4];
						}
					} else 					if (typeof isEditMode != 'undefined') {
						if (isEditMode !== null && typeof isEditMode['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = isEditMode;
							isEditMode = arguments[4];
						}
					} else 					if (typeof tag != 'undefined') {
						if (tag !== null && typeof tag['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = tag;
							tag = arguments[4];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[4];
						}
					} else {
					}
				}
				if (typeof readonly == 'undefined') readonly=arguments['callee']['__args__'][5][1];
				var delBtn;
				$pyjs['track']={'module':'bones.string', 'lineno':81};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=81;
				$pyjs['track']['lineno']=82;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['Tag'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_64_err){if (!$p['isinstance']($pyjs_dbg_64_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_64_err);}throw $pyjs_dbg_64_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_65_err){if (!$p['isinstance']($pyjs_dbg_65_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_65_err);}throw $pyjs_dbg_65_err;
}})();
				$pyjs['track']['lineno']=83;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['__getitem__']('class')['append']('tag');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_66_err){if (!$p['isinstance']($pyjs_dbg_66_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_66_err);}throw $pyjs_dbg_66_err;
}})();
				$pyjs['track']['lineno']=85;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('input', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})()) : $p['setattr'](self, 'input', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_67_err){if (!$p['isinstance']($pyjs_dbg_67_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_67_err);}throw $pyjs_dbg_67_err;
}})()); 
				$pyjs['track']['lineno']=86;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'input')['__setitem__']('type', 'text');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_68_err){if (!$p['isinstance']($pyjs_dbg_68_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_68_err);}throw $pyjs_dbg_68_err;
}})();
				$pyjs['track']['lineno']=87;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'input')['__setitem__']('value', tag);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_69_err){if (!$p['isinstance']($pyjs_dbg_69_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_69_err);}throw $pyjs_dbg_69_err;
}})();
				$pyjs['track']['lineno']=88;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']($p['getattr'](self, 'input'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_70_err){if (!$p['isinstance']($pyjs_dbg_70_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_70_err);}throw $pyjs_dbg_70_err;
}})();
				$pyjs['track']['lineno']=90;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](readonly);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_71_err){if (!$p['isinstance']($pyjs_dbg_71_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_71_err);}throw $pyjs_dbg_71_err;
}})()) {
					$pyjs['track']['lineno']=91;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'input')['__setitem__']('readonly', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_72_err){if (!$p['isinstance']($pyjs_dbg_72_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_72_err);}throw $pyjs_dbg_72_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=93;
					delBtn = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['ext']['Button']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['translate']('Delete');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_73_err){if (!$p['isinstance']($pyjs_dbg_73_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_73_err);}throw $pyjs_dbg_73_err;
}})(), $p['getattr'](self, 'removeMe'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_74_err){if (!$p['isinstance']($pyjs_dbg_74_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_74_err);}throw $pyjs_dbg_74_err;
}})();
					$pyjs['track']['lineno']=94;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return delBtn['__getitem__']('class')['append']('icon delete tag');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_75_err){if (!$p['isinstance']($pyjs_dbg_75_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_75_err);}throw $pyjs_dbg_75_err;
}})();
					$pyjs['track']['lineno']=95;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild'](delBtn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_76_err){if (!$p['isinstance']($pyjs_dbg_76_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_76_err);}throw $pyjs_dbg_76_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['tag'],['isEditMode'],['readonly', false]]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=97;
			$method = $pyjs__bind_method2('removeMe', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,0,arguments['length']-1));

					var kwargs = arguments['length'] >= 1 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,1,arguments['length']-1));

					var kwargs = arguments['length'] >= 2 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '623973c12ba852f4c0217674f4363f99') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[1];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'bones.string', 'lineno':97};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=97;
				$pyjs['track']['lineno']=98;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['parent']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_77_err){if (!$p['isinstance']($pyjs_dbg_77_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_77_err);}throw $pyjs_dbg_77_err;
}})()['removeChild'](self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_78_err){if (!$p['isinstance']($pyjs_dbg_78_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_78_err);}throw $pyjs_dbg_78_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self']]);
			$cls_definition['removeMe'] = $method;
			$pyjs['track']['lineno']=100;
			$method = $pyjs__bind_method2('focus', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '623973c12ba852f4c0217674f4363f99') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.string', 'lineno':100};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=100;
				$pyjs['track']['lineno']=101;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['input']['focus']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_79_err){if (!$p['isinstance']($pyjs_dbg_79_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_79_err);}throw $pyjs_dbg_79_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['focus'] = $method;
			$pyjs['track']['lineno']=80;
			var $bases = new Array($p['getattr']($m['html5'], 'Span'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('Tag', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=103;
		$m['StringEditBone'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.string';
			$cls_definition['__md5__'] = 'a55280d6f690d79eeb16b3b6ef1a0a17';
			$pyjs['track']['lineno']=104;
			$method = $pyjs__bind_method2('__init__', function(modulName, boneName, readOnly, multiple, languages) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,5,arguments['length']-1));

					var kwargs = arguments['length'] >= 6 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					modulName = arguments[1];
					boneName = arguments[2];
					readOnly = arguments[3];
					multiple = arguments[4];
					languages = arguments[5];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,6,arguments['length']-1));

					var kwargs = arguments['length'] >= 7 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a55280d6f690d79eeb16b3b6ef1a0a17') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof languages != 'undefined') {
						if (languages !== null && typeof languages['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = languages;
							languages = arguments[6];
						}
					} else 					if (typeof multiple != 'undefined') {
						if (multiple !== null && typeof multiple['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = multiple;
							multiple = arguments[6];
						}
					} else 					if (typeof readOnly != 'undefined') {
						if (readOnly !== null && typeof readOnly['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = readOnly;
							readOnly = arguments[6];
						}
					} else 					if (typeof boneName != 'undefined') {
						if (boneName !== null && typeof boneName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = boneName;
							boneName = arguments[6];
						}
					} else 					if (typeof modulName != 'undefined') {
						if (modulName !== null && typeof modulName['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modulName;
							modulName = arguments[6];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[6];
						}
					} else {
					}
				}
				if (typeof multiple == 'undefined') multiple=arguments['callee']['__args__'][6][1];
				if (typeof languages == 'undefined') languages=arguments['callee']['__args__'][7][1];
				var inputField,$iter3_array,$iter2_type,$iter3_idx,$iter2_iter,tagContainer,$iter3_iter,addBtn,$and1,$and2,$and3,$and4,$and5,$and6,$iter2_idx,langBtn,$iter3_type,lang,$iter2_nextval,$pyjs__trackstack_size_1,$iter3_nextval,$iter2_array;
				$pyjs['track']={'module':'bones.string', 'lineno':104};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=104;
				$pyjs['track']['lineno']=105;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['StringEditBone'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_80_err){if (!$p['isinstance']($pyjs_dbg_80_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_80_err);}throw $pyjs_dbg_80_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_81_err){if (!$p['isinstance']($pyjs_dbg_81_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_81_err);}throw $pyjs_dbg_81_err;
}})();
				$pyjs['track']['lineno']=106;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modulName', modulName) : $p['setattr'](self, 'modulName', modulName); 
				$pyjs['track']['lineno']=107;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('boneName', boneName) : $p['setattr'](self, 'boneName', boneName); 
				$pyjs['track']['lineno']=108;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('readOnly', readOnly) : $p['setattr'](self, 'readOnly', readOnly); 
				$pyjs['track']['lineno']=109;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('multiple', multiple) : $p['setattr'](self, 'multiple', multiple); 
				$pyjs['track']['lineno']=110;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('languages', languages) : $p['setattr'](self, 'languages', languages); 
				$pyjs['track']['lineno']=111;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('boneName', boneName) : $p['setattr'](self, 'boneName', boneName); 
				$pyjs['track']['lineno']=112;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentLanguage', null) : $p['setattr'](self, 'currentLanguage', null); 
				$pyjs['track']['lineno']=114;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and1=$p['getattr'](self, 'languages'))?$p['getattr'](self, 'multiple'):$and1));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_82_err){if (!$p['isinstance']($pyjs_dbg_82_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_82_err);}throw $pyjs_dbg_82_err;
}})()) {
					$pyjs['track']['lineno']=115;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__getitem__']('class')['append']('is_translated');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_83_err){if (!$p['isinstance']($pyjs_dbg_83_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_83_err);}throw $pyjs_dbg_83_err;
}})();
					$pyjs['track']['lineno']=116;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__getitem__']('class')['append']('is_multiple');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_84_err){if (!$p['isinstance']($pyjs_dbg_84_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_84_err);}throw $pyjs_dbg_84_err;
}})();
					$pyjs['track']['lineno']=117;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('languagesContainer', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})()) : $p['setattr'](self, 'languagesContainer', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_85_err){if (!$p['isinstance']($pyjs_dbg_85_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_85_err);}throw $pyjs_dbg_85_err;
}})()); 
					$pyjs['track']['lineno']=118;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'languagesContainer'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_86_err){if (!$p['isinstance']($pyjs_dbg_86_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_86_err);}throw $pyjs_dbg_86_err;
}})();
					$pyjs['track']['lineno']=119;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('buttonContainer', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})()) : $p['setattr'](self, 'buttonContainer', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_87_err){if (!$p['isinstance']($pyjs_dbg_87_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_87_err);}throw $pyjs_dbg_87_err;
}})()); 
					$pyjs['track']['lineno']=120;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'buttonContainer')['__setitem__']('class', 'languagebuttons');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_88_err){if (!$p['isinstance']($pyjs_dbg_88_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_88_err);}throw $pyjs_dbg_88_err;
}})();
					$pyjs['track']['lineno']=121;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'buttonContainer'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_89_err){if (!$p['isinstance']($pyjs_dbg_89_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_89_err);}throw $pyjs_dbg_89_err;
}})();
					$pyjs['track']['lineno']=122;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('langEdits', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})()) : $p['setattr'](self, 'langEdits', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_90_err){if (!$p['isinstance']($pyjs_dbg_90_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_90_err);}throw $pyjs_dbg_90_err;
}})()); 
					$pyjs['track']['lineno']=123;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('langBtns', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_91_err){if (!$p['isinstance']($pyjs_dbg_91_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_91_err);}throw $pyjs_dbg_91_err;
}})()) : $p['setattr'](self, 'langBtns', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_91_err){if (!$p['isinstance']($pyjs_dbg_91_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_91_err);}throw $pyjs_dbg_91_err;
}})()); 
					$pyjs['track']['lineno']=125;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter2_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'languages');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_92_err){if (!$p['isinstance']($pyjs_dbg_92_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_92_err);}throw $pyjs_dbg_92_err;
}})();
					$iter2_nextval=$p['__iter_prepare']($iter2_iter,false);
					while (typeof($p['__wrapped_next']($iter2_nextval)['$nextval']) != 'undefined') {
						lang = $iter2_nextval['$nextval'];
						$pyjs['track']['lineno']=126;
						tagContainer = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_93_err){if (!$p['isinstance']($pyjs_dbg_93_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_93_err);}throw $pyjs_dbg_93_err;
}})();
						$pyjs['track']['lineno']=127;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return tagContainer['__getitem__']('class')['append']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('lang_%s', lang);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_94_err){if (!$p['isinstance']($pyjs_dbg_94_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_94_err);}throw $pyjs_dbg_94_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_95_err){if (!$p['isinstance']($pyjs_dbg_95_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_95_err);}throw $pyjs_dbg_95_err;
}})();
						$pyjs['track']['lineno']=128;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return tagContainer['__getitem__']('class')['append']('tagcontainer');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_96_err){if (!$p['isinstance']($pyjs_dbg_96_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_96_err);}throw $pyjs_dbg_96_err;
}})();
						$pyjs['track']['lineno']=129;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return tagContainer['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_97_err){if (!$p['isinstance']($pyjs_dbg_97_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_97_err);}throw $pyjs_dbg_97_err;
}})();
						$pyjs['track']['lineno']=131;
						langBtn = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'onLangBtnClicked')}, lang]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_98_err){if (!$p['isinstance']($pyjs_dbg_98_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_98_err);}throw $pyjs_dbg_98_err;
}})();
						$pyjs['track']['lineno']=132;
						langBtn['__is_instance__'] && typeof langBtn['__setattr__'] == 'function' ? langBtn['__setattr__']('lang', lang) : $p['setattr'](langBtn, 'lang', lang); 
						$pyjs['track']['lineno']=133;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['buttonContainer']['appendChild'](langBtn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_99_err){if (!$p['isinstance']($pyjs_dbg_99_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_99_err);}throw $pyjs_dbg_99_err;
}})();
						$pyjs['track']['lineno']=135;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['bool']($p['getattr'](self, 'readOnly')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_100_err){if (!$p['isinstance']($pyjs_dbg_100_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_100_err);}throw $pyjs_dbg_100_err;
}})()) {
							$pyjs['track']['lineno']=136;
							addBtn = (function(){try{try{$pyjs['in_try_except'] += 1;
							return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'onBtnGenTag')}, (function(){try{try{$pyjs['in_try_except'] += 1;
							return $m['translate']('New');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_101_err){if (!$p['isinstance']($pyjs_dbg_101_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_101_err);}throw $pyjs_dbg_101_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_102_err){if (!$p['isinstance']($pyjs_dbg_102_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_102_err);}throw $pyjs_dbg_102_err;
}})();
							$pyjs['track']['lineno']=137;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return addBtn['__getitem__']('class')['append']('icon new tag');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_103_err){if (!$p['isinstance']($pyjs_dbg_103_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_103_err);}throw $pyjs_dbg_103_err;
}})();
							$pyjs['track']['lineno']=138;
							addBtn['__is_instance__'] && typeof addBtn['__setattr__'] == 'function' ? addBtn['__setattr__']('lang', lang) : $p['setattr'](addBtn, 'lang', lang); 
							$pyjs['track']['lineno']=139;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return tagContainer['appendChild'](addBtn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_104_err){if (!$p['isinstance']($pyjs_dbg_104_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_104_err);}throw $pyjs_dbg_104_err;
}})();
						}
						$pyjs['track']['lineno']=141;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['languagesContainer']['appendChild'](tagContainer);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_105_err){if (!$p['isinstance']($pyjs_dbg_105_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_105_err);}throw $pyjs_dbg_105_err;
}})();
						$pyjs['track']['lineno']=142;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'langEdits')['__setitem__'](lang, tagContainer);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_106_err){if (!$p['isinstance']($pyjs_dbg_106_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_106_err);}throw $pyjs_dbg_106_err;
}})();
						$pyjs['track']['lineno']=143;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'langBtns')['__setitem__'](lang, langBtn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_107_err){if (!$p['isinstance']($pyjs_dbg_107_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_107_err);}throw $pyjs_dbg_107_err;
}})();
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.string';
					$pyjs['track']['lineno']=145;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['setLang']($p['getattr'](self, 'languages')['__getitem__']($constant_int_0));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_108_err){if (!$p['isinstance']($pyjs_dbg_108_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_108_err);}throw $pyjs_dbg_108_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and3=$p['getattr'](self, 'languages'))?!$p['bool']($p['getattr'](self, 'multiple')):$and3));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_109_err){if (!$p['isinstance']($pyjs_dbg_109_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_109_err);}throw $pyjs_dbg_109_err;
}})()) {
					$pyjs['track']['lineno']=148;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__getitem__']('class')['append']('is_translated');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_110_err){if (!$p['isinstance']($pyjs_dbg_110_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_110_err);}throw $pyjs_dbg_110_err;
}})();
					$pyjs['track']['lineno']=149;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('languagesContainer', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_111_err){if (!$p['isinstance']($pyjs_dbg_111_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_111_err);}throw $pyjs_dbg_111_err;
}})()) : $p['setattr'](self, 'languagesContainer', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_111_err){if (!$p['isinstance']($pyjs_dbg_111_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_111_err);}throw $pyjs_dbg_111_err;
}})()); 
					$pyjs['track']['lineno']=150;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'languagesContainer'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_112_err){if (!$p['isinstance']($pyjs_dbg_112_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_112_err);}throw $pyjs_dbg_112_err;
}})();
					$pyjs['track']['lineno']=151;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('buttonContainer', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_113_err){if (!$p['isinstance']($pyjs_dbg_113_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_113_err);}throw $pyjs_dbg_113_err;
}})()) : $p['setattr'](self, 'buttonContainer', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_113_err){if (!$p['isinstance']($pyjs_dbg_113_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_113_err);}throw $pyjs_dbg_113_err;
}})()); 
					$pyjs['track']['lineno']=152;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'buttonContainer')['__setitem__']('class', 'languagebuttons');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_114_err){if (!$p['isinstance']($pyjs_dbg_114_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_114_err);}throw $pyjs_dbg_114_err;
}})();
					$pyjs['track']['lineno']=153;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'buttonContainer'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_115_err){if (!$p['isinstance']($pyjs_dbg_115_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_115_err);}throw $pyjs_dbg_115_err;
}})();
					$pyjs['track']['lineno']=154;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('langEdits', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_116_err){if (!$p['isinstance']($pyjs_dbg_116_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_116_err);}throw $pyjs_dbg_116_err;
}})()) : $p['setattr'](self, 'langEdits', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_116_err){if (!$p['isinstance']($pyjs_dbg_116_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_116_err);}throw $pyjs_dbg_116_err;
}})()); 
					$pyjs['track']['lineno']=155;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('langBtns', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_117_err){if (!$p['isinstance']($pyjs_dbg_117_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_117_err);}throw $pyjs_dbg_117_err;
}})()) : $p['setattr'](self, 'langBtns', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_117_err){if (!$p['isinstance']($pyjs_dbg_117_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_117_err);}throw $pyjs_dbg_117_err;
}})()); 
					$pyjs['track']['lineno']=157;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter3_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'languages');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_118_err){if (!$p['isinstance']($pyjs_dbg_118_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_118_err);}throw $pyjs_dbg_118_err;
}})();
					$iter3_nextval=$p['__iter_prepare']($iter3_iter,false);
					while (typeof($p['__wrapped_next']($iter3_nextval)['$nextval']) != 'undefined') {
						lang = $iter3_nextval['$nextval'];
						$pyjs['track']['lineno']=158;
						langBtn = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'onLangBtnClicked')}, lang]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_119_err){if (!$p['isinstance']($pyjs_dbg_119_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_119_err);}throw $pyjs_dbg_119_err;
}})();
						$pyjs['track']['lineno']=159;
						langBtn['__is_instance__'] && typeof langBtn['__setattr__'] == 'function' ? langBtn['__setattr__']('lang', lang) : $p['setattr'](langBtn, 'lang', lang); 
						$pyjs['track']['lineno']=160;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['buttonContainer']['appendChild'](langBtn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_120_err){if (!$p['isinstance']($pyjs_dbg_120_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_120_err);}throw $pyjs_dbg_120_err;
}})();
						$pyjs['track']['lineno']=162;
						inputField = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_121_err){if (!$p['isinstance']($pyjs_dbg_121_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_121_err);}throw $pyjs_dbg_121_err;
}})();
						$pyjs['track']['lineno']=163;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return inputField['__setitem__']('type', 'text');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_122_err){if (!$p['isinstance']($pyjs_dbg_122_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_122_err);}throw $pyjs_dbg_122_err;
}})();
						$pyjs['track']['lineno']=164;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return inputField['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_123_err){if (!$p['isinstance']($pyjs_dbg_123_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_123_err);}throw $pyjs_dbg_123_err;
}})();
						$pyjs['track']['lineno']=165;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return inputField['__getitem__']('class')['append']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('lang_%s', lang);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_124_err){if (!$p['isinstance']($pyjs_dbg_124_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_124_err);}throw $pyjs_dbg_124_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_125_err){if (!$p['isinstance']($pyjs_dbg_125_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_125_err);}throw $pyjs_dbg_125_err;
}})();
						$pyjs['track']['lineno']=167;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']($p['getattr'](self, 'readOnly'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_126_err){if (!$p['isinstance']($pyjs_dbg_126_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_126_err);}throw $pyjs_dbg_126_err;
}})()) {
							$pyjs['track']['lineno']=168;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return inputField['__setitem__']('readonly', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_127_err){if (!$p['isinstance']($pyjs_dbg_127_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_127_err);}throw $pyjs_dbg_127_err;
}})();
						}
						$pyjs['track']['lineno']=170;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['languagesContainer']['appendChild'](inputField);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_128_err){if (!$p['isinstance']($pyjs_dbg_128_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_128_err);}throw $pyjs_dbg_128_err;
}})();
						$pyjs['track']['lineno']=171;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'langEdits')['__setitem__'](lang, inputField);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_129_err){if (!$p['isinstance']($pyjs_dbg_129_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_129_err);}throw $pyjs_dbg_129_err;
}})();
						$pyjs['track']['lineno']=172;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'langBtns')['__setitem__'](lang, langBtn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_130_err){if (!$p['isinstance']($pyjs_dbg_130_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_130_err);}throw $pyjs_dbg_130_err;
}})();
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.string';
					$pyjs['track']['lineno']=174;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['setLang']($p['getattr'](self, 'languages')['__getitem__']($constant_int_0));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_131_err){if (!$p['isinstance']($pyjs_dbg_131_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_131_err);}throw $pyjs_dbg_131_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and5=!$p['bool']($p['getattr'](self, 'languages')))?$p['getattr'](self, 'multiple'):$and5));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_132_err){if (!$p['isinstance']($pyjs_dbg_132_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_132_err);}throw $pyjs_dbg_132_err;
}})()) {
					$pyjs['track']['lineno']=177;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['__getitem__']('class')['append']('is_multiple');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_133_err){if (!$p['isinstance']($pyjs_dbg_133_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_133_err);}throw $pyjs_dbg_133_err;
}})();
					$pyjs['track']['lineno']=178;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('tagContainer', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_134_err){if (!$p['isinstance']($pyjs_dbg_134_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_134_err);}throw $pyjs_dbg_134_err;
}})()) : $p['setattr'](self, 'tagContainer', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Div']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_134_err){if (!$p['isinstance']($pyjs_dbg_134_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_134_err);}throw $pyjs_dbg_134_err;
}})()); 
					$pyjs['track']['lineno']=179;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'tagContainer')['__getitem__']('class')['append']('tagcontainer');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_135_err){if (!$p['isinstance']($pyjs_dbg_135_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_135_err);}throw $pyjs_dbg_135_err;
}})();
					$pyjs['track']['lineno']=180;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'tagContainer'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_136_err){if (!$p['isinstance']($pyjs_dbg_136_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_136_err);}throw $pyjs_dbg_136_err;
}})();
					$pyjs['track']['lineno']=182;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool'](!$p['bool']($p['getattr'](self, 'readOnly')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_137_err){if (!$p['isinstance']($pyjs_dbg_137_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_137_err);}throw $pyjs_dbg_137_err;
}})()) {
						$pyjs['track']['lineno']=183;
						addBtn = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $pyjs_kwargs_call($m['html5']['ext'], 'Button', null, null, [{'callback':$p['getattr'](self, 'onBtnGenTag')}, (function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['translate']('New');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_138_err){if (!$p['isinstance']($pyjs_dbg_138_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_138_err);}throw $pyjs_dbg_138_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_139_err){if (!$p['isinstance']($pyjs_dbg_139_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_139_err);}throw $pyjs_dbg_139_err;
}})();
						$pyjs['track']['lineno']=184;
						addBtn['__is_instance__'] && typeof addBtn['__setattr__'] == 'function' ? addBtn['__setattr__']('lang', null) : $p['setattr'](addBtn, 'lang', null); 
						$pyjs['track']['lineno']=185;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return addBtn['__getitem__']('class')['append']('icon new tag');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_140_err){if (!$p['isinstance']($pyjs_dbg_140_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_140_err);}throw $pyjs_dbg_140_err;
}})();
						$pyjs['track']['lineno']=187;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['tagContainer']['appendChild'](addBtn);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_141_err){if (!$p['isinstance']($pyjs_dbg_141_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_141_err);}throw $pyjs_dbg_141_err;
}})();
					}
				}
				else {
					$pyjs['track']['lineno']=190;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('input', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_142_err){if (!$p['isinstance']($pyjs_dbg_142_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_142_err);}throw $pyjs_dbg_142_err;
}})()) : $p['setattr'](self, 'input', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_142_err){if (!$p['isinstance']($pyjs_dbg_142_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_142_err);}throw $pyjs_dbg_142_err;
}})()); 
					$pyjs['track']['lineno']=191;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'input')['__setitem__']('type', 'text');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_143_err){if (!$p['isinstance']($pyjs_dbg_143_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_143_err);}throw $pyjs_dbg_143_err;
}})();
					$pyjs['track']['lineno']=192;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'input'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_144_err){if (!$p['isinstance']($pyjs_dbg_144_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_144_err);}throw $pyjs_dbg_144_err;
}})();
					$pyjs['track']['lineno']=194;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'readOnly'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_145_err){if (!$p['isinstance']($pyjs_dbg_145_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_145_err);}throw $pyjs_dbg_145_err;
}})()) {
						$pyjs['track']['lineno']=195;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'input')['__setitem__']('readonly', true);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_146_err){if (!$p['isinstance']($pyjs_dbg_146_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_146_err);}throw $pyjs_dbg_146_err;
}})();
					}
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['modulName'],['boneName'],['readOnly'],['multiple', false],['languages', null]]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=198;
			$method = $pyjs__bind_method2('fromSkelStructure', function(modulName, boneName, skelStructure) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var $and8,multiple,$and7,languages,readOnly;
				$pyjs['track']={'module':'bones.string', 'lineno':198};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=198;
				$pyjs['track']['lineno']=199;
				readOnly = ($p['bool']($and7=(function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_147_err){if (!$p['isinstance']($pyjs_dbg_147_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_147_err);}throw $pyjs_dbg_147_err;
}})()['__contains__']('readonly'))?skelStructure['__getitem__'](boneName)['__getitem__']('readonly'):$and7);
				$pyjs['track']['lineno']=201;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return skelStructure['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_148_err){if (!$p['isinstance']($pyjs_dbg_148_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_148_err);}throw $pyjs_dbg_148_err;
}})()['__contains__'](boneName));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_149_err){if (!$p['isinstance']($pyjs_dbg_149_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_149_err);}throw $pyjs_dbg_149_err;
}})()) {
					$pyjs['track']['lineno']=202;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_150_err){if (!$p['isinstance']($pyjs_dbg_150_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_150_err);}throw $pyjs_dbg_150_err;
}})()['__contains__']('multiple'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_151_err){if (!$p['isinstance']($pyjs_dbg_151_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_151_err);}throw $pyjs_dbg_151_err;
}})()) {
						$pyjs['track']['lineno']=203;
						multiple = skelStructure['__getitem__'](boneName)['__getitem__']('multiple');
					}
					else {
						$pyjs['track']['lineno']=205;
						multiple = false;
					}
					$pyjs['track']['lineno']=206;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return skelStructure['__getitem__'](boneName)['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_152_err){if (!$p['isinstance']($pyjs_dbg_152_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_152_err);}throw $pyjs_dbg_152_err;
}})()['__contains__']('languages'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_153_err){if (!$p['isinstance']($pyjs_dbg_153_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_153_err);}throw $pyjs_dbg_153_err;
}})()) {
						$pyjs['track']['lineno']=207;
						languages = skelStructure['__getitem__'](boneName)['__getitem__']('languages');
					}
					else {
						$pyjs['track']['lineno']=209;
						languages = null;
					}
				}
				$pyjs['track']['lineno']=210;
				$pyjs['track']['lineno']=210;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['StringEditBone'], null, null, [{'multiple':multiple, 'languages':languages}, modulName, boneName, readOnly]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_154_err){if (!$p['isinstance']($pyjs_dbg_154_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_154_err);}throw $pyjs_dbg_154_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['modulName'],['boneName'],['skelStructure']]);
			$cls_definition['fromSkelStructure'] = $method;
			$pyjs['track']['lineno']=212;
			$method = $pyjs__bind_method2('onLangBtnClicked', function(btn) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					btn = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a55280d6f690d79eeb16b3b6ef1a0a17') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.string', 'lineno':212};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=212;
				$pyjs['track']['lineno']=213;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['setLang']($p['getattr'](btn, 'lang'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_155_err){if (!$p['isinstance']($pyjs_dbg_155_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_155_err);}throw $pyjs_dbg_155_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['btn']]);
			$cls_definition['onLangBtnClicked'] = $method;
			$pyjs['track']['lineno']=215;
			$method = $pyjs__bind_method2('isFilled', function(lang) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 0 || arguments['length'] > 1)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					lang = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a55280d6f690d79eeb16b3b6ef1a0a17') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof lang == 'undefined') lang=arguments['callee']['__args__'][3][1];
				var $and9,$iter5_idx,$iter5_nextval,$iter4_nextval,$iter5_array,$iter4_idx,item,$and10,$iter5_iter,$iter4_type,$pyjs__trackstack_size_1,$iter4_array,$iter5_type,$and12,$iter4_iter,$and11;
				$pyjs['track']={'module':'bones.string', 'lineno':215};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=215;
				$pyjs['track']['lineno']=216;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'languages'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_156_err){if (!$p['isinstance']($pyjs_dbg_156_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_156_err);}throw $pyjs_dbg_156_err;
}})()) {
					$pyjs['track']['lineno']=217;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_is'](lang, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_157_err){if (!$p['isinstance']($pyjs_dbg_157_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_157_err);}throw $pyjs_dbg_157_err;
}})()) {
						$pyjs['track']['lineno']=218;
						lang = $p['getattr'](self, 'languages')['__getitem__']($constant_int_0);
					}
					$pyjs['track']['lineno']=220;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['getattr'](self, 'multiple'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_158_err){if (!$p['isinstance']($pyjs_dbg_158_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_158_err);}throw $pyjs_dbg_158_err;
}})()) {
						$pyjs['track']['lineno']=221;
						$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
						$iter4_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr']($p['getattr'](self, 'langEdits')['__getitem__'](lang), '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_159_err){if (!$p['isinstance']($pyjs_dbg_159_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_159_err);}throw $pyjs_dbg_159_err;
}})();
						$iter4_nextval=$p['__iter_prepare']($iter4_iter,false);
						while (typeof($p['__wrapped_next']($iter4_nextval)['$nextval']) != 'undefined') {
							item = $iter4_nextval['$nextval'];
							$pyjs['track']['lineno']=222;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool'](($p['bool']($and9=(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['isinstance'](item, $m['Tag']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_160_err){if (!$p['isinstance']($pyjs_dbg_160_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_160_err);}throw $pyjs_dbg_160_err;
}})())?$p['getattr'](item, 'input')['__getitem__']('value'):$and9));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_161_err){if (!$p['isinstance']($pyjs_dbg_161_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_161_err);}throw $pyjs_dbg_161_err;
}})()) {
								$pyjs['track']['lineno']=223;
								$pyjs['track']['lineno']=223;
								var $pyjs__ret = true;
								$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
								return $pyjs__ret;
							}
						}
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='bones.string';
						$pyjs['track']['lineno']=225;
						$pyjs['track']['lineno']=225;
						var $pyjs__ret = false;
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
					else {
						$pyjs['track']['lineno']=227;
						$pyjs['track']['lineno']=227;
						var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['len']($p['getattr'](self, 'langEdits')['__getitem__'](lang)['__getitem__']('value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_162_err){if (!$p['isinstance']($pyjs_dbg_162_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_162_err);}throw $pyjs_dbg_162_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_163_err){if (!$p['isinstance']($pyjs_dbg_163_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_163_err);}throw $pyjs_dbg_163_err;
}})();
						$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
						return $pyjs__ret;
					}
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'multiple'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_164_err){if (!$p['isinstance']($pyjs_dbg_164_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_164_err);}throw $pyjs_dbg_164_err;
}})()) {
					$pyjs['track']['lineno']=230;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter5_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr']($p['getattr'](self, 'tagContainer'), '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_165_err){if (!$p['isinstance']($pyjs_dbg_165_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_165_err);}throw $pyjs_dbg_165_err;
}})();
					$iter5_nextval=$p['__iter_prepare']($iter5_iter,false);
					while (typeof($p['__wrapped_next']($iter5_nextval)['$nextval']) != 'undefined') {
						item = $iter5_nextval['$nextval'];
						$pyjs['track']['lineno']=231;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](($p['bool']($and11=(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['isinstance'](item, $m['Tag']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_166_err){if (!$p['isinstance']($pyjs_dbg_166_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_166_err);}throw $pyjs_dbg_166_err;
}})())?$p['getattr'](item, 'input')['__getitem__']('value'):$and11));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_167_err){if (!$p['isinstance']($pyjs_dbg_167_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_167_err);}throw $pyjs_dbg_167_err;
}})()) {
							$pyjs['track']['lineno']=232;
							$pyjs['track']['lineno']=232;
							var $pyjs__ret = true;
							$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
							return $pyjs__ret;
						}
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.string';
					$pyjs['track']['lineno']=234;
					$pyjs['track']['lineno']=234;
					var $pyjs__ret = false;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=236;
				$pyjs['track']['lineno']=236;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['len']($p['getattr'](self, 'input')['__getitem__']('value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_168_err){if (!$p['isinstance']($pyjs_dbg_168_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_168_err);}throw $pyjs_dbg_168_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_169_err){if (!$p['isinstance']($pyjs_dbg_169_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_169_err);}throw $pyjs_dbg_169_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['lang', null]]);
			$cls_definition['isFilled'] = $method;
			$pyjs['track']['lineno']=238;
			$method = $pyjs__bind_method2('_updateLanguageButtons', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a55280d6f690d79eeb16b3b6ef1a0a17') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var lang,$iter6_idx,$iter6_type,$iter6_array,$pyjs__trackstack_size_1,$iter6_iter,$iter6_nextval;
				$pyjs['track']={'module':'bones.string', 'lineno':238};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=238;
				$pyjs['track']['lineno']=239;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']($p['getattr'](self, 'languages')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_170_err){if (!$p['isinstance']($pyjs_dbg_170_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_170_err);}throw $pyjs_dbg_170_err;
}})()) {
					$pyjs['track']['lineno']=240;
					$pyjs['track']['lineno']=240;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=242;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter6_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'languages');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_171_err){if (!$p['isinstance']($pyjs_dbg_171_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_171_err);}throw $pyjs_dbg_171_err;
}})();
				$iter6_nextval=$p['__iter_prepare']($iter6_iter,false);
				while (typeof($p['__wrapped_next']($iter6_nextval)['$nextval']) != 'undefined') {
					lang = $iter6_nextval['$nextval'];
					$pyjs['track']['lineno']=243;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return self['isFilled'](lang);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_172_err){if (!$p['isinstance']($pyjs_dbg_172_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_172_err);}throw $pyjs_dbg_172_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_173_err){if (!$p['isinstance']($pyjs_dbg_173_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_173_err);}throw $pyjs_dbg_173_err;
}})()) {
						$pyjs['track']['lineno']=244;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'langBtns')['__getitem__'](lang)['__getitem__']('class')['remove']('is_unfilled');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_174_err){if (!$p['isinstance']($pyjs_dbg_174_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_174_err);}throw $pyjs_dbg_174_err;
}})();
						$pyjs['track']['lineno']=245;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['bool']($p['getattr'](self, 'langBtns')['__getitem__'](lang)['__getitem__']('class')['__contains__']('is_filled')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_175_err){if (!$p['isinstance']($pyjs_dbg_175_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_175_err);}throw $pyjs_dbg_175_err;
}})()) {
							$pyjs['track']['lineno']=246;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['getattr'](self, 'langBtns')['__getitem__'](lang)['__getitem__']('class')['append']('is_filled');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_176_err){if (!$p['isinstance']($pyjs_dbg_176_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_176_err);}throw $pyjs_dbg_176_err;
}})();
						}
					}
					else {
						$pyjs['track']['lineno']=248;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'langBtns')['__getitem__'](lang)['__getitem__']('class')['remove']('is_filled');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_177_err){if (!$p['isinstance']($pyjs_dbg_177_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_177_err);}throw $pyjs_dbg_177_err;
}})();
						$pyjs['track']['lineno']=249;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['bool']($p['getattr'](self, 'langBtns')['__getitem__'](lang)['__getitem__']('class')['__contains__']('is_unfilled')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_178_err){if (!$p['isinstance']($pyjs_dbg_178_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_178_err);}throw $pyjs_dbg_178_err;
}})()) {
							$pyjs['track']['lineno']=250;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['getattr'](self, 'langBtns')['__getitem__'](lang)['__getitem__']('class')['append']('is_unfilled');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_179_err){if (!$p['isinstance']($pyjs_dbg_179_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_179_err);}throw $pyjs_dbg_179_err;
}})();
						}
					}
					$pyjs['track']['lineno']=252;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_eq'](lang, $p['getattr'](self, 'currentLanguage')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_180_err){if (!$p['isinstance']($pyjs_dbg_180_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_180_err);}throw $pyjs_dbg_180_err;
}})()) {
						$pyjs['track']['lineno']=256;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!$p['bool']($p['getattr'](self, 'langBtns')['__getitem__'](lang)['__getitem__']('class')['__contains__']('is_active')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_181_err){if (!$p['isinstance']($pyjs_dbg_181_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_181_err);}throw $pyjs_dbg_181_err;
}})()) {
							$pyjs['track']['lineno']=257;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['getattr'](self, 'langBtns')['__getitem__'](lang)['__getitem__']('class')['append']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_182_err){if (!$p['isinstance']($pyjs_dbg_182_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_182_err);}throw $pyjs_dbg_182_err;
}})();
						}
					}
					else {
						$pyjs['track']['lineno']=259;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr'](self, 'langBtns')['__getitem__'](lang)['__getitem__']('class')['remove']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_183_err){if (!$p['isinstance']($pyjs_dbg_183_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_183_err);}throw $pyjs_dbg_183_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='bones.string';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self']]);
			$cls_definition['_updateLanguageButtons'] = $method;
			$pyjs['track']['lineno']=261;
			$method = $pyjs__bind_method2('setLang', function(lang) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					lang = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a55280d6f690d79eeb16b3b6ef1a0a17') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var btn,$iter7_nextval,$iter7_iter,$iter7_array,$iter7_idx,$iter7_type,$pyjs__trackstack_size_1;
				$pyjs['track']={'module':'bones.string', 'lineno':261};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=261;
				$pyjs['track']['lineno']=262;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['getattr'](self, 'currentLanguage'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_184_err){if (!$p['isinstance']($pyjs_dbg_184_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_184_err);}throw $pyjs_dbg_184_err;
}})()) {
					$pyjs['track']['lineno']=263;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'langEdits')['__getitem__']($p['getattr'](self, 'currentLanguage'))['__getitem__']('style')['__setitem__']('display', 'none');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_185_err){if (!$p['isinstance']($pyjs_dbg_185_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_185_err);}throw $pyjs_dbg_185_err;
}})();
				}
				$pyjs['track']['lineno']=265;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('currentLanguage', lang) : $p['setattr'](self, 'currentLanguage', lang); 
				$pyjs['track']['lineno']=266;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr'](self, 'langEdits')['__getitem__']($p['getattr'](self, 'currentLanguage'))['__getitem__']('style')['__setitem__']('display', '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_186_err){if (!$p['isinstance']($pyjs_dbg_186_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_186_err);}throw $pyjs_dbg_186_err;
}})();
				$pyjs['track']['lineno']=267;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_updateLanguageButtons']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_187_err){if (!$p['isinstance']($pyjs_dbg_187_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_187_err);}throw $pyjs_dbg_187_err;
}})();
				$pyjs['track']['lineno']=269;
				$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
				$iter7_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['getattr']($p['getattr'](self, 'buttonContainer'), '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_188_err){if (!$p['isinstance']($pyjs_dbg_188_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_188_err);}throw $pyjs_dbg_188_err;
}})();
				$iter7_nextval=$p['__iter_prepare']($iter7_iter,false);
				while (typeof($p['__wrapped_next']($iter7_nextval)['$nextval']) != 'undefined') {
					btn = $iter7_nextval['$nextval'];
					$pyjs['track']['lineno']=270;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']($p['op_eq']($p['getattr'](btn, 'lang'), lang));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_189_err){if (!$p['isinstance']($pyjs_dbg_189_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_189_err);}throw $pyjs_dbg_189_err;
}})()) {
						$pyjs['track']['lineno']=271;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](!btn['__getitem__']('class')['__contains__']('is_active'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_190_err){if (!$p['isinstance']($pyjs_dbg_190_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_190_err);}throw $pyjs_dbg_190_err;
}})()) {
							$pyjs['track']['lineno']=272;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return btn['__getitem__']('class')['append']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_191_err){if (!$p['isinstance']($pyjs_dbg_191_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_191_err);}throw $pyjs_dbg_191_err;
}})();
						}
					}
					else {
						$pyjs['track']['lineno']=274;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return btn['__getitem__']('class')['remove']('is_active');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_192_err){if (!$p['isinstance']($pyjs_dbg_192_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_192_err);}throw $pyjs_dbg_192_err;
}})();
					}
				}
				if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
					$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
					$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
				}
				$pyjs['track']['module']='bones.string';
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['lang']]);
			$cls_definition['setLang'] = $method;
			$pyjs['track']['lineno']=276;
			$method = $pyjs__bind_method2('onBtnGenTag', function(btn) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					btn = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a55280d6f690d79eeb16b3b6ef1a0a17') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var tag;
				$pyjs['track']={'module':'bones.string', 'lineno':276};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=276;
				$pyjs['track']['lineno']=277;
				tag = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(self, 'genTag', null, null, [{'lang':$p['getattr'](btn, 'lang')}, '']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_193_err){if (!$p['isinstance']($pyjs_dbg_193_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_193_err);}throw $pyjs_dbg_193_err;
}})();
				$pyjs['track']['lineno']=278;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return tag['focus']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_194_err){if (!$p['isinstance']($pyjs_dbg_194_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_194_err);}throw $pyjs_dbg_194_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['btn']]);
			$cls_definition['onBtnGenTag'] = $method;
			$pyjs['track']['lineno']=280;
			$method = $pyjs__bind_method2('unserialize', function(data, extendedErrorInformation) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 2)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 3, arguments['length']+1);
				} else {
					var self = arguments[0];
					data = arguments[1];
					extendedErrorInformation = arguments[2];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 2 || arguments['length'] > 3)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 3, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a55280d6f690d79eeb16b3b6ef1a0a17') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof extendedErrorInformation == 'undefined') extendedErrorInformation=arguments['callee']['__args__'][4][1];
				var $iter11_nextval,$iter10_nextval,$iter11_array,$iter8_iter,$iter10_iter,$and20,$and18,val,$iter9_iter,$iter9_nextval,$iter9_idx,$iter11_idx,$iter9_type,$iter10_idx,$iter8_idx,$iter11_iter,$iter8_type,$pyjs__trackstack_size_1,$and13,$and16,$iter8_nextval,$and14,$and15,$and17,$and19,lang,tagStr,$iter11_type,$iter8_array,$iter10_array,$pyjs__trackstack_size_2,v,$iter10_type,$iter9_array;
				$pyjs['track']={'module':'bones.string', 'lineno':280};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=280;
				$pyjs['track']['lineno']=281;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_195_err){if (!$p['isinstance']($pyjs_dbg_195_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_195_err);}throw $pyjs_dbg_195_err;
}})()['__contains__']($p['getattr'](self, 'boneName'))));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_196_err){if (!$p['isinstance']($pyjs_dbg_196_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_196_err);}throw $pyjs_dbg_196_err;
}})()) {
					$pyjs['track']['lineno']=282;
					$pyjs['track']['lineno']=282;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=283;
				data = data['__getitem__']($p['getattr'](self, 'boneName'));
				$pyjs['track']['lineno']=284;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['bool'](data));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_197_err){if (!$p['isinstance']($pyjs_dbg_197_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_197_err);}throw $pyjs_dbg_197_err;
}})()) {
					$pyjs['track']['lineno']=285;
					$pyjs['track']['lineno']=285;
					var $pyjs__ret = null;
					$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
					return $pyjs__ret;
				}
				$pyjs['track']['lineno']=286;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and13=$p['getattr'](self, 'languages'))?$p['getattr'](self, 'multiple'):$and13));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_198_err){if (!$p['isinstance']($pyjs_dbg_198_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_198_err);}throw $pyjs_dbg_198_err;
}})()) {
					$pyjs['track']['lineno']=287;
					if (!( (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](data, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_199_err){if (!$p['isinstance']($pyjs_dbg_199_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_199_err);}throw $pyjs_dbg_199_err;
}})() )) {
					   throw $p['AssertionError']();
					 }
					$pyjs['track']['lineno']=288;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter8_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'languages');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_200_err){if (!$p['isinstance']($pyjs_dbg_200_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_200_err);}throw $pyjs_dbg_200_err;
}})();
					$iter8_nextval=$p['__iter_prepare']($iter8_iter,false);
					while (typeof($p['__wrapped_next']($iter8_nextval)['$nextval']) != 'undefined') {
						lang = $iter8_nextval['$nextval'];
						$pyjs['track']['lineno']=289;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_201_err){if (!$p['isinstance']($pyjs_dbg_201_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_201_err);}throw $pyjs_dbg_201_err;
}})()['__contains__'](lang));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_202_err){if (!$p['isinstance']($pyjs_dbg_202_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_202_err);}throw $pyjs_dbg_202_err;
}})()) {
							$pyjs['track']['lineno']=290;
							val = data['__getitem__'](lang);
							$pyjs['track']['lineno']=291;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['isinstance'](val, $p['str']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_203_err){if (!$p['isinstance']($pyjs_dbg_203_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_203_err);}throw $pyjs_dbg_203_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_204_err){if (!$p['isinstance']($pyjs_dbg_204_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_204_err);}throw $pyjs_dbg_204_err;
}})()) {
								$pyjs['track']['lineno']=292;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return $pyjs_kwargs_call(self, 'genTag', null, null, [{'lang':lang}, (function(){try{try{$pyjs['in_try_except'] += 1;
								return $m['html5']['utils']['unescape'](val);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_205_err){if (!$p['isinstance']($pyjs_dbg_205_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_205_err);}throw $pyjs_dbg_205_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_206_err){if (!$p['isinstance']($pyjs_dbg_206_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_206_err);}throw $pyjs_dbg_206_err;
}})();
							}
							else if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['isinstance'](val, $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_207_err){if (!$p['isinstance']($pyjs_dbg_207_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_207_err);}throw $pyjs_dbg_207_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_208_err){if (!$p['isinstance']($pyjs_dbg_208_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_208_err);}throw $pyjs_dbg_208_err;
}})()) {
								$pyjs['track']['lineno']=294;
								$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
								$iter9_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
								return val;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_209_err){if (!$p['isinstance']($pyjs_dbg_209_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_209_err);}throw $pyjs_dbg_209_err;
}})();
								$iter9_nextval=$p['__iter_prepare']($iter9_iter,false);
								while (typeof($p['__wrapped_next']($iter9_nextval)['$nextval']) != 'undefined') {
									v = $iter9_nextval['$nextval'];
									$pyjs['track']['lineno']=295;
									(function(){try{try{$pyjs['in_try_except'] += 1;
									return $pyjs_kwargs_call(self, 'genTag', null, null, [{'lang':lang}, (function(){try{try{$pyjs['in_try_except'] += 1;
									return $m['html5']['utils']['unescape'](v);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_210_err){if (!$p['isinstance']($pyjs_dbg_210_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_210_err);}throw $pyjs_dbg_210_err;
}})()]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_211_err){if (!$p['isinstance']($pyjs_dbg_211_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_211_err);}throw $pyjs_dbg_211_err;
}})();
								}
								if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
									$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
									$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
								}
								$pyjs['track']['module']='bones.string';
							}
						}
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.string';
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and15=$p['getattr'](self, 'languages'))?!$p['bool']($p['getattr'](self, 'multiple')):$and15));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_212_err){if (!$p['isinstance']($pyjs_dbg_212_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_212_err);}throw $pyjs_dbg_212_err;
}})()) {
					$pyjs['track']['lineno']=297;
					if (!( (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](data, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_213_err){if (!$p['isinstance']($pyjs_dbg_213_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_213_err);}throw $pyjs_dbg_213_err;
}})() )) {
					   throw $p['AssertionError']();
					 }
					$pyjs['track']['lineno']=298;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter10_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'languages');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_214_err){if (!$p['isinstance']($pyjs_dbg_214_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_214_err);}throw $pyjs_dbg_214_err;
}})();
					$iter10_nextval=$p['__iter_prepare']($iter10_iter,false);
					while (typeof($p['__wrapped_next']($iter10_nextval)['$nextval']) != 'undefined') {
						lang = $iter10_nextval['$nextval'];
						$pyjs['track']['lineno']=299;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](($p['bool']($and17=(function(){try{try{$pyjs['in_try_except'] += 1;
						return data['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_215_err){if (!$p['isinstance']($pyjs_dbg_215_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_215_err);}throw $pyjs_dbg_215_err;
}})()['__contains__'](lang))?data['__getitem__'](lang):$and17));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_216_err){if (!$p['isinstance']($pyjs_dbg_216_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_216_err);}throw $pyjs_dbg_216_err;
}})()) {
							$pyjs['track']['lineno']=300;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['getattr'](self, 'langEdits')['__getitem__'](lang)['__setitem__']('value', (function(){try{try{$pyjs['in_try_except'] += 1;
							return $m['html5']['utils']['unescape']((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['str'](data['__getitem__'](lang));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_217_err){if (!$p['isinstance']($pyjs_dbg_217_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_217_err);}throw $pyjs_dbg_217_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_218_err){if (!$p['isinstance']($pyjs_dbg_218_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_218_err);}throw $pyjs_dbg_218_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_219_err){if (!$p['isinstance']($pyjs_dbg_219_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_219_err);}throw $pyjs_dbg_219_err;
}})();
						}
						else {
							$pyjs['track']['lineno']=302;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['getattr'](self, 'langEdits')['__getitem__'](lang)['__setitem__']('value', '');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_220_err){if (!$p['isinstance']($pyjs_dbg_220_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_220_err);}throw $pyjs_dbg_220_err;
}})();
						}
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.string';
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and19=!$p['bool']($p['getattr'](self, 'languages')))?$p['getattr'](self, 'multiple'):$and19));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_221_err){if (!$p['isinstance']($pyjs_dbg_221_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_221_err);}throw $pyjs_dbg_221_err;
}})()) {
					$pyjs['track']['lineno']=304;
					if ((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['isinstance'](data, $p['list']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_222_err){if (!$p['isinstance']($pyjs_dbg_222_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_222_err);}throw $pyjs_dbg_222_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_223_err){if (!$p['isinstance']($pyjs_dbg_223_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_223_err);}throw $pyjs_dbg_223_err;
}})()) {
						$pyjs['track']['lineno']=305;
						$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
						$iter11_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
						return data;
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_224_err){if (!$p['isinstance']($pyjs_dbg_224_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_224_err);}throw $pyjs_dbg_224_err;
}})();
						$iter11_nextval=$p['__iter_prepare']($iter11_iter,false);
						while (typeof($p['__wrapped_next']($iter11_nextval)['$nextval']) != 'undefined') {
							tagStr = $iter11_nextval['$nextval'];
							$pyjs['track']['lineno']=306;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return self['genTag']((function(){try{try{$pyjs['in_try_except'] += 1;
							return $m['html5']['utils']['unescape'](tagStr);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_225_err){if (!$p['isinstance']($pyjs_dbg_225_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_225_err);}throw $pyjs_dbg_225_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_226_err){if (!$p['isinstance']($pyjs_dbg_226_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_226_err);}throw $pyjs_dbg_226_err;
}})();
						}
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='bones.string';
					}
					else {
						$pyjs['track']['lineno']=308;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return self['genTag']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $m['html5']['utils']['unescape'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_227_err){if (!$p['isinstance']($pyjs_dbg_227_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_227_err);}throw $pyjs_dbg_227_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_228_err){if (!$p['isinstance']($pyjs_dbg_228_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_228_err);}throw $pyjs_dbg_228_err;
}})();
					}
				}
				else {
					$pyjs['track']['lineno']=310;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'input')['__setitem__']('value', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['utils']['unescape']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['str'](data);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_229_err){if (!$p['isinstance']($pyjs_dbg_229_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_229_err);}throw $pyjs_dbg_229_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_230_err){if (!$p['isinstance']($pyjs_dbg_230_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_230_err);}throw $pyjs_dbg_230_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_231_err){if (!$p['isinstance']($pyjs_dbg_231_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_231_err);}throw $pyjs_dbg_231_err;
}})();
				}
				$pyjs['track']['lineno']=312;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['_updateLanguageButtons']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_232_err){if (!$p['isinstance']($pyjs_dbg_232_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_232_err);}throw $pyjs_dbg_232_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['data'],['extendedErrorInformation', null]]);
			$cls_definition['unserialize'] = $method;
			$pyjs['track']['lineno']=314;
			$method = $pyjs__bind_method2('serializeForPost', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a55280d6f690d79eeb16b3b6ef1a0a17') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $and28,$iter13_idx,$and23,$and22,$and21,txt,$and27,$and26,$and25,$and24,$iter15_iter,res,$iter13_type,$iter14_array,$iter15_array,$iter14_type,$iter15_type,$iter12_array,$iter14_iter,child,$iter14_idx,$iter14_nextval,lang,$iter13_nextval,$iter13_iter,$iter12_type,$iter15_idx,$iter13_array,$iter12_iter,$pyjs__trackstack_size_2,$iter15_nextval,$pyjs__trackstack_size_1,$iter12_idx,$iter12_nextval;
				$pyjs['track']={'module':'bones.string', 'lineno':314};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=314;
				$pyjs['track']['lineno']=315;
				res = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['dict']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_233_err){if (!$p['isinstance']($pyjs_dbg_233_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_233_err);}throw $pyjs_dbg_233_err;
}})();
				$pyjs['track']['lineno']=316;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and21=$p['getattr'](self, 'languages'))?$p['getattr'](self, 'multiple'):$and21));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_234_err){if (!$p['isinstance']($pyjs_dbg_234_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_234_err);}throw $pyjs_dbg_234_err;
}})()) {
					$pyjs['track']['lineno']=317;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter12_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'languages');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_235_err){if (!$p['isinstance']($pyjs_dbg_235_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_235_err);}throw $pyjs_dbg_235_err;
}})();
					$iter12_nextval=$p['__iter_prepare']($iter12_iter,false);
					while (typeof($p['__wrapped_next']($iter12_nextval)['$nextval']) != 'undefined') {
						lang = $iter12_nextval['$nextval'];
						$pyjs['track']['lineno']=318;
						(function(){try{try{$pyjs['in_try_except'] += 1;
						return res['__setitem__']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['sprintf']('%s.%s', (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['tuple']([$p['getattr'](self, 'boneName'), lang]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_237_err){if (!$p['isinstance']($pyjs_dbg_237_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_237_err);}throw $pyjs_dbg_237_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_238_err){if (!$p['isinstance']($pyjs_dbg_238_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_238_err);}throw $pyjs_dbg_238_err;
}})(), (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_236_err){if (!$p['isinstance']($pyjs_dbg_236_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_236_err);}throw $pyjs_dbg_236_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_239_err){if (!$p['isinstance']($pyjs_dbg_239_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_239_err);}throw $pyjs_dbg_239_err;
}})();
						$pyjs['track']['lineno']=319;
						$pyjs__trackstack_size_2=$pyjs['trackstack']['length'];
						$iter13_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['getattr']($p['getattr'](self, 'langEdits')['__getitem__'](lang), '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_240_err){if (!$p['isinstance']($pyjs_dbg_240_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_240_err);}throw $pyjs_dbg_240_err;
}})();
						$iter13_nextval=$p['__iter_prepare']($iter13_iter,false);
						while (typeof($p['__wrapped_next']($iter13_nextval)['$nextval']) != 'undefined') {
							child = $iter13_nextval['$nextval'];
							$pyjs['track']['lineno']=320;
							if ((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['isinstance'](child, $m['Tag']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_241_err){if (!$p['isinstance']($pyjs_dbg_241_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_241_err);}throw $pyjs_dbg_241_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_242_err){if (!$p['isinstance']($pyjs_dbg_242_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_242_err);}throw $pyjs_dbg_242_err;
}})()) {
								$pyjs['track']['lineno']=321;
								(function(){try{try{$pyjs['in_try_except'] += 1;
								return res['__getitem__']((function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['sprintf']('%s.%s', (function(){try{try{$pyjs['in_try_except'] += 1;
								return $p['tuple']([$p['getattr'](self, 'boneName'), lang]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_243_err){if (!$p['isinstance']($pyjs_dbg_243_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_243_err);}throw $pyjs_dbg_243_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_244_err){if (!$p['isinstance']($pyjs_dbg_244_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_244_err);}throw $pyjs_dbg_244_err;
}})())['append']($p['getattr'](child, 'input')['__getitem__']('value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_245_err){if (!$p['isinstance']($pyjs_dbg_245_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_245_err);}throw $pyjs_dbg_245_err;
}})();
							}
						}
						if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_2) {
							$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_2);
							$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
						}
						$pyjs['track']['module']='bones.string';
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.string';
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and23=$p['getattr'](self, 'languages'))?!$p['bool']($p['getattr'](self, 'multiple')):$and23));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_246_err){if (!$p['isinstance']($pyjs_dbg_246_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_246_err);}throw $pyjs_dbg_246_err;
}})()) {
					$pyjs['track']['lineno']=323;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter14_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'languages');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_247_err){if (!$p['isinstance']($pyjs_dbg_247_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_247_err);}throw $pyjs_dbg_247_err;
}})();
					$iter14_nextval=$p['__iter_prepare']($iter14_iter,false);
					while (typeof($p['__wrapped_next']($iter14_nextval)['$nextval']) != 'undefined') {
						lang = $iter14_nextval['$nextval'];
						$pyjs['track']['lineno']=324;
						txt = $p['getattr'](self, 'langEdits')['__getitem__'](lang)['__getitem__']('value');
						$pyjs['track']['lineno']=325;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool'](txt);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_248_err){if (!$p['isinstance']($pyjs_dbg_248_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_248_err);}throw $pyjs_dbg_248_err;
}})()) {
							$pyjs['track']['lineno']=326;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return res['__setitem__']((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['sprintf']('%s.%s', (function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['tuple']([$p['getattr'](self, 'boneName'), lang]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_249_err){if (!$p['isinstance']($pyjs_dbg_249_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_249_err);}throw $pyjs_dbg_249_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_250_err){if (!$p['isinstance']($pyjs_dbg_250_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_250_err);}throw $pyjs_dbg_250_err;
}})(), txt);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_251_err){if (!$p['isinstance']($pyjs_dbg_251_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_251_err);}throw $pyjs_dbg_251_err;
}})();
						}
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.string';
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and25=!$p['bool']($p['getattr'](self, 'languages')))?$p['getattr'](self, 'multiple'):$and25));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_252_err){if (!$p['isinstance']($pyjs_dbg_252_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_252_err);}throw $pyjs_dbg_252_err;
}})()) {
					$pyjs['track']['lineno']=328;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return res['__setitem__']($p['getattr'](self, 'boneName'), (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['list']([]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_253_err){if (!$p['isinstance']($pyjs_dbg_253_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_253_err);}throw $pyjs_dbg_253_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_254_err){if (!$p['isinstance']($pyjs_dbg_254_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_254_err);}throw $pyjs_dbg_254_err;
}})();
					$pyjs['track']['lineno']=329;
					$pyjs__trackstack_size_1=$pyjs['trackstack']['length'];
					$iter15_iter = (function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr']($p['getattr'](self, 'tagContainer'), '_children');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_255_err){if (!$p['isinstance']($pyjs_dbg_255_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_255_err);}throw $pyjs_dbg_255_err;
}})();
					$iter15_nextval=$p['__iter_prepare']($iter15_iter,false);
					while (typeof($p['__wrapped_next']($iter15_nextval)['$nextval']) != 'undefined') {
						child = $iter15_nextval['$nextval'];
						$pyjs['track']['lineno']=330;
						if ((function(){try{try{$pyjs['in_try_except'] += 1;
							return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
						return $p['isinstance'](child, $m['Tag']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_256_err){if (!$p['isinstance']($pyjs_dbg_256_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_256_err);}throw $pyjs_dbg_256_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_257_err){if (!$p['isinstance']($pyjs_dbg_257_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_257_err);}throw $pyjs_dbg_257_err;
}})()) {
							$pyjs['track']['lineno']=331;
							(function(){try{try{$pyjs['in_try_except'] += 1;
							return res['__getitem__']($p['getattr'](self, 'boneName'))['append']($p['getattr'](child, 'input')['__getitem__']('value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_258_err){if (!$p['isinstance']($pyjs_dbg_258_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_258_err);}throw $pyjs_dbg_258_err;
}})();
						}
					}
					if ($pyjs['trackstack']['length'] > $pyjs__trackstack_size_1) {
						$pyjs['trackstack'] = $pyjs['trackstack']['slice'](0,$pyjs__trackstack_size_1);
						$pyjs['track'] = $pyjs['trackstack']['slice'](-1)[0];
					}
					$pyjs['track']['module']='bones.string';
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](($p['bool']($and27=!$p['bool']($p['getattr'](self, 'languages')))?!$p['bool']($p['getattr'](self, 'multiple')):$and27));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_259_err){if (!$p['isinstance']($pyjs_dbg_259_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_259_err);}throw $pyjs_dbg_259_err;
}})()) {
					$pyjs['track']['lineno']=333;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return res['__setitem__']($p['getattr'](self, 'boneName'), $p['getattr'](self, 'input')['__getitem__']('value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_260_err){if (!$p['isinstance']($pyjs_dbg_260_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_260_err);}throw $pyjs_dbg_260_err;
}})();
				}
				$pyjs['track']['lineno']=334;
				$pyjs['track']['lineno']=334;
				var $pyjs__ret = res;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['serializeForPost'] = $method;
			$pyjs['track']['lineno']=336;
			$method = $pyjs__bind_method2('serializeForDocument', function() {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 0) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']+1);
				} else {
					var self = arguments[0];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 1, 1, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a55280d6f690d79eeb16b3b6ef1a0a17') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.string', 'lineno':336};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=336;
				$pyjs['track']['lineno']=337;
				$pyjs['track']['lineno']=337;
				var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
				return self['serialize']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_261_err){if (!$p['isinstance']($pyjs_dbg_261_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_261_err);}throw $pyjs_dbg_261_err;
}})();
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self']]);
			$cls_definition['serializeForDocument'] = $method;
			$pyjs['track']['lineno']=339;
			$method = $pyjs__bind_method2('genTag', function(tag, editMode, lang) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 1 || arguments['length'] > 3)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 4, arguments['length']+1);
				} else {
					var self = arguments[0];
					tag = arguments[1];
					editMode = arguments[2];
					lang = arguments[3];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && (arguments['length'] < 2 || arguments['length'] > 4)) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 4, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== 'a55280d6f690d79eeb16b3b6ef1a0a17') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof editMode == 'undefined') editMode=arguments['callee']['__args__'][4][1];
				if (typeof lang == 'undefined') lang=arguments['callee']['__args__'][5][1];

				$pyjs['track']={'module':'bones.string', 'lineno':339};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=339;
				$pyjs['track']['lineno']=340;
				tag = (function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call(null, $m['Tag'], null, null, [{'readonly':$p['getattr'](self, 'readOnly')}, tag, editMode]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_262_err){if (!$p['isinstance']($pyjs_dbg_262_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_262_err);}throw $pyjs_dbg_262_err;
}})();
				$pyjs['track']['lineno']=341;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool'](!$p['op_is'](lang, null));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_263_err){if (!$p['isinstance']($pyjs_dbg_263_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_263_err);}throw $pyjs_dbg_263_err;
}})()) {
					$pyjs['track']['lineno']=342;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'langEdits')['__getitem__'](lang)['appendChild'](tag);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_264_err){if (!$p['isinstance']($pyjs_dbg_264_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_264_err);}throw $pyjs_dbg_264_err;
}})();
				}
				else {
					$pyjs['track']['lineno']=344;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['tagContainer']['appendChild'](tag);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_265_err){if (!$p['isinstance']($pyjs_dbg_265_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_265_err);}throw $pyjs_dbg_265_err;
}})();
				}
				$pyjs['track']['lineno']=346;
				$pyjs['track']['lineno']=346;
				var $pyjs__ret = tag;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['tag'],['editMode', false],['lang', null]]);
			$cls_definition['genTag'] = $method;
			$pyjs['track']['lineno']=103;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('StringEditBone', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=349;
		$m['CheckForStringBone'] = function(modulName, boneName, skelStucture) {
			if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, null, arguments['length']);
			var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

			var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
			if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
				if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
				kwargs = arguments[arguments['length']+1];
			} else {
				delete kwargs['$pyjs_is_kwarg'];
			}
			if (typeof kwargs == 'undefined') {
				kwargs = $p['__empty_dict']();
				if (typeof skelStucture != 'undefined') {
					if (skelStucture !== null && typeof skelStucture['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = skelStucture;
						skelStucture = arguments[3];
					}
				} else 				if (typeof boneName != 'undefined') {
					if (boneName !== null && typeof boneName['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = boneName;
						boneName = arguments[3];
					}
				} else 				if (typeof modulName != 'undefined') {
					if (modulName !== null && typeof modulName['$pyjs_is_kwarg'] != 'undefined') {
						kwargs = modulName;
						modulName = arguments[3];
					}
				} else {
				}
			}

			$pyjs['track']={'module':'bones.string','lineno':349};$pyjs['trackstack']['push']($pyjs['track']);
			$pyjs['track']['module']='bones.string';
			$pyjs['track']['lineno']=349;
			$pyjs['track']['lineno']=350;
			$pyjs['track']['lineno']=350;
			var $pyjs__ret = (function(){try{try{$pyjs['in_try_except'] += 1;
			return (function(){try{try{$pyjs['in_try_except'] += 1;
			return $p['str'](skelStucture['__getitem__'](boneName)['__getitem__']('type'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_266_err){if (!$p['isinstance']($pyjs_dbg_266_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_266_err);}throw $pyjs_dbg_266_err;
}})()['startswith']('str');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_267_err){if (!$p['isinstance']($pyjs_dbg_267_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_267_err);}throw $pyjs_dbg_267_err;
}})();
			$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
			return $pyjs__ret;
		};
		$m['CheckForStringBone']['__name__'] = 'CheckForStringBone';

		$m['CheckForStringBone']['__bind_type__'] = 0;
		$m['CheckForStringBone']['__args__'] = ['args',['kwargs'],['modulName'],['boneName'],['skelStucture']];
		$pyjs['track']['lineno']=353;
		$m['ExtendedStringSearch'] = (function(){
			var $cls_definition = new Object();
			var $method;
			$cls_definition['__module__'] = 'bones.string';
			$cls_definition['__md5__'] = '0968e7cf838e635f0e3d083e22061176';
			$pyjs['track']['lineno']=354;
			$method = $pyjs__bind_method2('__init__', function(extension, view, modul) {
				if (this['__is_instance__'] === true) {
					var self = this;
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,3,arguments['length']-1));

					var kwargs = arguments['length'] >= 4 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						var kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_count'] && arguments['length'] < 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']+1);
				} else {
					var self = arguments[0];
					extension = arguments[1];
					view = arguments[2];
					modul = arguments[3];
					var args = $p['tuple']($pyjs_array_slice['call'](arguments,4,arguments['length']-1));

					var kwargs = arguments['length'] >= 5 ? arguments[arguments['length']-1] : arguments[arguments['length']];
					if (kwargs === null || typeof kwargs != 'object' || kwargs['__name__'] != 'dict' || typeof kwargs['$pyjs_is_kwarg'] == 'undefined') {
						if (typeof kwargs != 'undefined') args['__array']['push'](kwargs);
						kwargs = arguments[arguments['length']+1];
					} else {
						delete kwargs['$pyjs_is_kwarg'];
					}
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] < 4) $pyjs__exception_func_param(arguments['callee']['__name__'], 4, null, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0968e7cf838e635f0e3d083e22061176') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				if (typeof kwargs == 'undefined') {
					kwargs = $p['__empty_dict']();
					if (typeof modul != 'undefined') {
						if (modul !== null && typeof modul['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = modul;
							modul = arguments[4];
						}
					} else 					if (typeof view != 'undefined') {
						if (view !== null && typeof view['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = view;
							view = arguments[4];
						}
					} else 					if (typeof extension != 'undefined') {
						if (extension !== null && typeof extension['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = extension;
							extension = arguments[4];
						}
					} else 					if (typeof self != 'undefined') {
						if (self !== null && typeof self['$pyjs_is_kwarg'] != 'undefined') {
							kwargs = self;
							self = arguments[4];
						}
					} else {
					}
				}

				$pyjs['track']={'module':'bones.string', 'lineno':354};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=354;
				$pyjs['track']['lineno']=355;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return $pyjs_kwargs_call((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['$$super']($m['ExtendedStringSearch'], self);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_268_err){if (!$p['isinstance']($pyjs_dbg_268_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_268_err);}throw $pyjs_dbg_268_err;
}})(), '__init__', args, kwargs, [{}]);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_269_err){if (!$p['isinstance']($pyjs_dbg_269_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_269_err);}throw $pyjs_dbg_269_err;
}})();
				$pyjs['track']['lineno']=356;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('view', view) : $p['setattr'](self, 'view', view); 
				$pyjs['track']['lineno']=357;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('extension', extension) : $p['setattr'](self, 'extension', extension); 
				$pyjs['track']['lineno']=358;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('modul', modul) : $p['setattr'](self, 'modul', modul); 
				$pyjs['track']['lineno']=359;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('opMode', extension['__getitem__']('mode')) : $p['setattr'](self, 'opMode', extension['__getitem__']('mode')); 
				$pyjs['track']['lineno']=360;
				self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('filterChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('filterChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_270_err){if (!$p['isinstance']($pyjs_dbg_270_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_270_err);}throw $pyjs_dbg_270_err;
}})()) : $p['setattr'](self, 'filterChangedEvent', (function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['EventDispatcher']('filterChanged');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_270_err){if (!$p['isinstance']($pyjs_dbg_270_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_270_err);}throw $pyjs_dbg_270_err;
}})()); 
				$pyjs['track']['lineno']=361;
				if (!( (function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list'](['equals', 'from', 'to', 'prefix', 'range']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_271_err){if (!$p['isinstance']($pyjs_dbg_271_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_271_err);}throw $pyjs_dbg_271_err;
}})()['__contains__']($p['getattr'](self, 'opMode')) )) {
				   throw $p['AssertionError']();
				 }
				$pyjs['track']['lineno']=362;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $m['html5']['TextNode'](extension['__getitem__']('name'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_272_err){if (!$p['isinstance']($pyjs_dbg_272_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_272_err);}throw $pyjs_dbg_272_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_273_err){if (!$p['isinstance']($pyjs_dbg_273_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_273_err);}throw $pyjs_dbg_273_err;
}})();
				$pyjs['track']['lineno']=363;
				(function(){try{try{$pyjs['in_try_except'] += 1;
				return self['sinkEvent']('onKeyDown');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_274_err){if (!$p['isinstance']($pyjs_dbg_274_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_274_err);}throw $pyjs_dbg_274_err;
}})();
				$pyjs['track']['lineno']=364;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['list'](['equals', 'from', 'to', 'prefix']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_275_err){if (!$p['isinstance']($pyjs_dbg_275_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_275_err);}throw $pyjs_dbg_275_err;
}})()['__contains__']($p['getattr'](self, 'opMode')));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_276_err){if (!$p['isinstance']($pyjs_dbg_276_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_276_err);}throw $pyjs_dbg_276_err;
}})()) {
					$pyjs['track']['lineno']=365;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('input', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_277_err){if (!$p['isinstance']($pyjs_dbg_277_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_277_err);}throw $pyjs_dbg_277_err;
}})()) : $p['setattr'](self, 'input', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_277_err){if (!$p['isinstance']($pyjs_dbg_277_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_277_err);}throw $pyjs_dbg_277_err;
}})()); 
					$pyjs['track']['lineno']=366;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'input')['__setitem__']('type', 'text');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_278_err){if (!$p['isinstance']($pyjs_dbg_278_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_278_err);}throw $pyjs_dbg_278_err;
}})();
					$pyjs['track']['lineno']=367;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'input'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_279_err){if (!$p['isinstance']($pyjs_dbg_279_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_279_err);}throw $pyjs_dbg_279_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](self, 'opMode'), 'range'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_280_err){if (!$p['isinstance']($pyjs_dbg_280_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_280_err);}throw $pyjs_dbg_280_err;
}})()) {
					$pyjs['track']['lineno']=369;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('input1', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_281_err){if (!$p['isinstance']($pyjs_dbg_281_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_281_err);}throw $pyjs_dbg_281_err;
}})()) : $p['setattr'](self, 'input1', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_281_err){if (!$p['isinstance']($pyjs_dbg_281_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_281_err);}throw $pyjs_dbg_281_err;
}})()); 
					$pyjs['track']['lineno']=370;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'input1')['__setitem__']('type', 'text');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_282_err){if (!$p['isinstance']($pyjs_dbg_282_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_282_err);}throw $pyjs_dbg_282_err;
}})();
					$pyjs['track']['lineno']=371;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'input1'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_283_err){if (!$p['isinstance']($pyjs_dbg_283_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_283_err);}throw $pyjs_dbg_283_err;
}})();
					$pyjs['track']['lineno']=372;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']((function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['TextNode']('to');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_284_err){if (!$p['isinstance']($pyjs_dbg_284_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_284_err);}throw $pyjs_dbg_284_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_285_err){if (!$p['isinstance']($pyjs_dbg_285_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_285_err);}throw $pyjs_dbg_285_err;
}})();
					$pyjs['track']['lineno']=373;
					self['__is_instance__'] && typeof self['__setattr__'] == 'function' ? self['__setattr__']('input2', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_286_err){if (!$p['isinstance']($pyjs_dbg_286_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_286_err);}throw $pyjs_dbg_286_err;
}})()) : $p['setattr'](self, 'input2', (function(){try{try{$pyjs['in_try_except'] += 1;
					return $m['html5']['Input']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_286_err){if (!$p['isinstance']($pyjs_dbg_286_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_286_err);}throw $pyjs_dbg_286_err;
}})()); 
					$pyjs['track']['lineno']=374;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['getattr'](self, 'input2')['__setitem__']('type', 'text');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_287_err){if (!$p['isinstance']($pyjs_dbg_287_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_287_err);}throw $pyjs_dbg_287_err;
}})();
					$pyjs['track']['lineno']=375;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['appendChild']($p['getattr'](self, 'input2'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_288_err){if (!$p['isinstance']($pyjs_dbg_288_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_288_err);}throw $pyjs_dbg_288_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, ['args',['kwargs'],['self'],['extension'],['view'],['modul']]);
			$cls_definition['__init__'] = $method;
			$pyjs['track']['lineno']=377;
			$method = $pyjs__bind_method2('onKeyDown', function(event) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					event = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0968e7cf838e635f0e3d083e22061176') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}

				$pyjs['track']={'module':'bones.string', 'lineno':377};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=377;
				$pyjs['track']['lineno']=378;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']((function(){try{try{$pyjs['in_try_except'] += 1;
				return (typeof isReturn == "undefined"?$m['isReturn']:isReturn)($p['getattr'](event, 'keyCode'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_289_err){if (!$p['isinstance']($pyjs_dbg_289_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_289_err);}throw $pyjs_dbg_289_err;
}})());
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_290_err){if (!$p['isinstance']($pyjs_dbg_290_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_290_err);}throw $pyjs_dbg_290_err;
}})()) {
					$pyjs['track']['lineno']=379;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return self['filterChangedEvent']['fire']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_291_err){if (!$p['isinstance']($pyjs_dbg_291_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_291_err);}throw $pyjs_dbg_291_err;
}})();
				}
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return null;
			}
	, 1, [null,null,['self'],['event']]);
			$cls_definition['onKeyDown'] = $method;
			$pyjs['track']['lineno']=381;
			$method = $pyjs__bind_method2('updateFilter', function(filter) {
				if (this['__is_instance__'] === true) {
					var self = this;
					if ($pyjs['options']['arg_count'] && arguments['length'] != 1) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']+1);
				} else {
					var self = arguments[0];
					filter = arguments[1];
					if ($pyjs['options']['arg_is_instance'] && self['__is_instance__'] !== true) $pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
					if ($pyjs['options']['arg_count'] && arguments['length'] != 2) $pyjs__exception_func_param(arguments['callee']['__name__'], 2, 2, arguments['length']);
				}
				if ($pyjs['options']['arg_instance_type']) {
					if (self.prototype['__md5__'] !== '0968e7cf838e635f0e3d083e22061176') {
						if (!$p['_isinstance'](self, arguments['callee']['__class__'])) {
							$pyjs__exception_func_instance_expected(arguments['callee']['__name__'], arguments['callee']['__class__']['__name__'], self);
						}
					}
				}
				var $add2,$add3,$add1,$add6,$add7,$add4,$add5,$add10,$add8,$add9;
				$pyjs['track']={'module':'bones.string', 'lineno':381};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=381;
				$pyjs['track']['lineno']=382;
				if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](self, 'opMode'), 'equals'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_292_err){if (!$p['isinstance']($pyjs_dbg_292_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_292_err);}throw $pyjs_dbg_292_err;
}})()) {
					$pyjs['track']['lineno']=383;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return filter['__setitem__']($p['getattr'](self, 'extension')['__getitem__']('target'), $p['getattr'](self, 'input')['__getitem__']('value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_293_err){if (!$p['isinstance']($pyjs_dbg_293_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_293_err);}throw $pyjs_dbg_293_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](self, 'opMode'), 'from'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_294_err){if (!$p['isinstance']($pyjs_dbg_294_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_294_err);}throw $pyjs_dbg_294_err;
}})()) {
					$pyjs['track']['lineno']=385;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return filter['__setitem__']($p['__op_add']($add1=$p['getattr'](self, 'extension')['__getitem__']('target'),$add2='$gt'), $p['getattr'](self, 'input')['__getitem__']('value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_295_err){if (!$p['isinstance']($pyjs_dbg_295_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_295_err);}throw $pyjs_dbg_295_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](self, 'opMode'), 'to'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_296_err){if (!$p['isinstance']($pyjs_dbg_296_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_296_err);}throw $pyjs_dbg_296_err;
}})()) {
					$pyjs['track']['lineno']=387;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return filter['__setitem__']($p['__op_add']($add3=$p['getattr'](self, 'extension')['__getitem__']('target'),$add4='$lt'), $p['getattr'](self, 'input')['__getitem__']('value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_297_err){if (!$p['isinstance']($pyjs_dbg_297_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_297_err);}throw $pyjs_dbg_297_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](self, 'opMode'), 'prefix'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_298_err){if (!$p['isinstance']($pyjs_dbg_298_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_298_err);}throw $pyjs_dbg_298_err;
}})()) {
					$pyjs['track']['lineno']=389;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return filter['__setitem__']($p['__op_add']($add5=$p['getattr'](self, 'extension')['__getitem__']('target'),$add6='$lk'), $p['getattr'](self, 'input')['__getitem__']('value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_299_err){if (!$p['isinstance']($pyjs_dbg_299_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_299_err);}throw $pyjs_dbg_299_err;
}})();
				}
				else if ((function(){try{try{$pyjs['in_try_except'] += 1;
					return $p['bool']($p['op_eq']($p['getattr'](self, 'opMode'), 'range'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_300_err){if (!$p['isinstance']($pyjs_dbg_300_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_300_err);}throw $pyjs_dbg_300_err;
}})()) {
					$pyjs['track']['lineno']=391;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return filter['__setitem__']($p['__op_add']($add7=$p['getattr'](self, 'extension')['__getitem__']('target'),$add8='$gt'), $p['getattr'](self, 'input1')['__getitem__']('value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_301_err){if (!$p['isinstance']($pyjs_dbg_301_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_301_err);}throw $pyjs_dbg_301_err;
}})();
					$pyjs['track']['lineno']=392;
					(function(){try{try{$pyjs['in_try_except'] += 1;
					return filter['__setitem__']($p['__op_add']($add9=$p['getattr'](self, 'extension')['__getitem__']('target'),$add10='$lt'), $p['getattr'](self, 'input2')['__getitem__']('value'));
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_302_err){if (!$p['isinstance']($pyjs_dbg_302_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_302_err);}throw $pyjs_dbg_302_err;
}})();
				}
				$pyjs['track']['lineno']=393;
				$pyjs['track']['lineno']=393;
				var $pyjs__ret = filter;
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 1, [null,null,['self'],['filter']]);
			$cls_definition['updateFilter'] = $method;
			$pyjs['track']['lineno']=396;
			$method = $pyjs__bind_method2('canHandleExtension', function(extension, view, modul) {
				if ($pyjs['options']['arg_count'] && arguments['length'] != 3) $pyjs__exception_func_param(arguments['callee']['__name__'], 3, 3, arguments['length']);
				var $or1,$or2,$and29,$and30,$and31;
				$pyjs['track']={'module':'bones.string', 'lineno':396};$pyjs['trackstack']['push']($pyjs['track']);
				$pyjs['track']['module']='bones.string';
				$pyjs['track']['lineno']=396;
				$pyjs['track']['lineno']=397;
				$pyjs['track']['lineno']=397;
				var $pyjs__ret = ($p['bool']($and29=(function(){try{try{$pyjs['in_try_except'] += 1;
				return $p['isinstance'](extension, $p['dict']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_303_err){if (!$p['isinstance']($pyjs_dbg_303_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_303_err);}throw $pyjs_dbg_303_err;
}})())?($p['bool']($and30=(function(){try{try{$pyjs['in_try_except'] += 1;
				return extension['keys']();
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_304_err){if (!$p['isinstance']($pyjs_dbg_304_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_304_err);}throw $pyjs_dbg_304_err;
}})()['__contains__']('type'))?($p['bool']($or1=$p['op_eq'](extension['__getitem__']('type'), 'string'))?$or1:(function(){try{try{$pyjs['in_try_except'] += 1;
				return extension['__getitem__']('type')['startswith']('string.');
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_305_err){if (!$p['isinstance']($pyjs_dbg_305_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_305_err);}throw $pyjs_dbg_305_err;
}})()):$and30):$and29);
				$pyjs['trackstack']['pop']();$pyjs['track']=$pyjs['trackstack']['pop']();$pyjs['trackstack']['push']($pyjs['track']);
				return $pyjs__ret;
			}
	, 3, [null,null,['extension'],['view'],['modul']]);
			$cls_definition['canHandleExtension'] = $method;
			$pyjs['track']['lineno']=353;
			var $bases = new Array($p['getattr']($m['html5'], 'Div'));
			var $data = $p['dict']();
			for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
			return $p['_create_class']('ExtendedStringSearch', $p['tuple']($bases), $data);
		})();
		$pyjs['track']['lineno']=401;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['editBoneSelector']['insert']($constant_int_3, $m['CheckForStringBone'], $m['StringEditBone']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_306_err){if (!$p['isinstance']($pyjs_dbg_306_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_306_err);}throw $pyjs_dbg_306_err;
}})();
		$pyjs['track']['lineno']=402;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['viewDelegateSelector']['insert']($constant_int_3, $m['CheckForStringBone'], $m['StringViewBoneDelegate']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_307_err){if (!$p['isinstance']($pyjs_dbg_307_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_307_err);}throw $pyjs_dbg_307_err;
}})();
		$pyjs['track']['lineno']=403;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['extendedSearchWidgetSelector']['insert']($constant_int_1, $p['getattr']($m['ExtendedStringSearch'], 'canHandleExtension'), $m['ExtendedStringSearch']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_308_err){if (!$p['isinstance']($pyjs_dbg_308_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_308_err);}throw $pyjs_dbg_308_err;
}})();
		$pyjs['track']['lineno']=404;
		(function(){try{try{$pyjs['in_try_except'] += 1;
		return $m['extractorDelegateSelector']['insert']($constant_int_3, $m['CheckForStringBone'], $m['StringBoneExtractor']);
}finally{$pyjs['in_try_except']-=1;}}catch($pyjs_dbg_309_err){if (!$p['isinstance']($pyjs_dbg_309_err, $p['StopIteration'])){$p['_handle_exception']($pyjs_dbg_309_err);}throw $pyjs_dbg_309_err;
}})();
	} catch ($pyjs_attr_err) {throw $p['_errorMapping']($pyjs_attr_err);};
	return this;
}; /* end bones.string */


/* end module: bones.string */


/*
PYJS_DEPS: ['html5', 'priorityqueue.editBoneSelector', 'priorityqueue', 'priorityqueue.viewDelegateSelector', 'priorityqueue.extendedSearchWidgetSelector', 'priorityqueue.extractorDelegateSelector', 'config.conf', 'config', 'event.EventDispatcher', 'event', 'html5.keycodes', 'i18n.translate', 'i18n']
*/
