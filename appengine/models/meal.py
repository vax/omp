# -*- coding: utf-8 -*-
from server.bones import *
from server.skeleton import Skeleton


class mealSkel(Skeleton):
	kindName = "meal"
	name = stringBone(descr=u"Name", searchable=True, required=True, indexed=True)
	description = textBone(descr=u"Beschreibung", searchable=True, visible=True, required=True)
	price = numericBone(descr=u"Preis", required=True, indexed=True, precision=2, defaultValue=3.8)
	image = fileBone(descr=u"Bild", required=False, indexed=True)
	