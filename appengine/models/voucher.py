# -*- coding: utf-8 -*-
from server.bones import *
from server.skeleton import Skeleton


class voucherSkel(Skeleton):
	kindName="voucher"
	code = stringBone(descr=u"Gutscheincode", searchable=True, required=True, indexed=True)
	value = numericBone(descr=u"Gutscheinwert", searchable=True, required=True, indexed=True, precision=2)
	used = booleanBone(descr=u"Eingelöst", searchable=True, required=False, indexed=True)
	#datecreate = dateBone(descr=u"Erstelldatum", searchable=True, required=True, indexed=True, time=False)
	dateused= dateBone(descr=u"Einlösedatum", searchable=True, required=False, indexed=True)