# -*- coding: utf-8 -*-
#Datastructure for userdata

from server.modules.user_custom import userSkel
from server.bones import *
from server import conf


class userSkel(userSkel):
	student_id = stringBone(descr=u"Schüler Identifikationsnummer", required=True, indexed=True, searchable=True)
	firstname = stringBone(descr=u"Vornamen", required=True, indexed=True, searchable=True)
	lastname = stringBone(descr=u"Nachname", required=True, indexed=True, searchable=True)
	salutation = selectOneBone(descr=u"Anrede", required=True,
	                           values={"Mr.": u"Herr", "Ms.": u"Frau", "none": u"Keine Angabe"})
	role = selectOneBone(descr=u"Rolle", required=True,
	                           values={"admin": u"Administrator", "student": u"Schüler", "worker": u"Mensa Mitarbeiter"})
	balance = numericBone(descr=u"Guthaben", indexed=True,required=True, defaultValue=True)
	
