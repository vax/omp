# -*- coding: utf-8 -*-
from server.bones import *
from server.skeleton import Skeleton


class qrSkel(Skeleton):
	kindName = "qr"
	name = stringBone(descr=u"Name", searchable=True, required=True, indexed=True,
	                  params={"frontend_list_visible": True})
	external_id = stringBone(descr=u"external id", searchable=True, visible=True, required=True, indexed=True,
	                         params={"frontend_list_visible": True})
	descr = textBone(descr=u"Beschreibung", required=False, params={"frontend_list_visible": True})
	target_url = stringBone(descr=u"Ziel URL", searchable=True, required=True, indexed=True,
	                        params={"frontend_list_visible": True})
