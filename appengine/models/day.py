# -*- coding: utf-8 -*-
from server.bones import *
from server.skeleton import Skeleton


class daySkel(Skeleton):
	kindName = "day"
	date = dateBone(descr=u"Ausgabedatum", searchable=True, required=True, indexed=True,time=False)
	menu1 = relationalBone(descr=u"ID Menü 1", searchable=True, visible=True, required=True, indexed=True, type="meal")
	menu2 = relationalBone(descr=u"ID Menü 2", searchable=True, visible=True, required=True, indexed=True, type="meal")
	max_amount = numericBone(descr=u"Maximale Gerichteanzahl", searchable=True, required=True, indexed=True, defaultValue=75)
	total_amount = numericBone(descr=u"Momentane Anzahl", searchable=True, required=True, indexed=True)
	menu1_amount = numericBone(descr=u"Anzahl Menü 1", searchable=True, required=True, indexed=True)
	menu2_amount = numericBone(descr=u"Anzahl Menü 2", searchable=True, required=True, indexed=True)	
	available = booleanBone(descr=u"Verfügbar?", searchable=True, required=True, indexed=True)
	availability_date = dateBone(descr=u"Bis wann ist Stornier/Buchbar", searchable=True, required=True, indexed=True)
		