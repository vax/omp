# -*- coding: utf-8 -*-
from server.bones import *
from server.skeleton import Skeleton


class bookingSkel(Skeleton):
	kindName = "booking"
	#id = stringBone(descr=u"Buchungs Identifikationsnummer", searchable=True, required=True, indexed=True,)
	user = relationalBone(type="user", descr=u"Benutzername", indexed=True, required=False)
	date_of_issue = relationalBone( type="day", descr=u"Tag der Ausgabe",format=u"$(date)", indexed=True, required=False)
	meal = relationalBone( type="meal", descr=u"Gericht", indexed=True, required=False)
	booked = booleanBone(descr=u"Zahlungspflichtig gebucht?", defaultValue=False, visible=True)
	#basket = booleanBone(descr=u"Im Warenkorb?", defaultValue=False, visible=True)
	reversal = booleanBone(descr=u"Storniert?", defaultValue=False, visible=True)