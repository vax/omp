# -*- coding: utf-8 -*-
from server.bones import *
from server.skeleton import Skeleton


class transactionSkel(Skeleton):
	kindName = "transaction"
	#id = stringBone(descr=u"Transaktions Identifikationsnummer", searchable=True, required=True, indexed=True)
	typ = selectOneBone(descr=u"Buchungstyp",values={"charge":u"Aufladung","payment":u"Bezahlung","debit":u"Abbuchung","reversal":u"Stornierung"})
	user_object = relationalBone( type="user", descr=u"Nutzerkonto", indexed=True, required=True)
	user_subject = relationalBone( type="user", descr=u"Ausführender Nutzer", indexed=True, required=False)	
	voucher = relationalBone( type="voucher", descr=u"Gutschein", indexed=True, required=False)
	booking = relationalBone( type="booking", descr=u"Buchung", indexed=True, required=False)
	value = numericBone(descr=u"Wert", required=True, precision=2)