
from server.applications.list import List
from server import errors
from server import request
from server import conf


class voucher(List):
	listTemplate = "voucher_list"
	viewTemplate = "voucher_view"
	columns = ["name","code","value","used","dateused","creationdate",]

	adminInfo = {"name": "voucher",
	             "handler": "list.voucher",  # Which handler to invoke
	             "icon": "icons/modules/voucher.svg",  # Icon for this modul
	             "previewurls": {"Web": "/{{modul}}/view/{{id}}"},
	             "filter": {"orderby": "name"},
	             "columns": columns,
	}

	def listFilter(self, rawfilter):
		return rawfilter

voucher.jinja2 = True