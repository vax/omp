
from server.applications.list import List
from server import errors
from server import request
from server import conf


class meal(List):
	listTemplate = "meal_list"
	viewTemplate = "meal_view"
	columns = ["name",]

	adminInfo = {"name": "meal",
	             "handler": "list.meal",  # Which handler to invoke
	             "icon": "icons/modules/meal.svg",  # Icon for this modul
	             "previewurls": {"Web": "/{{modul}}/view/{{id}}"},
	             "filter": {"orderby": "name"},
	             "columns": columns,
	}

	def listFilter(self, rawfilter):
		return rawfilter

meal.jinja2 = True