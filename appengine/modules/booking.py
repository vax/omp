# -*- coding: utf-8 -*-
from server.applications.list import List
from server import errors
from server import request
from server import conf
from server import exposed
from server import utils

class booking(List):
	listTemplate = "booking_list"
	viewTemplate = "booking_view"
	columns = ["date_of_issue","user","meal","booked","reversal"]

	adminInfo = {"name": "booking",
	             "handler": "list.booking",  # Which handler to invoke
	             "icon": "icons/modules/booking.svg",  # Icon for this modul
	             "previewurls": {"Web": "/{{modul}}/view/{{id}}"},
	             "filter": {"orderby": "name"},
	             "columns": columns,
	}

	def listFilter(self, rawfilter):
		return rawfilter
	
	@exposed
	def book(self, meal,day):
		userobj=utils.getCurrentUser()
		if not userobj:
			raise errors.Unauthorized(u"Nur eingeloggte Nutzer dürfen buchen!")
		if self.viewSkel().all().mergeExternalFilter({"date_of_issue.id":day,"user.id":userobj["id"]}).get():
			return ("An diesem Tag haben Sie bereits ein Essen bestellt")
		
		skel = self.addSkel()		
		skel["meal"].fromClient("id", {"id": meal})
		skel["date_of_issue"].fromClient("id", {"id": day})

		skel["user"].fromClient("id", {"id": userobj["id"]})
		skel.toDB()
		return ("Buchung vorgemerkt")
		
booking.jinja2 = True