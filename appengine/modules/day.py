
from server.applications.list import List
from server import errors
from server import request
from server import conf


class day(List):
	listTemplate = "day_list"
	viewTemplate = "day_view"
	columns = ["date","menu1","menu2","max_amount","total_amount","menu1_amount","menu2_amount","available","availability_date"]

	adminInfo = {"name": "day",
	             "handler": "list.day",  # Which handler to invoke
	             "icon": "icons/modules/day.svg",  # Icon for this modul
	             "previewurls": {"Web": "/{{modul}}/view/{{id}}"},
	             "filter": {"orderby": "name"},
	             "columns": columns,
	}

	def listFilter(self, rawfilter):
		return rawfilter

day.jinja2 = True