# -*- coding: utf-8 -*-

from server.applications.list import List
from server import errors
from server import request
from server import conf


class transaction(List):
	listTemplate = "transaction_list"
	viewTemplate = "transaction_view"
	columns = ["name","typ","user_object","user_subject","voucher","booking","value"]

	adminInfo = {"name": "transaction",
	             "handler": "list.transaction",  # Which handler to invoke
	             "icon": "icons/modules/transaction.svg",  # Icon for this modul
	             "previewurls": {"Web": "/{{modul}}/view/{{id}}"},
	             "filter": {"orderby": "name"},
	             "columns": columns,
	}

def listFilter(self, rawfilter):
		return rawfilter

transaction.jinja2 = True
