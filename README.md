# README #

Hier schreiben wir an einem offenen Mensabestellsystem auf Basis des ViUR-Frameworks

### How do I get set up? ###

* Server: Branch develop: https://bitbucket.org/viur/server
* Vi-Quellcode: https://bitbucket.org/viur/vi
* oder Vi-Compiled: http://staticfiles.mausbrand.de/files/vi-2015-12-21.zip

* Das compilierte "vi" und der "Server" als SubModule wurden hinzugefügt. Änderungen hier werden durch dir Standard .gitignore ignoriert.

### Wichtige Tools für die Entwicklung
* (Git Client graphisch: [TortoiseGit](https://tortoisegit.org))
* (Git Client Kommandozeile: [Git Client](https://git-scm.com/))

* --> Bessere Variante ist SourceTree [SourceTree](https://www.sourcetreeapp.com/))

* Editor: [Notepad ++](https://notepad-plus-plus.org/) oder [Atom Editor](https://atom.io/)
* Google App Engine: [Google App Engine](https://cloud.google.com/appengine/downloads)


### Installation SourceTree

* Legt Euch einen Ordner auf c: oder auf dem Schulrechner in einem eigenen Netzordner an. Ich bevorzuge hier zum Beispiel c:\projekte und dort hinein einen Unterorder für das Projekt "omp"

![Projektordner](images/Projektordner_001.jpg)

* Holt Euch das Installationspaket von der Webseite https://www.sourcetreeapp.com.
* Klickt mit der rechten Maustaste auf und wählt "Als Administrator ausführen" für die Installation.

![SourceTree Installation](images/SourceTree_001.jpg)

* Sofern noch nicht installiert wird automatisch dot.net Framework von Microsoft in der Version 4.5 heruntergeladen und installiert. Dauert einen kleinen Moment.

![SourceTree Installation](images/SourceTree_002.jpg)

* Beim ersten Start sucht SourceTree nach einer passenden Git Version auf Eurem Rechner. Wenn diese nicht vorhanden ist wird sie automatisch geladen:

![SourceTree Start](images/SourceTree_003.jpg)

* SourceTree fragt anschließend nach den globalen "ignore files". Hier könnt Ihr bedenkenlos "Yes wählen":

![SourceTree Start](images/SourceTree_004.jpg)

* SourceTree fragt anschließend nach den Login Daten von BitBucket:

![SourceTree Start](images/SourceTree_005.jpg)
Gebt hier Eure Zugangsdaten ein.

* Danach bekommt Ihr enventuell vorgeschlagen ein Repository zu clonen. Wählt hier "Setup übersprigen":

![SourceTree Start](images/SourceTree_006.jpg)

* Bei der Frage nach einem SSH Schlüssel könnt Ihr "Nein" wählen:

![SourceTree Start](images/SourceTree_007.jpg)

#### Geschafft !

### Arbeitsumgebung klonen

* Zunächst müsst Ihr den Arbeitszweig auf Bitbucket auf Euren Rechner klonen. Geht auf https://bitbucket.org und meldet Euch dort mit Euren Daten an. Geht dann im Dashboard auf das Projekt "Vax / OMP".

* Stellt oben das Übertragungsprotoll auf "HTTPS" ein und klickt dann auf das "Download-Symbol"

![SourceTree Start](images/BitBucket_001.jpg)

* Wählt dann "Clone in SourceTree" aus

![SourceTree Start](images/BitBucket_002.jpg)

* Eventuell müsst Ihr den Zugriff auf SourceTree bestätigen

![SourceTree Start](images/SourceTree_008.jpg)

* In SourceTree Könnt Ihr dann den Zweig auswählen, den Ihr klonen möchtet. Hier ist der "develop" Zweig die richtige Wahl:

![SourceTree Start](images/SourceTree_009.jpg)

* Den Fortschritt könnt Ihr in dem neuen Fenster beobachten:

![SourceTree Start](images/SourceTree_010.jpg)

#### Geschafft !

![SourceTree Start](images/SourceTree_011.jpg)


### Notepad++ und GoogleAppEngine installieren

* Ladet Euch Notepad++ herunter und installiert es

* Ladet Euch die GoogleAppEngine herunter und installiert diese. Hier die Python Version herunterladen

* Bei der Installation der GoogleAppEngine wird festgestellt, dass eventuell noch kein Python installiert ist. Über diesen Dialog könnt Ihr das nachholen:

![SourceTree Start](images/GoogleAppEngine_001.jpg)

* Durch einen Klick auf die Schaltfläche Python.org landet ihr auf der Webseite von Python. Ladet hier die Version 2.7.x für Windows herunter und installiert das Paket (Für alle Benutzer).

![SourceTree Start](images/GoogleAppEngine_002.jpg)

* Jetzt könnt Ihr zu dem Setup der GoogleAppEngine zurückkehren und die Installation fortsetzen.

* Nach dem Start der GoogleAppEngine wählt im Menü den Punkt "Add Existing Application" aus. Der Application Path ist der Projektordner "omp" mit dem Unterorder "appengine"

![SourceTree Start](images/GoogleAppEngine_003.jpg)

![SourceTree Start](images/GoogleAppEngine_004.jpg)

* Jetzt könnt Ihr die Anwendung über "Run" starten. Wenn die Anwendung gestartet ist könnt Ihr auf "Browse" klicken:

![SourceTree Start](images/GoogleAppEngine_005.jpg)

* Jetzt geht ein Browserfenster auf und Ihr könnt Euer Werk betrachten:

![SourceTree Start](images/GoogleAppEngine_006.jpg)

#### Geschafft !
